﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace FormUpdated.Areas.BillingSystem
{
    public class log : ILogger
    {
        private static readonly Lazy<log> Instance = new Lazy<log>(() => new log());

        public log()
        {

        }

        public static log GetInstance
        {
            get
            {
               return Instance.Value;
            }
        }

        public void LogExceptions(Exception ex, string folderPath)
        {
           
            if (!(Directory.Exists(folderPath)))
            {
                Directory.CreateDirectory(folderPath);
            }

            FileStream objFileStrome = new FileStream(folderPath + "/errlog.txt", FileMode.Append, FileAccess.Write);
            StreamWriter objStreamWriter = new StreamWriter(objFileStrome);
            objStreamWriter.Write("Message: " + ex.Message);
            objStreamWriter.Write("StackTrace: " + ex.StackTrace);
            objStreamWriter.Write("Date/Time: " + DateTime.Now.ToString());
            objStreamWriter.Write("============================================");
            objStreamWriter.Close();
            objFileStrome.Close();
        }
    }
}