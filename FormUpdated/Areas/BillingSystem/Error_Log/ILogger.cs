﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormUpdated.Areas.BillingSystem
{
    public interface ILogger
    {
        void LogExceptions(Exception exception, string folderPath);
    }
}
