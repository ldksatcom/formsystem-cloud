﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;

namespace FormUpdated.Areas.BillingSystem.Controllers
{
    [Authorize]
    public class CommonTableController : Controller
    {
        // GET: BillingSystem/CommonTable
        public ActionResult Index()
        {
            return View();
        }

        public List<UploadFiles> getCdrfiles()
        {
            List<UploadFiles> lstfiles = new List<UploadFiles>();

            string folderPath = Server.MapPath("../CDR Files/");
            int i = 1;

            foreach (string file in System.IO.Directory.EnumerateFiles(folderPath, "*.csv"))
            {
                UploadFiles up = new UploadFiles();
                up.id = i++;
                up.location = file;
                up.Filename = Path.GetFileName(file);
                lstfiles.Add(up);
            }

            foreach (string file in System.IO.Directory.EnumerateFiles(folderPath, "*.xlsx"))
            {
                UploadFiles up = new UploadFiles();
                up.id = i++;
                up.location = file;
                up.Filename = Path.GetFileName(file);
                lstfiles.Add(up);
            }

            return lstfiles;
        }

        [Route("Area/BillingSystem")]
        public ActionResult CDRtoCT()
        {
            List<UploadFiles> lstfiles = getCdrfiles();
            Models.BillingSystemEntities db = new Models.BillingSystemEntities();
            ViewBag.LESRef = new SelectList(db.BsMtLESRefs, "LesRefID", "Description");

            ViewBag.Msg = null;
            return View(lstfiles);
        }

        [HttpPost]
        public ActionResult GenerateInvoice(string[] lists)
        {
            Models.BillingSystemEntities db = new Models.BillingSystemEntities();
            ViewBag.LESRef = new SelectList(db.BsMtLESRefs, "LesRefID", "Description");
            string mes = string.Empty;
            string fileName = string.Empty;
            int lesref = 0;

            if (lists.Length > 1)
            {
                fileName = lists[0];

                bool result = int.TryParse(lists[1], out lesref);
                if (result)
                    lesref = lesref;
                else
                    return Json("Please choose appropriate LES Ref", JsonRequestBehavior.AllowGet);
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                 helper_BctFbb helper = new helper_BctFbb();

                var spreadsheet = helper.CDRtoCT(fileName, lesref);

                string LESName = spreadsheet.Item4;
                TempData["ErrorMsg"] = spreadsheet.Item5;
                string destPath = System.IO.Path.Combine(Server.MapPath("../CT Files/"), spreadsheet.Item2);
                string HtmldestPath = System.IO.Path.Combine(Server.MapPath("../HtmlFiles/"), "ErrorDetails.html");
                using (var fileStream = new FileStream(HtmldestPath, FileMode.Create, FileAccess.Write))
                {
                    using (StreamWriter w = new StreamWriter(fileStream, System.Text.Encoding.UTF8))
                    {
                        w.WriteLine(spreadsheet.Item6);
                    }
                }
                    using (var fileStream = new FileStream(destPath, FileMode.Create, FileAccess.Write))
                {
                    spreadsheet.Item1.CopyTo(fileStream);
                }
                using (StreamReader reader = System.IO.File.OpenText(HtmldestPath))
                {
                    System.Text.StringBuilder subject = new System.Text.StringBuilder();
                    subject.AppendLine("Dear Sir,<br />").AppendLine();
                    subject.AppendLine("Good Day!<br />").AppendLine();
                    subject.AppendFormat("The Billing System {0} CDR Successfully stored in Common Table and it CT data is attached excel file, please find.,<br />", LESName).AppendLine();

                    subject.AppendFormat("CDR Upload Status: {0} ", reader.ReadToEnd()).AppendLine();
                    

                    string folderPath = Server.MapPath("../CDR Files/");
                    string CDRFile = fileName;
                    CDRFile += "||" + HtmldestPath;
                    //foreach (string file in System.IO.Directory.EnumerateFiles(folderPath, "*.csv"))
                    //{
                    //    CDRFile = file;
                    //}

                    //string Toemail = "krishna@smtspl.com";
                    //string cc = "lawrence.tan@smtspl.com;marx@smtspl.com;karthi@smtspl.com;senthil@smtspl.com";

                    string Toemail = ConfigurationManager.AppSettings["ToEmail"].ToString();
                    string cc = ConfigurationManager.AppSettings["CCMail"].ToString();
                    mes = helper_OteSat.SendEmail(Toemail, destPath, "Billing System: " + LESName + " CDR to Store Common Table Status Info", subject.ToString(), cc, CDRFile);
                    mes = "Sucessfully stored and " + mes;
                }
            }

            return Json(mes, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Error()
        {
            List<ErrorDetails> lstError = new List<ErrorDetails>();
            if (TempData["ErrorMsg"] != null)
            {
                lstError = (List<ErrorDetails>)TempData["ErrorMsg"];
            }
            TempData.Keep();
            return View(lstError);
        }

        [HttpPost]
        public ActionResult Delete(int? id)
        {
            return View("otesatCdrSave");
        }

        [HttpPost]
        public ActionResult RemoveCDR()
        {
            string fname = System.IO.Path.Combine(Server.MapPath("../CDR Files/"));
            foreach (string file in System.IO.Directory.EnumerateFiles(fname, "*.xlsx"))
            {
                System.IO.File.Delete(file);
                ViewBag.Msg = "The file was deleted successfully.";
            }

            foreach (string file in System.IO.Directory.EnumerateFiles(fname, "*.csv"))
            {
                System.IO.File.Delete(file);
                ViewBag.Msg = "The file was deleted successfully.";
            }
            return View("otesatCdrSave");
        }


        [HttpPost]
        public ActionResult RemoveCDRFiles(string[] lists)
        {
            string msg = string.Empty;
            if (lists.Length > 0)
            {
                string filename = string.Empty;
                foreach (string file in lists)
                {
                    filename = System.IO.Path.GetFileNameWithoutExtension(file);
                    System.IO.File.Delete(file);
                    msg += string.Format("The file {0} was deleted successfully.", filename);
                }
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RemoveCT()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AdoConnstr"].ToString()))
            {
                try
                {
                    con.Open();
                    //SqlCommand cmd = new SqlCommand("delete from BCtBs; delete from BCtAs;", con);
                    SqlCommand cmd = new SqlCommand("delete from BCtFbbTrans", con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                    ViewBag.Msg = "Common table records removed successfully";

                    string folderPath = Server.MapPath("../CDR Files/");
                    foreach (string file in System.IO.Directory.EnumerateFiles(folderPath, "*.csv"))
                    {
                        ViewBag.File = file;
                    }
                }
                catch
                {
                    ViewBag.Msg = "Not clear Common Table, please try again";
                }
            }

            Models.BillingSystemEntities db = new Models.BillingSystemEntities();
            ViewBag.LESRef = new SelectList(db.BsMtLESRefs, "LesRefID", "Description");

            //using (BillingSystemEntities db = new BillingSystemEntities()) {
            //try
            //{
            //    var cta = db.BCtAs.ToList();
            //    var ctb = db.BCtBs.ToList();

            //    foreach (var b in ctb)
            //    {
            //        db.BCtBs.Remove(b);
            //    }

            //    foreach (var a in cta)
            //    {
            //        db.BCtAs.Remove(a);
            //    }

            //    db.SaveChanges();

            //    ViewBag.Msg = "Common table records removed successfully";

            //    string folderPath = Server.MapPath("../CDR Files/");
            //    foreach (string file in System.IO.Directory.EnumerateFiles(folderPath, "*.xlsx"))
            //    {
            //        ViewBag.File = file;
            //    }
            //}
            //catch
            //{
            //    ViewBag.Msg = "Not clear Common Table, please try again";
            //}
            //}
            return View("otesatCdr2Ct");
        }

        [HttpPost]
        public ActionResult RemoveCTRecords()
        {
            string mes = string.Empty;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AdoConnstr"].ToString()))
            {
                try
                {
                    con.Open();
                    //SqlCommand cmd = new SqlCommand("delete from BCtBs; delete from BCtAs;", con);
                    SqlCommand cmd = new SqlCommand("delete from BCtFbb", con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                    mes = "Common table records removed successfully";
                }
                catch
                {
                    mes = "Not clear Common Table, please try again";
                }
            }

            Models.BillingSystemEntities db = new Models.BillingSystemEntities();
            ViewBag.LESRef = new SelectList(db.BsMtLESRefs, "LesRefID", "Description");

            return Json(mes, JsonRequestBehavior.AllowGet);
        }
    }
}