﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers
{
    [Authorize]
    public class StatementsController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        private ILogger logger;

        public StatementsController()
        {
            logger = log.GetInstance;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            string folderPath = System.Configuration.ConfigurationManager.AppSettings["ErrorFolder"];
            folderPath = Server.MapPath(folderPath);

            logger.LogExceptions(filterContext.Exception, folderPath);

            filterContext.ExceptionHandled = true;
            this.View("Error").ExecuteResult(this.ControllerContext);
        }

        // GET: BillingSystem/Statements
        public ActionResult Index()
        {
            return View(db.Statements.ToList());
        }

        public ActionResult ListStatement()
        {
            return View(db.Bs2Statement.ToList());
        }

        [HttpPost]
        public ActionResult ExportExcel()
        {
            try
            {
                var dictStatement = db.Statements.ToList();
                string date = DateTime.Now.Date.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                string tempPath = System.IO.Path.GetTempPath() + "Statements" + date + ".xlsx";

                string handle = Guid.NewGuid().ToString();
                BillingHelpers.ExportDataSet(dictStatement, tempPath);
                TempData[handle] = handle;
                var data = new Dictionary<string, string>();
                data.Add("FileGuid", handle);
                data.Add("FileName", tempPath);

                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return new JsonResult() { };
            }
        }

        [HttpGet]
        public virtual ActionResult Download(string fileGuid, string fileName)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = System.IO.File.ReadAllBytes(fileName);
                return File(data, "application/vnd.ms-excel", fileName);
            }
            else
            {
                // Problem - Log the error, generate a blank file,
                //           redirect to another controller action - whatever fits with your application
                return new EmptyResult();
            }
        }

        // GET: BillingSystem/Statements/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Statement statement = db.Statements.Find(id);
            if (statement == null)
            {
                return HttpNotFound();
            }
            return View(statement);
        }

        // GET: BillingSystem/Statements/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BillingSystem/Statements/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "stmtID,InvoiceNo,Duration,LES,Customer,Vessel,TotalCP,TotalSP,Margin,invFileID,IvoiceDate,IMSIID")] Statement statement)
        {
            if (ModelState.IsValid)
            {
                db.Statements.Add(statement);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(statement);
        }

        // GET: BillingSystem/Statements/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Statement statement = db.Statements.Find(id);
            if (statement == null)
            {
                return HttpNotFound();
            }
            return View(statement);
        }

        // POST: BillingSystem/Statements/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "stmtID,InvoiceNo,Duration,LES,Customer,Vessel,TotalCP,TotalSP,Margin,invFileID,IvoiceDate,IMSIID")] Statement statement)
        {
            if (ModelState.IsValid)
            {
                db.Entry(statement).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(statement);
        }

        // GET: BillingSystem/Statements/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Statement statement = db.Statements.Find(id);
            if (statement == null)
            {
                return HttpNotFound();
            }
            return View(statement);
        }

        // POST: BillingSystem/Statements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Statement statement = db.Statements.Find(id);
            db.Statements.Remove(statement);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
