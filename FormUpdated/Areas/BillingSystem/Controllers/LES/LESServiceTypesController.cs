﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers
{
    [Authorize]
    public class LESServiceTypesController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/LESServiceTypes
        public ActionResult Index()
        {
            var bsPtSPServiceDests = db.BsPtSPServiceDests.Include(b => b.BsMtCommunicationType).Include(b => b.BsMtServiceProvider).Include(b => b.BsMtUnit).Include(b => b.BsPtSPLesReg);
            return View(bsPtSPServiceDests.ToList());
        }

        // GET: BillingSystem/LESServiceTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtSPServiceDest bsPtSPServiceDest = db.BsPtSPServiceDests.Find(id);
            if (bsPtSPServiceDest == null)
            {
                return HttpNotFound();
            }
            return View(bsPtSPServiceDest);
        }

        // GET: BillingSystem/LESServiceTypes/Create
        public ActionResult Create()
        {
            ViewBag.ComTypeID = new SelectList(db.BsMtCommunicationTypes, "ComID", "ComTypes");
            ViewBag.fkSpID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider");
            ViewBag.fkUnitsType = new SelectList(db.BsMtUnits, "UID", "Units");
            ViewBag.BPtServTfkLesRegID = new SelectList(db.BsPtSPLesRegs, "SPLesRegID", "BsPtlesRegFbContNo");
            return View();
        }

        // POST: BillingSystem/LESServiceTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SPServDID,fkSpID,BsPtServiceTypes,fkUnitsType,BsPtServiceCostPrice,BsPtServDescription,ComTypeID,BsPtServAsDescription,BPtServTfkLesRegID")] BsPtSPServiceDest bsPtSPServiceDest)
        {
            if (ModelState.IsValid)
            {
                db.BsPtSPServiceDests.Add(bsPtSPServiceDest);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ComTypeID = new SelectList(db.BsMtCommunicationTypes, "ComID", "ComTypes", bsPtSPServiceDest.ComTypeID);
            ViewBag.fkSpID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider", bsPtSPServiceDest.fkSpID);
            ViewBag.fkUnitsType = new SelectList(db.BsMtUnits, "UID", "Units", bsPtSPServiceDest.fkUnitsType);
            ViewBag.BPtServTfkLesRegID = new SelectList(db.BsPtSPLesRegs, "SPLesRegID", "BsPtlesRegFbContNo", bsPtSPServiceDest.BPtServTfkLesRegID);
            return View(bsPtSPServiceDest);
        }

        // GET: BillingSystem/LESServiceTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtSPServiceDest bsPtSPServiceDest = db.BsPtSPServiceDests.Find(id);
            if (bsPtSPServiceDest == null)
            {
                return HttpNotFound();
            }
            ViewBag.ComTypeID = new SelectList(db.BsMtCommunicationTypes, "ComID", "ComTypes", bsPtSPServiceDest.ComTypeID);
            ViewBag.fkSpID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider", bsPtSPServiceDest.fkSpID);
            ViewBag.fkUnitsType = new SelectList(db.BsMtUnits, "UID", "Units", bsPtSPServiceDest.fkUnitsType);
            ViewBag.BPtServTfkLesRegID = new SelectList(db.BsPtSPLesRegs, "SPLesRegID", "BsPtlesRegFbContNo", bsPtSPServiceDest.BPtServTfkLesRegID);
            return View(bsPtSPServiceDest);
        }

        // POST: BillingSystem/LESServiceTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SPServDID,fkSpID,BsPtServiceTypes,fkUnitsType,BsPtServiceCostPrice,BsPtServDescription,ComTypeID,BsPtServAsDescription,BPtServTfkLesRegID")] BsPtSPServiceDest bsPtSPServiceDest)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsPtSPServiceDest).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ComTypeID = new SelectList(db.BsMtCommunicationTypes, "ComID", "ComTypes", bsPtSPServiceDest.ComTypeID);
            ViewBag.fkSpID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider", bsPtSPServiceDest.fkSpID);
            ViewBag.fkUnitsType = new SelectList(db.BsMtUnits, "UID", "Units", bsPtSPServiceDest.fkUnitsType);
            ViewBag.BPtServTfkLesRegID = new SelectList(db.BsPtSPLesRegs, "SPLesRegID", "BsPtlesRegFbContNo", bsPtSPServiceDest.BPtServTfkLesRegID);
            return View(bsPtSPServiceDest);
        }

        // GET: BillingSystem/LESServiceTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtSPServiceDest bsPtSPServiceDest = db.BsPtSPServiceDests.Find(id);
            if (bsPtSPServiceDest == null)
            {
                return HttpNotFound();
            }
            return View(bsPtSPServiceDest);
        }

        // POST: BillingSystem/LESServiceTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsPtSPServiceDest bsPtSPServiceDest = db.BsPtSPServiceDests.Find(id);
            db.BsPtSPServiceDests.Remove(bsPtSPServiceDest);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
