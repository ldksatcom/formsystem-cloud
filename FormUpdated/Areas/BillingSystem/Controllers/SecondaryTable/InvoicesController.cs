﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClosedXML.Excel;
using FormUpdated.Areas.BillingSystem.Models;
using Ionic.Zip;

namespace FormUpdated.Areas.BillingSystem.Controllers
{
    [Authorize]
    public class InvoicesController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        private ILogger logger;
        public InvoicesController()
        {
            logger = log.GetInstance;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            string folderPath = System.Configuration.ConfigurationManager.AppSettings["ErrorFolder"];
            folderPath = Server.MapPath(folderPath);

            if (folderPath.Contains("FormUpdated"))
            {
                string[] stringSeparators = new string[] { "FormUpdated" };
                string[] index = folderPath.Split(stringSeparators, StringSplitOptions.None);
                index[0] = index[0] + @"FormUpdated\Areas\BillingSystem";
                folderPath = index[0] + index[1];
            }

            logger.LogExceptions(filterContext.Exception, folderPath);

            filterContext.ExceptionHandled = true;
            this.View("Error").ExecuteResult(this.ControllerContext);
        }

        // GET: BillingSystem/Invoices
        public ActionResult Index()
        {
            int year, month;
            if (Request.Form["Years"] != null && Request.Form["Months"] != null)
            {
                year = Convert.ToInt32(Request.Form["Years"]);
                month = Convert.ToInt32(Request.Form["Months"]);

                if (year == -1 || month == -1)
                    return View(db.BSt2A.ToList());
                try
                {
                    DateTime statDate = Convert.ToDateTime(month + "/" + "01/" + year);
                    DateTime endDate = DateTime.Now;
                    try
                    {
                        if (month.Equals("2"))
                            endDate = statDate.AddDays(27);
                        else
                            endDate = statDate.AddDays(30);
                    }
                    catch
                    {
                        endDate = statDate.AddDays(28);
                    }

                    if (!endDate.Month.Equals(Convert.ToInt32(month)))
                        endDate = endDate.AddDays(-1);

                    List<BSt2A> invoicelst = (from invoice in db.BSt2A
                                              where invoice.Bt2CusFbContStart >= statDate.Date && invoice.Bt2CdrFbContEnd <= endDate.Date
                                                  select invoice).ToList();


                    return View(invoicelst);
                }
                catch (Exception err)
                {
                    return View(db.BSt2A.ToList());
                }
            }
            return View(db.BSt2A.ToList());
        }

        public ActionResult Invoice()
        {
            return View(db.BS2MtCusT.ToList());
        }

        [HttpPost]
        public ActionResult Index(string[] lists)
        {
            foreach (string id in lists)
            {
                int value = 0;
                if (int.TryParse(id, out value))
                {
                    BSt2A bSt2A = db.BSt2A.FirstOrDefault(t => t.Bst2A_ID.Equals(value));

                    Bst1A bst1A = new Bst1A();
                    bst1A.Bt1LesFbIMSIId = bSt2A.Bt2CusFbIMSIId;
                    bst1A.Bt1LesFbMSIDNNo = bSt2A.Bt2CusFbMSIDNNo;
                    bst1A.Bt1LesFbSimIdNo = bSt2A.Bt2CusFbSimIdNo;
                    bst1A.Bt1LesFbServType = bSt2A.Bt2CusFbServType;
                    db.Bst1A.Add(bst1A);

                    db.BSt2A.Remove(bSt2A);

                    db.SaveChanges();

                }
            }
            return RedirectToAction("Index");
        }

        public System.IO.Stream GetStream(XLWorkbook exwb)
        {
            System.IO.Stream fs = new System.IO.MemoryStream();
            exwb.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        [HttpPost]
        public ActionResult ExcelInvoice(string list)
        {
            return Json(list);
        }

        [HttpGet]
        public ActionResult Test()
        {
            return Json("Server Reached", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("Area/BillingSystem")]
        public ActionResult GenerateInvoices(string[] list)
        {
            try
            {
                // getting Invoice comapny logo path if empty then
                if (!string.IsNullOrEmpty(MySession.InvoiceLogoPath) || string.IsNullOrEmpty(MySession.InvoiceLogoPath))
                {
                    try
                    {
                        string mapPath = Server.MapPath("../Images/smts logo.jpg");
                        if (mapPath.Contains("FormUpdated"))
                        {
                            string[] stringSeparators = new string[] { "FormUpdated" };
                            string[] index = mapPath.Split(stringSeparators, StringSplitOptions.None);
                            index[0] = index[0] + @"FormUpdated\Areas";
                            mapPath = index[0] + index[1];
                            MySession.InvoiceLogoPath = mapPath;
                        }//SWG:\Project\Sprint11_6\Dev11_6\FormUpdated\Areas\BillingSystem\Images\Excel.jpg
                    }
                    catch { }
                }

                var data = new Dictionary<string, string>();
                BillingHelpers billingHelpers = new BillingHelpers();
                List<Dictionary<string, System.IO.Stream>> dictdata = new List<Dictionary<string, Stream>>();

                data.Add("FileGuid", Convert.ToString(""));
                data.Add("FileName", "");

                foreach (string id in list)
                {
                    //Invoice 
                    var spreadsheetstream = billingHelpers.GnerateInvoice(id);

                    if (spreadsheetstream.Item1 != null && !string.IsNullOrEmpty(spreadsheetstream.Item2))
                    {
                        Dictionary<string, Stream> valuePairs = new Dictionary<string, Stream>();
                        valuePairs.Add(spreadsheetstream.Item2, (System.IO.Stream)spreadsheetstream.Item1);
                        dictdata.Add(valuePairs);
                    }
                    //else
                    //{
                    //    data.Add("FileNameInvoice" + id, "Error Invoice");
                    //}

                    //Call Log
                    spreadsheetstream = billingHelpers.GnerateCalllog(id);

                    if (spreadsheetstream.Item1 != null && !string.IsNullOrEmpty(spreadsheetstream.Item2))
                    {
                        Dictionary<string, Stream> valuePairs = new Dictionary<string, Stream>();
                        valuePairs.Add(spreadsheetstream.Item2, (System.IO.Stream)spreadsheetstream.Item1);
                        dictdata.Add(valuePairs);
                    }
                    //else
                    //    data.Add("callLog" + id, "Error Call Log");
                }

                if (dictdata.Count > 0)
                {
                    TempData["DictData"] = (List<Dictionary<string, Stream>>)dictdata;
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                    return View("./Index");
            }
            catch (Exception error)
            {
                return Json("Error: " + error.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [Route("Area/BillingSystem")]
        public ActionResult DownloadInvoices(string[] list)
        {
            var data = new Dictionary<string, string>();

            try
            {
                // getting Invoice comapny logo path if empty then
                //if (string.IsNullOrEmpty(MySession.InvoiceLogoPath) || !string.IsNullOrEmpty(MySession.InvoiceLogoPath))
                //{
                //    try
                //    {
                //        string mapPath = Server.MapPath("../Images/smts logo.jpg");
                //        if (mapPath.Contains("FormUpdated"))
                //        {
                //            string[] stringSeparators = new string[] { "FormUpdated" };
                //            string[] index = mapPath.Split(stringSeparators, StringSplitOptions.None);
                //            index[0] = index[0] + @"FormUpdated\Areas";
                //            mapPath = index[0] + index[1];
                //            MySession.InvoiceLogoPath = mapPath;
                //        }
                //    }
                //    catch { }
                //}

                string mapPath = Server.MapPath("../Images/smts logo.jpg");

                helper_NSSLGlobal billingHelpers = new helper_NSSLGlobal();
                List<Dictionary<string, System.IO.Stream>> dictdata = new List<Dictionary<string, Stream>>();
                List<int> primayIDs = new List<int>();

                data.Add("FileGuid", Convert.ToString(""));
                data.Add("FileName", "");
                try
                {
                    foreach (string id in list)
                    {

                        #region Generate Invoice 
                        //Invoice 
                        var spreadsheetstream = billingHelpers.GnerateInvoice(id, mapPath);

                        if(spreadsheetstream.Item3)
                        {
                            if(data.Count>0)
                            {
                                data.Clear();
                            }

                            data.Add("FileGuid", Convert.ToString("0"));
                            data.Add("FileName", "SessionExpired");

                            return Json(data, JsonRequestBehavior.AllowGet);
                        }

                        if (spreadsheetstream.Item1 != null && !string.IsNullOrEmpty(spreadsheetstream.Item2))
                        {
                            Dictionary<string, Stream> valuePairs = new Dictionary<string, Stream>();
                            valuePairs.Add(spreadsheetstream.Item2, (System.IO.Stream)spreadsheetstream.Item1);
                            dictdata.Add(valuePairs);
                        }

                        #endregion

                        #region Generate Call Log

                        // Call Log
                        var spreadsheetstreamCallLog = billingHelpers.GnerateCalllog(id, mapPath);

                        if (spreadsheetstreamCallLog.Item1 != null && !string.IsNullOrEmpty(spreadsheetstreamCallLog.Item2))
                        {
                            Dictionary<string, Stream> valuePairs = new Dictionary<string, Stream>();
                            valuePairs.Add(spreadsheetstreamCallLog.Item2, (System.IO.Stream)spreadsheetstreamCallLog.Item1);
                            dictdata.Add(valuePairs);
                        }
                        //else
                        //    data.Add("callLog" + id, "Error Call Log");

                        #endregion

                        primayIDs.Add(Convert.ToInt32(id));
                    }

                    #region Generate Invoice List Report

                    var spreadsheetInvList = billingHelpers.GnerateInvListReport(primayIDs, mapPath);

                    if (spreadsheetInvList.Item1 != null && !string.IsNullOrEmpty(spreadsheetInvList.Item2))
                    {
                        Dictionary<string, Stream> valuePairs = new Dictionary<string, Stream>();
                        valuePairs.Add(spreadsheetInvList.Item2, (System.IO.Stream)spreadsheetInvList.Item1);
                        dictdata.Add(valuePairs);
                    }

                    #endregion
                }
                catch (Exception error)
                {
                    string folderPath = System.Configuration.ConfigurationManager.AppSettings["ErrorFolder"];
                    folderPath = Server.MapPath(folderPath);

                    if (folderPath.Contains("FormUpdated"))
                    {
                        string[] stringSeparators = new string[] { "FormUpdated" };
                        string[] index = folderPath.Split(stringSeparators, StringSplitOptions.None);
                        index[0] = index[0] + @"FormUpdated\Areas\BillingSystem";
                        folderPath = index[0] + index[1];
                    }

                    logger.LogExceptions(error, folderPath);

                    if (data.Count > 0)
                    {
                        data.Clear();
                    }

                    data.Add("FileGuid", Convert.ToString("0"));
                    data.Add("FileName", "SessionExpired");

                    return Json(data, JsonRequestBehavior.AllowGet);
                }

                if (dictdata.Count > 0)
                {
                    TempData["DictData"] = (List<Dictionary<string, Stream>>)dictdata;
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string folderPath = System.Configuration.ConfigurationManager.AppSettings["ErrorFolder"];
                    folderPath = Server.MapPath(folderPath);

                    if (folderPath.Contains("FormUpdated"))
                    {
                        string[] stringSeparators = new string[] { "FormUpdated" };
                        string[] index = folderPath.Split(stringSeparators, StringSplitOptions.None);
                        index[0] = index[0] + @"FormUpdated\Areas\BillingSystem";
                        folderPath = index[0] + index[1];
                    }

                    logger.LogExceptions(new Exception("the dictinary(invoice and call log ) not found in this case."), folderPath);

                    if (data.Count > 0)
                    {
                        data.Clear();
                    }

                    data.Add("FileGuid", Convert.ToString("0"));
                    data.Add("FileName", "SessionExpired");

                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                    
            }
            catch (Exception error)
            {
                string folderPath = System.Configuration.ConfigurationManager.AppSettings["ErrorFolder"];
                folderPath = Server.MapPath(folderPath);

                if (folderPath.Contains("FormUpdated"))
                {
                    string[] stringSeparators = new string[] { "FormUpdated" };
                    string[] index = folderPath.Split(stringSeparators, StringSplitOptions.None);
                    index[0] = index[0] + @"FormUpdated\Areas\BillingSystem";
                    folderPath = index[0] + index[1];
                }

                logger.LogExceptions(error, folderPath);

                if (data.Count > 0)
                {
                    data.Clear();
                }

                data.Add("FileGuid", Convert.ToString("0"));
                data.Add("FileName", "SessionExpired");

                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ExcelInvoice(string[] list)
        {
            if (string.IsNullOrEmpty(MySession.InvoiceLogoPath))
            {
                try
                {
                    string mapPath = Server.MapPath("../Images/smts logo.jpg");
                    if (mapPath.Contains("FormUpdated"))
                    {
                        string[] stringSeparators = new string[] { "FormUpdated" };
                        string[] index = mapPath.Split(stringSeparators, StringSplitOptions.None);
                        index[0] = index[0] + @"FormUpdated\Areas";
                        mapPath = index[0] + index[1];
                        MySession.InvoiceLogoPath = mapPath;
                    }
                }
                catch { }
            }

            BillingHelpers billingHelpers = new BillingHelpers();
            var data = new Dictionary<string, string>();
            List<Dictionary<string, System.IO.Stream>> dictdata = new List<Dictionary<string, Stream>>();
            int count = 0;

            data.Add("FileGuid", Convert.ToString(""));

            data.Add("FileName", "");

            foreach (string id in list)
            {
                //Invoice 
                var spreadsheetstream = billingHelpers.GnerateInvoice(id);

                if (spreadsheetstream.Item1 != null && !string.IsNullOrEmpty(spreadsheetstream.Item2))
                {
                    Dictionary<string, Stream> valuePairs = new Dictionary<string, Stream>();
                    valuePairs.Add(spreadsheetstream.Item2, (System.IO.Stream)spreadsheetstream.Item1);
                    dictdata.Add(valuePairs);
                }

                //Call Log
                spreadsheetstream = billingHelpers.GnerateCalllog(id);

                if (spreadsheetstream.Item1 != null && !string.IsNullOrEmpty(spreadsheetstream.Item2))
                {
                    Dictionary<string, Stream> valuePairs = new Dictionary<string, Stream>();
                    valuePairs.Add(spreadsheetstream.Item2, (System.IO.Stream)spreadsheetstream.Item1);
                    dictdata.Add(valuePairs);
                }
            }
            
            if (dictdata.Count > 0)
            {
                TempData["DictData"] = (List<Dictionary<string, Stream>>)dictdata;
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
                return View("./Index");

            //System.IO.Compression.ZipArchive zip = null;

            //string zipFiles = Server.MapPath("~/zipfiles.zip");

            //using (ZipFile zip = new ZipFile())
            //{
            //    foreach (string id in list)
            //    {
            //        var spreadsheetstream = billingHelpers.GnerateInvoice(id, temp);
            //        zip.AlternateEncodingUsage = ZipOption.AsNecessary;
            //        //zip.AddDirectoryByName("Files");
            //        zip.AddEntry(spreadsheetstream.Item2, spreadsheetstream.Item1);
            //    }

            //    using (MemoryStream memoryStream = new MemoryStream())
            //    {
            //        zip.Save(memoryStream);
            //        return File(memoryStream.ToArray(), "application/zip", "Invoices.zip");
            //    }

            //}

        }

        private const long BUFFER_SIZE = 4096;
        private static void CopyStream(System.IO.FileStream inputStream, System.IO.Stream outputStream)
        {
            long bufferSize = inputStream.Length < BUFFER_SIZE ? inputStream.Length : BUFFER_SIZE;
            byte[] buffer = new byte[bufferSize];
            int bytesRead = 0;
            long bytesWritten = 0;
            while ((bytesRead = inputStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                outputStream.Write(buffer, 0, bytesRead);
                bytesWritten += bufferSize;
            }


        }

        public System.Data.DataTable getcallLogData()
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString))
            using (var cmd = new SqlCommand("CallLogHeaderData", conn))
            using (var adapter = new SqlDataAdapter(cmd))
            {
                System.Data.DataSet ds = new System.Data.DataSet();
                adapter.Fill(ds);

                var invoice = new System.Data.DataTable();
                adapter.Fill(invoice);
                return invoice;

            }
        }

        [HttpPost]
        public ActionResult InvoiceCallLog(string[] List)
        {
            if (string.IsNullOrEmpty(MySession.InvoiceLogoPath))
            {
                try
                {
                    string mapPath = Server.MapPath("../Images/smts logo.jpg");
                    if (mapPath.Contains("FormUpdated"))
                    {
                        string[] stringSeparators = new string[] { "FormUpdated" };
                        string[] index = mapPath.Split(stringSeparators, StringSplitOptions.None);
                        index[0] = index[0] + @"FormUpdated\Areas";
                        mapPath = index[0] + index[1];
                        MySession.InvoiceLogoPath = mapPath;
                    }
                }
                catch { }
            }

            List<BCtB> bctb = new List<BCtB>();
            List<BPtCustomer> bPtCustomers = db.BPtCustomers.ToList();

            foreach (string id in List)
            {
                int value = 0;
                string imsiID = string.Empty;
                string InvoiceNo = string.Empty;
                string CustomerAddress = string.Empty;
                string customerName = string.Empty;
                string CallLogName = string.Empty;

                if (int.TryParse(id, out value))
                {
                    try
                    {
                        BSt2A bSt2A = db.BSt2A.FirstOrDefault(t => t.Bst2A_ID.Equals(value));
                        if (bSt2A != null)
                        {
                            imsiID = bSt2A.Bt2CusFbIMSIId;
                            InvoiceNo = bSt2A.InvoiceNo;
                            CustomerAddress = bSt2A.Bt2CusFbCustomerAddress;

                            if (!string.IsNullOrEmpty(CustomerAddress))
                            {
                                string[] addressparts = CustomerAddress.Split(',');

                                customerName = addressparts.Length > 0 ? addressparts[0] : string.Empty;
                                //street = addressparts.Length > 1 ? addressparts[1] : string.Empty;
                                //district = addressparts.Length > 2 ? addressparts[2] : string.Empty;
                                //city = addressparts.Length > 3 ? addressparts[3] : string.Empty;
                                //country = addressparts.Length > 4 ? addressparts[4] : string.Empty;
                            }

                            CallLogName = InvoiceNo + 'C' + '_' + customerName + '_' + "Vessel Name" + ".xlsx";

                            //ExcelPackage excel = new ExcelPackage();
                            System.IO.Stream SpreadsheetStream = new System.IO.MemoryStream();
                            XLWorkbook xLWorkbook = new XLWorkbook();
                            IXLWorksheet workSheet = xLWorkbook.Worksheets.Add("CallLog");

                            workSheet.Column(1).Width = 16;
                            workSheet.Column(2).Width = 15;
                            workSheet.Column(3).Width = 25;
                            workSheet.Column(4).Width = 10;
                            workSheet.Column(5).Width = 10;
                            workSheet.Column(6).Width = 10;
                            workSheet.Column(7).Width = 10;
                            workSheet.Column(8).Width = 10;
                            workSheet.Cell(2, 1).Style.Font.Bold = true;
                            workSheet.Cell(4, 1).Style.Font.Bold = true;
                            workSheet.Cell(11, 1).Style.Font.Bold = true;
                            workSheet.Cell(12, 1).Style.Font.Bold = true;
                            workSheet.Cell(12, 2).Style.Font.Bold = true;
                            workSheet.Cell(3, 6).Style.Font.Bold = true;
                            workSheet.Row(6).Style.Font.Bold = true;

                            int j = 6;
                            while (j < 78)
                            {
                                workSheet.Row(j).Style.Font.FontSize = 10;
                                j++;
                            }

                            string Imagepath = MySession.InvoiceLogoPath;
                            var image = workSheet.AddPicture(Imagepath)
                                         .MoveTo(workSheet.Cell(2, 1)).Scale(1.05);

                            //workSheet.Cell(2, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                            //workSheet.Cell(2, 1).Value = "Pte Ltd";
                            //workSheet.Cell(2, 1).Style.Font.Bold = true;
                            //workSheet.Cell(2, 1).Style.Font.FontSize = 20;
                            workSheet.Range("A2:B2").Merge();
                            // workSheet.Cell(2, 1).Value = "SMTS Pte Ltd";
                            //workSheet.Cell(2, 1).Style.Font.FontSize = 20;
                            workSheet.Cell(4, 1).Value = "Call Data Details";
                            workSheet.Cell(4, 1).Style.Font.FontSize = 20;
                            workSheet.Row(12).Style.Font.Bold = false;
                            workSheet.Cell(6, 1).Value = "Invoice No";
                            workSheet.Cell(6, 2).Value = InvoiceNo;
                            workSheet.Cell(6, 5).Value = "Customer";
                            workSheet.Cell(6, 6).Value = customerName;
                            workSheet.Row(6).Style.Font.Bold = true;
                            workSheet.Cell(7, 1).Value = "Invoice Date";
                            workSheet.Cell(7, 2).Value = DateTime.Now.Date;
                            workSheet.Cell(7, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);

                            workSheet.Cell(7, 5).Value = "LES";
                            // workSheet.Cell(7, 6).Value = LES.ToString();
                            workSheet.Row(7).Style.Font.Bold = true;

                            workSheet.Cell(8, 5).Value = "Vessel";
                            // workSheet.Cell(8, 6).Value = Vessel.ToString();
                            workSheet.Row(8).Style.Font.Bold = true;

                            workSheet.Cell(10, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            workSheet.Cell(10, 1).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                            workSheet.Cell(10, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            workSheet.Cell(10, 2).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                            workSheet.Cell(10, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            workSheet.Cell(10, 3).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                            workSheet.Cell(10, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            workSheet.Cell(10, 4).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                            workSheet.Cell(10, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            workSheet.Cell(10, 5).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                            workSheet.Cell(10, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            workSheet.Cell(10, 6).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                            workSheet.Cell(10, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                            workSheet.Cell(10, 7).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                            workSheet.Cell(10, 1).Value = "Date/Time";
                            workSheet.Cell(10, 2).Value = "LES";
                            workSheet.Cell(10, 3).Value = "Orgin//Destination";
                            workSheet.Cell(10, 4).Value = "Usage";
                            workSheet.Cell(10, 5).Value = "Unit";
                            workSheet.Cell(10, 6).Value = "Rate";
                            workSheet.Cell(10, 7).Value = "Charges(USD)";

                            //workSheet.Cell(25, 6).Value = "Total(USD)";

                            workSheet.Row(10).Style.Font.Bold = true;

                            workSheet.Cell(10, 1).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 1).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 1).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 1).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 2).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 2).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 2).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 2).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 3).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 3).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 3).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 3).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 4).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 4).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 4).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 4).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 5).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 5).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 5).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 5).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 6).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 6).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 6).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 6).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 7).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 7).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 7).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                            workSheet.Cell(10, 7).Style.Border.LeftBorder = XLBorderStyleValues.Medium;

                            workSheet.Row(25).Style.Font.Bold = true;
                            workSheet.SheetView.Freeze(10, 0);

                            int count = 11, cond = 11;

                            int dataServType = 0, VceServType = 0;

                            List<string> DataServiceDest = db.BSt2B.AsEnumerable().Where(t => t.Bt2CusFbIMSIId != null && t.Bt2CusFbTranDServType != null && t.Bt2CusFbIMSIId.Equals(imsiID))
                                                        .Select(t => t.Bt2CusFbTranDServType).Distinct().ToList();
                            List<string> VceServiceDest = db.BSt2B.AsEnumerable().Where(t => t.Bt2CusFbIMSIId != null && t.Bt2CusFbTranVServType != null && t.Bt2CusFbIMSIId.Equals(imsiID))
                                                        .Select(t => t.Bt2CusFbTranVServType).Distinct().ToList();

                            if (DataServiceDest.Count > 1 && DataServiceDest.Contains("Subscription Fee"))
                            {
                                DataServiceDest.Remove("Subscription Fee");
                            }

                            List<BSt2B> lstInvoiceB;
                            BPtFbbCusReg cusRegSP = null;
                            try
                            {
                                cusRegSP = db.BPtFbbCusRegs.FirstOrDefault(t => t.BPtCusFBimSingle.Equals(imsiID));
                            }
                            catch
                            {
                                //ErrorMsg.AppendFormat("The IMSI: {0} not registerd in Customer Registeration Table.", Imsi).AppendLine();
                            }

                            decimal? TotalChargeUSD = 0;

                            decimal? dateRate1MB_SP = 0;
                            decimal? Vce2Fixed1MIN_SP = 0;
                            decimal? Vce2Cell1Min_SP = 0;
                            decimal? Vce2Fbb1Min_SP = 0;

                            if (cusRegSP != null)
                            {
                                dateRate1MB_SP = cusRegSP.BPtCusFbDataRate;
                                dateRate1MB_SP = Math.Round(Convert.ToDecimal(dateRate1MB_SP), 2, MidpointRounding.AwayFromZero);

                                Vce2Fixed1MIN_SP = cusRegSP.BPtCusFbVce2Fixed;
                                Vce2Fixed1MIN_SP = Math.Round(Convert.ToDecimal(Vce2Fixed1MIN_SP), 2, MidpointRounding.AwayFromZero);

                                Vce2Cell1Min_SP = cusRegSP.BPtCusFbVce2Cell;
                                Vce2Cell1Min_SP = Math.Round(Convert.ToDecimal(Vce2Cell1Min_SP), 2, MidpointRounding.AwayFromZero);

                                Vce2Fbb1Min_SP = cusRegSP.BPtCusFbVce2Fb;
                                Vce2Fbb1Min_SP = Math.Round(Convert.ToDecimal(Vce2Fbb1Min_SP), 2, MidpointRounding.AwayFromZero);
                            }

                            foreach (string services in DataServiceDest)
                            {
                                dataServType++;

                                workSheet.Cell(count, 1).Value = "Service Type :" + services + " " + "Id No:" + imsiID;

                                //Data Call Log
                                lstInvoiceB = db.BSt2B.AsEnumerable()
                                                .Where(t => t.Bt2CusFbTranDServType != null
                                                        && t.Bt2CusFbTranDServType.ToLower().Equals(services.ToLower())
                                                        && (t.Bt2CusFbIMSIId.Equals(imsiID))).ToList();

                                if (lstInvoiceB != null)
                                {
                                    foreach (BSt2B objB in lstInvoiceB)
                                    {
                                        count++;
                                        cond++;

                                        // Date or Time
                                        if (objB.Bt2CusFbTranDStartDate != null)
                                            workSheet.Cell(count, 1).Value = Convert.ToDateTime(objB.Bt2CusFbTranDStartDate).ToShortDateString() + " / " + objB.Bt2CusFbTranDStartTime;
                                        else
                                            workSheet.Cell(count, 1).Value = objB.Bt2CusFbTranDStartDate + " / " + objB.Bt2CusFbTranDStartTime;
                                        // Les
                                        //workSheet.Cell(count, 2).Value = objB.Bt2CusFbTranDStartDate;
                                        // Origin or Destination
                                        workSheet.Cell(count, 3).Style.NumberFormat.SetNumberFormatId((int)XLPredefinedFormat.Number.Integer);
                                        workSheet.Cell(count, 3).Value = objB.Bt2CusFbTranDDest;
                                        // Usage
                                        decimal usage = Math.Round(Convert.ToDecimal(objB.Bt2CusFbBilIncre), 2, MidpointRounding.AwayFromZero);
                                        workSheet.Cell(count, 4).Value = usage;
                                        // Unit Type
                                        workSheet.Cell(count, 5).Value = objB.Bt2CusFbBillUnitType;
                                        // Rate
                                        workSheet.Cell(count, 6).Value = dateRate1MB_SP;
                                        // Total charge in USD
                                        workSheet.Cell(count, 7).Value = Math.Round(Convert.ToDecimal((usage * dateRate1MB_SP)), 2, MidpointRounding.AwayFromZero);

                                        TotalChargeUSD += Math.Round(Convert.ToDecimal((usage * dateRate1MB_SP)), 2, MidpointRounding.AwayFromZero);
                                    }
                                }
                            }

                            foreach (string services in VceServiceDest)
                            {
                                workSheet.Cell(++count, 1).Value = "Service Type :" + services + " " + "Id No:" + imsiID;

                                // Voice Call log
                                lstInvoiceB = db.BSt2B.AsEnumerable()
                                             .Where(t => t.Bt2CusFbTranVServType != null && t.Bt2CusFbTranVServType.ToLower().Equals(services.ToLower())
                                                    && (t.Bt2CusFbIMSIId.Equals(imsiID))).ToList();
                                if (lstInvoiceB != null)
                                {
                                    foreach (BSt2B objB in lstInvoiceB)
                                    {
                                        count++;
                                        cond++;

                                        // Date or Time
                                        if (objB.Bt2CusFbTranVStartDate != null)
                                            workSheet.Cell(count, 1).Value = Convert.ToDateTime(objB.Bt2CusFbTranVStartDate).ToShortDateString() + " / " + objB.Bt2CusFbTranVStartTime;
                                        else
                                            workSheet.Cell(count, 1).Value = objB.Bt2CusFbTranVStartDate + " / " + objB.Bt2CusFbTranVStartTime;
                                        // Les
                                        //workSheet.Cell(count, 2).Value = objB.Bt2CusFbTranDStartDate;
                                        // Origin or Destination
                                        workSheet.Cell(count, 3).Value = objB.Bt2CusFbTranVDest;
                                        // Usage
                                        decimal VceUsage = Math.Round(Convert.ToDecimal(objB.Bt2CusFbVcebillincre), 2, MidpointRounding.AwayFromZero);
                                        workSheet.Cell(count, 4).Value = VceUsage;
                                        // Unit Type
                                        workSheet.Cell(count, 5).Value = objB.Bt2CusFbVcebillingUnittype;
                                        // Rate
                                        if (services.ToLower().Contains("fixed"))
                                        {
                                            workSheet.Cell(count, 6).Value = Vce2Fixed1MIN_SP;
                                            // Total charge in USD
                                            workSheet.Cell(count, 7).Value = Math.Round(Convert.ToDecimal((VceUsage * Vce2Fixed1MIN_SP)), 2, MidpointRounding.AwayFromZero);

                                            TotalChargeUSD += Math.Round(Convert.ToDecimal((VceUsage * Vce2Fixed1MIN_SP)), 2, MidpointRounding.AwayFromZero);
                                        }
                                        else if (services.ToLower().Contains("cell"))
                                        {
                                            workSheet.Cell(count, 6).Value = Vce2Cell1Min_SP;
                                            // Total charge in USD
                                            workSheet.Cell(count, 7).Value = Math.Round(Convert.ToDecimal((VceUsage * Vce2Cell1Min_SP)), 2, MidpointRounding.AwayFromZero);

                                            TotalChargeUSD += Math.Round(Convert.ToDecimal((VceUsage * Vce2Cell1Min_SP)), 2, MidpointRounding.AwayFromZero);
                                        }
                                        else if (services.ToLower().Contains("fbb") || services.ToLower().Contains("boardband") || services.ToLower().Contains("fleetbroadband"))
                                        {
                                            workSheet.Cell(count, 6).Value = Vce2Fbb1Min_SP;
                                            // Total charge in USD
                                            workSheet.Cell(count, 7).Value = Math.Round(Convert.ToDecimal((VceUsage * Vce2Fbb1Min_SP)), 2, MidpointRounding.AwayFromZero);

                                            TotalChargeUSD += Math.Round(Convert.ToDecimal((VceUsage * Vce2Fbb1Min_SP)), 2, MidpointRounding.AwayFromZero);
                                        }
                                    }
                                }
                            }

                            workSheet.Cell(count + 2, 6).Value = "Total(USD)";
                            workSheet.Cell(count + 2, 7).Value = TotalChargeUSD;

                            xLWorkbook.SaveAs(SpreadsheetStream);
                            SpreadsheetStream.Position = 0;

                            var data = new Dictionary<string, string>();

                            data.Add("FileGuid", Convert.ToString(SpreadsheetStream));

                            data.Add("FileName", CallLogName);

                            TempData["streamData"] = SpreadsheetStream;

                            return Json(data, JsonRequestBehavior.AllowGet);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }

            return View("./Index");
        }

        [HttpGet]
        public virtual ActionResult Dwnload(string fileGuid, string fileName)
        {
            if (TempData["DictData"] != null)
            {
                List<Dictionary<string, Stream>> dictData = (List<Dictionary<string, Stream>>)TempData["DictData"];

                using (ZipFile zip = new ZipFile())
                {
                    int i = 1;
                    foreach (Dictionary<string, Stream> datas in dictData)
                    {
                        zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                        //zip.AddDirectoryByName("Files"+i++);
                        //string directoryB = "Root/B";

                        if (datas.Count > 0)
                        {
                            try
                            {
                                string dictKey = datas.FirstOrDefault(t => t.Key != null).Key.ToString();
                                Stream streamContent = (Stream)datas[dictKey];
                                var fileInfo = BillingHelpers.GetFileInfo(dictKey);

                                zip.AddEntry($"{fileInfo.Item2}/" + dictKey + ".xlsx", streamContent);
                                // zip.AddEntry(dictKey, (Stream)datas[dictKey]);
                            }
                            catch (Exception error) {
                                string folderPath = System.Configuration.ConfigurationManager.AppSettings["ErrorFolder"];
                                folderPath = Server.MapPath(folderPath);

                                logger.LogExceptions(error, folderPath);
                            }
                        }
                    }
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        zip.Save(memoryStream);
                        return File(memoryStream.ToArray(), "application/zip", "Invoices.zip");
                    }
                }
            }
            System.IO.Stream data = (System.IO.Stream)TempData["streamData"];
            return File(data, "application/vnd.ms-excel", fileName);
        }
    }
}