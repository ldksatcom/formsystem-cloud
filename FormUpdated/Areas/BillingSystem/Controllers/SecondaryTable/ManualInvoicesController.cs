﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FormUpdated.Areas.BillingSystem.Controllers.SecondaryTable
{
    [Authorize]
    public class ManualInvoicesController : Controller
    {
        // GET: BillingSystem/ManualInvoices
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ManualInvoice()
        {
            ViewBag.InvoiveNo = BillingHelpers.GenInvoice(true, 0);
            ViewBag.InvoiceDate = DateTime.Now.Date.ToString("dd/MM/yyyy");
            BillingSystem.Models.BillingSystemEntities db = new Models.BillingSystemEntities();
            ViewBag.Customers = new SelectList(db.BsMtCustomers, "CusID", "Customers");
            ViewBag.Dealers = new SelectList(db.BPtDealers, "Dealer_ID", "BPtDealName");
            ViewBag.Vesseels = new SelectList(db.BPtShips, "Ship_ID", "BPtShipName");

            return View();
        }

        [HttpPost]
        public ActionResult GenInvoiceManualIOP(VM_ManualInvoice list)
        {
            try
            {
                // getting Invoice comapny logo path if empty then
                if (string.IsNullOrEmpty(MySession.InvoiceLogoPath))
                {
                    try
                    {
                        string mapPath = Server.MapPath("../Images/smts logo.jpg");
                        if (mapPath.Contains("FormUpdated"))
                        {
                            string[] stringSeparators = new string[] { "FormUpdated" };
                            string[] index = mapPath.Split(stringSeparators, StringSplitOptions.None);
                            index[0] = index[0] + @"FormUpdated\Areas";
                            mapPath = index[0] + index[1];
                            MySession.InvoiceLogoPath = mapPath;
                        }
                    }
                    catch { }
                }

                //if (!string.IsNullOrEmpty(list))
                if (list != null)
                {
                    var jsonObj = list;//System.Web.Helpers.Json.Decode(list);

                    //if (jsonObj != null)
                    //{

                    //    Dictionary<string, dynamic> dictRecords = new Dictionary<string, dynamic>();

                    //    dictRecords.Add("invno", jsonObj.InvoiceNo);
                    //    dictRecords.Add("invdate", jsonObj.InvoiceDate);
                    //    dictRecords.Add("ownername", jsonObj.OwnerName);
                    //    dictRecords.Add("ponumer", jsonObj.PONo);
                    //    dictRecords.Add("donumber", jsonObj.DONo);

                    //    if (jsonObj.CstomerName != null)
                    //    {
                    //        int customerID = Convert.ToInt32(jsonObj.CstomerName);
                    //        using (Areas.BillingSystem.Models.BillingSystemEntities db = new Models.BillingSystemEntities())
                    //        {
                    //            string customerName = db.BsMtCustomers.FirstOrDefault(t => t.CusID == customerID).Customers;
                    //            dictRecords.Add("customername", customerName);
                    //            string cusAddress = db.BsPtCusRegs_.FirstOrDefault(t => t.fkCusID == customerID).BsPtCusAddr;
                    //            dictRecords.Add("customeraddress", cusAddress);
                    //        }

                    //    }

                    //    dictRecords.Add("vessel", jsonObj.Vessel);
                    //    dictRecords.Add("accode", jsonObj.AcCode);

                    //    Dictionary<string, dynamic> dictServiceDescription = new Dictionary<string, dynamic>();
                    //    int count = 0;
                    //    foreach (var sd in jsonObj.ServiceDest)
                    //    {
                    //        dictServiceDescription.Add("SN", count++);
                    //        dictServiceDescription.Add("description", sd.description);
                    //        dictServiceDescription.Add("qty", sd.qty);
                    //        dictServiceDescription.Add("unitprice", sd.unitprice);
                    //    }
                    //}


                    var data = new Dictionary<string, string>();
                    helper_NSSLGlobal billingHelpers = new helper_NSSLGlobal();
                    List<Dictionary<string, System.IO.Stream>> dictdata = new List<Dictionary<string, Stream>>();

                    data.Add("FileGuid", Convert.ToString(""));
                    data.Add("FileName", "");

                    //Invoice 
                    var spreadsheetstream = billingHelpers.GnerateManualInvoice(list);

                    if (spreadsheetstream.Item1 != null && !string.IsNullOrEmpty(spreadsheetstream.Item2))
                    {
                        Dictionary<string, Stream> valuePairs = new Dictionary<string, Stream>();
                        valuePairs.Add(spreadsheetstream.Item2, (System.IO.Stream)spreadsheetstream.Item1);
                        dictdata.Add(valuePairs);
                    }

                    if (dictdata.Count > 0)
                    {
                        TempData["DictData"] = (List<Dictionary<string, Stream>>)dictdata;
                        return Json(data, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(data, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception error)
            {
                return Json(error.ToString(), JsonRequestBehavior.AllowGet);
            }
            return Json("Message", JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult GetVessels(string list)
        //{
        //    BillingSystem.Models.BillingSystemEntities db = new Models.BillingSystemEntities();
        //    List<vessels> lstVessel = db.BPtShips.Where(t.), "Ship_ID", "BPtShipName");
        //    return Json(Message, JsonRequestBehavior.AllowGet);
        //}

       public class vessels
        {
            public int VesselID { get; set; }
            public string VesselName { get; set; }
        }

    }

    public class VM_ManualInvoice
    {
        public string InvoiceNo { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string OwnerName { get; set; }
        public string SNJNo { get; set; }
        public int CstomerName { get; set; }
        public string DONo { get; set; }
        public string Dealers { get; set; }
        public string PONo { get; set; }
        public int Vessel { get; set; }
        public string AcCode { get; set; }
        public List<VM_ServiceDescription> ServiceDest { get; set; }

    }

    public class VM_ServiceDescription
    {
        public string servicecode { get; set; }
        public string description { get; set; }
        public int? qty { get; set; }
        public decimal? unitprice { get; set; }
    }
}