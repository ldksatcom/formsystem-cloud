﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers
{
    [Authorize]
    [Route("Area/BillingSystem")]
    public class RejectionsController : Controller
    {
        private ILogger logger;
        private BillingSystemEntities db = new BillingSystemEntities();

        public RejectionsController()
        {
            logger = log.GetInstance;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            string folderPath = System.Configuration.ConfigurationManager.AppSettings["ErrorFolder"];
            folderPath = Server.MapPath(folderPath);

            logger.LogExceptions(filterContext.Exception, folderPath);

            filterContext.ExceptionHandled = true;
            this.View("Error").ExecuteResult(this.ControllerContext);
        }

        public ActionResult CusRegUpload()
        {
            return View();
        }

        public ActionResult LesRegUpload()
        {
            return View();
        }

        #region Upload cus Reg
        [HttpPost]
        public ActionResult UploadCusReg()
        {
            string Message = string.Empty;

            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        //fname = Path.Combine(Server.MapPath("~/Uploads/"), fname);
                        string tempPath = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + fname;
                        file.SaveAs(tempPath);

                        Helper.Dummy_Helper helper = new Helper.Dummy_Helper();
                        Message = helper.CusRegUpload(tempPath);
                        //List<int> lstID = helper.EvoSatCDR2CT(tempPath, lesid); //new List<int> { 58 };

                        //if (lstID.Count > 0)
                        //  Message = helper.RejectionCondition_EvoSat(lstID);
                        //else
                        //  return Json("NSSL CDR Getting Error, it not return Master Primary ID.", JsonRequestBehavior.AllowGet);
                    }
                    // Returns message that successfully uploaded  
                    return Json(Message, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("No files selected.", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Upload Les Reg
        [HttpPost]
        public ActionResult UploadLesReg()
        {
            string Message = string.Empty;

            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        //fname = Path.Combine(Server.MapPath("~/Uploads/"), fname);
                        string tempPath = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + fname;
                        file.SaveAs(tempPath);

                        Helper.Dummy_Helper helper = new Helper.Dummy_Helper();
                        Message = helper.LesRegUpload(tempPath);
                        //List<int> lstID = helper.EvoSatCDR2CT(tempPath, lesid); //new List<int> { 58 };

                        //if (lstID.Count > 0)
                        //  Message = helper.RejectionCondition_EvoSat(lstID);
                        //else
                        //  return Json("NSSL CDR Getting Error, it not return Master Primary ID.", JsonRequestBehavior.AllowGet);
                    }
                    // Returns message that successfully uploaded  
                    return Json(Message, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("No files selected.", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        // GET: BillingSystem/Rejections
        public ActionResult Index()
        {
            return View(db.Bst1A.ToList());
        }

        public ActionResult Rejection()
        {
            return View(db.BS1ALessT.ToList());
        }

        [HttpPost]
        public ActionResult MovetoInvoice(string[] lists)
        {
            List<Tuple<bool, string>> Msg = new List<Tuple<bool, string>>();
            if (lists.Length > 0)
            {
                foreach (string id in lists)
                {
                    helper_NSSLGlobal billingHelpers = new helper_NSSLGlobal();

                    int value = 0;
                    if (int.TryParse(id, out value))
                    {
                        Msg = billingHelpers.Move2Invoice(value);
                    }
                }
            }

            string returnmsg = string.Empty;
            foreach (var tupl in Msg)
            {
                if (tupl.Item1)
                    returnmsg += string.Format("{0} Succefully moved to Invoice", tupl.Item2);
                else
                    returnmsg += string.Format("{0} Not Succefully move to Invoice", tupl.Item2);
            }

            return Json(returnmsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ViewRejectionLog(string[] lists)
        {
            System.Text.StringBuilder Message = new System.Text.StringBuilder();
            if (lists.Length > 0)
            {
                foreach (string id in lists)
                {
                    int value = 0;
                    if (int.TryParse(id, out value))
                    {
                        try
                        {
                            var msgs = db.BS1LesRejLog.FirstOrDefault(t => t.fkBs1LesReg == value).Bs1lesRejection;
                            Message.Append(msgs).AppendLine();
                        }
                        catch { }
                    }
                }
            }
            return Json(Message.ToString(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Index(string[] lists)
        {
            if (lists.Length > 0)
            {
                foreach (string id in lists)
                {
                    BillingHelpers billingHelpers = new BillingHelpers();

                    int value = 0;
                    if (int.TryParse(id, out value))
                    {
                        billingHelpers.MoveRejection_Invoice(value);
                    }
                }
            }
            return RedirectToAction("Index");
        }

        public List<UploadFiles> getCdrfiles()
        {
            List<UploadFiles> lstfiles = new List<UploadFiles>();

            string folderPath = Server.MapPath("../CDR Files/");
            int i = 1;

            foreach (string file in System.IO.Directory.EnumerateFiles(folderPath, "*.csv"))
            {
                UploadFiles up = new UploadFiles();
                up.id = i++;
                up.location = file;
                up.Filename = Path.GetFileName(file);
                lstfiles.Add(up);
            }

            foreach (string file in System.IO.Directory.EnumerateFiles(folderPath, "*.xlsx"))
            {
                UploadFiles up = new UploadFiles();
                up.id = i++;
                up.location = file;
                up.Filename = Path.GetFileName(file);
                lstfiles.Add(up);
            }

            return lstfiles;
        }

        [Route("Area/BillingSystem")]
        public ActionResult otesatCdrtoCt()
        {
            List<UploadFiles> lstfiles = getCdrfiles();
            Models.BillingSystemEntities db = new Models.BillingSystemEntities();
            ViewBag.LESRef = new SelectList(db.BsMtLESRefs, "LesRefID", "Description");

            ViewBag.Msg = null;
            return View(lstfiles);
        }

        [Route("Area/BillingSystem")]
        public ActionResult otesatCdr2Ct()
        {
            string folderPath = Server.MapPath("../CDR Files/");
            foreach (string file in System.IO.Directory.EnumerateFiles(folderPath, "*.csv"))
            {
                ViewBag.File = file;
            }

            Models.BillingSystemEntities db = new Models.BillingSystemEntities();
            ViewBag.LESRef = new SelectList(db.BsMtLESRefs, "LesRefID", "Description");

            ViewBag.Msg = null;
            return View();
        }

        [HttpPost]
        public ActionResult GenerateInvoice(string[] lists)
        {
            Models.BillingSystemEntities db = new Models.BillingSystemEntities();
            ViewBag.LESRef = new SelectList(db.BsMtLESRefs, "LesRefID", "Description");
            string mes = string.Empty;
            string fileName = string.Empty;
            int lesref = 0;

            if (lists.Length > 1)
            {
                fileName = lists[0];

                bool result = int.TryParse(lists[1], out lesref);
                if (result)
                    lesref = lesref;
                else
                    return Json("Please choose appropriate LES Ref", JsonRequestBehavior.AllowGet);
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                helper_NI helper = new helper_NI();
                helper_OteSat otesat = new helper_OteSat();

                var spreadsheet = helper.CDRtoCT(fileName, lesref);

                string LESName = spreadsheet.Item4;
                string destPath = System.IO.Path.Combine(Server.MapPath("../CT Files/"), spreadsheet.Item2);
                using (var fileStream = new FileStream(destPath, FileMode.Create, FileAccess.Write))
                {
                    spreadsheet.Item1.CopyTo(fileStream);
                    System.Text.StringBuilder subject = new System.Text.StringBuilder();
                    subject.AppendLine("Dear Sir,");
                    subject.AppendLine("Good Day!");
                    subject.AppendFormat("The Billing System {0} CDR Successfully stored in Common Table and it CT data is attached excel file, please find,", LESName);
                    subject.AppendFormat("CDR Upload Status: {0} ", spreadsheet.Item3);

                    string folderPath = Server.MapPath("../CDR Files/");
                    string CDRFile = fileName;
                    //foreach (string file in System.IO.Directory.EnumerateFiles(folderPath, "*.csv"))
                    //{
                    //    CDRFile = file;
                    //}

                    //string Toemail = "krishna@smtspl.com";
                    //string cc = "lawrence.tan@smtspl.com;marx@smtspl.com;karthi@smtspl.com;senthil@smtspl.com";

                    string Toemail = ConfigurationManager.AppSettings["ToEmail"].ToString();
                    string cc = ConfigurationManager.AppSettings["CCMail"].ToString();
                    mes = helper_OteSat.SendEmail(Toemail, destPath, "Billing System: "+ LESName + " CDR to Store Common Table Status Info", subject.ToString(), cc, CDRFile);
                    mes = "Sucessfully stored and " + mes;
                }
            }

            return Json(mes, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult toCT(string txtfilesName)
        {

            Models.BillingSystemEntities db = new Models.BillingSystemEntities();
            ViewBag.LESRef = new SelectList(db.BsMtLESRefs, "LesRefID", "Description");

            helper_OteSat oteSat = new helper_OteSat();
            Tuple<string, List<OteSatCtVM>> tupleslst = new Tuple<string, List<OteSatCtVM>>(string.Empty, new List<OteSatCtVM>());

            if (Path.GetExtension(txtfilesName) == ".csv")
            {
                List<OteSatCtVM> lstVM = helper_OteSat.CsvToDictionary(txtfilesName);

                tupleslst = oteSat.OteSat2CT_model(lstVM, 1);

            }
            else if (Path.GetExtension(txtfilesName) == ".xlsx")
            {

            }

            //Tuple<List<int>, string> tuple = oteSat.OteSat2CT(txtfilesName, 1);
            var spreadsheet = oteSat.GnerateManualInvoice(tupleslst.Item2);

            string destPath = System.IO.Path.Combine(Server.MapPath("../CT Files/"), spreadsheet.Item2);
            using (var fileStream = new FileStream(destPath, FileMode.Create, FileAccess.Write))
            {
                spreadsheet.Item1.CopyTo(fileStream);
                System.Text.StringBuilder subject = new System.Text.StringBuilder();
                subject.AppendLine("Dear Sir,");
                subject.AppendLine("Good Day!");
                subject.AppendLine("The Billing System OteSat CDR Successfully stored in Common Table and it CT data is attached excel file, please find,");
                subject.AppendFormat("CDR Upload Status: {0} ", tupleslst.Item1);

                string folderPath = Server.MapPath("../CDR Files/");
                string CDRFile = string.Empty;
                foreach (string file in System.IO.Directory.EnumerateFiles(folderPath, "*.csv"))
                {
                    CDRFile = file;
                }

                //string Toemail = "krishna@smtspl.com";
                //string cc = "lawrence.tan@smtspl.com;marx@smtspl.com;karthi@smtspl.com;senthil@smtspl.com";

                string Toemail = ConfigurationManager.AppSettings["ToEmail"].ToString();
                string cc = ConfigurationManager.AppSettings["CCMail"].ToString();
                string mes = helper_OteSat.SendEmail(Toemail, destPath, "Billing System: OteSat CDR to Store Common Table Status Info", subject.ToString(), cc, CDRFile);
                ViewBag.Msg = "Sucessfully stored and " + mes;
            }
            return View("otesatCdr2Ct");
        }

        [HttpPost]
        public ActionResult Delete(int? id)
        {
            return View("otesatCdrSave");
        }

        [HttpPost]
        public ActionResult RemoveCDR()
        {
            string fname = System.IO.Path.Combine(Server.MapPath("../CDR Files/"));
            foreach (string file in System.IO.Directory.EnumerateFiles(fname, "*.xlsx"))
            {
                System.IO.File.Delete(file);
                ViewBag.Msg = "The file was deleted successfully.";
            }

            foreach (string file in System.IO.Directory.EnumerateFiles(fname, "*.csv"))
            {
                System.IO.File.Delete(file);
                ViewBag.Msg = "The file was deleted successfully.";
            }
            return View("otesatCdrSave");
        }


        [HttpPost]
        public ActionResult RemoveCDRFiles(string[] lists)
        {
            string msg = string.Empty;
            if (lists.Length > 0)
            {
                string filename = string.Empty;
                foreach (string file in lists)
                {
                    filename = System.IO.Path.GetFileNameWithoutExtension(file);
                    System.IO.File.Delete(file);
                    msg += string.Format("The file {0} was deleted successfully.", filename);
                }
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RemoveCT()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AdoConnstr"].ToString()))
            {
                try
                {
                    con.Open();
                    //SqlCommand cmd = new SqlCommand("delete from BCtBs; delete from BCtAs;", con);
                    SqlCommand cmd = new SqlCommand("delete from BCtFbbTrans", con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                    ViewBag.Msg = "Common table records removed successfully";

                    string folderPath = Server.MapPath("../CDR Files/");
                    foreach (string file in System.IO.Directory.EnumerateFiles(folderPath, "*.csv"))
                    {
                        ViewBag.File = file;
                    }
                }
                catch
                {
                    ViewBag.Msg = "Not clear Common Table, please try again";
                }
            }

            Models.BillingSystemEntities db = new Models.BillingSystemEntities();
            ViewBag.LESRef = new SelectList(db.BsMtLESRefs, "LesRefID", "Description");

            //using (BillingSystemEntities db = new BillingSystemEntities()) {
            //try
            //{
            //    var cta = db.BCtAs.ToList();
            //    var ctb = db.BCtBs.ToList();

            //    foreach (var b in ctb)
            //    {
            //        db.BCtBs.Remove(b);
            //    }

            //    foreach (var a in cta)
            //    {
            //        db.BCtAs.Remove(a);
            //    }

            //    db.SaveChanges();

            //    ViewBag.Msg = "Common table records removed successfully";

            //    string folderPath = Server.MapPath("../CDR Files/");
            //    foreach (string file in System.IO.Directory.EnumerateFiles(folderPath, "*.xlsx"))
            //    {
            //        ViewBag.File = file;
            //    }
            //}
            //catch
            //{
            //    ViewBag.Msg = "Not clear Common Table, please try again";
            //}
            //}
            return View("otesatCdr2Ct");
        }

        [HttpPost]
        public ActionResult RemoveCTRecords()
        {
            string mes = string.Empty;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AdoConnstr"].ToString()))
            {
                try
                {
                    con.Open();
                    //SqlCommand cmd = new SqlCommand("delete from BCtBs; delete from BCtAs;", con);
                    SqlCommand cmd = new SqlCommand("delete from BCtFbbTrans", con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                    mes = "Common table records removed successfully";
                }
                catch
                {
                    mes = "Not clear Common Table, please try again";
                }
            }

            Models.BillingSystemEntities db = new Models.BillingSystemEntities();
            ViewBag.LESRef = new SelectList(db.BsMtLESRefs, "LesRefID", "Description");

            return Json(mes, JsonRequestBehavior.AllowGet);
        }

        public ActionResult otesatCdrSave()
        {
            ViewBag.Msg = null;
            return View();
        }


        [HttpPost]
        [Route("Area/BillingSystem")]
        public ActionResult Upload_otesatCdrSave()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        fname = System.IO.Path.Combine(Server.MapPath("../CDR Files/"), fname);
                        file.SaveAs(fname);
                    }
                    // Returns message that successfully uploaded  
                    return Json("Successfully stored.", JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("No files selected.", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult otesatCdrDownload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult otesatCTDownload()
        {
            return View();
        }
    }
}