﻿using FormUpdated.Areas.BillingSystem.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace FormUpdated.Areas.BillingSystem.Controllers
{
    [Authorize]
    public class CDRController : Controller
    {
        public ActionResult SendEmail()
        {
            BillingSystem.Models.BillingSystemEntities db = new Models.BillingSystemEntities();
            int spid = db.BsMtServiceProviders.FirstOrDefault(t => t.ServiceProvider.ToLower().Equals("NI")).spID;
            ViewBag.fkLesRef = new SelectList(db.BsMtLESRefs.Where(t => t.fkSPID == spid), "LesRefID", "Description");

            //SmtpClient client = new SmtpClient();
            //If you need to authenticate
            //client.Credentials = new NetworkCredential("karthi@smtspl.com", "smtskarthi@2018");

            MailMessage mailMessage = new MailMessage();
            //mailMessage.From = new MailAddress("karthi@smtspl.com"); 
            mailMessage.To.Add(new MailAddress("abi@smtspl.com"));
            mailMessage.Subject = "Hello There";
            mailMessage.Body = "Hello my friend!";

            try
            {
                using (var client = new SmtpClient())
                {
                    client.Send(mailMessage);
                }
            }
            catch (Exception error)
            {

            }

            return View("NIToCDR");
        }

        private ILogger logger;

        public CDRController() { logger = log.GetInstance; }

        protected override void OnException(ExceptionContext filterContext)
        {
            string folderPath = "~/Error_Log"; //System.Configuration.ConfigurationManager.AppSettings["ErrorFolder"];
            folderPath = Server.MapPath(folderPath);

            if (folderPath.Contains("FormUpdated"))
            {
                string[] stringSeparators = new string[] { "FormUpdated" };
                string[] index = folderPath.Split(stringSeparators, StringSplitOptions.None);
                index[0] = index[0] + @"FormUpdated\Areas\BillingSystem";
                folderPath = index[0] + index[1];
            }

            logger.LogExceptions(filterContext.Exception, folderPath);
            filterContext.ExceptionHandled = true;
            this.View("Error").ExecuteResult(this.ControllerContext);
        }

        public ActionResult Error()
        {
            return View();
        }

        // GET: BillingSystem/CDR
        public ActionResult Index()
        {

            return View("LDKSATCOM");
        }

        public ActionResult OteSatCDR()
        {
            return View();
        }

        public ActionResult ImarSatCDR()
        {
            return View();
        }

        public ActionResult NSSLCDR()
        {
            BillingSystem.Models.BillingSystemEntities db = new Models.BillingSystemEntities();
            int spid = db.BsMtServiceProviders.FirstOrDefault(t => t.ServiceProvider.ToLower().Equals("NSSL GLOBAL")).spID;
            ViewBag.fkLesRef = new SelectList(db.BsMtLESRefs.Where(t => t.fkSPID == spid), "LesRefID", "Description");

            return View();
        }

        public ActionResult NewOteSat2CDR()
        {
            BillingSystem.Models.BillingSystemEntities db = new Models.BillingSystemEntities();
            int spid = db.BsMtServiceProviders.FirstOrDefault(t => t.ServiceProvider.ToLower().Equals("otesat")).spID;
            ViewBag.fkLesRef = new SelectList(db.BsMtLESRefs.Where(t => t.fkSPID == spid), "LesRefID", "Description");
            //ViewBag.fkSatType = new SelectList(db.BsMtSatTypes, "MsSatType", "SatTypes");
            //ViewBag.fkEqupType = new SelectList(db.BsMtEquipTypes, "EquipTypeID", "Equipments");
            return View();
        }

        public ActionResult NewOteSatToCDR()
        {
            ViewModel.OteSatFileUploadVM uploadVM = new ViewModel.OteSatFileUploadVM();

            BillingSystem.Models.BillingSystemEntities db = new Models.BillingSystemEntities();
            int spid = db.BsMtServiceProviders.FirstOrDefault(t => t.ServiceProvider.ToLower().Equals("otesat")).spID;
            ViewBag.LESRef = new SelectList(db.BsMtLESRefs, "LesRefID", "Description");
            ViewBag.Satellite = new SelectList(db.BsMtSatTypes, "MsSatType", "SatTypes");
            ViewBag.Equipment = new SelectList(db.BsMtEquipTypes, "EquipTypeID", "Equipments");

            return View(uploadVM);
        }

        public ActionResult NIToCDR()
        {
            BillingSystem.Models.BillingSystemEntities db = new Models.BillingSystemEntities();
            int spid = db.BsMtServiceProviders.FirstOrDefault(t => t.ServiceProvider.ToLower().Equals("NI")).spID;
            ViewBag.fkLesRef = new SelectList(db.BsMtLESRefs.Where(t => t.fkSPID == spid), "LesRefID", "Description");

            return View();
        }

        public ActionResult EvoSatCDRToCT()
        {
            BillingSystem.Models.BillingSystemEntities db = new Models.BillingSystemEntities();
            int spid = db.BsMtServiceProviders.FirstOrDefault(t => t.ServiceProvider.ToLower().Equals("EvoSat")).spID;
            ViewBag.fkLesRef = new SelectList(db.BsMtLESRefs.Where(t => t.fkSPID == spid), "LesRefID", "Description");

            return View();
        }

        #region OteSat UploadFiles
        [HttpPost]
        public ActionResult UploadFiles()
        {
            string Message = string.Empty;
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        //fname = Path.Combine(Server.MapPath("~/Uploads/"), fname);
                        string tempPath = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + fname;
                        file.SaveAs(tempPath);

                        BillingHelpers helper = new BillingHelpers();
                        var duration = helper.OteSattoCT(tempPath);

                        Message = helper.RejectCondition(duration);
                    }
                    // Returns message that successfully uploaded  
                    return Json(Message, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("No files selected.", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult OteSatCDRDBView()
        {
            Models.BillingSystemEntities billingSystemEntities = new Models.BillingSystemEntities();

            List<Models.BCtB> bCtB = billingSystemEntities.BCtBs.ToList();

            return View(bCtB);
        }
        #endregion

        #region ImarSar UploadFiles
        [HttpPost]
        public ActionResult ImarSat_UploadFiles()
        {
            string Message = string.Empty;
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        //fname = Path.Combine(Server.MapPath("~/Uploads/"), fname);
                        string tempPath = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + fname;
                        file.SaveAs(tempPath);

                        BillingHelpers helper = new BillingHelpers();
                        //var duration = helper.OteSattoCT(tempPath);

                        //Message = helper.RejectCondition(duration);
                    }
                    // Returns message that successfully uploaded  
                    return Json(Message, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("No files selected.", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region NSSL Global UploadFiles
        [HttpPost]
        public ActionResult NSSL_UploadFiles(string LES_Ref)
        {
            string Message = string.Empty;
            // Checking no of files injected in Request object  

            int lesid = 0;
            using (Models.BillingSystemEntities db = new Models.BillingSystemEntities())
            {
                ViewBag.LESRef = new SelectList(db.BsMtLESRefs, "LesRefID", "Description");
            }

            if (!string.IsNullOrEmpty(LES_Ref) && int.TryParse(LES_Ref, out lesid))
            {
                try
                {
                    lesid = Convert.ToInt32(LES_Ref);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json("The LES Ref is required filed, please choose the LES Ref types.", JsonRequestBehavior.AllowGet);

            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        //fname = Path.Combine(Server.MapPath("~/Uploads/"), fname);
                        string tempPath = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + fname;
                        file.SaveAs(tempPath);

                        helper_NSSLGlobal helper = new helper_NSSLGlobal();
                        Tuple<List<int>, string> lstID = helper.NSSLtoCT(tempPath, lesid);

                        if (lstID.Item1.Count > 0)
                            Message = helper.RejectionCondition(lstID.Item1, lstID.Item2);
                        else
                            return Json("NSSL CDR Getting Error, it not return Master Primary ID.", JsonRequestBehavior.AllowGet);
                    }
                    // Returns message that successfully uploaded  
                    return Json(Message, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("No files selected.", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region New OteSat UploadFiles

        [HttpPost]
        public ActionResult UploadExcel(string LesRefID, HttpPostedFileBase CDRExcelFile)
        {
            BillingSystem.Models.BillingSystemEntities db = new Models.BillingSystemEntities();
            int spid = db.BsMtServiceProviders.FirstOrDefault(t => t.ServiceProvider.ToLower().Equals("otesat")).spID;
            //ViewBag.LESRef = new SelectList(db.BsMtLESRefs.Where(t => t.fkSPID == spid), "LesRefID", "Description");
            return View("NewOteSatToCDR");
        }

        [HttpPost]
        public ActionResult NewOteSatUpload(string LES_Ref)
        {
            string Message = string.Empty;

            int lesid = 0;
            using (Models.BillingSystemEntities db = new Models.BillingSystemEntities())
            {
                ViewBag.LESRef = new SelectList(db.BsMtLESRefs, "LesRefID", "Description");
            }

            if (!string.IsNullOrEmpty(LES_Ref) && int.TryParse(LES_Ref, out lesid))
            {
                try
                {
                    lesid = Convert.ToInt32(LES_Ref);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json("The LES Ref is required filed, please choose the LES Ref types.", JsonRequestBehavior.AllowGet);

            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        //fname = Path.Combine(Server.MapPath("~/Uploads/"), fname);
                        string tempPath = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + fname;
                        file.SaveAs(tempPath);

                        //Dummy_Helper helper = new Dummy_Helper();
                        //Message = helper.NewOteSat2CT(tempPath, lesid);

                        helper_OteSat oteSat = new helper_OteSat();
                        Tuple<List<int>, string> tuple = oteSat.OteSat2CT(tempPath, lesid);

                         Message = oteSat.RejectionCondition_OteSat(tuple.Item1, tuple.Item2);
                    }
                    // Returns message that successfully uploaded  
                    return Json(Message, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("No files selected.", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region NI Upload Files
        [HttpPost]
        public ActionResult UploadNICDR(string LES_Ref)
        {
            string Message = string.Empty;

            int lesid = 0;
            using (Models.BillingSystemEntities db = new Models.BillingSystemEntities())
            {
                ViewBag.LESRef = new SelectList(db.BsMtLESRefs, "LesRefID", "Description");
            }

            if (!string.IsNullOrEmpty(LES_Ref) && int.TryParse(LES_Ref, out lesid))
            {
                try
                {
                    lesid = Convert.ToInt32(LES_Ref);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json("The LES Ref is required filed, please choose the LES Ref types.", JsonRequestBehavior.AllowGet);

            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        //fname = Path.Combine(Server.MapPath("~/Uploads/"), fname);
                        string tempPath = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + fname;
                        file.SaveAs(tempPath);

                        Dummy_Helper helper = new Dummy_Helper();
                        Message = helper.NICDRToCT(tempPath, lesid);

                        // Message = helper.RejectCondition(duration);
                    }
                    // Returns message that successfully uploaded  
                    return Json(Message, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("No files selected.", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region EvoSat Upload
              [HttpPost]
        public ActionResult UploadEvoSatCDR(string LES_Ref)
        {
            string Message = string.Empty;

            int lesid = 0;
            using (Models.BillingSystemEntities db = new Models.BillingSystemEntities())
            {
                ViewBag.LESRef = new SelectList(db.BsMtLESRefs, "LesRefID", "Description");
            }

            if (!string.IsNullOrEmpty(LES_Ref) && int.TryParse(LES_Ref, out lesid))
            {
                try
                {
                    lesid = Convert.ToInt32(LES_Ref);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json("The LES Ref is required filed, please choose the LES Ref types.", JsonRequestBehavior.AllowGet);

            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        //fname = Path.Combine(Server.MapPath("~/Uploads/"), fname);
                        string tempPath = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + fname;
                        file.SaveAs(tempPath);

                        Dummy_Helper helper = new Dummy_Helper();
                        List<int> lstID = helper.EvoSatCDR2CT(tempPath, lesid); //new List<int> { 58 };

                        if (lstID.Count > 0)
                            Message = helper.RejectionCondition_EvoSat(lstID);
                        else
                            return Json("NSSL CDR Getting Error, it not return Master Primary ID.", JsonRequestBehavior.AllowGet);
                    }
                    // Returns message that successfully uploaded  
                    return Json(Message, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("No files selected.", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}