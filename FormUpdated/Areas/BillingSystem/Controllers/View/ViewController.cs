﻿using FormUpdated.Areas.BillingSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using System.Net;
using FormUpdated.Areas.BillingSystem.ViewModel;

namespace FormUpdated.Areas.BillingSystem.Controllers
{
    [Authorize]
    public class ViewController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();
        // GET: BillingSystem/View
        public ActionResult Index()
        {
            var LesRegPric = db.viewLesRegPrices;
            return View(LesRegPric.ToList());
        }

        public ActionResult LESRegPrices()
        {
            return View(db.viewLesRegPrices.ToList());
        }

        public ActionResult EditLesRegPrice(int lesid, int cusid, int shipid, int simid)
        {
            if (lesid < 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Helper.Dummy_Helper dummy = new Helper.Dummy_Helper();
            ViewModel.LesRegPriceEditVM lesRegPrice = dummy.getLesRegVM(lesid, cusid, shipid, simid);
            return View(lesRegPrice);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditLesRegPrice([Bind(Include = "LesRegID, CusID, ShipID, SimID, Customers, Vessel, InMarsatID, IMSIID, DataPlan, Recurfee, InBundleRate, OutBundleRate, StartDate, EndDate, VoicetoCellular, VoicetoFixed, VoicetoFleetBroadBand, VoicetoVoicemail, VoicetoIridium, ISDN, SMS, VoicetoBegan, VoicetoGSPS, StandardIP, BarDate, BarDateLifted, SusDate, susDateLifted, LayupDate, LayupDateLifted ")]LesRegPriceEditVM lesRegPrice)
        {
            if (ModelState.IsValid)
            {
                //int lesregid = 0 , cusid = 0, shipid = 0, simid = 0;
                //Helper.Dummy_Helper helper = new Helper.Dummy_Helper();

                //ViewModel.LesRegPriceEditVM UpdateDB = new ViewModel.LesRegPriceEditVM();

                using (BillingSystemEntities db = new BillingSystemEntities())
                {
                    var LesReg = db.BsPtSPLesRegs.FirstOrDefault(t => t.SPLesRegID == lesRegPrice.LesRegID);
                    var CusName = db.BsMtCustomers.FirstOrDefault(t => t.CusID == lesRegPrice.CusID);
                    var Vessel = db.BPtShips.FirstOrDefault(t => t.Ship_ID == lesRegPrice.ShipID);
                    var Sim = db.BptFbSims.FirstOrDefault(t => t.Sim_ID == lesRegPrice.SimID);

                    if (LesReg != null)
                    {
                        var serviceTypes = db.BsPtSPServiceDests.Where(t => t.BPtServTfkLesRegID == LesReg.SPLesRegID).ToList();

                        decimal? V2C = lesRegPrice.VoicetoCellular;
                        decimal? V2F = lesRegPrice.VoicetoFixed;
                        decimal? V2FBB = lesRegPrice.VoicetoFleetBroadBand;
                        decimal? V2V = lesRegPrice.VoicetoVoicemail;
                        decimal? V2I = lesRegPrice.VoicetoIridium;
                        decimal? Isdn = lesRegPrice.ISDN;
                        decimal? Sms = lesRegPrice.SMS;
                        decimal? V2Begn = lesRegPrice.VoicetoBegan;
                        decimal? V2Gsps = lesRegPrice.VoicetoGSPS;
                        decimal? StdIP = lesRegPrice.StandardIP;

                        CusName.Customers = lesRegPrice.Customers;
                        Vessel.BPtShipName = lesRegPrice.Vessel;
                        Sim.BPtFBimMsisdn = lesRegPrice.InMarsatID;
                        Sim.BPtFBImImsi = lesRegPrice.IMSIID;
                        LesReg.BPtLesRgnFbRecurFee = lesRegPrice.RecurFee;
                        LesReg.BPtLesRgnFBDataPlanIn = Convert.ToString(lesRegPrice.DataPlan);
                        LesReg.BPtLesRgnFbInbundleDataRate = lesRegPrice.InBundleRate;
                        LesReg.BPtLesRgnFbOutBundleDataRate = lesRegPrice.OutBundleRate;
                        LesReg.BsPtlesRegStartdate = Convert.ToDateTime(lesRegPrice.StartDate);
                        LesReg.BsPtlesRegEnd = Convert.ToDateTime(lesRegPrice.EndDate);
                        LesReg.BsPtLesRegFBBarDate = lesRegPrice.BarDate;
                        LesReg.BsPtLesRegFbBarDateLifted = lesRegPrice.BarDateLifted;
                        LesReg.BsPtLesRegFBSusDate = lesRegPrice.SusDate;
                        LesReg.BsPtLesRegFBSusDateLifted = lesRegPrice.susDateLifted;
                        LesReg.BsPtLesRegFBLayUpDate = lesRegPrice.LayupDate;
                        LesReg.BsPtLesRegFBLayUpDateLifted = lesRegPrice.LayupDateLifted;

                        foreach (var item in serviceTypes)
                        {
                            switch (item.BsPtServiceTypes)
                            {
                                case "Voice  to Cellular":
                                    item.BsPtServiceCostPrice = V2C;
                                    break;
                                case "Voice  to Fixed":
                                    item.BsPtServiceCostPrice = V2F;
                                    break;
                                case "Voice  to FleetBroadband":
                                    item.BsPtServiceCostPrice = V2FBB;
                                    break;
                                case "Voice  to Voicemail":
                                    item.BsPtServiceCostPrice = V2V;
                                    break;
                                case "Voice  to Iridium":
                                    item.BsPtServiceCostPrice = V2I;
                                    break;
                                case "ISDN":
                                    item.BsPtServiceCostPrice = Isdn;
                                    break;
                                case "SMS":
                                    item.BsPtServiceCostPrice = Sms;
                                    break;
                                case "Voice to Began":
                                    item.BsPtServiceCostPrice = V2Begn;
                                    break;
                                case "Voice  to GSPS":
                                    item.BsPtServiceCostPrice = V2Gsps;
                                    break;
                                case "Standard IP":
                                    item.BsPtServiceCostPrice = StdIP;
                                    break;
                            }

                        }

                        db.SaveChanges();
                    }
                }
                return RedirectToAction("LESRegPrices");
            }
            return View(lesRegPrice);
        }

        public ActionResult CUSRegPrices()
        {
            return View(db.viewCusRegPrices.ToList());
        }

        public ActionResult EditCusRegPrice(int CusRegID, int CusID, int ShipID, int SimID)
        {
            if (CusRegID < 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Helper.Dummy_Helper helper = new Helper.Dummy_Helper();
            ViewModel.CusRegPriceEditVM cusRegPrice = helper.getCusRegVM(CusRegID, CusID, ShipID, SimID);
            return View(cusRegPrice);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCusRegPrice([Bind(Include = "CusRegID, CusID, ShipID, SimID, Customer, Vessel, InmarsatID, ImsiID, IccID, Dataplan, Recurfee, InbundleRate, OutbundleRate, StartDate, EndDate, VoicetoCellular, VoicetoFixed, VoicetoFleetBroadband, VoicetoVoicemail, VoicetoIridium, ISDN, SMS, VoicetoBegan, VoicetoGSPS, StandardIP, BarDate, BarDateLifted, SusDate, SusDateLifted, LayupDate, LayupDateLifted")] CusRegPriceEditVM cusRegPrice)
        {
            if (ModelState.IsValid)
            {
                using (BillingSystemEntities db = new BillingSystemEntities())
                {
                    var Cusreg = db.BsPtCusRegs_.FirstOrDefault(t => t.Cus_RegID == cusRegPrice.CusRegID);
                    var CusName = db.BsMtCustomers.FirstOrDefault(t => t.CusID == cusRegPrice.CusID);
                    var Vessel = db.BPtShips.FirstOrDefault(t => t.Ship_ID == cusRegPrice.ShipID);
                    var Sim = db.BptFbSims.FirstOrDefault(t => t.Sim_ID == cusRegPrice.SimID);

                    if (Cusreg != null)
                    {
                        var serviceType = db.BsPtCusServiceDests.Where(t => t.BPtCusServfkCusRegID == Cusreg.Cus_RegID).ToList();

                        decimal? V2C = cusRegPrice.VoicetoCellular;
                        decimal? V2F = cusRegPrice.VoicetoFixed;
                        decimal? V2FBB = cusRegPrice.VoicetoFleetBroadband;
                        decimal? V2V = cusRegPrice.VoicetoVoicemail;
                        decimal? V2I = cusRegPrice.VoicetoIridium;
                        decimal? Isdn = cusRegPrice.ISDN;
                        decimal? Sms = cusRegPrice.SMS;
                        decimal? V2Bgn = cusRegPrice.VoicetoBegan;
                        decimal? V2Gsps = cusRegPrice.VoicetoGSPS;
                        decimal? StdIP = cusRegPrice.StandardIP;

                        CusName.Customers = cusRegPrice.Customer;
                        Vessel.BPtShipName = cusRegPrice.Vessel;
                        Sim.BPtFBimMsisdn = cusRegPrice.InmarsatID;
                        Sim.BPtFBImImsi = cusRegPrice.ImsiID;
                        Sim.BPtFBimIccid = cusRegPrice.IccID;
                        Cusreg.BPtCusRgnFBDataPlanIn = Convert.ToString(cusRegPrice.Dataplan);
                        Cusreg.BPtCusRgnFbRecurFee = cusRegPrice.Recurfee;
                        Cusreg.BPtCusRgnFbInbundleDataRate = cusRegPrice.InbundleRate;
                        Cusreg.BPtCusRgnFbOutBundleDataRate = cusRegPrice.OutbundleRate;
                        Cusreg.BsPtCusRegStartdate = cusRegPrice.StartDate;
                        Cusreg.BsPtCusRegEnd = cusRegPrice.EndDate;
                        Cusreg.BsPtCusRegFBBarDate = cusRegPrice.BarDate;
                        Cusreg.BsPtCusRegFbBarDateLifted = cusRegPrice.BarDateLifted;
                        Cusreg.BsPtCusRegFBSusDate = cusRegPrice.SusDate;
                        Cusreg.BsPtCusRegFBSusDateLifted = cusRegPrice.SusDateLifted;
                        Cusreg.BsPtCusRegFBLayUpDate = cusRegPrice.LayupDate;
                        Cusreg.BsPtCusRegFBLayUpDateLifted = cusRegPrice.LayupDateLifted;

                        foreach (var item in serviceType)
                        {
                            switch (item.BsPtCusServiceTypes)
                            {
                                case "Voice  to Cellular":
                                    item.BsPtCusServiceSellerPrice = V2C;
                                    break;
                                case "Voice  to Fixed":
                                    item.BsPtCusServiceSellerPrice = V2F;
                                    break;
                                case "Voice  to FleetBroadband":
                                    item.BsPtCusServiceSellerPrice = V2FBB;
                                    break;
                                case "Voice  to Voicemail":
                                    item.BsPtCusServiceSellerPrice = V2V;
                                    break;
                                case "Voice  to Iridium":
                                    item.BsPtCusServiceSellerPrice = V2I;
                                    break;
                                case "ISDN":
                                    item.BsPtCusServiceSellerPrice = Isdn;
                                    break;
                                case "SMS":
                                    item.BsPtCusServiceSellerPrice = Sms;
                                    break;
                                case "Voice to Began":
                                    item.BsPtCusServiceSellerPrice = V2Bgn;
                                    break;
                                case "Voice  to GSPS":
                                    item.BsPtCusServiceSellerPrice = V2Gsps;
                                    break;
                                case "Standard IP":
                                    item.BsPtCusServiceSellerPrice = StdIP;
                                    break;
                            }

                        }
                        db.SaveChanges();

                    }
                }

                return RedirectToAction("CUSRegPrices");
            }

            return View(cusRegPrice);
        }
    }
}