﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers.Master_Ctrl
{
    [Authorize]
    public class CommunicationTypesController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/CommunicationTypes
        public ActionResult Index()
        {
            return View(db.BsMtCommunicationTypes.ToList());
        }

        // GET: BillingSystem/CommunicationTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtCommunicationType bsMtCommunicationType = db.BsMtCommunicationTypes.Find(id);
            if (bsMtCommunicationType == null)
            {
                return HttpNotFound();
            }
            return View(bsMtCommunicationType);
        }

        // GET: BillingSystem/CommunicationTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BillingSystem/CommunicationTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ComID,ComTypes,Descriptions")] BsMtCommunicationType bsMtCommunicationType)
        {
            if (ModelState.IsValid)
            {
                db.BsMtCommunicationTypes.Add(bsMtCommunicationType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bsMtCommunicationType);
        }

        // GET: BillingSystem/CommunicationTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtCommunicationType bsMtCommunicationType = db.BsMtCommunicationTypes.Find(id);
            if (bsMtCommunicationType == null)
            {
                return HttpNotFound();
            }
            return View(bsMtCommunicationType);
        }

        // POST: BillingSystem/CommunicationTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ComID,ComTypes,Descriptions")] BsMtCommunicationType bsMtCommunicationType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsMtCommunicationType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bsMtCommunicationType);
        }

        // GET: BillingSystem/CommunicationTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtCommunicationType bsMtCommunicationType = db.BsMtCommunicationTypes.Find(id);
            if (bsMtCommunicationType == null)
            {
                return HttpNotFound();
            }
            return View(bsMtCommunicationType);
        }

        // POST: BillingSystem/CommunicationTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsMtCommunicationType bsMtCommunicationType = db.BsMtCommunicationTypes.Find(id);
            db.BsMtCommunicationTypes.Remove(bsMtCommunicationType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
