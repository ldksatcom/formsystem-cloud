﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers.Master_Ctrl
{
    [Authorize]
    public class SPLesRegsController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/SPLesRegs
        public ActionResult Index()
        {
            var bsPtSPLesRegs = db.BsPtSPLesRegs.Include(b => b.BptFbSim).Include(b => b.BsMtServiceProvider).Include(b => b.BsMtUnit).Include(b => b.BsPtSPContract);
            return View(bsPtSPLesRegs.ToList());
        }

        // GET: BillingSystem/SPLesRegs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtSPLesReg bsPtSPLesReg = db.BsPtSPLesRegs.Find(id);
            if (bsPtSPLesReg == null)
            {
                return HttpNotFound();
            }
            return View(bsPtSPLesReg);
        }

        // GET: BillingSystem/SPLesRegs/Create
        public ActionResult Create()
        {
            ViewBag.fkSimID = new SelectList(db.BptFbSims, "Sim_ID", "BPtFBImImsi");
            ViewBag.fkSPID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider");
            ViewBag.BsPtLesRegDataRateUnitType = new SelectList(db.BsMtUnits, "UID", "Units");
            ViewBag.fkspcID = new SelectList(db.BsPtSPContracts, "spcID", "BsPtlesFbContNo");
            return View();
        }

        // POST: BillingSystem/SPLesRegs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SPLesRegID,fkSPID,fkspcID,fkSimID,BsPtlesRegFbContNo,BsPtlesRegStartdate,BsPtlesRegEnd,BsPtLesRegFBTerm,BsPtLesRegFBCardFee,BsPtLesRegFBActFee,BsPtLesRegFBRgnFee,BPtLesRgnFBMultiVce,BPtLesRgnFBDataPlanIn,BPtLesRgnFbVcePlanOPtLesRgn,BPtLesRgnFbRecurFee,BPtLesRgnFbRecurFeeType,BPtLesRgnFbInbundleDataRate,BPtLesRgnFbOutBundleDataRate,BsPtLesRegDataRateUnitType,BsPtLesRegFBBarDate,BsPtLesRegFbBarDateLifted,BsPtLesRegFBSusDate,BsPtLesRegFBSusDateLifted,BsPtLesRegFBSusMB_Lmt,BsPtLesRegFBSusUS_Lmt,BsPtLesRegFBLayUpDate,BsPtLesRegFBLayUpDateLifted,BsPtLesRegFBLayUpNosPerYr,BsPtLesRegFBLayUpPeriod")] BsPtSPLesReg bsPtSPLesReg)
        {
            if (ModelState.IsValid)
            {
                db.BsPtSPLesRegs.Add(bsPtSPLesReg);
                db.SaveChanges();
                return RedirectToAction("OteSatCDR","CDR");
            }

            ViewBag.fkSimID = new SelectList(db.BptFbSims, "Sim_ID", "BPtFBImImsi", bsPtSPLesReg.fkSimID);
            ViewBag.fkSPID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider", bsPtSPLesReg.fkSPID);
            ViewBag.BsPtLesRegDataRateUnitType = new SelectList(db.BsMtUnits, "UID", "Units", bsPtSPLesReg.BsPtLesRegDataRateUnitType);
            ViewBag.fkspcID = new SelectList(db.BsPtSPContracts, "spcID", "BsPtlesFbContNo", bsPtSPLesReg.fkspcID);
            return View(bsPtSPLesReg);
        }

        // GET: BillingSystem/SPLesRegs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtSPLesReg bsPtSPLesReg = db.BsPtSPLesRegs.Find(id);
            if (bsPtSPLesReg == null)
            {
                return HttpNotFound();
            }
            ViewBag.fkSimID = new SelectList(db.BptFbSims, "Sim_ID", "BPtFBImImsi", bsPtSPLesReg.fkSimID);
            ViewBag.fkSPID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider", bsPtSPLesReg.fkSPID);
            ViewBag.BsPtLesRegDataRateUnitType = new SelectList(db.BsMtUnits, "UID", "Units", bsPtSPLesReg.BsPtLesRegDataRateUnitType);
            ViewBag.fkspcID = new SelectList(db.BsPtSPContracts, "spcID", "BsPtlesFbContNo", bsPtSPLesReg.fkspcID);
            return View(bsPtSPLesReg);
        }

        // POST: BillingSystem/SPLesRegs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SPLesRegID,fkSPID,fkspcID,fkSimID,BsPtlesRegFbContNo,BsPtlesRegStartdate,BsPtlesRegEnd,BsPtLesRegFBTerm,BsPtLesRegFBCardFee,BsPtLesRegFBActFee,BsPtLesRegFBRgnFee,BPtLesRgnFBMultiVce,BPtLesRgnFBDataPlanIn,BPtLesRgnFbVcePlanOPtLesRgn,BPtLesRgnFbRecurFee,BPtLesRgnFbRecurFeeType,BPtLesRgnFbInbundleDataRate,BPtLesRgnFbOutBundleDataRate,BsPtLesRegDataRateUnitType,BsPtLesRegFBBarDate,BsPtLesRegFbBarDateLifted,BsPtLesRegFBSusDate,BsPtLesRegFBSusDateLifted,BsPtLesRegFBSusMB_Lmt,BsPtLesRegFBSusUS_Lmt,BsPtLesRegFBLayUpDate,BsPtLesRegFBLayUpDateLifted,BsPtLesRegFBLayUpNosPerYr,BsPtLesRegFBLayUpPeriod")] BsPtSPLesReg bsPtSPLesReg)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsPtSPLesReg).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.fkSimID = new SelectList(db.BptFbSims, "Sim_ID", "BPtFBImImsi", bsPtSPLesReg.fkSimID);
            ViewBag.fkSPID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider", bsPtSPLesReg.fkSPID);
            ViewBag.BsPtLesRegDataRateUnitType = new SelectList(db.BsMtUnits, "UID", "Units", bsPtSPLesReg.BsPtLesRegDataRateUnitType);
            ViewBag.fkspcID = new SelectList(db.BsPtSPContracts, "spcID", "BsPtlesFbContNo", bsPtSPLesReg.fkspcID);
            return View(bsPtSPLesReg);
        }

        // GET: BillingSystem/SPLesRegs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtSPLesReg bsPtSPLesReg = db.BsPtSPLesRegs.Find(id);
            if (bsPtSPLesReg == null)
            {
                return HttpNotFound();
            }
            return View(bsPtSPLesReg);
        }

        // POST: BillingSystem/SPLesRegs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsPtSPLesReg bsPtSPLesReg = db.BsPtSPLesRegs.Find(id);
            db.BsPtSPLesRegs.Remove(bsPtSPLesReg);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
