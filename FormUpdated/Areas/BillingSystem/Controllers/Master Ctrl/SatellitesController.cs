﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers
{
    [Authorize]
    public class SatellitesController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/Satellites
        public ActionResult Index()
        {
            return View(db.BsMtSatTypes.ToList());
        }

        // GET: BillingSystem/Satellites/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtSatType bsMtSatType = db.BsMtSatTypes.Find(id);
            if (bsMtSatType == null)
            {
                return HttpNotFound();
            }
            return View(bsMtSatType);
        }

        // GET: BillingSystem/Satellites/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BillingSystem/Satellites/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MsSatType,Satcode,SatTypes")] BsMtSatType bsMtSatType)
        {
            if (ModelState.IsValid)
            {
                db.BsMtSatTypes.Add(bsMtSatType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bsMtSatType);
        }

        // GET: BillingSystem/Satellites/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtSatType bsMtSatType = db.BsMtSatTypes.Find(id);
            if (bsMtSatType == null)
            {
                return HttpNotFound();
            }
            return View(bsMtSatType);
        }

        // POST: BillingSystem/Satellites/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MsSatType,Satcode,SatTypes")] BsMtSatType bsMtSatType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsMtSatType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bsMtSatType);
        }

        // GET: BillingSystem/Satellites/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtSatType bsMtSatType = db.BsMtSatTypes.Find(id);
            if (bsMtSatType == null)
            {
                return HttpNotFound();
            }
            return View(bsMtSatType);
        }

        // POST: BillingSystem/Satellites/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsMtSatType bsMtSatType = db.BsMtSatTypes.Find(id);
            db.BsMtSatTypes.Remove(bsMtSatType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
