﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers.Master_Ctrl
{
    [Authorize]
    public class ContractsController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/Contracts
        public ActionResult Index()
        {
            var bsPtSPContracts = db.BsPtSPContracts.Include(b => b.BsMtServiceProvider);
            return View(bsPtSPContracts.ToList());
        }

        // GET: BillingSystem/Contracts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtSPContract bsPtSPContract = db.BsPtSPContracts.Find(id);
            if (bsPtSPContract == null)
            {
                return HttpNotFound();
            }
            return View(bsPtSPContract);
        }

        // GET: BillingSystem/Contracts/Create
        public ActionResult Create()
        {
            ViewBag.fkSPID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider");
            return View();
        }

        // POST: BillingSystem/Contracts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "spcID,fkSPID,BsPtlesFbContNo,BsPtlesFbContStart,BsPtlesFbContEnd,BsPtLesFBstartDate,BsPtLesFBendDate,BsPtLesFBTerm,BsPtLesFBCardFee,BsPtLesFBActFee,BsPtLesFBRgnFee,BsPtLesFBBarDate,BsPtLesFbBarDateLifted,BsPtLesFBSusDate,BsPtLesFBSusDateLifted,BsPtLesFBSusMB_Lmt,BsPtLesFBSusUS_Lmt,BsPtLesFBLayUpDate,BsPtLesFBLayUpDateLifted,BsPtLesFBLayUpNosPerYr,BsPtLesFBLayUpPeriod")] BsPtSPContract bsPtSPContract)
        {
            if (ModelState.IsValid)
            {
                db.BsPtSPContracts.Add(bsPtSPContract);
                db.SaveChanges();
                return RedirectToAction("Create","FBBSimsRegs");
            }

            ViewBag.fkSPID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider", bsPtSPContract.fkSPID);
            return View(bsPtSPContract);
        }

        // GET: BillingSystem/Contracts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtSPContract bsPtSPContract = db.BsPtSPContracts.Find(id);
            if (bsPtSPContract == null)
            {
                return HttpNotFound();
            }
            ViewBag.fkSPID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider", bsPtSPContract.fkSPID);
            return View(bsPtSPContract);
        }

        // POST: BillingSystem/Contracts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "spcID,fkSPID,BsPtlesFbContNo,BsPtlesFbContStart,BsPtlesFbContEnd,BsPtLesFBstartDate,BsPtLesFBendDate,BsPtLesFBTerm,BsPtLesFBCardFee,BsPtLesFBActFee,BsPtLesFBRgnFee,BsPtLesFBBarDate,BsPtLesFbBarDateLifted,BsPtLesFBSusDate,BsPtLesFBSusDateLifted,BsPtLesFBSusMB_Lmt,BsPtLesFBSusUS_Lmt,BsPtLesFBLayUpDate,BsPtLesFBLayUpDateLifted,BsPtLesFBLayUpNosPerYr,BsPtLesFBLayUpPeriod")] BsPtSPContract bsPtSPContract)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsPtSPContract).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.fkSPID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider", bsPtSPContract.fkSPID);
            return View(bsPtSPContract);
        }

        // GET: BillingSystem/Contracts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtSPContract bsPtSPContract = db.BsPtSPContracts.Find(id);
            if (bsPtSPContract == null)
            {
                return HttpNotFound();
            }
            return View(bsPtSPContract);
        }

        // POST: BillingSystem/Contracts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsPtSPContract bsPtSPContract = db.BsPtSPContracts.Find(id);
            db.BsPtSPContracts.Remove(bsPtSPContract);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
