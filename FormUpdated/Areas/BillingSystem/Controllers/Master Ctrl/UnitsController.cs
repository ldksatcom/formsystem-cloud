﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers.Master_Ctrl
{
    [Authorize]
    public class UnitsController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/Units
        public ActionResult Index()
        {
            return View(db.BsMtUnits.ToList());
        }

        // GET: BillingSystem/Units/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtUnit bsMtUnit = db.BsMtUnits.Find(id);
            if (bsMtUnit == null)
            {
                return HttpNotFound();
            }
            return View(bsMtUnit);
        }

        // GET: BillingSystem/Units/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BillingSystem/Units/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UID,Units,Descriptions")] BsMtUnit bsMtUnit)
        {
            if (ModelState.IsValid)
            {
                db.BsMtUnits.Add(bsMtUnit);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bsMtUnit);
        }

        // GET: BillingSystem/Units/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtUnit bsMtUnit = db.BsMtUnits.Find(id);
            if (bsMtUnit == null)
            {
                return HttpNotFound();
            }
            return View(bsMtUnit);
        }

        // POST: BillingSystem/Units/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UID,Units,Descriptions")] BsMtUnit bsMtUnit)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsMtUnit).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bsMtUnit);
        }

        // GET: BillingSystem/Units/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtUnit bsMtUnit = db.BsMtUnits.Find(id);
            if (bsMtUnit == null)
            {
                return HttpNotFound();
            }
            return View(bsMtUnit);
        }

        // POST: BillingSystem/Units/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsMtUnit bsMtUnit = db.BsMtUnits.Find(id);
            db.BsMtUnits.Remove(bsMtUnit);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
