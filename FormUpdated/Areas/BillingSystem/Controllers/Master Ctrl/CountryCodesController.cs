﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers.Master_Ctrl
{
    [Authorize]
    public class CountryCodesController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/CountryCodes
        public ActionResult Index()
        {
            return View(db.BsMtImarsatCountryCodes.ToList());
        }

        // GET: BillingSystem/CountryCodes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtImarsatCountryCode bsMtImarsatCountryCode = db.BsMtImarsatCountryCodes.Find(id);
            if (bsMtImarsatCountryCode == null)
            {
                return HttpNotFound();
            }
            return View(bsMtImarsatCountryCode);
        }

        // GET: BillingSystem/CountryCodes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BillingSystem/CountryCodes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CountryCodeID,RefCode,Abbreviation")] BsMtImarsatCountryCode bsMtImarsatCountryCode)
        {
            if (ModelState.IsValid)
            {
                db.BsMtImarsatCountryCodes.Add(bsMtImarsatCountryCode);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bsMtImarsatCountryCode);
        }

        // GET: BillingSystem/CountryCodes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtImarsatCountryCode bsMtImarsatCountryCode = db.BsMtImarsatCountryCodes.Find(id);
            if (bsMtImarsatCountryCode == null)
            {
                return HttpNotFound();
            }
            return View(bsMtImarsatCountryCode);
        }

        // POST: BillingSystem/CountryCodes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CountryCodeID,RefCode,Abbreviation")] BsMtImarsatCountryCode bsMtImarsatCountryCode)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsMtImarsatCountryCode).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bsMtImarsatCountryCode);
        }

        // GET: BillingSystem/CountryCodes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtImarsatCountryCode bsMtImarsatCountryCode = db.BsMtImarsatCountryCodes.Find(id);
            if (bsMtImarsatCountryCode == null)
            {
                return HttpNotFound();
            }
            return View(bsMtImarsatCountryCode);
        }

        // POST: BillingSystem/CountryCodes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsMtImarsatCountryCode bsMtImarsatCountryCode = db.BsMtImarsatCountryCodes.Find(id);
            db.BsMtImarsatCountryCodes.Remove(bsMtImarsatCountryCode);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
