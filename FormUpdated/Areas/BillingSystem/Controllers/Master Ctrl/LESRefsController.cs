﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers.Master_Ctrl
{
    [Authorize]
    public class LESRefsController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/LESRefs
        public ActionResult Index()
        {
            var bsMtLESRefs = db.BsMtLESRefs.Include(b => b.BsMtServiceProvider);
            return View(bsMtLESRefs.ToList());
        }

        // GET: BillingSystem/LESRefs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtLESRef bsMtLESRef = db.BsMtLESRefs.Find(id);
            if (bsMtLESRef == null)
            {
                return HttpNotFound();
            }
            return View(bsMtLESRef);
        }

        // GET: BillingSystem/LESRefs/Create
        public ActionResult Create()
        {
            ViewBag.fkSPID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider");
            return View();
        }

        // POST: BillingSystem/LESRefs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LesRefID,LESCode,Description,fkSPID,SMTSCode")] BsMtLESRef bsMtLESRef)
        {
            if (ModelState.IsValid)
            {
                db.BsMtLESRefs.Add(bsMtLESRef);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.fkSPID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider", bsMtLESRef.fkSPID);
            return View(bsMtLESRef);
        }

        // GET: BillingSystem/LESRefs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtLESRef bsMtLESRef = db.BsMtLESRefs.Find(id);
            if (bsMtLESRef == null)
            {
                return HttpNotFound();
            }
            ViewBag.fkSPID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider", bsMtLESRef.fkSPID);
            return View(bsMtLESRef);
        }

        // POST: BillingSystem/LESRefs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LesRefID,LESCode,Description,fkSPID,SMTSCode")] BsMtLESRef bsMtLESRef)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsMtLESRef).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.fkSPID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider", bsMtLESRef.fkSPID);
            return View(bsMtLESRef);
        }

        // GET: BillingSystem/LESRefs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtLESRef bsMtLESRef = db.BsMtLESRefs.Find(id);
            if (bsMtLESRef == null)
            {
                return HttpNotFound();
            }
            return View(bsMtLESRef);
        }

        // POST: BillingSystem/LESRefs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsMtLESRef bsMtLESRef = db.BsMtLESRefs.Find(id);
            db.BsMtLESRefs.Remove(bsMtLESRef);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
