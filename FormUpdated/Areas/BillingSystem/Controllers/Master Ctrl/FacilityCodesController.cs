﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers.Master_Ctrl
{
    [Authorize]
    public class FacilityCodesController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/FacilityCodes
        public ActionResult Index()
        {
            return View(db.BsMtImarsatCDFACIs.ToList());
        }

        // GET: BillingSystem/FacilityCodes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtImarsatCDFACI bsMtImarsatCDFACI = db.BsMtImarsatCDFACIs.Find(id);
            if (bsMtImarsatCDFACI == null)
            {
                return HttpNotFound();
            }
            return View(bsMtImarsatCDFACI);
        }

        // GET: BillingSystem/FacilityCodes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BillingSystem/FacilityCodes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CDFACIID,RefCode,Abbreviation,Descriptions")] BsMtImarsatCDFACI bsMtImarsatCDFACI)
        {
            if (ModelState.IsValid)
            {
                db.BsMtImarsatCDFACIs.Add(bsMtImarsatCDFACI);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bsMtImarsatCDFACI);
        }

        // GET: BillingSystem/FacilityCodes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtImarsatCDFACI bsMtImarsatCDFACI = db.BsMtImarsatCDFACIs.Find(id);
            if (bsMtImarsatCDFACI == null)
            {
                return HttpNotFound();
            }
            return View(bsMtImarsatCDFACI);
        }

        // POST: BillingSystem/FacilityCodes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CDFACIID,RefCode,Abbreviation,Descriptions")] BsMtImarsatCDFACI bsMtImarsatCDFACI)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsMtImarsatCDFACI).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bsMtImarsatCDFACI);
        }

        // GET: BillingSystem/FacilityCodes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtImarsatCDFACI bsMtImarsatCDFACI = db.BsMtImarsatCDFACIs.Find(id);
            if (bsMtImarsatCDFACI == null)
            {
                return HttpNotFound();
            }
            return View(bsMtImarsatCDFACI);
        }

        // POST: BillingSystem/FacilityCodes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsMtImarsatCDFACI bsMtImarsatCDFACI = db.BsMtImarsatCDFACIs.Find(id);
            db.BsMtImarsatCDFACIs.Remove(bsMtImarsatCDFACI);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
