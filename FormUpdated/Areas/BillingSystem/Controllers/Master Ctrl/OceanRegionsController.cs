﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers
{
    [Authorize]
    public class OceanRegionsController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/OceanRegions
        public ActionResult Index()
        {
            return View(db.BsMtImarsatOceanRegions.ToList());
        }

        // GET: BillingSystem/OceanRegions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtImarsatOceanRegion bsMtImarsatOceanRegion = db.BsMtImarsatOceanRegions.Find(id);
            if (bsMtImarsatOceanRegion == null)
            {
                return HttpNotFound();
            }
            return View(bsMtImarsatOceanRegion);
        }

        // GET: BillingSystem/OceanRegions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BillingSystem/OceanRegions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OceanRegionID,RefCode,Abbreviation")] BsMtImarsatOceanRegion bsMtImarsatOceanRegion)
        {
            if (ModelState.IsValid)
            {
                db.BsMtImarsatOceanRegions.Add(bsMtImarsatOceanRegion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bsMtImarsatOceanRegion);
        }

        // GET: BillingSystem/OceanRegions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtImarsatOceanRegion bsMtImarsatOceanRegion = db.BsMtImarsatOceanRegions.Find(id);
            if (bsMtImarsatOceanRegion == null)
            {
                return HttpNotFound();
            }
            return View(bsMtImarsatOceanRegion);
        }

        // POST: BillingSystem/OceanRegions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OceanRegionID,RefCode,Abbreviation")] BsMtImarsatOceanRegion bsMtImarsatOceanRegion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsMtImarsatOceanRegion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bsMtImarsatOceanRegion);
        }

        // GET: BillingSystem/OceanRegions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtImarsatOceanRegion bsMtImarsatOceanRegion = db.BsMtImarsatOceanRegions.Find(id);
            if (bsMtImarsatOceanRegion == null)
            {
                return HttpNotFound();
            }
            return View(bsMtImarsatOceanRegion);
        }

        // POST: BillingSystem/OceanRegions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsMtImarsatOceanRegion bsMtImarsatOceanRegion = db.BsMtImarsatOceanRegions.Find(id);
            db.BsMtImarsatOceanRegions.Remove(bsMtImarsatOceanRegion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
