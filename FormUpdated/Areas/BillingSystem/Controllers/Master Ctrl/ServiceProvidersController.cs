﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers.Master_Ctrl
{
    [Authorize]
    public class ServiceProvidersController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/ServiceProviders
        public ActionResult Index()
        {
            return View(db.BsMtServiceProviders.ToList());
        }

        // GET: BillingSystem/ServiceProviders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtServiceProvider bsMtServiceProvider = db.BsMtServiceProviders.Find(id);
            if (bsMtServiceProvider == null)
            {
                return HttpNotFound();
            }
            return View(bsMtServiceProvider);
        }

        // GET: BillingSystem/ServiceProviders/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BillingSystem/ServiceProviders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "spID,ServiceProvider,Descriptions")] BsMtServiceProvider bsMtServiceProvider)
        {
            if (ModelState.IsValid)
            {
                db.BsMtServiceProviders.Add(bsMtServiceProvider);
                db.SaveChanges();
                return RedirectToAction("Create","Contracts");
            }

            return View(bsMtServiceProvider);
        }

        // GET: BillingSystem/ServiceProviders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtServiceProvider bsMtServiceProvider = db.BsMtServiceProviders.Find(id);
            if (bsMtServiceProvider == null)
            {
                return HttpNotFound();
            }
            return View(bsMtServiceProvider);
        }

        // POST: BillingSystem/ServiceProviders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "spID,ServiceProvider,Descriptions")] BsMtServiceProvider bsMtServiceProvider)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsMtServiceProvider).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bsMtServiceProvider);
        }

        // GET: BillingSystem/ServiceProviders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtServiceProvider bsMtServiceProvider = db.BsMtServiceProviders.Find(id);
            if (bsMtServiceProvider == null)
            {
                return HttpNotFound();
            }
            return View(bsMtServiceProvider);
        }

        // POST: BillingSystem/ServiceProviders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsMtServiceProvider bsMtServiceProvider = db.BsMtServiceProviders.Find(id);
            db.BsMtServiceProviders.Remove(bsMtServiceProvider);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
