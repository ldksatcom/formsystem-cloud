﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers
{
    [Authorize]
    public class ServiceCodesController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/ServiceCodes
        public ActionResult Index()
        {
            return View(db.BsMtImarsatServCodes.ToList());
        }

        // GET: BillingSystem/ServiceCodes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtImarsatServCode bsMtImarsatServCode = db.BsMtImarsatServCodes.Find(id);
            if (bsMtImarsatServCode == null)
            {
                return HttpNotFound();
            }
            return View(bsMtImarsatServCode);
        }

        // GET: BillingSystem/ServiceCodes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BillingSystem/ServiceCodes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ServCodeID,RefCode,Abbreviation,Descriptions")] BsMtImarsatServCode bsMtImarsatServCode)
        {
            if (ModelState.IsValid)
            {
                db.BsMtImarsatServCodes.Add(bsMtImarsatServCode);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bsMtImarsatServCode);
        }

        // GET: BillingSystem/ServiceCodes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtImarsatServCode bsMtImarsatServCode = db.BsMtImarsatServCodes.Find(id);
            if (bsMtImarsatServCode == null)
            {
                return HttpNotFound();
            }
            return View(bsMtImarsatServCode);
        }

        // POST: BillingSystem/ServiceCodes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ServCodeID,RefCode,Abbreviation,Descriptions")] BsMtImarsatServCode bsMtImarsatServCode)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsMtImarsatServCode).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bsMtImarsatServCode);
        }

        // GET: BillingSystem/ServiceCodes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtImarsatServCode bsMtImarsatServCode = db.BsMtImarsatServCodes.Find(id);
            if (bsMtImarsatServCode == null)
            {
                return HttpNotFound();
            }
            return View(bsMtImarsatServCode);
        }

        // POST: BillingSystem/ServiceCodes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsMtImarsatServCode bsMtImarsatServCode = db.BsMtImarsatServCodes.Find(id);
            db.BsMtImarsatServCodes.Remove(bsMtImarsatServCode);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
