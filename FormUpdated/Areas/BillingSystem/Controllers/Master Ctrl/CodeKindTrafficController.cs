﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers.Master_Ctrl
{
    [Authorize]
    public class CodeKindTrafficController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/CodeKindTraffic
        public ActionResult Index()
        {
            return View(db.BsMtImarsatCDKINDs.ToList());
        }

        // GET: BillingSystem/CodeKindTraffic/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtImarsatCDKIND bsMtImarsatCDKIND = db.BsMtImarsatCDKINDs.Find(id);
            if (bsMtImarsatCDKIND == null)
            {
                return HttpNotFound();
            }
            return View(bsMtImarsatCDKIND);
        }

        // GET: BillingSystem/CodeKindTraffic/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BillingSystem/CodeKindTraffic/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CDKINDID,RefCode,Abbreviation,Descriptions")] BsMtImarsatCDKIND bsMtImarsatCDKIND)
        {
            if (ModelState.IsValid)
            {
                db.BsMtImarsatCDKINDs.Add(bsMtImarsatCDKIND);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bsMtImarsatCDKIND);
        }

        // GET: BillingSystem/CodeKindTraffic/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtImarsatCDKIND bsMtImarsatCDKIND = db.BsMtImarsatCDKINDs.Find(id);
            if (bsMtImarsatCDKIND == null)
            {
                return HttpNotFound();
            }
            return View(bsMtImarsatCDKIND);
        }

        // POST: BillingSystem/CodeKindTraffic/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CDKINDID,RefCode,Abbreviation,Descriptions")] BsMtImarsatCDKIND bsMtImarsatCDKIND)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsMtImarsatCDKIND).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bsMtImarsatCDKIND);
        }

        // GET: BillingSystem/CodeKindTraffic/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtImarsatCDKIND bsMtImarsatCDKIND = db.BsMtImarsatCDKINDs.Find(id);
            if (bsMtImarsatCDKIND == null)
            {
                return HttpNotFound();
            }
            return View(bsMtImarsatCDKIND);
        }

        // POST: BillingSystem/CodeKindTraffic/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsMtImarsatCDKIND bsMtImarsatCDKIND = db.BsMtImarsatCDKINDs.Find(id);
            db.BsMtImarsatCDKINDs.Remove(bsMtImarsatCDKIND);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
