﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers
{
    [Authorize]
    public class CallTypeCodesController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/CallTypeCodes
        public ActionResult Index()
        {
            return View(db.BsMtImarsatCallTypeCodes.ToList());
        }

        // GET: BillingSystem/CallTypeCodes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtImarsatCallTypeCode bsMtImarsatCallTypeCode = db.BsMtImarsatCallTypeCodes.Find(id);
            if (bsMtImarsatCallTypeCode == null)
            {
                return HttpNotFound();
            }
            return View(bsMtImarsatCallTypeCode);
        }

        // GET: BillingSystem/CallTypeCodes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BillingSystem/CallTypeCodes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CallTypeCodeID,RefCode,Abbreviation,Descriptions")] BsMtImarsatCallTypeCode bsMtImarsatCallTypeCode)
        {
            if (ModelState.IsValid)
            {
                db.BsMtImarsatCallTypeCodes.Add(bsMtImarsatCallTypeCode);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bsMtImarsatCallTypeCode);
        }

        // GET: BillingSystem/CallTypeCodes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtImarsatCallTypeCode bsMtImarsatCallTypeCode = db.BsMtImarsatCallTypeCodes.Find(id);
            if (bsMtImarsatCallTypeCode == null)
            {
                return HttpNotFound();
            }
            return View(bsMtImarsatCallTypeCode);
        }

        // POST: BillingSystem/CallTypeCodes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CallTypeCodeID,RefCode,Abbreviation,Descriptions")] BsMtImarsatCallTypeCode bsMtImarsatCallTypeCode)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsMtImarsatCallTypeCode).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bsMtImarsatCallTypeCode);
        }

        // GET: BillingSystem/CallTypeCodes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtImarsatCallTypeCode bsMtImarsatCallTypeCode = db.BsMtImarsatCallTypeCodes.Find(id);
            if (bsMtImarsatCallTypeCode == null)
            {
                return HttpNotFound();
            }
            return View(bsMtImarsatCallTypeCode);
        }

        // POST: BillingSystem/CallTypeCodes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsMtImarsatCallTypeCode bsMtImarsatCallTypeCode = db.BsMtImarsatCallTypeCodes.Find(id);
            db.BsMtImarsatCallTypeCodes.Remove(bsMtImarsatCallTypeCode);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
