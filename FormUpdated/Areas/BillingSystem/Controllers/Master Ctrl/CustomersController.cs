﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers
{
    [Authorize]
    public class CustomersController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/Customers
        public ActionResult Index()
        {
            return View(db.BsMtCustomers.ToList());
        }

        // GET: BillingSystem/Customers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtCustomer bsMtCustomer = db.BsMtCustomers.Find(id);
            if (bsMtCustomer == null)
            {
                return HttpNotFound();
            }
            return View(bsMtCustomer);
        }

        // GET: BillingSystem/Customers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BillingSystem/Customers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CusID,Customers,Descriptions")] BsMtCustomer bsMtCustomer)
        {
            if (ModelState.IsValid)
            {
                db.BsMtCustomers.Add(bsMtCustomer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bsMtCustomer);
        }

        // GET: BillingSystem/Customers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtCustomer bsMtCustomer = db.BsMtCustomers.Find(id);
            if (bsMtCustomer == null)
            {
                return HttpNotFound();
            }
            return View(bsMtCustomer);
        }

        // POST: BillingSystem/Customers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CusID,Customers,Descriptions")] BsMtCustomer bsMtCustomer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsMtCustomer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bsMtCustomer);
        }

        // GET: BillingSystem/Customers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsMtCustomer bsMtCustomer = db.BsMtCustomers.Find(id);
            if (bsMtCustomer == null)
            {
                return HttpNotFound();
            }
            return View(bsMtCustomer);
        }

        // POST: BillingSystem/Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsMtCustomer bsMtCustomer = db.BsMtCustomers.Find(id);
            db.BsMtCustomers.Remove(bsMtCustomer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
