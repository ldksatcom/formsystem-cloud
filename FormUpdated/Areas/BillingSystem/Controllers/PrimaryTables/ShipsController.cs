﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers
{
    [Authorize]
    public class ShipsController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/Ships
        public ActionResult Index()
        {
            var bPtShips = db.BPtShips.Include(b => b.BsPtCusContract).Include(b => b.BsMtCustomer);
            return View(bPtShips.ToList());
        }

        // GET: BillingSystem/Ships/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BPtShip bPtShip = db.BPtShips.Find(id);
            if (bPtShip == null)
            {
                return HttpNotFound();
            }
            return View(bPtShip);
        }

        // GET: BillingSystem/Ships/Create
        public ActionResult Create()
        {
            ViewBag.BPtShipCusContractID = new SelectList(db.BsPtCusContracts, "cuscID", "BsPtCusFbContNo");
            ViewBag.BPtShipCusID = new SelectList(db.BsMtCustomers, "CusID", "Customers");
            return View();
        }

        // POST: BillingSystem/Ships/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Ship_ID,BPtShipCode,BPtShipName,BPtShipCallSign,BPtShipAAIC,BPtShipMMSI,NA,NA1,NA2,NA3,BPtShipOwrCoCode,BPtShipBillCoCode,BPtShipDealCoCode,NA4,NA5,BPtShipCountry,BPtShipIMO,BPtShipRegistryPort,BPtShipHomePort,BPtShipType,BPtShipGRT,BPtShipOWT,BPtShipBuildYear,BPtShipSelfPropelled,BPtShipNoPerson,BPtShipEmergencyCtc,BPtShipEmergencyCtcTel,NA6,NA7,NA8,BPtShipStartDate,BPtShipEndDate,BPtShipSuspStDate,BPtShipSuspEndDate,BPtShipBarStDate,BPtShipBarEndDate,BPtShipCusID,BPtShipCusContractID")] BPtShip bPtShip)
        {
            if (ModelState.IsValid)
            {
                db.BPtShips.Add(bPtShip);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BPtShipCusContractID = new SelectList(db.BsPtCusContracts, "cuscID", "BsPtCusFbContNo", bPtShip.BPtShipCusContractID);
            ViewBag.BPtShipCusID = new SelectList(db.BsMtCustomers, "CusID", "Customers", bPtShip.BPtShipCusID);
            return View(bPtShip);
        }

        // GET: BillingSystem/Ships/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BPtShip bPtShip = db.BPtShips.Find(id);
            if (bPtShip == null)
            {
                return HttpNotFound();
            }
            ViewBag.BPtShipCusContractID = new SelectList(db.BsPtCusContracts, "cuscID", "BsPtCusFbContNo", bPtShip.BPtShipCusContractID);
            ViewBag.BPtShipCusID = new SelectList(db.BsMtCustomers, "CusID", "Customers", bPtShip.BPtShipCusID);
            return View(bPtShip);
        }

        // POST: BillingSystem/Ships/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Ship_ID,BPtShipCode,BPtShipName,BPtShipCallSign,BPtShipAAIC,BPtShipMMSI,NA,NA1,NA2,NA3,BPtShipOwrCoCode,BPtShipBillCoCode,BPtShipDealCoCode,NA4,NA5,BPtShipCountry,BPtShipIMO,BPtShipRegistryPort,BPtShipHomePort,BPtShipType,BPtShipGRT,BPtShipOWT,BPtShipBuildYear,BPtShipSelfPropelled,BPtShipNoPerson,BPtShipEmergencyCtc,BPtShipEmergencyCtcTel,NA6,NA7,NA8,BPtShipStartDate,BPtShipEndDate,BPtShipSuspStDate,BPtShipSuspEndDate,BPtShipBarStDate,BPtShipBarEndDate,BPtShipCusID,BPtShipCusContractID")] BPtShip bPtShip)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bPtShip).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BPtShipCusContractID = new SelectList(db.BsPtCusContracts, "cuscID", "BsPtCusFbContNo", bPtShip.BPtShipCusContractID);
            ViewBag.BPtShipCusID = new SelectList(db.BsMtCustomers, "CusID", "Customers", bPtShip.BPtShipCusID);
            return View(bPtShip);
        }

        // GET: BillingSystem/Ships/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BPtShip bPtShip = db.BPtShips.Find(id);
            if (bPtShip == null)
            {
                return HttpNotFound();
            }
            return View(bPtShip);
        }

        // POST: BillingSystem/Ships/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BPtShip bPtShip = db.BPtShips.Find(id);
            db.BPtShips.Remove(bPtShip);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
