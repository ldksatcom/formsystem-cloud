﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers.PrimaryTables
{
    [Authorize]
    public class StaffRegsController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/StaffRegs
        public ActionResult Index()
        {
            return View(db.BsPtStaffs.ToList());
        }

        // GET: BillingSystem/StaffRegs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtStaff bsPtStaff = db.BsPtStaffs.Find(id);
            if (bsPtStaff == null)
            {
                return HttpNotFound();
            }
            return View(bsPtStaff);
        }

        // GET: BillingSystem/StaffRegs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BillingSystem/StaffRegs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Staff_ID,BPtStaffCode,BPtStaffName,BPtStaffICNo,BPtStaffPassportNo,BPtStaffDOB,BPtStaffNationality,NA,NA1,NA2,BPtStaffTitle,BPtStafDept,BPtStaffStartDate,BPtStaffEndDate,BPtStaffSuspStDate,BPtStaffSuspEndDate,NA3,NA4,Na5,Na6,BPtStaffUserID,BPtStaffPwd,BPtStaffAccessLevel,NA7,NA8,BPtStaffOfficeEmail,BPtStaffOfficeTelNo,BPtStaffOfficeTelExtNo,BPtStaffMobile,NA9,BPtStaffPrivateMobile,BPtStaffPrivateEmail,BPtStaffKinName,BPtStaffKinMobile,BPtStaffAddrSt,BPtStaffAddrBldg,BPtStaffCity,BPtStaffState,BPtStaffCountry,BPtStaffZipCode")] BsPtStaff bsPtStaff)
        {
            if (ModelState.IsValid)
            {
                db.BsPtStaffs.Add(bsPtStaff);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bsPtStaff);
        }

        // GET: BillingSystem/StaffRegs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtStaff bsPtStaff = db.BsPtStaffs.Find(id);
            if (bsPtStaff == null)
            {
                return HttpNotFound();
            }
            return View(bsPtStaff);
        }

        // POST: BillingSystem/StaffRegs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Staff_ID,BPtStaffCode,BPtStaffName,BPtStaffICNo,BPtStaffPassportNo,BPtStaffDOB,BPtStaffNationality,NA,NA1,NA2,BPtStaffTitle,BPtStafDept,BPtStaffStartDate,BPtStaffEndDate,BPtStaffSuspStDate,BPtStaffSuspEndDate,NA3,NA4,Na5,Na6,BPtStaffUserID,BPtStaffPwd,BPtStaffAccessLevel,NA7,NA8,BPtStaffOfficeEmail,BPtStaffOfficeTelNo,BPtStaffOfficeTelExtNo,BPtStaffMobile,NA9,BPtStaffPrivateMobile,BPtStaffPrivateEmail,BPtStaffKinName,BPtStaffKinMobile,BPtStaffAddrSt,BPtStaffAddrBldg,BPtStaffCity,BPtStaffState,BPtStaffCountry,BPtStaffZipCode")] BsPtStaff bsPtStaff)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsPtStaff).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bsPtStaff);
        }

        // GET: BillingSystem/StaffRegs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtStaff bsPtStaff = db.BsPtStaffs.Find(id);
            if (bsPtStaff == null)
            {
                return HttpNotFound();
            }
            return View(bsPtStaff);
        }

        // POST: BillingSystem/StaffRegs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsPtStaff bsPtStaff = db.BsPtStaffs.Find(id);
            db.BsPtStaffs.Remove(bsPtStaff);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
