﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers
{
    [Authorize]
    public class LesRegsController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        private ILogger logger;

        public LesRegsController()
        {
            logger = log.GetInstance;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            string folderPath = System.Configuration.ConfigurationManager.AppSettings["ErrorFolder"];
            folderPath = Server.MapPath(folderPath);

            logger.LogExceptions(filterContext.Exception, folderPath);

            filterContext.ExceptionHandled = true;
            this.View("Error").ExecuteResult(this.ControllerContext);
        }

        // GET: BillingSystem/LesRegs
        public ActionResult Index()
        {
            return View(db.BsPtLesRegs.ToList());
        }

        // GET: BillingSystem/LesRegs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtLesReg bsPtLesReg = db.BsPtLesRegs.Find(id);
            if (bsPtLesReg == null)
            {
                return HttpNotFound();
            }
            return View(bsPtLesReg);
        }

        // GET: BillingSystem/LesRegs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BillingSystem/LesRegs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LesReg_ID,BPtLesRgnFBimSingle,BPtLesRgnFBimDual,BPtLesRgnFbMultiVce,BPtLesRgnFbContNo,BPtLesRgnFbContStart,NA6,NA7,BPtLesRgnFbContEnd,BPtLesRgnFBtartDate,BPtLesRgnpFbEndDate,BPtLesRgnFbTerm,BPtLesRgnFbCardFee,BPtLesRgnFbActFee,BPtLesRgnFbRgnFee,NA15,NA16,NA17,NA18,NA19,BPtLesRgnFbDataInBundleId,BPtLesRgnFbDataMSIDNId,BPtLesRgnFbDataSimardId,NA23,NA24,BPtLesRgnFbDataPlanIn,BPtLesRgnFbVcePlanOPtLesRgn,BPtLesRgnFbRecurFee,BPtLesRgnFbRecurFeeType,BPtLesRgnFbDataRate,BPtLesRgnFbBilIncre,BPtLesRgnFbunitType,NA32,NA33,NA34,BPtLesRgnFbTranDServType,BPtLesRgnFbTranDStartDate,BPtLesRgnFbTranDStartTime,BPtLesRgnFbTranDEndTime,BPtLesRgnFbTranDOrig,BPtLesRgnFbTranDDest,BPtLesRgnFbTranDUsage,NA42,NA43,NA44,NA45,NA46,NA47,NA48,NA49,BPtLesRgnFbVce2Fixed,BPtLesRgnFbVce2Cell,BPtLesRgnFbVc2Bgan,BPtLesRgnFbVce2Fb,BPtLesRgnFbVce2Gpsp,BPtLesRgnFbVce2VceMail,BPtLesRgnFbVce2Others,BPtLesRgnFbVcebillincre,BPtLesRgnFbVceunitytype,NA59,BPtLesRgnFbTranVServType,BPtLesRgnFbTranVStartDate,BPtLesRgnFbTranVStartTime,BPtLesRgnFbTranVEndTime,BPtLesRgnFbTranVOrig,BPtLesRgnFbTranVDest,BPtLesRgnFbTranVUsage,NA67,NA68,BPtLesRgnStrInBundkeFbId,BPtLesRgnFbStrMSIDNId,BPtLesRgnFbStrSimCardId,NA72,BPtLesRgnFb8kbps,BPtLesRgnFb16kbps,BPtLesRgnFb32kbps,BPtLesRgnFb64kbps,BPtLesRgnFb128kbps,BPtLesRgnFb256kbps,BPtLesRgnFBtBillIncre,BPtLesRgnFBtBillingUnittype,NA81,NA82,BPtLesRgnFbTranStServType,BPtLesRgnFbTranStStartDate,BPtLesRgnFbTranStStartTime,BPtLesRgnFbTranStEndTime,BPtLesRgnFbTranStOrig,BPtLesRgnFbTranStDest,BPtLesRgnFbTranStUsage,NA90,NA91,NA92,BPtLesRgnFb2Bgan,BPtLesRgnFb2FB,BPtLesRgnFb2Gsps,BPtLesRgnFb2Fleet,BPtLesRgnFb2SwV,BPtLesRgnFb2AeroV,BPtLesRgnFb2Iridium,BPtLesRgnFb2Thu,BPtLesRgnFb2OMes,NA102,BPtLesRgnFbTranOtherServType,BPtLesRgnFbTranOtherstartDate,BPtLesRgnFbTranOtherStartTime,BPtLesRgnFbTranOtherEndTime,BPtLesRgnFbTranOtherOrig,BPtLesRgnFbTranOtherDest,BPtLesRgnFbTranOtherUsage,NA110,NA111,NA112,BPtLesRgnFbSMSInBundleId,BPtLesRgnFbSMSMSIDNId,BPtLesRgnFbSMSSimCardId,BPtLesRgnFBms,BPtLesRgnFbIsdnFax,BPtLesRgnFbTranSmsServType,BPtLesRgnFbTranSmsstartDate,BPtLesRgnFbTranSmsStartTime,BPtLesRgnFbTranSmsEndTime,BPtLesRgnFbTranSmsOrig,BPtLesRgnFbTranSmsDest,BPtLesRgnFbTranSmsUsage,NA125,BPtLesRgnFbLesRegTCId,BPtLesRgnFbLesRegTCMSIDNId,BPtLesRgnFbLesRegTCSimCardId,NA129,BPtLesRgnEndFbPenalty,BPtLesRgnFbCurBill,NA132,BPtLesRgnFbGst,BPtLesRgnFbPayTerm,NA135,NA136,NA137,NA138,NA139,BPtLesRgnFbIbvFmt,BPtLesRgnFbBarDate,BPtLesRgnFbBarDateLifted,BPtLesRgnFBusDate,BPtLesRgnFBusDateLifted,BPtLesRgnFBusMB_Lmt,BPtLesRgnFBusUS_Lmt,BPtLesRgnFblayUpDate,BPtLesRgnFbLayUpDateLifted,BPtLesRgnFbLayUpNosPerYr,BPtLesRgnFbLayUpPeriod,BPtLesRgnFbBillrecip,BPtLesRgnFbInvPWeight,BPtLesRgnFEerrAdju")] BsPtLesReg bsPtLesReg)
        {
            if (ModelState.IsValid)
            {
                db.BsPtLesRegs.Add(bsPtLesReg);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bsPtLesReg);
        }

        [HttpPost]
        public ActionResult UploadFiles()
        {
            string columnname = string.Empty;

            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                    
                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        //fname = Path.Combine(Server.MapPath("~/Uploads/"), fname);
                        string tempPath = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + fname;
                        file.SaveAs(tempPath);

                        Dictionary<string, string> Dictrejection;

                        Dictrejection = new Dictionary<string, string>();

                        BillingHelpers.connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString;

                        BillingHelpers helpers = new BillingHelpers();
                        string test = BillingHelpers.LesRegExcelRead(tempPath, "LesReg");
                    }
                    // Returns message that successfully uploaded  
                    return Json("File Uploaded Successfully!", JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    string path = Server.MapPath("~/ErrorLog/ErrorLog.txt");
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }
                    BillingHelpers.LogError(ex, path);

                    return Json("Error occurred. Error details: " + ex.Message, JsonRequestBehavior.AllowGet);
                }
                finally
                {

                }
            }
            else
            {
                return Json("No files selected.");
            }
        }

        // GET: BillingSystem/LesRegs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtLesReg bsPtLesReg = db.BsPtLesRegs.Find(id);
            if (bsPtLesReg == null)
            {
                return HttpNotFound();
            }
            return View(bsPtLesReg);
        }

        // POST: BillingSystem/LesRegs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LesReg_ID,BPtLesRgnFBimSingle,BPtLesRgnFBimDual,BPtLesRgnFbMultiVce,BPtLesRgnFbContNo,BPtLesRgnFbContStart,NA6,NA7,BPtLesRgnFbContEnd,BPtLesRgnFBtartDate,BPtLesRgnpFbEndDate,BPtLesRgnFbTerm,BPtLesRgnFbCardFee,BPtLesRgnFbActFee,BPtLesRgnFbRgnFee,NA15,NA16,NA17,NA18,NA19,BPtLesRgnFbDataInBundleId,BPtLesRgnFbDataMSIDNId,BPtLesRgnFbDataSimardId,NA23,NA24,BPtLesRgnFbDataPlanIn,BPtLesRgnFbVcePlanOPtLesRgn,BPtLesRgnFbRecurFee,BPtLesRgnFbRecurFeeType,BPtLesRgnFbDataRate,BPtLesRgnFbBilIncre,BPtLesRgnFbunitType,NA32,NA33,NA34,BPtLesRgnFbTranDServType,BPtLesRgnFbTranDStartDate,BPtLesRgnFbTranDStartTime,BPtLesRgnFbTranDEndTime,BPtLesRgnFbTranDOrig,BPtLesRgnFbTranDDest,BPtLesRgnFbTranDUsage,NA42,NA43,NA44,NA45,NA46,NA47,NA48,NA49,BPtLesRgnFbVce2Fixed,BPtLesRgnFbVce2Cell,BPtLesRgnFbVc2Bgan,BPtLesRgnFbVce2Fb,BPtLesRgnFbVce2Gpsp,BPtLesRgnFbVce2VceMail,BPtLesRgnFbVce2Others,BPtLesRgnFbVcebillincre,BPtLesRgnFbVceunitytype,NA59,BPtLesRgnFbTranVServType,BPtLesRgnFbTranVStartDate,BPtLesRgnFbTranVStartTime,BPtLesRgnFbTranVEndTime,BPtLesRgnFbTranVOrig,BPtLesRgnFbTranVDest,BPtLesRgnFbTranVUsage,NA67,NA68,BPtLesRgnStrInBundkeFbId,BPtLesRgnFbStrMSIDNId,BPtLesRgnFbStrSimCardId,NA72,BPtLesRgnFb8kbps,BPtLesRgnFb16kbps,BPtLesRgnFb32kbps,BPtLesRgnFb64kbps,BPtLesRgnFb128kbps,BPtLesRgnFb256kbps,BPtLesRgnFBtBillIncre,BPtLesRgnFBtBillingUnittype,NA81,NA82,BPtLesRgnFbTranStServType,BPtLesRgnFbTranStStartDate,BPtLesRgnFbTranStStartTime,BPtLesRgnFbTranStEndTime,BPtLesRgnFbTranStOrig,BPtLesRgnFbTranStDest,BPtLesRgnFbTranStUsage,NA90,NA91,NA92,BPtLesRgnFb2Bgan,BPtLesRgnFb2FB,BPtLesRgnFb2Gsps,BPtLesRgnFb2Fleet,BPtLesRgnFb2SwV,BPtLesRgnFb2AeroV,BPtLesRgnFb2Iridium,BPtLesRgnFb2Thu,BPtLesRgnFb2OMes,NA102,BPtLesRgnFbTranOtherServType,BPtLesRgnFbTranOtherstartDate,BPtLesRgnFbTranOtherStartTime,BPtLesRgnFbTranOtherEndTime,BPtLesRgnFbTranOtherOrig,BPtLesRgnFbTranOtherDest,BPtLesRgnFbTranOtherUsage,NA110,NA111,NA112,BPtLesRgnFbSMSInBundleId,BPtLesRgnFbSMSMSIDNId,BPtLesRgnFbSMSSimCardId,BPtLesRgnFBms,BPtLesRgnFbIsdnFax,BPtLesRgnFbTranSmsServType,BPtLesRgnFbTranSmsstartDate,BPtLesRgnFbTranSmsStartTime,BPtLesRgnFbTranSmsEndTime,BPtLesRgnFbTranSmsOrig,BPtLesRgnFbTranSmsDest,BPtLesRgnFbTranSmsUsage,NA125,BPtLesRgnFbLesRegTCId,BPtLesRgnFbLesRegTCMSIDNId,BPtLesRgnFbLesRegTCSimCardId,NA129,BPtLesRgnEndFbPenalty,BPtLesRgnFbCurBill,NA132,BPtLesRgnFbGst,BPtLesRgnFbPayTerm,NA135,NA136,NA137,NA138,NA139,BPtLesRgnFbIbvFmt,BPtLesRgnFbBarDate,BPtLesRgnFbBarDateLifted,BPtLesRgnFBusDate,BPtLesRgnFBusDateLifted,BPtLesRgnFBusMB_Lmt,BPtLesRgnFBusUS_Lmt,BPtLesRgnFblayUpDate,BPtLesRgnFbLayUpDateLifted,BPtLesRgnFbLayUpNosPerYr,BPtLesRgnFbLayUpPeriod,BPtLesRgnFbBillrecip,BPtLesRgnFbInvPWeight,BPtLesRgnFEerrAdju")] BsPtLesReg bsPtLesReg)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsPtLesReg).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bsPtLesReg);
        }

        // GET: BillingSystem/LesRegs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtLesReg bsPtLesReg = db.BsPtLesRegs.Find(id);
            if (bsPtLesReg == null)
            {
                return HttpNotFound();
            }
            return View(bsPtLesReg);
        }

        // POST: BillingSystem/LesRegs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsPtLesReg bsPtLesReg = db.BsPtLesRegs.Find(id);
            db.BsPtLesRegs.Remove(bsPtLesReg);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
