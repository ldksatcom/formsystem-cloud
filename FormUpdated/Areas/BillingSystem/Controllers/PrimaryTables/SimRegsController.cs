﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers.PrimaryTables
{
    [Authorize]
    public class SimRegsController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/SimRegs
        public ActionResult Index()
        {
            var bptFbSims = db.BptFbSims.Include(b => b.BsMtEquipType).Include(b => b.BsMtLESRef).Include(b => b.BsMtSatType).Include(b => b.BPtShip);
            return View(bptFbSims.ToList());
        }

        // GET: BillingSystem/SimRegs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BptFbSim bptFbSim = db.BptFbSims.Find(id);
            if (bptFbSim == null)
            {
                return HttpNotFound();
            }
            return View(bptFbSim);
        }

        // GET: BillingSystem/SimRegs/Create
        public ActionResult Create()
        {
            ViewBag.fkEquipTypeID = new SelectList(db.BsMtEquipTypes, "EquipTypeID", "EquipmentCode");
            ViewBag.fkLesRefID = new SelectList(db.BsMtLESRefs, "LesRefID", "LESCode");
            ViewBag.fkSatTypeID = new SelectList(db.BsMtSatTypes, "MsSatType", "Satcode");
            ViewBag.BPtfkShipID = new SelectList(db.BPtShips, "Ship_ID", "BPtShipName");
            return View();
        }

        // POST: BillingSystem/SimRegs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Sim_ID,BPtFBImImsi,BPtFBimMsisdn,BPtFBimIccid,BPtFBimLesCode,BPtFBimBillCode,BPtFBimCusCode,BPtFBimDealCode,BPtFBimShipCode,BPtFBSimFax,BPtFBimApn,BPtFBimApnUserName,BPtFBimApnPW,BPtFBSimPin1,BPtFBSimPin2,BPtFBSimPuk1,BPtFBSimPuk2,BPtFBSimlock,BPtFBSimPin,BPtFBSimRgnDate,BPtFBSimDeRgnDate,BPtFBSimActnDate,BPtFBSimdeSuspDate,BPtFBSimSuspDate,BPtFBSimBarDate,BPtFBSimDeBarDate,fkLesRefID,fkSatTypeID,fkEquipTypeID,BPtfkShipID")] BptFbSim bptFbSim)
        {
            if (ModelState.IsValid)
            {
                db.BptFbSims.Add(bptFbSim);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.fkEquipTypeID = new SelectList(db.BsMtEquipTypes, "EquipTypeID", "EquipmentCode", bptFbSim.fkEquipTypeID);
            ViewBag.fkLesRefID = new SelectList(db.BsMtLESRefs, "LesRefID", "LESCode", bptFbSim.fkLesRefID);
            ViewBag.fkSatTypeID = new SelectList(db.BsMtSatTypes, "MsSatType", "Satcode", bptFbSim.fkSatTypeID);
            ViewBag.BPtfkShipID = new SelectList(db.BPtShips, "Ship_ID", "BPtShipName", bptFbSim.BPtfkShipID);
            return View(bptFbSim);
        }

        // GET: BillingSystem/SimRegs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BptFbSim bptFbSim = db.BptFbSims.Find(id);
            if (bptFbSim == null)
            {
                return HttpNotFound();
            }
            ViewBag.fkEquipTypeID = new SelectList(db.BsMtEquipTypes, "EquipTypeID", "EquipmentCode", bptFbSim.fkEquipTypeID);
            ViewBag.fkLesRefID = new SelectList(db.BsMtLESRefs, "LesRefID", "LESCode", bptFbSim.fkLesRefID);
            ViewBag.fkSatTypeID = new SelectList(db.BsMtSatTypes, "MsSatType", "Satcode", bptFbSim.fkSatTypeID);
            ViewBag.BPtfkShipID = new SelectList(db.BPtShips, "Ship_ID", "BPtShipName", bptFbSim.BPtfkShipID);
            return View(bptFbSim);
        }

        // POST: BillingSystem/SimRegs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Sim_ID,BPtFBImImsi,BPtFBimMsisdn,BPtFBimIccid,BPtFBimLesCode,BPtFBimBillCode,BPtFBimCusCode,BPtFBimDealCode,BPtFBimShipCode,BPtFBSimFax,BPtFBimApn,BPtFBimApnUserName,BPtFBimApnPW,BPtFBSimPin1,BPtFBSimPin2,BPtFBSimPuk1,BPtFBSimPuk2,BPtFBSimlock,BPtFBSimPin,BPtFBSimRgnDate,BPtFBSimDeRgnDate,BPtFBSimActnDate,BPtFBSimdeSuspDate,BPtFBSimSuspDate,BPtFBSimBarDate,BPtFBSimDeBarDate,fkLesRefID,fkSatTypeID,fkEquipTypeID,BPtfkShipID")] BptFbSim bptFbSim)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bptFbSim).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.fkEquipTypeID = new SelectList(db.BsMtEquipTypes, "EquipTypeID", "EquipmentCode", bptFbSim.fkEquipTypeID);
            ViewBag.fkLesRefID = new SelectList(db.BsMtLESRefs, "LesRefID", "LESCode", bptFbSim.fkLesRefID);
            ViewBag.fkSatTypeID = new SelectList(db.BsMtSatTypes, "MsSatType", "Satcode", bptFbSim.fkSatTypeID);
            ViewBag.BPtfkShipID = new SelectList(db.BPtShips, "Ship_ID", "BPtShipName", bptFbSim.BPtfkShipID);
            return View(bptFbSim);
        }

        // GET: BillingSystem/SimRegs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BptFbSim bptFbSim = db.BptFbSims.Find(id);
            if (bptFbSim == null)
            {
                return HttpNotFound();
            }
            return View(bptFbSim);
        }

        // POST: BillingSystem/SimRegs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BptFbSim bptFbSim = db.BptFbSims.Find(id);
            db.BptFbSims.Remove(bptFbSim);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
