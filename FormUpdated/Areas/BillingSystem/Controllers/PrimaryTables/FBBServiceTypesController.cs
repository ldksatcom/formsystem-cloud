﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers
{
    [Authorize]
    public class FBBServiceTypesController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/FBBServiceTypes
        public ActionResult Index()
        {
            return View(db.BsFBBServiceTypes.ToList());
        }

        // GET: BillingSystem/FBBServiceTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsFBBServiceType bsFBBServiceType = db.BsFBBServiceTypes.Find(id);
            if (bsFBBServiceType == null)
            {
                return HttpNotFound();
            }
            return View(bsFBBServiceType);
        }

        // GET: BillingSystem/FBBServiceTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BillingSystem/FBBServiceTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ServiceTypeID,SMTSCode,ServiceTypes,UnityRate")] BsFBBServiceType bsFBBServiceType)
        {
            if (ModelState.IsValid)
            {
                db.BsFBBServiceTypes.Add(bsFBBServiceType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bsFBBServiceType);
        }

        // GET: BillingSystem/FBBServiceTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsFBBServiceType bsFBBServiceType = db.BsFBBServiceTypes.Find(id);
            if (bsFBBServiceType == null)
            {
                return HttpNotFound();
            }
            return View(bsFBBServiceType);
        }

        // POST: BillingSystem/FBBServiceTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ServiceTypeID,SMTSCode,ServiceTypes,UnityRate")] BsFBBServiceType bsFBBServiceType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsFBBServiceType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bsFBBServiceType);
        }

        // GET: BillingSystem/FBBServiceTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsFBBServiceType bsFBBServiceType = db.BsFBBServiceTypes.Find(id);
            if (bsFBBServiceType == null)
            {
                return HttpNotFound();
            }
            return View(bsFBBServiceType);
        }

        // POST: BillingSystem/FBBServiceTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsFBBServiceType bsFBBServiceType = db.BsFBBServiceTypes.Find(id);
            db.BsFBBServiceTypes.Remove(bsFBBServiceType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
