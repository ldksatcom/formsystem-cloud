﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers
{
    [Authorize]
    public class CusRegsController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        private ILogger logger;

        public CusRegsController() {
            logger = log.GetInstance;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            string folderPath = System.Configuration.ConfigurationManager.AppSettings["ErrorFolder"];
            folderPath = Server.MapPath(folderPath);

            logger.LogExceptions(filterContext.Exception, folderPath);

            filterContext.ExceptionHandled = true;
            this.View("Error").ExecuteResult(this.ControllerContext);
        }

        // GET: BillingSystem/CusRegs
        public ActionResult Index()
        {
            return View(db.BPtFbbCusRegs.ToList());
        }

        // GET: BillingSystem/CusRegs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BPtFbbCusReg bPtFbbCusReg = db.BPtFbbCusRegs.Find(id);
            if (bPtFbbCusReg == null)
            {
                return HttpNotFound();
            }
            return View(bPtFbbCusReg);
        }

        // GET: BillingSystem/CusRegs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BillingSystem/CusRegs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CusReg_ID,BPtCusFBimSingle,BPtCusFBimDual,BPtCusFbMultiVce,BPtCusFbContNo,BPtCusName,BPtCusAddrSt,BPtCusFbContStart,NA,NA1,BPtCusFbContEnd,BPtCusFBStartDate,BPtCuspFbEndDate,BPtCusFbTerm,BPtCusFbCardFee,BPtCusFbActFee,BPtCusFbRgnFee,NA2,NA3,NA4,NA5,NA6,NA7,NA8,BPtFbDIBInmarsatId,BPtFbDIBMSIDNId,BPtFbDIBSimId,NA9,NA10,Na11,BPtCusFbDataPlanIn,BPtCusFbVcePlanOPtCus,BPtCusFbRecurFee,BPtCusFbRecurFeeType,BPtCusFbDataRate,BPtCusFbBilIncre,BPtCusFbunitType,NA12,NA13,NA14,BPtCusFbTranDServType,BPtCusFbTranDStartDate,BPtCusFbTranDStartTime,BPtCusFbTranDEndTime,BPtCusFbTranDOrig,BPtCusFbTranDDest,BPtCusFbTranDUsage,NA15,NA16,NA17,NA18,NA19,NA20,NA21,NA22,BPtCusFbVce2Fixed,BPtCusFbVce2Cell,BPtCusFbVc2Bgan,BPtCusFbVce2Fb,BPtCusFbVce2Gpsp,BPtCusFbVce2VceMail,BPtCusFbVce2Others,BPtCusFbVcebillincre,BPtCusFbVceunitytype,NA23,BPtCusFbTranVServType,BPtCusFbTranVStartDate,BPtCusFbTranVStartTime,BPtCusFbTranVEndTime,BPtCusFbTranVOrig,BPtCusFbTranVDest,BPtCusFbTranVUsage,NA24,NA25,BPtCusFbSIBInmarsatId,BPtCusFbSIBMSIDNId,BPtCusFbSIBSimId,NA26,BPtCusFb8kbps,BPtCusFb16kbps,BPtCusFb32kbps,BPtCusFb64kbps,BPtCusFb128kbps,BPtCusFb256kbps,BPtCusFBtBillIncre,BPtCusFBtBillingUnittype,NA27,NA28,BPtCusFbTranStServType,BPtCusFbTranStStartDate,BPtCusFbTranStStartTime,BPtCusFbTranStEndTime,BPtCusFbTranStOrig,BPtCusFbTranStDest,BPtCusFbTranStUsage,NA29,NA30,NA31,BPtCusFb2Bgan,BPtCusFb2FB,BPtCusFb2Gsps,BPtCusFb2Fleet,BPtCusFb2SwV,BPtCusFb2AeroV,BPtCusFb2Iridium,BPtCusFb2Thu,BPtCusFb2OMes,NA32,BPtCusFbTranOtherServType,BPtCusFbTranOtherstartDate,BPtCusFbTranOtherStartTime,BPtCusFbTranOtherEndTime,BPtCusFbTranOtherOrig,BPtCusFbTranOtherDest,BPtCusFbTranOtherUsage,NA33,NA34,NA35,BPtCusFbSMSInmarsatId,BPtCusFbSMSMSIDNId,BPtCusFbSMSSimId,BPtCusFBSms,BPtCusFbIsdnsms,BPtCusFbTranSmsServType,BPtCusFbTranSmsstartDate,BPtCusFbTranSmsStartTime,BPtCusFbTranSmsEndTime,BPtCusFbTranSmsOrig,BPtCusFbTranSmsDest,BPtCusFbTranSmsUsage,NA36,Na37,NA38,BPtCusFbTCInmarsatId,BPtCusFbTCMSIDNId,BPtCusFbTCSimId,NA39,BPtCusEndFbPenalty,BPtCusFbCurBill,NA40,BPtCusFbGst,BPtCusFbPayTerm,NA41,NA42,BPtCusFbIbvFmt,BPtCusFbBarDate,BPtCusFbBarDateLifted,BPtCusFBSusDate,BPtCusFBusDateLifted,BPtCusFBSusMB_Lmt,BPtCusFBSusUS_Lmt,BPtCusFblayUpDate,BPtCusFbLayUpDateLifted,BPtCusFbLayUpNosPerYr,BPtCusFbLayUpPeriod,BPtCusFbBillrecip,BPtCusFbInvPWeight,BPtCusFEerrAdju")] BPtFbbCusReg bPtFbbCusReg)
        {
            if (ModelState.IsValid)
            {
                db.BPtFbbCusRegs.Add(bPtFbbCusReg);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bPtFbbCusReg);
        }

        [HttpPost]
        public ActionResult UploadFiles()
        {
            string columnname = string.Empty;

            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        string tempPath = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + fname;
                        file.SaveAs(tempPath);
                        Dictionary<string, string> Dictrejection;

                        Dictrejection = new Dictionary<string, string>();

                        BillingHelpers.connectionstring = ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString;

                        BillingHelpers helpers = new BillingHelpers();

                        helpers.CusRegExcelRead(tempPath, "CusReg");
                    }

                    // Returns message that successfully uploaded  
                    return Json("File Uploaded Successfully!", JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    string path = Server.MapPath("~/ErrorLog/ErrorLog.txt");
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }
                    BillingHelpers.LogError(ex, path);

                    //BSt1C bSt1C = new BSt1C();
                    //bSt1C.Bt1LesFbSmsIMSIId = ex.Message;
                    //bSt1C.Bt1LesFbStreamIMSIId = ex.StackTrace;
                    //db.BSt1C.Add(bSt1C);
                    //db.SaveChanges();

                    return Json("Error occurred: " + ex.Message, JsonRequestBehavior.AllowGet);
                }
                finally
                {

                }
            }
            else
            {
                return Json("No files selected", JsonRequestBehavior.AllowGet);
            }
        }

        // GET: BillingSystem/CusRegs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BPtFbbCusReg bPtFbbCusReg = db.BPtFbbCusRegs.Find(id);
            if (bPtFbbCusReg == null)
            {
                return HttpNotFound();
            }
            return View(bPtFbbCusReg);
        }

        // POST: BillingSystem/CusRegs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CusReg_ID,BPtCusFBimSingle,BPtCusFBimDual,BPtCusFbMultiVce,BPtCusFbContNo,BPtCusName,BPtCusAddrSt,BPtCusFbContStart,NA,NA1,BPtCusFbContEnd,BPtCusFBStartDate,BPtCuspFbEndDate,BPtCusFbTerm,BPtCusFbCardFee,BPtCusFbActFee,BPtCusFbRgnFee,NA2,NA3,NA4,NA5,NA6,NA7,NA8,BPtFbDIBInmarsatId,BPtFbDIBMSIDNId,BPtFbDIBSimId,NA9,NA10,Na11,BPtCusFbDataPlanIn,BPtCusFbVcePlanOPtCus,BPtCusFbRecurFee,BPtCusFbRecurFeeType,BPtCusFbDataRate,BPtCusFbBilIncre,BPtCusFbunitType,NA12,NA13,NA14,BPtCusFbTranDServType,BPtCusFbTranDStartDate,BPtCusFbTranDStartTime,BPtCusFbTranDEndTime,BPtCusFbTranDOrig,BPtCusFbTranDDest,BPtCusFbTranDUsage,NA15,NA16,NA17,NA18,NA19,NA20,NA21,NA22,BPtCusFbVce2Fixed,BPtCusFbVce2Cell,BPtCusFbVc2Bgan,BPtCusFbVce2Fb,BPtCusFbVce2Gpsp,BPtCusFbVce2VceMail,BPtCusFbVce2Others,BPtCusFbVcebillincre,BPtCusFbVceunitytype,NA23,BPtCusFbTranVServType,BPtCusFbTranVStartDate,BPtCusFbTranVStartTime,BPtCusFbTranVEndTime,BPtCusFbTranVOrig,BPtCusFbTranVDest,BPtCusFbTranVUsage,NA24,NA25,BPtCusFbSIBInmarsatId,BPtCusFbSIBMSIDNId,BPtCusFbSIBSimId,NA26,BPtCusFb8kbps,BPtCusFb16kbps,BPtCusFb32kbps,BPtCusFb64kbps,BPtCusFb128kbps,BPtCusFb256kbps,BPtCusFBtBillIncre,BPtCusFBtBillingUnittype,NA27,NA28,BPtCusFbTranStServType,BPtCusFbTranStStartDate,BPtCusFbTranStStartTime,BPtCusFbTranStEndTime,BPtCusFbTranStOrig,BPtCusFbTranStDest,BPtCusFbTranStUsage,NA29,NA30,NA31,BPtCusFb2Bgan,BPtCusFb2FB,BPtCusFb2Gsps,BPtCusFb2Fleet,BPtCusFb2SwV,BPtCusFb2AeroV,BPtCusFb2Iridium,BPtCusFb2Thu,BPtCusFb2OMes,NA32,BPtCusFbTranOtherServType,BPtCusFbTranOtherstartDate,BPtCusFbTranOtherStartTime,BPtCusFbTranOtherEndTime,BPtCusFbTranOtherOrig,BPtCusFbTranOtherDest,BPtCusFbTranOtherUsage,NA33,NA34,NA35,BPtCusFbSMSInmarsatId,BPtCusFbSMSMSIDNId,BPtCusFbSMSSimId,BPtCusFBSms,BPtCusFbIsdnsms,BPtCusFbTranSmsServType,BPtCusFbTranSmsstartDate,BPtCusFbTranSmsStartTime,BPtCusFbTranSmsEndTime,BPtCusFbTranSmsOrig,BPtCusFbTranSmsDest,BPtCusFbTranSmsUsage,NA36,Na37,NA38,BPtCusFbTCInmarsatId,BPtCusFbTCMSIDNId,BPtCusFbTCSimId,NA39,BPtCusEndFbPenalty,BPtCusFbCurBill,NA40,BPtCusFbGst,BPtCusFbPayTerm,NA41,NA42,BPtCusFbIbvFmt,BPtCusFbBarDate,BPtCusFbBarDateLifted,BPtCusFBSusDate,BPtCusFBusDateLifted,BPtCusFBSusMB_Lmt,BPtCusFBSusUS_Lmt,BPtCusFblayUpDate,BPtCusFbLayUpDateLifted,BPtCusFbLayUpNosPerYr,BPtCusFbLayUpPeriod,BPtCusFbBillrecip,BPtCusFbInvPWeight,BPtCusFEerrAdju")] BPtFbbCusReg bPtFbbCusReg)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bPtFbbCusReg).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bPtFbbCusReg);
        }

        // GET: BillingSystem/CusRegs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BPtFbbCusReg bPtFbbCusReg = db.BPtFbbCusRegs.Find(id);
            if (bPtFbbCusReg == null)
            {
                return HttpNotFound();
            }
            return View(bPtFbbCusReg);
        }

        // POST: BillingSystem/CusRegs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BPtFbbCusReg bPtFbbCusReg = db.BPtFbbCusRegs.Find(id);
            db.BPtFbbCusRegs.Remove(bPtFbbCusReg);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
