﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers
{
    [Authorize]
    public class CusContractsController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/CusContracts
        public ActionResult Index()
        {
            var bsPtCusContracts = db.BsPtCusContracts.Include(b => b.BsMtCustomer);
            return View(bsPtCusContracts.ToList());
        }

        // GET: BillingSystem/CusContracts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtCusContract bsPtCusContract = db.BsPtCusContracts.Find(id);
            if (bsPtCusContract == null)
            {
                return HttpNotFound();
            }
            return View(bsPtCusContract);
        }

        // GET: BillingSystem/CusContracts/Create
        public ActionResult Create()
        {
            ViewBag.fkCusID = new SelectList(db.BsMtCustomers, "CusID", "Customers");
            return View();
        }

        // POST: BillingSystem/CusContracts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "cuscID,fkCusID,BsPtCusFbContNo,BsPtCusFbContStart,BsPtCusFbContEnd,BsPtCusFBstartDate,BsPtCusFBendDate,BsPtCusFBTerm,BsPtCusFBCardFee,BsPtCusFBActFee,BsPtCusFBRgnFee,BsPtCusFBBarDate,BsPtCusFbBarDateLifted,BsPtCusFBSusDate,BsPtCusFBSusDateLifted,BsPtCusFBSusMB_Lmt,BsPtCusFBSusUS_Lmt,BsPtCusFBLayUpDate,BsPtCusFBLayUpDateLifted,BsPtCusFBLayUpNosPerYr,BsPtCusFBLayUpPeriod")] BsPtCusContract bsPtCusContract)
        {
            if (ModelState.IsValid)
            {
                db.BsPtCusContracts.Add(bsPtCusContract);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.fkCusID = new SelectList(db.BsMtCustomers, "CusID", "Customers", bsPtCusContract.fkCusID);
            return View(bsPtCusContract);
        }

        // GET: BillingSystem/CusContracts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtCusContract bsPtCusContract = db.BsPtCusContracts.Find(id);
            if (bsPtCusContract == null)
            {
                return HttpNotFound();
            }
            ViewBag.fkCusID = new SelectList(db.BsMtCustomers, "CusID", "Customers", bsPtCusContract.fkCusID);
            return View(bsPtCusContract);
        }

        // POST: BillingSystem/CusContracts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "cuscID,fkCusID,BsPtCusFbContNo,BsPtCusFbContStart,BsPtCusFbContEnd,BsPtCusFBstartDate,BsPtCusFBendDate,BsPtCusFBTerm,BsPtCusFBCardFee,BsPtCusFBActFee,BsPtCusFBRgnFee,BsPtCusFBBarDate,BsPtCusFbBarDateLifted,BsPtCusFBSusDate,BsPtCusFBSusDateLifted,BsPtCusFBSusMB_Lmt,BsPtCusFBSusUS_Lmt,BsPtCusFBLayUpDate,BsPtCusFBLayUpDateLifted,BsPtCusFBLayUpNosPerYr,BsPtCusFBLayUpPeriod")] BsPtCusContract bsPtCusContract)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsPtCusContract).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.fkCusID = new SelectList(db.BsMtCustomers, "CusID", "Customers", bsPtCusContract.fkCusID);
            return View(bsPtCusContract);
        }

        // GET: BillingSystem/CusContracts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtCusContract bsPtCusContract = db.BsPtCusContracts.Find(id);
            if (bsPtCusContract == null)
            {
                return HttpNotFound();
            }
            return View(bsPtCusContract);
        }

        // POST: BillingSystem/CusContracts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsPtCusContract bsPtCusContract = db.BsPtCusContracts.Find(id);
            db.BsPtCusContracts.Remove(bsPtCusContract);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
