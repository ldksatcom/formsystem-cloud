﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers
{
    [Authorize]
    public class CusRegs_Controller : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/CusRegs_
        public ActionResult Index()
        {
            var bsPtCusRegs_ = db.BsPtCusRegs_.Include(b => b.BptFbSim).Include(b => b.BsMtCustomer).Include(b => b.BsMtUnit).Include(b => b.BsPtCusContract);
            return View(bsPtCusRegs_.ToList());
        }

        // GET: BillingSystem/CusRegs_/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtCusRegs_ bsPtCusRegs_ = db.BsPtCusRegs_.Find(id);
            if (bsPtCusRegs_ == null)
            {
                return HttpNotFound();
            }
            return View(bsPtCusRegs_);
        }

        // GET: BillingSystem/CusRegs_/Create
        public ActionResult Create()
        {
            ViewBag.fkSimID = new SelectList(db.BptFbSims, "Sim_ID", "BPtFBImImsi");
            ViewBag.fkCusID = new SelectList(db.BsMtCustomers, "CusID", "Customers");
            ViewBag.BsPtCusRegDataRateUnitType = new SelectList(db.BsMtUnits, "UID", "Units");
            ViewBag.fkCuscID = new SelectList(db.BsPtCusContracts, "cuscID", "BsPtCusFbContNo");
            return View();
        }

        // POST: BillingSystem/CusRegs_/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Cus_RegID,fkCusID,fkCuscID,fkSimID,BsPtCusRegFbContNo,BsPtCusRegStartdate,BsPtCusRegEnd,BsPtCusRegFBTerm,BsPtCusRegFBCardFee,BsPtCusRegFBActFee,BsPtCusRegFBRgnFee,BPtCusRgnFBMultiVce,BPtCusRgnFBDataPlanIn,BPtCusRgnFbVcePlanOPtLesRgn,BPtCusRgnFbRecurFee,BPtCusRgnFbRecurFeeType,BPtCusRgnFbInbundleDataRate,BPtCusRgnFbOutBundleDataRate,BsPtCusRegDataRateUnitType, BsPtCusAddr, BsPtCusRegFBBarDate,BsPtCusRegFbBarDateLifted,BsPtCusRegFBSusDate,BsPtCusRegFBSusDateLifted,BsPtCusRegFBSusMB_Lmt,BsPtCusRegFBSusUS_Lmt,BsPtCusRegFBLayUpDate,BsPtCusRegFBLayUpDateLifted,BsPtCusRegFBLayUpNosPerYr,BsPtCusRegFBLayUpPeriod")] BsPtCusRegs_ bsPtCusRegs_)
        {
            if (ModelState.IsValid)
            {
                db.BsPtCusRegs_.Add(bsPtCusRegs_);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.fkSimID = new SelectList(db.BptFbSims, "Sim_ID", "BPtFBImImsi", bsPtCusRegs_.fkSimID);
            ViewBag.fkCusID = new SelectList(db.BsMtCustomers, "CusID", "Customers", bsPtCusRegs_.fkCusID);
            ViewBag.BsPtCusRegDataRateUnitType = new SelectList(db.BsMtUnits, "UID", "Units", bsPtCusRegs_.BsPtCusRegDataRateUnitType);
            ViewBag.fkCuscID = new SelectList(db.BsPtCusContracts, "cuscID", "BsPtCusFbContNo", bsPtCusRegs_.fkCuscID);
            return View(bsPtCusRegs_);
        }

        // GET: BillingSystem/CusRegs_/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtCusRegs_ bsPtCusRegs_ = db.BsPtCusRegs_.Find(id);
            if (bsPtCusRegs_ == null)
            {
                return HttpNotFound();
            }
            ViewBag.fkSimID = new SelectList(db.BptFbSims, "Sim_ID", "BPtFBImImsi", bsPtCusRegs_.fkSimID);
            ViewBag.fkCusID = new SelectList(db.BsMtCustomers, "CusID", "Customers", bsPtCusRegs_.fkCusID);
            ViewBag.BsPtCusRegDataRateUnitType = new SelectList(db.BsMtUnits, "UID", "Units", bsPtCusRegs_.BsPtCusRegDataRateUnitType);
            ViewBag.fkCuscID = new SelectList(db.BsPtCusContracts, "cuscID", "BsPtCusFbContNo", bsPtCusRegs_.fkCuscID);
            return View(bsPtCusRegs_);
        }

        // POST: BillingSystem/CusRegs_/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Cus_RegID,fkCusID,fkCuscID,fkSimID,BsPtCusRegFbContNo,BsPtCusRegStartdate,BsPtCusRegEnd,BsPtCusRegFBTerm,BsPtCusRegFBCardFee,BsPtCusRegFBActFee,BsPtCusRegFBRgnFee,BPtCusRgnFBMultiVce,BPtCusRgnFBDataPlanIn,BPtCusRgnFbVcePlanOPtLesRgn,BPtCusRgnFbRecurFee,BPtCusRgnFbRecurFeeType,BPtCusRgnFbInbundleDataRate,BPtCusRgnFbOutBundleDataRate,BsPtCusRegDataRateUnitType,BsPtCusAddr,BsPtCusRegFBBarDate,BsPtCusRegFbBarDateLifted,BsPtCusRegFBSusDate,BsPtCusRegFBSusDateLifted,BsPtCusRegFBSusMB_Lmt,BsPtCusRegFBSusUS_Lmt,BsPtCusRegFBLayUpDate,BsPtCusRegFBLayUpDateLifted,BsPtCusRegFBLayUpNosPerYr,BsPtCusRegFBLayUpPeriod")] BsPtCusRegs_ bsPtCusRegs_)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsPtCusRegs_).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.fkSimID = new SelectList(db.BptFbSims, "Sim_ID", "BPtFBImImsi", bsPtCusRegs_.fkSimID);
            ViewBag.fkCusID = new SelectList(db.BsMtCustomers, "CusID", "Customers", bsPtCusRegs_.fkCusID);
            ViewBag.BsPtCusRegDataRateUnitType = new SelectList(db.BsMtUnits, "UID", "Units", bsPtCusRegs_.BsPtCusRegDataRateUnitType);
            ViewBag.fkCuscID = new SelectList(db.BsPtCusContracts, "cuscID", "BsPtCusFbContNo", bsPtCusRegs_.fkCuscID);
            return View(bsPtCusRegs_);
        }

        // GET: BillingSystem/CusRegs_/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtCusRegs_ bsPtCusRegs_ = db.BsPtCusRegs_.Find(id);
            if (bsPtCusRegs_ == null)
            {
                return HttpNotFound();
            }
            return View(bsPtCusRegs_);
        }

        // POST: BillingSystem/CusRegs_/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsPtCusRegs_ bsPtCusRegs_ = db.BsPtCusRegs_.Find(id);
            db.BsPtCusRegs_.Remove(bsPtCusRegs_);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
