﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem.Controllers
{
    [Authorize]
    public class CusServiceTypesController : Controller
    {
        private BillingSystemEntities db = new BillingSystemEntities();

        // GET: BillingSystem/CusServiceTypes
        public ActionResult Index()
        {
            var bsPtCusServiceDests = db.BsPtCusServiceDests.Include(b => b.BsMtCommunicationType).Include(b => b.BsMtCustomer).Include(b => b.BsMtServiceProvider).Include(b => b.BsMtUnit).Include(b => b.BsPtCusRegs_);
            return View(bsPtCusServiceDests.ToList());
        }

        // GET: BillingSystem/CusServiceTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtCusServiceDest bsPtCusServiceDest = db.BsPtCusServiceDests.Find(id);
            if (bsPtCusServiceDest == null)
            {
                return HttpNotFound();
            }
            return View(bsPtCusServiceDest);
        }

        // GET: BillingSystem/CusServiceTypes/Create
        public ActionResult Create()
        {
            ViewBag.ComTypeID = new SelectList(db.BsMtCommunicationTypes, "ComID", "ComTypes");
            ViewBag.fkCusID = new SelectList(db.BsMtCustomers, "CusID", "Customers");
            ViewBag.fkSpID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider");
            ViewBag.fkUnitsType = new SelectList(db.BsMtUnits, "UID", "Units");
            ViewBag.BPtCusServfkCusRegID = new SelectList(db.BsPtCusRegs_, "Cus_RegID", "BsPtCusRegFbContNo");
            return View();
        }

        // POST: BillingSystem/CusServiceTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CusServDID,fkSpID,BsPtCusServiceTypes,fkUnitsType,BsPtCusServiceSellerPrice,BsPtCusServDescription,ComTypeID,BsPtCusServAsDescription,fkCusID,BPtCusServfkCusRegID")] BsPtCusServiceDest bsPtCusServiceDest)
        {
            if (ModelState.IsValid)
            {
                db.BsPtCusServiceDests.Add(bsPtCusServiceDest);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ComTypeID = new SelectList(db.BsMtCommunicationTypes, "ComID", "ComTypes", bsPtCusServiceDest.ComTypeID);
            ViewBag.fkCusID = new SelectList(db.BsMtCustomers, "CusID", "Customers", bsPtCusServiceDest.fkCusID);
            ViewBag.fkSpID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider", bsPtCusServiceDest.fkSpID);
            ViewBag.fkUnitsType = new SelectList(db.BsMtUnits, "UID", "Units", bsPtCusServiceDest.fkUnitsType);
            ViewBag.BPtCusServfkCusRegID = new SelectList(db.BsPtCusRegs_, "Cus_RegID", "BsPtCusRegFbContNo", bsPtCusServiceDest.BPtCusServfkCusRegID);
            return View(bsPtCusServiceDest);
        }

        // GET: BillingSystem/CusServiceTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtCusServiceDest bsPtCusServiceDest = db.BsPtCusServiceDests.Find(id);
            if (bsPtCusServiceDest == null)
            {
                return HttpNotFound();
            }
            ViewBag.ComTypeID = new SelectList(db.BsMtCommunicationTypes, "ComID", "ComTypes", bsPtCusServiceDest.ComTypeID);
            ViewBag.fkCusID = new SelectList(db.BsMtCustomers, "CusID", "Customers", bsPtCusServiceDest.fkCusID);
            ViewBag.fkSpID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider", bsPtCusServiceDest.fkSpID);
            ViewBag.fkUnitsType = new SelectList(db.BsMtUnits, "UID", "Units", bsPtCusServiceDest.fkUnitsType);
            ViewBag.BPtCusServfkCusRegID = new SelectList(db.BsPtCusRegs_, "Cus_RegID", "BsPtCusRegFbContNo", bsPtCusServiceDest.BPtCusServfkCusRegID);
            return View(bsPtCusServiceDest);
        }

        // POST: BillingSystem/CusServiceTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CusServDID,fkSpID,BsPtCusServiceTypes,fkUnitsType,BsPtCusServiceSellerPrice,BsPtCusServDescription,ComTypeID,BsPtCusServAsDescription,fkCusID,BPtCusServfkCusRegID")] BsPtCusServiceDest bsPtCusServiceDest)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bsPtCusServiceDest).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ComTypeID = new SelectList(db.BsMtCommunicationTypes, "ComID", "ComTypes", bsPtCusServiceDest.ComTypeID);
            ViewBag.fkCusID = new SelectList(db.BsMtCustomers, "CusID", "Customers", bsPtCusServiceDest.fkCusID);
            ViewBag.fkSpID = new SelectList(db.BsMtServiceProviders, "spID", "ServiceProvider", bsPtCusServiceDest.fkSpID);
            ViewBag.fkUnitsType = new SelectList(db.BsMtUnits, "UID", "Units", bsPtCusServiceDest.fkUnitsType);
            ViewBag.BPtCusServfkCusRegID = new SelectList(db.BsPtCusRegs_, "Cus_RegID", "BsPtCusRegFbContNo", bsPtCusServiceDest.BPtCusServfkCusRegID);
            return View(bsPtCusServiceDest);
        }

        // GET: BillingSystem/CusServiceTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BsPtCusServiceDest bsPtCusServiceDest = db.BsPtCusServiceDests.Find(id);
            if (bsPtCusServiceDest == null)
            {
                return HttpNotFound();
            }
            return View(bsPtCusServiceDest);
        }

        // POST: BillingSystem/CusServiceTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BsPtCusServiceDest bsPtCusServiceDest = db.BsPtCusServiceDests.Find(id);
            db.BsPtCusServiceDests.Remove(bsPtCusServiceDest);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
