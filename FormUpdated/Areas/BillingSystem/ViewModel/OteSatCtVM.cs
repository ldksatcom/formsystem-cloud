﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FormUpdated.Areas.BillingSystem
{
    public class OteSatCtVM
    {
        public int BctID { get; set; }
        public string BCtFbbLES { get; set; }
        public string BCtFBImImsi { get; set; }
        public string BCtFbContNo { get; set; }
        public string BCtFbCdrCode { get; set; }
        public Nullable<int> BCtFbLesRefCode { get; set; }
        public Nullable<int> BCtFbSatCode { get; set; }
        public Nullable<int> BCtFbEquipmentCode { get; set; }
        public Nullable<decimal> BCtFbRecurFee { get; set; }
        public string BCtAFbRecurFeeType { get; set; }
        public Nullable<System.DateTime> BCtTranStartDate { get; set; }
        public Nullable<System.DateTime> BCtTranEndDate { get; set; }
        public Nullable<System.TimeSpan> BCtTranStartTime { get; set; }
        public string BCtTranOrigin { get; set; }
        public string BCtTranServType { get; set; }
        public string BCtTranServDest { get; set; }
        public Nullable<decimal> BCtTranUnitUsage { get; set; }
        public string BCtTranUnitType { get; set; }
        public decimal? BCtTranTotalUsage { get; set; }
        public Nullable<int> BCtTranComTypes { get; set; }
        public Nullable<decimal> BCtTranBillIncr { get; set; }
        public string BCtFbBillNo { get; set; }
        public string BCtFbBillPeriod { get; set; }
        public Nullable<System.DateTime> BCtFbBillDate { get; set; }
        public string BCtFbDualSim { get; set; }
        public Nullable<System.DateTime> BCtFbContStartDate { get; set; }
        public Nullable<System.DateTime> BCtFbContEndDate { get; set; }
        public string BCtFbTerm { get; set; }
        public Nullable<decimal> BCtFbRgnFee { get; set; }
        public Nullable<decimal> BCtFbActFee { get; set; }
        public Nullable<decimal> BCtFbCardFee { get; set; }
    }

    public class UploadFiles
    {
        public int id { get; set; }
        public string Filename { get; set; }
        public string location { get; set; }    
    }
}