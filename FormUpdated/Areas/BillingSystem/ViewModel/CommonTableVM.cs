﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FormUpdated.Areas.BillingSystem
{
    public class CommonTableVM
    {
        public int BctID { get; set; }
        public string BCtFbMSIDNNo { get; set; }
        public string BCtFbICCID { get; set; }
        public string BCtFbImImsi { get; set; }
        public string NA { get; set; }
        public string NA0 { get; set; }
        public string NA1 { get; set; }
        public string NA2 { get; set; }
        public string BCtFbbLES { get; set; }
        public string BCtFbContNo { get; set; }
        public string BCtFbCdrCode { get; set; }
        public string NA3 { get; set; }
        public string NA4 { get; set; }
        public Nullable<int> BCtFbLesRefCode { get; set; }
        public Nullable<int> BCtFbSatCode { get; set; }
        public Nullable<int> BCtFbEquipmentCode { get; set; }
        public string NA5 { get; set; }
        public string NA6 { get; set; }
        public Nullable<decimal> BCtFbRecurFee { get; set; }
        public string BCtAFbRecurFeeType { get; set; }
        public string NA7 { get; set; }
        public string NA8 { get; set; }
        public string NA9 { get; set; }
        public string NA10 { get; set; }
        public Nullable<System.DateTime> BCtTranStartDate { get; set; }
        public Nullable<System.DateTime> BCtTranEndDate { get; set; }
        public Nullable<System.TimeSpan> BCtTranStartTime { get; set; }
        public string NA11 { get; set; }
        public string BCtTranOrigin { get; set; }
        public string NA12 { get; set; }
        public string BCtTranServType { get; set; }
        public string BCtTranServDest { get; set; }
        public string NA13 { get; set; }
        public string NA14 { get; set; }
        public Nullable<decimal> BCtTranUnitUsage { get; set; }
        public string BCtTranUnitType { get; set; }
        public Nullable<decimal> BCtTranTotalUsage { get; set; }
        public string NA15 { get; set; }
        public string BCtTranComTypes { get; set; }
        public Nullable<decimal> BCtTranBillIncr { get; set; }
        public string NA16 { get; set; }
        public string NA17 { get; set; }
        public string NA18 { get; set; }
        public string NA19 { get; set; }
        public string NA20 { get; set; }
        public string BCtFbBillNo { get; set; }
        public string BCtFbBillPeriod { get; set; }
        public Nullable<System.DateTime> BCtFbBillDate { get; set; }
        public string NA21 { get; set; }
        public string NA22 { get; set; }
        public Nullable<bool> BCtFbSIMSingle { get; set; }
        public Nullable<bool> BCtFbDualSim { get; set; }
        public Nullable<bool> BCtFbMultiVce { get; set; }
        public string NA23 { get; set; }
        public string NA24 { get; set; }
        public Nullable<System.DateTime> BCtFbContStartDate { get; set; }
        public Nullable<System.DateTime> BCtFbContEndDate { get; set; }
        public string NA25 { get; set; }
        public string NA26 { get; set; }
        public string BCtFbTerm { get; set; }
        public Nullable<decimal> BCtFbRgnFee { get; set; }
        public Nullable<decimal> BCtFbActFee { get; set; }
        public Nullable<decimal> BCtFbCardFee { get; set; }
        public string NA27 { get; set; }
        public string NA28 { get; set; }
        public string NA29 { get; set; }
        public string NA30 { get; set; }
    }

    public class ErrorDetails
    {
        public string ErrorCode { get; set; }
        public string Descriptions { get; set; }
        public string CT { get; set; }
        public string LES { get; set; }
        public string Details { get; set; }
        public string ToBill { get; set; }
        public string ToReject { get; set; }
    }
}
