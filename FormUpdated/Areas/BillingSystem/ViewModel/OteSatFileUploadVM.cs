﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FormUpdated.Areas.BillingSystem.ViewModel
{
    public class OteSatFileUploadVM
    {
        public int ID { get; set; }

        [Required(ErrorMessage ="Please choose LES References Type.")]
        [Display(Name ="LES References")]
        public int LesRefID { get; set; }

        [Display(Name ="OteSat CDR File")]
        [Required(ErrorMessage ="Please select OteSat CDR file.")]
        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.xlsx)$", ErrorMessage ="Only Excel(.xlsx) files allowed.")]
        public HttpPostedFileBase CDRExcelFile { get; set; }
    }
}