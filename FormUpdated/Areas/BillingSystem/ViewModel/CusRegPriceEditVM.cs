﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FormUpdated.Areas.BillingSystem.ViewModel
{
    public class CusRegPriceEditVM
    {
        public int CusRegID { get; set; }
        public int CusID { get; set; }
        public int ShipID { get; set; }
        public int SimID { get; set; }

        public string Customer { get; set; }
        public string Vessel { get; set; }
        public string InmarsatID { get; set; }
        public string ImsiID { get; set; }
        public string IccID { get; set; }

        [Required(ErrorMessage ="Please Enter Dataplan")]
        public int Dataplan { get; set; }

        public decimal? Recurfee { get; set; }
        public decimal? InbundleRate { get; set; }
        public decimal? OutbundleRate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }
        
        public decimal? VoicetoCellular { get; set; }
        public decimal? VoicetoFixed { get; set; }
        public decimal? VoicetoFleetBroadband { get; set; }
        public decimal? VoicetoVoicemail { get; set; }
        public decimal? VoicetoIridium { get; set; }
        public decimal? ISDN { get; set; }
        public decimal? SMS { get; set; }
        public decimal? VoicetoBegan { get; set; }
        public decimal? VoicetoGSPS { get; set; }
        public decimal? StandardIP { get; set; }


        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? BarDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? BarDateLifted { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? SusDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? SusDateLifted { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LayupDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LayupDateLifted { get; set; }
    }
}