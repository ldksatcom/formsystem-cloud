﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FormUpdated.Areas.BillingSystem.ViewModel
{
    public class LesRegPriceEditVM
    {
        //public int ID { get; set; }
        public int LesRegID { get; set; }
        public int CusID { get; set; }
        public int ShipID { get; set; }
        public int SimID { get; set; }

        public string Customers { get; set; }
        public string Vessel { get; set; }
        public string InMarsatID { get; set; }
        public string IMSIID { get; set; }

        [Required(ErrorMessage ="Please Enter Dataplan")]
        public int DataPlan { get; set; }
        public decimal? RecurFee { get; set; }
        public decimal? InBundleRate { get; set; }
        public decimal? OutBundleRate { get; set; }

        
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        //[DataType(DataType.Date)]
        public DateTime? StartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }
        public decimal? VoicetoCellular { get; set; }
        public decimal? VoicetoFixed { get; set; }
        public decimal? VoicetoFleetBroadBand { get; set; }
        public decimal? VoicetoVoicemail { get; set; }
        public decimal? VoicetoIridium { get; set; }
        public decimal? ISDN { get; set; }
        public decimal? SMS { get; set; }
        public decimal? VoicetoBegan { get; set; }
        public decimal? VoicetoGSPS { get; set; }
        public decimal? StandardIP { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? BarDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? BarDateLifted { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? SusDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? susDateLifted { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LayupDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LayupDateLifted { get; set; }
    }
}