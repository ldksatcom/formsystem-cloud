﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FormUpdated.Areas.BillingSystem
{
    public class otesatcdrVM
    {
        public string VesselName { get; set; }
        public string IMNIMSI { get; set; }
        public string System { get; set; }
        public DateTime? Date { get; set; }
        public TimeSpan? Time { get; set; }
        public string OceanRegion { get; set; }
        public string TimeZone { get; set; }
        public string ServiceType { get; set; }
        public string MRN { get; set; }
        public string Country { get; set; }
        public string Destination { get; set; }
        public decimal? Units { get; set; }
        public string UnitsType { get; set; }
        public decimal? AmountDueUSD { get; set; }
    }
}