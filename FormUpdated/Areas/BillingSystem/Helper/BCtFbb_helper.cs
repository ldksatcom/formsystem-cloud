﻿using ClosedXML.Excel;
using FormUpdated.Areas.BillingSystem.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;

namespace FormUpdated.Areas.BillingSystem
{
    public class helper_BctFbb
    {
        public static string LesName { get; set; } = "OteSat";
        public int LesID { get; set; } = getLesID();

        public static int getLesID()
        {
            try
            {
                using (BillingSystemEntities context = new BillingSystemEntities())
                {
                    return context.BsMtServiceProviders.FirstOrDefault(t => t.ServiceProvider.Equals(LesName)).spID;
                }
            }
            catch
            {
                return 0;
            }
        }

        public Tuple<string> getLes(int lesrefid)
        {
            int lesid = 0;
            string lesName = string.Empty;

            using (BillingSystemEntities context = new BillingSystemEntities())
            {
                lesName = context.BsMtLESRefs.FirstOrDefault(t => t.LesRefID == lesrefid).BsMtServiceProvider.ServiceProvider;
            }

            return new Tuple<string>(lesName);
        }

        public Tuple<Stream, string, string, string, List<ErrorDetails>, string> CDRtoCT(string fileName, int lesrefID)
        {
            var les = getLes(lesrefID);
            List<ErrorDetails> lstmsg = new List<ErrorDetails>();
            if (!string.IsNullOrEmpty(les.Item1))
            {
                switch (les.Item1)
                {
                    case "IMARSAT":
                        helper_ImarSat imarSat = new helper_ImarSat();
                        Tuple<string, List<OteSatCtVM>> tuplesInmarsat = new Tuple<string, List<OteSatCtVM>>(string.Empty, new List<OteSatCtVM>());
                        string msgInmarsat = string.Empty;

                        if (Path.GetExtension(fileName) == ".csv")
                        {
                            List<OteSatCtVM> lstVM = helper_ImarSat.CsvToViewModel(fileName, lesrefID);

                            //tuplesInmarsat = NItoCT_BulkCopy(lstVM);

                            //msg = oteSat.RejectionOtesat_Speed("OteSat", tuplesInmarsat.Item1);
                            //msg = oteSat.RejectionOtesat_New("OteSat", tupleslst.Item1);
                        }
                        else if (Path.GetExtension(fileName) == ".xlsx")
                        {

                        }
                        //var spreadsheetInmarSat = WriteCTRecords2Excel(tuplesInmarsat.Item2);
                        //return new Tuple<Stream, string, string, string>(spreadsheetInmarSat.Item1, spreadsheetInmarSat.Item2, msgInmarsat, les.Item1);
                        break;
                    case "NSSL GLOBAL":
                        break;
                    case "OteSat":

                        Tuple<string, List<CommonTableVM>> tupleslst = new Tuple<string, List<CommonTableVM>>(string.Empty, new List<CommonTableVM>());
                        string msg = string.Empty;
                        string code = string.Empty;
                        using (BillingSystemEntities db = new BillingSystemEntities())
                        {
                            code = db.BsMtLESRefs.FirstOrDefault(t => t.LesRefID == lesrefID).LESCode;
                        }
                        if (code.Equals("LOte004"))
                        {
                            if (Path.GetExtension(fileName) == ".csv")
                            {
                                List<OteSatCtVM> lstVM = helper_OteSat.CsvToVMFleetOne(fileName, lesrefID);

                                //tupleslst = //oteSat.OteSat2CT_model(lstVM, lesrefID);

                                //msg = oteSat.RejectionOtesat_Speed("OteSat", tupleslst.Item1);
                                //msg = oteSat.RejectionOtesat_New("OteSat", tupleslst.Item1);
                            }
                            else if (Path.GetExtension(fileName) == ".xlsx")
                            {

                            }
                            // var spreadsheet = //oteSat.GenreateCTExcelFleetOne(tupleslst.Item2);
                            // return new Tuple<Stream, string, string, string>(spreadsheet.Item1, spreadsheet.Item2, msg, les.Item1);
                        }
                        else
                        {
                            if (Path.GetExtension(fileName) == ".csv")
                            {
                                List<CommonTableVM> lstCTVM = CsvToVMBctFbb(fileName, lesrefID);

                                var otesatFbbTuple = BctFbbVMtoCT(lstCTVM, lesrefID);

                                var retmsg = RejectionOtesat_BctFbb("OteSat", tupleslst.Item1);
                                //msg = oteSat.RejectionOtesat_New("OteSat", tupleslst.Item1);

                                var spreadsheet = WriteExcelFromBCtFbb(otesatFbbTuple.Item2);
                                return new Tuple<Stream, string, string, string, List<ErrorDetails>, string>(spreadsheet.Item1, spreadsheet.Item2, msg, les.Item1, retmsg.Item1, retmsg.Item2);
                            }
                            else if (Path.GetExtension(fileName) == ".xlsx")
                            {

                            }

                        }

                        break;
                    case "NI":
                        Tuple<string, List<OteSatCtVM>> tuplesNI = new Tuple<string, List<OteSatCtVM>>(string.Empty, new List<OteSatCtVM>());

                        if (Path.GetExtension(fileName) == ".csv")
                        {
                            //List<OteSatCtVM> lstVM = CsvToDictionary(fileName, lesrefID);

                            //tuplesNI = NItoCT_BulkCopy(lstVM);
                        }
                        else if (Path.GetExtension(fileName) == ".xlsx")
                        {

                        }
                        //var spreadsheetNI = WriteCTRecords2Excel(tuplesNI.Item2);
                        //return new Tuple<Stream, string, string, string>(spreadsheetNI.Item1, spreadsheetNI.Item2, tuplesNI.Item1, les.Item1);
                        break;
                    case "EvoSat":
                        break;
                }
            }

            Stream stream = null;
            return new Tuple<Stream, string, string, string, List<ErrorDetails>, string>(stream, string.Empty, string.Empty, string.Empty, lstmsg, string.Empty);
        }

        public static Tuple<string, List<CommonTableVM>> testmethod(List<CommonTableVM> lstVm, int id)
        {
            List<CommonTableVM> lstCom = lstVm;
            Tuple<string, List<CommonTableVM>> ls = new Tuple<string, List<CommonTableVM>>("", lstCom);
            return ls;
        }

        public static List<CommonTableVM> CsvToVMBctFbb(string csv_file_path, int lesrefid)
        {
            List<CommonTableVM> lstVM = new List<CommonTableVM>();
            try
            {
                List<string> columnFields = new List<string>();
                using (Microsoft.VisualBasic.FileIO.TextFieldParser csvReader = new Microsoft.VisualBasic.FileIO.TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields = csvReader.ReadFields();
                    foreach (string column in colFields)
                    {
                        string columnName = string.Empty;
                        if (column.StartsWith(" "))
                        {
                            columnName = column.Substring(1);
                            columnName = columnName.TrimEnd();
                        }
                        columnName = column.TrimEnd();
                        columnFields.Add(System.Text.RegularExpressions.Regex.Replace(columnName, @"\t|\n|\r", ""));
                    }

                    Dictionary<string, dynamic> dictRecord = new Dictionary<string, dynamic>();

                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        dictRecord = new Dictionary<string, dynamic>();

                        CommonTableVM vM = new CommonTableVM();

                        //Making empty value as null
                        for (int i = 0, colIndex = 0; i < fieldData.Length; i++, colIndex++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = string.Empty;
                            }
                            vM = storeCollection_BCtFbb(columnFields[colIndex], fieldData[i], vM, lesrefid);
                        }
                        if (vM.BCtTranServType != string.Empty && vM.BCtTranServType != "" && !string.IsNullOrEmpty(vM.BCtTranServType))
                            lstVM.Add(vM);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstVM;
        }

        public static CommonTableVM storeCollection_BCtFbb(string index, string value, CommonTableVM vum, int lesrefid)
        {
            try
            {
                switch (index)
                {
                    case "Vessel Name":
                        //vum.VesselName = value;
                        vum.BCtFbbLES = "OteSat";
                        break;
                    case "IMN / IMSI":
                        vum.BCtFbImImsi = value;
                        break;
                    case "System":
                        if (!string.IsNullOrEmpty(value) && value.Contains("Inmarsat-FleetBroadband"))
                        {
                            vum.BCtFbLesRefCode = lesrefid;
                            vum.BCtFbEquipmentCode = 3;
                            vum.BCtFbSatCode = 1;
                        }
                        else if (!string.IsNullOrEmpty(value) && value.Contains("Inmarsat-FleetOne"))
                        {
                            vum.BCtFbLesRefCode = lesrefid;
                            vum.BCtFbEquipmentCode = 3;
                            vum.BCtFbSatCode = 1;
                        }
                        else if (!string.IsNullOrEmpty(value) && value.Contains("Inmarsat-GSPS"))
                        {
                            vum.BCtFbLesRefCode = lesrefid;
                            vum.BCtFbEquipmentCode = 3;
                            vum.BCtFbSatCode = 1;
                        }
                        break;
                    case "Date":
                        if (!string.IsNullOrEmpty(value))
                        {
                            DateTime val;
                            bool result = DateTime.TryParse(value, out val);
                            if (result)
                                vum.BCtTranStartDate = val;
                            else
                                vum.BCtTranStartDate = null;
                        }
                        break;
                    case "Time":
                        if (!string.IsNullOrEmpty(value))
                        {
                            TimeSpan val;
                            bool result = TimeSpan.TryParse(value, out val);
                            if (result)
                                vum.BCtTranStartTime = val;
                            else
                                vum.BCtTranStartTime = null;
                        }
                        break;
                    case "Ocean Region":
                        //vum.OceanRegion = value;
                        break;
                    case "Time Zone":
                        //vum.TimeZone = value;
                        break;
                    case "Service Type":
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (value.ToLower().Contains("discount"))
                                vum.BCtTranServType = value;
                            else
                            {
                                vum.BCtTranServType = value;
                            }
                        }
                        else
                            vum.BCtTranServType = null;

                        break;
                    case "MRN":
                        //vum.MRN = value;
                        break;
                    case "Country":
                        //vum.Country = value;
                        break;
                    case "Destination":
                        vum.BCtTranServDest = value;
                        break;
                    case "Units":
                        if (!string.IsNullOrEmpty(value))
                        {
                            decimal val = 0;
                            bool result = decimal.TryParse(value, out val);
                            if (result)
                            {
                                if (!string.IsNullOrEmpty(vum.BCtTranServType) && vum.BCtTranServType.Equals("Standard IP"))
                                {
                                    long dat = Convert.ToInt64(val);
                                    double ip = Converters.ConvertKilobytesToBit(dat);
                                    double bytes = 0;
                                    bool res = double.TryParse(ip.ToString(), out bytes);
                                    if (res)
                                    {
                                        vum.BCtTranUnitUsage = Convert.ToDecimal(ip);
                                    }
                                    else
                                    {
                                        vum.BCtTranUnitUsage = val;
                                    }
                                }
                                else
                                {
                                    vum.BCtTranUnitUsage = val;
                                }
                            }
                            else
                            {
                                vum.BCtTranUnitUsage = null;
                            }
                        }
                        break;
                    case "Units Type":
                        if (!string.IsNullOrEmpty(vum.BCtTranServType) && vum.BCtTranServType.Equals("Standard IP"))
                        {
                            vum.BCtTranUnitType = "Bits";
                        }
                        else
                        {
                            vum.BCtTranUnitType = value;
                        }
                        break;
                    case "Amount Due (USD)":
                        if (!string.IsNullOrEmpty(value))
                        {
                            decimal val = 0;
                            bool result = decimal.TryParse(value, out val);
                            if (result)
                            {
                                if (!string.IsNullOrEmpty(vum.BCtTranServType) && vum.BCtTranServType.Contains("Subscription Fee"))
                                    vum.BCtFbRecurFee = val;
                                else
                                    vum.BCtTranTotalUsage = val;
                            }
                            else
                                vum.BCtTranTotalUsage = null;
                        }
                        break;
                }
            }
            catch (Exception rer)
            {
                throw rer;
            }
            return vum;
        }

        public Tuple<string, List<CommonTableVM>> BctFbbVMtoCT(List<CommonTableVM> lstVM, int LesRefID)
        {
            string msg = string.Empty;
            try
            {
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["AdoConnstr"].ConnectionString))
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();

                    using (var bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.Default, transaction))
                    {
                        bulkCopy.BatchSize = 100;
                        bulkCopy.DestinationTableName = "dbo.BCtFbb";
                        try
                        {
                            var stopwatch = new Stopwatch();
                            stopwatch.Start();

                            bulkCopy.WriteToServer(lstVM.AsDataTable());

                            stopwatch.Stop();
                            msg += "Time: " + stopwatch.ElapsedMilliseconds + " ";
                        }
                        catch (Exception e)
                        {
                            transaction.Rollback();
                            connection.Close();
                            msg = "faild to store common table";
                            Tuple<string, List<CommonTableVM>> lstTuple1 = new Tuple<string, List<CommonTableVM>>(msg, lstVM);
                            return lstTuple1;
                        }
                    }

                    transaction.Commit();
                    msg = "successfully store common table";
                    Tuple<string, List<CommonTableVM>> lstTuple2 = new Tuple<string, List<CommonTableVM>>(msg, lstVM);
                    return lstTuple2;
                }
            }
            catch (Exception error)
            {
                msg = "faild to store common table eorro: " + error.Message.ToString();
                Tuple<string, List<CommonTableVM>> lstTuple3 = new Tuple<string, List<CommonTableVM>>(msg, lstVM);
                return lstTuple3;
            }
        }

        public Tuple<List<ErrorDetails>, string> RejectionOtesat_BctFbb(string bctles, string errorEsg)
        {
            System.Text.StringBuilder returnMsg = new System.Text.StringBuilder();
            returnMsg.Append(errorEsg).AppendLine();

            System.Text.StringBuilder Msg = new System.Text.StringBuilder();
            System.Text.StringBuilder htmlString = new System.Text.StringBuilder();
            htmlString.AppendLine("<table class='table table - striped' id='tbl'>");
            htmlString.AppendLine(" <tr> <th> ErrorCode </th> <th> Description </th>  <th> CT </th> <th> LES </th> <th> Details </th> <th> ToBill </th> <th> ToReject </th> </tr> ");

            var lstError = new List<ErrorDetails>();

            // Create new stopwatch.
            Stopwatch stopwatch = new Stopwatch();
            // Begin timing.
            stopwatch.Start();

            List<BCtFbb> lstCtRecord = new List<BCtFbb>();
            List<BsPtSPServiceDest> lstLES_ServiceType = new List<BsPtSPServiceDest>();

            List<BSt1FbbTrans> lstRejecttbl = new List<BSt1FbbTrans>();
            List<BSt2FbbTrans> lstInvoicetbl = new List<BSt2FbbTrans>();

            using (BillingSystemEntities db = new BillingSystemEntities())
            {
                try
                {
                    lstCtRecord = db.BCtFbbs.Where(t => t.BCtFbbLES.Equals(bctles)).ToList();

                    var lstSubscripRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("Subscription Fee")).ToList();
                    var lstStdIPRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("Standard IP")).ToList();
                    var lstV2CellRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("Voice  to Cellular")).ToList();
                    var lstV2FixedRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("Voice  to Fixed")).ToList();
                    var lstV2FBBRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("Voice  to FleetBroadband")).ToList();
                    var lstV2IdridumRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("Voice  to Iridium")).ToList();
                    var lstISDNRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("ISDN")).ToList();
                    var lstSMSRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("SMS")).ToList();

                    var lstCusRegs = db.BsPtCusRegs_.ToList();
                    var lstLesRegs = db.BsPtSPLesRegs.ToList();
                    var LES_Services = db.BsPtSPServiceDests.ToList();
                    var lstUnityType = db.BsMtUnits.ToList();

                    if (lstSubscripRecords.Count > 0)
                    {
                        int i = 1;
                        foreach (var CT_Fbs in lstSubscripRecords)
                        {
                            var emsg = new ErrorDetails();
                            
                            string ImarSatID = string.Empty;
                            try
                            {
                                #region Varaiable declarations

                                string ImsiID = string.Empty;
                                int SimID = 0;
                                decimal? CDR_RecuFee = 0;

                                decimal? SMTS_TotalCost = 0;
                                decimal? Les_TotalCostCP = 0;
                                decimal? Cus_TotalCostSP = 0;

                                int? LES_ContractID = 0;
                                int LES_RegID = 0;

                                decimal? LES_DataPlan = 0;
                                decimal? LES_OutbundleRate = 0;
                                decimal? LES_RecurFee = 0;
                                decimal? LES_DataRateper1MB = 0;
                                decimal? LES_VoiceCellular = 0;
                                decimal? LES_VoiceFixed = 0;
                                decimal? LES_VoiceFBB = 0;
                                decimal? LES_VoiceIridium = 0;
                                decimal? LES_ISDN = 0;
                                decimal? LES_SMS = 0;

                                decimal? LES_InvRptDataCostTot = 0;

                                int CUS_Id = 0;
                                int CUS_ContractID = 0;
                                int CUS_RegsID = 0;

                                decimal? Cus_DataPlan = 0;
                                decimal? Cus_OutbundleRate = 0;
                                decimal? CUS_RecurFee = 0;
                                decimal? CUS_DataRateper1MB = 0;
                                decimal? CUS_VoiceCellular = 0;
                                decimal? CUS_VoiceFixed = 0;
                                decimal? CUS_VoiceFBB = 0;
                                decimal? CUS_VoiceIridium = 0;
                                decimal? CUS_ISDN = 0;
                                decimal? CUS_SMS = 0;

                                DateTime? LES_ContrRegStrDate = null;
                                DateTime? LES_ContrRegEndDate = null;

                                DateTime? LES_ContrBarStrDate = null;
                                DateTime? LES_ContrBarEndDate = null;

                                DateTime? LES_ContrSusStrDate = null;
                                DateTime? LES_ContrSusEndDate = null;

                                DateTime? LES_ContrLayoutStrDate = null;
                                DateTime? LES_ContrLayoutEndDate = null;

                                DateTime? CUS_ContrRegStrDate = null;
                                DateTime? CUS_ContrRegEndDate = null;

                                DateTime? CUS_ContrBarStrDate = null;
                                DateTime? CUS_ContrBarEndDate = null;

                                DateTime? CUS_ContrSusStrDate = null;
                                DateTime? CUS_ContrSusEndDate = null;

                                DateTime? CUS_ContrLayoutStrDate = null;
                                DateTime? CUS_ContrLayoutEndDate = null;

                                DateTime? CDR_StartDate = null;
                                DateTime? CDR_EndDate = null;

                                bool is_LESGBPlan = false;
                                bool is_CUSGBPlan = false;
                                bool isNoTransData = false;

                                int ServTypeID = 0;
                                int ServTypeMobile = 0;
                                int ServTypeVce2Fixed = 0;
                                int ServTypeVce2FBB = 0;
                                int LES_ServTypeVce2Iridium = 0;
                                int ServTypeVce2ISDN = 0;
                                int ServTypeSMS = 0;

                                int CUS_ServTypeID = 0;
                                int CUS_ServTypeMobile = 0;
                                int CUS_ServTypeVce2Fixed = 0;
                                int CUS_ServTypeVce2FBB = 0;
                                int CUS_ServTypeVce2Iridium = 0;
                                int CUS_ServTypeVce2ISDN = 0;
                                int CUS_ServTypeSMS = 0;

                                bool isLES_ContrRegDate = false;
                                bool isLES_ContrBarDate = false;
                                bool isLES_ContrSusDate = false;
                                bool isLES_ContrLayUpDate = false;

                                bool isCUS_ContrRegDate = false;
                                bool isCUS_ContrBarDate = false;
                                bool isCUS_ContrSusDate = false;
                                bool isCUS_ContrLayUpDate = false;

                                #endregion

                                var tblSim = db.BptFbSims.FirstOrDefault(t => t.BPtFBImImsi.Equals(CT_Fbs.BCtFbImImsi));

                                var lstNotSubscripRecords = lstCtRecord.Where(t => t.BCtFbImImsi.Equals(CT_Fbs.BCtFbImImsi)).ToList();

                                if (tblSim != null)
                                {
                                    ImarSatID = tblSim.BPtFBimMsisdn;
                                    SimID = tblSim.Sim_ID;
                                }
                                else
                                {
                                    emsg.ErrorCode = "Err00" + i;
                                    emsg.Descriptions = "MSISDN ID no ship table/ Les Registration table";
                                    emsg.CT = "BCtFBImImsi";
                                    emsg.LES = "BPtLesRgnFbDataIMSIId";
                                    emsg.Details = string.Format("This IMSI id {0} is not registered in our database.", CT_Fbs.BCtFbImImsi);
                                    emsg.ToReject = "Yes";
                                    lstError.Add(emsg);

                                    htmlString.AppendFormat("<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> <td>{5}</td> <td>{6}</td> </tr>", emsg.ErrorCode, emsg.Descriptions, emsg.CT, emsg.LES, emsg.Details, emsg.ToBill, emsg.ToReject).AppendLine();
                                }

                                CDR_RecuFee = CT_Fbs.BCtFbRecurFee;

                                if (lstNotSubscripRecords.Count > 0)
                                {
                                    CDR_StartDate = lstNotSubscripRecords.Min(t => t.BCtTranStartDate);
                                    CDR_EndDate = lstNotSubscripRecords.Max(t => t.BCtTranStartDate);
                                }
                                else
                                    isNoTransData = true;

                                var lstStdIP_Trans = lstStdIPRecords.Where(t => t.BCtFbImImsi.Equals(CT_Fbs.BCtFbImImsi)).ToList();
                                var lstV2Cell_Trans = lstV2CellRecords.Where(t => t.BCtFbImImsi.Equals(CT_Fbs.BCtFbImImsi)).ToList();
                                var lstV2Fixed_Trans = lstV2FixedRecords.Where(t => t.BCtFbImImsi.Equals(CT_Fbs.BCtFbImImsi)).ToList();
                                var lstV2Fbb_Trans = lstV2FBBRecords.Where(t => t.BCtFbImImsi.Equals(CT_Fbs.BCtFbImImsi)).ToList();
                                var lstV2Iridium_Trans = lstV2IdridumRecords.Where(t => t.BCtFbImImsi.Equals(CT_Fbs.BCtFbImImsi)).ToList();
                                var lstISDN_Trans = lstISDNRecords.Where(t => t.BCtFbImImsi.Equals(CT_Fbs.BCtFbImImsi)).ToList();
                                var lstSMS_Trans = lstSMSRecords.Where(t => t.BCtFbImImsi.Equals(CT_Fbs.BCtFbImImsi)).ToList();

                                #region get Cus Register tbl info

                                var cusRegs = lstCusRegs.FirstOrDefault(t => t.fkSimID == SimID);
                                if (cusRegs != null)
                                {
                                    CUS_Id = Convert.ToInt32(cusRegs.fkCusID);
                                    CUS_ContractID = Convert.ToInt32(cusRegs.fkCuscID);
                                    CUS_RegsID = Convert.ToInt32(cusRegs.Cus_RegID);

                                    CUS_RecurFee = cusRegs.BPtCusRgnFbRecurFee;

                                    Cus_DataPlan = Convert.ToDecimal(cusRegs.BPtCusRgnFBDataPlanIn);

                                    CUS_DataRateper1MB = cusRegs.BPtCusRgnFbInbundleDataRate;
                                    Cus_OutbundleRate = cusRegs.BPtCusRgnFbOutBundleDataRate;

                                    CUS_ContrRegStrDate = cusRegs.BsPtCusRegStartdate;
                                    CUS_ContrRegEndDate = cusRegs.BsPtCusRegEnd;

                                    CUS_ContrBarStrDate = cusRegs.BsPtCusRegFBBarDate;
                                    CUS_ContrBarEndDate = cusRegs.BsPtCusRegFbBarDateLifted;

                                    CUS_ContrSusStrDate = cusRegs.BsPtCusRegFBSusDate;
                                    CUS_ContrSusEndDate = cusRegs.BsPtCusRegFBSusDateLifted;

                                    CUS_ContrLayoutStrDate = cusRegs.BsPtCusRegFBLayUpDate;
                                    CUS_ContrLayoutEndDate = cusRegs.BsPtCusRegFBLayUpDateLifted;

                                    string unit = cusRegs.BsMtUnit.Units;
                                    if (unit.Contains("GB"))
                                        is_CUSGBPlan = true;
                                    else
                                        is_CUSGBPlan = false;
                                }
                                else
                                {
                                    Msg.AppendFormat("This msi {0} id not registered in customer registered, please check.<br />", ImarSatID).AppendLine();
                                }

                                List<BsPtCusServiceDest> lstSP = db.BsPtCusServiceDests.Where(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID).ToList();

                                try
                                {
                                    CUS_VoiceCellular = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                             t.BsPtCusServiceTypes.Equals("Voice  to Cellular")).BsPtCusServiceSellerPrice;
                                }
                                catch { CUS_VoiceCellular = 0; }

                                try
                                {
                                    CUS_VoiceFixed = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                          t.BsPtCusServiceTypes.Equals("Voice  to Fixed")).BsPtCusServiceSellerPrice;
                                }
                                catch { CUS_VoiceFixed = 0; }

                                try
                                {
                                    CUS_VoiceFBB = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                        t.BsPtCusServiceTypes.Equals("Voice  to FleetBroadband")).BsPtCusServiceSellerPrice;
                                }
                                catch { CUS_VoiceFBB = 0; }

                                try
                                {
                                    CUS_VoiceIridium = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                            t.BsPtCusServiceTypes.Equals("Voice  to Iridium")).BsPtCusServiceSellerPrice;
                                }
                                catch { CUS_VoiceIridium = 0; }

                                try
                                {
                                    CUS_ISDN = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                    t.BsPtCusServiceTypes.Equals("ISDN")).BsPtCusServiceSellerPrice;
                                }
                                catch { CUS_ISDN = 0; }

                                try
                                {
                                    CUS_SMS = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                   t.BsPtCusServiceTypes.Equals("SMS")).BsPtCusServiceSellerPrice;
                                }
                                catch { CUS_SMS = 0; }
                                #endregion

                                #region GET LES Details
                                try
                                {
                                    try
                                    {
                                        var Les_Reg = lstLesRegs.FirstOrDefault(t => t.fkSPID == LesID && t.fkSimID == SimID);
                                        //var Les_Reg = db.BsPtSPLesRegs.FirstOrDefault(t => t.SPLesRegID == CT_Fbs.BCtAfkLesRegID);

                                        LES_RegID = Les_Reg.SPLesRegID;
                                        LES_ContractID = Les_Reg.fkspcID;

                                        LES_RecurFee = Les_Reg.BPtLesRgnFbRecurFee;
                                        LES_DataPlan = Convert.ToDecimal(Les_Reg.BPtLesRgnFBDataPlanIn);
                                        LES_DataRateper1MB = Les_Reg.BPtLesRgnFbInbundleDataRate;
                                        LES_OutbundleRate = Les_Reg.BPtLesRgnFbOutBundleDataRate;

                                        LES_ContrRegStrDate = Les_Reg.BsPtlesRegStartdate;
                                        LES_ContrRegEndDate = Les_Reg.BsPtlesRegEnd;

                                        LES_ContrBarStrDate = Les_Reg.BsPtLesRegFBBarDate;
                                        LES_ContrBarEndDate = Les_Reg.BsPtLesRegFbBarDateLifted;

                                        LES_ContrSusStrDate = Les_Reg.BsPtLesRegFBSusDate;
                                        LES_ContrSusEndDate = Les_Reg.BsPtLesRegFBSusDateLifted;

                                        LES_ContrLayoutStrDate = Les_Reg.BsPtLesRegFBLayUpDate;
                                        LES_ContrLayoutEndDate = Les_Reg.BsPtLesRegFBLayUpDateLifted;

                                        string unit = Les_Reg.BsMtUnit.Units;
                                        if (unit.Contains("GB"))
                                            is_LESGBPlan = true;
                                        else
                                            is_LESGBPlan = false;
                                    }
                                    catch { LES_RecurFee = 0; }

                                    lstLES_ServiceType = (from ls in LES_Services
                                                          where ls.fkSpID == LesID && ls.BPtServTfkLesRegID == LES_RegID
                                                          select ls).ToList();

                                    try
                                    {
                                        LES_VoiceCellular = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                 t.BsPtServiceTypes.Equals("Voice  to Cellular")).BsPtServiceCostPrice;
                                    }
                                    catch { LES_VoiceCellular = 0; }

                                    try
                                    {
                                        LES_VoiceFixed = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                              t.BsPtServiceTypes.Equals("Voice  to Fixed")).BsPtServiceCostPrice;
                                    }
                                    catch { LES_VoiceFixed = 0; }

                                    try
                                    {
                                        LES_VoiceFBB = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                            t.BsPtServiceTypes.Equals("Voice  to FleetBroadband")).BsPtServiceCostPrice;
                                    }
                                    catch { LES_VoiceFBB = 0; }

                                    try
                                    {
                                        LES_VoiceIridium = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                t.BsPtServiceTypes.Equals("Voice  to Iridium")).BsPtServiceCostPrice;
                                    }
                                    catch { LES_VoiceIridium = 0; }

                                    try
                                    {
                                        LES_ISDN = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                        t.BsPtServiceTypes.Equals("ISDN")).BsPtServiceCostPrice;
                                    }
                                    catch { LES_ISDN = 0; }

                                    try
                                    {
                                        LES_SMS = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                       t.BsPtServiceTypes.Equals("SMS")).BsPtServiceCostPrice;
                                    }
                                    catch { LES_SMS = 0; }
                                }
                                catch (Exception e)
                                {
                                    Msg.AppendFormat("Error:{0}", e.Message.ToString());
                                }
                                //-------------------------------------------------------------------------------------------------------------------------------
                                #endregion

                                #region Standard IP

                                try
                                {
                                    ServTypeID = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                        t.BsPtServiceTypes.Equals("Standard IP")).SPServDID;
                                }
                                catch
                                {
                                    ServTypeID = 0;
                                    Msg.Append("The Standard IP is Registed in the LES Service Description<br />.").AppendLine();
                                }

                                try
                                {
                                    CUS_ServTypeID = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                            t.BsPtCusServiceTypes.Equals("Standard IP")).CusServDID;
                                }
                                catch
                                {
                                    CUS_ServTypeID = 0;
                                    Msg.Append("The Standard IP is Registed in the CUS Service Description<br />.").AppendLine();
                                }

                                decimal? DataUnits_CDR = null;
                                decimal? CDR_DataCP = null;
                                if (lstStdIP_Trans.Count > 0)
                                {
                                    DataUnits_CDR = lstStdIP_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    DataUnits_CDR = Math.Round(Convert.ToDecimal(DataUnits_CDR), 2, MidpointRounding.AwayFromZero);

                                    CDR_DataCP = lstCtRecord.AsEnumerable().Sum(s => s.BCtTranTotalUsage);

                                    CDR_DataCP = Math.Round(Convert.ToDecimal(CDR_DataCP), 2, MidpointRounding.AwayFromZero);
                                }

                                SMTS_TotalCost += CDR_DataCP;

                                #endregion

                                #region Basic voice - Cellular

                                // <--------------------------- Basic voice - Mobile  ----------------------------->
                                try
                                {
                                    ServTypeMobile = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                            t.BsPtServiceTypes.Equals("Voice  to Cellular")).SPServDID;
                                }
                                catch { }

                                try
                                {
                                    CUS_ServTypeMobile = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                t.BsPtCusServiceTypes.Equals("Voice  to Cellular")).CusServDID;
                                }
                                catch { }

                                decimal? CDR_VceCellularUintsCT = null;
                                decimal? CDR_VceCellularCostPriceCT = null;

                                if (lstV2Cell_Trans.Count > 0)
                                {
                                    CDR_VceCellularUintsCT = lstV2Cell_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    if (CDR_VceCellularUintsCT != null)
                                        CDR_VceCellularUintsCT = Math.Round(Convert.ToDecimal(CDR_VceCellularUintsCT), 2, MidpointRounding.AwayFromZero);

                                    CDR_VceCellularCostPriceCT = lstV2Cell_Trans.AsEnumerable().Sum(s => s.BCtTranTotalUsage);

                                    if (CDR_VceCellularCostPriceCT != null)
                                        CDR_VceCellularCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceCellularCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                }

                                SMTS_TotalCost += CDR_VceCellularCostPriceCT;
                                // <--------------------------- End Basic voice -FBB/Cellular  ----------------------------->
                                #endregion

                                #region Basic Voice  to Fixed

                                // <--------------------------- Basic Voice  to Fixed  ----------------------------->
                                try
                                {
                                    ServTypeVce2Fixed = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID
                                                                                                && t.BsPtServiceTypes.Equals("Voice  to Fixed")).SPServDID;
                                }
                                catch { }

                                try
                                {
                                    CUS_ServTypeVce2Fixed = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                    t.BsPtCusServiceTypes.Equals("Voice  to Fixed")).CusServDID;
                                }
                                catch { }

                                decimal? CDR_VceFixedUintsCT = null;
                                decimal? CDR_VceFixedCostPriceCT = null;

                                if (lstV2Fixed_Trans.Count > 0)
                                {
                                    CDR_VceFixedUintsCT = lstV2Fixed_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    if (CDR_VceFixedUintsCT != null)
                                        CDR_VceFixedUintsCT = Math.Round(Convert.ToDecimal(CDR_VceFixedUintsCT), 2, MidpointRounding.AwayFromZero);

                                    CDR_VceFixedCostPriceCT = lstV2Fixed_Trans.AsEnumerable().Sum(s => s.BCtTranTotalUsage);

                                    if (CDR_VceFixedCostPriceCT != null)
                                        CDR_VceFixedCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceFixedCostPriceCT), 2, MidpointRounding.AwayFromZero);

                                    SMTS_TotalCost += CDR_VceFixedCostPriceCT;
                                }
                                // <--------------------------- End Basic Voice  to Fixed  ----------------------------->

                                #endregion

                                #region Voice to FleetBroadband
                                // <--------------------------- Voice to FleetBroadband  ----------------------------->

                                try
                                {
                                    ServTypeVce2FBB = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                            t.BsPtServiceTypes.Equals("Voice to FleetBroadband")).SPServDID;
                                }
                                catch
                                {
                                    ServTypeVce2FBB = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                                t.BsPtServiceTypes.Equals("Voice  to FleetBroadband")).SPServDID;
                                }

                                try
                                {
                                    CUS_ServTypeVce2FBB = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                t.BsPtCusServiceTypes.Equals("Voice to FleetBroadband")).CusServDID;
                                }
                                catch
                                {
                                    CUS_ServTypeVce2FBB = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                t.BsPtCusServiceTypes.Equals("Voice  to FleetBroadband")).CusServDID;
                                }

                                decimal? CDR_VceFBBUintsCT = null;
                                decimal? CDR_VceFBBCostPriceCT = null;

                                if (lstV2Fbb_Trans.Count > 0)
                                {
                                    CDR_VceFBBUintsCT = lstV2Fbb_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    if (CDR_VceFBBUintsCT != null)
                                        CDR_VceFBBUintsCT = Math.Round(Convert.ToDecimal(CDR_VceFBBUintsCT), 2, MidpointRounding.AwayFromZero);

                                    CDR_VceFBBCostPriceCT = lstV2Fbb_Trans.AsEnumerable().Sum(s => s.BCtTranTotalUsage);

                                    if (CDR_VceFBBCostPriceCT != null)
                                        CDR_VceFBBCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceFBBCostPriceCT), 2, MidpointRounding.AwayFromZero);

                                    SMTS_TotalCost += CDR_VceFBBCostPriceCT;
                                }
                                // <--------------------------- End Voice to FleetBroadband  ----------------------------->
                                #endregion

                                #region Voice  to Iridium
                                // <--------------------------- Voice  to Iridium  ----------------------------->
                                try
                                {
                                    LES_ServTypeVce2Iridium = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                                   t.BsPtServiceTypes.Equals("Voice  to Iridium")).SPServDID;
                                }
                                catch { }
                                try
                                {
                                    CUS_ServTypeVce2Iridium = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                    t.BsPtCusServiceTypes.Equals("Voice  to Iridium")).CusServDID;
                                }
                                catch { }

                                decimal? CDR_VceIridiumUintsCT = null;
                                decimal? CDR_VceIridiumCostPriceCT = null;

                                if (lstV2Iridium_Trans.Count > 0)
                                {
                                    CDR_VceIridiumUintsCT = lstV2Iridium_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    if (CDR_VceIridiumUintsCT != null)
                                        CDR_VceIridiumUintsCT = Math.Round(Convert.ToDecimal(CDR_VceIridiumUintsCT), 2, MidpointRounding.AwayFromZero);

                                    CDR_VceIridiumCostPriceCT = lstV2Iridium_Trans.AsEnumerable().Sum(s => s.BCtTranTotalUsage);

                                    if (CDR_VceIridiumCostPriceCT != null)
                                        CDR_VceIridiumCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceIridiumCostPriceCT), 2, MidpointRounding.AwayFromZero);

                                    SMTS_TotalCost += CDR_VceIridiumCostPriceCT;
                                }
                                // <--------------------------- End Voice  to Iridium  ----------------------------->
                                #endregion

                                #region ISDN
                                // <--------------------------- ISDN  ----------------------------->
                                try
                                {
                                    ServTypeVce2ISDN = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                                    t.BsPtServiceTypes.Equals("ISDN")).SPServDID;
                                }
                                catch { }
                                try
                                {
                                    CUS_ServTypeVce2ISDN = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                    t.BsPtCusServiceTypes.Equals("ISDN")).CusServDID;
                                }
                                catch { }

                                decimal? CDR_ISDNUintsCT = null;
                                decimal? CDR_ISDNCostPriceCT = null;
                                if (lstISDN_Trans.Count > 0)
                                {
                                    CDR_ISDNUintsCT = lstISDN_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    if (CDR_ISDNUintsCT != null)
                                        CDR_ISDNUintsCT = Math.Round(Convert.ToDecimal(CDR_ISDNUintsCT), 2, MidpointRounding.AwayFromZero);

                                    CDR_ISDNCostPriceCT = lstISDN_Trans.AsEnumerable().Sum(s => s.BCtTranTotalUsage);

                                    if (CDR_ISDNCostPriceCT != null)
                                        CDR_ISDNCostPriceCT = Math.Round(Convert.ToDecimal(CDR_ISDNCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                }
                                SMTS_TotalCost += CDR_ISDNCostPriceCT;
                                // <--------------------------- End ISDN  ----------------------------->
                                #endregion

                                #region SMS
                                // <--------------------------- ISDN  ----------------------------->
                                try
                                {
                                    ServTypeSMS = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals("SMS")).SPServDID;
                                }
                                catch { }
                                try
                                {
                                    CUS_ServTypeSMS = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("SMS")).CusServDID;
                                }
                                catch { }

                                decimal? CDR_SMSUintsCT = null;
                                decimal? CDR_SMSCostPriceCT = null;
                                if (lstSMS_Trans.Count > 0)
                                {
                                    CDR_SMSUintsCT = lstSMS_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    if (CDR_SMSUintsCT != null)
                                        CDR_SMSUintsCT = Math.Round(Convert.ToDecimal(CDR_SMSUintsCT), 2, MidpointRounding.AwayFromZero);

                                    CDR_SMSCostPriceCT = lstSMS_Trans.AsEnumerable().Sum(s => s.BCtTranTotalUsage);


                                    if (CDR_SMSCostPriceCT != null)
                                        CDR_SMSCostPriceCT = Math.Round(Convert.ToDecimal(CDR_SMSCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                }

                                SMTS_TotalCost += CDR_SMSCostPriceCT;
                                // <--------------------------- End ISDN  ----------------------------->
                                #endregion

                                bool isRecurFeeMached = false;
                                bool isDataMatched = false;
                                bool isVoiceCellurlarMatched = false;
                                bool isVoiceFixedMatched = false;
                                bool isVoiceFbbMatched = false;
                                bool isVoiceIridiumMatched = false;
                                bool isISDNMatched = false;
                                bool isSMSMatched = false;

                                bool isOutbundleUsed = false;
                                decimal? OutbundleUsedData = 0;
                                decimal? CDR_OutbundleRate = 0;

                                Les_TotalCostCP += LES_RecurFee;
                                SMTS_TotalCost += CDR_RecuFee;

                                // && CDR_RecuFee < CUS_RecurFee
                                if ((CDR_RecuFee < LES_RecurFee || CDR_RecuFee.Equals(LES_RecurFee)))
                                {
                                    isRecurFeeMached = true;
                                }
                                else
                                {
                                    isRecurFeeMached = false;

                                    emsg = new ErrorDetails();
                                    emsg.ErrorCode = "Err00" + i++;
                                    emsg.Descriptions = string.Format("{0} Subscription Fee rate  more that LES Registration", CT_Fbs.BCtFbImImsi);
                                    emsg.CT = "BCtFbRecurFee";
                                    emsg.LES = "BPtLesRgnFbRecurFee";
                                    emsg.Details = string.Format("CDR Recur Fee is {0} and LES Reg Recur Fee is {1}.", CDR_RecuFee, LES_RecurFee);
                                    emsg.ToReject = "Yes";
                                    
                                    lstError.Add(emsg);

                                    htmlString.AppendFormat("<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> <td>{5}</td> <td>{6}</td> </tr>", emsg.ErrorCode, emsg.Descriptions, emsg.CT, emsg.LES, emsg.Details, emsg.ToBill, emsg.ToReject).AppendLine();

                                    //Msg.AppendFormat("This Inmarsat id {0}: Recuring Feee is Less then LES/CUS Recur Fee.<br />", ImarSatID).AppendLine();
                                    //Msg.AppendFormat("Recuring Feee from LES CDR Price:{0}.<br />", CDR_RecuFee).AppendLine();
                                    //Msg.AppendFormat("Recuring Feee from LES Reg CostPrice:{0}.<br />", LES_RecurFee).AppendLine();
                                    //Msg.AppendFormat("Recuring Feee from Cus Reg SellerPrice:{0}.<br />", CUS_RecurFee).AppendLine();
                                    //returnMsg.AppendFormat("This Inmarsat id {0}: Recuring Feee is Less then LES/CUS Recur Fee.<br />", ImarSatID).AppendLine();
                                }

                                #region Standard IP rate calculation
                                if (DataUnits_CDR != null)
                                {
                                    long data = Convert.ToInt64(Convert.ToDecimal(DataUnits_CDR));
                                    decimal? CDR_DataTotalUsageMB = (decimal)Converters.ConvertKilobytesToMegabytes(data);

                                    if (CDR_DataTotalUsageMB != null)
                                        CDR_DataTotalUsageMB = Math.Round(Convert.ToDecimal(CDR_DataTotalUsageMB), 2, MidpointRounding.AwayFromZero);

                                    if (is_LESGBPlan)
                                    {
                                        LES_DataPlan = LES_DataPlan * 1024;
                                    }

                                    if (LES_DataPlan < CDR_DataTotalUsageMB)
                                    {
                                        isOutbundleUsed = true;
                                        OutbundleUsedData = CDR_DataTotalUsageMB - LES_DataPlan;
                                        OutbundleUsedData = Math.Round(Convert.ToDecimal(OutbundleUsedData), 2, MidpointRounding.AwayFromZero);
                                    }

                                    decimal? LES_DataCP = CDR_DataTotalUsageMB * LES_DataRateper1MB;
                                    decimal? Cus_DataSP = CDR_DataTotalUsageMB * CUS_DataRateper1MB;
                                    decimal? LES_OutbundleDataCP = OutbundleUsedData * LES_OutbundleRate;
                                    decimal? Cus_OutbundleDataSP = OutbundleUsedData * Cus_OutbundleRate;

                                    if (Cus_DataSP != null)
                                        Cus_DataSP = Math.Round(Convert.ToDecimal(Cus_DataSP), 2, MidpointRounding.AwayFromZero);

                                    if (LES_DataCP != null)
                                        LES_DataCP = Math.Round(Convert.ToDecimal(LES_DataCP), 2, MidpointRounding.AwayFromZero);

                                    if (LES_OutbundleDataCP != null)
                                        LES_OutbundleDataCP = Math.Round(Convert.ToDecimal(LES_OutbundleDataCP), 2, MidpointRounding.AwayFromZero);

                                    if (Cus_OutbundleDataSP != null)
                                        Cus_OutbundleDataSP = Math.Round(Convert.ToDecimal(Cus_OutbundleDataSP), 2, MidpointRounding.AwayFromZero);

                                    if (isOutbundleUsed)
                                    {
                                        CDR_OutbundleRate = CDR_DataCP - CDR_RecuFee;
                                        CDR_OutbundleRate = Math.Round(Convert.ToDecimal(CDR_OutbundleRate), 2, MidpointRounding.AwayFromZero);

                                        //if ((CDR_DataCP < LES_OutbundleDataCP || CDR_DataCP.Equals(LES_OutbundleDataCP)) && (CDR_DataCP < Cus_OutbundleDataSP))
                                        if ((CDR_OutbundleRate < LES_OutbundleDataCP || CDR_OutbundleRate.Equals(LES_OutbundleDataCP)) && (CDR_OutbundleRate < Cus_OutbundleDataSP))
                                        {
                                            isDataMatched = true;
                                            Les_TotalCostCP += LES_OutbundleDataCP;
                                            Cus_TotalCostSP += Cus_OutbundleDataSP;
                                        }
                                        else
                                        {
                                            isDataMatched = false;

                                            emsg = new ErrorDetails();
                                            emsg.ErrorCode = "Err00" + i++;
                                            emsg.Descriptions = string.Format("{0} ID outbundle charge more than les reg table", CT_Fbs.BCtFbImImsi);
                                            emsg.CT = "BCtTranTotalUsage";
                                            emsg.LES = "BPtLesRgnFbDataOutBundle";
                                            emsg.Details = string.Format("CDR outbundle rate is {0} and LES Reg outbundle rate is {1}.", CDR_OutbundleRate, LES_OutbundleDataCP);
                                            emsg.ToReject = "Yes";
                                            
                                            lstError.Add(emsg);
                                            htmlString.AppendFormat("<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> <td>{5}</td> <td>{6}</td> </tr>", emsg.ErrorCode, emsg.Descriptions, emsg.CT, emsg.LES, emsg.Details, emsg.ToBill, emsg.ToReject).AppendLine();
                                            //Msg.AppendFormat("This Inmarsat id {0}: Data of Background IP data rate is not match.<br />", ImarSatID).AppendLine();
                                            //Msg.AppendFormat("Outbundle Data Rate from LES CDR Price:{0}.<br />", CDR_OutbundleRate).AppendLine();
                                            //Msg.AppendFormat("Outbundle Data Rate from LES Reg CostPrice:{0}.<br />", LES_OutbundleDataCP).AppendLine();
                                            //Msg.AppendFormat("Outbundle Data Rate from Cus Reg SellerPrice:{0}.<br />", Cus_OutbundleDataSP).AppendLine();
                                            //returnMsg.AppendFormat("This Inmarsat id {0}: Data of Background IP data rate is not match.<br />", ImarSatID).AppendLine();
                                            Les_TotalCostCP += LES_OutbundleDataCP;
                                            Cus_TotalCostSP += Cus_OutbundleDataSP;

                                        }
                                    }
                                    else
                                    {
                                        //Les_TotalCostCP += LES_DataCP;
                                        Cus_TotalCostSP += Cus_DataSP;
                                        if (CDR_DataCP == 0 || Cus_DataSP == 0)
                                        {
                                            isDataMatched = true;
                                        }
                                        else
                                        {
                                            isDataMatched = false;

                                            emsg = new ErrorDetails();
                                            emsg.ErrorCode = "Err00" + i++;
                                            emsg.Descriptions = string.Format("{0} ID Inbundle charge more than les reg table", CT_Fbs.BCtFbImImsi);
                                            emsg.CT = "BCtTranTotalUsage";
                                            emsg.LES = "BPtLesRgnFbDataInBundle";
                                            emsg.Details = string.Format("CDR Inbundle rate is {0} and LES Reg Inbundle rate is {1}.", CDR_DataCP, LES_DataCP);
                                            emsg.ToReject = "Yes";
                                            
                                            lstError.Add(emsg);
                                            htmlString.AppendFormat("<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> <td>{5}</td> <td>{6}</td> </tr>", emsg.ErrorCode, emsg.Descriptions, emsg.CT, emsg.LES, emsg.Details, emsg.ToBill, emsg.ToReject).AppendLine();
                                            //Msg.AppendFormat("This Inmarsat id {0}: Data of Background IP data rate is not match.<br />", ImarSatID).AppendLine();
                                            //Msg.AppendFormat("Data Rate from LES CDR Price:{0}.<br />", CDR_DataCP).AppendLine();
                                            //Msg.AppendFormat("Data Rate from LES Reg CostPrice:{0}.<br />", LES_DataCP).AppendLine();
                                            //Msg.AppendFormat("Data Rate from Cus Reg SellerPrice:{0}.<br />", Cus_DataSP).AppendLine();
                                            //returnMsg.AppendFormat("This Inmarsat id {0}: Data of Background IP data rate is not match.<br />", ImarSatID).AppendLine();
                                        }
                                    }
                                }
                                else
                                    isDataMatched = true;
                                #endregion

                                #region V2Cellular rate calculation
                                if (CDR_VceCellularUintsCT != null)
                                {
                                    decimal V2CellTotalUnitsCT = 0;

                                    foreach (var ct in lstV2Cell_Trans)
                                    {
                                        if (ct.BCtTranUnitUsage != null)
                                        {
                                            V2CellTotalUnitsCT += Convert.ToDecimal(helper_OteSat.getMinutes(ct.BCtTranUnitUsage));
                                        }
                                    }

                                    double lngUnits = Convert.ToDouble(V2CellTotalUnitsCT);
                                    decimal? CDR_VceCellulartotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                                    decimal? LES_VceCellCPperMin = CDR_VceCellulartotalUnits_Minutes * LES_VoiceCellular;
                                    decimal? CUS_VceCellSPperMin = CDR_VceCellulartotalUnits_Minutes * CUS_VoiceCellular;

                                    if (CDR_VceCellulartotalUnits_Minutes != null)
                                        CDR_VceCellulartotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceCellulartotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);



                                    if (LES_VceCellCPperMin != null)
                                        LES_VceCellCPperMin = Math.Round(Convert.ToDecimal(LES_VceCellCPperMin), 2, MidpointRounding.AwayFromZero);

                                    if (CUS_VceCellSPperMin != null)
                                        CUS_VceCellSPperMin = Math.Round(Convert.ToDecimal(CUS_VceCellSPperMin), 2, MidpointRounding.AwayFromZero);

                                    Les_TotalCostCP += LES_VceCellCPperMin;
                                    Cus_TotalCostSP += CUS_VceCellSPperMin;

                                    //&& (CDR_VceCellularCostPriceCT < CUS_VceCellSPperMin)
                                    if ((LES_VceCellCPperMin.Equals(CDR_VceCellularCostPriceCT) || LES_VceCellCPperMin > CDR_VceCellularCostPriceCT))
                                    {
                                        isVoiceCellurlarMatched = true;
                                    }
                                    else
                                    {
                                        isVoiceCellurlarMatched = false;

                                        emsg = new ErrorDetails();
                                        emsg.ErrorCode = "Err00" + i++;
                                        emsg.Descriptions = string.Format("{0} ID per unit rate charged for voice to cellular more than les table", CT_Fbs.BCtFbImImsi);
                                        emsg.CT = "BCtTranTotalUsage";
                                        emsg.LES = "BPtLesRgnFbVce2Cell";
                                        emsg.Details = string.Format("CDR Voice to Cell rate is {0} and LES Reg Voice to Cell rate is {1}.", CDR_VceCellularCostPriceCT, LES_VceCellCPperMin);
                                        emsg.ToReject = "Yes";
                                        
                                        lstError.Add(emsg);
                                        htmlString.AppendFormat("<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> <td>{5}</td> <td>{6}</td> </tr>", emsg.ErrorCode, emsg.Descriptions, emsg.CT, emsg.LES, emsg.Details, emsg.ToBill, emsg.ToReject).AppendLine();
                                        //Msg.AppendFormat("This Inmarsat {0}: Voice to Cellular data rate is less than LES/CUS rate.<br />", ImarSatID).AppendLine();
                                        //Msg.AppendFormat("Voice to Cellular Rate from LES CDR Price:{0}.<br />", CDR_VceCellularCostPriceCT).AppendLine();
                                        //Msg.AppendFormat("Voice to Cellular Rate from LES Reg CostPrice:{0}.<br />", LES_VceCellCPperMin).AppendLine();
                                        //Msg.AppendFormat("Voice to Cellular Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VceCellSPperMin).AppendLine();
                                        //returnMsg.AppendFormat("This Inmarsat {0}: Voice to Cellular data rate is less than LES/CUS rate.<br />", ImarSatID).AppendLine();
                                    }
                                }
                                else
                                    isVoiceCellurlarMatched = true;
                                #endregion

                                #region V2Fixed rate calculations
                                if (CDR_VceFixedUintsCT != null)
                                {
                                    decimal V2FixedTotalUnitsCT = 0;

                                    foreach (var ct in lstV2Fixed_Trans)
                                    {
                                        if (ct.BCtTranUnitUsage != null)
                                        {
                                            V2FixedTotalUnitsCT += Convert.ToDecimal(helper_OteSat.getMinutes(ct.BCtTranUnitUsage));
                                        }
                                    }

                                    double lngUnits = Convert.ToDouble(V2FixedTotalUnitsCT);
                                    decimal? CDR_VceFixedTotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                                    if (CDR_VceFixedTotalUnits_Minutes != null)
                                        CDR_VceFixedTotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceFixedTotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

                                    decimal? LES_VceFixedCPperMin = CDR_VceFixedTotalUnits_Minutes * LES_VoiceFixed;
                                    decimal? CUS_VceFixedSPperMin = CDR_VceFixedTotalUnits_Minutes * CUS_VoiceFixed;

                                    if (LES_VceFixedCPperMin != null)
                                        LES_VceFixedCPperMin = Math.Round(Convert.ToDecimal(LES_VceFixedCPperMin), 2, MidpointRounding.AwayFromZero);

                                    if (CUS_VceFixedSPperMin != null)
                                        CUS_VceFixedSPperMin = Math.Round(Convert.ToDecimal(CUS_VceFixedSPperMin), 2, MidpointRounding.AwayFromZero);

                                    Les_TotalCostCP += LES_VceFixedCPperMin;
                                    Cus_TotalCostSP += CUS_VceFixedSPperMin;

                                    //&& (CDR_VceFixedCostPriceCT < CUS_VceFixedSPperMin)
                                    if ((LES_VceFixedCPperMin.Equals(CDR_VceFixedCostPriceCT) || LES_VceFixedCPperMin > CDR_VceFixedCostPriceCT))
                                    {
                                        isVoiceFixedMatched = true;
                                    }
                                    else
                                    {
                                        isVoiceFixedMatched = false;

                                        emsg = new ErrorDetails();
                                        emsg.ErrorCode = "Err00" + i++;
                                        emsg.Descriptions = string.Format("{0} ID per unit rate charged for voice to fixed more than les table", CT_Fbs.BCtFbImImsi);
                                        emsg.CT = "BCtTranTotalUsage";
                                        emsg.LES = "BPtLesRgnFbVce2Fixed";
                                        emsg.Details = string.Format("CDR Voice to Fixed rate is {0} and LES Reg Voice to Fixed rate is {1}.", CDR_VceFixedCostPriceCT, LES_VceFixedCPperMin);
                                        emsg.ToReject = "Yes";
                                        
                                        lstError.Add(emsg);

                                        htmlString.AppendFormat("<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> <td>{5}</td> <td>{6}</td> </tr>", emsg.ErrorCode, emsg.Descriptions, emsg.CT, emsg.LES, emsg.Details, emsg.ToBill, emsg.ToReject).AppendLine();

                                        //Msg.AppendFormat("This Inmarsat {0}: Voice to Fixed data rate is not match.<br />", ImarSatID).AppendLine();
                                        //Msg.AppendFormat("Voice to Fixed Rate from LES CDR Price:{0}.<br />", CDR_VceFixedCostPriceCT).AppendLine();
                                        //Msg.AppendFormat("Voice to Fixed Rate from LES Reg CostPrice:{0}.<br />", LES_VceFixedCPperMin).AppendLine();
                                        //Msg.AppendFormat("Voice to Fixed Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VceFixedSPperMin).AppendLine();
                                        //returnMsg.AppendFormat("This Inmarsat {0}: Voice to Fixed data rate is not match.<br />", ImarSatID).AppendLine();
                                    }
                                }
                                else
                                    isVoiceFixedMatched = true;
                                #endregion

                                #region v2FBB rate Calculations

                                if (CDR_VceFBBUintsCT != null)
                                {
                                    decimal V2FBBTotalUnitsCT = 0;

                                    foreach (var ct in lstV2Fbb_Trans)
                                    {
                                        if (ct.BCtTranUnitUsage != null)
                                        {
                                            V2FBBTotalUnitsCT += Convert.ToDecimal(helper_OteSat.getMinutes(ct.BCtTranUnitUsage));
                                        }
                                    }

                                    double lngUnits = Convert.ToDouble(V2FBBTotalUnitsCT);
                                    decimal? CDR_VceFBBTotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                                    if (CDR_VceFBBTotalUnits_Minutes != null)
                                        CDR_VceFBBTotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceFBBTotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

                                    decimal? LES_VceFBBCPperMin = CDR_VceFBBTotalUnits_Minutes * LES_VoiceFBB;
                                    decimal? CUS_VceFBBSPperMin = CDR_VceFBBTotalUnits_Minutes * CUS_VoiceFBB;

                                    if (LES_VceFBBCPperMin != null)
                                        LES_VceFBBCPperMin = Math.Round(Convert.ToDecimal(LES_VceFBBCPperMin), 2, MidpointRounding.AwayFromZero);

                                    if (CUS_VceFBBSPperMin != null)
                                        CUS_VceFBBSPperMin = Math.Round(Convert.ToDecimal(CUS_VceFBBSPperMin), 2, MidpointRounding.AwayFromZero);

                                    Les_TotalCostCP += LES_VceFBBCPperMin;
                                    Cus_TotalCostSP += CUS_VceFBBSPperMin;

                                    // && (CDR_VceFBBCostPriceCT < CUS_VceFBBSPperMin)
                                    if ((LES_VceFBBCPperMin.Equals(CDR_VceFBBCostPriceCT) || LES_VceFBBCPperMin > CDR_VceFBBCostPriceCT))
                                    {
                                        isVoiceFbbMatched = true;
                                    }
                                    else
                                    {
                                        isVoiceFbbMatched = false;

                                        emsg = new ErrorDetails();
                                        emsg.ErrorCode = "Err00" + i++;
                                        emsg.Descriptions = string.Format("{0} ID per unit rate charged for voice to fbb more than les table", CT_Fbs.BCtFbImImsi);
                                        emsg.CT = "BCtTranTotalUsage";
                                        emsg.LES = "BPtLesRgnFbVce2Fb";
                                        emsg.Details = string.Format("CDR Voice to Fbb rate is {0} and LES Reg Voice to Fbb rate is {1}.", CDR_VceFBBCostPriceCT, LES_VceFBBCPperMin);
                                        emsg.ToReject = "Yes";
                                        
                                        lstError.Add(emsg);

                                        htmlString.AppendFormat("<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> <td>{5}</td> <td>{6}</td> </tr>", emsg.ErrorCode, emsg.Descriptions, emsg.CT, emsg.LES, emsg.Details, emsg.ToBill, emsg.ToReject).AppendLine();
                                        //Msg.AppendFormat("This Inmarsat {0}: Voice to FleetBroadband data rate is less than LES/Cus rate.<br />", ImarSatID).AppendLine();
                                        //Msg.AppendFormat("Voice to FleetBroadband Rate from LES CDR Price:{0}.<br />", CDR_VceFBBCostPriceCT).AppendLine();
                                        //Msg.AppendFormat("Voice to FleetBroadband Rate from LES Reg CostPrice:{0}.<br />", LES_VceFBBCPperMin).AppendLine();
                                        //Msg.AppendFormat("Voice to FleetBroadband Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VceFBBSPperMin).AppendLine();
                                        //returnMsg.AppendFormat("This Inmarsat {0}: Voice to FleetBroadband data rate is less than LES/Cus rate.<br />", ImarSatID).AppendLine();
                                    }
                                }
                                else
                                    isVoiceFbbMatched = true;
                                #endregion

                                #region V2Iridium rate calculations

                                if (CDR_VceIridiumUintsCT != null)
                                {
                                    decimal V2IriTotalUnitsCT = 0;

                                    foreach (var ct in lstV2Iridium_Trans)
                                    {
                                        if (ct.BCtTranUnitUsage != null)
                                        {
                                            V2IriTotalUnitsCT += Convert.ToDecimal(helper_OteSat.getMinutes(ct.BCtTranUnitUsage));
                                        }
                                    }

                                    double lngUnits = Convert.ToDouble(V2IriTotalUnitsCT);
                                    decimal? CDR_VceIridiumTotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                                    if (CDR_VceIridiumTotalUnits_Minutes != null)
                                        CDR_VceIridiumTotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceIridiumTotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

                                    decimal? LES_VceIridiumCPperMin = CDR_VceIridiumTotalUnits_Minutes * LES_VoiceIridium;
                                    decimal? CUS_VceIridiumSPperMin = CDR_VceIridiumTotalUnits_Minutes * CUS_VoiceIridium;

                                    if (LES_VceIridiumCPperMin != null)
                                        LES_VceIridiumCPperMin = Math.Round(Convert.ToDecimal(LES_VceIridiumCPperMin), 2, MidpointRounding.AwayFromZero);

                                    if (CUS_VceIridiumSPperMin != null)
                                        CUS_VceIridiumSPperMin = Math.Round(Convert.ToDecimal(CUS_VceIridiumSPperMin), 2, MidpointRounding.AwayFromZero);

                                    Les_TotalCostCP += LES_VceIridiumCPperMin;
                                    Cus_TotalCostSP += CUS_VceIridiumSPperMin;

                                    // && (CDR_VceIridiumCostPriceCT < CUS_VceIridiumSPperMin)
                                    if ((LES_VceIridiumCPperMin.Equals(CDR_VceIridiumCostPriceCT) || LES_VceIridiumCPperMin > CDR_VceIridiumCostPriceCT))
                                    {
                                        isVoiceIridiumMatched = true;
                                    }
                                    else
                                    {
                                        isVoiceIridiumMatched = false;

                                        emsg = new ErrorDetails();
                                        emsg.ErrorCode = "Err00" + i++;
                                        emsg.Descriptions = string.Format("{0} ID per unit rate charged for voice to iridium more than les table", CT_Fbs.BCtFbImImsi);
                                        emsg.CT = "BCtTranTotalUsage";
                                        emsg.LES = "BPtLesRgnFb2Iridium";
                                        emsg.Details = string.Format("CDR Voice to Iridium rate is {0} and LES Reg Voice to Iridium rate is {1}.", CDR_VceIridiumCostPriceCT, LES_VceIridiumCPperMin);
                                        emsg.ToReject = "Yes";
                                        lstError.Add(emsg);

                                        htmlString.AppendFormat("<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> <td>{5}</td> <td>{6}</td> </tr>", emsg.ErrorCode, emsg.Descriptions, emsg.CT, emsg.LES, emsg.Details, emsg.ToBill, emsg.ToReject).AppendLine();

                                        //Msg.AppendFormat("This Inmarsat {0}: Voice to Iridium data rate is less than LES/CUS rate.<br />", ImarSatID).AppendLine();
                                        //Msg.AppendFormat("Voice to Iridium Rate from LES CDR Price:{0}.<br />", CDR_VceIridiumCostPriceCT).AppendLine();
                                        //Msg.AppendFormat("Voice to Iridium Rate from LES Reg CostPrice:{0}.<br />", LES_VceIridiumCPperMin).AppendLine();
                                        //Msg.AppendFormat("Voice to Iridium Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VceIridiumSPperMin).AppendLine();
                                        //returnMsg.AppendFormat("This Inmarsat {0}: Voice to Iridium data rate is less than LES/CUS rate.<br />", ImarSatID).AppendLine();
                                    }
                                }
                                else
                                    isVoiceIridiumMatched = true;
                                #endregion

                                #region ISDN Rate Calulations

                                if (CDR_ISDNUintsCT != null)
                                {
                                    decimal V2ISDNTotalUnitsCT = 0;

                                    foreach (var ct in lstISDN_Trans)
                                    {
                                        if (ct.BCtTranUnitUsage != null)
                                        {
                                            V2ISDNTotalUnitsCT += Convert.ToDecimal(helper_OteSat.getMinutes(ct.BCtTranUnitUsage));
                                        }
                                    }

                                    double lngUnits = Convert.ToDouble(V2ISDNTotalUnitsCT);
                                    decimal? CDR_ISDNTotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                                    if (CDR_ISDNTotalUnits_Minutes != null)
                                        CDR_ISDNTotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_ISDNTotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

                                    decimal? LES_ISDNCPperMin = CDR_ISDNTotalUnits_Minutes * LES_ISDN;
                                    decimal? CUS_ISDNSPperMin = CDR_ISDNTotalUnits_Minutes * CUS_ISDN;

                                    if (LES_ISDNCPperMin != null)
                                        LES_ISDNCPperMin = Math.Round(Convert.ToDecimal(LES_ISDNCPperMin), 2, MidpointRounding.AwayFromZero);

                                    if (CUS_ISDNSPperMin != null)
                                        CUS_ISDNSPperMin = Math.Round(Convert.ToDecimal(CUS_ISDNSPperMin), 2, MidpointRounding.AwayFromZero);

                                    Les_TotalCostCP += LES_ISDNCPperMin;
                                    Cus_TotalCostSP += CUS_ISDNSPperMin;

                                    //&& (CDR_ISDNCostPriceCT < CUS_ISDNSPperMin)
                                    if ((LES_ISDNCPperMin.Equals(CDR_ISDNCostPriceCT) || LES_ISDNCPperMin > CDR_ISDNCostPriceCT))
                                    {
                                        isISDNMatched = true;
                                    }
                                    else
                                    {
                                        isISDNMatched = false;

                                        emsg = new ErrorDetails();
                                        emsg.ErrorCode = "Err00" + i++;
                                        emsg.Descriptions = string.Format("{0} ID per unit rate charged for voice to isdn more than les table", CT_Fbs.BCtFbImImsi);
                                        emsg.CT = "BCtTranTotalUsage";
                                        emsg.LES = "BPtLesRgnFbVce2Others";
                                        emsg.Details = string.Format("CDR Voice to ISDN rate is {0} and LES Reg Voice to ISDN rate is {1}.", CDR_ISDNCostPriceCT, LES_ISDNCPperMin);
                                        emsg.ToReject = "Yes";
                                        
                                        lstError.Add(emsg);

                                        htmlString.AppendFormat("<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> <td>{5}</td> <td>{6}</td> </tr>", emsg.ErrorCode, emsg.Descriptions, emsg.CT, emsg.LES, emsg.Details, emsg.ToBill, emsg.ToReject).AppendLine();

                                        //Msg.AppendFormat("This Inmarsat {0}: ISDN rate is less than LES/Cus rate.<br />", ImarSatID).AppendLine();
                                        //Msg.AppendFormat("ISDN Rate from LES CDR Price:{0}.<br />", CDR_ISDNCostPriceCT).AppendLine();
                                        //Msg.AppendFormat("ISDN Rate from LES Reg CostPrice:{0}.<br />", LES_ISDNCPperMin).AppendLine();
                                        //Msg.AppendFormat("ISDN Rate from Cus Reg SellerPrice:{0}.<br />", CUS_ISDNSPperMin).AppendLine();
                                        //returnMsg.AppendFormat("This Inmarsat {0}: ISDN rate is less than LES/Cus rate.<br />", ImarSatID).AppendLine();
                                    }
                                }
                                else
                                    isISDNMatched = true;
                                #endregion

                                #region SMS rate calculations

                                if (CDR_SMSUintsCT != null)
                                {

                                    decimal? CDR_SMSTotalUnits_Msg = CDR_SMSUintsCT;

                                    if (CDR_SMSTotalUnits_Msg != null)
                                        CDR_SMSTotalUnits_Msg = Math.Round(Convert.ToDecimal(CDR_SMSTotalUnits_Msg), 2, MidpointRounding.AwayFromZero);

                                    decimal? LES_SMSCPperMin = CDR_SMSTotalUnits_Msg * LES_SMS;
                                    decimal? CUS_SMSSPperMin = CDR_SMSTotalUnits_Msg * CUS_SMS;

                                    if (LES_SMSCPperMin != null)
                                        LES_SMSCPperMin = Math.Round(Convert.ToDecimal(LES_SMSCPperMin), 2, MidpointRounding.AwayFromZero);

                                    if (CUS_SMSSPperMin != null)
                                        CUS_SMSSPperMin = Math.Round(Convert.ToDecimal(CUS_SMSSPperMin), 2, MidpointRounding.AwayFromZero);

                                    Les_TotalCostCP += LES_SMSCPperMin;
                                    Cus_TotalCostSP += CUS_SMSSPperMin;

                                    //&& (CDR_SMSCostPriceCT < CUS_SMSSPperMin)
                                    if ((LES_SMSCPperMin.Equals(CDR_SMSCostPriceCT) || LES_SMSCPperMin > CDR_SMSCostPriceCT))
                                    {
                                        isSMSMatched = true;
                                    }
                                    else
                                    {
                                        isISDNMatched = false;

                                        emsg = new ErrorDetails();
                                        emsg.ErrorCode = "Err00" + i++;
                                        emsg.Descriptions = string.Format("{0} ID per unit rate charged for SMS more than les table", CT_Fbs.BCtFbImImsi);
                                        emsg.CT = "BCtTranTotalUsage";
                                        emsg.LES = "BPtLesRgnFbTranSmsUsage";
                                        emsg.Details = string.Format("CDR SMS rate is {0} and LES Reg SMS rate is {1}.", CDR_SMSCostPriceCT, LES_SMSCPperMin);
                                        emsg.ToReject = "Yes";
                                        
                                        lstError.Add(emsg);

                                        htmlString.AppendFormat("<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> <td>{5}</td> <td>{6}</td> </tr>", emsg.ErrorCode, emsg.Descriptions, emsg.CT, emsg.LES, emsg.Details, emsg.ToBill, emsg.ToReject).AppendLine();

                                        //Msg.AppendFormat("This Inmarsat {0}: SMS rate is less than LES/CUS rate.<br />", ImarSatID).AppendLine();
                                        //Msg.AppendFormat("SMS Rate from LES CDR Price:{0}.<br />", CDR_SMSCostPriceCT).AppendLine();
                                        //Msg.AppendFormat("SMS Rate from LES Reg CostPrice:{0}.<br />", LES_SMSCPperMin).AppendLine();
                                        //Msg.AppendFormat("SMS Rate from Cus Reg SellerPrice:{0}.<br />", CUS_SMSSPperMin).AppendLine();
                                        //returnMsg.AppendFormat("This Inmarsat {0}: SMS rate is less than LES/CUS rate.<br />", ImarSatID).AppendLine();
                                    }
                                }
                                else
                                    isSMSMatched = true;
                                #endregion

                                string InvoiceNo = string.Empty;

                                #region Get Invoice Number
                                try
                                {
                                    InvoiceNo = db.BS2MtCusT.Max(t => t.BSMtCusInvNo);

                                    if (!string.IsNullOrEmpty(InvoiceNo))
                                    {
                                        if (InvoiceNo.Contains("SM"))
                                        {
                                            string tempInv = InvoiceNo.Replace("SM", string.Empty);
                                            if (int.TryParse(tempInv, out int result))
                                            {
                                                InvoiceNo = BillingHelpers.GenInvoice(false, result);
                                            }
                                        }
                                    }
                                    else
                                        InvoiceNo = BillingHelpers.GenInvoice(false, 0);
                                }
                                catch (Exception error)
                                {
                                    Msg.AppendFormat("MSI:{0} Error Msg: {1} Stack Trace: {2}. <br />", ImarSatID, error.Message.ToString(), error.StackTrace);
                                }
                                #endregion

                                #region check Rejection condition and store bool value

                                if ((CDR_StartDate >= LES_ContrRegStrDate && CDR_EndDate <= LES_ContrRegEndDate) || isNoTransData)
                                    isLES_ContrRegDate = true;
                                else
                                {
                                    isLES_ContrRegDate = false;

                                    if (!(CDR_StartDate >= LES_ContrRegStrDate))
                                    {
                                        emsg = new ErrorDetails();
                                        emsg.ErrorCode = "Err00" + i++;
                                        emsg.Descriptions = string.Format("{0} ID transaction date before Registeration", CT_Fbs.BCtFbImImsi);
                                        emsg.CT = "BCtTranStartDate";
                                        emsg.LES = "BPtLesRgnFbStartDate";
                                        emsg.Details = string.Format("CDR Start date is {0} and LES Reg start date is {1}.", CDR_StartDate, LES_ContrRegStrDate);
                                        emsg.ToReject = "Yes";
                                        
                                        lstError.Add(emsg);

                                        htmlString.AppendFormat("<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> <td>{5}</td> <td>{6}</td> </tr>", emsg.ErrorCode, emsg.Descriptions, emsg.CT, emsg.LES, emsg.Details, emsg.ToBill, emsg.ToReject).AppendLine();
                                    }

                                    if (!(CDR_EndDate <= LES_ContrRegEndDate))
                                    {
                                        emsg = new ErrorDetails();
                                        emsg.ErrorCode = "Err00" + i++;
                                        emsg.Descriptions = string.Format("{0} ID transaction date after termination", CT_Fbs.BCtFbImImsi);
                                        emsg.CT = "BCtTranStartDate";
                                        emsg.LES = "BPtLesRgnFbStartDate";
                                        emsg.Details = string.Format("CDR start date is {0} and LES Reg termination date is {1}.", CDR_StartDate, LES_ContrRegEndDate);
                                        emsg.ToReject = "Yes";
                                        
                                        lstError.Add(emsg);

                                        htmlString.AppendFormat("<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> <td>{5}</td> <td>{6}</td> </tr>", emsg.ErrorCode, emsg.Descriptions, emsg.CT, emsg.LES, emsg.Details, emsg.ToBill, emsg.ToReject).AppendLine();
                                    }

                                    //Msg.AppendFormat("This Inmarsat {0}: Contract CDR Start/End date Less than les Reg Start/End Date.<br />", ImarSatID).AppendLine();
                                    //Msg.AppendFormat("Contract CDR Start Date {0}: LES Reg Strart Date {1}.<br />", CDR_StartDate, LES_ContrRegStrDate).AppendLine();
                                    //Msg.AppendFormat("Contract CDR Start Date {0}: LES Reg End Date {1}.<br />", CDR_StartDate, LES_ContrRegEndDate).AppendLine();
                                    //returnMsg.AppendFormat("This Inmarsat {0}: Contract Reg start date Less than les Reg start Date.<br />", ImarSatID).AppendLine();
                                }

                                // && CDR_EndDate > LES_ContrBarEndDate
                                if ((LES_ContrBarStrDate == null && LES_ContrBarEndDate == null) || (CDR_StartDate > LES_ContrBarStrDate ))
                                    isLES_ContrBarDate = true;
                                else
                                {
                                    isLES_ContrBarDate = false;

                                    emsg = new ErrorDetails();
                                    emsg.ErrorCode = "Err00" + i++;
                                    emsg.Descriptions = string.Format("{0} ID transaction date after barring", CT_Fbs.BCtFbImImsi);
                                    emsg.CT = "BCtTranStartDate";
                                    emsg.LES = "BPtLesRgnFbBarDate";
                                    emsg.Details = string.Format("CDR Start rate is {0} and LES Reg Bar date is {1}.", CDR_StartDate, LES_ContrBarStrDate);
                                    emsg.ToReject = "Yes";
                                    
                                    lstError.Add(emsg);

                                    htmlString.AppendFormat("<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> <td>{5}</td> <td>{6}</td> </tr>", emsg.ErrorCode, emsg.Descriptions, emsg.CT, emsg.LES, emsg.Details, emsg.ToBill, emsg.ToReject).AppendLine();

                                    //Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than lES Bar Start/End Date.<br />", ImarSatID).AppendLine();
                                    //Msg.AppendFormat("Contract CDR Start Date {0}: LES Bar Strart Date {1}.<br />", CDR_StartDate, LES_ContrBarStrDate).AppendLine();
                                    //Msg.AppendFormat("Contract CDR Start Date {0}: LES Bar End Date {1}.<br />", CDR_StartDate, LES_ContrBarEndDate).AppendLine();
                                    //returnMsg.AppendFormat("This Inmarsat {0}: Contract CCDR start date Greater than lES Bar start Date.<br />", ImarSatID).AppendLine();
                                }

                                // && CDR_EndDate > LES_ContrSusEndDate
                                if ((LES_ContrSusStrDate == null && LES_ContrSusEndDate == null) || (CDR_StartDate > LES_ContrSusStrDate ))
                                    isLES_ContrSusDate = true;
                                else
                                {
                                    isLES_ContrBarDate = false;

                                    emsg = new ErrorDetails();
                                    emsg.ErrorCode = "Err00" + i++;
                                    emsg.Descriptions = string.Format("{0} ID transaction date after suspensioon", CT_Fbs.BCtFbImImsi);
                                    emsg.CT = "BCtTranStartDate";
                                    emsg.LES = "BPtLesRgnFBSusDate";
                                    emsg.Details = string.Format("CDR Start rate is {0} and LES Reg Sus date is {1}.", CDR_StartDate, LES_ContrSusStrDate);
                                    emsg.ToReject = "Yes";
                                    
                                    lstError.Add(emsg);

                                    htmlString.AppendFormat("<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> <td>{5}</td> <td>{6}</td> </tr>", emsg.ErrorCode, emsg.Descriptions, emsg.CT, emsg.LES, emsg.Details, emsg.ToBill, emsg.ToReject).AppendLine();

                                    //Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than lES Suspenstion Start/End Date.<br />", ImarSatID).AppendLine();
                                    //Msg.AppendFormat("Contract CDR Start Date {0}: LES Suspenstion Start Date {1}.<br />", CDR_StartDate, LES_ContrSusStrDate).AppendLine();
                                    //Msg.AppendFormat("Contract CDR Start Date {0}: LES Suspenstion End Date {1}.<br />", CDR_StartDate, LES_ContrSusEndDate).AppendLine();
                                    //returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than lES Suspenstion start Date.<br />", ImarSatID).AppendLine();
                                }

                                //&& CDR_EndDate > LES_ContrLayoutEndDate
                                if ((LES_ContrLayoutStrDate == null && LES_ContrLayoutEndDate == null) || (CDR_StartDate > LES_ContrLayoutStrDate ))
                                    isLES_ContrLayUpDate = true;
                                else
                                {
                                    isLES_ContrLayUpDate = true;

                                    emsg = new ErrorDetails();
                                    emsg.ErrorCode = "Err00" + i++;
                                    emsg.Descriptions = string.Format("{0} ID transaction date after layUp", CT_Fbs.BCtFbImImsi);
                                    emsg.CT = "BCtTranStartDate";
                                    emsg.LES = "BPtLesRgnFblayUpDate";
                                    emsg.Details = string.Format("CDR Start rate is {0} and LES Reg LayUp date is {1}.", CDR_StartDate, LES_ContrLayoutStrDate);
                                    emsg.ToReject = "Yes";
                                    
                                    lstError.Add(emsg);

                                    htmlString.AppendFormat("<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> <td>{5}</td> <td>{6}</td> </tr>", emsg.ErrorCode, emsg.Descriptions, emsg.CT, emsg.LES, emsg.Details, emsg.ToBill, emsg.ToReject).AppendLine();

                                    //Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than LES LayUp Start/End Date.<br />", ImarSatID).AppendLine();
                                    //Msg.AppendFormat("Contract CDR Start Date {0}: LES LayUp Start Date {1}.<br />", CDR_StartDate, LES_ContrLayoutStrDate).AppendLine();
                                    //Msg.AppendFormat("Contract CDR Start Date {0}: LES LayUp End Date {1}.<br />", CDR_StartDate, LES_ContrLayoutEndDate).AppendLine();
                                    //returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than LayUp Start/End Date.<br />", ImarSatID).AppendLine();
                                }
                                #region Cus Reg date Validate
                                //if ((CDR_StartDate >= CUS_ContrRegStrDate && CDR_EndDate <= CUS_ContrRegEndDate) || isNoTransData)
                                //    isCUS_ContrRegDate = true;
                                //else
                                //{
                                //    isCUS_ContrRegDate = false;
                                //    Msg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Less than CUS Reg Start/End Date.<br />", ImarSatID).AppendLine();
                                //    Msg.AppendFormat("Contract CDR Start Date {0}: CUS Reg Strart Date {1}.<br />", CDR_StartDate, CUS_ContrRegStrDate).AppendLine();
                                //    Msg.AppendFormat("Contract CDR Start Date {0}: CUS Reg End Date {1}.<br />", CDR_StartDate, CUS_ContrRegEndDate).AppendLine();
                                //    returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Less than CUS Reg Start/End Date.<br />", ImarSatID).AppendLine();
                                //}

                                //if ((CUS_ContrBarStrDate == null && CUS_ContrBarEndDate == null) || (CDR_StartDate > CUS_ContrBarStrDate && CDR_EndDate > CUS_ContrBarEndDate))
                                //    isCUS_ContrBarDate = true;
                                //else
                                //{
                                //    isCUS_ContrBarDate = false;
                                //    Msg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Greater than CUS Bar Start/End Date.<br />", ImarSatID).AppendLine();
                                //    Msg.AppendFormat("Contract CDR Start Date {0}: CUS Bar Strart Date {1}.<br />", CDR_StartDate, CUS_ContrBarStrDate).AppendLine();
                                //    Msg.AppendFormat("Contract CDR Start Date {0}: CUS Bar End Date {1}.<br />", CDR_StartDate, CUS_ContrBarStrDate).AppendLine();
                                //    returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Greater than CUS Bar Start/End Date.<br />", ImarSatID).AppendLine();
                                //}

                                //if ((CUS_ContrSusStrDate == null && CUS_ContrSusEndDate == null) || (CDR_StartDate > CUS_ContrSusStrDate && CDR_EndDate > CUS_ContrSusEndDate))
                                //    isCUS_ContrSusDate = true;
                                //else
                                //{
                                //    isCUS_ContrSusDate = false;
                                //    Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS Suspenstion Start/End Date.<br />", ImarSatID).AppendLine();
                                //    Msg.AppendFormat("Contract CDR Start Date {0}: CUS Suspenstion Start Date {1}.<br />", CDR_StartDate, CUS_ContrSusStrDate).AppendLine();
                                //    Msg.AppendFormat("Contract CDR Start Date {0}: CUS Suspenstion End Date {1}.<br />", CDR_StartDate, CUS_ContrSusEndDate).AppendLine();
                                //    returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS Suspenstion Start/End Date.<br />", ImarSatID).AppendLine();
                                //}

                                //if ((CUS_ContrLayoutStrDate == null && CUS_ContrLayoutEndDate == null) || (CDR_StartDate > CUS_ContrLayoutStrDate && CDR_EndDate > CUS_ContrLayoutEndDate))
                                //    isCUS_ContrLayUpDate = true;
                                //else
                                //{
                                //    isCUS_ContrLayUpDate = true;
                                //    Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS LayUp Start/End Date.<br />", ImarSatID).AppendLine();
                                //    Msg.AppendFormat("Contract CDR Start Date {0}: CUS LayUp Start Date {1}.<br />", CDR_StartDate, LES_ContrSusStrDate).AppendLine();
                                //    Msg.AppendFormat("Contract CDR Start Date {0}: CUS LayUp End Date {1}.<br />", CDR_StartDate, LES_ContrSusEndDate).AppendLine();
                                //    returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS LayUp Start/End Date.<br />", ImarSatID).AppendLine();
                                //}
                                #endregion
                                #endregion

                                if (isRecurFeeMached && isDataMatched && isVoiceCellurlarMatched && isVoiceFixedMatched && isVoiceFbbMatched && isVoiceIridiumMatched &&
                                    isISDNMatched && isLES_ContrRegDate && isLES_ContrBarDate && isLES_ContrSusDate && isLES_ContrLayUpDate )
                                {
                                    emsg = new ErrorDetails();
                                    emsg.ErrorCode = "Err00" + i++;
                                    emsg.Descriptions = string.Format("{0} ID Should ready to store Invoice table", CT_Fbs.BCtFbImImsi);
                                    emsg.CT = "";
                                    emsg.LES = "";
                                    emsg.Details = string.Format("All conditions is matched it will ready to go secondary2 invoice table.", CDR_StartDate, LES_ContrLayoutStrDate);
                                    emsg.ToBill = "Yes";
                                    
                                    lstError.Add(emsg);

                                    htmlString.AppendFormat("<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> <td>{4}</td> <td>{5}</td> <td>{6}</td> </tr>", emsg.ErrorCode, emsg.Descriptions, emsg.CT, emsg.LES, emsg.Details, emsg.ToBill, emsg.ToReject).AppendLine();
                                }

                                #region Condition to store Reject or Invoice
                                //    if (isRecurFeeMached && isDataMatched && isVoiceCellurlarMatched && isVoiceFixedMatched && isVoiceFbbMatched && isVoiceIridiumMatched &&
                                //    isISDNMatched && isLES_ContrRegDate && isLES_ContrBarDate && isLES_ContrSusDate && isLES_ContrLayUpDate && isCUS_ContrRegDate &&
                                //    isCUS_ContrBarDate && isCUS_ContrSusDate && isCUS_ContrLayUpDate && isSMSMatched)
                                //{
                                //    if (!string.IsNullOrEmpty(InvoiceNo))
                                //    {
                                //        BSt2FbbTrans SecMtCust = new BSt2FbbTrans();

                                //        SecMtCust.Bt2CusFbInvNo = InvoiceNo;
                                //        SecMtCust.Bt2CusFbLES = Convert.ToString(LesID);
                                //        SecMtCust.Bt2CusFbCusRegID = CUS_RegsID;
                                //        SecMtCust.Bt2CusFbIMSIId = ImsiID;
                                //        SecMtCust.Bt2CusFbMsidnId = ImarSatID;
                                //        SecMtCust.Bt2CusFbLesRefCode = CT_Fbs.BCtFbLesRefCode;
                                //        SecMtCust.Bt2CusFbSatCode = CT_Fbs.BCtFbSatCode;
                                //        SecMtCust.Bt2CusFbEquipmentCode = CT_Fbs.BCtFbEquipmentCode;
                                //        //SecMtCust.BSMtCusLesCode 
                                //        SecMtCust.Bt2CusFbCdrCode = CT_Fbs.BCtFbCdrCode;
                                //        //SecMtCust.BSMtCusSatType = null;
                                //        SecMtCust.Bt2CusFbTranDest = CT_Fbs.BCtTranServDest;
                                //        SecMtCust.Bt2CusFbTranServType = null;
                                //        SecMtCust.Bt2CusFbServiceType = CT_Fbs.BCtTranServType;
                                //        SecMtCust.Bt2CusFbTranUsage = CT_Fbs.BCtTranUnitUsage;
                                //        SecMtCust.Bt2CusFbTranUnitType = 10;
                                //        SecMtCust.Bt2CusFbRecurFee = CUS_RecurFee;
                                //        Cus_TotalCostSP += SecMtCust.Bt2CusFbRecurFee;
                                //        SecMtCust.Bt2CusFbRecurFeeType = CT_Fbs.BCtAFbRecurFeeType;
                                //        //SecMtCust.BSMtCusCDRRef = CT_Fbs.BCtAFbCDRRef;
                                //        SecMtCust.Bt2CusFbBillno = CT_Fbs.BCtFbBillNo;
                                //        SecMtCust.Bt2CusFbBillPeriod = CT_Fbs.BCtFbBillPeriod;
                                //        SecMtCust.Bt2CusFbBillDate = CT_Fbs.BCtFbBillDate;

                                //        if (CDR_RecuFee < CDR_DataCP && isOutbundleUsed)
                                //        {
                                //            LES_InvRptDataCostTot = CDR_DataCP - CDR_RecuFee;
                                //            LES_InvRptDataCostTot = Math.Round(Convert.ToDecimal(LES_InvRptDataCostTot), 2, MidpointRounding.AwayFromZero);
                                //        }
                                //        else
                                //            LES_InvRptDataCostTot = 0;

                                //        Guid guid = new Guid();
                                //        SecMtCust.Bt2CusFbTranGUID = guid.ToString();

                                //        db.BSt2FbbTrans.Add(SecMtCust);

                                //        // Get value from BS2MtCusT and save to BInvRprt table
                                //        BPtInvLstReport Invreport = new BPtInvLstReport();

                                //        if (SecMtCust.Bt2CusFbServiceType != null)
                                //        {
                                //            Invreport.BInvRptGuID = SecMtCust.Bt2CusFbTranGUID;
                                //            Invreport.BInvRprtSerType = SecMtCust.Bt2CusFbServiceType;
                                //            Invreport.BInvRprtTotBil = SecMtCust.Bt2CusFbRecurFee;
                                //            Invreport.BInvRprtTotCst = CT_Fbs.BCtFbRecurFee;
                                //            db.BPtInvLstReports.Add(Invreport);
                                //        }

                                //        #region Inv Standard IP rate calculations

                                //        decimal? outbindleData = 0;
                                //        decimal? LES_RptDataCost = 0;
                                //        decimal? CUS_RptDataCost = 0;

                                //        if (is_CUSGBPlan)
                                //        {
                                //            Cus_DataPlan = Cus_DataPlan * 1024;
                                //        }

                                //        foreach (var records in lstStdIP_Trans)
                                //        {
                                //            BSt2FbbTrans bs2Sec = new BSt2FbbTrans();
                                //            bs2Sec.Bt2CusFbTranStartDate = records.BCtTranStartDate;
                                //            bs2Sec.Bt2CusFbTranStartTime = records.BCtTranStartTime;
                                //            bs2Sec.Bt2CusFbTranDOrig = records.BCtTranOrigin;
                                //            bs2Sec.Bt2CusFbTranDest = records.BCtTranServDest;
                                //            bs2Sec.Bt2CusFbTranServType = CUS_ServTypeID;

                                //            double dataconvertToMP = Convert.ToDouble(records.BCtTranUnitUsage);
                                //            bs2Sec.Bt2CusFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertBitToMegabytes(dataconvertToMP)), 2, MidpointRounding.AwayFromZero);
                                //            bs2Sec.Bt2CusFbTranUnitType = lstUnityType.FirstOrDefault(t => t.Units.Equals("MB")).UID;
                                //            outbindleData += bs2Sec.Bt2CusFbTranUsage;

                                //            if (Cus_DataPlan <= outbindleData)
                                //            {
                                //                bs2Sec.Bt2CusFbTranTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.Bt2CusFbTranUsage * Cus_OutbundleRate), 3, MidpointRounding.AwayFromZero);

                                //                if (Invreport.BInvRprtSerType != null)
                                //                    CUS_RptDataCost += bs2Sec.Bt2CusFbTranTotalUsage;
                                //            }
                                //            else
                                //            {
                                //                bs2Sec.Bt2CusFbTranTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.Bt2CusFbTranUsage * 0), 3, MidpointRounding.AwayFromZero);

                                //                if (Invreport.BInvRprtSerType != null)
                                //                    CUS_RptDataCost += bs2Sec.Bt2CusFbTranTotalUsage;
                                //            }
                                //            bs2Sec.Bt2CusFbTranComTypes = 1;

                                //            db.BSt2FbbTrans.Add(bs2Sec);
                                //        }

                                //        // Invoice List Report Add in BInvLstReports
                                //        if (lstStdIP_Trans.Count > 0)
                                //        {
                                //            Invreport = new BPtInvLstReport();
                                //            try
                                //            {
                                //                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeID).BsPtCusServiceTypes;
                                //            }
                                //            catch { Invreport.BInvRprtSerType = null; }

                                //            if (Invreport.BInvRprtSerType != null)
                                //            {
                                //                Invreport.BInvRptGuID = SecMtCust.Bt2CusFbTranGUID;
                                //                Invreport.BInvRprtTotCst = LES_InvRptDataCostTot;
                                //                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptDataCost), 3, MidpointRounding.AwayFromZero);
                                //                db.BPtInvLstReports.Add(Invreport);
                                //            }
                                //        }
                                //        #endregion

                                //        #region Invoice Voice to Cellular rate Calculations

                                //        decimal? LES_RptV2cCost = CDR_VceCellularCostPriceCT;
                                //        decimal? CUS_RptV2cCost = 0;

                                //        foreach (var records in lstV2Cell_Trans)
                                //        {
                                //            BSt2FbbTrans bs2Sec = new BSt2FbbTrans();
                                //            bs2Sec.Bt2CusFbTranStartDate = records.BCtTranStartDate;
                                //            bs2Sec.Bt2CusFbTranStartTime = records.BCtTranStartTime;
                                //            bs2Sec.Bt2CusFbTranDOrig = records.BCtTranOrigin;
                                //            bs2Sec.Bt2CusFbTranDest = records.BCtTranServDest;
                                //            bs2Sec.Bt2CusFbTranServType = CUS_ServTypeMobile;

                                //            decimal? v2CTransSec = 0;
                                //            if (records.BCtTranUnitUsage != null)
                                //            {
                                //                v2CTransSec = getMinutes(records.BCtTranUnitUsage);
                                //            }
                                //            long SecconvertToMin = Convert.ToInt64(v2CTransSec);
                                //            bs2Sec.Bt2CusFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                //            bs2Sec.Bt2CusFbTranUnitType = lstUnityType.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                //            bs2Sec.Bt2CusFbTranTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.Bt2CusFbTranUsage * CUS_VoiceCellular), 3, MidpointRounding.AwayFromZero);

                                //            if (Invreport.BInvRprtSerType != null)
                                //                CUS_RptV2cCost += bs2Sec.Bt2CusFbTranTotalUsage;

                                //            bs2Sec.Bt2CusFbTranComTypes = 2;

                                //            db.BSt2FbbTrans.Add(bs2Sec);
                                //        }

                                //        // Invoice List Report Add in BInvLstReports
                                //        if (lstV2Cell_Trans.Count > 0)
                                //        {
                                //            Invreport = new BPtInvLstReport();
                                //            try
                                //            {
                                //                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeMobile).BsPtCusServiceTypes;
                                //            }
                                //            catch { Invreport.BInvRprtSerType = null; }

                                //            if (Invreport.BInvRprtSerType != null)
                                //            {
                                //                Invreport.BInvRptGuID = SecMtCust.Bt2CusFbTranGUID;
                                //                Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2cCost), 2, MidpointRounding.AwayFromZero);
                                //                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2cCost), 2, MidpointRounding.AwayFromZero);
                                //                db.BPtInvLstReports.Add(Invreport);
                                //            }
                                //        }
                                //        #endregion

                                //        #region Invoice Voice to Fixed rate calculations

                                //        decimal? LES_RptV2fCost = CDR_VceFixedCostPriceCT;
                                //        decimal? CUS_RptV2fCost = 0;

                                //        foreach (var records in lstV2Fixed_Trans)
                                //        {
                                //            BSt2FbbTrans bs2Sec = new BSt2FbbTrans();
                                //            bs2Sec.Bt2CusFbTranStartDate = records.BCtTranStartDate;
                                //            bs2Sec.Bt2CusFbTranStartTime = records.BCtTranStartTime;
                                //            bs2Sec.Bt2CusFbTranDOrig = records.BCtTranOrigin;
                                //            bs2Sec.Bt2CusFbTranDest = records.BCtTranServDest;
                                //            bs2Sec.Bt2CusFbTranServType = CUS_ServTypeVce2Fixed;

                                //            decimal? v2FixedTransSec = 0;
                                //            if (records.BCtTranUnitUsage != null)
                                //            {
                                //                v2FixedTransSec = getMinutes(records.BCtTranUnitUsage);
                                //            }

                                //            long SecconvertToMin = Convert.ToInt64(v2FixedTransSec);
                                //            bs2Sec.Bt2CusFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                //            bs2Sec.Bt2CusFbTranUnitType = lstUnityType.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                //            bs2Sec.Bt2CusFbTranTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.Bt2CusFbTranUsage * CUS_VoiceFixed), 3, MidpointRounding.AwayFromZero);

                                //            if (Invreport.BInvRprtSerType != null)
                                //                CUS_RptV2fCost += bs2Sec.Bt2CusFbTranTotalUsage;

                                //            bs2Sec.Bt2CusFbTranComTypes = 2;

                                //            db.BSt2FbbTrans.Add(bs2Sec);
                                //        }

                                //        // Invoice List Report Add in BInvLstReports
                                //        if (lstV2Fixed_Trans.Count > 0)
                                //        {
                                //            Invreport = new BPtInvLstReport();

                                //            try
                                //            {
                                //                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeVce2Fixed).BsPtCusServiceTypes;
                                //            }
                                //            catch { Invreport.BInvRprtSerType = null; }

                                //            if (Invreport.BInvRprtSerType != null)
                                //            {
                                //                Invreport.BInvRptGuID = SecMtCust.Bt2CusFbTranGUID;
                                //                Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2fCost), 3, MidpointRounding.AwayFromZero);
                                //                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2fCost), 3, MidpointRounding.AwayFromZero);
                                //                db.BPtInvLstReports.Add(Invreport);
                                //            }
                                //        }
                                //        #endregion

                                //        #region Invoice V2FBB rate calculations

                                //        decimal? LES_RptV2fbCost = CDR_VceFBBCostPriceCT;
                                //        decimal? CUS_RptV2fbCost = 0;

                                //        foreach (var records in lstV2Fbb_Trans)
                                //        {
                                //            BSt2FbbTrans bs2Sec = new BSt2FbbTrans();
                                //            bs2Sec.Bt2CusFbTranStartDate = records.BCtTranStartDate;
                                //            bs2Sec.Bt2CusFbTranStartTime = records.BCtTranStartTime;
                                //            bs2Sec.Bt2CusFbTranDOrig = records.BCtTranOrigin;
                                //            bs2Sec.Bt2CusFbTranDest = records.BCtTranServDest;
                                //            bs2Sec.Bt2CusFbTranServType = CUS_ServTypeVce2FBB;

                                //            decimal? v2FBBTransSec = 0;
                                //            if (records.BCtTranUnitUsage != null)
                                //            {
                                //                v2FBBTransSec = getMinutes(records.BCtTranUnitUsage);
                                //            }

                                //            long SecconvertToMin = Convert.ToInt64(v2FBBTransSec);
                                //            bs2Sec.Bt2CusFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                //            bs2Sec.Bt2CusFbTranUnitType = lstUnityType.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                //            bs2Sec.Bt2CusFbTranTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.Bt2CusFbTranUsage * CUS_VoiceFBB), 3, MidpointRounding.AwayFromZero);

                                //            if (Invreport.BInvRprtSerType != null)
                                //                CUS_RptV2fbCost += bs2Sec.Bt2CusFbTranTotalUsage;

                                //            bs2Sec.Bt2CusFbTranComTypes = 2;

                                //            db.BSt2FbbTrans.Add(bs2Sec);
                                //        }

                                //        if (lstV2Fbb_Trans.Count > 0)
                                //        {
                                //            Invreport = new BPtInvLstReport();
                                //            try
                                //            {
                                //                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeVce2FBB).BsPtCusServiceTypes;
                                //            }
                                //            catch { Invreport.BInvRprtSerType = null; }

                                //            if (Invreport.BInvRprtSerType != null)
                                //            {
                                //                Invreport.BInvRptGuID = SecMtCust.Bt2CusFbTranGUID;
                                //                Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2fbCost), 3, MidpointRounding.AwayFromZero);
                                //                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2fbCost), 3, MidpointRounding.AwayFromZero);
                                //                db.BPtInvLstReports.Add(Invreport);
                                //            }
                                //        }
                                //        #endregion

                                //        #region Invoice V2 Iridium rate calc

                                //        decimal? LES_RptV2irdCost = CDR_VceIridiumCostPriceCT;
                                //        decimal? CUS_RptV2irdCost = 0;

                                //        foreach (var records in lstV2Iridium_Trans)
                                //        {
                                //            BSt2FbbTrans bs2Sec = new BSt2FbbTrans();
                                //            bs2Sec.Bt2CusFbTranStartDate = records.BCtTranStartDate;
                                //            bs2Sec.Bt2CusFbTranStartTime = records.BCtTranStartTime;
                                //            bs2Sec.Bt2CusFbTranDOrig = records.BCtTranOrigin;
                                //            bs2Sec.Bt2CusFbTranDest = records.BCtTranServDest;
                                //            bs2Sec.Bt2CusFbTranServType = CUS_ServTypeVce2Iridium;

                                //            decimal? v2IriTransSec = 0;
                                //            if (records.BCtTranUnitUsage != null)
                                //            {
                                //                v2IriTransSec = getMinutes(records.BCtTranUnitUsage);
                                //            }

                                //            long SecconvertToMin = Convert.ToInt64(v2IriTransSec);

                                //            bs2Sec.Bt2CusFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 3, MidpointRounding.AwayFromZero);
                                //            bs2Sec.Bt2CusFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                //            bs2Sec.Bt2CusFbTranTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.Bt2CusFbTranUsage * CUS_VoiceIridium), 3, MidpointRounding.AwayFromZero);

                                //            if (Invreport.BInvRprtSerType != null)
                                //                CUS_RptV2irdCost += bs2Sec.Bt2CusFbTranTotalUsage;

                                //            bs2Sec.Bt2CusFbTranComTypes = 2;

                                //            db.BSt2FbbTrans.Add(bs2Sec);
                                //        }

                                //        // Invoice List Report Add in BInvLstReports
                                //        if (lstV2Iridium_Trans.Count > 0)
                                //        {
                                //            Invreport = new BPtInvLstReport();
                                //            try
                                //            {
                                //                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeVce2Iridium).BsPtCusServiceTypes;
                                //            }
                                //            catch { Invreport.BInvRprtSerType = null; }

                                //            if (Invreport.BInvRprtSerType != null)
                                //            {
                                //                Invreport.BInvRptGuID = SecMtCust.Bt2CusFbTranGUID;
                                //                Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2irdCost), 3, MidpointRounding.AwayFromZero);
                                //                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2irdCost), 3, MidpointRounding.AwayFromZero);
                                //                db.BPtInvLstReports.Add(Invreport);
                                //            }
                                //        }
                                //        #endregion

                                //        #region Invoice ISDN rate calc

                                //        decimal? LES_RptV2isdnCost = CDR_ISDNCostPriceCT;
                                //        decimal? CUS_RptV2isdnCost = 0;

                                //        foreach (var records in lstISDN_Trans)
                                //        {
                                //            BSt2FbbTrans bs2Sec = new BSt2FbbTrans();
                                //            bs2Sec.Bt2CusFbTranStartDate = records.BCtTranStartDate;
                                //            bs2Sec.Bt2CusFbTranStartTime = records.BCtTranStartTime;
                                //            bs2Sec.Bt2CusFbTranDOrig = records.BCtTranOrigin;
                                //            bs2Sec.Bt2CusFbTranDest = records.BCtTranServDest;
                                //            bs2Sec.Bt2CusFbTranServType = CUS_ServTypeVce2ISDN;

                                //            decimal? v2ISDNTransSec = 0;
                                //            if (records.BCtTranUnitUsage != null)
                                //            {
                                //                v2ISDNTransSec = getMinutes(records.BCtTranUnitUsage);
                                //            }

                                //            long SecconvertToMin = Convert.ToInt64(v2ISDNTransSec);

                                //            bs2Sec.Bt2CusFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 3, MidpointRounding.AwayFromZero);
                                //            bs2Sec.Bt2CusFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                //            bs2Sec.Bt2CusFbTranTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.Bt2CusFbTranUsage * CUS_ISDN), 3, MidpointRounding.AwayFromZero);

                                //            if (Invreport.BInvRprtSerType != null)
                                //                CUS_RptV2isdnCost += bs2Sec.Bt2CusFbTranTotalUsage;

                                //            bs2Sec.Bt2CusFbTranComTypes = 2;

                                //            db.BSt2FbbTrans.Add(bs2Sec);
                                //        }

                                //        // Invoice List Report Add in BInvLstReports
                                //        if (lstISDN_Trans.Count > 0)
                                //        {
                                //            Invreport = new BPtInvLstReport();
                                //            try
                                //            {
                                //                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeVce2ISDN).BsPtCusServiceTypes;
                                //            }
                                //            catch { Invreport.BInvRprtSerType = null; }

                                //            if (Invreport.BInvRprtSerType != null)
                                //            {
                                //                Invreport.BInvRptGuID = SecMtCust.Bt2CusFbTranGUID;
                                //                Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2isdnCost), 3, MidpointRounding.AwayFromZero);
                                //                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2isdnCost), 3, MidpointRounding.AwayFromZero);
                                //                db.BPtInvLstReports.Add(Invreport);
                                //            }
                                //        }
                                //        #endregion

                                //        #region Invoice SMS rate Calc

                                //        int commtype = 1;
                                //        try
                                //        {
                                //            commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                                //        }
                                //        catch
                                //        {
                                //            commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                                //        }

                                //        decimal? LES_RptV2smsCost = CDR_SMSCostPriceCT;
                                //        decimal? CUS_RptV2smsCost = 0;

                                //        foreach (var records in lstSMS_Trans)
                                //        {
                                //            BSt2FbbTrans bs2Sec = new BSt2FbbTrans();
                                //            bs2Sec.Bt2CusFbTranStartDate = records.BCtTranStartDate;
                                //            bs2Sec.Bt2CusFbTranStartTime = records.BCtTranStartTime;
                                //            bs2Sec.Bt2CusFbTranDOrig = records.BCtTranOrigin;
                                //            bs2Sec.Bt2CusFbTranDest = records.BCtTranServDest;
                                //            bs2Sec.Bt2CusFbTranServType = CUS_ServTypeSMS;

                                //            //long SecconvertToMin = Convert.ToInt64(records.BsCtBillIncre);
                                //            bs2Sec.Bt2CusFbTranUsage = Convert.ToInt32(records.BCtTranUnitUsage); //(decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                //            try
                                //            {
                                //                bs2Sec.Bt2CusFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("message")).UID;
                                //            }
                                //            catch
                                //            {
                                //                bs2Sec.Bt2CusFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Message")).UID;
                                //            }

                                //            bs2Sec.Bt2CusFbTranTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.Bt2CusFbTranUsage * CUS_SMS), 3, MidpointRounding.AwayFromZero);

                                //            if (Invreport.BInvRprtSerType != null)
                                //                CUS_RptV2smsCost += bs2Sec.Bt2CusFbTranTotalUsage;

                                //            bs2Sec.Bt2CusFbTranComTypes = commtype;

                                //            db.BSt2FbbTrans.Add(bs2Sec);
                                //        }

                                //        // Invoice List Report Add in BInvLstReports
                                //        if (lstSMS_Trans.Count > 0)
                                //        {
                                //            Invreport = new BPtInvLstReport();
                                //            try
                                //            {
                                //                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeSMS).BsPtCusServiceTypes;
                                //            }
                                //            catch { Invreport.BInvRprtSerType = null; }
                                //            if (Invreport.BInvRprtSerType != null)
                                //            {
                                //                Invreport.BInvRptGuID = SecMtCust.Bt2CusFbTranGUID;
                                //                Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2smsCost), 3, MidpointRounding.AwayFromZero);
                                //                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2smsCost), 3, MidpointRounding.AwayFromZero);
                                //                db.BPtInvLstReports.Add(Invreport);
                                //            }
                                //        }

                                //        #endregion

                                //        #region Invoice statement
                                //        // List of Satatement 
                                //        //Bs2Statement statment = null;
                                //        //try
                                //        //{
                                //        //    statment = db.Bs2Statement.FirstOrDefault(t => t.fkBs2CusID == SecFBBID);
                                //        //}
                                //        //catch { }

                                //        //if (statment == null)
                                //        //{
                                //        //    statment = new Bs2Statement();

                                //        //    statment.fkBs2CusID = SecFBBID;
                                //        //    statment.fkSimID = SimID;
                                //        //    statment.Bs2StTotalCP = Les_TotalCostCP;
                                //        //    statment.Bs2StTotalSP = Cus_TotalCostSP;
                                //        //    statment.Bs2StSMTSCost = SMTS_TotalCost;
                                //        //    statment.fkSPID = LesID;
                                //        //    statment.fkCusID = CUS_Id;
                                //        //    statment.Bs2StIvoiceDate = DateTime.Now.Date;
                                //        //    statment.Bs2StDuration = CT_Fbs.BCtTranServDest;

                                //        //    if (statment.Bs2StTotalCP > 0 && statment.Bs2StTotalSP > 0)
                                //        //    {
                                //        //        decimal CrossProfit = Convert.ToDecimal(statment.Bs2StTotalSP - statment.Bs2StSMTSCost);
                                //        //        decimal Margin = (CrossProfit / Convert.ToDecimal(statment.Bs2StTotalSP) * 100);
                                //        //        statment.Bs2stMargin = Margin > 0 ? Convert.ToInt32(Margin) : (int)Math.Abs(Margin);
                                //        //    }

                                //        //    db.Bs2Statement.Add(statment);
                                //        //}
                                //        #endregion

                                //    }
                                //}
                                //else
                                //{
                                //    BSt1FbbTrans SecRejLes = new BSt1FbbTrans();

                                //    SecRejLes.Bt1LesFbLES = Convert.ToString(LesID);
                                //    SecRejLes.Bt1LesFbLesRegID = LES_RegID;
                                //    SecRejLes.Bt1LesFbCusRegID = CUS_RegsID;
                                //    SecRejLes.Bt1LesFbMsidnId = ImarSatID;
                                //    SecRejLes.Bt1LesFbIMSIId = ImsiID;
                                //    SecRejLes.Bt1LesFbLesRefCode = CT_Fbs.BCtFbLesRefCode;
                                //    SecRejLes.Bt1LesFbSatCode = CT_Fbs.BCtFbSatCode;
                                //    SecRejLes.Bt1LesFbEquipmentCode = CT_Fbs.BCtFbEquipmentCode;
                                //    SecRejLes.Bt1LesFbCdrCode = CT_Fbs.BCtFbCdrCode;
                                //    SecRejLes.Bt1LesFbTranDest = CT_Fbs.BCtTranServDest;
                                //    SecRejLes.Bt1LesFbServiceType = CT_Fbs.BCtTranServType;
                                //    SecRejLes.Bt1LesFbTranServType = null;
                                //    SecRejLes.Bt1LesFbTranUsage = Convert.ToInt32(CT_Fbs.BCtTranUnitUsage);
                                //    SecRejLes.Bt1LesFbTranUnitType = 10;
                                //    SecRejLes.Bt1LesFbRecurFee = LES_RecurFee;
                                //    SecRejLes.Bt1LesFbRecurFeeType = CT_Fbs.BCtAFbRecurFeeType;
                                //    SecRejLes.Bt1LesFbBillno = CT_Fbs.BCtFbBillNo;
                                //    SecRejLes.Bt1LesFbBillPeriod = CT_Fbs.BCtFbBillPeriod;
                                //    SecRejLes.Bt1LesFbBillDate = CT_Fbs.BCtFbBillDate;

                                //    Guid guid = new Guid();
                                //    SecRejLes.Bt1LesGUID = guid.ToString();

                                //    db.BSt1FbbTrans.Add(SecRejLes);


                                //    if (CDR_RecuFee < CDR_DataCP && isOutbundleUsed)
                                //    {
                                //        LES_InvRptDataCostTot = CDR_DataCP - CDR_RecuFee;
                                //        LES_InvRptDataCostTot = Math.Round(Convert.ToDecimal(LES_InvRptDataCostTot), 2, MidpointRounding.AwayFromZero);
                                //    }
                                //    else
                                //        LES_InvRptDataCostTot = 0;

                                //    BPtInvLstReport InvreportCost = new BPtInvLstReport();
                                //    if (SecRejLes.Bt1LesFbServiceType != null)
                                //    {
                                //        InvreportCost.BInvRptGuID = SecRejLes.Bt1LesGUID;
                                //        InvreportCost.BInvRprtSerType = SecRejLes.Bt1LesFbServiceType;
                                //        InvreportCost.BInvRprtTotCst = CDR_RecuFee;
                                //        InvreportCost.BInvRprtTotBil = null;

                                //        db.BPtInvLstReports.Add(InvreportCost);
                                //    }

                                //    BSt1LesRejLogs lesRejLog = new BSt1LesRejLogs();
                                //    lesRejLog.Bs1LesRejGuID = SecRejLes.Bt1LesGUID;
                                //    lesRejLog.Bs1lesRejection = Msg.ToString();
                                //    lesRejLog.Bs1lesRejRemark = "";
                                //    db.BSt1LesRejLogs.Add(lesRejLog);

                                //    #region Standard IP Tranasction

                                //    BSt1FbbTrans lesRej1;
                                //    foreach (var records in lstStdIP_Trans)
                                //    {
                                //        lesRej1 = new BSt1FbbTrans();
                                //        lesRej1.Bt1LesFbTranStartDate = records.BCtTranStartDate;
                                //        lesRej1.Bt1LesFbTranStartTime = records.BCtTranStartTime;
                                //        lesRej1.Bt1LesFbTranDOrig = records.BCtTranOrigin;
                                //        lesRej1.Bt1LesFbTranDest = records.BCtTranServDest;
                                //        lesRej1.Bt1LesFbTranServType = ServTypeID;

                                //        double dataconvertToMP = Convert.ToDouble(records.BCtTranUnitUsage);
                                //        lesRej1.Bt1LesFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertBitToMegabytes(dataconvertToMP)), 2, MidpointRounding.AwayFromZero);
                                //        lesRej1.Bt1LesFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("MB")).UID;
                                //        lesRej1.Bt1LesFbTranTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bt1LesFbTranUsage * LES_DataRateper1MB), 3, MidpointRounding.AwayFromZero);
                                //        lesRej1.Bt1LesFbTranComTypes = 1;

                                //        db.BSt1FbbTrans.Add(lesRej1);
                                //    }

                                //    // Invoice List Report Add in BInvLstReports
                                //    if (lstStdIP_Trans.Count > 0)
                                //    {
                                //        InvreportCost = new BPtInvLstReport();
                                //        try
                                //        {
                                //            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeID).BsPtServiceTypes;
                                //        }
                                //        catch { InvreportCost.BInvRprtSerType = null; }

                                //        if (InvreportCost.BInvRprtSerType != null)
                                //        {
                                //            InvreportCost.BInvRptGuID = SecRejLes.Bt1LesGUID;
                                //            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_InvRptDataCostTot), 2, MidpointRounding.AwayFromZero);
                                //            InvreportCost.BInvRprtTotBil = null;
                                //            db.BPtInvLstReports.Add(InvreportCost);
                                //        }
                                //    }
                                //    #endregion

                                //    #region Reject2 V2 Cellular Trans

                                //    foreach (var records in lstV2Cell_Trans)
                                //    {
                                //        lesRej1 = new BSt1FbbTrans();
                                //        lesRej1.Bt1LesFbTranStartDate = records.BCtTranStartDate;
                                //        lesRej1.Bt1LesFbTranStartTime = records.BCtTranStartTime;
                                //        lesRej1.Bt1LesFbTranDOrig = records.BCtTranOrigin;
                                //        lesRej1.Bt1LesFbTranDest = records.BCtTranServDest;
                                //        lesRej1.Bt1LesFbTranServType = ServTypeMobile;

                                //        decimal? v2CTransSec = 0;
                                //        if (records.BCtTranUnitUsage != null)
                                //        {
                                //            v2CTransSec = getMinutes(records.BCtTranUnitUsage);
                                //        }

                                //        long SecconvertToMin = Convert.ToInt64(v2CTransSec);
                                //        lesRej1.Bt1LesFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                //        lesRej1.Bt1LesFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                //        lesRej1.Bt1LesFbTranTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bt1LesFbTranUsage * LES_VoiceCellular), 3, MidpointRounding.AwayFromZero);
                                //        lesRej1.Bt1LesFbTranComTypes = 2;

                                //        db.BSt1FbbTrans.Add(lesRej1);
                                //    }

                                //    // Invoice List Report Add in BInvLstReports
                                //    if (lstV2Cell_Trans.Count > 0)
                                //    {
                                //        InvreportCost = new BPtInvLstReport();
                                //        try
                                //        {
                                //            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeMobile).BsPtServiceTypes;
                                //        }
                                //        catch { InvreportCost.BInvRprtSerType = null; }

                                //        if (InvreportCost.BInvRprtSerType != null)
                                //        {
                                //            InvreportCost.BInvRptGuID = SecRejLes.Bt1LesGUID;
                                //            //InvreportCost.fkBCus2ID = null;
                                //            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_VceCellularCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                //            InvreportCost.BInvRprtTotBil = null;
                                //            db.BPtInvLstReports.Add(InvreportCost);
                                //        }
                                //    }
                                //    #endregion

                                //    #region Reject V2Fixed Trans

                                //    foreach (var records in lstV2Fixed_Trans)
                                //    {
                                //        lesRej1 = new BSt1FbbTrans();
                                //        lesRej1.Bt1LesFbTranStartDate = records.BCtTranStartDate;
                                //        lesRej1.Bt1LesFbTranStartTime = records.BCtTranStartTime;
                                //        lesRej1.Bt1LesFbTranDOrig = records.BCtTranOrigin;
                                //        lesRej1.Bt1LesFbTranDest = records.BCtTranServDest;
                                //        lesRej1.Bt1LesFbTranServType = ServTypeVce2Fixed;

                                //        decimal? v2FixedTransSec = 0;
                                //        if (records.BCtTranUnitUsage != null)
                                //        {
                                //            v2FixedTransSec = getMinutes(records.BCtTranUnitUsage);
                                //        }

                                //        long SecconvertToMin = Convert.ToInt64(v2FixedTransSec);
                                //        lesRej1.Bt1LesFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                //        lesRej1.Bt1LesFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                //        lesRej1.Bt1LesFbTranTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bt1LesFbTranUsage * LES_VoiceFixed), 3, MidpointRounding.AwayFromZero);
                                //        lesRej1.Bt1LesFbTranComTypes = 2;

                                //        db.BSt1FbbTrans.Add(lesRej1);
                                //    }

                                //    // Invoice List Report Add in BInvLstReports
                                //    if (lstV2Fixed_Trans.Count > 0)
                                //    {
                                //        InvreportCost = new BPtInvLstReport();
                                //        try
                                //        {
                                //            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeVce2Fixed).BsPtServiceTypes;
                                //        }
                                //        catch { InvreportCost.BInvRprtSerType = null; }

                                //        if (InvreportCost.BInvRprtSerType != null)
                                //        {
                                //            InvreportCost.BInvRptGuID = SecRejLes.Bt1LesGUID;
                                //            //InvreportCost.fkBCus2ID = null;
                                //            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_VceFixedCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                //            InvreportCost.BInvRprtTotBil = null;
                                //            db.BPtInvLstReports.Add(InvreportCost);
                                //        }
                                //    }
                                //    #endregion

                                //    #region Reject V2FBB Trans

                                //    foreach (var records in lstV2Fixed_Trans)
                                //    {
                                //        lesRej1 = new BSt1FbbTrans();
                                //        lesRej1.Bt1LesFbTranStartDate = records.BCtTranStartDate;
                                //        lesRej1.Bt1LesFbTranStartTime = records.BCtTranStartTime;
                                //        lesRej1.Bt1LesFbTranDOrig = records.BCtTranOrigin;
                                //        lesRej1.Bt1LesFbTranDest = records.BCtTranServDest;
                                //        lesRej1.Bt1LesFbTranServType = ServTypeVce2FBB;

                                //        decimal? v2FBBTransSec = 0;
                                //        if (records.BCtTranUnitUsage != null)
                                //        {
                                //            v2FBBTransSec = getMinutes(records.BCtTranUnitUsage);
                                //        }

                                //        long SecconvertToMin = Convert.ToInt64(v2FBBTransSec);
                                //        lesRej1.Bt1LesFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                //        lesRej1.Bt1LesFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                //        lesRej1.Bt1LesFbTranTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bt1LesFbTranUsage * LES_VoiceFBB), 3, MidpointRounding.AwayFromZero);
                                //        lesRej1.Bt1LesFbTranComTypes = 2;

                                //        db.BSt1FbbTrans.Add(lesRej1);
                                //    }

                                //    // Invoice List Report Add in BInvLstReports
                                //    if (lstV2Fixed_Trans.Count > 0)
                                //    {
                                //        InvreportCost = new BPtInvLstReport();
                                //        try
                                //        {
                                //            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeVce2FBB).BsPtServiceTypes;
                                //        }
                                //        catch { InvreportCost.BInvRprtSerType = null; }

                                //        if (InvreportCost.BInvRprtSerType != null)
                                //        {
                                //            InvreportCost.BInvRptGuID = SecRejLes.Bt1LesGUID;
                                //            //InvreportCost.fkBCus2ID = null;
                                //            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_VceFBBCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                //            InvreportCost.BInvRprtTotBil = null;
                                //            db.BPtInvLstReports.Add(InvreportCost);
                                //        }
                                //    }
                                //    #endregion

                                //    #region Reject V2Iridium

                                //    foreach (var records in lstV2Iridium_Trans)
                                //    {
                                //        lesRej1 = new BSt1FbbTrans();
                                //        lesRej1.Bt1LesFbTranStartDate = records.BCtTranStartDate;
                                //        lesRej1.Bt1LesFbTranStartTime = records.BCtTranStartTime;
                                //        lesRej1.Bt1LesFbTranDOrig = records.BCtTranOrigin;
                                //        lesRej1.Bt1LesFbTranDest = records.BCtTranServDest;
                                //        lesRej1.Bt1LesFbTranServType = LES_ServTypeVce2Iridium;

                                //        decimal? v2IriTransSec = 0;
                                //        if (records.BCtTranUnitUsage != null)
                                //        {
                                //            v2IriTransSec = getMinutes(records.BCtTranUnitUsage);
                                //        }

                                //        long SecconvertToMin = Convert.ToInt64(v2IriTransSec);

                                //        lesRej1.Bt1LesFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                //        lesRej1.Bt1LesFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                //        lesRej1.Bt1LesFbTranTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bt1LesFbTranUsage * LES_VoiceIridium), 3, MidpointRounding.AwayFromZero);
                                //        lesRej1.Bt1LesFbTranComTypes = 2;

                                //        db.BSt1FbbTrans.Add(lesRej1);
                                //    }

                                //    // Invoice List Report Add in BInvLstReports
                                //    if (lstV2Iridium_Trans.Count > 0)
                                //    {
                                //        InvreportCost = new BPtInvLstReport();
                                //        try
                                //        {
                                //            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == LES_ServTypeVce2Iridium).BsPtServiceTypes;
                                //        }
                                //        catch { InvreportCost.BInvRprtSerType = null; }

                                //        if (InvreportCost.BInvRprtSerType != null)
                                //        {
                                //            InvreportCost.BInvRptGuID = SecRejLes.Bt1LesGUID;
                                //            //InvreportCost.fkBCus2ID = null;
                                //            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_VceIridiumCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                //            InvreportCost.BInvRprtTotBil = null;
                                //            db.BPtInvLstReports.Add(InvreportCost);
                                //        }
                                //    }
                                //    #endregion

                                //    #region RejectionConditionISDN Trans

                                //    foreach (var records in lstISDN_Trans)
                                //    {
                                //        lesRej1 = new BSt1FbbTrans();
                                //        lesRej1.Bt1LesFbTranStartDate = records.BCtTranStartDate;
                                //        lesRej1.Bt1LesFbTranStartTime = records.BCtTranStartTime;
                                //        lesRej1.Bt1LesFbTranDOrig = records.BCtTranOrigin;
                                //        lesRej1.Bt1LesFbTranDest = records.BCtTranServDest;
                                //        lesRej1.Bt1LesFbTranServType = ServTypeVce2ISDN;

                                //        decimal? v2ISDNTransSec = 0;
                                //        if (records.BCtTranUnitUsage != null)
                                //        {
                                //            v2ISDNTransSec = getMinutes(records.BCtTranUnitUsage);
                                //        }

                                //        long SecconvertToMin = Convert.ToInt64(v2ISDNTransSec);

                                //        lesRej1.Bt1LesFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                //        lesRej1.Bt1LesFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                //        lesRej1.Bt1LesFbTranTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bt1LesFbTranUsage * LES_ISDN), 3, MidpointRounding.AwayFromZero);
                                //        lesRej1.Bt1LesFbTranComTypes = 2;

                                //        db.BSt1FbbTrans.Add(lesRej1);
                                //    }

                                //    // Invoice List Report Add in BInvLstReports
                                //    if (lstISDN_Trans.Count > 0)
                                //    {
                                //        InvreportCost = new BPtInvLstReport();
                                //        try
                                //        {
                                //            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeVce2ISDN).BsPtServiceTypes;
                                //        }
                                //        catch { InvreportCost.BInvRprtSerType = null; }

                                //        if (InvreportCost.BInvRprtSerType != null)
                                //        {
                                //            InvreportCost.BInvRptGuID = SecRejLes.Bt1LesGUID;
                                //            //InvreportCost.fkBCus2ID = null;
                                //            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_ISDNCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                //            InvreportCost.BInvRprtTotBil = null;
                                //            db.BPtInvLstReports.Add(InvreportCost);
                                //        }
                                //    }
                                //    #endregion

                                //    #region Reject SMS Trans

                                //    int commtype = 1;
                                //    try
                                //    {
                                //        commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                                //    }
                                //    catch
                                //    {
                                //        commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                                //    }

                                //    foreach (var records in lstSMS_Trans)
                                //    {
                                //        lesRej1 = new BSt1FbbTrans();
                                //        lesRej1.Bt1LesFbTranStartDate = records.BCtTranStartDate;
                                //        lesRej1.Bt1LesFbTranStartTime = records.BCtTranStartTime;
                                //        lesRej1.Bt1LesFbTranDOrig = records.BCtTranOrigin;
                                //        lesRej1.Bt1LesFbTranDest = records.BCtTranServDest;
                                //        lesRej1.Bt1LesFbTranServType = ServTypeSMS;

                                //        //long SecconvertToMin = Convert.ToInt64(records.BCtBTransUsage);
                                //        lesRej1.Bt1LesFbTranUsage = Convert.ToInt32(records.BCtTranUnitUsage);//(decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                //        try
                                //        {
                                //            lesRej1.Bt1LesFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("message")).UID;
                                //        }
                                //        catch
                                //        {
                                //            lesRej1.Bt1LesFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Message")).UID;
                                //        }

                                //        lesRej1.Bt1LesFbTranTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bt1LesFbTranUsage * LES_SMS), 3, MidpointRounding.AwayFromZero);
                                //        lesRej1.Bt1LesFbTranComTypes = commtype;

                                //        db.BSt1FbbTrans.Add(lesRej1);
                                //    }

                                //    // Invoice List Report Add in BInvLstReports
                                //    if (lstSMS_Trans.Count > 0)
                                //    {
                                //        InvreportCost = new BPtInvLstReport();
                                //        try
                                //        {
                                //            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeSMS).BsPtServiceTypes;
                                //        }
                                //        catch { InvreportCost.BInvRprtSerType = null; }

                                //        if (InvreportCost.BInvRprtSerType != null)
                                //        {
                                //            InvreportCost.BInvRptGuID = SecRejLes.Bt1LesGUID;
                                //            //InvreportCost.fkBCus2ID = null;
                                //            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_SMSCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                //            InvreportCost.BInvRprtTotBil = null;
                                //            db.BPtInvLstReports.Add(InvreportCost);
                                //        }
                                //    }

                                //    #endregion
                                //}
                                #endregion
                            }
                            catch (Exception error)
                            {
                                Msg.AppendFormat("Error:ID:{0}, {1}", ImarSatID, error.Message.ToString());
                            }

                            i++;
                        }

                        stopwatch.Stop();
                        returnMsg.AppendFormat("Collectiong all record into DB took Time: {0}", stopwatch.ElapsedMilliseconds);
                        try
                        {
                            stopwatch = new Stopwatch();
                            stopwatch.Start();
                            db.SaveChanges();
                            stopwatch.Stop();
                            returnMsg.AppendFormat("Save Changes all record into DB took Time: {0}", stopwatch.ElapsedMilliseconds);
                        }
                        catch (Exception error)
                        {
                            throw error;
                        }
                    }
                }
                catch (Exception error)
                {
                    throw error;
                    //Msg.AppendFormat("Error:ID:{0}, {1}", ImarSatID, error.Message.ToString());
                }
            }

            htmlString.AppendLine("</table>");
            return new Tuple<List<ErrorDetails>, string>(lstError, htmlString.ToString());
        }

        public Tuple<Stream, string> WriteExcelFromBCtFbb(List<CommonTableVM> lstID)
        {
            System.IO.Stream spreadsheetstream = new System.IO.MemoryStream();

            string Invoiceno = string.Empty;

            int value = 0;

            try
            {
                string imsiID = string.Empty;
                string invoiceNo = string.Empty;
                string ImarsatID = string.Empty;

                string buildingName = string.Empty;
                string street = string.Empty;
                string district = string.Empty;
                string city = string.Empty;
                string country = string.Empty;

                string VesselName = string.Empty;

                string CustomerName = string.Empty;

                BillingSystemEntities db = new BillingSystemEntities();

                DateTime dt = DateTime.Now;
                string Period = dt.ToString("MMM");
                DateTime dy = DateTime.Today;
                string currYear = dy.Year.ToString();

                string CurrPeriod = Period + '-' + currYear;
                string InvoiceFileName = invoiceNo + '_' + buildingName + '_' + VesselName;

                string pathid = Guid.NewGuid().ToString() + " " + DateTime.Now.Date.ToString("dd-MM-yyyy");

                #region WorkBook Prepartion

                XLWorkbook xLWorkbook = new XLWorkbook();
                IXLWorksheet workSheet = xLWorkbook.Worksheets.Add("Invoice" + Invoiceno);


                // Work Sheet Header

                workSheet.Cell(1, 1).Value = "BctID";
                workSheet.Cell(1, 2).Value = "BCtFbMSIDNNo";
                workSheet.Cell(1, 3).Value = "BCtFbICCID";
                workSheet.Cell(1, 4).Value = "BCtFbImImsi";

                workSheet.Cell(1, 5).Value = "NA";
                workSheet.Cell(1, 6).Value = "NA0";
                workSheet.Cell(1, 7).Value = "NA1";
                workSheet.Cell(1, 8).Value = "NA2";

                workSheet.Cell(1, 9).Value = "BCtFbbLES";
                workSheet.Cell(1, 10).Value = "BCtFbContNo";
                workSheet.Cell(1, 11).Value = "BCtFbCdrCode";

                workSheet.Cell(1, 12).Value = "NA3";
                workSheet.Cell(1, 13).Value = "NA4";

                workSheet.Cell(1, 14).Value = "BCtFbLesRefCode";
                workSheet.Cell(1, 15).Value = "BCtFbSatCode";
                workSheet.Cell(1, 16).Value = "BCtFbEquipmentCode";

                workSheet.Cell(1, 17).Value = "NA5";
                workSheet.Cell(1, 18).Value = "NA6";

                workSheet.Cell(1, 19).Value = "BCtFbRecurFee";
                workSheet.Cell(1, 20).Value = "BCtAFbRecurFeeType";

                workSheet.Cell(1, 21).Value = "NA7";
                workSheet.Cell(1, 22).Value = "NA8";
                workSheet.Cell(1, 23).Value = "NA9";
                workSheet.Cell(1, 24).Value = "NA10";

                workSheet.Cell(1, 25).Value = "BCtTranStartDate";
                workSheet.Cell(1, 26).Value = "BCtTranEndDate";
                workSheet.Cell(1, 27).Value = "BCtTranStartTime";
                workSheet.Cell(1, 28).Value = "NA11";

                workSheet.Cell(1, 29).Value = "BCtTranOrigin";
                workSheet.Cell(1, 30).Value = "NA12";
                workSheet.Cell(1, 31).Value = "BCtTranServType";
                workSheet.Cell(1, 32).Value = "BCtTranServDest";

                workSheet.Cell(1, 33).Value = "NA13";
                workSheet.Cell(1, 34).Value = "NA14";

                workSheet.Cell(1, 35).Value = "BCtTranUnitUsage";
                workSheet.Cell(1, 36).Value = "BCtTranUnitType";
                workSheet.Cell(1, 37).Value = "BCtTranTotalUsage";
                workSheet.Cell(1, 38).Value = "NA15";

                workSheet.Cell(1, 39).Value = "BCtTranComTypes";
                workSheet.Cell(1, 40).Value = "BCtTranBillIncr";

                workSheet.Cell(1, 41).Value = "NA16";
                workSheet.Cell(1, 42).Value = "NA17";
                workSheet.Cell(1, 43).Value = "NA18";
                workSheet.Cell(1, 44).Value = "NA19";
                workSheet.Cell(1, 45).Value = "NA20";

                workSheet.Cell(1, 46).Value = "BCtFbBillNo";
                workSheet.Cell(1, 47).Value = "BCtFbBillPeriod";
                workSheet.Cell(1, 48).Value = "BCtFbBillDate";

                workSheet.Cell(1, 49).Value = "NA21";
                workSheet.Cell(1, 50).Value = "NA22";

                workSheet.Cell(1, 51).Value = "BCtFbSIMSingle";
                workSheet.Cell(1, 52).Value = "BCtFbDualSim";
                workSheet.Cell(1, 53).Value = "BCtFbMultiVce";

                workSheet.Cell(1, 54).Value = "NA23";
                workSheet.Cell(1, 55).Value = "NA24";

                workSheet.Cell(1, 56).Value = "BCtFbContStartDate";
                workSheet.Cell(1, 57).Value = "BCtFbContEndDate";

                workSheet.Cell(1, 58).Value = "NA25";
                workSheet.Cell(1, 59).Value = "NA26";

                workSheet.Cell(1, 60).Value = "BCtFbTerm";
                workSheet.Cell(1, 61).Value = "BCtAFbRgnFee";
                workSheet.Cell(1, 62).Value = "BCtAFbActFee";
                workSheet.Cell(1, 63).Value = "BCtAFbCardFee";

                workSheet.Cell(1, 64).Value = "NA27";
                workSheet.Cell(1, 65).Value = "NA28";
                workSheet.Cell(1, 66).Value = "NA29";
                workSheet.Cell(1, 67).Value = "NA30";


                int row = 2;
                decimal? totals = 0;
                int sno = 1;

                foreach (var ct in lstID)
                {
                    workSheet.Cell(row, 1).Value = ct.BctID;

                    workSheet.Cell(row, 2).Style.NumberFormat.Format = "@";
                    workSheet.Cell(row, 2).Value = ct.BCtFbMSIDNNo;

                    workSheet.Cell(row, 3).Style.NumberFormat.Format = "@";
                    workSheet.Cell(row, 3).Value = ct.BCtFbICCID;

                    workSheet.Cell(row, 4).Style.NumberFormat.Format = "@";
                    workSheet.Cell(row, 4).Value = ct.BCtFbImImsi;

                    workSheet.Cell(row, 5).Value = ct.NA;
                    workSheet.Cell(row, 6).Value = ct.NA0;
                    workSheet.Cell(row, 7).Value = ct.NA1;
                    workSheet.Cell(row, 8).Value = ct.NA2;

                    workSheet.Cell(row, 9).Value = ct.BCtFbbLES;
                    workSheet.Cell(row, 10).Value = ct.BCtFbContNo;
                    workSheet.Cell(row, 11).Value = ct.BCtFbCdrCode;

                    string satCode = string.Empty;
                    string EquipCode = string.Empty;
                    string LesRefCode = string.Empty;

                    workSheet.Cell(row, 12).Value = ct.NA3;
                    workSheet.Cell(row, 13).Value = ct.NA4;

                    try
                    {
                        LesRefCode = db.BsMtLESRefs.FirstOrDefault(t => t.LesRefID == ct.BCtFbLesRefCode).LESCode;
                    }
                    catch { }

                    try
                    {
                        satCode = db.BsMtSatTypes.FirstOrDefault(t => t.MsSatType == ct.BCtFbSatCode).Satcode;
                    }
                    catch
                    {
                    }

                    try
                    {
                        EquipCode = db.BsMtEquipTypes.FirstOrDefault(t => t.EquipTypeID == ct.BCtFbEquipmentCode).EquipmentCode;
                    }
                    catch { }

                    workSheet.Cell(row, 14).Value = LesRefCode;
                    workSheet.Cell(row, 15).Value = satCode;
                    workSheet.Cell(row, 16).Value = EquipCode;

                    workSheet.Cell(row, 17).Value = ct.NA5;
                    workSheet.Cell(row, 18).Value = ct.NA6;

                    workSheet.Cell(row, 17).Value = ct.NA5;
                    workSheet.Cell(row, 18).Value = ct.NA6;

                    if (ct.BCtTranServType.Contains("Subscription Fee"))
                    {
                        workSheet.Cell(row, 19).Value = ct.BCtTranTotalUsage;
                        workSheet.Cell(row, 19).Value = ct.BCtFbRecurFee;
                    }

                    workSheet.Cell(row, 20).Value = ct.BCtAFbRecurFeeType;

                    workSheet.Cell(row, 21).Value = ct.NA7;
                    workSheet.Cell(row, 22).Value = ct.NA8;
                    workSheet.Cell(row, 23).Value = ct.NA9;
                    workSheet.Cell(row, 24).Value = ct.NA10;

                    workSheet.Cell(row, 25).Style.NumberFormat.Format = "dd-mm-yyyy"; ;
                    workSheet.Cell(row, 25).Value = ct.BCtTranStartDate;
                    workSheet.Cell(row, 26).Style.NumberFormat.Format = "dd-mm-yyyy"; ;
                    workSheet.Cell(row, 26).Value = ct.BCtTranEndDate;

                    workSheet.Cell(row, 27).Style.NumberFormat.Format = "hh:mm:ss";
                    workSheet.Cell(row, 27).Value = ct.BCtTranStartTime;
                    workSheet.Cell(row, 28).Value = ct.NA11;

                    workSheet.Cell(row, 29).Value = ct.BCtTranOrigin;
                    workSheet.Cell(row, 30).Value = ct.NA12;


                    workSheet.Cell(row, 31).Value = ct.BCtTranServType;

                    workSheet.Cell(row, 32).Value = ct.BCtTranServDest;

                    workSheet.Cell(row, 33).Value = ct.NA13;
                    workSheet.Cell(row, 34).Value = ct.NA14;

                    workSheet.Cell(row, 35).Style.NumberFormat.Format = "0.00";
                    workSheet.Cell(row, 35).DataType = XLDataType.Number;
                    workSheet.Cell(row, 35).Value = ct.BCtTranUnitUsage;
                    workSheet.Cell(row, 36).Value = ct.BCtTranUnitType;
                    if (!ct.BCtTranServType.Contains("Subscription Fee"))
                    {
                        workSheet.Cell(row, 37).Style.NumberFormat.Format = "0.00"; ;
                        workSheet.Cell(row, 37).DataType = XLDataType.Number;
                        workSheet.Cell(row, 37).Value = ct.BCtTranTotalUsage;
                    }
                    else
                    {
                        workSheet.Cell(row, 37).Style.NumberFormat.Format = "0.00"; ;
                        workSheet.Cell(row, 37).DataType = XLDataType.Number;
                        workSheet.Cell(row, 37).Value = 0.00;
                    }
                    workSheet.Cell(row, 38).Value = ct.NA15;

                    workSheet.Cell(row, 39).Value = ct.BCtTranComTypes;
                    workSheet.Cell(row, 40).Value = ct.BCtTranBillIncr;

                    workSheet.Cell(row, 41).Value = ct.NA16;
                    workSheet.Cell(row, 42).Value = ct.NA17;
                    workSheet.Cell(row, 43).Value = ct.NA18;
                    workSheet.Cell(row, 44).Value = ct.NA19;
                    workSheet.Cell(row, 45).Value = ct.NA20;

                    workSheet.Cell(row, 46).Value = ct.BCtFbBillNo;
                    workSheet.Cell(row, 47).Value = ct.BCtFbBillPeriod;
                    workSheet.Cell(row, 48).Value = ct.BCtFbBillDate;

                    workSheet.Cell(row, 49).Value = ct.NA21;
                    workSheet.Cell(row, 50).Value = ct.NA22;

                    workSheet.Cell(row, 51).Value = ct.BCtFbSIMSingle;
                    workSheet.Cell(row, 52).Value = ct.BCtFbDualSim;
                    workSheet.Cell(row, 53).Value = ct.BCtFbMultiVce;

                    workSheet.Cell(row, 54).Value = ct.NA23;
                    workSheet.Cell(row, 55).Value = ct.NA24;

                    workSheet.Cell(row, 56).Value = ct.BCtFbContStartDate;
                    workSheet.Cell(row, 57).Value = ct.BCtFbContEndDate;
                    workSheet.Cell(row, 58).Value = ct.NA25;
                    workSheet.Cell(row, 59).Value = ct.NA26;

                    workSheet.Cell(row, 60).Value = ct.BCtFbTerm;
                    workSheet.Cell(row, 61).Value = ct.BCtFbRgnFee;
                    workSheet.Cell(row, 62).Value = ct.BCtFbActFee;
                    workSheet.Cell(row, 63).Value = ct.BCtFbCardFee;

                    workSheet.Cell(row, 64).Value = ct.NA27;
                    workSheet.Cell(row, 65).Value = ct.NA28;
                    workSheet.Cell(row, 66).Value = ct.NA29;
                    workSheet.Cell(row, 67).Value = ct.NA30;

                    row++;

                }
                #endregion

                // Save Work sheet into workbook
                xLWorkbook.SaveAs(spreadsheetstream);
                spreadsheetstream.Position = 0;

                //InvoiceFileName = temppath + "\\" + InvoiceFileName;

                //FileStream fileStream = new FileStream(InvoiceFileName, FileMode.Create);

                return new Tuple<Stream, string>(spreadsheetstream, "CommonTableDatas.xlsx");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            throw new Exception("The master primary is not an interger, please check and update the issue.");
        }
    }
}