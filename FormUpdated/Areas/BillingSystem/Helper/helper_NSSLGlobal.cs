﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data;
using System.Text.RegularExpressions;
using ClosedXML.Excel;
using FormUpdated.Areas.BillingSystem.Models;
using System.Text;

namespace FormUpdated.Areas.BillingSystem
{
    public class helper_NSSLGlobal
    {
        public static string LesName { get; set; } = "NSSL Global";
        public int LesID { get; set; } = getLesID();

        public enum CommunicationType { Data = 1, Voice, Fax, SMS }

        public enum UOM { KB = 1, MB, GB, TB, Bytes }

        private static int CellReferenceToIndex(Cell cell)
        {
            int index = 0;
            string reference = cell.CellReference.ToString().ToUpper();
            foreach (char ch in reference)
            {
                if (Char.IsLetter(ch))
                {
                    int value = (int)ch - (int)'A';
                    index = (index == 0) ? value : ((index + 1) * 26) + value;
                }
                else
                    return index;
            }
            return index;
        }

        public static string GetColumnName(string cellReference)
        {
            // Create a regular expression to match the column name portion of the cell name.
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellReference);
            return match.Value;
        }

        public static int? GetColumnIndexFromName(string columnName)
        {

            //return columnIndex;
            string name = columnName;
            int number = 0;
            int pow = 1;
            for (int i = name.Length - 1; i >= 0; i--)
            {
                number += (name[i] - 'A' + 1) * pow;
                pow *= 26;
            }
            return number;
        }

        public static string GetDestionation(string dest)
        {
            var result = string.Empty;
            if (dest.ToLower().Contains("subscription"))
            {
                var regex = dest.Split(new string[] { "Subscription" }, StringSplitOptions.None);
                result = regex[1].TrimStart();
                result = result.TrimEnd();
            }
            return result;
        }

        public Tuple<List<int>, string> NSSLtoCT(string fileName, int LesRefID)
        {
            List<int> ctMasterID = new List<int>();
            StringBuilder ErrMsg = new StringBuilder();
            try
            {
                BillingSystemEntities db = new BillingSystemEntities();
                List<Dictionary<string, dynamic>> pairs = GetDictfromExcel(fileName);
                var imsi = pairs.Select(ims => ims["imsi"]).Distinct().ToList();

                int ctID = 0;

                foreach (var id in imsi)
                {
                    bool isNotExist = false;
                    List<Dictionary<string, dynamic>> keyValues = pairs.Where(t => t["imsi"].Equals(id)).ToList();
                    List<Dictionary<string, dynamic>> dictFee = keyValues.Where(t => t["imsi"].Equals(id) && t["type"].Equals("FEE")).ToList();

                    // Subscribtion Fee to store FBB Master table
                    foreach (Dictionary<string, dynamic> dictRecord in dictFee)
                    {
                        if (dictRecord.Count > 0)
                        {
                            //if (dictRecord.ContainsKey("type"))
                            //{
                            //    string types = dictRecord["type"];

                            //    switch (types.ToUpper())
                            //    {
                            //        case "FEE":
                            //            if (dictRecord.ContainsKey("description"))
                            //            {
                            //                if (dictRecord["description"].ToLower().Contains("monthly subscription"))
                            //                {
                            //                    BCtA ctMaster = new BCtA();
                            //                    ctMaster.BCtAfkLESID = LesID;
                            //                    string imsiID = Convert.ToString(id);
                            //                    try
                            //                    {
                            //                        var SimReg = db.BptFbSims.FirstOrDefault(t => t.BPtFBImImsi.Equals(imsiID));
                            //                        if (SimReg != null)
                            //                        {
                            //                            ctMaster.BCtAfkSimID = SimReg.Sim_ID;
                            //                            ctMaster.fkLesRefID = LesID;
                            //                            ctMaster.fkSatTypeID = SimReg.fkSatTypeID;
                            //                            ctMaster.fkEquipType = SimReg.fkEquipTypeID;
                            //                            ctMaster.BCtAfkLESCID = db.BsPtSPContracts.FirstOrDefault(t => t.fkSPID == LesID).spcID;
                            //                            ctMaster.BCtAfkLesRegID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSPID == LesID
                            //                                                                                    && t.fkSimID == ctMaster.BCtAfkSimID).SPLesRegID;
                            //                        }
                            //                        else
                            //                        {
                            //                            ErrMsg.AppendFormat("The IMSI:{0} SIM is not registered in our Database. Please check", id).AppendLine();
                            //                            isNotExist = true;
                            //                            continue;
                            //                        }
                            //                    }
                            //                    catch (Exception eror)
                            //                    {
                            //                        ErrMsg.AppendFormat("The IMSI:{0} getting error. Please return the transaction", eror.Message.ToString()).AppendLine();
                            //                        isNotExist = true;
                            //                        continue;
                            //                    }

                            //                    ctMaster.BCtAFbLesCode = null;
                            //                    ctMaster.BCtAFbCdrCode = dictRecord["cdr_id"];
                            //                    ctMaster.BCtAFbServDest = GetDestionation(dictRecord["description"]);
                            //                    ctMaster.BCtAFbServType = dictRecord["description"];
                            //                    if (dictRecord.ContainsKey("quantity_charged") && dictRecord["quantity_charged"] != null)
                            //                        ctMaster.BCtAFbTransUsage = Convert.ToInt32(dictRecord["quantity_charged"]);

                            //                    //ctMaster.BCMtFbUOM = dictRecord["quantity_charged"];
                            //                    if (dictRecord.ContainsKey("isp_price") && dictRecord["isp_price"] != null)
                            //                    {
                            //                        try
                            //                        {
                            //                            ctMaster.BCtAFbRecurFee = Convert.ToDecimal(dictRecord["isp_price"]);
                            //                        }
                            //                        catch
                            //                        {
                            //                            ctMaster.BCtAFbRecurFee = 0;
                            //                        }
                            //                    }
                            //                    //ctMaster.BCMtFbRecurFeeType ="";
                            //                    //ctMaster.BCMtFbSimSingle = 0;
                            //                    //ctMaster.BCMtFbSimDual = 0;
                            //                    //ctMaster.BCMtFbMultiVce = 0
                            //                    //ctMaster.BCMtFbContNo = "";
                            //                    //ctMaster.BCMtFbCardFee = 0;
                            //                    //ctMaster.BCMtFbActFee = 0;
                            //                    //ctMaster.BCMtFbRgnFee = 0;
                            //                    db.BCtAs.Add(ctMaster);
                            //                    db.SaveChanges();
                            //                    ctID = ctMaster.BCtAID;
                            //                    ctMasterID.Add(ctID);
                            //                }
                            //            }
                            //            break;
                            //    }
                            //}
                        }
                    }

                    keyValues = pairs.Where(t => t["imsi"].Equals(id) && !t["type"].Equals("FEE")).ToList();

                    // Call data record store to FBB common table
                    if (!isNotExist)
                    {
                        //foreach (Dictionary<string, dynamic> dictRecord in keyValues)
                        //{
                        //    if (dictRecord.Count > 0)
                        //    {
                        //        if (dictRecord.ContainsKey("type") && dictRecord["type"] != null)
                        //        {
                        //            string types = dictRecord["type"];

                        //            switch (types.ToUpper())
                        //            {
                        //                case "DATA":
                        //                    if (dictRecord.ContainsKey("description"))
                        //                    {
                        //                        if (dictRecord["description"].ToLower().Contains("background ip"))
                        //                        {
                        //                            BCtB ctRecord = new BCtB();
                        //                            ctRecord.BCtBfkFbID = ctID;

                        //                            string dateTime = Convert.ToString(dictRecord["call_start"]);
                        //                            var splitdatetime = splitDateTime(dateTime);
                        //                            ctRecord.BCtBStartDate = splitdatetime.Item1;
                        //                            //ctRecord.BsCtEndtDate
                        //                            ctRecord.BCtBStartTime = splitdatetime.Item2;
                        //                            //ctRecord.BsCtEndTime 
                        //                            //ctRecord.BsCtOrigion
                        //                            ctRecord.BCtBDestination = dictRecord["b_number"];
                        //                            string ServiceTypes = dictRecord["description"];
                        //                            ctRecord.BCtBLesServTypeID = db.BsPtSPServiceDests.FirstOrDefault(t => t.BsPtServiceTypes.ToLower().Equals(ServiceTypes.ToLower())).SPServDID;
                        //                            if (dictRecord.ContainsKey("quantity(byte)") && dictRecord["quantity(byte)"] != null)
                        //                            {
                        //                                try
                        //                                {
                        //                                    ctRecord.BCtBTransUsage = Convert.ToDecimal(dictRecord["quantity(byte)"]);
                        //                                }
                        //                                catch
                        //                                {
                        //                                    ctRecord.BCtBTransUsage = 0;
                        //                                }
                        //                            }
                        //                            ctRecord.BCtBfkUOMID = 9;
                        //                            if (dictRecord.ContainsKey("isp_price") && dictRecord["isp_price"] != null)
                        //                            {
                        //                                try
                        //                                {
                        //                                    ctRecord.BCtBTotalUsage = Convert.ToDecimal(dictRecord["isp_price"]);
                        //                                }
                        //                                catch
                        //                                {
                        //                                    ctRecord.BCtBTotalUsage = 0;
                        //                                }
                        //                            }

                        //                            ctRecord.BCtBfkCommType = 1;
                        //                            db.BCtBs.Add(ctRecord);
                        //                        }
                        //                    }
                        //                    break;

                        //                case "VOICE":
                        //                    if (dictRecord.ContainsKey("description"))
                        //                    {
                        //                        if (dictRecord["description"].Contains("Basic voice") || dictRecord["description"].ToLower().Contains("prepaid                                                                                                                         server"))
                        //                        {
                        //                            BCtB ctRecord = new BCtB();
                        //                            ctRecord.BCtBfkFbID = ctID;

                        //                            string dateTime = Convert.ToString(dictRecord["call_start"]);
                        //                            var splitdatetime = splitDateTime(dateTime);
                        //                            ctRecord.BCtBStartDate = splitdatetime.Item1;
                        //                            //ctRecord.BsCtEndtDate
                        //                            ctRecord.BCtBStartTime = splitdatetime.Item2;
                        //                            //ctRecord.BsCtEndTime 
                        //                            //ctRecord.BsCtOrigion
                        //                            ctRecord.BCtBDestination = dictRecord["b_number"];
                        //                            string ServiceTypes = dictRecord["description"];
                        //                            ctRecord.BCtBLesServTypeID = db.BsPtSPServiceDests.FirstOrDefault(t => t.BsPtServiceTypes.ToLower().Equals(ServiceTypes.ToLower())).SPServDID;
                        //                            if (dictRecord.ContainsKey("quantity_charged") && dictRecord["quantity_charged"] != null)
                        //                            {
                        //                                try
                        //                                {
                        //                                    ctRecord.BCtBBillIncr = Convert.ToDecimal(dictRecord["quantity_charged"]);
                        //                                }
                        //                                catch
                        //                                {
                        //                                    ctRecord.BCtBBillIncr = 0;
                        //                                }
                        //                            }
                        //                            ctRecord.BCtBfkUOMID = 6;

                        //                            if (dictRecord.ContainsKey("isp_price") && dictRecord["isp_price"] != null)
                        //                            {
                        //                                try
                        //                                {
                        //                                    ctRecord.BCtBTotalUsage = Convert.ToDecimal(dictRecord["isp_price"]);
                        //                                }
                        //                                catch
                        //                                {
                        //                                    ctRecord.BCtBTotalUsage = 0;
                        //                                }
                        //                            }
                        //                            ctRecord.BCtBfkCommType = Convert.ToInt32(CommunicationType.Voice);
                        //                            db.BCtBs.Add(ctRecord);
                        //                        }
                        //                    }
                        //                    break;

                        //                case "SMS":
                        //                    if (dictRecord.ContainsKey("description"))
                        //                    {
                        //                        if (dictRecord["description"].Contains("SMS"))
                        //                        {
                        //                            BCtB ctRecord = new BCtB();
                        //                            ctRecord.BCtBfkFbID = ctID;

                        //                            string dateTime = Convert.ToString(dictRecord["call_start"]);
                        //                            var splitdatetime = splitDateTime(dateTime);
                        //                            ctRecord.BCtBStartDate = splitdatetime.Item1;
                        //                            //ctRecord.BsCtEndtDate
                        //                            ctRecord.BCtBStartTime = splitdatetime.Item2;
                        //                            //ctRecord.BsCtEndTime 
                        //                            //ctRecord.BsCtOrigion
                        //                            ctRecord.BCtBDestination = dictRecord["b_number"];
                        //                            string ServiceTypes = dictRecord["description"];
                        //                            ctRecord.BCtBLesServTypeID = db.BsPtSPServiceDests.FirstOrDefault(t => t.BsPtServiceTypes.ToLower().Equals(ServiceTypes.ToLower())).SPServDID;
                        //                            if (dictRecord.ContainsKey("quantity_charged") && dictRecord["quantity_charged"] != null)
                        //                            {
                        //                                try
                        //                                {
                        //                                    ctRecord.BCtBTransUsage = Convert.ToDecimal(dictRecord["quantity_charged"]);
                        //                                }
                        //                                catch
                        //                                {
                        //                                    ctRecord.BCtBTransUsage = 0;
                        //                                }
                        //                            }
                        //                            ctRecord.BCtBfkUOMID = 8;

                        //                            if (dictRecord.ContainsKey("isp_price") && dictRecord["isp_price"] != null)
                        //                            {
                        //                                try
                        //                                {
                        //                                    ctRecord.BCtBTotalUsage = Convert.ToDecimal(dictRecord["isp_price"]);
                        //                                }
                        //                                catch
                        //                                {
                        //                                    ctRecord.BCtBTotalUsage = 0;
                        //                                }
                        //                            }
                        //                            ctRecord.BCtBfkCommType = Convert.ToInt32(CommunicationType.SMS);
                        //                            db.BCtBs.Add(ctRecord);
                        //                        }
                        //                    }
                        //                    break;
                        //            }
                        //        }
                        //    }
                        //}
                    }
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        string errormsg = string.Empty;
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            errormsg = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors: ", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                errormsg = string.Format(" - Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        ErrMsg.AppendFormat("MSI NO:{0}, Error:{1}", id, errormsg).AppendLine();
                    }
                    catch (Exception error)
                    {
                        ErrMsg.AppendFormat("MSI NO:{0}, Error:{1}", id, error.Message.ToString() + error.StackTrace).AppendLine();
                    }
                }


                return new Tuple<List<int>, string>(ctMasterID, ErrMsg.ToString());
            }
            catch (Exception e)
            {
                return new Tuple<List<int>, string>(ctMasterID, e.Message.ToString() + e.StackTrace);
            }
        }

        public List<Dictionary<string, dynamic>> GetDictfromExcel(string filename)
        {
            List<Dictionary<string, dynamic>> lstPairs = new List<Dictionary<string, dynamic>>();
            try
            {
                DataTable dt = new DataTable();
                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(filename, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();

                    List<string> receiptField = new List<string>();
                    string ColName = string.Empty;
                    string colValue = string.Empty;

                    foreach (Cell cell in rows.ElementAt(0))
                    {
                        ColName = GetCellValue(spreadSheetDocument, cell);
                        if (ColName.StartsWith(" "))
                            ColName = ColName.Substring(1);
                        receiptField.Add(System.Text.RegularExpressions.Regex.Replace(ColName, @"\t|\n|\r", ""));
                    }

                    foreach (Row row in rows) //this will also include your header row...
                    {
                        if (row.RowIndex != 1)
                        {
                            Dictionary<string, dynamic> dictPairs = new Dictionary<string, dynamic>();

                            string[] tempData = new string[receiptField.Count];
                            int columnIndex = 0;

                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                // Gets the column index of the cell with data
                                int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
                                cellColumnIndex--; //zero based index
                                if (columnIndex < cellColumnIndex)
                                {
                                    do
                                    {
                                        tempData[columnIndex] = ""; //Insert blank data here;
                                        columnIndex++;
                                    }
                                    while (columnIndex < cellColumnIndex);
                                }
                                tempData[columnIndex] = GetCellValue(spreadSheetDocument, cell);

                                columnIndex++;
                            }

                            for (int i = 0, arrIndex = 0; i < receiptField.Count; i++, arrIndex++) ///row.Descendants<Cell>().Count() replaced
                            {
                                try
                                {
                                    colValue = tempData[arrIndex];
                                }
                                catch
                                {
                                    if (receiptField[arrIndex].ToLower().Equals("isp_price"))
                                        colValue = "0.00";
                                }

                                switch (receiptField[arrIndex].ToLower())
                                {
                                    case "cdr_id":
                                        dictPairs.Add("cdr_id", colValue);
                                        break;
                                    case "msisdn":
                                        dictPairs.Add("msisdn", colValue);
                                        break;
                                    case "imsi":
                                        dictPairs.Add("imsi", colValue);
                                        break;
                                    case "billing period":
                                        dictPairs.Add("Billing Period", colValue);
                                        break;
                                    case "call_start":
                                        double date = Convert.ToDouble(colValue);
                                        DateTime dateTime = DateTime.FromOADate(date);
                                        dictPairs.Add("call_start", dateTime);
                                        break;
                                    case "event_id":
                                        dictPairs.Add("event_id", colValue);
                                        break;
                                    case "description":
                                        dictPairs.Add("description", colValue.TrimStart().TrimEnd());
                                        break;
                                    case "b_number":
                                        dictPairs.Add("b_number", colValue);
                                        break;
                                    case "bundle_indicator":
                                        dictPairs.Add("bundle_indicator", colValue);
                                        break;
                                    case "unit_price":
                                        dictPairs.Add("unit_price", colValue);
                                        break;
                                    case "quantity_charged":
                                        dictPairs.Add("quantity_charged", colValue);
                                        break;
                                    case "quantity(byte)":
                                        dictPairs.Add("quantity(byte)", colValue);
                                        break;
                                    case "type":
                                        if (colValue.Equals(0))
                                            colValue = "DATA";

                                        dictPairs.Add("type", colValue);
                                        break;
                                    case "isp_price":
                                        dictPairs.Add("isp_price", Convert.ToDecimal(colValue));
                                        break;
                                }
                            }

                            if (dictPairs.Count > 0)
                            {
                                lstPairs.Add(dictPairs);
                            }
                        }
                    }
                }
                return lstPairs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string RejectionCondition(List<int> ID, string errorMsg)
        {

            System.Text.StringBuilder returnMsg = new System.Text.StringBuilder();
            returnMsg.Append(errorMsg).AppendLine();

            //foreach (int id in ID)
            //{
            //    System.Text.StringBuilder Msg = new System.Text.StringBuilder();

            //    int SimID = 0;
            //    decimal? CDR_RecuFee = 0;
            //    string ImarSatID = string.Empty;
            //    List<BCtB> lstCtRecord = new List<BCtB>();
            //    List<BsPtSPServiceDest> lstLES_ServiceType = new List<BsPtSPServiceDest>();

            //    decimal? SMTS_TotalCost = 0;
            //    decimal? Les_TotalCostCP = 0;
            //    decimal? Cus_TotalCostSP = 0;

            //    int? LES_ContractID = 0;

            //    decimal? CDR_VcePrepaidServerUintsCT = null;
            //    decimal? CDR_VcePrepaidServerCostPriceCT = null;

            //    int? ServTypePadServer = null;

            //    decimal? LES_DataPlan = 0;
            //    decimal? LES_OutbundleRate = 0;
            //    //decimal? LES_VoiceCellular = 0;
            //    //decimal? LES_VoiceFixed = 0;
            //    decimal? LES_VoiceFBB = 0;
            //    decimal? LES_MonthlySub3 = 0;
            //    decimal? LES_MonthlySub30 = 0;
            //    decimal? LES_SMS = 0;

            //    int LES_RegID = 0;
            //    decimal? LES_RecurFee = 0;
            //    decimal? LES_DataRateper1MB = 0;
            //    decimal? LES_VoiceFBBCellular = 0;
            //    decimal? LES_VoiceFBBPSTN = 0;
            //    decimal? LES_PrepaidServer = 0;

            //    decimal? LES_InvRptDataCostTot = 0;

            //    int CUS_Id = 0;
            //    int CUS_ContractID = 0;
            //    int CUS_RegsID = 0;
            //    decimal? CUS_RecurFee = 0;
            //    decimal? CUS_DataRateper1MB = 0;
            //    decimal? CUS_VoiceFBBCellular = 0;
            //    decimal? CUS_VoiceFBBPSTN = 0;
            //    decimal? CUS_PrepaidServer = 0;

            //    decimal? Cus_DataPlan = 0;
            //    decimal? Cus_OutbundleRate = 0;
            //    //decimal? CUS_VoiceCellular = 0;
            //    //decimal? CUS_VoiceFixed = 0;
            //    decimal? CUS_VoiceFBB = 0;
            //    decimal? CUS_MonthlySub3 = 0;
            //    decimal? CUS_MonthlySub30 = 0;
            //    decimal? CUS_SMS = 0;

            //    DateTime? LES_ContrRegStrDate = null;
            //    DateTime? LES_ContrRegEndDate = null;

            //    DateTime? LES_ContrBarStrDate = null;
            //    DateTime? LES_ContrBarEndDate = null;

            //    DateTime? LES_ContrSusStrDate = null;
            //    DateTime? LES_ContrSusEndDate = null;

            //    DateTime? LES_ContrLayoutStrDate = null;
            //    DateTime? LES_ContrLayoutEndDate = null;

            //    DateTime? CUS_ContrRegStrDate = null;
            //    DateTime? CUS_ContrRegEndDate = null;

            //    DateTime? CUS_ContrBarStrDate = null;
            //    DateTime? CUS_ContrBarEndDate = null;

            //    DateTime? CUS_ContrSusStrDate = null;
            //    DateTime? CUS_ContrSusEndDate = null;

            //    DateTime? CUS_ContrLayoutStrDate = null;
            //    DateTime? CUS_ContrLayoutEndDate = null;

            //    DateTime? CDR_StartDate = null;
            //    DateTime? CDR_EndDate = null;

            //    bool is_LESGBPlan = false;
            //    bool is_CUSGBPlan = false;

            //    int ServTypeID = 0;
            //    int? ServTypeMobile = 0;
            //    int? ServTypeFBB = 0;
            //    int? ServTypePSTN = 0;
            //    int? ServTypeMnthlySubc = 0;
            //    int? ServTypeSMS = 0;
            //    //int ServTypeVce2Fixed = 0;
            //    //int ServTypeVce2FBB = 0;
            //    //int LES_ServTypeVce2MonthlySub3 = 0;
            //    //int LES_ServTypeVce2MonthlySub30 = 0;
            //    //int ServTypeSMS = 0;

            //    int CUS_ServTypeID = 0;
            //    int CUS_ServTypeMobile = 0;
            //    int Cus_ServTypePSTN = 0;
            //    int Cus_ServTypeFBB = 0;
            //    int Cus_ServTypeMnthlySubc = 0;
            //    int Cus_ServTypeSMS = 0;
            //    //int CUS_ServTypeVce2Fixed = 0;
            //    //int CUS_ServTypeVce2FBB = 0;
            //    //int CUS_ServTypeVce2MonthlySub3 = 0;
            //    //int CUS_ServTypeVce2MonthlySub30 = 0;
            //    //int CUS_ServTypeSMS = 0;

            //    using (BillingSystemEntities db = new BillingSystemEntities())
            //    {

            //        try
            //        {
            //            lstCtRecord = db.BCtBs.Where(t => t.BCtBfkFbID == id).ToList();

            //            var CT_Fbs = db.BCtAs.Where(t => t.BCtAID.Equals(id)).FirstOrDefault();

            //            SimID = CT_Fbs.BCtAfkSimID;
            //            CDR_RecuFee = CT_Fbs.BCtAFbRecurFee;
            //            ImarSatID = db.BptFbSims.FirstOrDefault(t => t.Sim_ID == SimID).BPtFBimMsisdn;

            //            try
            //            {
            //                CDR_StartDate = lstCtRecord.Min(t => t.BCtBStartDate);
            //                CDR_EndDate = lstCtRecord.Max(t => t.BCtBStartDate);
            //            }
            //            catch { }

            //            //To get customer price details---------------------------------------------------------------------------------------------
            //            #region Cus

            //            var cusRegs = db.BsPtCusRegs_.FirstOrDefault(t => t.fkSimID == SimID);
            //            if (cusRegs != null)
            //            {
            //                CUS_Id = Convert.ToInt32(cusRegs.fkCusID);
            //                CUS_ContractID = Convert.ToInt32(cusRegs.fkCuscID);
            //                CUS_RegsID = Convert.ToInt32(cusRegs.Cus_RegID);

            //                CUS_RecurFee = cusRegs.BPtCusRgnFbRecurFee;

            //                Cus_DataPlan = Convert.ToDecimal(cusRegs.BPtCusRgnFBDataPlanIn);

            //                CUS_DataRateper1MB = cusRegs.BPtCusRgnFbInbundleDataRate;
            //                Cus_OutbundleRate = cusRegs.BPtCusRgnFbOutBundleDataRate;

            //                CUS_ContrRegStrDate = cusRegs.BsPtCusRegStartdate;
            //                CUS_ContrRegEndDate = cusRegs.BsPtCusRegEnd;

            //                CUS_ContrBarStrDate = cusRegs.BsPtCusRegFBBarDate;
            //                CUS_ContrBarEndDate = cusRegs.BsPtCusRegFbBarDateLifted;

            //                CUS_ContrSusStrDate = cusRegs.BsPtCusRegFBSusDate;
            //                CUS_ContrSusEndDate = cusRegs.BsPtCusRegFBSusDateLifted;

            //                CUS_ContrLayoutStrDate = cusRegs.BsPtCusRegFBLayUpDate;
            //                CUS_ContrLayoutEndDate = cusRegs.BsPtCusRegFBLayUpDateLifted;

            //                string unit = cusRegs.BsMtUnit.Units;
            //                if (unit.Contains("GB"))
            //                {
            //                    is_CUSGBPlan = true;
            //                }
            //                else
            //                {
            //                    is_CUSGBPlan = false;
            //                }
            //            }
            //            else
            //            {
            //                Msg.AppendFormat("This IMSI {0} id is not registered in Customer registration. <br />", ImarSatID).AppendLine();
            //            }

            //            List<BsPtCusServiceDest> lstSP = db.BsPtCusServiceDests.Where(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID).ToList();
            //            try
            //            {
            //                CUS_VoiceFBBCellular = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Basic voice -FBB/Cellular")).BsPtCusServiceSellerPrice;
            //            }
            //            catch { CUS_VoiceFBBCellular = 0; }

            //            try
            //            {
            //                CUS_VoiceFBBPSTN = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Basic voice -FBB/PSTN")).BsPtCusServiceSellerPrice;
            //            }
            //            catch { CUS_VoiceFBBPSTN = 0; }
            //            try
            //            {
            //                CUS_VoiceFBB = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Basic voice -FBB/FBB")).BsPtCusServiceSellerPrice;
            //            }
            //            catch { LES_VoiceFBB = 0; }
            //            try
            //            {
            //                CUS_MonthlySub3 = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Monthly Subscription 2019-09-01 - 2019-09-03")).BsPtCusServiceSellerPrice;
            //            }
            //            catch { CUS_MonthlySub3 = 0; }

            //            try
            //            {
            //                CUS_MonthlySub30 = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Monthly Subscription 2019-09-01 - 2019-09-30")).BsPtCusServiceSellerPrice;
            //            }
            //            catch { CUS_MonthlySub30 = 0; }

            //            try
            //            {
            //                CUS_SMS = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("SMS")).BsPtCusServiceSellerPrice;
            //            }
            //            catch { CUS_SMS = 0; }
            //            #endregion
            //            //------------------------------------------------------------------------------------------------------------------

            //            #region LES
            //            //To get LES price details------------------------------------------------------------------------------------------

            //            var LES_services = db.BsPtSPServiceDests.ToList();
            //            try
            //            {
            //                try
            //                {
            //                    var Les_Reg = db.BsPtSPLesRegs.FirstOrDefault(t => t.SPLesRegID == CT_Fbs.BCtAfkLesRegID);

            //                    LES_RegID = Les_Reg.SPLesRegID;
            //                    LES_ContractID = Les_Reg.fkspcID;

            //                    LES_RecurFee = Les_Reg.BPtLesRgnFbRecurFee;
            //                    LES_DataPlan = Convert.ToDecimal(Les_Reg.BPtLesRgnFBDataPlanIn);

            //                    LES_DataRateper1MB = Les_Reg.BPtLesRgnFbInbundleDataRate;
            //                    LES_OutbundleRate = Les_Reg.BPtLesRgnFbOutBundleDataRate;

            //                    LES_ContrRegStrDate = Les_Reg.BsPtlesRegStartdate;
            //                    LES_ContrRegEndDate = Les_Reg.BsPtlesRegEnd;

            //                    LES_ContrBarStrDate = Les_Reg.BsPtLesRegFBBarDate;
            //                    LES_ContrBarEndDate = Les_Reg.BsPtLesRegFbBarDateLifted;

            //                    LES_ContrSusStrDate = Les_Reg.BsPtLesRegFBSusDate;
            //                    LES_ContrSusEndDate = Les_Reg.BsPtLesRegFBSusDateLifted;

            //                    LES_ContrLayoutStrDate = Les_Reg.BsPtLesRegFBLayUpDate;
            //                    LES_ContrLayoutEndDate = Les_Reg.BsPtLesRegFBLayUpDateLifted;

            //                    string unit = Les_Reg.BsMtUnit.Units;
            //                    if (unit.Contains("GB"))
            //                        is_LESGBPlan = true;
            //                    else
            //                        is_LESGBPlan = false;
            //                }
            //                catch { LES_RecurFee = 0; }

            //                lstLES_ServiceType = (from ls in LES_services where ls.fkSpID == LesID && ls.BPtServTfkLesRegID == LES_RegID select ls).ToList();

            //                try
            //                {
            //                    LES_VoiceFBBCellular = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
            //                                                                          t.BsPtServiceTypes.Equals("Basic voice -FBB/Cellular")).BsPtServiceCostPrice;
            //                }
            //                catch { LES_VoiceFBBCellular = 0; }

            //                try
            //                {
            //                    LES_VoiceFBBPSTN = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals("Basic voice -FBB/PSTN")).BsPtServiceCostPrice;
            //                }
            //                catch { LES_VoiceFBBPSTN = 0; }

            //                try
            //                {
            //                    LES_VoiceFBB = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals("Basic voice -FBB/FBB")).BsPtServiceCostPrice;
            //                }
            //                catch { LES_VoiceFBB = 0; }

            //                try
            //                {
            //                    LES_MonthlySub3 = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals("Monthly Subscription 2019-09-01 - 2019-09-03")).BsPtServiceCostPrice;
            //                }
            //                catch { LES_MonthlySub3 = 0; }

            //                try
            //                {
            //                    LES_MonthlySub30 = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals("Monthly Subscription 2019-09-01 - 2019-09-30")).BsPtServiceCostPrice;
            //                }
            //                catch { LES_MonthlySub30 = 0; }

            //                try
            //                {
            //                    LES_SMS = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals("SMS")).BsPtServiceCostPrice;
            //                }
            //                catch { LES_SMS = 0; }
            //            }
            //            catch (Exception e)
            //            {
            //                Msg.AppendFormat("Error:{0}", e.Message.ToString());
            //            }
            //            //----------------------------------------------------------------------------------------------------------------------------
            //            #endregion

            //            // Data

            //            try
            //            {
            //                ServTypeID = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BsPtServiceTypes.Equals("Background IP") && t.BPtServTfkLesRegID == LES_RegID).SPServDID;
            //                CUS_ServTypeID = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Background IP")).CusServDID;
            //            }
            //            catch
            //            {
            //                ServTypeID = 0;
            //                Msg.Append("The Background IP is Registed in the LES Service Description<br />.").AppendLine();
            //                //returnMsg.Append("The Background IP is Registed in the LES Service Description<br />.").AppendLine();
            //            }
            //            decimal? DataUnits_CDR = lstCtRecord.AsEnumerable()
            //                                          .Where(t => t.BCtBfkCommType == 1 && t.BCtBLesServTypeID == ServTypeID && (t.BCtBfkFbID.Equals(id)))
            //                                          .GroupBy(t => t.BCtBfkFbID.Equals(id))
            //                                          .Select(t => t.Sum(s => s.BCtBTransUsage)).FirstOrDefault();
            //            DataUnits_CDR = Math.Round(Convert.ToDecimal(DataUnits_CDR), 2, MidpointRounding.AwayFromZero);

            //            decimal? CDR_DataCP = lstCtRecord.AsEnumerable()
            //                                                    .Where(t => t.BCtBfkCommType == 1 && t.BCtBLesServTypeID == ServTypeID &&
            //                                                            (t.BCtBfkFbID.Equals(id)))
            //                                                    .GroupBy(t => t.BCtBfkFbID.Equals(id))
            //                                                    .Select(t => t.Sum(s => s.BCtBTotalUsage)).FirstOrDefault();
            //            CDR_DataCP = Math.Round(Convert.ToDecimal(CDR_DataCP), 2, MidpointRounding.AwayFromZero);

            //            SMTS_TotalCost += CDR_DataCP;
            //            //---- End Data Process

            //            #region VoicetoCellular
            //            // <--------------------------- Basic voice -FBB/Cellular  ----------------------------->

            //            try
            //            {
            //                ServTypeMobile = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals
            //                                                               ("Basic voice -FBB/Cellular")).SPServDID;
            //                CUS_ServTypeMobile = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Basic voice -FBB/Cellular")).CusServDID;
            //            }
            //            catch { ServTypeMobile = null; }

            //            decimal? CDR_VceFbbCellulardUintsCT = lstCtRecord.AsEnumerable()
            //                                                        .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeMobile &&
            //                                                            (t.BCtBfkFbID.Equals(id)))
            //                                                        .GroupBy(t => t.BCtBfkFbID.Equals(id))
            //                                                        .Select(t => t.Sum(s => s.BCtBTransUsage)).FirstOrDefault();

            //            if (CDR_VceFbbCellulardUintsCT != null)
            //                CDR_VceFbbCellulardUintsCT = Math.Round(Convert.ToDecimal(CDR_VceFbbCellulardUintsCT), 2, MidpointRounding.AwayFromZero);

            //            decimal? CDR_VceFbbCellularCostPriceCT = lstCtRecord.AsEnumerable()
            //                                                        .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeMobile &&
            //                                                            (t.BCtBfkFbID.Equals(id)))
            //                                                        .GroupBy(t => t.BCtBfkFbID.Equals(id))
            //                                                        .Select(t => t.Sum(s => s.BCtBTotalUsage)).FirstOrDefault();

            //            if (CDR_VceFbbCellularCostPriceCT != null)
            //                CDR_VceFbbCellularCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceFbbCellularCostPriceCT), 2, MidpointRounding.AwayFromZero);

            //            SMTS_TotalCost += CDR_VceFbbCellularCostPriceCT;
            //            // <--------------------------- End Basic voice -FBB/Cellular  ----------------------------->
            //            #endregion

            //            #region VoicetoPSTN
            //            // <--------------------------- Basic voice -FBB/PSTN  ----------------------------->
            //            try
            //            {
            //                ServTypePSTN = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals
            //                                                               ("Basic voice -FBB/PSTN")).SPServDID;
            //                Cus_ServTypePSTN = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Basic voice -FBB/PSTN")).CusServDID;
            //            }
            //            catch { ServTypePSTN = null; }


            //            decimal? CDR_VceFbbPsyndUintsCT = lstCtRecord.AsEnumerable()
            //                                                        .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypePSTN &&
            //                                                            (t.BCtBfkFbID.Equals(id)))
            //                                                        .GroupBy(t => t.BCtBfkFbID.Equals(id))
            //                                                        .Select(t => t.Sum(s => s.BCtBTransUsage)).FirstOrDefault();

            //            if (CDR_VceFbbPsyndUintsCT != null)
            //                CDR_VceFbbPsyndUintsCT = Math.Round(Convert.ToDecimal(CDR_VceFbbPsyndUintsCT), 2, MidpointRounding.AwayFromZero);

            //            decimal? CDR_VceFbbPSTNCostPriceCT = lstCtRecord.AsEnumerable()
            //                                                        .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypePSTN &&
            //                                                            (t.BCtBfkFbID.Equals(id)))
            //                                                        .GroupBy(t => t.BCtBfkFbID.Equals(id))
            //                                                        .Select(t => t.Sum(s => s.BCtBTotalUsage)).FirstOrDefault();

            //            if (CDR_VceFbbPSTNCostPriceCT != null)
            //                CDR_VceFbbPSTNCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceFbbPSTNCostPriceCT), 2, MidpointRounding.AwayFromZero);

            //            SMTS_TotalCost += CDR_VceFbbPSTNCostPriceCT;
            //            // <--------------------------- End Basic voice -FBB/PSTN  ----------------------------->
            //            #endregion

            //            #region VoicetoFBB
            //            // <--------------------------- Basic voice -FBB/FBB  ----------------------------->
            //            try
            //            {
            //                ServTypeFBB = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals
            //                                                               ("Basic voice -FBB/FBB")).SPServDID;
            //                Cus_ServTypeFBB = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Basic voice -FBB/FBB")).CusServDID;
            //            }
            //            catch { ServTypeFBB = null; }


            //            decimal? CDR_VoicetoFbbdUintsCT = lstCtRecord.AsEnumerable()
            //                                                        .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeFBB &&
            //                                                            (t.BCtBfkFbID.Equals(id)))
            //                                                        .GroupBy(t => t.BCtBfkFbID.Equals(id))
            //                                                        .Select(t => t.Sum(s => s.BCtBTransUsage)).FirstOrDefault();

            //            if (CDR_VoicetoFbbdUintsCT != null)
            //                CDR_VoicetoFbbdUintsCT = Math.Round(Convert.ToDecimal(CDR_VoicetoFbbdUintsCT), 2, MidpointRounding.AwayFromZero);

            //            decimal? CDR_VoicetoFbbCostPriceCT = lstCtRecord.AsEnumerable()
            //                                                        .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeFBB &&
            //                                                            (t.BCtBfkFbID.Equals(id)))
            //                                                        .GroupBy(t => t.BCtBfkFbID.Equals(id))
            //                                                        .Select(t => t.Sum(s => s.BCtBTotalUsage)).FirstOrDefault();

            //            if (CDR_VoicetoFbbCostPriceCT != null)
            //                CDR_VoicetoFbbCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VoicetoFbbCostPriceCT), 2, MidpointRounding.AwayFromZero);

            //            SMTS_TotalCost += CDR_VoicetoFbbCostPriceCT;
            //            // <--------------------------- End Basic voice -FBB/FBB  ----------------------------->
            //            #endregion

            //            // <--------------------------- Prepaid Server ----------------------------->
            //            //int? ServTypePadServer = null;
            //            int Cus_ServTypePadServer = 0;
            //            try
            //            {
            //                ServTypePadServer = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals("Prepaid Server")).SPServDID;
            //                Cus_ServTypePadServer = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Prepaid Server")).CusServDID;
            //            }
            //            catch { ServTypePadServer = null; }

            //            //decimal? CDR_VcePrepaidServerUintsCT = null;
            //            //decimal? CDR_VcePrepaidServerCostPriceCT = null;

            //            if (ServTypePadServer != null)
            //            {

            //                CDR_VcePrepaidServerUintsCT = lstCtRecord.AsEnumerable()
            //                                                            .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypePadServer &&
            //                                                                (t.BCtBfkFbID.Equals(id)))
            //                                                            .GroupBy(t => t.BCtBfkFbID.Equals(id))
            //                                                            .Select(t => t.Sum(s => s.BCtBTransUsage)).FirstOrDefault();

            //                if (CDR_VcePrepaidServerUintsCT != null)
            //                    CDR_VcePrepaidServerUintsCT = Math.Round(Convert.ToDecimal(CDR_VcePrepaidServerUintsCT), 2, MidpointRounding.AwayFromZero);

            //                CDR_VcePrepaidServerCostPriceCT = lstCtRecord.AsEnumerable()
            //                                                            .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypePadServer &&
            //                                                                (t.BCtBfkFbID.Equals(id)))
            //                                                            .GroupBy(t => t.BCtBfkFbID.Equals(id))
            //                                                            .Select(t => t.Sum(s => s.BCtBTotalUsage)).FirstOrDefault();

            //                if (CDR_VcePrepaidServerCostPriceCT != null)
            //                    CDR_VcePrepaidServerCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VcePrepaidServerCostPriceCT), 2, MidpointRounding.AwayFromZero);

            //                SMTS_TotalCost += CDR_VcePrepaidServerCostPriceCT;
            //            }
            //            // <--------------------------- End Prepaid Server  ----------------------------->

            //            #region Subscription fee
            //            // <--------------------------- Monthly Subscription 2019-10-01 - 2019-10-31  ----------------------------->
            //            try
            //            {
            //                ServTypeMnthlySubc = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals
            //                                                               ("Monthly Subscription 2019-10-01 - 2019-10-31")).SPServDID;
            //                Cus_ServTypeMnthlySubc = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Monthly Subscription 2019-10-01 - 2019-10-31")).CusServDID;
            //            }
            //            catch { ServTypeMnthlySubc = null; }


            //            decimal? CDR_MonthlySubcrdUintsCT = lstCtRecord.AsEnumerable()
            //                                                        .Where(t => t.BCtBLesServTypeID == ServTypeMnthlySubc &&
            //                                                            (t.BCtBfkFbID.Equals(id)))
            //                                                        .GroupBy(t => t.BCtBfkFbID.Equals(id))
            //                                                        .Select(t => t.Sum(s => s.BCtBTransUsage)).FirstOrDefault();

            //            if (CDR_MonthlySubcrdUintsCT != null)
            //                CDR_MonthlySubcrdUintsCT = Math.Round(Convert.ToDecimal(CDR_MonthlySubcrdUintsCT), 2, MidpointRounding.AwayFromZero);

            //            decimal? CDR_MonthlySubcrCostPriceCT = lstCtRecord.AsEnumerable()
            //                                                        .Where(t => t.BCtBLesServTypeID == ServTypeMnthlySubc &&
            //                                                            (t.BCtBfkFbID.Equals(id)))
            //                                                        .GroupBy(t => t.BCtBfkFbID.Equals(id))
            //                                                        .Select(t => t.Sum(s => s.BCtBTotalUsage)).FirstOrDefault();

            //            if (CDR_MonthlySubcrCostPriceCT != null)
            //                CDR_MonthlySubcrCostPriceCT = Math.Round(Convert.ToDecimal(CDR_MonthlySubcrCostPriceCT), 2, MidpointRounding.AwayFromZero);

            //            SMTS_TotalCost += CDR_MonthlySubcrCostPriceCT;
            //            // <--------------------------- Monthly Subscription 2019-10-01 - 2019-10-31  ----------------------------->
            //            #endregion

            //            #region SMS
            //            // <--------------------------- SMS  ----------------------------->
            //            try
            //            {
            //                ServTypeSMS = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals
            //                                                              ("SMS")).SPServDID;
            //                Cus_ServTypeSMS = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("SMS")).CusServDID;
            //            }
            //            catch { ServTypeSMS = null; }

            //            decimal? CDR_SMSdUintsCT = lstCtRecord.AsEnumerable()
            //                                                        .Where(t => t.BCtBLesServTypeID == ServTypeSMS &&
            //                                                            (t.BCtBfkFbID.Equals(id)))
            //                                                        .GroupBy(t => t.BCtBfkFbID.Equals(id))
            //                                                        .Select(t => t.Sum(s => s.BCtBTransUsage)).FirstOrDefault();

            //            if (CDR_SMSdUintsCT != null)
            //                CDR_SMSdUintsCT = Math.Round(Convert.ToDecimal(CDR_SMSdUintsCT), 2, MidpointRounding.AwayFromZero);

            //            decimal? CDR_SMSCostPriceCT = lstCtRecord.AsEnumerable()
            //                                                        .Where(t => t.BCtBLesServTypeID == ServTypeSMS &&
            //                                                            (t.BCtBfkFbID.Equals(id)))
            //                                                        .GroupBy(t => t.BCtBfkFbID.Equals(id))
            //                                                        .Select(t => t.Sum(s => s.BCtBTotalUsage)).FirstOrDefault();

            //            if (CDR_SMSCostPriceCT != null)
            //                CDR_SMSCostPriceCT = Math.Round(Convert.ToDecimal(CDR_SMSCostPriceCT), 2, MidpointRounding.AwayFromZero);

            //            SMTS_TotalCost += CDR_SMSCostPriceCT;
            //            // <--------------------------- SMS  ----------------------------->
            //            #endregion

            //            //decimal? LES_RecurFee = 0;
            //            //decimal? LES_DataRateper1MB = 0;
            //            //decimal? LES_VoiceFBBCellular = 0;
            //            //decimal? LES_VoiceFBBPSTN = 0;
            //            //decimal? LES_PrepaidServer = 0;

            //            //int CUS_Id = 0;
            //            //int CUS_ContractID = 0;
            //            //int CUS_RegsID = 0;
            //            //decimal? CUS_RecurFee = 0;
            //            //decimal? CUS_DataRateper1MB = 0;
            //            //decimal? CUS_VoiceFBBCellular = 0;
            //            //decimal? CUS_VoiceFBBPSTN = 0;
            //            //decimal? CUS_PrepaidServer = 0;

            //            /*try
            //            {
            //                int LES_ContractID = 0;

            //                List<BsPtSPServiceDest> lstCP = db.BsPtSPServiceDests.Where(t => t.fkSpID == LesID).ToList();
            //                try
            //                {
            //                    LES_RecurFee = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSPID == LesID && t.fkSimID == SimID).BPtLesRgnFbRecurFee;
            //                }
            //                catch { LES_RecurFee = 0; }
            //                try
            //                {
            //                    LES_DataRateper1MB = lstCP.FirstOrDefault(t => t.fkSpID == LesID && t.BsPtServiceTypes.Equals("Background IP")).BsPtServiceCostPrice;
            //                }
            //                catch { LES_DataRateper1MB = 0; }
            //                try
            //                {
            //                    LES_VoiceFBBCellular = lstCP.FirstOrDefault(t => t.fkSpID == LesID && t.BsPtServiceTypes.Equals("Basic voice -FBB/Cellular")).BsPtServiceCostPrice;
            //                }
            //                catch { LES_VoiceFBBCellular = 0; }
            //                try
            //                {
            //                    LES_VoiceFBBPSTN = lstCP.FirstOrDefault(t => t.fkSpID == LesID && t.BsPtServiceTypes.Equals("Basic voice -FBB/PSTN")).BsPtServiceCostPrice;
            //                }
            //                catch { LES_VoiceFBBPSTN = 0; }
            //                try
            //                {
            //                    LES_PrepaidServer = lstCP.FirstOrDefault(t => t.fkSpID == LesID && t.BsPtServiceTypes.Equals("Prepaid Server")).BsPtServiceCostPrice;
            //                }
            //                catch { LES_PrepaidServer = 0; }

            //                var cusRegs = db.BsPtCusRegs_.FirstOrDefault(t => t.fkSimID == SimID);
            //                if (cusRegs != null)
            //                {
            //                    CUS_Id = Convert.ToInt32(cusRegs.fkCusID);
            //                    CUS_ContractID = Convert.ToInt32(cusRegs.fkCuscID);
            //                    CUS_RegsID = Convert.ToInt32(cusRegs.Cus_RegID);

            //                    CUS_RecurFee = cusRegs.BPtCusRgnFbRecurFee;
            //                }
            //                else
            //                {
            //                    Msg.AppendFormat("This msi {0} id not registered in customer registered, please check.<br />", ImarSatID).AppendLine();
            //                    returnMsg.AppendFormat("This msi {0} id not registered in customer registered, please check.<br />", ImarSatID).AppendLine();
            //                }

            //                List<BsPtCusServiceDest> lstSP = db.BsPtCusServiceDests.Where(t => t.fkSpID == LesID).ToList();
            //                try
            //                {
            //                    CUS_DataRateper1MB = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BsPtCusServiceTypes.Equals("Background IP")).BsPtCusServiceSellerPrice;
            //                }
            //                catch { CUS_DataRateper1MB = 0; }
            //                try
            //                {
            //                    CUS_VoiceFBBCellular = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BsPtCusServiceTypes.Equals("Basic voice -FBB/Cellular")).BsPtCusServiceSellerPrice;
            //                }
            //                catch { CUS_VoiceFBBCellular = 0; }
            //                try
            //                {
            //                    CUS_VoiceFBBPSTN = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BsPtCusServiceTypes.Equals("Basic voice -FBB/PSTN")).BsPtCusServiceSellerPrice;
            //                }
            //                catch { CUS_VoiceFBBPSTN = 0; }
            //                try
            //                {
            //                    CUS_PrepaidServer = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BsPtCusServiceTypes.Equals("Prepaid Server")).BsPtCusServiceSellerPrice;
            //                }
            //                catch { CUS_PrepaidServer = 0; }

            //            }
            //            catch (Exception e) { }*/

            //            bool isRecurFeeMached = false;
            //            bool isDataMatched = false;
            //            bool isVoiceFbbCellurlarMatched = false;
            //            bool isVoiceFbbPSTNMatched = false;
            //            bool isVoiceFbbMatched = false;
            //            bool isPrepaidServerMatched = false;
            //            bool isSMSMatched = false;


            //            Les_TotalCostCP += LES_RecurFee;
            //            SMTS_TotalCost += CDR_RecuFee;

            //            bool isOutBundleused = false;
            //            decimal? OutbundleDataused = 0;
            //            decimal? CDR_OutbundleRate = 0;

            //            if ((CDR_RecuFee < LES_RecurFee || CDR_RecuFee.Equals(LES_RecurFee))
            //                                  && CDR_RecuFee < CUS_RecurFee)
            //            {
            //                isRecurFeeMached = true;
            //            }
            //            else
            //            {
            //                isRecurFeeMached = false;
            //                Msg.AppendFormat("This Inmarsat id {0}: Recuring Feee is not match.<br />", ImarSatID).AppendLine();
            //                Msg.AppendFormat("Recuring Feee from LES CDR Price:{0}.<br />", CDR_RecuFee).AppendLine();
            //                Msg.AppendFormat("Recuring Feee from LES Reg CostPrice:{0}.<br />", LES_RecurFee).AppendLine();
            //                Msg.AppendFormat("Recuring Feee from Cus Reg SellerPrice:{0}.<br />", CUS_RecurFee).AppendLine();
            //                returnMsg.AppendFormat("This Inmarsat id {0}: Recuring Feee is not match.<br />", ImarSatID).AppendLine();
            //            }

            //            if (DataUnits_CDR != null)
            //            {
            //                long data = Convert.ToInt64(Convert.ToDecimal(DataUnits_CDR));
            //                decimal? CDR_DataTotalUsageMB = (decimal)Converters.ConvertBytesToMegabytes(data);

            //                if (CDR_DataTotalUsageMB != null)
            //                    CDR_DataTotalUsageMB = Math.Round(Convert.ToDecimal(CDR_DataTotalUsageMB), 2, MidpointRounding.AwayFromZero);

            //                if (is_LESGBPlan)
            //                {
            //                    LES_DataPlan = LES_DataPlan * 1024;
            //                }

            //                if (LES_DataPlan < CDR_DataTotalUsageMB)
            //                {
            //                    isOutBundleused = true;
            //                    OutbundleDataused = CDR_DataTotalUsageMB - LES_DataPlan;
            //                    OutbundleDataused = Math.Round(Convert.ToDecimal(OutbundleDataused), 2, MidpointRounding.AwayFromZero);
            //                }

            //                decimal? LES_DataCP = CDR_DataTotalUsageMB * LES_DataRateper1MB;
            //                decimal? Cus_DataSP = CDR_DataTotalUsageMB * CUS_DataRateper1MB;
            //                decimal? LES_OutbundleDataCP = OutbundleDataused * LES_OutbundleRate;
            //                decimal? Cus_OutbundleDataSP = OutbundleDataused * Cus_OutbundleRate;

            //                if (Cus_DataSP != null)
            //                    Cus_DataSP = Math.Round(Convert.ToDecimal(Cus_DataSP), 2, MidpointRounding.AwayFromZero);

            //                if (LES_DataCP != null)
            //                    LES_DataCP = Math.Round(Convert.ToDecimal(LES_DataCP), 2, MidpointRounding.AwayFromZero);

            //                if (LES_OutbundleDataCP != null)
            //                    LES_OutbundleDataCP = Math.Round(Convert.ToDecimal(LES_OutbundleDataCP), 2, MidpointRounding.AwayFromZero);

            //                if (Cus_OutbundleDataSP != null)
            //                    Cus_OutbundleDataSP = Math.Round(Convert.ToDecimal(Cus_OutbundleDataSP), 2, MidpointRounding.AwayFromZero);
            //                if (isOutBundleused)
            //                {
            //                    CDR_OutbundleRate = CDR_DataCP - CDR_RecuFee;
            //                    CDR_OutbundleRate = Math.Round(Convert.ToDecimal(CDR_OutbundleRate), 2, MidpointRounding.AwayFromZero);

            //                    if ((CDR_OutbundleRate < LES_OutbundleDataCP || CDR_OutbundleRate.Equals(LES_OutbundleDataCP)) && CDR_OutbundleRate < Cus_OutbundleDataSP)
            //                    {
            //                        isDataMatched = true;
            //                        Les_TotalCostCP += LES_OutbundleDataCP;
            //                        Cus_TotalCostSP += Cus_OutbundleDataSP;
            //                    }
            //                    else
            //                    {
            //                        isDataMatched = false;
            //                        Msg.AppendFormat("This Inmarsat id {0}: Data of Background IP is not a match. <br />", ImarSatID).AppendLine();
            //                        Msg.AppendFormat("Outbundle Data rate from LES CDR price: {0}. <br />", CDR_DataCP).AppendLine();
            //                        Msg.AppendFormat("Outbundle Data rate from LES Resg Costprice: {0}. <br />", LES_OutbundleDataCP).AppendLine();
            //                        Msg.AppendFormat("Outbundle Data rate from CUS Reg Sellerprice: {0}. <br />", Cus_OutbundleDataSP).AppendLine();
            //                        returnMsg.AppendFormat("This Inmarsat id {0}: Data of Background IP is not a match. <br />", ImarSatID).AppendLine();
            //                        Les_TotalCostCP += LES_OutbundleDataCP;
            //                        Cus_TotalCostSP += Cus_OutbundleDataSP;
            //                    }
            //                }
            //                else
            //                {
            //                    //Les_TotalCostCP += LES_DataCP;
            //                    Cus_TotalCostSP += Cus_DataSP;
            //                    if (CDR_DataCP == 0 || Cus_DataSP == 0)
            //                    {
            //                        isDataMatched = true;
            //                    }
            //                    else
            //                    {
            //                        isDataMatched = false;
            //                        Msg.AppendFormat("This Inmarsat id {0}: Data of Background IP data rate is not match.<br />", ImarSatID).AppendLine();
            //                        Msg.AppendFormat("Data Rate from LES CDR Price:{0}.<br />", CDR_DataCP).AppendLine();
            //                        Msg.AppendFormat("Data Rate from LES Reg CostPrice:{0}.<br />", LES_DataCP).AppendLine();
            //                        Msg.AppendFormat("Data Rate from Cus Reg SellerPrice:{0}.<br />", Cus_DataSP).AppendLine();
            //                        returnMsg.AppendFormat("This Inmarsat id {0}: Data of Background IP data rate is not match.<br />", ImarSatID).AppendLine();
            //                    }
            //                }
            //            }
            //            else
            //                isDataMatched = true;

            //            if (CDR_VceFbbCellulardUintsCT != null)
            //            {
            //                double lngUnits = Convert.ToDouble(CDR_VceFbbCellulardUintsCT);
            //                decimal? CDR_VceFbbCelltotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

            //                if (CDR_VceFbbCelltotalUnits_Minutes != null)
            //                    CDR_VceFbbCelltotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceFbbCelltotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

            //                decimal? LES_VceFbbCellCPperMin = CDR_VceFbbCelltotalUnits_Minutes * LES_VoiceFBBCellular;
            //                decimal? CUS_VceFbbCellSPperMin = CDR_VceFbbCelltotalUnits_Minutes * CUS_VoiceFBBCellular;

            //                if (LES_VceFbbCellCPperMin != null)
            //                    LES_VceFbbCellCPperMin = Math.Round(Convert.ToDecimal(LES_VceFbbCellCPperMin), 2, MidpointRounding.AwayFromZero);

            //                if (CUS_VceFbbCellSPperMin != null)
            //                    CUS_VceFbbCellSPperMin = Math.Round(Convert.ToDecimal(CUS_VceFbbCellSPperMin), 2, MidpointRounding.AwayFromZero);

            //                Les_TotalCostCP += LES_VceFbbCellCPperMin;
            //                Cus_TotalCostSP += CUS_VceFbbCellSPperMin;

            //                if ((LES_VceFbbCellCPperMin.Equals(CDR_VceFbbCellularCostPriceCT) || LES_VceFbbCellCPperMin > CDR_VceFbbCellularCostPriceCT)
            //                    && (CDR_VceFbbCellularCostPriceCT < CUS_VceFbbCellSPperMin))
            //                {
            //                    isVoiceFbbCellurlarMatched = true;
            //                }
            //                else
            //                {
            //                    isVoiceFbbCellurlarMatched = false;
            //                    Msg.AppendFormat("This Inmarsat {0}: Voice of Basic 'voice -FBB/Cellular' data rate is not match.<br />", ImarSatID).AppendLine();
            //                    Msg.AppendFormat("Voice -FBB/Cellular Rate from LES CDR Price:{0}.<br />", CDR_VceFbbCellularCostPriceCT).AppendLine();
            //                    Msg.AppendFormat("Voice -FBB/Cellular Rate from LES Reg CostPrice:{0}.<br />", LES_VceFbbCellCPperMin).AppendLine();
            //                    Msg.AppendFormat("Voice -FBB/Cellular Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VceFbbCellSPperMin).AppendLine();
            //                    returnMsg.AppendFormat("This Inmarsat {0}: Voice of Basic 'voice -FBB/Cellular' data rate is not match.<br />", ImarSatID).AppendLine();
            //                }
            //            }
            //            else
            //                isVoiceFbbCellurlarMatched = false;

            //            if (CDR_VceFbbPsyndUintsCT != null)
            //            {
            //                double lngUnits = Convert.ToDouble(CDR_VceFbbPsyndUintsCT);
            //                decimal? CDR_VceFbbPSTNtotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

            //                if (CDR_VceFbbPSTNtotalUnits_Minutes != null)
            //                    CDR_VceFbbPSTNtotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceFbbPSTNtotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

            //                decimal? LES_VceFbbPSTNCPperMin = CDR_VceFbbPSTNtotalUnits_Minutes * LES_VoiceFBBPSTN;
            //                decimal? CUS_VceFbbPSTNSPperMin = CDR_VceFbbPSTNtotalUnits_Minutes * CUS_VoiceFBBPSTN;

            //                if (LES_VceFbbPSTNCPperMin != null)
            //                    LES_VceFbbPSTNCPperMin = Math.Round(Convert.ToDecimal(LES_VceFbbPSTNCPperMin), 2, MidpointRounding.AwayFromZero);

            //                if (CUS_VceFbbPSTNSPperMin != null)
            //                    CUS_VceFbbPSTNSPperMin = Math.Round(Convert.ToDecimal(CUS_VceFbbPSTNSPperMin), 2, MidpointRounding.AwayFromZero);

            //                Les_TotalCostCP += LES_VceFbbPSTNCPperMin;
            //                Cus_TotalCostSP += CUS_VceFbbPSTNSPperMin;

            //                if ((LES_VceFbbPSTNCPperMin.Equals(CDR_VceFbbPSTNCostPriceCT) || LES_VceFbbPSTNCPperMin > CDR_VceFbbPSTNCostPriceCT)
            //                    && (CDR_VceFbbPSTNCostPriceCT < CUS_VceFbbPSTNSPperMin))
            //                {
            //                    isVoiceFbbPSTNMatched = true;
            //                }
            //                else
            //                {
            //                    isVoiceFbbPSTNMatched = false;
            //                    Msg.AppendFormat("This Inmarsat {0}: Voice of Basic 'voice -FBB/PSTN' data rate is not match.<br />", ImarSatID).AppendLine();
            //                    Msg.AppendFormat("Voice -FBB/PSTN Rate from LES CDR Price:{0}.<br />", CDR_VceFbbPSTNCostPriceCT).AppendLine();
            //                    Msg.AppendFormat("Voice -FBB/PSTN Rate from LES Reg CostPrice:{0}.<br />", LES_VceFbbPSTNCPperMin).AppendLine();
            //                    Msg.AppendFormat("Voice -FBB/PSTN Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VceFbbPSTNSPperMin).AppendLine();
            //                    returnMsg.AppendFormat("This Inmarsat {0}: Voice of Basic 'voice -FBB/PSTN' data rate is not match.<br />", ImarSatID).AppendLine();
            //                }
            //            }
            //            else
            //                isVoiceFbbPSTNMatched = true;

            //            if (CDR_VoicetoFbbdUintsCT != null)
            //            {
            //                double lngUnits = Convert.ToDouble(CDR_VoicetoFbbdUintsCT);
            //                decimal? CDR_VoicetoFBBtotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

            //                if (CDR_VoicetoFBBtotalUnits_Minutes != null)
            //                    CDR_VoicetoFBBtotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VoicetoFBBtotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

            //                decimal? LES_VoicetoFbbCPperMin = CDR_VoicetoFBBtotalUnits_Minutes * LES_VoiceFBB;
            //                decimal? CUS_VoicetoFbbSPperMin = CDR_VoicetoFBBtotalUnits_Minutes * CUS_VoiceFBB;

            //                if (LES_VoicetoFbbCPperMin != null)
            //                    LES_VoicetoFbbCPperMin = Math.Round(Convert.ToDecimal(LES_VoicetoFbbCPperMin), 2, MidpointRounding.AwayFromZero);

            //                if (CUS_VoicetoFbbSPperMin != null)
            //                    CUS_VoicetoFbbSPperMin = Math.Round(Convert.ToDecimal(CUS_VoicetoFbbSPperMin), 2, MidpointRounding.AwayFromZero);

            //                Les_TotalCostCP += LES_VoicetoFbbCPperMin;
            //                Cus_TotalCostSP += CUS_VoicetoFbbSPperMin;

            //                if ((LES_VoicetoFbbCPperMin.Equals(CDR_VoicetoFbbCostPriceCT) || LES_VoicetoFbbCPperMin > CDR_VoicetoFbbCostPriceCT)
            //                    && (CDR_VoicetoFbbCostPriceCT < CUS_VoicetoFbbSPperMin))
            //                {
            //                    isVoiceFbbMatched = true;
            //                }
            //                else
            //                {
            //                    isVoiceFbbMatched = false;
            //                    Msg.AppendFormat("This Inmarsat {0}: Voice of Basic 'voice -FBB/PSTN' data rate is not match.<br />", ImarSatID).AppendLine();
            //                    Msg.AppendFormat("Voice -FBB/PSTN Rate from LES CDR Price:{0}.<br />", CDR_VoicetoFbbCostPriceCT).AppendLine();
            //                    Msg.AppendFormat("Voice -FBB/PSTN Rate from LES Reg CostPrice:{0}.<br />", LES_VoicetoFbbCPperMin).AppendLine();
            //                    Msg.AppendFormat("Voice -FBB/PSTN Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VoicetoFbbSPperMin).AppendLine();
            //                    returnMsg.AppendFormat("This Inmarsat {0}: Voice of Basic 'voice -FBB/PSTN' data rate is not match.<br />", ImarSatID).AppendLine();
            //                }
            //            }
            //            else
            //                isVoiceFbbMatched = true;

            //            if (CDR_VcePrepaidServerUintsCT != null && CDR_VcePrepaidServerUintsCT > 0)
            //            {
            //                double lngUnits = Convert.ToDouble(CDR_VcePrepaidServerUintsCT);
            //                decimal? CDR_VcePrepaidServertotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

            //                if (CDR_VcePrepaidServertotalUnits_Minutes != null)
            //                    CDR_VcePrepaidServertotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VcePrepaidServertotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

            //                decimal? LES_VcePdServerCPperMin = CDR_VcePrepaidServertotalUnits_Minutes * LES_PrepaidServer;
            //                decimal? CUS_VcePdServerSPperMin = CDR_VcePrepaidServertotalUnits_Minutes * CUS_PrepaidServer;

            //                if (LES_VcePdServerCPperMin != null)
            //                    LES_VcePdServerCPperMin = Math.Round(Convert.ToDecimal(LES_VcePdServerCPperMin), 2, MidpointRounding.AwayFromZero);

            //                if (CUS_VcePdServerSPperMin != null)
            //                    CUS_VcePdServerSPperMin = Math.Round(Convert.ToDecimal(CUS_VcePdServerSPperMin), 2, MidpointRounding.AwayFromZero);

            //                Les_TotalCostCP += LES_VcePdServerCPperMin;
            //                Cus_TotalCostSP += CUS_VcePdServerSPperMin;

            //                if ((LES_VcePdServerCPperMin.Equals(CDR_VcePrepaidServerCostPriceCT) || LES_VcePdServerCPperMin > CDR_VcePrepaidServerCostPriceCT)
            //                    && (CDR_VceFbbPSTNCostPriceCT < CUS_VcePdServerSPperMin))
            //                {
            //                    isPrepaidServerMatched = true;
            //                }
            //                else
            //                {
            //                    isPrepaidServerMatched = false;
            //                    Msg.AppendFormat("This Inmarsat {0}: Voice of Basic 'Prepaid Server' data rate is not match.<br />", ImarSatID).AppendLine();
            //                    Msg.AppendFormat("Voice -Prepaid Server Rate from LES CDR Price:{0}.<br />", CDR_VcePrepaidServerCostPriceCT).AppendLine();
            //                    Msg.AppendFormat("Voice -Prepaid Server Rate from LES Reg CostPrice:{0}.<br />", LES_VcePdServerCPperMin).AppendLine();
            //                    Msg.AppendFormat("Voice -Prepaid Server Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VcePdServerSPperMin).AppendLine();
            //                    returnMsg.AppendFormat("This Inmarsat {0}: Voice of Basic 'Prepaid Server' data rate is not match.<br />", ImarSatID).AppendLine();
            //                }
            //            }
            //            else
            //                isPrepaidServerMatched = true;

            //            if (CDR_SMSdUintsCT != null)
            //            {
            //                decimal? CDR_SMSTotalUnits_Msg = CDR_SMSdUintsCT;

            //                if (CDR_SMSTotalUnits_Msg != null)
            //                    CDR_SMSTotalUnits_Msg = Math.Round(Convert.ToDecimal(CDR_SMSTotalUnits_Msg), 2, MidpointRounding.AwayFromZero);

            //                decimal? LES_SMSCPperMin = CDR_SMSTotalUnits_Msg * LES_SMS;
            //                decimal? CUS_SMSSPperMin = CDR_SMSTotalUnits_Msg * CUS_SMS;

            //                if (LES_SMSCPperMin != null)
            //                    LES_SMSCPperMin = Math.Round(Convert.ToDecimal(LES_SMSCPperMin), 2, MidpointRounding.AwayFromZero);

            //                if (CUS_SMSSPperMin != null)
            //                    CUS_SMSSPperMin = Math.Round(Convert.ToDecimal(CUS_SMSSPperMin), 2, MidpointRounding.AwayFromZero);

            //                Les_TotalCostCP += LES_SMSCPperMin;
            //                Cus_TotalCostSP += CUS_SMSSPperMin;

            //                if ((LES_SMSCPperMin.Equals(CDR_SMSCostPriceCT) || LES_SMSCPperMin > CDR_SMSCostPriceCT)
            //                    && (CDR_SMSCostPriceCT < CUS_SMSSPperMin))
            //                {
            //                    isSMSMatched = true;
            //                }
            //                else
            //                {
            //                    isSMSMatched = false;
            //                    Msg.AppendFormat("This Inmarsat {0}: Voice of Basic 'voice -FBB/PSTN' data rate is not match.<br />", ImarSatID).AppendLine();
            //                    Msg.AppendFormat("Voice -FBB/PSTN Rate from LES CDR Price:{0}.<br />", CDR_SMSCostPriceCT).AppendLine();
            //                    Msg.AppendFormat("Voice -FBB/PSTN Rate from LES Reg CostPrice:{0}.<br />", LES_SMSCPperMin).AppendLine();
            //                    Msg.AppendFormat("Voice -FBB/PSTN Rate from Cus Reg SellerPrice:{0}.<br />", CUS_SMSSPperMin).AppendLine();
            //                    returnMsg.AppendFormat("This Inmarsat {0}: Voice of Basic 'voice -FBB/PSTN' data rate is not match.<br />", ImarSatID).AppendLine();
            //                }
            //            }
            //            else
            //                isSMSMatched = true;

            //            string InvoiceNo = string.Empty;

            //            try
            //            {
            //                InvoiceNo = db.BS2MtCusT.Max(t => t.BSMtCusInvNo);

            //                if (!string.IsNullOrEmpty(InvoiceNo))
            //                {
            //                    if (InvoiceNo.Contains("SM"))
            //                    {
            //                        string tempInv = InvoiceNo.Replace("SM", string.Empty);
            //                        if (int.TryParse(tempInv, out int result))
            //                        {
            //                            InvoiceNo = BillingHelpers.GenInvoice(false, result);
            //                        }
            //                    }
            //                }
            //                else
            //                    InvoiceNo = BillingHelpers.GenInvoice(false, 0);
            //            }
            //            catch (Exception error)
            //            {
            //                Msg.AppendFormat("IMSI: {0} Error Msg: {1} Stack Trace: {2}. <br />", ImarSatID, error.Message.ToString(), error.StackTrace);
            //                //ErrorMsg.AppendFormat("MSI:{0} Error Msg: {1} Stack Trace: {2}. <br />", Imsi, error.Message.ToString(), error.StackTrace);
            //            }

            //            bool isLES_ContrRegDate = false;
            //            bool isLES_ContrBarDate = false;
            //            bool isLES_ContrSusDate = false;
            //            bool isLES_ContrLayUpDate = false;

            //            bool isCUS_ContrRegDate = false;
            //            bool isCUS_ContrBarDate = false;
            //            bool isCUS_ContrSusDate = false;
            //            bool isCUS_ContrLayUpDate = false;

            //            if (CDR_StartDate > LES_ContrRegStrDate && CDR_EndDate < LES_ContrRegEndDate)
            //                isLES_ContrRegDate = true;
            //            else
            //            {
            //                isLES_ContrRegDate = false;
            //                Msg.AppendFormat("This Inmarsat {0}: Contract CDR Start/End date Less than les Reg Start/End Date.<br />", ImarSatID).AppendLine();
            //                Msg.AppendFormat("Contract CDR Start Date {0}: LES Reg Strart Date {1}.<br />", CDR_StartDate, LES_ContrRegStrDate).AppendLine();
            //                Msg.AppendFormat("Contract CDR Start Date {0}: LES Reg End Date {1}.<br />", CDR_StartDate, LES_ContrRegEndDate).AppendLine();
            //                returnMsg.AppendFormat("This Inmarsat {0}: Contract Reg start date Less than les Reg start Date.<br />", ImarSatID).AppendLine();
            //            }

            //            if ((LES_ContrBarStrDate == null && LES_ContrBarEndDate == null) || (CDR_StartDate > LES_ContrBarStrDate && CDR_EndDate > LES_ContrBarEndDate))
            //                isLES_ContrBarDate = true;
            //            else
            //            {
            //                isLES_ContrBarDate = false;
            //                Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than lES Bar Start/End Date.<br />", ImarSatID).AppendLine();
            //                Msg.AppendFormat("Contract CDR Start Date {0}: LES Bar Strart Date {1}.<br />", CDR_StartDate, LES_ContrBarStrDate).AppendLine();
            //                Msg.AppendFormat("Contract CDR Start Date {0}: LES Bar End Date {1}.<br />", CDR_StartDate, LES_ContrBarEndDate).AppendLine();
            //                returnMsg.AppendFormat("This Inmarsat {0}: Contract CCDR start date Greater than lES Bar start Date.<br />", ImarSatID).AppendLine();
            //            }

            //            if ((LES_ContrSusStrDate == null && LES_ContrSusEndDate == null) || (CDR_StartDate > LES_ContrSusStrDate && CDR_EndDate > LES_ContrSusEndDate))
            //                isLES_ContrSusDate = true;
            //            else
            //            {
            //                isLES_ContrBarDate = false;
            //                Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than lES Suspenstion Start/End Date.<br />", ImarSatID).AppendLine();
            //                Msg.AppendFormat("Contract CDR Start Date {0}: LES Suspenstion Start Date {1}.<br />", CDR_StartDate, LES_ContrSusStrDate).AppendLine();
            //                Msg.AppendFormat("Contract CDR Start Date {0}: LES Suspenstion End Date {1}.<br />", CDR_StartDate, LES_ContrSusEndDate).AppendLine();
            //                returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than lES Suspenstion start Date.<br />", ImarSatID).AppendLine();
            //            }

            //            if ((LES_ContrLayoutStrDate == null && LES_ContrLayoutEndDate == null) || (CDR_StartDate > LES_ContrLayoutStrDate && CDR_EndDate > LES_ContrLayoutEndDate))
            //                isLES_ContrLayUpDate = true;
            //            else
            //            {
            //                isLES_ContrLayUpDate = true;
            //                Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than LES LayUp Start/End Date.<br />", ImarSatID).AppendLine();
            //                Msg.AppendFormat("Contract CDR Start Date {0}: LES LayUp Start Date {1}.<br />", CDR_StartDate, LES_ContrSusStrDate).AppendLine();
            //                Msg.AppendFormat("Contract CDR Start Date {0}: LES LayUp End Date {1}.<br />", CDR_StartDate, LES_ContrSusEndDate).AppendLine();
            //                returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than LayUp Start/End Date.<br />", ImarSatID).AppendLine();
            //            }

            //            if (CDR_StartDate > CUS_ContrRegStrDate && CDR_EndDate < CUS_ContrRegEndDate)
            //                isCUS_ContrRegDate = true;
            //            else
            //            {
            //                isCUS_ContrRegDate = false;
            //                Msg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Less than CUS Reg Start/End Date.<br />", ImarSatID).AppendLine();
            //                Msg.AppendFormat("Contract CDR Start Date {0}: CUS Reg Strart Date {1}.<br />", CDR_StartDate, CUS_ContrRegStrDate).AppendLine();
            //                Msg.AppendFormat("Contract CDR Start Date {0}: CUS Reg End Date {1}.<br />", CDR_StartDate, CUS_ContrRegEndDate).AppendLine();
            //                returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Less than CUS Reg Start/End Date.<br />", ImarSatID).AppendLine();
            //            }

            //            if ((CUS_ContrBarStrDate == null && CUS_ContrBarEndDate == null) || (CDR_StartDate > CUS_ContrBarStrDate && CDR_EndDate > CUS_ContrBarEndDate))
            //                isCUS_ContrBarDate = true;
            //            else
            //            {
            //                isCUS_ContrBarDate = false;
            //                Msg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Greater than CUS Bar Start/End Date.<br />", ImarSatID).AppendLine();
            //                Msg.AppendFormat("Contract CDR Start Date {0}: CUS Bar Strart Date {1}.<br />", CDR_StartDate, CUS_ContrBarStrDate).AppendLine();
            //                Msg.AppendFormat("Contract CDR Start Date {0}: CUS Bar End Date {1}.<br />", CDR_StartDate, CUS_ContrBarStrDate).AppendLine();
            //                returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Greater than CUS Bar Start/End Date.<br />", ImarSatID).AppendLine();
            //            }

            //            if ((CUS_ContrSusStrDate == null && CUS_ContrSusEndDate == null) || (CDR_StartDate > CUS_ContrSusStrDate && CDR_EndDate > CUS_ContrSusEndDate))
            //                isCUS_ContrSusDate = true;
            //            else
            //            {
            //                isCUS_ContrSusDate = false;
            //                Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS Suspenstion Start/End Date.<br />", ImarSatID).AppendLine();
            //                Msg.AppendFormat("Contract CDR Start Date {0}: CUS Suspenstion Start Date {1}.<br />", CDR_StartDate, CUS_ContrSusStrDate).AppendLine();
            //                Msg.AppendFormat("Contract CDR Start Date {0}: CUS Suspenstion End Date {1}.<br />", CDR_StartDate, CUS_ContrSusEndDate).AppendLine();
            //                returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS Suspenstion Start/End Date.<br />", ImarSatID).AppendLine();
            //            }

            //            if ((CUS_ContrLayoutStrDate == null && CUS_ContrLayoutEndDate == null) || (CDR_StartDate > CUS_ContrLayoutStrDate && CDR_EndDate > CUS_ContrLayoutEndDate))
            //                isCUS_ContrLayUpDate = true;
            //            else
            //            {
            //                isCUS_ContrLayUpDate = true;
            //                Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS LayUp Start/End Date.<br />", ImarSatID).AppendLine();
            //                Msg.AppendFormat("Contract CDR Start Date {0}: CUS LayUp Start Date {1}.<br />", CDR_StartDate, LES_ContrSusStrDate).AppendLine();
            //                Msg.AppendFormat("Contract CDR Start Date {0}: CUS LayUp End Date {1}.<br />", CDR_StartDate, LES_ContrSusEndDate).AppendLine();
            //                returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS LayUp Start/End Date.<br />", ImarSatID).AppendLine();
            //            }

            //            //if ((startDateLes > cdrStartDate && contractStartDateLes > cdrStartDate && barDateLes > cdrStartDate
            //            //                  && suspensionDateLes > cdrStartDate && layupDateLes > cdrStartDate) && (
            //            if (isRecurFeeMached && isDataMatched && isVoiceFbbCellurlarMatched && isVoiceFbbPSTNMatched && isVoiceFbbMatched && isPrepaidServerMatched && isSMSMatched && isLES_ContrRegDate && isLES_ContrBarDate && isLES_ContrSusDate && isLES_ContrLayUpDate && isCUS_ContrRegDate &&
            //                    isCUS_ContrBarDate && isCUS_ContrSusDate && isCUS_ContrLayUpDate)
            //            {
            //                if (!string.IsNullOrEmpty(InvoiceNo))
            //                {
            //                    BS2MtCusT SecMtCust = new BS2MtCusT();

            //                    SecMtCust.BSMtCusInvNo = InvoiceNo;
            //                    SecMtCust.fkSPID = CT_Fbs.BCtAfkLESID;
            //                    SecMtCust.fkCusID = CUS_Id;
            //                    SecMtCust.fkCusConID = CUS_ContractID;
            //                    SecMtCust.fkCusRegID = CUS_RegsID;
            //                    SecMtCust.fkspcID = CT_Fbs.BCtAfkLESCID;
            //                    SecMtCust.fkSimID = SimID;
            //                    SecMtCust.fkLesRefID = CT_Fbs.fkLesRefID;
            //                    SecMtCust.fkSatTypeID = CT_Fbs.fkSatTypeID;
            //                    SecMtCust.fkEquipTypeID = CT_Fbs.fkEquipType;
            //                    SecMtCust.BSMtCusLesCode = CT_Fbs.BCtAFbLesCode;
            //                    SecMtCust.BSMtCusCdrCode = CT_Fbs.BCtAFbCdrCode;
            //                    SecMtCust.BSMtCusSatType = null;
            //                    SecMtCust.BSMtCusServDest = CT_Fbs.BCtAFbServDest;
            //                    SecMtCust.BSMtCusServType = CT_Fbs.BCtAFbServType;
            //                    SecMtCust.BSMtCusTransUsage = CT_Fbs.BCtAFbTransUsage;
            //                    SecMtCust.BSMtCusUOM = CT_Fbs.BCtAFbUOM;
            //                    SecMtCust.BSMtCusRecurFee = CUS_RecurFee;
            //                    Cus_TotalCostSP += SecMtCust.BSMtCusRecurFee;
            //                    SecMtCust.BSMtCusRecurFeeType = CT_Fbs.BCtAFbRecurFeeType;
            //                    SecMtCust.BSMtCusCDRRef = CT_Fbs.BCtAFbCDRRef;
            //                    SecMtCust.BSMtCusBillNo = CT_Fbs.BCtAFbBillNo;
            //                    SecMtCust.BSMtCusBillPeriod = CT_Fbs.BCtAFbBillPeriod;
            //                    SecMtCust.BSMtCusBillDate = CT_Fbs.BCtAFbBillDate;

            //                    if (CDR_RecuFee < CDR_DataCP && isOutBundleused)
            //                    {
            //                        LES_InvRptDataCostTot = CDR_DataCP - CDR_RecuFee;
            //                    }
            //                    else
            //                        LES_InvRptDataCostTot = 0;

            //                    db.BS2MtCusT.Add(SecMtCust);
            //                    db.SaveChanges();

            //                    int SecFBBID = SecMtCust.BSMtCusTID;

            //                    //Get value from BS2MtCusT and save to BInvRprt table
            //                    BInvLstReport Invreport = new BInvLstReport();

            //                    if (SecMtCust.BSMtCusServType != null)
            //                    {
            //                        Invreport.fkBCus2ID = SecFBBID;
            //                        Invreport.BInvRprtSerType = SecMtCust.BSMtCusServType;
            //                        Invreport.BInvRprtTotBil = SecMtCust.BSMtCusRecurFee;
            //                        Invreport.BInvRprtTotCst = CT_Fbs.BCtAFbRecurFee;
            //                    }

            //                    List<BCtB> lstDateCT = lstCtRecord.AsEnumerable()
            //                                     .Where(t => t.BCtBfkCommType == 1 && t.BCtBLesServTypeID == ServTypeID &&
            //                                     (t.BCtBfkFbID == id)).ToList();
            //                    Bs2Sec bs2Sec;
            //                    decimal? outbundleData = 0;
            //                    decimal? LES_RptDataCost = 0;
            //                    decimal? Cus_RptDataCost = 0;

            //                    if (is_CUSGBPlan)
            //                    {
            //                        Cus_DataPlan = Cus_DataPlan * 1024;
            //                    }

            //                    if (lstDateCT.Count > 0)
            //                    {
            //                        try
            //                        {
            //                            Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeID).BsPtCusServiceTypes;
            //                        }
            //                        catch { Invreport.BInvRprtSerType = null; }
            //                    }

            //                    foreach (var records in lstDateCT)
            //                    {
            //                        bs2Sec = new Bs2Sec();
            //                        bs2Sec.fkCTFbID = SecFBBID;
            //                        bs2Sec.BsCtStartDate = records.BCtBStartDate;
            //                        bs2Sec.BsCtStarTime = records.BCtBStartTime;
            //                        bs2Sec.BsCtOrigion = records.BCtBOrigin;
            //                        bs2Sec.BsCtDestination = records.BCtBDestination;
            //                        bs2Sec.fkSPServTypeID = CUS_ServTypeID;

            //                        long dataconvertToMP = Convert.ToInt64(records.BCtBTransUsage);
            //                        bs2Sec.BsCtTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertKilobytesToMegabytes(dataconvertToMP)), 2, MidpointRounding.AwayFromZero);
            //                        bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("MB")).UID;

            //                        outbundleData += bs2Sec.BsCtBillIncre;

            //                        if (Cus_DataPlan <= outbundleData)
            //                        {
            //                            bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal((bs2Sec.BsCtTransUsage / 8388608) * Cus_OutbundleRate), 2, MidpointRounding.AwayFromZero);
            //                            if (Invreport.BInvRprtSerType != null)
            //                                Cus_RptDataCost += bs2Sec.BsCtTotalUsage;
            //                        }
            //                        else
            //                        {
            //                            bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * 0), 2, MidpointRounding.AwayFromZero);
            //                            if (Invreport.BInvRprtSerType != null)
            //                                Cus_RptDataCost += bs2Sec.BsCtTotalUsage;
            //                        }
            //                        bs2Sec.fkCommType = 1;

            //                        db.Bs2Sec.Add(bs2Sec);
            //                    }

            //                    //Invoice list report add in BInvLstReports
            //                    if (Invreport.BInvRprtSerType != null)
            //                    {
            //                        Invreport.fkBCus2ID = SecFBBID;
            //                        Invreport.BInvRprtTotCst = LES_InvRptDataCostTot;
            //                        Invreport.BInvRprtTotBil = Cus_RptDataCost;
            //                        db.BInvLstReports.Add(Invreport);
            //                    }

            //                    List<BCtB> lstVceFbbCellCT = lstCtRecord.AsEnumerable()
            //                                     .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeMobile &&
            //                                     (t.BCtBfkFbID == id)).ToList();

            //                    Invreport = new BInvLstReport();
            //                    if (lstVceFbbCellCT.Count > 0)
            //                    {
            //                        try
            //                        {
            //                            Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeMobile).BsPtCusServiceTypes;
            //                        }
            //                        catch { Invreport.BInvRprtSerType = null; }
            //                    }
            //                    decimal? LES_RptV2FBBcCost = CDR_VceFbbCellularCostPriceCT;
            //                    decimal? CUS_RptV2FBBcCost = 0;

            //                    foreach (var records in lstVceFbbCellCT)
            //                    {
            //                        bs2Sec = new Bs2Sec();
            //                        bs2Sec.fkCTFbID = SecFBBID;
            //                        bs2Sec.BsCtStartDate = records.BCtBStartDate;
            //                        bs2Sec.BsCtStarTime = records.BCtBStartTime;
            //                        bs2Sec.BsCtOrigion = records.BCtBOrigin;
            //                        bs2Sec.BsCtDestination = records.BCtBDestination;
            //                        bs2Sec.fkSPServTypeID = CUS_ServTypeMobile;

            //                        long SecconvertToMin = Convert.ToInt64(records.BCtBTotalUsage);
            //                        bs2Sec.BsCtTotalUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
            //                        bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
            //                        bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTotalUsage * CUS_VoiceFBBCellular), 2, MidpointRounding.AwayFromZero);

            //                        if (Invreport.BInvRprtSerType != null)
            //                            CUS_RptV2FBBcCost += bs2Sec.BsCtTotalUsage;

            //                        bs2Sec.fkCommType = 2;

            //                        db.Bs2Sec.Add(bs2Sec);
            //                    }

            //                    // Invoice List Report Add in BInvLstReports
            //                    if (Invreport.BInvRprtSerType != null)
            //                    {
            //                        Invreport.fkBCus2ID = SecFBBID;
            //                        Invreport.BInvRprtTotCst = LES_RptV2FBBcCost;
            //                        Invreport.BInvRprtTotBil = CUS_RptV2FBBcCost;
            //                        db.BInvLstReports.Add(Invreport);
            //                    }

            //                    List<BCtB> lstVceFbbPSTNCT = lstCtRecord.AsEnumerable()
            //                                     .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypePSTN &&
            //                                     (t.BCtBfkFbID.Equals(id))).ToList();

            //                    if (lstVceFbbPSTNCT.Count > 0)
            //                    {
            //                        try
            //                        {
            //                            Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == Cus_ServTypePSTN).BsPtCusServiceTypes;
            //                        }
            //                        catch { Invreport.BInvRprtSerType = null; }
            //                    }
            //                    decimal? LES_RptV2PSTNCost = CDR_VceFbbPSTNCostPriceCT;
            //                    decimal? CUS_RptV2PSTNCost = 0;


            //                    foreach (var records in lstVceFbbPSTNCT)
            //                    {
            //                        bs2Sec = new Bs2Sec();
            //                        bs2Sec.fkCTFbID = SecFBBID;
            //                        bs2Sec.BsCtStartDate = records.BCtBStartDate;
            //                        bs2Sec.BsCtStarTime = records.BCtBStartTime;
            //                        bs2Sec.BsCtOrigion = records.BCtBOrigin;
            //                        bs2Sec.BsCtDestination = records.BCtBDestination;
            //                        bs2Sec.fkSPServTypeID = Cus_ServTypePSTN;

            //                        long SecconvertToMin = Convert.ToInt64(records.BCtBTotalUsage);
            //                        bs2Sec.BsCtTotalUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
            //                        bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
            //                        bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTotalUsage * CUS_VoiceFBBPSTN), 2, MidpointRounding.AwayFromZero);

            //                        if (Invreport.BInvRprtSerType != null)
            //                            CUS_RptV2PSTNCost += bs2Sec.BsCtTotalUsage;

            //                        bs2Sec.fkCommType = 2;

            //                        db.Bs2Sec.Add(bs2Sec);
            //                    }

            //                    // Invoice List Report Add in BInvLstReports
            //                    if (Invreport.BInvRprtSerType != null)
            //                    {
            //                        Invreport.fkBCus2ID = SecFBBID;
            //                        Invreport.BInvRprtTotCst = LES_RptV2PSTNCost;
            //                        Invreport.BInvRprtTotBil = CUS_RptV2PSTNCost;
            //                        db.BInvLstReports.Add(Invreport);
            //                    }

            //                    List<BCtB> lstVceFbbFbbCT = lstCtRecord.AsEnumerable()
            //                                     .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeFBB &&
            //                                     (t.BCtBfkFbID.Equals(id))).ToList();

            //                    if (lstVceFbbFbbCT.Count > 0)
            //                    {
            //                        try
            //                        {
            //                            Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == Cus_ServTypeFBB).BsPtCusServiceTypes;
            //                        }
            //                        catch { Invreport.BInvRprtSerType = null; }
            //                    }
            //                    decimal? LES_RptV2FBBFBBCost = CDR_VoicetoFbbCostPriceCT;
            //                    decimal? CUS_RptV2FBBFBBCost = 0;


            //                    foreach (var records in lstVceFbbFbbCT)
            //                    {
            //                        bs2Sec = new Bs2Sec();
            //                        bs2Sec.fkCTFbID = SecFBBID;
            //                        bs2Sec.BsCtStartDate = records.BCtBStartDate;
            //                        bs2Sec.BsCtStarTime = records.BCtBStartTime;
            //                        bs2Sec.BsCtOrigion = records.BCtBOrigin;
            //                        bs2Sec.BsCtDestination = records.BCtBDestination;
            //                        bs2Sec.fkSPServTypeID = Cus_ServTypeFBB;

            //                        long SecconvertToMin = Convert.ToInt64(records.BCtBTransUsage);
            //                        bs2Sec.BsCtTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
            //                        bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
            //                        bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_VoiceFBB), 2, MidpointRounding.AwayFromZero);

            //                        if (Invreport.BInvRprtSerType != null)
            //                            CUS_RptV2FBBFBBCost += bs2Sec.BsCtTotalUsage;

            //                        bs2Sec.fkCommType = 2;

            //                        db.Bs2Sec.Add(bs2Sec);
            //                    }

            //                    // Invoice List Report Add in BInvLstReports
            //                    if (Invreport.BInvRprtSerType != null)
            //                    {
            //                        Invreport.fkBCus2ID = SecFBBID;
            //                        Invreport.BInvRprtTotCst = LES_RptV2FBBFBBCost;
            //                        Invreport.BInvRprtTotBil = CUS_RptV2FBBFBBCost;
            //                        db.BInvLstReports.Add(Invreport);
            //                    }

            //                    List<BCtB> lstPrepaidServerCT = lstCtRecord.AsEnumerable()
            //                                     .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypePadServer &&
            //                                     (t.BCtBfkFbID == id)).ToList();

            //                    if (lstPrepaidServerCT.Count > 0)
            //                    {
            //                        try
            //                        {
            //                            Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == Cus_ServTypePadServer).BsPtCusServiceTypes;
            //                        }
            //                        catch { Invreport.BInvRprtSerType = null; }

            //                    }
            //                    decimal? LES_RptPrepaidServCost = CDR_VcePrepaidServerCostPriceCT;
            //                    decimal? CUS_RptPrepaidServCost = 0;

            //                    foreach (var records in lstPrepaidServerCT)
            //                    {
            //                        bs2Sec = new Bs2Sec();
            //                        bs2Sec.fkCTFbID = SecFBBID;
            //                        bs2Sec.BsCtStartDate = records.BCtBStartDate;
            //                        bs2Sec.BsCtStarTime = records.BCtBStartTime;
            //                        bs2Sec.BsCtOrigion = records.BCtBOrigin;
            //                        bs2Sec.BsCtDestination = records.BCtBDestination;
            //                        bs2Sec.fkSPServTypeID = Cus_ServTypePadServer;

            //                        long SecconvertToMin = Convert.ToInt64(records.BCtBTransUsage);
            //                        bs2Sec.BsCtTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
            //                        bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
            //                        bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_PrepaidServer), 2, MidpointRounding.AwayFromZero);

            //                        if (Invreport.BInvRprtSerType != null)
            //                            CUS_RptPrepaidServCost += bs2Sec.BsCtTotalUsage;

            //                        bs2Sec.fkCommType = 2;

            //                        db.Bs2Sec.Add(bs2Sec);
            //                    }

            //                    // Invoice List Report Add in BInvLstReports
            //                    if (Invreport.BInvRprtSerType != null)
            //                    {
            //                        Invreport.fkBCus2ID = SecFBBID;
            //                        Invreport.BInvRprtTotCst = LES_RptPrepaidServCost;
            //                        Invreport.BInvRprtTotBil = CUS_RptPrepaidServCost;
            //                        db.BInvLstReports.Add(Invreport);
            //                    }

            //                    int commtype = 1;
            //                    try
            //                    {
            //                        commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
            //                    }
            //                    catch
            //                    {
            //                        commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
            //                    }

            //                    List<BCtB> lstSMSCT = lstCtRecord.AsEnumerable()
            //                        .Where(t => t.BCtBfkCommType == commtype && t.BCtBLesServTypeID == ServTypeSMS && (t.BCtBfkFbID.Equals(id))).ToList();

            //                    if (lstSMSCT.Count > 0)
            //                    {
            //                        try
            //                        {
            //                            Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == Cus_ServTypeSMS).BsPtCusServiceTypes;
            //                        }
            //                        catch { Invreport.BInvRprtSerType = null; }
            //                    }

            //                    decimal? LES_RptsmsCost = CDR_SMSCostPriceCT;
            //                    decimal? CUS_RptsmsCost = 0;

            //                    foreach (var records in lstSMSCT)
            //                    {
            //                        bs2Sec = new Bs2Sec();
            //                        bs2Sec.fkCTFbID = SecFBBID;
            //                        bs2Sec.BsCtStartDate = records.BCtBStartDate;
            //                        bs2Sec.BsCtStarTime = records.BCtBStartTime;
            //                        bs2Sec.BsCtOrigion = records.BCtBOrigin;
            //                        bs2Sec.BsCtDestination = records.BCtBDestination;
            //                        bs2Sec.fkSPServTypeID = Cus_ServTypeSMS;

            //                        //long SecconvertToMin = Convert.ToInt64(records.BsCtBillIncre);
            //                        bs2Sec.BsCtTransUsage = Convert.ToInt32(records.BCtBTransUsage); //(decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
            //                        try
            //                        {
            //                            bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("message")).UID;
            //                        }
            //                        catch
            //                        {
            //                            bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Message")).UID;
            //                        }

            //                        bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_SMS), 2, MidpointRounding.AwayFromZero);

            //                        if (Invreport.BInvRprtSerType != null)
            //                            CUS_RptsmsCost += bs2Sec.BsCtTotalUsage;

            //                        bs2Sec.fkCommType = commtype;

            //                        db.Bs2Sec.Add(bs2Sec);
            //                    }

            //                    // Invoice List Report Add in BInvLstReports
            //                    if (Invreport.BInvRprtSerType != null)
            //                    {
            //                        Invreport.fkBCus2ID = SecFBBID;
            //                        Invreport.BInvRprtTotCst = LES_RptsmsCost;
            //                        Invreport.BInvRprtTotBil = CUS_RptsmsCost;
            //                        db.BInvLstReports.Add(Invreport);
            //                    }

            //                    // List of Satatement 
            //                    Bs2Statement statment = null;
            //                    try
            //                    {
            //                        statment = db.Bs2Statement.FirstOrDefault(t => t.fkBs2CusID == SecFBBID);
            //                    }
            //                    catch { }


            //                    if (statment == null)
            //                    {
            //                        statment = new Bs2Statement();

            //                        statment.fkBs2CusID = SecFBBID;
            //                        statment.fkSimID = SimID;
            //                        statment.Bs2StTotalCP = Les_TotalCostCP;
            //                        statment.Bs2StTotalSP = Cus_TotalCostSP;
            //                        statment.Bs2StSMTSCost = SMTS_TotalCost;
            //                        statment.fkSPID = CT_Fbs.BCtAfkLESID;
            //                        statment.fkCusID = CUS_Id;
            //                        statment.Bs2StIvoiceDate = DateTime.Now.Date;
            //                        statment.Bs2StDuration = CT_Fbs.BCtAFbServDest;

            //                        if (statment.Bs2StTotalCP > 0 && statment.Bs2StTotalSP > 0)
            //                        {
            //                            decimal CrossProfit = Convert.ToDecimal(statment.Bs2StTotalSP - statment.Bs2StSMTSCost);
            //                            decimal Margin = (CrossProfit / Convert.ToDecimal(statment.Bs2StTotalSP) * 100);
            //                            statment.Bs2stMargin = Margin > 0 ? Convert.ToInt32(Margin) : (int)Math.Abs(Margin);
            //                        }

            //                        db.Bs2Statement.Add(statment);
            //                    }

            //                    try
            //                    {
            //                        db.SaveChanges();
            //                    }
            //                    catch { }
            //                    returnMsg.AppendFormat("This Inmarsat {0}: Successfully stored to Invoice.<br />", ImarSatID);
            //                }
            //            }
            //            else
            //            {
            //                BS1ALessT SecRejLes = new BS1ALessT();

            //                SecRejLes.fkSPID = CT_Fbs.BCtAfkLESID;
            //                SecRejLes.fkCusID = CUS_Id;
            //                SecRejLes.fkCusConID = CUS_ContractID;
            //                SecRejLes.fkCusRegID = CUS_RegsID;
            //                SecRejLes.fkspcID = CT_Fbs.BCtAfkLESCID;
            //                SecRejLes.fkSimID = SimID;
            //                SecRejLes.fkLesRefID = CT_Fbs.fkLesRefID;
            //                SecRejLes.fkSatTypeID = CT_Fbs.fkSatTypeID;
            //                SecRejLes.fkEquipTypeID = CT_Fbs.fkEquipType;
            //                SecRejLes.BS1ALesLesCode = CT_Fbs.BCtAFbLesCode;
            //                SecRejLes.BS1ALesCdrCode = CT_Fbs.BCtAFbCdrCode;
            //                SecRejLes.BS1ALesSatType = null;
            //                SecRejLes.BS1ALesServDest = CT_Fbs.BCtAFbServDest;
            //                SecRejLes.BS1ALesServType = CT_Fbs.BCtAFbServType;
            //                SecRejLes.BS1ALesTransUsage = CT_Fbs.BCtAFbTransUsage;
            //                SecRejLes.BS1ALesUOM = CT_Fbs.BCtAFbUOM;
            //                SecRejLes.BS1ALesRecurFee = LES_RecurFee;
            //                SecRejLes.BS1ALesRecurFeeType = CT_Fbs.BCtAFbRecurFeeType;
            //                SecRejLes.BS1ALesCDRRef = CT_Fbs.BCtAFbCDRRef;
            //                SecRejLes.BS1ALesBillNo = CT_Fbs.BCtAFbBillNo;
            //                SecRejLes.BS1ALesBillPeriod = CT_Fbs.BCtAFbBillPeriod;
            //                SecRejLes.BS1ALesBillDate = CT_Fbs.BCtAFbBillDate;

            //                db.BS1ALessT.Add(SecRejLes);
            //                try
            //                {
            //                    db.SaveChanges();
            //                }
            //                catch (Exception err)
            //                {

            //                }

            //                int SecRejID = SecRejLes.BS1ALesID;

            //                if (CDR_RecuFee < CDR_DataCP && isOutBundleused)
            //                {
            //                    LES_InvRptDataCostTot = CDR_DataCP - CDR_RecuFee;
            //                }
            //                else
            //                    LES_InvRptDataCostTot = 0;

            //                BInvLstReport InvreportCost = new BInvLstReport();
            //                if (SecRejLes.BS1ALesServType != null)
            //                {
            //                    InvreportCost.BInvRprtLes1ID = SecRejID;
            //                    InvreportCost.BInvRprtSerType = SecRejLes.BS1ALesServType;
            //                    InvreportCost.BInvRprtTotCst = CDR_RecuFee;
            //                    InvreportCost.BInvRprtTotBil = null;

            //                    db.BInvLstReports.Add(InvreportCost);
            //                }

            //                List<BCtB> lstDateCT = lstCtRecord.AsEnumerable()
            //                                 .Where(t => t.BCtBfkCommType == 1 && t.BCtBLesServTypeID == ServTypeID &&
            //                                 (t.BCtBfkFbID == id)).ToList();

            //                BS1LesRejLog lesRejLog = new BS1LesRejLog();
            //                lesRejLog.fkBs1LesReg = SecRejID;
            //                lesRejLog.Bs1lesRejection = Msg.ToString();
            //                lesRejLog.Bs1lesRejRemark = "";
            //                db.BS1LesRejLog.Add(lesRejLog);

            //                Bs1BLesT lesRej1;
            //                foreach (var records in lstDateCT)
            //                {
            //                    lesRej1 = new Bs1BLesT();
            //                    lesRej1.fkBs1BLesID = SecRejID;
            //                    lesRej1.Bs1BLesStartDate = records.BCtBStartDate;
            //                    lesRej1.Bs1BLesStarTime = records.BCtBStartTime;
            //                    lesRej1.Bs1BLesOrigion = records.BCtBOrigin;
            //                    lesRej1.Bs1BLesDestination = records.BCtBDestination;
            //                    lesRej1.fkSPServTypeID = records.BCtBLesServTypeID;

            //                    long dataconvertToMP = Convert.ToInt64(records.BCtBTransUsage);
            //                    lesRej1.Bs1BLesTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertBytesToMegabytes(dataconvertToMP)), 2, MidpointRounding.AwayFromZero);
            //                    lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("MB")).UID;
            //                    lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal((lesRej1.Bs1BLesTransUsage / 8388608) * LES_DataRateper1MB), 2, MidpointRounding.AwayFromZero);
            //                    lesRej1.fkCommType = 1;

            //                    db.Bs1BLesT.Add(lesRej1);
            //                }

            //                // Invoice List Report Add in BInvLstReports
            //                if (lstDateCT.Count > 0)
            //                {
            //                    InvreportCost = new BInvLstReport();
            //                    try
            //                    {
            //                        InvreportCost.BInvRprtSerType = LES_services.FirstOrDefault(t => t.SPServDID == ServTypeID).BsPtServiceTypes;
            //                    }
            //                    catch { InvreportCost.BInvRprtSerType = null; }

            //                    if (InvreportCost.BInvRprtSerType != null)
            //                    {
            //                        InvreportCost.BInvRprtLes1ID = SecRejID;
            //                        InvreportCost.fkBCus2ID = null;
            //                        InvreportCost.BInvRprtTotCst = LES_InvRptDataCostTot;
            //                        InvreportCost.BInvRprtTotBil = null;
            //                        db.BInvLstReports.Add(InvreportCost);
            //                    }

            //                }


            //                List<BCtB> lstVceFbbCellCT = lstCtRecord.AsEnumerable()
            //                                 .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeMobile &&
            //                                 (t.BCtBfkFbID == id)).ToList();

            //                foreach (var records in lstVceFbbCellCT)
            //                {
            //                    lesRej1 = new Bs1BLesT();
            //                    lesRej1.fkBs1BLesID = SecRejID;
            //                    lesRej1.Bs1BLesStartDate = records.BCtBStartDate;
            //                    lesRej1.Bs1BLesStarTime = records.BCtBStartTime;
            //                    lesRej1.Bs1BLesOrigion = records.BCtBOrigin;
            //                    lesRej1.Bs1BLesDestination = records.BCtBDestination;
            //                    lesRej1.fkSPServTypeID = records.BCtBLesServTypeID;

            //                    long SecconvertToMin = Convert.ToInt64(records.BCtBTransUsage);
            //                    lesRej1.Bs1BLesTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
            //                    lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
            //                    lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_VoiceFBBCellular), 2, MidpointRounding.AwayFromZero);
            //                    lesRej1.fkCommType = 2;

            //                    db.Bs1BLesT.Add(lesRej1);
            //                }

            //                // Invoice List Report Add in BInvLstReports
            //                if (lstVceFbbCellCT.Count > 0)
            //                {
            //                    InvreportCost = new BInvLstReport();
            //                    try
            //                    {
            //                        InvreportCost.BInvRprtSerType = LES_services.FirstOrDefault(t => t.SPServDID == ServTypeMobile).BsPtServiceTypes;
            //                    }
            //                    catch { InvreportCost.BInvRprtSerType = null; }

            //                    if (InvreportCost.BInvRprtSerType != null)
            //                    {
            //                        InvreportCost.BInvRprtLes1ID = SecRejID;
            //                        InvreportCost.fkBCus2ID = null;
            //                        InvreportCost.BInvRprtTotCst = CDR_VceFbbCellularCostPriceCT;
            //                        InvreportCost.BInvRprtTotBil = null;
            //                        db.BInvLstReports.Add(InvreportCost);
            //                    }
            //                }

            //                List<BCtB> lstVceFbbPSTNCT = lstCtRecord.AsEnumerable()
            //                                 .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypePSTN &&
            //                                 (t.BCtBfkFbID == id)).ToList();

            //                foreach (var records in lstVceFbbPSTNCT)
            //                {
            //                    lesRej1 = new Bs1BLesT();
            //                    lesRej1.fkBs1BLesID = SecRejID;
            //                    lesRej1.Bs1BLesStartDate = records.BCtBStartDate;
            //                    lesRej1.Bs1BLesStarTime = records.BCtBStartTime;
            //                    lesRej1.Bs1BLesOrigion = records.BCtBOrigin;
            //                    lesRej1.Bs1BLesDestination = records.BCtBDestination;
            //                    lesRej1.fkSPServTypeID = records.BCtBLesServTypeID;

            //                    long SecconvertToMin = Convert.ToInt64(records.BCtBTransUsage);
            //                    lesRej1.Bs1BLesTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
            //                    lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
            //                    lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_VoiceFBBPSTN), 2, MidpointRounding.AwayFromZero);
            //                    lesRej1.fkCommType = 2;

            //                    db.Bs1BLesT.Add(lesRej1);
            //                }

            //                // Invoice List Report Add in BInvLstReports
            //                if (lstVceFbbPSTNCT.Count > 0)
            //                {
            //                    InvreportCost = new BInvLstReport();
            //                    try
            //                    {
            //                        InvreportCost.BInvRprtSerType = LES_services.FirstOrDefault(t => t.SPServDID == ServTypePSTN).BsPtServiceTypes;
            //                    }
            //                    catch { InvreportCost.BInvRprtSerType = null; }

            //                    if (InvreportCost.BInvRprtSerType != null)
            //                    {
            //                        InvreportCost.BInvRprtLes1ID = SecRejID;
            //                        InvreportCost.fkBCus2ID = null;
            //                        InvreportCost.BInvRprtTotCst = CDR_VceFbbPSTNCostPriceCT;
            //                        InvreportCost.BInvRprtTotBil = null;
            //                        db.BInvLstReports.Add(InvreportCost);
            //                    }
            //                }

            //                List<BCtB> lstVceFbbfbbCT = lstCtRecord.AsEnumerable()
            //                                 .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeFBB &&
            //                                 (t.BCtBfkFbID == id)).ToList();

            //                foreach (var records in lstVceFbbfbbCT)
            //                {
            //                    lesRej1 = new Bs1BLesT();
            //                    lesRej1.fkBs1BLesID = SecRejID;
            //                    lesRej1.Bs1BLesStartDate = records.BCtBStartDate;
            //                    lesRej1.Bs1BLesStarTime = records.BCtBStartTime;
            //                    lesRej1.Bs1BLesOrigion = records.BCtBOrigin;
            //                    lesRej1.Bs1BLesDestination = records.BCtBDestination;
            //                    lesRej1.fkSPServTypeID = records.BCtBLesServTypeID;

            //                    long SecconvertToMin = Convert.ToInt64(records.BCtBTransUsage);
            //                    lesRej1.Bs1BLesTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
            //                    lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
            //                    lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_VoiceFBB), 2, MidpointRounding.AwayFromZero);
            //                    lesRej1.fkCommType = 2;

            //                    db.Bs1BLesT.Add(lesRej1);
            //                }

            //                // Invoice List Report Add in BInvLstReports
            //                if (lstVceFbbfbbCT.Count > 0)
            //                {
            //                    InvreportCost = new BInvLstReport();
            //                    try
            //                    {
            //                        InvreportCost.BInvRprtSerType = LES_services.FirstOrDefault(t => t.SPServDID == ServTypeFBB).BsPtServiceTypes;
            //                    }
            //                    catch { InvreportCost.BInvRprtSerType = null; }

            //                    if (InvreportCost.BInvRprtSerType != null)
            //                    {
            //                        InvreportCost.BInvRprtLes1ID = SecRejID;
            //                        InvreportCost.fkBCus2ID = null;
            //                        InvreportCost.BInvRprtTotCst = CDR_VoicetoFbbCostPriceCT;
            //                        InvreportCost.BInvRprtTotBil = null;
            //                        db.BInvLstReports.Add(InvreportCost);
            //                    }
            //                }

            //                List<BCtB> lstPrepaidServerCT = lstCtRecord.AsEnumerable()
            //                                 .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypePadServer &&
            //                                 (t.BCtBfkFbID == id)).ToList();

            //                foreach (var records in lstPrepaidServerCT)
            //                {
            //                    lesRej1 = new Bs1BLesT();
            //                    lesRej1.fkBs1BLesID = SecRejID;
            //                    lesRej1.Bs1BLesStartDate = records.BCtBStartDate;
            //                    lesRej1.Bs1BLesStarTime = records.BCtBStartTime;
            //                    lesRej1.Bs1BLesOrigion = records.BCtBOrigin;
            //                    lesRej1.Bs1BLesDestination = records.BCtBDestination;
            //                    lesRej1.fkSPServTypeID = records.BCtBLesServTypeID;

            //                    long SecconvertToMin = Convert.ToInt64(records.BCtBTransUsage);
            //                    lesRej1.Bs1BLesTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
            //                    lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
            //                    lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_PrepaidServer), 2, MidpointRounding.AwayFromZero);
            //                    lesRej1.fkCommType = 2;

            //                    db.Bs1BLesT.Add(lesRej1);
            //                }

            //                // Invoice List Report Add in BInvLstReports
            //                if (lstPrepaidServerCT.Count > 0)
            //                {
            //                    InvreportCost = new BInvLstReport();
            //                    try
            //                    {
            //                        InvreportCost.BInvRprtSerType = LES_services.FirstOrDefault(t => t.SPServDID == ServTypePadServer).BsPtServiceTypes;
            //                    }
            //                    catch { InvreportCost.BInvRprtSerType = null; }

            //                    if (InvreportCost.BInvRprtSerType != null)
            //                    {
            //                        InvreportCost.BInvRprtLes1ID = SecRejID;
            //                        InvreportCost.fkBCus2ID = null;
            //                        InvreportCost.BInvRprtTotCst = CDR_VcePrepaidServerCostPriceCT;
            //                        InvreportCost.BInvRprtTotBil = null;
            //                        db.BInvLstReports.Add(InvreportCost);
            //                    }
            //                }

            //                int commtype = 1;
            //                try
            //                {
            //                    commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
            //                }
            //                catch
            //                {
            //                    commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
            //                }

            //                List<BCtB> lstSMSCT = lstCtRecord.AsEnumerable()
            //                                  .Where(t => t.BCtBfkCommType == commtype && t.BCtBLesServTypeID == ServTypeSMS &&
            //                                  (t.BCtBfkFbID == id)).ToList();

            //                foreach (var records in lstSMSCT)
            //                {
            //                    lesRej1 = new Bs1BLesT();
            //                    lesRej1.fkBs1BLesID = SecRejID;
            //                    lesRej1.Bs1BLesStartDate = records.BCtBStartDate;
            //                    lesRej1.Bs1BLesStarTime = records.BCtBStartTime;
            //                    lesRej1.Bs1BLesOrigion = records.BCtBOrigin;
            //                    lesRej1.Bs1BLesDestination = records.BCtBDestination;
            //                    lesRej1.fkSPServTypeID = records.BCtBLesServTypeID;

            //                    //long SecconvertToMin = Convert.ToInt64(records.BCtBTransUsage);
            //                    lesRej1.Bs1BLesTransUsage = Convert.ToInt32(records.BCtBTransUsage);//(decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
            //                    try
            //                    {
            //                        lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("message")).UID;
            //                    }
            //                    catch
            //                    {
            //                        lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Message")).UID;
            //                    }

            //                    lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_SMS), 2, MidpointRounding.AwayFromZero);
            //                    lesRej1.fkCommType = commtype;

            //                    db.Bs1BLesT.Add(lesRej1);
            //                }

            //                // Invoice List Report Add in BInvLstReports
            //                if (lstSMSCT.Count > 0)
            //                {
            //                    InvreportCost = new BInvLstReport();
            //                    try
            //                    {
            //                        InvreportCost.BInvRprtSerType = LES_services.FirstOrDefault(t => t.SPServDID == ServTypeSMS).BsPtServiceTypes;
            //                    }
            //                    catch { InvreportCost.BInvRprtSerType = null; }

            //                    if (InvreportCost.BInvRprtSerType != null)
            //                    {
            //                        InvreportCost.BInvRprtLes1ID = SecRejID;
            //                        InvreportCost.fkBCus2ID = null;
            //                        InvreportCost.BInvRprtTotCst = CDR_SMSCostPriceCT;
            //                        InvreportCost.BInvRprtTotBil = null;
            //                        db.BInvLstReports.Add(InvreportCost);
            //                    }
            //                }

            //                db.SaveChanges();
            //                returnMsg.AppendFormat("This Inmarsat {0}: Successfully stored Rejection Les price table.<br />", ImarSatID).AppendLine();
            //            }
            //        }
            //        catch (Exception error)
            //        {
            //            Msg.AppendFormat("Error:id:{0}, {1}", ImarSatID, error.Message.ToString());
            //        }
            //    }
            //}
            return returnMsg.ToString();
        }

        // Get Les primary Key ID
        public static int getLesID()
        {
            try
            {
                using (BillingSystemEntities context = new BillingSystemEntities())
                {
                    return context.BsMtServiceProviders.FirstOrDefault(t => t.ServiceProvider.Equals(LesName)).spID;
                }
            }
            catch
            {
                return 0;
            }
        }

        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;

            try
            {
                string value = cell.CellValue.InnerXml;

                if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                {
                    return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
                }
                else
                {
                    return value;
                }
            }
            catch { return string.Empty; }


        }

        public static Tuple<DateTime?, TimeSpan?> splitDateTime(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                try
                {
                    DateTime dateValue = DateTime.Parse(input);
                    DateTime date = dateValue.Date;
                    TimeSpan time = dateValue.TimeOfDay;
                    return new Tuple<DateTime?, TimeSpan?>(date, time);

                }
                catch (FormatException)
                {
                    return new Tuple<DateTime?, TimeSpan?>(null, null);
                }
            }
            else
                return new Tuple<DateTime?, TimeSpan?>(null, null);
        }

        public Tuple<Stream, string, bool> GnerateInvoice(string primaryID, string logopath)
        {
            System.IO.Stream spreadsheetstream = new System.IO.MemoryStream();

            string Invoiceno = string.Empty;

            int value = 0;

            if (int.TryParse(primaryID, out value))
            {
                try
                {
                    int id = Convert.ToInt32(primaryID);
                    string imsiID = string.Empty;
                    string invoiceNo = string.Empty;
                    string ImarsatID = string.Empty;

                    string buildingName = string.Empty;
                    string street = string.Empty;
                    string district = string.Empty;
                    string city = string.Empty;
                    string country = string.Empty;

                    string VesselName = string.Empty;

                    List<string> SDescription = new List<string>();
                    Dictionary<string, decimal?> dictServiceDescription = new Dictionary<string, decimal?>();

                    using (BillingSystemEntities db = new BillingSystemEntities())
                    {

                        var SecFbb = db.BS2MtCusT.FirstOrDefault(t => t.BSMtCusTID == id);

                        if (SecFbb != null)
                        {
                            invoiceNo = SecFbb.BSMtCusInvNo;
                            List<Bs2Sec> lstSecRecords = db.Bs2Sec.Where(t => t.fkCTFbID == id).ToList();

                            var ServiceTypeID = lstSecRecords.Where(t => t.fkCTFbID == id)
                                                    .Select(t => t.fkSPServTypeID).Distinct().ToList();

                            try
                            {
                                decimal? totalAmountRecuFee = SecFbb.BSMtCusRecurFee;

                                totalAmountRecuFee = Math.Round(Convert.ToDecimal(totalAmountRecuFee), 2, MidpointRounding.AwayFromZero);

                                dictServiceDescription.Add(SecFbb.BSMtCusServType, totalAmountRecuFee);
                            }
                            catch { }

                            foreach (var typeID in ServiceTypeID)
                            {
                                string service = string.Empty;
                                try
                                {
                                    service = db.BsPtCusServiceDests.FirstOrDefault(t => t.CusServDID == typeID).BsPtCusServiceTypes;
                                    SDescription.Add(service);
                                }
                                catch { }

                                try
                                {
                                    if (!string.IsNullOrEmpty(service))
                                    {
                                        decimal? totalAmountData = lstSecRecords.AsEnumerable().Where(
                                                                t => t.fkSPServTypeID == typeID)
                                                                .GroupBy(t => t.fkSPServTypeID == typeID)
                                                                .Select(t => t.Sum(s => s.BsCtTotalUsage)).FirstOrDefault();
                                        totalAmountData = Math.Round(Convert.ToDecimal(totalAmountData), 2, MidpointRounding.AwayFromZero);

                                        dictServiceDescription.Add(service, totalAmountData);
                                    }
                                }
                                catch { }
                            }
                        }

                        try
                        {
                            var sim = db.BptFbSims.Where(t => t.Sim_ID == SecFbb.fkSimID).Select(t => new { t.BPtFBImImsi, t.BPtFBimMsisdn });
                            foreach (var val in sim)
                            {
                                imsiID = val.BPtFBImImsi;
                                ImarsatID = val.BPtFBimMsisdn;
                            }

                            VesselName = db.BPtShips.FirstOrDefault(t => t.BPtShipMMSI.Equals(imsiID)).BPtShipName;
                        }
                        catch { VesselName = "Not Found"; }

                        string CusAddress = db.BsPtCusRegs_.FirstOrDefault(t => t.fkSimID == SecFbb.fkSimID).BsPtCusAddr;
                        if (!string.IsNullOrEmpty(CusAddress))
                        {
                            string[] addressparts = CusAddress.Split(',');

                            buildingName = addressparts.Length > 0 ? addressparts[0] : string.Empty;
                            street = addressparts.Length > 1 ? addressparts[1] : string.Empty;
                            district = addressparts.Length > 2 ? addressparts[2] : string.Empty;
                            city = addressparts.Length > 3 ? addressparts[3] : string.Empty;
                            country = addressparts.Length > 4 ? addressparts[4] : string.Empty;
                        }
                    }

                    DateTime dt = DateTime.Now;
                    string Period = dt.ToString("MMM");
                    DateTime dy = DateTime.Today;
                    string currYear = dy.Year.ToString();

                    string CurrPeriod = Period + '-' + currYear;
                    string InvoiceFileName = invoiceNo + '_' + buildingName + '_' + VesselName;

                    string pathid = Guid.NewGuid().ToString() + " " + DateTime.Now.Date.ToString("dd-MM-yyyy");

                    #region WorkBook Prepartion

                    XLWorkbook xLWorkbook = new XLWorkbook();
                    IXLWorksheet workSheet = xLWorkbook.Worksheets.Add("Invoice" + Invoiceno);

                    workSheet.Column(1).Width = 0.83;
                    workSheet.Column(2).Width = 1.50;
                    workSheet.Column(3).Width = 0.46;
                    workSheet.Column(4).Width = 1.50;
                    workSheet.Column(5).Width = 1.50;
                    workSheet.Column(6).Width = 2.71;
                    workSheet.Column(7).Width = 0.54;
                    workSheet.Column(8).Width = 4.25;
                    workSheet.Column(9).Width = 1.50;
                    workSheet.Column(10).Width = 1.50;
                    workSheet.Column(11).Width = 1.50;
                    workSheet.Column(12).Width = 1.50;
                    workSheet.Column(13).Width = 9.00;
                    workSheet.Column(14).Width = 0.54;
                    workSheet.Column(15).Width = 0.69;
                    workSheet.Column(16).Width = 0.54;
                    workSheet.Column(17).Width = 0.62;
                    workSheet.Column(18).Width = 0.69;
                    workSheet.Column(19).Width = 0.00;
                    workSheet.Column(20).Width = 0.00;
                    workSheet.Column(21).Width = 1.50;
                    workSheet.Column(22).Width = 1.50;
                    workSheet.Column(23).Width = 1.50;
                    workSheet.Column(24).Width = 1.50;
                    workSheet.Column(25).Width = 1.50;
                    workSheet.Column(26).Width = 0.38;
                    workSheet.Column(27).Width = 1.50;
                    workSheet.Column(28).Width = 1.50;
                    workSheet.Column(29).Width = 0.00;
                    workSheet.Column(30).Width = 0.38;
                    workSheet.Column(31).Width = 0.45;
                    workSheet.Column(32).Width = 0.83;
                    workSheet.Column(33).Width = 3.38;
                    workSheet.Column(34).Width = 2.75;
                    workSheet.Column(35).Width = 0.54;
                    workSheet.Column(36).Width = 0.83;
                    workSheet.Column(37).Width = 1.00;
                    workSheet.Column(38).Width = 0.46;
                    workSheet.Column(39).Width = 1.50;
                    workSheet.Column(40).Width = 1.50;
                    workSheet.Column(41).Width = 1.50;
                    workSheet.Column(42).Width = 1.50;

                    // Heading
                    workSheet.Range("A3:AP3").Merge();

                    //old code
                    //workSheet.Cell(2, 1).Style.Font.Bold = true;
                    //workSheet.Cell(4, 1).Style.Font.Bold = false;

                    workSheet.Row(1).Height = 57.75;
                    workSheet.Row(2).Height = 12.75;
                    workSheet.Row(3).Height = 15.00;
                    workSheet.Row(4).Height = 15.00;
                    workSheet.Row(5).Height = 15.00;
                    workSheet.Row(6).Height = 12.75;
                    workSheet.Row(7).Height = 14.25;
                    workSheet.Row(8).Height = 12.00;
                    workSheet.Row(9).Height = 12.00;
                    workSheet.Row(10).Height = 13.50;
                    workSheet.Row(11).Height = 11.25;

                    //---------------- Style Start --------------------------
                    workSheet.Row(4).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(5).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(6).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(7).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(8).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(9).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(10).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(11).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(12).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(13).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(14).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(15).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(16).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(17).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(18).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(19).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(26).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(27).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(28).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(29).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(30).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(31).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(32).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(33).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(35).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(36).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(38).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(39).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(40).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(41).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(42).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(43).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(44).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(45).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(46).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(47).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(48).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(49).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(50).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(51).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(52).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(53).Style.Font.FontName = "Times New Roman";

                    //--------------- Style End -------------------------------


                    workSheet.Row(6).Style.Font.Bold = false;
                    workSheet.Row(13).Style.Font.Bold = false;
                    int j = 6;
                    while (j < 78)
                    {
                        workSheet.Row(j).Style.Font.FontSize = 10;
                        j++;
                    }

                    //string Imagepath = string.Empty;
                    //if (MySession.InvoiceLogoPath != null)
                    //{
                    //    Imagepath = MySession.InvoiceLogoPath;
                    //}
                    //else
                    //    return new Tuple<Stream, string, bool>(spreadsheetstream, InvoiceFileName, true);


                    //IXLRanges myrange = (IXLRanges)workSheet.Range("A1:AO");

                    var image = workSheet.AddPicture(logopath)
                                  .MoveTo(workSheet.Cell(1, 1)).Scale(1.05);

                    //workSheet.Cell(1, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                    //workSheet.Cell(1, 7).Value = "Pte Ltd";
                    //workSheet.Cell(1, 7).Style.Font.Bold = true;
                    //workSheet.Cell(1, 7).Style.Font.FontSize = 20;
                    //workSheet.Cell(1, 7).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Top);

                    //workSheet.Range("A2:B2").Merge();
                    //workSheet.Cell(2, 2).Style.Font.Bold = true;

                    //workSheet.Cell(6, 3).Style.Font.FontSize = 20;

                    //workSheet.Cell(3, 1).Value = "32 Old Toh Tuck Road,#05-01 Ibiz Centre,Singapore 597658 Tel: +65 66726090 Fax: +65 64651656 Email: accounts@smtspl.com";
                    //workSheet.Cell(4, 1).Value = "Mailing Address: Bukit Batok Central Post Office,PO Box 181,Singapore 916507";
                    //workSheet.Cell(4, 1).Style.Font.Bold = false;
                    //workSheet.Cell(5, 1).Value = "Co.Reg.No/GST No: 199901546C";
                    //workSheet.Cell(5, 1).Style.Font.Bold = true;

                    // Invoice Header
                    workSheet.Range("A3:AO3").Merge();
                    workSheet.Cell(3, 1).Value = "Invoice";
                    workSheet.Cell(3, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    workSheet.Cell(3, 1).Style.Font.FontSize = 14;
                    workSheet.Row(3).Style.Font.FontName = "Times New Roman";
                    workSheet.Row(3).Style.Font.Bold = true;

                    // Invoice Number format
                    workSheet.Cell(4, 24).Value = "Invoice No :";
                    workSheet.Cell(4, 24).Style.Font.Bold = true;
                    //workSheet.Cell(4, 24).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                    workSheet.Range("AE4:AO4").Merge();
                    workSheet.Cell(4, 31).Value = invoiceNo;
                    workSheet.Cell(4, 31).Style.Font.Bold = true;

                    // Invoice Date
                    workSheet.Cell(5, 24).Value = "Date :";
                    workSheet.Range("AE5:AO5").Merge();
                    workSheet.Cell(5, 31).Value = DateTime.Now.Date.ToString();
                    workSheet.Cell(5, 31).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);

                    // Vessel and To
                    workSheet.Cell(6, 24).Value = "Vessel :";
                    //workSheet.Cell(6, 24).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                    workSheet.Range("AE6:AO6").Merge();
                    workSheet.Cell(6, 31).Value = VesselName;
                    workSheet.Cell(4, 1).Value = "To:";
                    workSheet.Range("D4:V4").Merge();

                    // Address
                    workSheet.Range("D5:V5").Merge();
                    workSheet.Cell(5, 4).Value = buildingName.ToString();
                    workSheet.Cell(5, 4).Style.Font.Bold = true;
                    //workSheet.Cell(5, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                    workSheet.Range("D6:V6").Merge();
                    workSheet.Cell(6, 4).Value = street.ToString();
                    workSheet.Range("D7:V7").Merge();
                    workSheet.Cell(7, 4).Value = district.ToString();
                    //workSheet.Cell(7, 4).Style.Font.Bold = false;
                    workSheet.Range("D8:V8").Merge();
                    workSheet.Cell(8, 4).Value = city.ToString();
                    workSheet.Range("D9:V9").Merge();
                    workSheet.Cell(9, 4).Value = country.ToString();

                    // Account Code
                    workSheet.Cell(7, 24).Value = "Account Code :";
                    workSheet.Range("AE7:AO7").Merge();

                    // Accounts Department
                    workSheet.Cell(10, 1).Value = " Attn";
                    workSheet.Cell(10, 4).Value = "Accounts Department";

                    // One Space Row
                    workSheet.Row(11).Height = 18;
                    workSheet.Cell(11, 1).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Top);

                    workSheet.Cell(12, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    workSheet.Cell(12, 1).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                    workSheet.Cell(12, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    workSheet.Cell(12, 4).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                    workSheet.Cell(12, 8).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    workSheet.Cell(12, 8).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                    workSheet.Cell(12, 15).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    workSheet.Cell(12, 15).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                    workSheet.Cell(12, 36).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    workSheet.Cell(12, 36).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);

                    workSheet.Range("A12:B12").Merge();
                    workSheet.Cell(12, 1).Value = "Sn";

                    workSheet.Range("D12:F12").Merge();
                    workSheet.Cell(12, 4).Value = "Period";

                    workSheet.Range("H12:M12").Merge();
                    workSheet.Cell(12, 8).Value = "InmarSat ID";

                    workSheet.Range("O12:AH12").Merge();
                    workSheet.Cell(12, 15).Value = "Service Description";

                    workSheet.Range("AJ12:AO12").Merge();
                    workSheet.Cell(12, 36).Value = "Amount";

                    workSheet.Cell(12, 1).Style.Font.Bold = true;
                    workSheet.Cell(12, 4).Style.Font.Bold = true;
                    workSheet.Cell(12, 8).Style.Font.Bold = true;
                    workSheet.Cell(12, 15).Style.Font.Bold = true;
                    workSheet.Cell(12, 36).Style.Font.Bold = true;

                    workSheet.Cell(12, 1).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 1).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 1).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 1).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 2).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 2).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 2).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 5).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 5).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 6).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 6).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 6).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 33).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 33).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 34).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 34).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 34).Style.Border.RightBorder = XLBorderStyleValues.Medium;

                    workSheet.Cell(12, 4).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 4).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 4).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 4).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 9).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 9).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 10).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 10).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 11).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 11).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 12).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 12).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 13).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 13).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 13).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 13).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 8).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 8).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 8).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 8).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 15).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 15).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 15).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 15).Style.Border.RightBorder = XLBorderStyleValues.Medium;

                    workSheet.Cell(12, 16).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 16).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 17).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 17).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 18).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 18).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 19).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 19).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 20).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 20).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 21).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 21).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 22).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 22).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 23).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 23).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 24).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 24).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 25).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 25).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 26).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 26).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 27).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 27).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 28).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 28).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 29).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 29).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 30).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 30).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 31).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 31).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 32).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 32).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 32).Style.Border.RightBorder = XLBorderStyleValues.Medium;

                    workSheet.Cell(12, 36).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 36).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 36).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 36).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 37).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 37).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 38).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 38).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 39).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 39).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 40).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 40).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 41).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 41).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                    workSheet.Cell(12, 41).Style.Border.RightBorder = XLBorderStyleValues.Medium;

                    workSheet.Cell(12, 1).Style.Font.FontSize = 12;
                    workSheet.Cell(12, 4).Style.Font.FontSize = 12;
                    workSheet.Cell(12, 8).Style.Font.FontSize = 12;
                    workSheet.Cell(12, 15).Style.Font.FontSize = 12;
                    workSheet.Cell(12, 36).Style.Font.FontSize = 12;

                    // Empty Space Row
                    workSheet.Row(13).Height = 6;

                    workSheet.Cell(14, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    workSheet.Cell(14, 1).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                    workSheet.Cell(14, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    workSheet.Cell(14, 2).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                    workSheet.Cell(14, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    workSheet.Cell(14, 3).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                    workSheet.Cell(14, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    workSheet.Cell(14, 4).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                    workSheet.Cell(14, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    workSheet.Cell(14, 5).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);

                    int row = 14;
                    decimal? totals = 0;
                    int sno = 1;
                    foreach (var services in dictServiceDescription)
                    {
                        workSheet.Range("A" + row + ":B" + row).Merge();
                        workSheet.Cell(row, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        workSheet.Cell(row, 1).Value = sno++;

                        workSheet.Range("D" + row + ":F" + row).Merge();
                        workSheet.Cell(row, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        workSheet.Cell(row, 4).Style.NumberFormat.Format = "@";
                        workSheet.Cell(row, 4).Value = CurrPeriod;

                        workSheet.Range("H" + row + ":M" + row).Merge();
                        workSheet.Cell(row, 8).Style.NumberFormat.Format = "@";
                        workSheet.Cell(row, 8).Value = ImarsatID;

                        workSheet.Range("O" + row + ":AH" + row).Merge();
                        workSheet.Cell(row, 15).Value = services.Key;

                        workSheet.Range("AJ" + row + ":AO" + row).Merge();
                        workSheet.Cell(row, 36).Value = services.Value;

                        totals += services.Value;
                        row++;
                    }

                    // workSheet.Cells[13, 7].Style.Font.Size = 12;
                    workSheet.SheetView.Freeze(12, 0);
                    // workSheet.Row(15).Style.Font.Bold = false;
                    // workSheet.Row(17).Style.Font.Bold = true;
                    workSheet.Cell(18, 6).Style.Font.Bold = true;
                    workSheet.Row(20).Style.Font.Bold = true;
                    workSheet.Row(21).Style.Font.Bold = true;
                    workSheet.Row(22).Style.Font.Bold = true;
                    workSheet.Row(23).Style.Font.Bold = true;

                    // workSheet.Cell(31, 22).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    //workSheet.Cell(31, 22).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                    workSheet.Cell(31, 22).Value = "Total";

                    workSheet.Cell(31, 36).Value = totals;
                    workSheet.Cell(31, 36).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                    workSheet.Cell(31, 37).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                    workSheet.Cell(31, 38).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                    workSheet.Cell(31, 39).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                    workSheet.Cell(31, 40).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                    workSheet.Cell(31, 41).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);

                    workSheet.Cell(32, 22).Value = "Exchange Rate";
                    workSheet.Cell(33, 22).Value = "Administrative Fee";
                    workSheet.Cell(33, 31).Value = "%";
                    workSheet.Cell(33, 36).Value = "-";
                    workSheet.Cell(33, 36).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                    workSheet.Row(38).Style.Font.FontSize = 9;
                    workSheet.Row(39).Style.Font.FontSize = 9;
                    workSheet.Row(40).Style.Font.FontSize = 9;
                    workSheet.Row(41).Style.Font.FontSize = 9;
                    workSheet.Row(42).Style.Font.FontSize = 9;
                    workSheet.Row(43).Style.Font.FontSize = 9;
                    workSheet.Row(44).Style.Font.FontSize = 9;
                    workSheet.Row(45).Style.Font.FontSize = 9;
                    workSheet.Row(46).Style.Font.FontSize = 9;
                    workSheet.Row(47).Style.Font.FontSize = 9;
                    workSheet.Row(48).Style.Font.FontSize = 9;
                    workSheet.Row(49).Style.Font.FontSize = 9;
                    workSheet.Row(50).Style.Font.FontSize = 9;
                    workSheet.Row(51).Style.Font.FontSize = 9;
                    workSheet.Row(52).Style.Font.FontSize = 9;
                    workSheet.Row(53).Style.Font.FontSize = 9;
                    workSheet.Cell(35, 27).Value = "Good Service Tax %";
                    workSheet.Cell(36, 22).Value = "Total Payable";
                    workSheet.Cell(36, 32).Value = "(USD)";
                    workSheet.Range("AJ31:AO31").Merge();
                    workSheet.Range("AJ32:AO32").Merge();
                    workSheet.Range("AJ33:AO33").Merge();
                    workSheet.Range("AJ35:AO35").Merge();
                    workSheet.Range("AJ36:AO36").Merge();
                    workSheet.Cell(36, 36).Value = totals;
                    workSheet.Cell(36, 36).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                    workSheet.Cell(36, 37).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                    workSheet.Cell(36, 38).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                    workSheet.Cell(36, 39).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                    workSheet.Cell(36, 40).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                    workSheet.Cell(36, 41).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);

                    workSheet.Cell(36, 22).Style.Font.Bold = true;
                    workSheet.Cell(36, 32).Style.Font.Bold = true;
                    workSheet.Cell(36, 35).Style.Font.Bold = true;
                    workSheet.Range("A37:AO37").Merge();
                    workSheet.Range("A37:AO37").Style.Border.BottomBorder = XLBorderStyleValues.Thick;

                    workSheet.Cell(38, 2).Value = "Important Notes";
                    workSheet.Cell(38, 2).Style.Font.Bold = true;
                    workSheet.Cell(38, 24).Value = "For Comptroller Purposes Only,Rate @ 1.37";
                    workSheet.Cell(39, 2).Value = "1";
                    workSheet.Cell(39, 4).Value = "Credit Terms: 30 days from Date of Invoice";
                    workSheet.Cell(40, 2).Value = "2";
                    workSheet.Cell(40, 4).Value = "Please quote clearly your account and invoice no when making payment";
                    workSheet.Cell(41, 2).Value = "3";
                    workSheet.Cell(41, 4).Value = "All Bank charges to be borne by remitter.";

                    workSheet.Cell(42, 2).Value = "4";
                    workSheet.Cell(42, 4).Value = "Please send all checks payment to";
                    workSheet.Cell(43, 4).Value = "Bukit Batok Central Post Office PO Box 181 Singapore 916507";

                    workSheet.Cell(44, 2).Value = "5";
                    workSheet.Cell(44, 4).Value = "Payment by Telegraphic Transfer can also be made to";
                    workSheet.Cell(45, 4).Value = "Beneficiary";
                    workSheet.Cell(45, 14).Value = "SMTS Pte Ltd";
                    workSheet.Cell(45, 14).Style.Font.Bold = true;
                    workSheet.Cell(46, 4).Value = "USD Account no";
                    workSheet.Cell(46, 14).Value = "651-000093-301";
                    workSheet.Cell(46, 14).Style.Font.Bold = true;
                    workSheet.Cell(47, 4).Value = "OCBC Swift Code";
                    workSheet.Cell(47, 14).Value = "OCBCSGSG";
                    workSheet.Cell(47, 14).Style.Font.Bold = true;
                    workSheet.Cell(48, 4).Value = "Bank";
                    workSheet.Cell(48, 14).Value = "Oversea-Chinese Banking Corporation Limited";
                    workSheet.Cell(48, 14).Style.Font.Bold = true;
                    workSheet.Cell(49, 4).Value = "Address";
                    workSheet.Cell(49, 14).Value = "10 Marina Boulevard #01-04 Marina Bay Financial Center Tower 2";
                    workSheet.Cell(49, 14).Style.Font.Bold = true;
                    workSheet.Cell(50, 14).Value = "Singapore 018983";
                    workSheet.Cell(50, 14).Style.Font.Bold = true;
                    workSheet.Cell(51, 4).Value = "Agent Swift Code";
                    workSheet.Cell(51, 14).Value = "CHASUS33";
                    workSheet.Cell(51, 14).Style.Font.Bold = true;
                    workSheet.Cell(52, 4).Value = "Agent Bank";
                    workSheet.Cell(52, 14).Value = "JP Morgan";
                    workSheet.Cell(52, 14).Style.Font.Bold = true;
                    workSheet.Cell(53, 2).Value = "6";
                    workSheet.Cell(53, 4).Value = "Please make full payment by the due date to avoid 1.5% interest for late payment charge per month";
                    workSheet.Cell(53, 4).Style.Font.Bold = true;

                    #endregion

                    // Save Work sheet into workbook
                    xLWorkbook.SaveAs(spreadsheetstream);
                    spreadsheetstream.Position = 0;

                    //InvoiceFileName = temppath + "\\" + InvoiceFileName;

                    //FileStream fileStream = new FileStream(InvoiceFileName, FileMode.Create);

                    return new Tuple<Stream, string, bool>(spreadsheetstream, InvoiceFileName, false);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
                throw new Exception("The master primary is not an interger, please check and update the issue.");
        }

        public Tuple<Stream, string> GnerateCalllog(string primaryID, string logopath)
        {
            Stream SpreadsheetStream = new MemoryStream();

            int value = 0;
            int SPID = 0;
            int CusID = 0;
            string LESName = string.Empty;
            string imsiID = string.Empty;
            string ImarSatID = string.Empty;
            string InvoiceNo = string.Empty;
            string CustomerAddress = string.Empty;
            string customerName = string.Empty;
            string CallLogName = string.Empty;
            string VesselName = string.Empty;
            string BillNo = string.Empty;

            decimal? Cus_DataPlan = null;
            decimal? Cus_OutbundleRate = null;
            decimal? LES_DataPlan = null;
            decimal? LES_OutbundleRate = null;
            int SimID = 0;
            if (int.TryParse(primaryID, out value))
            {
                try
                {
                    int id = Convert.ToInt32(primaryID);

                    BillingSystemEntities db = new BillingSystemEntities();
                    var cusMaster = db.BS2MtCusT.FirstOrDefault(t => t.BSMtCusTID == id);

                    if (cusMaster != null)
                    {
                        var sim = db.BptFbSims.Where(t => t.Sim_ID == cusMaster.fkSimID).Select(t => new { t.BPtFBImImsi, t.BPtFBimMsisdn, t.Sim_ID });
                        foreach (var val in sim)
                        {
                            imsiID = val.BPtFBImImsi;
                            ImarSatID = val.BPtFBimMsisdn;
                            SimID = val.Sim_ID;
                        }

                        var cusRegs = db.BsPtCusRegs_.FirstOrDefault(t => t.fkSimID == SimID);
                        if (cusRegs != null)
                        {
                            Cus_DataPlan = Convert.ToDecimal(cusRegs.BPtCusRgnFBDataPlanIn);
                            Cus_OutbundleRate = cusRegs.BPtCusRgnFbOutBundleDataRate;
                        }

                        var Les_Reg = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSPID == LesID && t.fkSimID == SimID);
                        if (Les_Reg != null)
                        {
                            LES_DataPlan = Convert.ToDecimal(Les_Reg.BPtLesRgnFBDataPlanIn);
                            LES_OutbundleRate = Les_Reg.BPtLesRgnFbOutBundleDataRate;
                        }

                        SPID = cusMaster.fkSPID;
                        CusID = cusMaster.fkCusID;
                        InvoiceNo = cusMaster.BSMtCusInvNo;
                        try
                        {
                            CustomerAddress = db.BsPtCusRegs_.FirstOrDefault(t => t.fkSimID == cusMaster.fkSimID).BsPtCusAddr;
                            string ServiceProviderName = db.BS2MtCusT.FirstOrDefault(t => t.BSMtCusTID == id).BsMtServiceProvider.ServiceProvider;
                            int year = DateTime.Now.Year;
                            int month = DateTime.Now.Month;
                            BillNo = ServiceProviderName + "" + year + "" + month;
                        }
                        catch
                        {
                            CustomerAddress = string.Empty;
                        }
                        try
                        {
                            VesselName = db.BPtShips.FirstOrDefault(t => t.BPtShipMMSI.Equals(imsiID)).BPtShipName;
                            LESName = db.BsMtLESRefs.FirstOrDefault(t => t.LesRefID == cusMaster.fkLesRefID).LESCode;
                        }
                        catch { VesselName = string.Empty; }

                        if (!string.IsNullOrEmpty(CustomerAddress))
                        {
                            string[] addressparts = CustomerAddress.Split(',');

                            customerName = addressparts.Length > 0 ? addressparts[0] : string.Empty;
                            //street = addressparts.Length > 1 ? addressparts[1] : string.Empty;
                            //district = addressparts.Length > 2 ? addressparts[2] : string.Empty;
                            //city = addressparts.Length > 3 ? addressparts[3] : string.Empty;
                            //country = addressparts.Length > 4 ? addressparts[4] : string.Empty;
                        }

                        CallLogName = InvoiceNo + 'C' + '_' + customerName + '_' + VesselName;

                        //ExcelPackage excel = new ExcelPackage();                        
                        XLWorkbook xLWorkbook = new XLWorkbook();

                        Dictionary<string, List<Dictionary<string, dynamic>>> dictCallLogRecords = new Dictionary<string, List<Dictionary<string, dynamic>>>();
                        List<Dictionary<string, List<Dictionary<string, dynamic>>>> lstdictCallLogRecords = new List<Dictionary<string, List<Dictionary<string, dynamic>>>>();

                        List<Dictionary<string, dynamic>> lstDict = new List<Dictionary<string, dynamic>>();
                        Dictionary<string, dynamic> dictRecords = new Dictionary<string, dynamic>();

                        int count = 0;
                        int sheet = 1;

                        //List<string> ServiceTypes = db.Bs2Sec.AsEnumerable().
                        //                            Where(t => t.fkCTFbID == id )
                        //                            .Select(t => t.BsPtSPServiceDest.BsPtServiceTypes).Distinct().ToList();

                        if (cusMaster.BSMtCusServType != null)
                        {
                            string serviceType = "Service Type :" + cusMaster.BSMtCusServType + " " + "Id No:" + ImarSatID;

                            dictRecords.Add("datetime", null);
                            dictRecords.Add("les", null);
                            dictRecords.Add("origindestination", cusMaster.BSMtCusServDest);
                            dictRecords.Add("usage", cusMaster.BSMtCusTransUsage);
                            dictRecords.Add("unittype", cusMaster.BSMtCusUOM);
                            dictRecords.Add("rate", null);
                            dictRecords.Add("charges(usd)", Math.Round(Convert.ToDecimal(cusMaster.BSMtCusRecurFee), 2, MidpointRounding.AwayFromZero));

                            lstDict.Add(dictRecords);
                            dictCallLogRecords.Add(serviceType, lstDict);
                            count += 2;
                        }

                        lstdictCallLogRecords.Add(dictCallLogRecords);
                        dictCallLogRecords = new Dictionary<string, List<Dictionary<string, dynamic>>>();
                        lstDict = new List<Dictionary<string, dynamic>>();


                        IXLWorksheet workSheet;
                        int skipIndex = 0;
                        int totalIndex = db.Bs2Sec.Where(t => t.fkCTFbID == id).Count();
                        decimal? totalCharged = 0;
                        decimal outbundleused = 0;
                        do
                        {
                            int take = 60 - count;
                            count = 0;

                            var ServiceTypes = db.Bs2Sec.AsEnumerable().
                                                    Where(t => t.fkCTFbID == id).Skip(skipIndex).Take(take)
                                                    .GroupBy(t => t.fkSPServTypeID).ToList();
                            //.Select(t => t.BsPtSPServiceDest.BsPtServiceTypes).ToList();

                            skipIndex += take;
                            foreach (var group in ServiceTypes)
                            {
                                lstDict = new List<Dictionary<string, dynamic>>();
                                dictCallLogRecords = new Dictionary<string, List<Dictionary<string, dynamic>>>();
                                string serviceType = string.Empty;
                                foreach (var obj in group)
                                {
                                    serviceType = "Service Type :" + obj.BsPtCusServiceDest.BsPtCusServiceTypes + "," + "Id No:" + ImarSatID;
                                    //count++;
                                    //Data Call Log InBundle Data
                                    //List<Bs2Sec> lstCalllogRecords = db.Bs2Sec
                                    //                .Where(t => t.BsPtSPServiceDest.BsPtServiceTypes.ToLower().Equals(services.ToLower())
                                    //                && (t.fkCTFbID == id)).ToList();

                                    //if (lstCalllogRecords != null)
                                    //{
                                    //    foreach (var obj in lstCalllogRecords)
                                    //    {
                                    //        count++;
                                    dictRecords = new Dictionary<string, dynamic>();

                                    // Date or Time
                                    if (obj.BsCtStartDate != null)
                                        dictRecords.Add("datetime", Convert.ToDateTime(obj.BsCtStartDate).ToShortDateString() + " / " + obj.BsCtStarTime);
                                    else
                                        dictRecords.Add("datetime", obj.BsCtStartDate + " / " + obj.BsCtStarTime);

                                    //LES
                                    dictRecords.Add("les", null);
                                    // Origin or Destination
                                    dictRecords.Add("origindestination", !string.IsNullOrEmpty(obj.BsCtDestination) ? obj.BsCtDestination : obj.BsPtCusServiceDest.BsPtCusServiceTypes);
                                    // Usage
                                    decimal usage = Math.Round(Convert.ToDecimal(obj.BsCtTransUsage), 2, MidpointRounding.AwayFromZero);
                                    dictRecords.Add("usage", usage);
                                    // Unit Type
                                    dictRecords.Add("unittype", obj.BsMtUnit.Units);
                                    if ((!string.IsNullOrEmpty(obj.BsMtUnit.Units)) && (obj.BsMtUnit.Units.Contains("MB") || obj.BsMtUnit.Units.Contains("mb") || obj.BsMtUnit.Units.Contains("megabytes") || obj.BsMtUnit.Units.Contains("megabyte") || obj.BsMtUnit.Units.Contains("MegaByte") || obj.BsMtUnit.Units.Contains("MegaBytes")))
                                    {
                                        outbundleused += usage;
                                    }
                                    // Rate
                                    decimal? sellerPrice = 0;
                                    try
                                    {
                                        sellerPrice = db.SP_GetSellerostPrice(CusID, SPID, obj.BsPtCusServiceDest.BsPtCusServiceTypes).FirstOrDefault();
                                        //sellerPrice = db.BsPtCusServiceDests.FirstOrDefault(t => t.BsPtCusServiceTypes.ToLower().Equals(services.ToLower())
                                        //                                                  && (t.fkCusID !=null || t.fkCusID == CusID) && t.fkSpID == SPID).BsPtCusServiceSellerPrice;
                                    }
                                    catch { }

                                    int commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("Data")).ComID;

                                    if (serviceType.ToLower().Contains("standard ip") || serviceType.ToLower().Contains("background ip") && (obj.fkCommType == commtype))
                                    {
                                        if (Cus_DataPlan <= outbundleused)
                                        {
                                            dictRecords.Add("rate", Cus_OutbundleRate);
                                        }
                                        else
                                            dictRecords.Add("rate", 0);
                                    }
                                    else
                                        dictRecords.Add("rate", sellerPrice);
                                    // Total charge in USD
                                    //dictRecords.Add("charges(usd)", Math.Round(Convert.ToDecimal((usage * sellerPrice)), 2, MidpointRounding.AwayFromZero));
                                    dictRecords.Add("charges(usd)", Math.Round(Convert.ToDecimal((obj.BsCtTotalUsage)), 2, MidpointRounding.AwayFromZero));

                                    lstDict.Add(dictRecords);
                                    //    }

                                    //}

                                }
                                dictCallLogRecords.Add(serviceType, lstDict);
                                lstdictCallLogRecords.Add(dictCallLogRecords);
                            }
                            try
                            {
                                workSheet = xLWorkbook.Worksheets.Add("CallLog" + sheet);
                                sheet++;
                                bool islastSheet = false;

                                if (skipIndex > totalIndex)
                                    islastSheet = true;

                                totalCharged = CreateWorkSheet(workSheet, InvoiceNo, customerName, LESName, BillNo, VesselName, lstdictCallLogRecords, islastSheet, totalCharged, logopath);
                                count = 0;
                                dictCallLogRecords = new Dictionary<string, List<Dictionary<string, dynamic>>>();
                                lstDict = new List<Dictionary<string, dynamic>>();
                                lstdictCallLogRecords = new List<Dictionary<string, List<Dictionary<string, dynamic>>>>();
                            }
                            catch (Exception error) { }

                        } while (skipIndex < totalIndex);

                        //lstdictCallLogRecords.Add(dictCallLogRecords);
                        //dictCallLogRecords = new Dictionary<string, List<Dictionary<string, dynamic>>>();
                        //lstDict = new List<Dictionary<string, dynamic>>();

                        //foreach (string services in ServiceTypes)
                        //{
                        //    string serviceType = "Service Type :" + services + "," + "Id No:" + ImarSatID;
                        //    count++;
                        //    //Data Call Log InBundle Data
                        //    List<Bs2Sec> lstCalllogRecords = db.Bs2Sec
                        //                    .Where(t => t.BsPtSPServiceDest.BsPtServiceTypes.ToLower().Equals(services.ToLower())
                        //                    && (t.fkCTFbID == id)).ToList();

                        //    if (lstCalllogRecords != null)
                        //    {
                        //        foreach (var obj in lstCalllogRecords)
                        //        {
                        //            count++;
                        //            dictRecords = new Dictionary<string, dynamic>();

                        //            // Date or Time
                        //            if (obj.BsCtStartDate != null)
                        //                dictRecords.Add("datetime", Convert.ToDateTime(obj.BsCtStartDate).ToShortDateString() + " / " + obj.BsCtStarTime);
                        //            else
                        //                dictRecords.Add("datetime", obj.BsCtStartDate + " / " + obj.BsCtStarTime);

                        //            //LES
                        //            dictRecords.Add("les", null);
                        //            // Origin or Destination
                        //            dictRecords.Add("origindestination", !string.IsNullOrEmpty(obj.BsCtDestination) ? obj.BsCtDestination : obj.BsPtSPServiceDest.BsPtServiceTypes);
                        //            // Usage
                        //            decimal usage = Math.Round(Convert.ToDecimal(obj.BsCtBillIncre), 2, MidpointRounding.AwayFromZero);
                        //            dictRecords.Add("usage", usage);
                        //            // Unit Type
                        //            dictRecords.Add("unittype", obj.BsMtUnit.Units);
                        //            // Rate
                        //            decimal? sellerPrice = 0;
                        //            try
                        //            {
                        //                sellerPrice = db.SP_GetSellerostPrice(CusID, SPID, services).FirstOrDefault();
                        //                //sellerPrice = db.BsPtCusServiceDests.FirstOrDefault(t => t.BsPtCusServiceTypes.ToLower().Equals(services.ToLower())
                        //                //                                                  && (t.fkCusID !=null || t.fkCusID == CusID) && t.fkSpID == SPID).BsPtCusServiceSellerPrice;
                        //            }
                        //            catch { }
                        //            dictRecords.Add("rate", sellerPrice);
                        //            // Total charge in USD
                        //            dictRecords.Add("charges(usd)", Math.Round(Convert.ToDecimal((usage * sellerPrice)), 2, MidpointRounding.AwayFromZero));

                        //            lstDict.Add(dictRecords);

                        //            if (count > 65)
                        //            {
                        //                try
                        //                {
                        //                    dictCallLogRecords.Add(serviceType, lstDict);
                        //                    lstdictCallLogRecords.Add(dictCallLogRecords);
                        //                    IXLWorksheet workSheetData = xLWorkbook.Worksheets.Add("CallLog" + sheet);
                        //                    sheet++;
                        //                    CreateWorkSheet(workSheetData, InvoiceNo, customerName, LESName, BillNo, VesselName, lstdictCallLogRecords);
                        //                    count = 0;
                        //                    dictCallLogRecords = new Dictionary<string, List<Dictionary<string, dynamic>>>();
                        //                    lstDict = new List<Dictionary<string, dynamic>>();
                        //                    lstdictCallLogRecords = new List<Dictionary<string, List<Dictionary<string, dynamic>>>>();
                        //                }
                        //                catch (Exception error) { }
                        //            }
                        //        }

                        //    }

                        //    if (lstDict.Count > 0)
                        //    {
                        //        dictCallLogRecords.Add(serviceType, lstDict);
                        //        lstdictCallLogRecords.Add(dictCallLogRecords);                                
                        //        dictCallLogRecords = new Dictionary<string, List<Dictionary<string, dynamic>>>();
                        //        lstDict = new List<Dictionary<string, dynamic>>();
                        //    }
                        //}
                        //IXLWorksheet workSheetDatas = xLWorkbook.Worksheets.Add("CallLog" + sheet);
                        //CreateWorkSheet(workSheetDatas, InvoiceNo, customerName, LESName, BillNo, VesselName, lstdictCallLogRecords);


                        var lst = from dd in dictCallLogRecords.Values.Take(78) select dd;



                        // Excel Sheet for CallLog Data 
                        #region CallLog Data
                        //                        IXLWorksheet workSheetData = xLWorkbook.Worksheets.Add("Data");

                        //                        var ws = workSheetData.Worksheet.PageSetup;
                        //                        ws.CenterHorizontally = false;
                        //                        ws.CenterVertically = false;
                        //                        ws.Margins.Top = 0;
                        //                        ws.Margins.Bottom = 0.6;
                        //                        ws.Margins.Left = 0.5;
                        //                        ws.Margins.Right = 0;
                        //                        ws.Margins.Footer = 0.5;
                        //                        ws.Margins.Header = 0;
                        //                        ws.AdjustTo(90);

                        //                        workSheetData.Column(1).Width = 16;
                        //                        workSheetData.Column(2).Width = 15;
                        //                        workSheetData.Column(3).Width = 25;
                        //                        workSheetData.Column(4).Width = 10;
                        //                        workSheetData.Column(5).Width = 10;
                        //                        workSheetData.Column(6).Width = 10;
                        //                        workSheetData.Column(7).Width = 10;
                        //                        workSheetData.Column(8).Width = 10;
                        //                        workSheetData.Cell(2, 1).Style.Font.Bold = true;
                        //                        workSheetData.Cell(4, 1).Style.Font.Bold = true;
                        //                        workSheetData.Cell(11, 1).Style.Font.Bold = true;
                        //                        workSheetData.Cell(12, 1).Style.Font.Bold = true;
                        //                        workSheetData.Cell(12, 2).Style.Font.Bold = true;
                        //                        workSheetData.Cell(3, 6).Style.Font.Bold = true;
                        //                        workSheetData.Row(6).Style.Font.Bold = true;

                        //                        int j = 6;
                        //                        while (j < 78)
                        //                        {
                        //                            workSheetData.Row(j).Style.Font.FontSize = 8;
                        //                            workSheetData.Row(j).Style.Font.FontName = "Tahoma";
                        //                            workSheetData.Row(j).Style.Font.Bold = true;
                        //                            j++;
                        //                        }

                        //                        string Imagepath = MySession.InvoiceLogoPath;

                        //                        workSheetData.Range("A1:E2").Merge();
                        //                        var image = workSheetData.AddPicture(Imagepath)
                        //                                  .MoveTo(workSheetData.Cell(1, 1)).Scale(1.05);

                        //                        //workSheet.Cell(2, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                        //                        //workSheet.Cell(2, 1).Value = "Pte Ltd";
                        //                        //workSheet.Cell(2, 1).Style.Font.Bold = true;
                        //                        //workSheet.Cell(2, 1).Style.Font.FontSize = 20;
                        ////                        workSheetData.Range("A2:B2").Merge();
                        //                        // workSheet.Cell(2, 1).Value = "SMTS Pte Ltd";
                        //                        //workSheet.Cell(2, 1).Style.Font.FontSize = 20;
                        //                        workSheetData.Range("A5:G5").Merge();
                        //                        workSheetData.Cell(5, 1).Value = "Call Data Details";
                        //                        workSheetData.Cell(5, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //                        workSheetData.Cell(5, 1).Style.Font.FontSize = 14;
                        //                        workSheetData.Row(5).Style.Font.FontName = "Times New Roman";
                        //                        workSheetData.Row(5).Style.Font.Bold = true;

                        //                        workSheetData.Row(12).Style.Font.Bold = false;
                        //                        workSheetData.Cell(6, 1).Value = "Invoice No";
                        //                        workSheetData.Cell(6, 2).Value = InvoiceNo;

                        //                        workSheetData.Cell(6, 4).Value = "Customer";
                        //                        workSheetData.Cell(6, 5).Value = customerName;
                        //                        workSheetData.Row(6).Style.Font.Bold = true;
                        //                        workSheetData.Row(6).Style.Font.FontName = "Tahoma";

                        //                        workSheetData.Cell(7, 1).Value = "Invoice Date";
                        //                        workSheetData.Cell(7, 2).Value = DateTime.Now.Date;
                        //                        workSheetData.Cell(7, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                        //                        workSheetData.Row(7).Style.Font.FontName = "Tahoma";

                        //                        workSheetData.Cell(7, 4).Value = "LES";
                        //                        workSheetData.Cell(7, 5).Value = LESName;
                        //                        workSheetData.Row(7).Style.Font.Bold = true;

                        //                        workSheetData.Cell(8, 1).Value = "Bill No:";
                        //                        workSheetData.Cell(8, 2).Value = BillNo;
                        //                        workSheetData.Row(8).Style.Font.Bold = true;

                        //                        workSheetData.Cell(8, 4).Value = "Vessel";
                        //                        workSheetData.Cell(8, 5).Value = VesselName;
                        //                        workSheetData.Row(8).Style.Font.Bold = true;

                        //                        workSheetData.Cell(10, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //                        workSheetData.Cell(10, 1).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                        //                        workSheetData.Cell(10, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //                        workSheetData.Cell(10, 2).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                        //                        workSheetData.Cell(10, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //                        workSheetData.Cell(10, 3).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                        //                        workSheetData.Cell(10, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //                        workSheetData.Cell(10, 4).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                        //                        workSheetData.Cell(10, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //                        workSheetData.Cell(10, 5).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                        //                        workSheetData.Cell(10, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //                        workSheetData.Cell(10, 6).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                        //                        workSheetData.Cell(10, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //                        workSheetData.Cell(10, 7).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                        //                        workSheetData.Cell(10, 1).Value = "Date/Time";
                        //                        workSheetData.Cell(10, 2).Value = "LES";
                        //                        workSheetData.Cell(10, 3).Value = "Orgin//Destination";
                        //                        workSheetData.Cell(10, 4).Value = "Usage";
                        //                        workSheetData.Cell(10, 5).Value = "Unit";
                        //                        workSheetData.Cell(10, 6).Value = "Rate";
                        //                        workSheetData.Cell(10, 7).Value = "Charges(USD)";

                        //                        //workSheet.Cell(25, 6).Value = "Total(USD)";

                        //                        workSheetData.Row(10).Style.Font.Bold = true;

                        //                        workSheetData.Cell(10, 1).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 1).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 1).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 1).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 2).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 2).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 2).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 2).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 3).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 3).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 3).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 3).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 4).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 4).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 4).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 4).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 5).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 5).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 5).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 5).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 6).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 6).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 6).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 6).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 7).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 7).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 7).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                        //                        workSheetData.Cell(10, 7).Style.Border.LeftBorder = XLBorderStyleValues.Medium;

                        //                        workSheetData.Row(25).Style.Font.Bold = true;
                        //                        workSheetData.SheetView.Freeze(10, 0);

                        //                        int count = 11, cond = 11;

                        //                        int dataServType = 0, VceServType = 0;

                        //                        List<string> ServiceDest = db.Bs2Sec.AsEnumerable().
                        //                                                    Where(t => t.fkCTFbID == id && (t.BsMtCommunicationType.ComTypes.Equals("Data")))
                        //                                                    .Select(t => t.BsPtSPServiceDest.BsPtServiceTypes).Distinct().ToList();

                        //                        List<Bs2Sec> lstInvoice;

                        //                        Dictionary<string, Dictionary<string, dynamic>> keyValuePairs = new Dictionary<string, Dictionary<string, dynamic>>();

                        //                        decimal? TotalChargeUSD = 0;
                        //                        decimal? SMTStotalCharge_CP = 0;

                        //                        if (cusMaster.BSMtCusServType != null)
                        //                        {
                        //                            dataServType++;

                        //                            workSheetData.Cell(count, 1).Value = "Service Type :" + cusMaster.BSMtCusServType + " " + "Id No:" + ImarSatID;

                        //                            count++;
                        //                            cond++;

                        //                            // Les
                        //                            //workSheet.Cell(count, 2).Value = objB.Bt2CusFbTranDStartDate;
                        //                            // Origin or Destination
                        //                            workSheetData.Cell(count, 3).Style.NumberFormat.SetNumberFormatId((int)XLPredefinedFormat.Number.Integer);
                        //                            workSheetData.Cell(count, 3).Value = cusMaster.BSMtCusServDest;
                        //                            // Usage
                        //                            decimal usage = Math.Round(Convert.ToDecimal(cusMaster.BSMtCusBillInce), 2, MidpointRounding.AwayFromZero);
                        //                            workSheetData.Cell(count, 4).Value = usage;
                        //                            // Unit Type
                        //                            workSheetData.Cell(count, 5).Value = cusMaster.BSMtCusUOM;

                        //                            // Total charge in USD
                        //                            workSheetData.Cell(count, 7).Value = Math.Round(Convert.ToDecimal(cusMaster.BSMtCusRecurFee), 2, MidpointRounding.AwayFromZero);
                        //                            TotalChargeUSD += Math.Round(Convert.ToDecimal(cusMaster.BSMtCusRecurFee), 2, MidpointRounding.AwayFromZero);
                        //                        }

                        //                        foreach (string services in ServiceDest)
                        //                        {
                        //                            dataServType++;

                        //                            workSheetData.Cell(++count, 1).Value = "Service Type :" + services + " " + "Id No:" + ImarSatID;

                        //                            //Data Call Log InBundle Data
                        //                            IQueryable<Bs2Sec> IQbs2sec = db.Bs2Sec
                        //                                            .Where(t => t.BsPtSPServiceDest.BsPtServiceTypes.ToLower().Equals(services.ToLower()) && (t.fkCTFbID == id) && (t.BsMtCommunicationType.ComTypes.Equals("Data")));
                        //                            lstInvoice = IQbs2sec.ToList();

                        //                            if (lstInvoice != null)
                        //                            {
                        //                                decimal stdIPSubtotal = 0;
                        //                                foreach (var obj in lstInvoice)
                        //                                {
                        //                                    count++;
                        //                                    cond++;

                        //                                    // Date or Time
                        //                                    if (obj.BsCtStartDate != null)
                        //                                        workSheetData.Cell(count, 1).Value = Convert.ToDateTime(obj.BsCtStartDate).ToShortDateString() + " / " + obj.BsCtStarTime;
                        //                                    else
                        //                                        workSheetData.Cell(count, 1).Value = obj.BsCtStartDate + " / " + obj.BsCtStarTime;

                        //                                    // Les
                        //                                    //workSheet.Cell(count, 2).Value = objB.Bt2CusFbTranDStartDate;
                        //                                    // Origin or Destination
                        //                                    workSheetData.Cell(count, 3).Style.NumberFormat.SetNumberFormatId((int)XLPredefinedFormat.Number.Integer);
                        //                                    workSheetData.Cell(count, 3).Value = !string.IsNullOrEmpty(obj.BsCtDestination)? obj.BsCtDestination : obj.BsPtSPServiceDest.BsPtServiceTypes ;
                        //                                    // Usage
                        //                                    decimal usage = Math.Round(Convert.ToDecimal(obj.BsCtBillIncre), 2, MidpointRounding.AwayFromZero);
                        //                                    workSheetData.Cell(count, 4).Value = usage;
                        //                                    // Unit Type
                        //                                    workSheetData.Cell(count, 5).Value = obj.BsMtUnit.Units;

                        //                                    // Rate
                        //                                    decimal? sellerPrice = db.BsPtCusServiceDests.FirstOrDefault(t => t.BsPtCusServiceTypes.ToLower().Equals(services.ToLower())).BsPtCusServiceSellerPrice;
                        //                                    workSheetData.Cell(count, 6).Value = sellerPrice;

                        //                                    // Total charge in USD
                        //                                    workSheetData.Cell(count, 7).Value = Math.Round(Convert.ToDecimal((usage * sellerPrice)), 2, MidpointRounding.AwayFromZero);
                        //                                    TotalChargeUSD += Math.Round(Convert.ToDecimal((usage * sellerPrice)), 2, MidpointRounding.AwayFromZero);
                        //                                    stdIPSubtotal += Math.Round(Convert.ToDecimal((usage * sellerPrice)), 2, MidpointRounding.AwayFromZero);

                        //                                    //SMTS Cost Price
                        //                                    //SMTStotalCharge_CP += Math.Round(Convert.ToDecimal((usage * SMTSdataRate1mb_CP)), 2, MidpointRounding.AwayFromZero);
                        //                                }
                        //                                count++;
                        //                                if (stdIPSubtotal > 0)
                        //                                {
                        //                                    workSheetData.Cell(count, 3).Value = string.Format("SUM({0}/{1})", services, ImarSatID);
                        //                                    workSheetData.Cell(count, 7).Value = stdIPSubtotal;
                        //                                }
                        //                            }

                        //                            #region Data CallLog for OutBundle data Rate
                        //                            ////Data CallLog for OutBundle data Rate

                        //                            //lstInvoiceB = db.BSt2B.AsEnumerable()
                        //                            //                .Where(t => t.Bt2CusFbTranDServType != null
                        //                            //                        && t.Bt2CusFbTranDServType.ToLower().Equals(services.ToLower())
                        //                            //                        && (t.Bt2CusFbIMSIId.Equals(imsiID)) && t.Bt2CusFbisOutBundle).ToList();

                        //                            //if (lstInvoiceB != null)
                        //                            //{
                        //                            //    workSheetData.Cell(count, 1).Value = "Service Type :" + services + " OutBundle " + "Id No:" + imsiID;

                        //                            //    decimal stdIPOutbundleTotal = 0;

                        //                            //    foreach (BSt2B objB in lstInvoiceB)
                        //                            //    {
                        //                            //        count++;
                        //                            //        cond++;

                        //                            //        // Date or Time
                        //                            //        if (objB.Bt2CusFbTranDStartDate != null)
                        //                            //            workSheetData.Cell(count, 1).Value = Convert.ToDateTime(objB.Bt2CusFbTranDStartDate).ToShortDateString() + " / " + objB.Bt2CusFbTranDStartTime;
                        //                            //        else
                        //                            //            workSheetData.Cell(count, 1).Value = objB.Bt2CusFbTranDStartDate + " / " + objB.Bt2CusFbTranDStartTime;

                        //                            //        // Les
                        //                            //        //workSheet.Cell(count, 2).Value = objB.Bt2CusFbTranDStartDate;
                        //                            //        // Origin or Destination
                        //                            //        workSheetData.Cell(count, 3).Style.NumberFormat.SetNumberFormatId((int)XLPredefinedFormat.Number.Integer);
                        //                            //        workSheetData.Cell(count, 3).Value = objB.Bt2CusFbTranDDest;
                        //                            //        // Usage
                        //                            //        decimal usage = Math.Round(Convert.ToDecimal(objB.Bt2CusFbBilIncre), 2, MidpointRounding.AwayFromZero);
                        //                            //        workSheetData.Cell(count, 4).Value = usage;
                        //                            //        // Unit Type
                        //                            //        workSheetData.Cell(count, 5).Value = objB.Bt2CusFbBillUnitType;

                        //                            //        // Rate
                        //                            //        workSheetData.Cell(count, 6).Value = Cus_OutBundleDataRateSP;

                        //                            //        // Total charge in USD
                        //                            //        workSheetData.Cell(count, 7).Value = Math.Round(Convert.ToDecimal((usage * Cus_OutBundleDataRateSP)), 2, MidpointRounding.AwayFromZero);
                        //                            //        TotalChargeUSD += Math.Round(Convert.ToDecimal((usage * Cus_OutBundleDataRateSP)), 2, MidpointRounding.AwayFromZero);

                        //                            //        stdIPOutbundleTotal += Math.Round(Convert.ToDecimal((usage * Cus_OutBundleDataRateSP)), 2, MidpointRounding.AwayFromZero);

                        //                            //        //SMTS Cost Price
                        //                            //        SMTStotalCharge_CP += Math.Round(Convert.ToDecimal((usage * SMTSdataRate1mb_CP)), 2, MidpointRounding.AwayFromZero);
                        //                            //    }
                        //                            //    count++;

                        //                            //    if (stdIPOutbundleTotal > 0)
                        //                            //    {
                        //                            //        workSheetData.Cell(count, 3).Value = string.Format("SUM({0}/{1})", services, imsiID);
                        //                            //        workSheetData.Cell(count, 7).Value = 0.00;
                        //                            //    }
                        //                            //}

                        //                            #endregion
                        //                        }

                        #endregion

                        #region CallLog Voice
                        // Worksheet for CallLog Voice
                        //IXLWorksheet WorksheetVoice = xLWorkbook.Worksheets.Add("Voice");

                        //var wsVoice = WorksheetVoice.Worksheet.PageSetup;
                        //wsVoice.CenterHorizontally = false;
                        //wsVoice.CenterVertically = false;
                        //wsVoice.Margins.Top = 0;
                        //wsVoice.Margins.Bottom = 0.6;
                        //wsVoice.Margins.Left = 0.5;
                        //wsVoice.Margins.Right = 0;
                        //wsVoice.Margins.Footer = 0.5;
                        //wsVoice.Margins.Header = 0;
                        //wsVoice.AdjustTo(90);

                        //string ImagePath = MySession.InvoiceLogoPath;
                        //var img = WorksheetVoice.AddPicture(ImagePath)
                        //             .MoveTo(WorksheetVoice.Cell(1, 1)).Scale(1.05);

                        ////WorksheetVoice.Range("A2:B2").Merge();
                        //WorksheetVoice.Range("A5:G5").Merge();
                        //WorksheetVoice.Cell(5, 1).Value = "Call Data Details";
                        //WorksheetVoice.Cell(5, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //WorksheetVoice.Cell(5, 1).Style.Font.FontSize = 14;
                        //WorksheetVoice.Row(5).Style.Font.FontName = "Times New Roman";
                        //WorksheetVoice.Row(5).Style.Font.Bold = true;

                        //WorksheetVoice.Cell(6, 4).Value = "Customer";
                        //WorksheetVoice.Cell(6, 5).Value = customerName;
                        //WorksheetVoice.Row(6).Style.Font.Bold = true;

                        //WorksheetVoice.Cell(7, 4).Value = "LES";
                        //WorksheetVoice.Cell(7, 5).Value = LESName;
                        //WorksheetVoice.Row(7).Style.Font.Bold = true;

                        //WorksheetVoice.Cell(8, 1).Value = "Bill No:";
                        //WorksheetVoice.Cell(8, 2).Value = BillNo;
                        //WorksheetVoice.Row(8).Style.Font.Bold = true;

                        //WorksheetVoice.Cell(8, 4).Value = "Vessel";
                        //WorksheetVoice.Cell(8, 5).Value = VesselName;
                        //WorksheetVoice.Row(8).Style.Font.Bold = true;

                        //WorksheetVoice.Column(1).Width = 16;
                        //WorksheetVoice.Column(2).Width = 15;
                        //WorksheetVoice.Column(3).Width = 25;
                        //WorksheetVoice.Column(4).Width = 10;
                        //WorksheetVoice.Column(5).Width = 10;
                        //WorksheetVoice.Column(6).Width = 10;
                        //WorksheetVoice.Column(7).Width = 12;
                        //WorksheetVoice.Column(8).Width = 10;

                        //WorksheetVoice.Cell(6, 1).Value = "Invoice No";
                        //WorksheetVoice.Cell(6, 2).Value = InvoiceNo.ToString();
                        //WorksheetVoice.Cell(7, 1).Value = "Invoice Date";
                        //WorksheetVoice.Cell(7, 2).Value = DateTime.Now.Date;
                        //WorksheetVoice.Cell(6, 1).Style.Font.Bold = true;
                        //WorksheetVoice.Cell(6, 2).Style.Font.Bold = true;
                        //WorksheetVoice.Cell(7, 1).Style.Font.Bold = true;
                        //WorksheetVoice.Cell(7, 2).Style.Font.Bold = true;
                        //WorksheetVoice.Cell(7, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);

                        //WorksheetVoice.SheetView.Freeze(10, 0);
                        //WorksheetVoice.Row(10).Style.Font.Bold = true;


                        //WorksheetVoice.Cell(10, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //WorksheetVoice.Cell(10, 1).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                        //WorksheetVoice.Cell(10, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //WorksheetVoice.Cell(10, 2).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                        //WorksheetVoice.Cell(10, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //WorksheetVoice.Cell(10, 3).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                        //WorksheetVoice.Cell(10, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //WorksheetVoice.Cell(10, 4).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                        //WorksheetVoice.Cell(10, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //WorksheetVoice.Cell(10, 5).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                        //WorksheetVoice.Cell(10, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //WorksheetVoice.Cell(10, 6).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                        //WorksheetVoice.Cell(10, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        //WorksheetVoice.Cell(10, 7).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                        //WorksheetVoice.Cell(10, 1).Value = "Date/Time";
                        //WorksheetVoice.Cell(10, 2).Value = "LES";
                        //WorksheetVoice.Cell(10, 3).Value = "Orgin//Destination";
                        //WorksheetVoice.Cell(10, 4).Value = "Usage";
                        //WorksheetVoice.Cell(10, 5).Value = "Unit";
                        //WorksheetVoice.Cell(10, 6).Value = "Rate";
                        //WorksheetVoice.Cell(10, 7).Value = "Charges(USD)";


                        //WorksheetVoice.Cell(10, 1).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 1).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 1).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 1).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 2).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 2).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 2).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 2).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 3).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 3).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 3).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 3).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 4).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 4).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 4).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 4).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 5).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 5).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 5).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 5).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 6).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 6).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 6).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 6).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 7).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 7).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 7).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                        //WorksheetVoice.Cell(10, 7).Style.Border.LeftBorder = XLBorderStyleValues.Medium;

                        //j = 6;
                        //while (j < 780)
                        //{
                        //    WorksheetVoice.Row(j).Style.Font.FontSize = 8;
                        //    WorksheetVoice.Row(j).Style.Font.FontName = "Tahoma";
                        //    WorksheetVoice.Row(j).Style.Font.Bold = true;
                        //    j++;
                        //}

                        //int counts = 10, con = 10;

                        //ServiceDest = db.Bs2Sec.AsEnumerable().
                        //                           Where(t => t.fkCTFbID == id && (t.BsMtCommunicationType.ComTypes.Equals("Voice")))
                        //                           .Select(t => t.BsPtSPServiceDest.BsPtServiceTypes).Distinct().ToList();

                        //foreach (string services in ServiceDest)
                        //{
                        //    dataServType++;

                        //    WorksheetVoice.Cell(++counts, 1).Value = "Service Type :" + services + " " + "Id No:" + ImarSatID;

                        //    //Data Call Log InBundle Data
                        //    IQueryable<Bs2Sec> IQbs2sec = db.Bs2Sec
                        //                    .Where(t => t.BsPtSPServiceDest.BsPtServiceTypes.ToLower().Equals(services.ToLower()) && 
                        //                    (t.fkCTFbID == id) && (t.BsMtCommunicationType.ComTypes.Equals("Voice")));
                        //    lstInvoice = IQbs2sec.ToList();

                        //    if (lstInvoice != null)
                        //    {
                        //        decimal stdIPSubtotal = 0;
                        //        foreach (var obj in lstInvoice)
                        //        {
                        //            counts++;
                        //            cond++;

                        //            // Date or Time
                        //            if (obj.BsCtStartDate != null)
                        //                WorksheetVoice.Cell(counts, 1).Value = Convert.ToDateTime(obj.BsCtStartDate).ToShortDateString() + " / " + obj.BsCtStarTime;
                        //            else
                        //                WorksheetVoice.Cell(counts, 1).Value = obj.BsCtStartDate + " / " + obj.BsCtStarTime;

                        //            // Les
                        //            //workSheet.Cell(count, 2).Value = objB.Bt2CusFbTranDStartDate;
                        //            // Origin or Destination
                        //            WorksheetVoice.Cell(counts, 3).Style.NumberFormat.SetNumberFormatId((int)XLPredefinedFormat.Number.Integer);
                        //            WorksheetVoice.Cell(counts, 3).Value = obj.BsCtDestination;
                        //            // Usage
                        //            decimal usage = Math.Round(Convert.ToDecimal(obj.BsCtBillIncre), 2, MidpointRounding.AwayFromZero);
                        //            WorksheetVoice.Cell(counts, 4).Value = usage;
                        //            // Unit Type
                        //            WorksheetVoice.Cell(counts, 5).Value = obj.BsMtUnit.Units;

                        //            // Rate
                        //            decimal? sellerPrice = db.BsPtCusServiceDests.FirstOrDefault(t => t.BsPtCusServiceTypes.ToLower().Equals(services.ToLower())).BsPtCusServiceSellerPrice;
                        //            WorksheetVoice.Cell(counts, 6).Value = sellerPrice;

                        //            // Total charge in USD
                        //            WorksheetVoice.Cell(counts, 7).Value = Math.Round(Convert.ToDecimal((usage * sellerPrice)), 2, MidpointRounding.AwayFromZero);
                        //            TotalChargeUSD += Math.Round(Convert.ToDecimal((usage * sellerPrice)), 2, MidpointRounding.AwayFromZero);

                        //            stdIPSubtotal += Math.Round(Convert.ToDecimal((usage * sellerPrice)), 2, MidpointRounding.AwayFromZero);
                        //            //SMTS Cost Price
                        //            //SMTStotalCharge_CP += Math.Round(Convert.ToDecimal((usage * SMTSdataRate1mb_CP)), 2, MidpointRounding.AwayFromZero);
                        //        }
                        //        counts++;
                        //        if (stdIPSubtotal > 0)
                        //        {
                        //            WorksheetVoice.Cell(counts, 3).Value = string.Format("SUM({0}/{1})", services, ImarSatID);
                        //            WorksheetVoice.Cell(counts, 7).Value = stdIPSubtotal;
                        //        }
                        //    }
                        //}
                        #endregion

                        //WorksheetVoice.Cell(counts + 2, 6).Value = "Total(USD)";
                        //WorksheetVoice.Cell(counts + 2, 7).Value = TotalChargeUSD;
                        //WorksheetVoice.Cell(counts + 2, 7).Style.Font.Bold = true;
                        //WorksheetVoice.Cell(counts + 2, 6).Style.Font.Bold = true;

                        var statment = db.Statements.FirstOrDefault(t => t.IMSIID.Equals(imsiID));
                        bool isnotexist = false;
                        if (statment != null)
                        {
                            if (statment.InvoiceNo != InvoiceNo)
                                isnotexist = true;
                        }

                        #region Statement of Accounts

                        //if (statment == null || isnotexist)
                        //{
                        //    statment = new Statement();

                        //    if (string.IsNullOrEmpty(statment.InvoiceNo))
                        //    {
                        //        statment.InvoiceNo = InvoiceNo;
                        //    }

                        //    statment.IMSIID = imsiID;
                        //    statment.TotalSP = TotalChargeUSD;
                        //    statment.Vessel = VesselName;
                        //    statment.Customer = customerName;
                        //    statment.IvoiceDate = DateTime.Now.Date;
                        //    statment.Duration = db.BCtBs.FirstOrDefault(t => t.BCtFbIMSIId.Equals(imsiID) && t.BCtFbTranDServType.ToLower().Equals("subscription fee")).BCtFbTranDDest;

                        //    using (BillingSystemEntities db = new BillingSystemEntities())
                        //    {
                        //        decimal? totalAmountMRC = db.BCtBs.FirstOrDefault(t => t.BCtFbTranDServType != null &&
                        //                                           t.BCtFbRecurFee != null && t.BCtFbIMSIId.Equals(imsiID)).BCtFbRecurFee;

                        //        totalAmountMRC = Math.Round(Convert.ToDecimal(totalAmountMRC), 2, MidpointRounding.AwayFromZero);

                        //        decimal? totalAmountData = db.BCtBs.AsEnumerable().Where(t => t.BCtFbTranDServType != null &&
                        //                                                                 t.BCtFbIMSIId.Equals(imsiID))
                        //                                                          .GroupBy(t => t.BCtFbIMSIId.Equals(imsiID))
                        //                                                          .Select(t => t.Sum(s => s.BCtFbTranDUsage)).FirstOrDefault();
                        //        totalAmountData = Math.Round(Convert.ToDecimal(totalAmountData), 2, MidpointRounding.AwayFromZero);

                        //        decimal? totalUnitsData = db.BCtBs.AsEnumerable().Where(t => t.BCtFbTranDServType != null &&
                        //                                                                t.BCtFbIMSIId.Equals(imsiID))
                        //                                                         .GroupBy(t => t.BCtFbIMSIId.Equals(imsiID))
                        //                                                         .Select(t => t.Sum(s => s.BCtFbBilIncre)).FirstOrDefault();
                        //        totalUnitsData = Math.Round(Convert.ToDecimal(totalUnitsData), 2, MidpointRounding.AwayFromZero);

                        //        decimal? totalAmountVce = db.BCtBs.AsEnumerable().Where(t => t.BCtFbTranVServType != null &&
                        //                                                                     t.BCtFbIMSIId.Equals(imsiID))
                        //                                                         .GroupBy(t => t.BCtFbIMSIId.Equals(imsiID))
                        //                                                         .Select(t => t.Sum(s => s.BCtFbTranVUsage)).FirstOrDefault();
                        //        totalAmountVce = Math.Round(Convert.ToDecimal(totalAmountVce), 2, MidpointRounding.AwayFromZero);

                        //        //statment.TotalCP = totalAmountMRC + totalAmountData + totalAmountVce;
                        //        statment.TotalCP = totalAmountMRC + totalAmountVce;
                        //    }


                        //    if (statment.TotalCP > 0 && statment.TotalSP > 0)
                        //    {
                        //        decimal CrossProfit = Convert.ToDecimal(statment.TotalSP - statment.TotalCP);
                        //        decimal Margin = (CrossProfit / Convert.ToDecimal(statment.TotalSP) * 100);
                        //        statment.Margin = Margin > 0 ? Convert.ToInt32(Margin) : (int)Math.Abs(Margin);
                        //    }

                        //    statment.SMTSCost = SMTStotalCharge_CP;

                        //    db.Statements.Add(statment);
                        //    try
                        //    {
                        //        db.SaveChanges();
                        //    }
                        //    catch { }
                        //}
                        #endregion

                        xLWorkbook.SaveAs(SpreadsheetStream);
                        SpreadsheetStream.Position = 0;

                        return new Tuple<Stream, string>(SpreadsheetStream, CallLogName);
                    }
                }
                catch (Exception ex)
                {
                    return new Tuple<Stream, string>(SpreadsheetStream, string.Empty);
                }
                return new Tuple<Stream, string>(SpreadsheetStream, string.Empty);
            }

            return new Tuple<Stream, string>(SpreadsheetStream, string.Empty);
        }

        public decimal? CreateWorkSheet(IXLWorksheet workSheet, string InvoiceNo, string customerName, string LESName, string BillNo, string VesselName, List<Dictionary<string, List<Dictionary<string, dynamic>>>> dictCallLog, bool isLastSheet, decimal? totalCharged, string logopath)
        {
            #region PageSetup
            var ws = workSheet.Worksheet.PageSetup;
            ws.CenterHorizontally = false;
            ws.CenterVertically = false;
            ws.Margins.Top = 0;
            ws.Margins.Bottom = 0.6;
            ws.Margins.Left = 0.5;
            ws.Margins.Right = 0;
            ws.Margins.Footer = 0.5;
            ws.Margins.Header = 0;
            ws.AdjustTo(90);
            #endregion

            #region Column Styles

            workSheet.Column(1).Width = 16;
            workSheet.Column(2).Width = 15;
            workSheet.Column(3).Width = 25;
            workSheet.Column(4).Width = 10;
            workSheet.Column(5).Width = 10;
            workSheet.Column(6).Width = 10;
            workSheet.Column(7).Width = 10;
            workSheet.Column(8).Width = 10;
            workSheet.Cell(2, 1).Style.Font.Bold = true;
            workSheet.Cell(4, 1).Style.Font.Bold = true;
            workSheet.Cell(11, 1).Style.Font.Bold = true;
            //workSheet.Cell(12, 1).Style.Font.Bold = true;
            //workSheet.Cell(12, 2).Style.Font.Bold = true;
            workSheet.Cell(3, 6).Style.Font.Bold = true;
            workSheet.Row(6).Style.Font.Bold = true;
            //workSheet.Row(12).Style.Font.Bold = true;

            int j = 6;
            while (j < 78)
            {
                workSheet.Row(j).Style.Font.FontSize = 8;
                workSheet.Row(j).Style.Font.FontName = "Tahoma";
                workSheet.Row(j).Style.Font.Bold = true;
                j++;
            }

            //string Imagepath = MySession.InvoiceLogoPath;

            workSheet.Range("A1:E2").Merge();
            var image = workSheet.AddPicture(logopath)
                      .MoveTo(workSheet.Cell(1, 1)).Scale(1.05);

            //workSheet.Cell(2, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            //workSheet.Cell(2, 1).Value = "Pte Ltd";
            //workSheet.Cell(2, 1).Style.Font.Bold = true;
            //workSheet.Cell(2, 1).Style.Font.FontSize = 20;
            //                        workSheetData.Range("A2:B2").Merge();
            // workSheet.Cell(2, 1).Value = "SMTS Pte Ltd";
            //workSheet.Cell(2, 1).Style.Font.FontSize = 20;
            #endregion

            #region Header and THeader
            workSheet.Range("A5:G5").Merge();
            workSheet.Cell(5, 1).Value = "Call Data Details";
            workSheet.Cell(5, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            workSheet.Cell(5, 1).Style.Font.FontSize = 14;
            workSheet.Row(5).Style.Font.FontName = "Times New Roman";
            workSheet.Row(5).Style.Font.Bold = true;

            workSheet.Row(12).Style.Font.Bold = false;
            workSheet.Cell(6, 1).Value = "Invoice No";
            workSheet.Cell(6, 2).Value = InvoiceNo;

            workSheet.Cell(6, 5).Value = "Customer";
            workSheet.Cell(6, 6).Value = customerName;
            workSheet.Row(6).Style.Font.Bold = true;
            workSheet.Row(6).Style.Font.FontName = "Tahoma";

            workSheet.Cell(7, 1).Value = "Invoice Date";
            workSheet.Cell(7, 2).Value = DateTime.Now.Date;
            workSheet.Cell(7, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
            workSheet.Row(7).Style.Font.FontName = "Tahoma";

            workSheet.Cell(7, 5).Value = "LES";
            workSheet.Cell(7, 6).Value = LESName;
            workSheet.Row(7).Style.Font.Bold = true;

            workSheet.Cell(8, 1).Value = "Bill No:";
            workSheet.Cell(8, 2).Value = BillNo;
            workSheet.Row(8).Style.Font.Bold = true;

            workSheet.Cell(8, 5).Value = "Vessel";
            workSheet.Cell(8, 6).Value = VesselName;
            workSheet.Row(8).Style.Font.Bold = true;

            workSheet.Cell(10, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            workSheet.Cell(10, 1).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
            workSheet.Cell(10, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            workSheet.Cell(10, 2).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
            workSheet.Cell(10, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            workSheet.Cell(10, 3).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
            workSheet.Cell(10, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            workSheet.Cell(10, 4).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
            workSheet.Cell(10, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            workSheet.Cell(10, 5).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
            workSheet.Cell(10, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            workSheet.Cell(10, 6).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
            workSheet.Cell(10, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            workSheet.Cell(10, 7).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
            workSheet.Cell(10, 1).Value = "Date/Time";
            workSheet.Cell(10, 2).Value = "LES";
            workSheet.Cell(10, 3).Value = "Orgin//Destination";
            workSheet.Cell(10, 4).Value = "Usage";
            workSheet.Cell(10, 5).Value = "Unit";
            workSheet.Cell(10, 6).Value = "Rate";
            workSheet.Cell(10, 7).Value = "Charges(USD)";

            //workSheet.Cell(25, 6).Value = "Total(USD)";

            workSheet.Row(10).Style.Font.Bold = true;

            workSheet.Cell(10, 1).Style.Border.TopBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 1).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 1).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 1).Style.Border.RightBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 2).Style.Border.TopBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 2).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 2).Style.Border.RightBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 2).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 3).Style.Border.TopBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 3).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 3).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 3).Style.Border.RightBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 4).Style.Border.TopBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 4).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 4).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 4).Style.Border.RightBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 5).Style.Border.TopBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 5).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 5).Style.Border.RightBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 5).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 6).Style.Border.TopBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 6).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 6).Style.Border.RightBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 6).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 7).Style.Border.TopBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 7).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 7).Style.Border.RightBorder = XLBorderStyleValues.Medium;
            workSheet.Cell(10, 7).Style.Border.LeftBorder = XLBorderStyleValues.Medium;

            workSheet.Row(10).Height = 16.50;
            workSheet.Row(12).Style.Font.FontSize = 8;
            workSheet.Row(12).Style.Font.FontName = "Tahoma";
            workSheet.Row(12).Style.Font.Bold = true;

            workSheet.Row(25).Style.Font.Bold = true;
            workSheet.SheetView.Freeze(10, 0);

            #endregion

            int count = 11, cond = 11;

            int dataServType = 0, VceServType = 0;
            decimal? TotalChargeUSD = totalCharged;
            decimal? SMTStotalCharge_CP = 0;
            decimal? stdIPSubtotal = 0;

            foreach (Dictionary<string, List<Dictionary<string, dynamic>>> dictReccord in dictCallLog)
            {
                string serviceType = dictReccord.FirstOrDefault(t => t.Key != null).Key;
                workSheet.Cell(count, 1).Value = serviceType;
                stdIPSubtotal = 0;
                foreach (List<Dictionary<string, dynamic>> lstRecords in dictReccord.Values)
                {
                    foreach (Dictionary<string, dynamic> calllog in lstRecords)
                    {
                        count++;

                        // Date or Time
                        if (calllog.ContainsKey("datetime") && !string.IsNullOrEmpty(calllog["datetime"]))
                            workSheet.Cell(count, 1).Value = calllog["datetime"];
                        else
                            workSheet.Cell(count, 1).Value = calllog["datetime"];

                        // Les
                        if (calllog.ContainsKey("les") && !string.IsNullOrEmpty(calllog["les"]))
                            workSheet.Cell(count, 2).Value = calllog["les"];
                        else
                            workSheet.Cell(count, 2).Value = string.Empty;

                        // Origin or Destination
                        workSheet.Cell(count, 3).Style.NumberFormat.SetNumberFormatId((int)XLPredefinedFormat.Number.Integer);
                        if (calllog.ContainsKey("origindestination") && !string.IsNullOrEmpty(calllog["origindestination"]))
                            workSheet.Cell(count, 3).Value = calllog["origindestination"];
                        else
                            workSheet.Cell(count, 3).Value = string.Empty;

                        // Usage
                        decimal usage = 0;
                        if (calllog.ContainsKey("usage") && calllog["usage"] != null)
                        {
                            usage = Math.Round(Convert.ToDecimal(calllog["usage"]), 2, MidpointRounding.AwayFromZero);
                            workSheet.Cell(count, 4).Value = usage;
                        }
                        else
                            workSheet.Cell(count, 4).Value = string.Empty;

                        // Unit Type
                        if (calllog.ContainsKey("unittype") && calllog["unittype"] != null)
                            workSheet.Cell(count, 5).Value = calllog["unittype"];
                        else
                            workSheet.Cell(count, 5).Value = string.Empty;

                        // Rate
                        decimal? sellerPrice = 0;
                        if (calllog.ContainsKey("rate") && calllog["rate"] != null)
                            workSheet.Cell(count, 6).Value = calllog["rate"];
                        else
                            workSheet.Cell(count, 6).Value = sellerPrice;

                        // Total charge in USD
                        decimal? total = 0;
                        if (calllog.ContainsKey("charges(usd)") && calllog["charges(usd)"] != null)
                        {
                            total = calllog["charges(usd)"];
                            workSheet.Cell(count, 7).Value = total;
                        }
                        else
                            workSheet.Cell(count, 7).Value = total;

                        //workSheet.Cell(count, 7).Value = Math.Round(Convert.ToDecimal((usage * sellerPrice)), 2, MidpointRounding.AwayFromZero);

                        TotalChargeUSD += total;
                        stdIPSubtotal += total;
                    }
                }

                count++;
                if (stdIPSubtotal > 0)
                {
                    string service = string.Empty, ImarSatID = string.Empty;
                    string[] set = serviceType.Split(',');
                    if (set.Length > 0)
                    {
                        string[] nan = set[0].Split(':');
                        if (nan.Length > 1)
                            service = nan[1];
                    }
                    if (set.Length > 1)
                    {
                        string[] nan = set[1].Split(':');
                        if (nan.Length > 1)
                            ImarSatID = nan[1];
                    }

                    if (!service.ToLower().Contains("subscription"))
                    {
                        workSheet.Cell(count, 3).Value = string.Format("SUM({0}/{1})", service, ImarSatID);
                        workSheet.Cell(count, 7).Value = stdIPSubtotal;
                        count++;
                    }
                }

            }

            if (isLastSheet)
            {
                workSheet.Cell(count + 2, 6).Value = "Total(USD)";
                workSheet.Cell(count + 2, 7).Value = TotalChargeUSD;
                workSheet.Cell(count + 2, 7).Style.Font.Bold = true;
                workSheet.Cell(count + 2, 6).Style.Font.Bold = true;
            }

            return TotalChargeUSD;
        }

        public List<Tuple<bool, string>> Move2Invoice(int primaryID)
        {
            bool isSucess = false;
            List<Tuple<bool, string>> msg = new List<Tuple<bool, string>>();
            try
            {
                using (BillingSystemEntities context = new BillingSystemEntities())
                {
                    var rejMtLes = context.BS1ALessT.FirstOrDefault(t => t.BS1ALesID == primaryID);
                    if (rejMtLes != null)
                    {
                        string ServiceProvider = context.BsMtServiceProviders.FirstOrDefault(t => t.spID == rejMtLes.fkSPID).ServiceProvider;
                        if (!string.IsNullOrEmpty(ServiceProvider))
                        {
                            switch (ServiceProvider)
                            {
                                case "IMARSAT":
                                    break;
                                case "NSSL GLOBAL":
                                    isSucess = MoveRejection_Invoice(context, rejMtLes);
                                    msg.Add(new Tuple<bool, string>(isSucess, ServiceProvider));
                                    break;
                                case "OteSat":
                                    helper_OteSat helper_Ote = new helper_OteSat();
                                    isSucess = helper_Ote.MoveRejection_Invoice(context, rejMtLes);
                                    msg.Add(new Tuple<bool, string>(isSucess, ServiceProvider));
                                    break;
                                case "NI":
                                    break;
                                case "EvoSat":
                                    break;
                            }
                        }
                    }
                    else
                        msg.Add(new Tuple<bool, string>(false, "Does not contain in LES"));
                }
            }
            catch (Exception error)
            {
                msg.Add(new Tuple<bool, string>(isSucess, error.Message.ToString()));
                return msg;
            }
            return msg;
        }

        public bool MoveRejection_Invoice(BillingSystemEntities context, BS1ALessT rejMtLes)
        {
            //bool isMovedtoInvoice = false;
            try
            {
                decimal? SMTS_TotalCost = 0;
                decimal? Les_TotalCostCP = 0;
                decimal? Cus_TotalCostSP = 0;

                try
                {
                    var secLes = rejMtLes;
                    if (secLes != null)
                    {
                        int CUS_Id = 0;
                        int CUS_ContractID = 0;
                        int CUS_RegsID = 0;

                        decimal? Cus_DataPlan = 0;
                        decimal? Cus_OutbundleRate = 0;
                        decimal? CUS_RecurFee = 0;
                        decimal? CUS_DataRateper1MB = 0;
                        decimal? CUS_VoiceFBBCellular = 0;
                        decimal? CUS_VoiceFBBPSTN = 0;
                        decimal? CUS_VoiceFBB = 0;
                        decimal? CUS_PrepaidServer = 0;
                        decimal? CUS_SMS = 0;

                        int? LES_ContractID = 0;
                        int LES_RegsID = 0;

                        decimal? LES_DataPlan = 0;
                        decimal? LES_OutbundleRate = 0;
                        decimal? LES_RecurFee = 0;
                        decimal? LES_DataRateper1MB = 0;
                        decimal? LES_VoiceFBBCellular = 0;
                        decimal? LES_VoiceFBBPSTN = 0;
                        decimal? LES_VoiceFBB = 0;
                        decimal? LES_PrepaidServer = 0;
                        decimal? LES_SMS = 0;

                        bool is_CUSGBPlan = false;
                        bool is_LESGBPlan = false;
                        string duration = string.Empty;

                        var cusRegs = context.BsPtCusRegs_.FirstOrDefault(t => t.fkSimID == secLes.fkSimID);
                        if (cusRegs != null)
                        {
                            CUS_Id = Convert.ToInt32(cusRegs.fkCusID);
                            CUS_ContractID = Convert.ToInt32(cusRegs.fkCuscID);
                            CUS_RegsID = Convert.ToInt32(cusRegs.Cus_RegID);

                            CUS_RecurFee = cusRegs.BPtCusRgnFbRecurFee;

                            Cus_DataPlan = Convert.ToDecimal(cusRegs.BPtCusRgnFBDataPlanIn);
                            Cus_OutbundleRate = cusRegs.BPtCusRgnFbOutBundleDataRate;

                            CUS_DataRateper1MB = cusRegs.BPtCusRgnFbInbundleDataRate;


                            string unit = cusRegs.BsMtUnit.Units;

                            if (unit.Contains("GB"))
                                is_CUSGBPlan = true;
                            else
                                is_CUSGBPlan = false;
                        }

                        List<BsPtCusServiceDest> lstSP = context.BsPtCusServiceDests.Where(t => t.fkSpID == LesID).ToList();
                        try
                        {
                            CUS_DataRateper1MB = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Background IP")).BsPtCusServiceSellerPrice;
                        }
                        catch { CUS_DataRateper1MB = 0; }
                        try
                        {
                            CUS_VoiceFBBCellular = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Basic voice -FBB/Cellular")).BsPtCusServiceSellerPrice;
                        }
                        catch { CUS_VoiceFBBCellular = 0; }
                        try
                        {
                            CUS_VoiceFBBPSTN = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Basic voice -FBB/PSTN")).BsPtCusServiceSellerPrice;
                        }
                        catch { CUS_VoiceFBBPSTN = 0; }
                        try
                        {
                            CUS_VoiceFBB = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Basic voice -FBB/FBB ")).BsPtCusServiceSellerPrice;
                        }
                        catch { CUS_VoiceFBB = 0; }
                        try
                        {
                            CUS_PrepaidServer = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Prepaid Server")).BsPtCusServiceSellerPrice;
                        }
                        catch { CUS_PrepaidServer = 0; }
                        try
                        {
                            CUS_SMS = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                           t.BsPtCusServiceTypes.Equals("SMS")).BsPtCusServiceSellerPrice;
                        }
                        catch { CUS_SMS = 0; }


                        List<BsPtSPServiceDest> lstCP = context.BsPtSPServiceDests.Where(t => t.fkSpID == LesID).ToList();
                        try
                        {
                            var Les_Reg = context.BsPtSPLesRegs.FirstOrDefault(t => t.fkSPID == LesID && t.fkSimID == secLes.fkSimID);
                            LES_RegsID = Les_Reg.SPLesRegID;
                            LES_ContractID = Les_Reg.fkspcID;
                            LES_RecurFee = Les_Reg.BPtLesRgnFbRecurFee;
                            LES_DataPlan = Convert.ToDecimal(Les_Reg.BPtLesRgnFBDataPlanIn);

                            LES_DataRateper1MB = Les_Reg.BPtLesRgnFbInbundleDataRate;
                            LES_OutbundleRate = Les_Reg.BPtLesRgnFbOutBundleDataRate;

                            string unit = Les_Reg.BsMtUnit.Units;
                            if (unit.Contains("GB"))
                                is_LESGBPlan = true;
                            else
                                is_LESGBPlan = false;

                            Les_TotalCostCP += LES_RecurFee;

                            /*LES_RecurFee = context.BsPtSPLesRegs.FirstOrDefault(t => t.fkSPID == LesID && t.fkSimID == secLes.fkSimID).BPtLesRgnFbRecurFee;
                            Les_TotalCostCP += LES_RecurFee;*/
                        }
                        catch { LES_RecurFee = 0; }
                        try
                        {
                            LES_DataRateper1MB = lstCP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegsID && t.BsPtServiceTypes.Equals("Background IP")).BsPtServiceCostPrice;
                        }
                        catch { LES_DataRateper1MB = 0; }
                        try
                        {
                            LES_VoiceFBBCellular = lstCP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegsID && t.BsPtServiceTypes.Equals("Basic voice -FBB/Cellular")).BsPtServiceCostPrice;
                        }
                        catch { LES_VoiceFBBCellular = 0; }
                        try
                        {
                            LES_VoiceFBBPSTN = lstCP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegsID && t.BsPtServiceTypes.Equals("Basic voice -FBB/PSTN")).BsPtServiceCostPrice;
                        }
                        catch { LES_VoiceFBBPSTN = 0; }
                        try
                        {
                            LES_VoiceFBB = lstCP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegsID && t.BsPtServiceTypes.Equals("Basic voice -FBB/FBB ")).BsPtServiceCostPrice;
                        }
                        catch { LES_VoiceFBB = 0; }
                        try
                        {
                            LES_PrepaidServer = lstCP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegsID && t.BsPtServiceTypes.Equals("Prepaid Server")).BsPtServiceCostPrice;
                        }
                        catch { LES_PrepaidServer = 0; }
                        try
                        {
                            LES_SMS = lstCP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegsID &&
                                                           t.BsPtServiceTypes.Equals("SMS")).BsPtServiceCostPrice;
                        }
                        catch { LES_SMS = 0; }

                        string InvoiceNo = string.Empty;

                        try
                        {
                            InvoiceNo = context.BS2MtCusT.Max(t => t.BSMtCusInvNo);

                            if (!string.IsNullOrEmpty(InvoiceNo))
                            {
                                if (InvoiceNo.Contains("SM"))
                                {
                                    string tempInv = InvoiceNo.Replace("SM", string.Empty);
                                    if (int.TryParse(tempInv, out int result))
                                    {
                                        InvoiceNo = BillingHelpers.GenInvoice(false, result);
                                    }
                                }
                            }
                            else
                                InvoiceNo = BillingHelpers.GenInvoice(false, 0);
                        }
                        catch (Exception error)
                        {
                            //ErrorMsg.AppendFormat("MSI:{0} Error Msg: {1} Stack Trace: {2}. <br />", Imsi, error.Message.ToString(), error.StackTrace);
                        }

                        if (!string.IsNullOrEmpty(InvoiceNo))
                        {
                            BS2MtCusT SecMtCust = new BS2MtCusT();

                            SecMtCust.BSMtCusInvNo = InvoiceNo;
                            SecMtCust.fkSPID = secLes.fkSPID;
                            SecMtCust.fkCusID = CUS_Id;
                            SecMtCust.fkCusConID = CUS_ContractID;
                            SecMtCust.fkCusRegID = CUS_RegsID;
                            SecMtCust.fkspcID = secLes.fkspcID;
                            SecMtCust.fkSimID = secLes.fkSimID;
                            SecMtCust.fkLesRefID = secLes.fkLesRefID;
                            SecMtCust.fkSatTypeID = secLes.fkSatTypeID;
                            SecMtCust.fkEquipTypeID = secLes.fkEquipTypeID;
                            SecMtCust.BSMtCusLesCode = secLes.BS1ALesLesCode;
                            SecMtCust.BSMtCusCdrCode = secLes.BS1ALesCdrCode;
                            SecMtCust.BSMtCusSatType = null;
                            SecMtCust.BSMtCusServDest = secLes.BS1ALesServDest;
                            duration = secLes.BS1ALesServDest;
                            SecMtCust.BSMtCusServType = secLes.BS1ALesServType;
                            SecMtCust.BSMtCusTransUsage = secLes.BS1ALesTransUsage;
                            SecMtCust.BSMtCusUOM = secLes.BS1ALesUOM;
                            SecMtCust.BSMtCusRecurFee = CUS_RecurFee;
                            Cus_TotalCostSP += SecMtCust.BSMtCusRecurFee;
                            SecMtCust.BSMtCusRecurFeeType = secLes.BS1ALesRecurFeeType;
                            SecMtCust.BSMtCusCDRRef = secLes.BS1ALesCDRRef;
                            SecMtCust.BSMtCusBillNo = secLes.BS1ALesBillNo;
                            SecMtCust.BSMtCusBillPeriod = secLes.BS1ALesBillPeriod;
                            SecMtCust.BSMtCusBillDate = secLes.BS1ALesBillDate;

                            context.BS2MtCusT.Add(SecMtCust);
                            context.SaveChanges();

                            int id = SecMtCust.BSMtCusTID;

                            int? LES_BackgrndIPServiceIP = 0;
                            int? LES_V2FbbCellServiceID = 0;
                            int? LES_V2PSTNServiceID = 0;
                            int? LES_V2FBBFBBServiceID = 0;
                            int? LES_PrepaidServiceID = 0;
                            int? LES_SMSServiceID = 0;

                            int? CUS_BackgrndIPServiceIP = 0;
                            int? CUS_V2FbbCellServiceID = 0;
                            int? CUS_V2PSTNServiceID = 0;
                            int? CUS_V2FBBFBBServiceID = 0;
                            int? CUS_PrepaidServiceID = 0;
                            int? CUS_SMSServiceID = 0;

                            BInvLstReport InvReport = new BInvLstReport();
                            List<BInvLstReport> lstInvreport = context.BInvLstReports.Where(t => t.BInvRprtLes1ID == secLes.BS1ALesID).ToList();

                            try
                            {
                                InvReport = lstInvreport.FirstOrDefault(t => t.BInvRprtLes1ID == secLes.BS1ALesID && t.BInvRprtSerType.ToLower().Equals(SecMtCust.BSMtCusServType.ToLower()));
                                if (InvReport != null)
                                {
                                    InvReport.fkBCus2ID = SecMtCust.BSMtCusTID;
                                    InvReport.BInvRprtSerType = SecMtCust.BSMtCusServType;
                                    InvReport.BInvRprtTotBil = SecMtCust.BSMtCusRecurFee;
                                }
                            }
                            catch { }

                            List<BsPtCusServiceDest> lstCusServDest = context.BsPtCusServiceDests.Where(t => t.BPtCusServfkCusRegID == CUS_RegsID).ToList();
                            List<BsPtSPServiceDest> lstSpServDest = context.BsPtSPServiceDests.Where(t => t.BPtServTfkLesRegID == LES_RegsID).ToList();


                            List<Bs1BLesT> lstRecord = context.Bs1BLesT.Where(t => t.fkBs1BLesID == secLes.BS1ALesID).ToList();

                            if (lstCusServDest.Count > 0)
                            {
                                try
                                {
                                    CUS_BackgrndIPServiceIP = lstCusServDest.FirstOrDefault(t => t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                     t.BsPtCusServiceTypes.Equals("Background IP")).CusServDID;
                                }
                                catch { CUS_BackgrndIPServiceIP = 0; }

                                try
                                {
                                    LES_BackgrndIPServiceIP = lstSpServDest.FirstOrDefault(t => t.BPtServTfkLesRegID == LES_RegsID &&
                                                                                                    t.BsPtServiceTypes.Equals("Background IP")).SPServDID;


                                }
                                catch
                                {
                                    LES_BackgrndIPServiceIP = 0;
                                }

                            }

                            List<Bs1BLesT> lstDateCT = lstRecord.AsEnumerable()
                                                 .Where(t => t.fkCommType == 1 && t.fkSPServTypeID == LES_BackgrndIPServiceIP &&
                                                       (t.fkBs1BLesID == secLes.BS1ALesID)).ToList();
                            Bs2Sec bs2Sec;

                            if (is_CUSGBPlan)
                                Cus_DataPlan = Cus_DataPlan * 1024;

                            if (is_LESGBPlan)
                                LES_DataPlan = LES_DataPlan * 1024;

                            decimal? outbundleData = 0;
                            decimal? Cus_InvRptTotalBill = 0;

                            foreach (var records in lstDateCT)
                            {
                                bs2Sec = new Bs2Sec();
                                bs2Sec.fkCTFbID = id;
                                bs2Sec.BsCtStartDate = records.Bs1BLesStartDate;
                                bs2Sec.BsCtStarTime = records.Bs1BLesStarTime;
                                bs2Sec.BsCtOrigion = records.Bs1BLesOrigion;
                                bs2Sec.BsCtDestination = records.Bs1BLesDestination;
                                bs2Sec.fkSPServTypeID = Convert.ToInt32(CUS_BackgrndIPServiceIP);

                                //long dataconvertToMP = Convert.ToInt64(records.Bs1LesBillIncre);
                                bs2Sec.BsCtTransUsage = records.Bs1BLesTransUsage;
                                bs2Sec.fkUOMID = context.BsMtUnits.FirstOrDefault(t => t.Units.Equals("MB")).UID;
                                //bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtBillIncre * CUS_DataRateper1MB), 2, MidpointRounding.AwayFromZero);
                                //bs2Sec.fkCommType = 1;

                                /*Cus_TotalCostSP += bs2Sec.BsCtTotalUsage;
                                Les_TotalCostCP += Math.Round(Convert.ToDecimal(bs2Sec.BsCtBillIncre * LES_DataRateper1MB), 2, MidpointRounding.AwayFromZero);
                                context.Bs2Sec.Add(bs2Sec);
                                context.Bs1LesT.Remove(records);*/

                                outbundleData += bs2Sec.BsCtBillIncre;

                                if (Cus_DataPlan <= outbundleData)
                                {
                                    bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal((bs2Sec.BsCtTransUsage / 8388608) * Cus_OutbundleRate), 2, MidpointRounding.AwayFromZero);
                                    Cus_InvRptTotalBill += bs2Sec.BsCtTotalUsage;
                                }
                                else
                                {
                                    bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal((bs2Sec.BsCtTransUsage / 8388608) * 0), 2, MidpointRounding.AwayFromZero);
                                    Cus_InvRptTotalBill += bs2Sec.BsCtTotalUsage;
                                }

                                bs2Sec.fkCommType = 1;

                                Cus_TotalCostSP += bs2Sec.BsCtTotalUsage;

                                if (LES_DataPlan <= outbundleData)
                                    Les_TotalCostCP += Math.Round(Convert.ToDecimal((bs2Sec.BsCtBillIncre / 8388608) * LES_OutbundleRate), 2, MidpointRounding.AwayFromZero);
                                else
                                    Les_TotalCostCP += Math.Round(Convert.ToDecimal(bs2Sec.BsCtBillIncre * 0), 2, MidpointRounding.AwayFromZero);

                                context.Bs2Sec.Add(bs2Sec);
                                // context.Bs1LesT.Remove(records);
                            }

                            try
                            {
                                if (lstDateCT.Count() > 0)
                                {
                                    InvReport = new BInvLstReport();
                                    InvReport = lstInvreport.FirstOrDefault(t => t.BInvRprtSerType.Equals("Background IP"));
                                    if (InvReport != null)
                                    {
                                        InvReport.fkBCus2ID = SecMtCust.BSMtCusTID;
                                        InvReport.BInvRprtLes1ID = null;
                                        InvReport.BInvRprtTotBil = Cus_InvRptTotalBill;
                                    }
                                }
                            }
                            catch { }

                            if (lstCusServDest.Count > 0)
                            {
                                try
                                {
                                    CUS_V2FbbCellServiceID = lstCusServDest.FirstOrDefault(t => t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                         t.BsPtCusServiceTypes.Equals("Basic voice -FBB/Cellular")).CusServDID;
                                }
                                catch { CUS_V2FbbCellServiceID = 0; }
                                try
                                {
                                    LES_V2FbbCellServiceID = lstSpServDest.FirstOrDefault(t => t.BPtServTfkLesRegID == LES_RegsID &&
                                                                                        t.BsPtServiceTypes.Equals("Basic voice -FBB/Cellular")).SPServDID;
                                }
                                catch
                                {
                                    LES_V2FbbCellServiceID = 0;
                                }
                            }

                            List<Bs1BLesT> lstVceFbbCell = lstRecord.AsEnumerable()
                                                 .Where(t => t.fkCommType == 2 && t.fkSPServTypeID == LES_V2FbbCellServiceID &&
                                                 /*t.BsPtSPServiceDest.BsPtServiceTypes.Equals("basic voice -fbb/cellular") &&*/
                                                 (t.fkBs1BLesID == secLes.BS1ALesID)).ToList();

                            decimal? Cus_InvRptTotalBill_V2FbbCell = 0;

                            foreach (var records in lstVceFbbCell)
                            {
                                bs2Sec = new Bs2Sec();
                                bs2Sec.fkCTFbID = id;
                                bs2Sec.BsCtStartDate = records.Bs1BLesStartDate;
                                bs2Sec.BsCtStarTime = records.Bs1BLesStarTime;
                                bs2Sec.BsCtOrigion = records.Bs1BLesOrigion;
                                bs2Sec.BsCtDestination = records.Bs1BLesDestination;
                                bs2Sec.fkSPServTypeID = Convert.ToInt32(CUS_V2FbbCellServiceID);

                                //long SecconvertToMin = Convert.ToInt64(records.BsCtBillIncre);
                                bs2Sec.BsCtTransUsage = records.Bs1BLesTransUsage;// (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkUOMID = context.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_VoiceFBBCellular), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkCommType = 2;

                                Cus_InvRptTotalBill_V2FbbCell += bs2Sec.BsCtTotalUsage;
                                Cus_TotalCostSP += bs2Sec.BsCtTotalUsage;
                                Les_TotalCostCP += Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * LES_VoiceFBBCellular), 2, MidpointRounding.AwayFromZero);

                                context.Bs2Sec.Add(bs2Sec);

                                //   context.Bs1LesT.Remove(records);
                            }

                            try
                            {
                                if (lstVceFbbCell.Count() > 0)
                                {
                                    InvReport = new BInvLstReport();
                                    InvReport = lstInvreport.FirstOrDefault(t => t.BInvRprtSerType.Equals("Basic voice -FBB/Cellular"));
                                    if (InvReport != null)
                                    {
                                        InvReport.fkBCus2ID = SecMtCust.BSMtCusTID;
                                        InvReport.BInvRprtLes1ID = null;
                                        InvReport.BInvRprtTotBil = Cus_InvRptTotalBill_V2FbbCell;
                                    }
                                }
                            }
                            catch { }

                            if (lstCusServDest.Count > 0)
                            {
                                try
                                {
                                    CUS_V2PSTNServiceID = lstCusServDest.FirstOrDefault(t => t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                        t.BsPtCusServiceTypes.Equals("Basic voice -FBB/PSTN")).CusServDID;
                                }
                                catch { CUS_V2PSTNServiceID = 0; }

                                try
                                {
                                    LES_V2PSTNServiceID = lstSpServDest.FirstOrDefault(t => t.BPtServTfkLesRegID == LES_RegsID &&
                                                                                                    t.BsPtServiceTypes.Equals("Basic voice -FBB/PSTN")).SPServDID;
                                }
                                catch
                                {
                                    LES_V2PSTNServiceID = 0;
                                }
                            }
                            List<Bs1BLesT> lstVceFbbPSTN = lstRecord.AsEnumerable()
                                                 .Where(t => t.fkCommType == 2 && t.fkSPServTypeID == LES_V2PSTNServiceID &&
                                                 (t.fkBs1BLesID == secLes.BS1ALesID)).ToList();

                            decimal? Cus_InvRptTotalBill_V2PSTN = 0;
                            foreach (var records in lstVceFbbPSTN)
                            {
                                bs2Sec = new Bs2Sec();
                                bs2Sec.fkCTFbID = id;
                                bs2Sec.BsCtStartDate = records.Bs1BLesStartDate;
                                bs2Sec.BsCtStarTime = records.Bs1BLesStarTime;
                                bs2Sec.BsCtOrigion = records.Bs1BLesOrigion;
                                bs2Sec.BsCtDestination = records.Bs1BLesDestination;
                                bs2Sec.fkSPServTypeID = Convert.ToInt32(CUS_V2PSTNServiceID);

                                //long SecconvertToMin = Convert.ToInt64(records.Bs1LesBillIncre);
                                bs2Sec.BsCtTransUsage = records.Bs1BLesTransUsage; //(decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkUOMID = context.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_VoiceFBBPSTN), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkCommType = 2;

                                Cus_TotalCostSP += bs2Sec.BsCtTotalUsage;
                                Cus_InvRptTotalBill_V2PSTN += bs2Sec.BsCtTotalUsage;
                                Les_TotalCostCP += Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * LES_VoiceFBBPSTN), 2, MidpointRounding.AwayFromZero);

                                context.Bs2Sec.Add(bs2Sec);

                                //context.Bs1LesT.Remove(records);
                            }

                            try
                            {
                                if (lstVceFbbPSTN.Count() > 0)
                                {
                                    InvReport = new BInvLstReport();
                                    InvReport = lstInvreport.FirstOrDefault(t => t.BInvRprtSerType.ToLower().Equals("Basic voice -FBB/PSTN"));
                                    if (InvReport != null)
                                    {
                                        InvReport.fkBCus2ID = SecMtCust.BSMtCusTID;
                                        InvReport.BInvRprtLes1ID = null;
                                        InvReport.BInvRprtTotBil = Cus_InvRptTotalBill_V2PSTN;
                                    }
                                }
                            }
                            catch { }

                            if (lstCusServDest.Count > 0)
                            {
                                try
                                {
                                    CUS_V2FBBFBBServiceID = lstCusServDest.FirstOrDefault(t => t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                        t.BsPtCusServiceTypes.Equals("Basic voice -FBB/FBB")).CusServDID;
                                }
                                catch { CUS_V2FBBFBBServiceID = 0; }

                                try
                                {
                                    LES_V2FBBFBBServiceID = lstSpServDest.FirstOrDefault(t => t.BPtServTfkLesRegID == LES_RegsID &&
                                                                                                    t.BsPtServiceTypes.Equals("Basic voice -FBB/FBB")).SPServDID;
                                }
                                catch
                                {
                                    LES_V2FBBFBBServiceID = 0;
                                }

                            }
                            List<Bs1BLesT> lstVceFbbFbb = lstRecord.AsEnumerable()
                                                 .Where(t => t.fkCommType == 2 && t.fkSPServTypeID == LES_V2FBBFBBServiceID &&
                                                 (t.fkBs1BLesID == secLes.BS1ALesID)).ToList();

                            decimal? Cus_InvRptTotalBill_V2FbbFbb = 0;
                            foreach (var records in lstVceFbbFbb)
                            {
                                bs2Sec = new Bs2Sec();
                                bs2Sec.fkCTFbID = id;
                                bs2Sec.BsCtStartDate = records.Bs1BLesStartDate;
                                bs2Sec.BsCtStarTime = records.Bs1BLesStarTime;
                                bs2Sec.BsCtOrigion = records.Bs1BLesOrigion;
                                bs2Sec.BsCtDestination = records.Bs1BLesDestination;
                                bs2Sec.fkSPServTypeID = Convert.ToInt32(CUS_V2FBBFBBServiceID);

                                //long SecconvertToMin = Convert.ToInt64(records.Bs1LesBillIncre);
                                bs2Sec.BsCtTransUsage = records.Bs1BLesTransUsage; //(decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkUOMID = context.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_VoiceFBB), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkCommType = 2;

                                Cus_TotalCostSP += bs2Sec.BsCtTotalUsage;
                                Cus_InvRptTotalBill_V2FbbFbb += bs2Sec.BsCtTotalUsage;
                                Les_TotalCostCP += Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * LES_VoiceFBB), 2, MidpointRounding.AwayFromZero);

                                context.Bs2Sec.Add(bs2Sec);

                                //context.Bs1LesT.Remove(records);
                            }

                            try
                            {
                                if (lstVceFbbFbb.Count() > 0)
                                {
                                    InvReport = new BInvLstReport();
                                    InvReport = lstInvreport.FirstOrDefault(t => t.BInvRprtSerType.Equals("Basic voice -FBB/FBB"));
                                    if (InvReport != null)
                                    {
                                        InvReport.fkBCus2ID = SecMtCust.BSMtCusTID;
                                        InvReport.BInvRprtLes1ID = null;
                                        InvReport.BInvRprtTotBil = Cus_InvRptTotalBill_V2FbbFbb;
                                    }
                                }
                            }
                            catch { }

                            // lstRecord = context.Bs1LesT.Where(t => t.fkBs1LesID == secLes.BS1MtLesID).ToList();

                            if (lstCusServDest.Count > 0)
                            {
                                try
                                {
                                    CUS_PrepaidServiceID = lstCusServDest.FirstOrDefault(t => t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                        t.BsPtCusServiceTypes.Equals("prepaid server")).CusServDID;
                                }
                                catch { CUS_PrepaidServiceID = 0; }

                                try
                                {
                                    LES_PrepaidServiceID = lstSpServDest.FirstOrDefault(t => t.BPtServTfkLesRegID == LES_RegsID &&
                                                                                                    t.BsPtServiceTypes.Equals("prepaid server")).SPServDID;
                                }
                                catch
                                {
                                    LES_PrepaidServiceID = 0;
                                }

                            }
                            List<Bs1BLesT> lstPrepaidServer = lstRecord.AsEnumerable()
                                                 .Where(t => t.fkCommType == 2 && t.BsPtSPServiceDest.BsPtServiceTypes.Equals("prepaid server") &&
                                                 (t.fkBs1BLesID == secLes.BS1ALesID)).ToList();

                            decimal? Cus_InvRptTotalBill_V2Prpaid = 0;
                            foreach (var records in lstPrepaidServer)
                            {
                                bs2Sec = new Bs2Sec();
                                bs2Sec.fkCTFbID = id;
                                bs2Sec.BsCtStartDate = records.Bs1BLesStartDate;
                                bs2Sec.BsCtStarTime = records.Bs1BLesStarTime;
                                bs2Sec.BsCtOrigion = records.Bs1BLesOrigion;
                                bs2Sec.BsCtDestination = records.Bs1BLesDestination;
                                bs2Sec.fkSPServTypeID = Convert.ToInt32(CUS_PrepaidServiceID);

                                //long SecconvertToMin = Convert.ToInt64(records.Bs1LesBillIncre);
                                bs2Sec.BsCtTransUsage = records.Bs1BLesTransUsage; //(decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkUOMID = context.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_PrepaidServer), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkCommType = 2;

                                Cus_TotalCostSP += bs2Sec.BsCtTotalUsage;
                                Cus_InvRptTotalBill_V2Prpaid += bs2Sec.BsCtTotalUsage;
                                Les_TotalCostCP += Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * LES_PrepaidServer), 2, MidpointRounding.AwayFromZero);

                                context.Bs2Sec.Add(bs2Sec);

                                //context.Bs1LesT.Remove(records);
                            }

                            try
                            {
                                if (lstPrepaidServer.Count() > 0)
                                {
                                    InvReport = new BInvLstReport();
                                    InvReport = lstInvreport.FirstOrDefault(t => t.BInvRprtSerType.Equals("prepaid server"));
                                    if (InvReport != null)
                                    {
                                        InvReport.fkBCus2ID = SecMtCust.BSMtCusTID;
                                        InvReport.BInvRprtLes1ID = null;
                                        InvReport.BInvRprtTotBil = Cus_InvRptTotalBill_V2Prpaid;
                                    }
                                }
                            }
                            catch { }

                            int commtype = 1;
                            try
                            {
                                commtype = context.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                            }
                            catch
                            {
                                commtype = context.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                            }

                            if (lstCusServDest.Count > 0)
                            {
                                try
                                {
                                    CUS_SMSServiceID = lstCusServDest.FirstOrDefault(t => t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                        t.BsPtCusServiceTypes.ToUpper().Equals("SMS")).CusServDID;
                                }
                                catch { CUS_SMSServiceID = 0; }

                                try
                                {
                                    LES_SMSServiceID = lstSpServDest.FirstOrDefault(t => t.BPtServTfkLesRegID == LES_RegsID &&
                                                                                                    t.BsPtServiceTypes.ToUpper().Equals("SMS")).SPServDID;
                                }
                                catch
                                {
                                    LES_SMSServiceID = 0;
                                }
                            }
                            List<Bs1BLesT> lstSMS = lstRecord.AsEnumerable()
                                            .Where(t => t.fkCommType == commtype && t.fkSPServTypeID == LES_SMSServiceID &&
                                            (t.fkBs1BLesID == secLes.BS1ALesID)).ToList();

                            decimal? Cus_InvRptTotalBill_sms = 0;
                            foreach (var records in lstSMS)
                            {
                                bs2Sec = new Bs2Sec();
                                bs2Sec.fkCTFbID = id;
                                bs2Sec.BsCtStartDate = records.Bs1BLesStartDate;
                                bs2Sec.BsCtStarTime = records.Bs1BLesStarTime;
                                bs2Sec.BsCtOrigion = records.Bs1BLesOrigion;
                                bs2Sec.BsCtDestination = records.Bs1BLesDestination;
                                bs2Sec.fkSPServTypeID = Convert.ToInt32(CUS_SMSServiceID);

                                //long SecconvertToMin = Convert.ToInt64(records.Bs1LesBillIncre);
                                bs2Sec.BsCtBillIncre = records.Bs1BLesTransUsage; //(decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                try
                                {
                                    bs2Sec.fkUOMID = context.BsMtUnits.FirstOrDefault(t => t.Units.Equals("message")).UID;
                                }
                                catch
                                {
                                    bs2Sec.fkUOMID = context.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Message")).UID;
                                }

                                bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_SMS), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkCommType = commtype;

                                Cus_InvRptTotalBill_sms += bs2Sec.BsCtTotalUsage;
                                Cus_TotalCostSP += bs2Sec.BsCtTotalUsage;
                                Les_TotalCostCP += Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * LES_SMS), 2, MidpointRounding.AwayFromZero);

                                context.Bs2Sec.Add(bs2Sec);
                            }

                            try
                            {
                                if (lstSMS.Count() > 0)
                                {
                                    InvReport = new BInvLstReport();
                                    InvReport = lstInvreport.FirstOrDefault(t => t.BInvRprtSerType.Equals("SMS"));
                                    if (InvReport != null)
                                    {
                                        InvReport.fkBCus2ID = SecMtCust.BSMtCusTID;
                                        InvReport.BInvRprtLes1ID = null;
                                        InvReport.BInvRprtTotBil = Cus_InvRptTotalBill_sms;
                                    }
                                }
                            }
                            catch { }

                            foreach (var lessT in lstDateCT)
                                context.Bs1BLesT.Remove(lessT);

                            foreach (var lessT in lstVceFbbCell)
                                context.Bs1BLesT.Remove(lessT);

                            foreach (var lessT in lstVceFbbPSTN)
                                context.Bs1BLesT.Remove(lessT);

                            foreach (var lessT in lstVceFbbFbb)
                                context.Bs1BLesT.Remove(lessT);

                            foreach (var lessT in lstPrepaidServer)
                                context.Bs1BLesT.Remove(lessT);

                            foreach (var lessT in lstSMS)
                                context.Bs1BLesT.Remove(lessT);

                            var rejLog = context.BS1LesRejLog.Where(t => t.fkBs1LesReg == secLes.BS1ALesID).ToList();
                            foreach (var rej in rejLog)
                                context.BS1LesRejLog.Remove(rej);

                            context.BS1ALessT.Remove(secLes);

                            // List of Satatement 
                            Bs2Statement statment = null;
                            try
                            {
                                statment = context.Bs2Statement.FirstOrDefault(t => t.fkBs2CusID == id);
                            }
                            catch { }


                            //if (statment == null)
                            //{
                            //    statment = new Bs2Statement();

                            //    statment.fkBs2CusID = id;
                            //    statment.fkSimID = secLes.fkSimID;
                            //    statment.Bs2StTotalCP = Les_TotalCostCP;
                            //    statment.Bs2StTotalSP = Cus_TotalCostSP;
                            //    try
                            //    {
                            //        int smtscp = context.BCtAs.FirstOrDefault(t => t.BCtAFbServDest.Equals(duration)).BCtAID;
                            //        decimal? recurFee = context.BCtAs.FirstOrDefault(t => t.BCtAID == smtscp).BCtAFbRecurFee;
                            //        recurFee += context.BCtBs.Where(t => t.BCtBfkFbID == smtscp).Sum(t => t.BCtBTotalUsage);
                            //        SMTS_TotalCost += Math.Round(Convert.ToDecimal(recurFee), 2, MidpointRounding.AwayFromZero);
                            //    }
                            //    catch { }
                            //    statment.Bs2StSMTSCost = SMTS_TotalCost;
                            //    statment.fkSPID = secLes.fkSPID;
                            //    statment.fkCusID = CUS_Id;
                            //    statment.Bs2StIvoiceDate = DateTime.Now.Date;
                            //    statment.Bs2StDuration = duration;

                            //    if (statment.Bs2StTotalCP > 0 && statment.Bs2StTotalSP > 0)
                            //    {
                            //        decimal CrossProfit = Convert.ToDecimal(statment.Bs2StTotalSP - statment.Bs2StSMTSCost);
                            //        decimal Margin = (CrossProfit / Convert.ToDecimal(statment.Bs2StTotalSP) * 100);
                            //        statment.Bs2stMargin = Margin > 0 ? Convert.ToInt32(Margin) : (int)Math.Abs(Margin);
                            //    }

                            //    context.Bs2Statement.Add(statment);
                            //}
                        }
                    }
                }
                catch (Exception erorr)
                {
                    throw erorr;
                }

                context.SaveChanges();
                return true;
                //isMovedtoInvoice = true;
            }
            catch (Exception error)
            {
                throw;
            }
            //return isMovedtoInvoice;
        }

        public Tuple<Stream, string, bool> GnerateManualInvoice(Controllers.SecondaryTable.VM_ManualInvoice manualInvoice)
        {
            System.IO.Stream spreadsheetstream = new System.IO.MemoryStream();

            string Invoiceno = string.Empty;

            int value = 0;

            try
            {
                string imsiID = string.Empty;
                string invoiceNo = string.Empty;
                string ImarsatID = string.Empty;

                string buildingName = string.Empty;
                string street = string.Empty;
                string district = string.Empty;
                string city = string.Empty;
                string country = string.Empty;

                string VesselName = string.Empty;

                string CustomerName = string.Empty;

                List<string> SDescription = new List<string>();
                Dictionary<string, decimal?> dictServiceDescription = new Dictionary<string, decimal?>();

                using (BillingSystemEntities db = new BillingSystemEntities())
                {
                    invoiceNo = manualInvoice.InvoiceNo;
                    VesselName = db.BPtShips.FirstOrDefault(t => t.Ship_ID == manualInvoice.Vessel).BPtShipName;
                    CustomerName = db.BsMtCustomers.FirstOrDefault(t => t.CusID == manualInvoice.CstomerName).Customers;
                    string CusAddress = db.BsPtCusRegs_.FirstOrDefault(t => t.fkCusID == manualInvoice.CstomerName).BsPtCusAddr;
                    if (!string.IsNullOrEmpty(CusAddress))
                    {
                        string[] addressparts = CusAddress.Split(',');

                        buildingName = addressparts.Length > 0 ? addressparts[0] : string.Empty;
                        street = addressparts.Length > 1 ? addressparts[1] : string.Empty;
                        district = addressparts.Length > 2 ? addressparts[2] : string.Empty;
                        city = addressparts.Length > 3 ? addressparts[3] : string.Empty;
                        country = addressparts.Length > 4 ? addressparts[4] : string.Empty;
                    }
                }

                DateTime dt = DateTime.Now;
                string Period = dt.ToString("MMM");
                DateTime dy = DateTime.Today;
                string currYear = dy.Year.ToString();

                string CurrPeriod = Period + '-' + currYear;
                string InvoiceFileName = invoiceNo + '_' + buildingName + '_' + VesselName;

                string pathid = Guid.NewGuid().ToString() + " " + DateTime.Now.Date.ToString("dd-MM-yyyy");

                #region WorkBook Prepartion

                XLWorkbook xLWorkbook = new XLWorkbook();
                IXLWorksheet workSheet = xLWorkbook.Worksheets.Add("Invoice" + Invoiceno);

                workSheet.Column(1).Width = 0.83;
                workSheet.Column(2).Width = 1.50;
                workSheet.Column(3).Width = 0.46;
                workSheet.Column(4).Width = 1.50;
                workSheet.Column(5).Width = 1.50;
                workSheet.Column(6).Width = 2.71;
                workSheet.Column(7).Width = 0.54;
                workSheet.Column(8).Width = 4.25;
                workSheet.Column(9).Width = 1.50;
                workSheet.Column(10).Width = 1.50;
                workSheet.Column(11).Width = 1.50;
                workSheet.Column(12).Width = 1.50;
                workSheet.Column(13).Width = 9.00;
                workSheet.Column(14).Width = 0.54;
                workSheet.Column(15).Width = 0.69;
                workSheet.Column(16).Width = 0.54;
                workSheet.Column(17).Width = 0.62;
                workSheet.Column(18).Width = 0.69;
                workSheet.Column(19).Width = 0.00;
                workSheet.Column(20).Width = 0.00;
                workSheet.Column(21).Width = 1.50;
                workSheet.Column(22).Width = 1.50;
                workSheet.Column(23).Width = 1.50;
                workSheet.Column(24).Width = 1.50;
                workSheet.Column(25).Width = 1.50;
                workSheet.Column(26).Width = 0.38;
                workSheet.Column(27).Width = 1.50;
                workSheet.Column(28).Width = 1.50;
                workSheet.Column(29).Width = 0.00;
                workSheet.Column(30).Width = 0.38;
                workSheet.Column(31).Width = 0.45;
                workSheet.Column(32).Width = 0.83;
                workSheet.Column(33).Width = 3.38;
                workSheet.Column(34).Width = 2.75;
                workSheet.Column(35).Width = 0.54;
                workSheet.Column(36).Width = 0.83;
                workSheet.Column(37).Width = 1.00;
                workSheet.Column(38).Width = 0.46;
                workSheet.Column(39).Width = 1.50;
                workSheet.Column(40).Width = 1.50;
                workSheet.Column(41).Width = 1.50;
                workSheet.Column(42).Width = 1.50;

                // Heading
                workSheet.Range("A3:AP3").Merge();

                //old code
                //workSheet.Cell(2, 1).Style.Font.Bold = true;
                //workSheet.Cell(4, 1).Style.Font.Bold = false;

                workSheet.Row(1).Height = 57.75;
                workSheet.Row(2).Height = 12.75;
                workSheet.Row(3).Height = 15.00;
                workSheet.Row(4).Height = 15.00;
                workSheet.Row(5).Height = 15.00;
                workSheet.Row(6).Height = 12.75;
                workSheet.Row(7).Height = 14.25;
                workSheet.Row(8).Height = 12.00;
                workSheet.Row(9).Height = 12.00;
                workSheet.Row(10).Height = 13.50;
                workSheet.Row(11).Height = 11.25;

                //---------------- Style Start --------------------------
                workSheet.Row(4).Style.Font.FontName = "Times New Roman";
                workSheet.Row(5).Style.Font.FontName = "Times New Roman";
                workSheet.Row(6).Style.Font.FontName = "Times New Roman";
                workSheet.Row(7).Style.Font.FontName = "Times New Roman";
                workSheet.Row(8).Style.Font.FontName = "Times New Roman";
                workSheet.Row(9).Style.Font.FontName = "Times New Roman";
                workSheet.Row(10).Style.Font.FontName = "Times New Roman";
                workSheet.Row(11).Style.Font.FontName = "Times New Roman";
                workSheet.Row(12).Style.Font.FontName = "Times New Roman";
                workSheet.Row(13).Style.Font.FontName = "Times New Roman";
                workSheet.Row(14).Style.Font.FontName = "Times New Roman";
                workSheet.Row(15).Style.Font.FontName = "Times New Roman";
                workSheet.Row(16).Style.Font.FontName = "Times New Roman";
                workSheet.Row(17).Style.Font.FontName = "Times New Roman";
                workSheet.Row(18).Style.Font.FontName = "Times New Roman";
                workSheet.Row(19).Style.Font.FontName = "Times New Roman";
                workSheet.Row(26).Style.Font.FontName = "Times New Roman";
                workSheet.Row(27).Style.Font.FontName = "Times New Roman";
                workSheet.Row(28).Style.Font.FontName = "Times New Roman";
                workSheet.Row(29).Style.Font.FontName = "Times New Roman";
                workSheet.Row(30).Style.Font.FontName = "Times New Roman";
                workSheet.Row(31).Style.Font.FontName = "Times New Roman";
                workSheet.Row(32).Style.Font.FontName = "Times New Roman";
                workSheet.Row(33).Style.Font.FontName = "Times New Roman";
                workSheet.Row(35).Style.Font.FontName = "Times New Roman";
                workSheet.Row(36).Style.Font.FontName = "Times New Roman";
                workSheet.Row(38).Style.Font.FontName = "Times New Roman";
                workSheet.Row(39).Style.Font.FontName = "Times New Roman";
                workSheet.Row(40).Style.Font.FontName = "Times New Roman";
                workSheet.Row(41).Style.Font.FontName = "Times New Roman";
                workSheet.Row(42).Style.Font.FontName = "Times New Roman";
                workSheet.Row(43).Style.Font.FontName = "Times New Roman";
                workSheet.Row(44).Style.Font.FontName = "Times New Roman";
                workSheet.Row(45).Style.Font.FontName = "Times New Roman";
                workSheet.Row(46).Style.Font.FontName = "Times New Roman";
                workSheet.Row(47).Style.Font.FontName = "Times New Roman";
                workSheet.Row(48).Style.Font.FontName = "Times New Roman";
                workSheet.Row(49).Style.Font.FontName = "Times New Roman";
                workSheet.Row(50).Style.Font.FontName = "Times New Roman";
                workSheet.Row(51).Style.Font.FontName = "Times New Roman";
                workSheet.Row(52).Style.Font.FontName = "Times New Roman";
                workSheet.Row(53).Style.Font.FontName = "Times New Roman";

                //--------------- Style End -------------------------------


                workSheet.Row(6).Style.Font.Bold = false;
                workSheet.Row(13).Style.Font.Bold = false;
                int j = 6;
                while (j < 78)
                {
                    workSheet.Row(j).Style.Font.FontSize = 10;
                    j++;
                }

                string Imagepath = string.Empty;
                if (MySession.InvoiceLogoPath != null)
                {
                    Imagepath = MySession.InvoiceLogoPath;
                }
                else
                    return new Tuple<Stream, string, bool>(spreadsheetstream, InvoiceFileName, true);

                var image = workSheet.AddPicture(Imagepath)
                              .MoveTo(workSheet.Cell(1, 1)).Scale(1.05);

                // Invoice Header
                workSheet.Range("A3:AO3").Merge();
                workSheet.Cell(3, 1).Value = "Invoice";
                workSheet.Cell(3, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                workSheet.Cell(3, 1).Style.Font.FontSize = 14;
                workSheet.Row(3).Style.Font.FontName = "Times New Roman";
                workSheet.Row(3).Style.Font.Bold = true;

                // Invoice Number format
                workSheet.Range("X4:AD4").Merge();
                workSheet.Cell(4, 24).Value = "Invoice No :";
                workSheet.Cell(4, 24).Style.Font.Bold = true;
                //workSheet.Cell(4, 24).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                workSheet.Range("AF4:AO4").Merge();
                workSheet.Cell(4, 32).Value = manualInvoice.InvoiceNo;
                workSheet.Cell(4, 32).Style.Font.Bold = true;

                // Invoice Date
                workSheet.Range("X5:AD5").Merge();
                workSheet.Cell(5, 24).Value = "Date :";
                workSheet.Range("AF5:AO5").Merge();
                workSheet.Cell(5, 32).Value = manualInvoice.InvoiceDate;
                workSheet.Cell(5, 32).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);

                // Vessel and To
                workSheet.Range("X6:AD6").Merge();
                workSheet.Cell(6, 24).Value = "Vessel :";
                //workSheet.Cell(6, 24).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                workSheet.Range("AF6:AO6").Merge();
                workSheet.Cell(6, 32).Value = VesselName;

                workSheet.Cell(4, 1).Value = "To:";
                workSheet.Range("D4:V4").Merge();

                // Address
                workSheet.Range("D5:V5").Merge();
                workSheet.Cell(5, 4).Value = buildingName.ToString();
                workSheet.Cell(5, 4).Style.Font.Bold = true;
                //workSheet.Cell(5, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                workSheet.Range("D6:V6").Merge();
                workSheet.Cell(6, 4).Value = street.ToString();
                workSheet.Range("D7:V7").Merge();
                workSheet.Cell(7, 4).Value = district.ToString();
                //workSheet.Cell(7, 4).Style.Font.Bold = false;
                workSheet.Range("D8:V8").Merge();
                workSheet.Cell(8, 4).Value = city.ToString();
                workSheet.Range("D9:V9").Merge();
                workSheet.Cell(9, 4).Value = country.ToString();

                // Account Code
                workSheet.Range("X7:AD7").Merge();
                workSheet.Cell(7, 24).Value = "Account Code :";
                workSheet.Range("AF7:AO7").Merge();
                workSheet.Cell(7, 32).Value = manualInvoice.AcCode;

                // PO Number
                workSheet.Range("X8:AD8").Merge();
                workSheet.Cell(8, 24).Value = "PO Number";
                workSheet.Range("AF8:AO8").Merge();
                workSheet.Cell(8, 32).Value = manualInvoice.PONo;

                // DO Number
                workSheet.Range("X9:AD9").Merge();
                workSheet.Cell(9, 24).Value = "DO Number";
                workSheet.Range("AF9:AO9").Merge();
                workSheet.Cell(9, 32).Value = manualInvoice.DONo;

                // Accounts Department
                workSheet.Cell(10, 1).Value = " Attn";
                workSheet.Cell(10, 4).Value = "Accounts Department";

                // One Space Row
                workSheet.Row(11).Height = 18;
                workSheet.Cell(11, 1).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Top);

                workSheet.Cell(12, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                workSheet.Cell(12, 1).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                workSheet.Cell(12, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                workSheet.Cell(12, 4).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                workSheet.Cell(12, 8).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                workSheet.Cell(12, 8).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                workSheet.Cell(12, 15).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                workSheet.Cell(12, 15).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                workSheet.Cell(12, 36).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                workSheet.Cell(12, 36).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);

                workSheet.Range("A12:B12").Merge();
                workSheet.Cell(12, 1).Value = "Sn";

                //workSheet.Range("D12:F12").Merge();
                //workSheet.Cell(12, 4).Value = "Period";

                //workSheet.Range("H12:M12").Merge();
                //workSheet.Cell(12, 8).Value = "InmarSat ID";

                //workSheet.Range("O12:AH12").Merge();
                //workSheet.Cell(12, 15).Value = "Service Description";

                workSheet.Range("D12:AB12").Merge();
                workSheet.Cell(12, 4).Value = "Service Description";

                workSheet.Range("AD12:AH12").Merge();
                workSheet.Cell(12, 30).Value = "Qty";

                workSheet.Range("AJ12:AO12").Merge();
                workSheet.Cell(12, 36).Value = "Amount";

                workSheet.Cell(12, 1).Style.Font.Bold = true;
                workSheet.Cell(12, 4).Style.Font.Bold = true;
                workSheet.Cell(12, 8).Style.Font.Bold = true;
                workSheet.Cell(12, 15).Style.Font.Bold = true;
                workSheet.Cell(12, 36).Style.Font.Bold = true;

                workSheet.Cell(12, 1).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 1).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 1).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 1).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 2).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 2).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 2).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 5).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 5).Style.Border.BottomBorder = XLBorderStyleValues.Medium;

                workSheet.Cell(12, 6).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 6).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 6).Style.Border.RightBorder = XLBorderStyleValues.Medium;

                workSheet.Cell(12, 7).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 7).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                //workSheet.Cell(12, 7).Style.Border.RightBorder = XLBorderStyleValues.Medium;

                workSheet.Cell(12, 33).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 33).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 34).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 34).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 34).Style.Border.RightBorder = XLBorderStyleValues.Medium;

                workSheet.Cell(12, 4).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 4).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 4).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                //workSheet.Cell(12, 4).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 9).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 9).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 10).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 10).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 11).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 11).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 12).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 12).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 13).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 13).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                //workSheet.Cell(12, 13).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                //workSheet.Cell(12, 13).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 14).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 14).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                //workSheet.Cell(12, 14).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                //workSheet.Cell(12, 14).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 8).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                //workSheet.Cell(12, 8).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 8).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                //workSheet.Cell(12, 8).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 15).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 15).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 15).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                //workSheet.Cell(12, 15).Style.Border.RightBorder = XLBorderStyleValues.Medium;

                workSheet.Cell(12, 16).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 16).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 17).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 17).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 18).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 18).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 19).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 19).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 20).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 20).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 21).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 21).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 22).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 22).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 23).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 23).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 24).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 24).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 25).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 25).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 26).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 26).Style.Border.BottomBorder = XLBorderStyleValues.Medium;

                workSheet.Cell(12, 27).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 27).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                //workSheet.Cell(12, 27).Style.Border.LeftBorder = XLBorderStyleValues.Medium;

                workSheet.Cell(12, 28).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 28).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 28).Style.Border.RightBorder = XLBorderStyleValues.Medium;

                //workSheet.Cell(12, 29).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                //workSheet.Cell(12, 29).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                //workSheet.Cell(12, 29).Style.Border.RightBorder = XLBorderStyleValues.Medium;

                workSheet.Cell(12, 30).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 30).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 30).Style.Border.LeftBorder = XLBorderStyleValues.Medium;

                workSheet.Cell(12, 31).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 31).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 32).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 32).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 32).Style.Border.RightBorder = XLBorderStyleValues.Medium;

                workSheet.Cell(12, 36).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 36).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 36).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 36).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 37).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 37).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 38).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 38).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 39).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 39).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 40).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 40).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 41).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 41).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(12, 41).Style.Border.RightBorder = XLBorderStyleValues.Medium;

                workSheet.Cell(12, 1).Style.Font.FontSize = 12;
                workSheet.Cell(12, 4).Style.Font.FontSize = 12;
                workSheet.Cell(12, 8).Style.Font.FontSize = 12;
                workSheet.Cell(12, 15).Style.Font.FontSize = 12;
                workSheet.Cell(12, 36).Style.Font.FontSize = 12;

                // Empty Space Row
                workSheet.Row(13).Height = 6;

                //workSheet.Cell(14, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                //workSheet.Cell(14, 1).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                //workSheet.Cell(14, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                //workSheet.Cell(14, 2).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                //workSheet.Cell(14, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                //workSheet.Cell(14, 3).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                //workSheet.Cell(14, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                //workSheet.Cell(14, 4).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                //workSheet.Cell(14, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                //workSheet.Cell(14, 5).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);

                int row = 14;
                decimal? totals = 0;
                int sno = 1;
                foreach (var services in manualInvoice.ServiceDest)
                {
                    workSheet.Range("A" + row + ":B" + row).Merge();
                    workSheet.Cell(row, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    workSheet.Cell(row, 1).Value = sno++;

                    workSheet.Range("D" + row + ":AB" + row).Merge();
                    workSheet.Cell(row, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                    workSheet.Cell(row, 4).Value = services.description;

                    workSheet.Range("AD" + row + ":AH" + row).Merge();
                    workSheet.Cell(row, 30).Style.NumberFormat.Format = "@";
                    workSheet.Cell(row, 30).Value = services.qty;

                    workSheet.Range("AJ" + row + ":AO" + row).Merge();
                    workSheet.Cell(row, 36).Value = services.unitprice;

                    //workSheet.Range("D" + row + ":F" + row).Merge();
                    //workSheet.Cell(row, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    //workSheet.Cell(row, 4).Style.NumberFormat.Format = "@";
                    //workSheet.Cell(row, 4).Value = CurrPeriod;

                    //workSheet.Range("H" + row + ":M" + row).Merge();
                    //workSheet.Cell(row, 8).Style.NumberFormat.Format = "@";
                    //workSheet.Cell(row, 8).Value = ImarsatID;

                    //workSheet.Range("O" + row + ":AH" + row).Merge();
                    //workSheet.Cell(row, 15).Value = services.Key;

                    //workSheet.Range("AJ" + row + ":AO" + row).Merge();
                    //workSheet.Cell(row, 36).Value = services.Value;

                    totals += services.unitprice;
                    row++;
                }

                // workSheet.Cells[13, 7].Style.Font.Size = 12;
                workSheet.SheetView.Freeze(12, 0);
                // workSheet.Row(15).Style.Font.Bold = false;
                // workSheet.Row(17).Style.Font.Bold = true;
                workSheet.Cell(18, 6).Style.Font.Bold = true;
                workSheet.Row(20).Style.Font.Bold = true;
                workSheet.Row(21).Style.Font.Bold = true;
                workSheet.Row(22).Style.Font.Bold = true;
                workSheet.Row(23).Style.Font.Bold = true;

                // workSheet.Cell(31, 22).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                //workSheet.Cell(31, 22).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                workSheet.Cell(31, 22).Value = "Total";

                workSheet.Cell(31, 36).Value = totals;
                workSheet.Cell(31, 36).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                workSheet.Cell(31, 37).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                workSheet.Cell(31, 38).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                workSheet.Cell(31, 39).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                workSheet.Cell(31, 40).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                workSheet.Cell(31, 41).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);

                workSheet.Cell(32, 22).Value = "Exchange Rate";
                workSheet.Cell(33, 22).Value = "Administrative Fee";
                workSheet.Cell(33, 31).Value = "%";
                workSheet.Cell(33, 36).Value = "-";
                workSheet.Cell(33, 36).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                workSheet.Row(38).Style.Font.FontSize = 9;
                workSheet.Row(39).Style.Font.FontSize = 9;
                workSheet.Row(40).Style.Font.FontSize = 9;
                workSheet.Row(41).Style.Font.FontSize = 9;
                workSheet.Row(42).Style.Font.FontSize = 9;
                workSheet.Row(43).Style.Font.FontSize = 9;
                workSheet.Row(44).Style.Font.FontSize = 9;
                workSheet.Row(45).Style.Font.FontSize = 9;
                workSheet.Row(46).Style.Font.FontSize = 9;
                workSheet.Row(47).Style.Font.FontSize = 9;
                workSheet.Row(48).Style.Font.FontSize = 9;
                workSheet.Row(49).Style.Font.FontSize = 9;
                workSheet.Row(50).Style.Font.FontSize = 9;
                workSheet.Row(51).Style.Font.FontSize = 9;
                workSheet.Row(52).Style.Font.FontSize = 9;
                workSheet.Row(53).Style.Font.FontSize = 9;
                workSheet.Cell(35, 27).Value = "Good Service Tax %";
                workSheet.Cell(36, 22).Value = "Total Payable";
                workSheet.Cell(36, 32).Value = "(USD)";
                workSheet.Range("AJ31:AO31").Merge();
                workSheet.Range("AJ32:AO32").Merge();
                workSheet.Range("AJ33:AO33").Merge();
                workSheet.Range("AJ35:AO35").Merge();
                workSheet.Range("AJ36:AO36").Merge();
                workSheet.Cell(36, 36).Value = totals;
                workSheet.Cell(36, 36).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                workSheet.Cell(36, 37).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                workSheet.Cell(36, 38).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                workSheet.Cell(36, 39).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                workSheet.Cell(36, 40).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
                workSheet.Cell(36, 41).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);

                workSheet.Cell(36, 22).Style.Font.Bold = true;
                workSheet.Cell(36, 32).Style.Font.Bold = true;
                workSheet.Cell(36, 35).Style.Font.Bold = true;
                workSheet.Range("A37:AO37").Merge();
                workSheet.Range("A37:AO37").Style.Border.BottomBorder = XLBorderStyleValues.Thick;

                workSheet.Cell(38, 2).Value = "Important Notes";
                workSheet.Cell(38, 2).Style.Font.Bold = true;
                workSheet.Cell(38, 24).Value = "For Comptroller Purposes Only,Rate @ 1.37";
                workSheet.Cell(39, 2).Value = "1";
                workSheet.Cell(39, 4).Value = "Credit Terms: 30 days from Date of Invoice";
                workSheet.Cell(40, 2).Value = "2";
                workSheet.Cell(40, 4).Value = "Please quote clearly your account and invoice no when making payment";
                workSheet.Cell(41, 2).Value = "3";
                workSheet.Cell(41, 4).Value = "All Bank charges to be borne by remitter.";

                workSheet.Cell(42, 2).Value = "4";
                workSheet.Cell(42, 4).Value = "Please send all checks payment to";
                workSheet.Cell(43, 4).Value = "Bukit Batok Central Post Office PO Box 181 Singapore 916507";

                workSheet.Cell(44, 2).Value = "5";
                workSheet.Cell(44, 4).Value = "Payment by Telegraphic Transfer can also be made to";
                workSheet.Cell(45, 4).Value = "Beneficiary";
                workSheet.Cell(45, 14).Value = "SMTS Pte Ltd";
                workSheet.Cell(45, 14).Style.Font.Bold = true;
                workSheet.Cell(46, 4).Value = "USD Account no";
                workSheet.Cell(46, 14).Value = "651-000093-301";
                workSheet.Cell(46, 14).Style.Font.Bold = true;
                workSheet.Cell(47, 4).Value = "OCBC Swift Code";
                workSheet.Cell(47, 14).Value = "OCBCSGSG";
                workSheet.Cell(47, 14).Style.Font.Bold = true;
                workSheet.Cell(48, 4).Value = "Bank";
                workSheet.Cell(48, 14).Value = "Oversea-Chinese Banking Corporation Limited";
                workSheet.Cell(48, 14).Style.Font.Bold = true;
                workSheet.Cell(49, 4).Value = "Address";
                workSheet.Cell(49, 14).Value = "10 Marina Boulevard #01-04 Marina Bay Financial Center Tower 2";
                workSheet.Cell(49, 14).Style.Font.Bold = true;
                workSheet.Cell(50, 14).Value = "Singapore 018983";
                workSheet.Cell(50, 14).Style.Font.Bold = true;
                workSheet.Cell(51, 4).Value = "Agent Swift Code";
                workSheet.Cell(51, 14).Value = "CHASUS33";
                workSheet.Cell(51, 14).Style.Font.Bold = true;
                workSheet.Cell(52, 4).Value = "Agent Bank";
                workSheet.Cell(52, 14).Value = "JP Morgan";
                workSheet.Cell(52, 14).Style.Font.Bold = true;
                workSheet.Cell(53, 2).Value = "6";
                workSheet.Cell(53, 4).Value = "Please make full payment by the due date to avoid 1.5% interest for late payment charge per month";
                workSheet.Cell(53, 4).Style.Font.Bold = true;

                #endregion

                // Save Work sheet into workbook
                xLWorkbook.SaveAs(spreadsheetstream);
                spreadsheetstream.Position = 0;

                //InvoiceFileName = temppath + "\\" + InvoiceFileName;

                //FileStream fileStream = new FileStream(InvoiceFileName, FileMode.Create);

                return new Tuple<Stream, string, bool>(spreadsheetstream, InvoiceFileName, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            throw new Exception("The master primary is not an interger, please check and update the issue.");
        }

        public Tuple<Stream, string, bool> GnerateInvListReport(List<int> primaryIDs, string logopath)
        {
            System.IO.Stream spreadsheetstream = new System.IO.MemoryStream();

            string Invoiceno = string.Empty;
            string LES_Name = string.Empty;

            try
            {
                List<BInvLstReport> lstinvReport = new List<BInvLstReport>();

                List<string> SDescription = new List<string>();
                Dictionary<string, dynamic> dictServiceDescription = new Dictionary<string, dynamic>();
                List<Dictionary<string, dynamic>> lstDict = new List<Dictionary<string, dynamic>>();

                using (BillingSystemEntities db = new BillingSystemEntities())
                {
                    foreach (int primaryID in primaryIDs)
                    {
                        int id = Convert.ToInt32(primaryID);

                        var SecFbb = db.BS2MtCusT.FirstOrDefault(t => t.BSMtCusTID == id);

                        if (SecFbb != null)
                        {
                            LES_Name = db.BsMtServiceProviders.FirstOrDefault(t => t.spID == SecFbb.fkSPID).ServiceProvider;
                            lstinvReport = db.BInvLstReports.Where(t => t.fkBCus2ID == id).ToList();

                            var temp = (from invlst in db.BInvLstReports
                                        join cust in db.BS2MtCusT on invlst.fkBCus2ID equals cust.BSMtCusTID
                                        join sim in db.BptFbSims on cust.fkSimID equals sim.Sim_ID
                                        join ship in db.BPtShips on sim.BPtFBImImsi equals ship.BPtShipMMSI
                                        where invlst.fkBCus2ID == SecFbb.BSMtCusTID
                                        select new
                                        {
                                            InvNo = cust.BSMtCusInvNo,
                                            InmarsatNo = sim.BPtFBimMsisdn,
                                            Vessel = ship.BPtShipName,
                                            serviceType = invlst.BInvRprtSerType,
                                            totalcp = invlst.BInvRprtTotCst,
                                            totalBill = invlst.BInvRprtTotBil
                                        }).ToList();

                            foreach (var p in temp)
                            {
                                dictServiceDescription = new Dictionary<string, dynamic>();
                                dictServiceDescription.Add("invoiceno", p.InvNo);
                                dictServiceDescription.Add("accode", string.Empty);
                                dictServiceDescription.Add("inmarsatid", p.InmarsatNo);
                                dictServiceDescription.Add("vessel", p.Vessel);
                                dictServiceDescription.Add("stype", p.serviceType);
                                dictServiceDescription.Add("cp", p.totalcp);
                                dictServiceDescription.Add("bill", p.totalBill);
                                dictServiceDescription.Add("admin", string.Empty);
                                dictServiceDescription.Add("status", string.Empty);
                                lstDict.Add(dictServiceDescription);
                            }

                        }
                    }
                }

                DateTime dt = DateTime.Now;
                string Period = dt.ToString("MMM");
                DateTime dy = DateTime.Today;
                string currYear = dy.Year.ToString();

                string CurrPeriod = Period + '-' + currYear;
                string InvoiceFileName = LES_Name+"001" + '_' + "Invoice List" + '_' + "Reports";

                string pathid = Guid.NewGuid().ToString() + " " + DateTime.Now.Date.ToString("dd-MM-yyyy");

                #region WorkBook Prepartion

                XLWorkbook xLWorkbook = new XLWorkbook();
                IXLWorksheet workSheet = xLWorkbook.Worksheets.Add("Report" + Invoiceno);

                workSheet.Column(1).Width = 2.83;
                workSheet.Column(2).Width = 16.44;
                workSheet.Column(3).Width = 12.15;
                workSheet.Column(4).Width = 12.50;
                workSheet.Column(5).Width = 23.1;
                workSheet.Column(6).Width = 22.1;
                workSheet.Column(7).Width = 9.44;
                workSheet.Column(8).Width = 11.56;
                workSheet.Column(9).Width = 9.83;
                workSheet.Column(10).Width = 9.53;
                //workSheet.Column(11).Width = 1.50;
                //workSheet.Column(12).Width = 1.50;
                //workSheet.Column(13).Width = 9.00;
                //workSheet.Column(14).Width = 0.54;
                //workSheet.Column(15).Width = 0.69;
                //workSheet.Column(16).Width = 0.54;
                //workSheet.Column(17).Width = 0.62;
                //workSheet.Column(18).Width = 0.69;
                //workSheet.Column(19).Width = 0.00;
                //workSheet.Column(20).Width = 0.00;
                //workSheet.Column(21).Width = 1.50;
                //workSheet.Column(22).Width = 1.50;
                //workSheet.Column(23).Width = 1.50;
                //workSheet.Column(24).Width = 1.50;
                //workSheet.Column(25).Width = 1.50;
                //workSheet.Column(26).Width = 0.38;
                //workSheet.Column(27).Width = 1.50;
                //workSheet.Column(28).Width = 1.50;
                //workSheet.Column(29).Width = 0.00;
                //workSheet.Column(30).Width = 0.38;
                //workSheet.Column(31).Width = 0.45;
                //workSheet.Column(32).Width = 0.83;
                //workSheet.Column(33).Width = 3.38;
                //workSheet.Column(34).Width = 2.75;
                //workSheet.Column(35).Width = 0.54;
                //workSheet.Column(36).Width = 0.83;
                //workSheet.Column(37).Width = 1.00;
                //workSheet.Column(38).Width = 0.46;
                //workSheet.Column(39).Width = 1.50;
                //workSheet.Column(40).Width = 1.50;
                //workSheet.Column(41).Width = 1.50;
                //workSheet.Column(42).Width = 1.50;

                //// Heading
                //workSheet.Range("A3:AP3").Merge();

                ////old code
                ////workSheet.Cell(2, 1).Style.Font.Bold = true;
                ////workSheet.Cell(4, 1).Style.Font.Bold = false;

                //workSheet.Row(1).Height = 57.75;
                //workSheet.Row(2).Height = 12.75;
                //workSheet.Row(3).Height = 15.00;
                //workSheet.Row(4).Height = 15.00;
                //workSheet.Row(5).Height = 15.00;
                //workSheet.Row(6).Height = 12.75;
                //workSheet.Row(7).Height = 14.25;
                //workSheet.Row(8).Height = 12.00;
                //workSheet.Row(9).Height = 12.00;
                //workSheet.Row(10).Height = 13.50;
                //workSheet.Row(11).Height = 11.25;

                //---------------- Style Start --------------------------
                workSheet.Row(4).Style.Font.FontName = "Times New Roman";
                workSheet.Row(5).Style.Font.FontName = "Times New Roman";
                workSheet.Row(6).Style.Font.FontName = "Times New Roman";
                workSheet.Row(7).Style.Font.FontName = "Times New Roman";
                workSheet.Row(8).Style.Font.FontName = "Times New Roman";
                workSheet.Row(9).Style.Font.FontName = "Times New Roman";
                workSheet.Row(10).Style.Font.FontName = "Times New Roman";
                workSheet.Row(11).Style.Font.FontName = "Times New Roman";
                workSheet.Row(12).Style.Font.FontName = "Times New Roman";
                workSheet.Row(13).Style.Font.FontName = "Times New Roman";
                workSheet.Row(14).Style.Font.FontName = "Times New Roman";
                workSheet.Row(15).Style.Font.FontName = "Times New Roman";
                workSheet.Row(16).Style.Font.FontName = "Times New Roman";
                workSheet.Row(17).Style.Font.FontName = "Times New Roman";
                workSheet.Row(18).Style.Font.FontName = "Times New Roman";
                workSheet.Row(19).Style.Font.FontName = "Times New Roman";
                workSheet.Row(26).Style.Font.FontName = "Times New Roman";
                workSheet.Row(27).Style.Font.FontName = "Times New Roman";
                workSheet.Row(28).Style.Font.FontName = "Times New Roman";
                workSheet.Row(29).Style.Font.FontName = "Times New Roman";
                workSheet.Row(30).Style.Font.FontName = "Times New Roman";
                workSheet.Row(31).Style.Font.FontName = "Times New Roman";
                workSheet.Row(32).Style.Font.FontName = "Times New Roman";
                workSheet.Row(33).Style.Font.FontName = "Times New Roman";
                workSheet.Row(35).Style.Font.FontName = "Times New Roman";
                workSheet.Row(36).Style.Font.FontName = "Times New Roman";
                workSheet.Row(38).Style.Font.FontName = "Times New Roman";
                workSheet.Row(39).Style.Font.FontName = "Times New Roman";
                workSheet.Row(40).Style.Font.FontName = "Times New Roman";
                workSheet.Row(41).Style.Font.FontName = "Times New Roman";
                workSheet.Row(42).Style.Font.FontName = "Times New Roman";
                workSheet.Row(43).Style.Font.FontName = "Times New Roman";
                workSheet.Row(44).Style.Font.FontName = "Times New Roman";
                workSheet.Row(45).Style.Font.FontName = "Times New Roman";
                workSheet.Row(46).Style.Font.FontName = "Times New Roman";
                workSheet.Row(47).Style.Font.FontName = "Times New Roman";
                workSheet.Row(48).Style.Font.FontName = "Times New Roman";
                workSheet.Row(49).Style.Font.FontName = "Times New Roman";
                workSheet.Row(50).Style.Font.FontName = "Times New Roman";
                workSheet.Row(51).Style.Font.FontName = "Times New Roman";
                workSheet.Row(52).Style.Font.FontName = "Times New Roman";
                workSheet.Row(53).Style.Font.FontName = "Times New Roman";

                //--------------- Style End -------------------------------


                workSheet.Row(6).Style.Font.Bold = false;
                workSheet.Row(13).Style.Font.Bold = false;
                int j = 6;
                while (j < 78)
                {
                    workSheet.Row(j).Style.Font.FontSize = 10;
                    j++;
                }

                var image = workSheet.AddPicture(logopath)
                              .MoveTo(workSheet.Cell(1, 1)).Scale(1.05);

                // Invoice Header
                workSheet.Range("A7:J7").Merge();
                workSheet.Cell(7, 1).Value = "Invoice List Report";
                workSheet.Cell(7, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                workSheet.Cell(7, 1).Style.Font.FontSize = 14;
                workSheet.Row(7).Style.Font.FontName = "Times New Roman";
                workSheet.Row(7).Style.Font.Bold = true;

                //LES Name
                workSheet.Cell(8, 2).Value = "LES Name";
                workSheet.Cell(8, 2).Style.Font.Bold = true;

                workSheet.Cell(8, 3).Value = LES_Name;
                workSheet.Cell(8, 3).Style.Font.Bold = true;

                //LES Invoice No
                workSheet.Cell(9, 2).Value = "LES Invoice No";
                workSheet.Cell(9, 2).Style.Font.Bold = true;

                workSheet.Cell(9, 3).Value = "XXX";
                workSheet.Cell(9, 3).Style.Font.Bold = true;

                // Invoice Number format
                workSheet.Cell(8, 9).Value = "Date:";
                workSheet.Cell(8, 9).Style.Font.Bold = true;
                //workSheet.Cell(4, 24).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                //workSheet.Range("AE4:AO4").Merge();
                workSheet.Cell(8, 10).Value = DateTime.Now.Date.ToString();
                workSheet.Cell(8, 10).Style.Font.Bold = true;

                // Invoice Date
                workSheet.Cell(9, 9).Value = "Duration :";
                //workSheet.Range("AE5:AO5").Merge();
                workSheet.Cell(9, 10).Value = "";/// waiting input from developer
                workSheet.Cell(9, 10).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);

                // One Space Row
                //workSheet.Row(11).Height = 18;
                //workSheet.Cell(11, 1).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Top);

                workSheet.Cell(11, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                workSheet.Cell(11, 1).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                workSheet.Cell(11, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                workSheet.Cell(11, 4).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                workSheet.Cell(11, 8).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                workSheet.Cell(11, 8).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                workSheet.Cell(11, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                workSheet.Cell(11, 2).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                workSheet.Cell(11, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                workSheet.Cell(11, 3).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                workSheet.Cell(11, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                workSheet.Cell(11, 5).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                workSheet.Cell(11, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                workSheet.Cell(11, 6).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                workSheet.Cell(11, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                workSheet.Cell(11, 7).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                workSheet.Cell(11, 9).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                workSheet.Cell(11, 9).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                workSheet.Cell(11, 10).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                workSheet.Cell(11, 10).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);

                //workSheet.Range("A12:B12").Merge();
                workSheet.Cell(11, 1).Value = "No";

                //workSheet.Range("D12:F12").Merge();
                workSheet.Cell(11, 2).Value = "SMTS Invoice No";

                //workSheet.Range("H12:M12").Merge();
                workSheet.Cell(11, 3).Value = "A/C Code";

                //workSheet.Range("O12:AH12").Merge();
                workSheet.Cell(11, 4).Value = "Inmarsat No";

                //workSheet.Range("AJ12:AO12").Merge();
                workSheet.Cell(11, 5).Value = "Vessel Name";

                workSheet.Cell(11, 6).Value = "Service Type";

                workSheet.Cell(11, 7).Value = "Total Cost";

                workSheet.Cell(11, 8).Value = "Total Billing";

                workSheet.Cell(11, 9).Value = "Admin Fee";

                workSheet.Cell(11, 10).Value = "Status";

                workSheet.Cell(11, 1).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(11, 2).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(11, 3).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(11, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(11, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(11, 6).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(11, 7).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(11, 8).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(11, 9).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                workSheet.Cell(11, 10).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;


                workSheet.Cell(11, 1).Style.Font.FontSize = 12;
                workSheet.Cell(11, 2).Style.Font.FontSize = 12;
                workSheet.Cell(11, 3).Style.Font.FontSize = 12;
                workSheet.Cell(11, 4).Style.Font.FontSize = 12;
                workSheet.Cell(11, 5).Style.Font.FontSize = 12;
                workSheet.Cell(11, 6).Style.Font.FontSize = 12;
                workSheet.Cell(11, 7).Style.Font.FontSize = 12;
                workSheet.Cell(11, 8).Style.Font.FontSize = 12;
                workSheet.Cell(11, 9).Style.Font.FontSize = 12;
                workSheet.Cell(11, 10).Style.Font.FontSize = 12;

                // Empty Space Row
                workSheet.Row(6).Height = 6;
                workSheet.Row(10).Height = 6;

                int row = 12;
                decimal? CP_totals = 0;
                decimal? SP_totals = 0;
                int sno = 1;
                foreach (var services in lstDict)
                {
                    //workSheet.Range("A" + row + ":B" + row).Merge();
                    workSheet.Cell(row, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    workSheet.Cell(row, 1).Value = sno++;

                    workSheet.Cell(row, 2).Value = services["invoiceno"];

                    workSheet.Cell(row, 3).Value = services["accode"];

                    workSheet.Cell(row, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    workSheet.Cell(row, 4).Style.NumberFormat.Format = "@";
                    workSheet.Cell(row, 4).Value = services["inmarsatid"];

                    workSheet.Cell(row, 5).Value = services["vessel"];

                    workSheet.Cell(row, 6).Value = services["stype"];

                    //workSheet.Range("D" + row + ":F" + row).Merge();
                    //workSheet.Cell(row, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                   // workSheet.Cell(row, 7).Style.NumberFormat.Format = "@";
                    workSheet.Cell(row, 7).Value = services["cp"];

                    //workSheet.Range("H" + row + ":M" + row).Merge();
                    //workSheet.Cell(row, 8).Style.NumberFormat.Format = "@";
                    workSheet.Cell(row, 8).Value = services["bill"];

                    // workSheet.Range("O" + row + ":AH" + row).Merge();
                    workSheet.Cell(row, 9).Value = services["admin"];

                    //workSheet.Range("AJ" + row + ":AO" + row).Merge();
                    workSheet.Cell(row, 10).Value = services["status"];

                    CP_totals += Convert.ToDecimal(services["cp"]);
                    SP_totals += Convert.ToDecimal(services["bill"]);
                    row++;
                }

                workSheet.Cell(row + 1, 6).Value = "Total";
                workSheet.Cell(row + 1, 7).Value = CP_totals;

                workSheet.Cell(row + 1, 8).Value = SP_totals;

                #endregion

                // Save Work sheet into workbook
                xLWorkbook.SaveAs(spreadsheetstream);
                spreadsheetstream.Position = 0;

                //InvoiceFileName = temppath + "\\" + InvoiceFileName;

                //FileStream fileStream = new FileStream(InvoiceFileName, FileMode.Create);

                return new Tuple<Stream, string, bool>(spreadsheetstream, InvoiceFileName, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}