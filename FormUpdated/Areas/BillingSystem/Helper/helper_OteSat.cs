﻿using ClosedXML.Excel;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using EASendMail;
using FormUpdated.Areas.BillingSystem.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.ComponentModel;

namespace FormUpdated.Areas.BillingSystem
{
    public class helper_OteSat
    {
        public static string LesName { get; set; } = "OteSat";
        public int LesID { get; set; } = getLesID();

        public enum CommunicationType { Data = 1, Voice, Fax, SMS }

        public enum UOM { KB = 1, MB, GB, TB, Bytes }

        private static int CellReferenceToIndex(Cell cell)
        {
            int index = 0;
            string reference = cell.CellReference.ToString().ToUpper();
            foreach (char ch in reference)
            {
                if (Char.IsLetter(ch))
                {
                    int value = (int)ch - (int)'A';
                    index = (index == 0) ? value : ((index + 1) * 26) + value;
                }
                else
                    return index;
            }
            return index;
        }

        public static string GetColumnName(string cellReference)
        {
            // Create a regular expression to match the column name portion of the cell name.
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellReference);
            return match.Value;
        }

        public static int? GetColumnIndexFromName(string columnName)
        {

            //return columnIndex;
            string name = columnName;
            int number = 0;
            int pow = 1;
            for (int i = name.Length - 1; i >= 0; i--)
            {
                number += (name[i] - 'A' + 1) * pow;
                pow *= 26;
            }
            return number;
        }

        public static string GetDestionation(string dest)
        {
            var result = string.Empty;
            if (dest.ToLower().Contains("subscription"))
            {
                var regex = dest.Split(new string[] { "Subscription" }, StringSplitOptions.None);
                result = regex[1].TrimStart();
                result = result.TrimEnd();
            }
            return result;
        }

        public Tuple<List<int>, string> OteSat2CT(string fileName, int LesRefID)
        {
            List<int> ctMasterID = new List<int>();
            StringBuilder ErrMsg = new StringBuilder();
            try
            {
                BillingSystemEntities db = new BillingSystemEntities();
                List<Dictionary<string, dynamic>> pairs = GetDictfromExcel(fileName);
                var imsi = pairs.Select(ims => ims["imn/imsi"]).Distinct().ToList();

                int ctID = 0;

                foreach (var id in imsi)
                {
                    int LesRegID = 0;
                    int lesContractID = 0;
                    bool isNotExist = false;

                    List<Dictionary<string, dynamic>> keyValues = pairs.Where(t => t["imn/imsi"].Equals(id)).ToList();

                    List<Dictionary<string, dynamic>> dictFee = keyValues.Where(t => t["imn/imsi"].Equals(id) && t["servicetype"].Equals("Subscription Fee")).ToList();

                    // Subscribtion Fe to store FBB Master table
                    foreach (Dictionary<string, dynamic> dictRecord in dictFee)
                    {
                        if (dictRecord.Count > 0)
                        {
                            if (dictRecord.ContainsKey("servicetype"))
                            {
                                string types = dictRecord["servicetype"];
                                BCtA ctMaster = new BCtA();

                                //switch (types.ToLower())
                                //{
                                //    case "subscription fee":
                                //        //BCtA ctMaster = new BCtA();

                                //        ctMaster.BCtAFbStartDate = dictRecord["date"];
                                //        ctMaster.BCtAfkLESID = LesID;
                                //        ctMaster.BCtAfkLESCID = db.BsPtSPContracts.FirstOrDefault(t => t.fkSPID == LesID).spcID;
                                //        lesContractID = ctMaster.BCtAfkLESCID;
                                //        string imsiID = Convert.ToString(id);
                                //        try
                                //        {
                                //            var SimReg = db.BptFbSims.FirstOrDefault(t => t.BPtFBImImsi.Equals(imsiID));
                                //            if (SimReg != null)
                                //            {
                                //                ctMaster.BCtAfkSimID = SimReg.Sim_ID;
                                //                ctMaster.fkLesRefID = LesRefID;
                                //                ctMaster.fkSatTypeID = SimReg.fkSatTypeID;
                                //                ctMaster.fkEquipType = SimReg.fkEquipTypeID;
                                //                ctMaster.BCtAfkLesRegID = SimReg.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == SimReg.Sim_ID).SPLesRegID;
                                //                LesRegID = ctMaster.BCtAfkLesRegID;
                                //            }
                                //            else
                                //            {
                                //                ErrMsg.AppendFormat("The IMSI:{0} no SIM not register in our database, please check.", id).AppendLine();
                                //                isNotExist = true;
                                //                continue;
                                //            }
                                //        }
                                //        catch (Exception eror)
                                //        {
                                //            ErrMsg.AppendFormat("The IMSI:{0} getting error: {0}, please rerun the transaction.", eror.Message.ToString()).AppendLine();
                                //            isNotExist = true;
                                //            continue;
                                //        }

                                //        ctMaster.BCtAFbLesCode = null;
                                //        //ctMaster.BCMtFbCdrCode = "";
                                //        ctMaster.BCtAFbServDest = dictRecord["destination"];
                                //        ctMaster.BCtAFbServType = types;
                                //        if (dictRecord.ContainsKey("units") && dictRecord["units"] != null)
                                //            ctMaster.BCtAFbTransUsage = Convert.ToInt32(dictRecord["units"]);

                                //        ctMaster.BCtAFbUOM = dictRecord["unitstype"];
                                //        if (dictRecord.ContainsKey("amountdue") && dictRecord["amountdue"] != null)
                                //        {
                                //            try
                                //            {
                                //                ctMaster.BCtAFbRecurFee = Convert.ToDecimal(dictRecord["amountdue"]);
                                //            }
                                //            catch
                                //            {
                                //                ctMaster.BCtAFbRecurFee = 0;
                                //            }
                                //        }
                                //        //ctMaster.BCMtFbRecurFeeType ="";
                                //        //ctMaster.BCMtFbSimSingle = 0;
                                //        //ctMaster.BCMtFbSimDual = 0;
                                //        //ctMaster.BCMtFbMultiVce = 0
                                //        //ctMaster.BCMtFbContNo = "";
                                //        //ctMaster.BCMtFbCardFee = 0;
                                //        //ctMaster.BCMtFbActFee = 0;
                                //        //ctMaster.BCMtFbRgnFee = 0;
                                //        //db.BCtAs.Add(ctMaster);
                                //        //db.SaveChanges();
                                //        //ctID = ctMaster.BCtAID;
                                //        //ctMasterID.Add(ctID);
                                //        break;
                                //}

                                keyValues = pairs.Where(t => t["imn/imsi"].Equals(id) && !t["servicetype"].Equals("Subscription Fee")).ToList();

                                //foreach (Dictionary<string, dynamic> dictRecords in keyValues)
                                //{
                                //    if (dictRecords.Count > 0)
                                //    {
                                //        if (dictRecords.ContainsKey("servicetype") && dictRecords["servicetype"] != null)
                                //        {
                                //            string typess = dictRecords["servicetype"];

                                //            if (!string.IsNullOrEmpty(types))
                                //            {
                                //                BCtB ctRecord = new BCtB();
                                //                // ctRecord.BCtBfkFbID = ctID;

                                //                ctRecord.BCtBStartDate = dictRecords["date"];
                                //                //ctRecord.BsCtEndtDate
                                //                ctRecord.BCtBStartTime = dictRecords["time"];
                                //                //ctRecord.BsCtEndTime 
                                //                //ctRecord.BsCtOrigion
                                //                ctRecord.BCtBDestination = dictRecords["destination"];
                                //                string ServiceTypes = dictRecords["servicetype"];
                                //                try
                                //                {
                                //                    ctRecord.BCtBLesServTypeID = db.BsPtSPServiceDests.FirstOrDefault(t => t.BPtServTfkLesRegID == LesRegID && t.BsPtServiceTypes.ToLower().Equals(ServiceTypes.ToLower())).SPServDID;
                                //                }
                                //                catch { }
                                //                if (dictRecords.ContainsKey("units") && dictRecords["units"] != null)
                                //                {
                                //                    try
                                //                    {
                                //                        ctRecord.BCtBTransUsage = Convert.ToDecimal(dictRecords["units"]);
                                //                    }
                                //                    catch
                                //                    {
                                //                        ctRecord.BCtBTransUsage = 0;
                                //                    }
                                //                }

                                //                try
                                //                {
                                //                    string units = dictRecords["unitstype"].ToLower();
                                //                    if (units.ToLower().Contains("kbytes"))
                                //                        units = "KB";
                                //                    else if (units.ToLower().Contains("sec"))
                                //                        units = "Sec";

                                //                    ctRecord.BCtBfkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals(units)).UID;
                                //                }
                                //                catch (Exception rr)
                                //                { }

                                //                if (dictRecords.ContainsKey("amountdue") && dictRecords["amountdue"] != null)
                                //                {
                                //                    try
                                //                    {
                                //                        ctRecord.BCtBTotalUsage = Convert.ToDecimal(dictRecords["amountdue"]);
                                //                    }
                                //                    catch
                                //                    {
                                //                        ctRecord.BCtBTotalUsage = 0;
                                //                    }
                                //                }

                                //                try
                                //                {
                                //                    ctRecord.BCtBfkCommType = Convert.ToInt32(db.BsPtSPServiceDests.FirstOrDefault(t => t.SPServDID == ctRecord.BCtBLesServTypeID).ComTypeID);

                                //                }
                                //                catch (Exception err)
                                //                {
                                //                    throw err;
                                //                }

                                //                db.BCtBs.Add(ctRecord);
                                //            }
                                //        }
                                //    }
                                //}
                            }
                        }
                    }

                    // End process of ct master table 

                    //keyValues = pairs.Where(t => t["imn/imsi"].Equals(id) && !t["servicetype"].Equals("Subscription Fee")).ToList();

                    //// Call data record store to FBB common table
                    //if (!isNotExist)
                    //{
                    //    foreach (Dictionary<string, dynamic> dictRecord in keyValues)
                    //    {
                    //        if (dictRecord.Count > 0)
                    //        {
                    //            if (dictRecord.ContainsKey("servicetype") && dictRecord["servicetype"] != null)
                    //            {
                    //                string types = dictRecord["servicetype"];

                    //                if (!string.IsNullOrEmpty(types))
                    //                {
                    //                    BCtB ctRecord = new BCtB();
                    //                    ctRecord.BCtBfkFbID = ctID;

                    //                    ctRecord.BCtBStartDate = dictRecord["date"];
                    //                    //ctRecord.BsCtEndtDate
                    //                    ctRecord.BCtBStartTime = dictRecord["time"];
                    //                    //ctRecord.BsCtEndTime 
                    //                    //ctRecord.BsCtOrigion
                    //                    ctRecord.BCtBDestination = dictRecord["destination"];
                    //                    string ServiceTypes = dictRecord["servicetype"];
                    //                    try
                    //                    {
                    //                        ctRecord.BCtBLesServTypeID = db.BsPtSPServiceDests.FirstOrDefault(t => t.BPtServTfkLesRegID == LesRegID && t.BsPtServiceTypes.ToLower().Equals(ServiceTypes.ToLower())).SPServDID;
                    //                    }
                    //                    catch { }
                    //                    if (dictRecord.ContainsKey("units") && dictRecord["units"] != null)
                    //                    {
                    //                        try
                    //                        {
                    //                            ctRecord.BCtBTransUsage = Convert.ToDecimal(dictRecord["units"]);
                    //                        }
                    //                        catch
                    //                        {
                    //                            ctRecord.BCtBTransUsage = 0;
                    //                        }
                    //                    }

                    //                    try
                    //                    {
                    //                        string units = dictRecord["unitstype"].ToLower();
                    //                        if (units.ToLower().Contains("kbytes"))
                    //                            units = "KB";
                    //                        else if (units.ToLower().Contains("sec"))
                    //                            units = "Sec";

                    //                        ctRecord.BCtBfkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals(units)).UID;
                    //                    }
                    //                    catch (Exception rr)
                    //                    { }

                    //                    if (dictRecord.ContainsKey("amountdue") && dictRecord["amountdue"] != null)
                    //                    {
                    //                        try
                    //                        {
                    //                            ctRecord.BCtBTotalUsage = Convert.ToDecimal(dictRecord["amountdue"]);
                    //                        }
                    //                        catch
                    //                        {
                    //                            ctRecord.BCtBTotalUsage = 0;
                    //                        }
                    //                    }

                    //                    try
                    //                    {
                    //                        ctRecord.BCtBfkCommType = Convert.ToInt32(db.BsPtSPServiceDests.FirstOrDefault(t => t.SPServDID == ctRecord.BCtBLesServTypeID).ComTypeID);

                    //                    }
                    //                    catch (Exception err)
                    //                    {
                    //                        throw err;
                    //                    }

                    //                    db.BCtBs.Add(ctRecord);
                    //                }
                    //            }
                    //        }
                    //    }
                    //}

                    // end process of ct records
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        string errormsg = string.Empty;
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            errormsg = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors: ", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                errormsg = string.Format(" - Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        ErrMsg.AppendFormat("MSI NO:{0}, Error:{1}", id, errormsg).AppendLine();
                    }
                    catch (Exception error)
                    {
                        ErrMsg.AppendFormat("MSI NO:{0}, Error:{1}", id, error.Message.ToString() + error.StackTrace).AppendLine();
                    }
                }
                return new Tuple<List<int>, string>(ctMasterID, ErrMsg.ToString());
            }
            catch (Exception e)
            {
                return new Tuple<List<int>, string>(ctMasterID, e.Message.ToString() + e.StackTrace);
            }
        }

        public static bool IsValidTimeFormat(string input)
        {
            TimeSpan dummyOutput;
            return TimeSpan.TryParse(input, out dummyOutput);
        }

        public static int getInterval(int intrSec)
        {
            if (intrSec >= 1 && intrSec <= 29)
                return 1;
            else if (intrSec >= 30 && intrSec <= 44)
                return 2;
            else if (intrSec >= 45 && intrSec <= 59)
                return 3;
            else
                return 0;
        }

        public static decimal? getMinutes(decimal? BillIncre)
        {
            decimal? chargedSeconds = BillIncre;
            int intervalSec = Convert.ToInt32(chargedSeconds % 60);
            int interval = getInterval(intervalSec);

            if (interval == 1)
            {
                chargedSeconds = chargedSeconds - intervalSec;
                chargedSeconds = chargedSeconds + 30;
            }
            else if (interval == 2)
            {
                chargedSeconds = chargedSeconds - intervalSec;
                chargedSeconds = chargedSeconds + 45;
            }
            else if (interval == 3)
            {
                chargedSeconds = chargedSeconds - intervalSec;
                chargedSeconds = chargedSeconds + 60;
            }
            else
                chargedSeconds = chargedSeconds;

            return chargedSeconds;
        }

        public List<Dictionary<string, dynamic>> GetDictfromExcel(string filename)
        {
            List<Dictionary<string, dynamic>> lstPairs = new List<Dictionary<string, dynamic>>();
            try
            {
                DataTable dt = new DataTable();
                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(filename, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();

                    List<string> receiptField = new List<string>();
                    string ColName = string.Empty;
                    string colValue = string.Empty;

                    foreach (Cell cell in rows.ElementAt(0))
                    {
                        ColName = helper_NSSLGlobal.GetCellValue(spreadSheetDocument, cell);
                        if (ColName.StartsWith(" "))
                            ColName = ColName.Substring(1);
                        receiptField.Add(System.Text.RegularExpressions.Regex.Replace(ColName, @"\t|\n|\r", ""));
                    }

                    foreach (Row row in rows) //this will also include your header row...
                    {
                        if (row.RowIndex != 1)
                        {
                            Dictionary<string, dynamic> dictPairs = new Dictionary<string, dynamic>();

                            string[] tempData = new string[receiptField.Count];
                            int columnIndex = 0;

                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                // Gets the column index of the cell with data
                                int cellColumnIndex = (int)helper_NSSLGlobal.GetColumnIndexFromName(helper_NSSLGlobal.GetColumnName(cell.CellReference));
                                cellColumnIndex--; //zero based index
                                if (columnIndex < cellColumnIndex)
                                {
                                    do
                                    {
                                        tempData[columnIndex] = ""; //Insert blank data here;
                                        columnIndex++;
                                    }
                                    while (columnIndex < cellColumnIndex);
                                }
                                tempData[columnIndex] = helper_NSSLGlobal.GetCellValue(spreadSheetDocument, cell);

                                columnIndex++;
                            }

                            for (int i = 0, arrIndex = 0; i < receiptField.Count; i++, arrIndex++) ///row.Descendants<Cell>().Count() replaced
                            {
                                try
                                {
                                    colValue = tempData[arrIndex];
                                }
                                catch
                                {

                                }

                                if (receiptField[arrIndex].ToLower().Equals("vessel name") && string.IsNullOrEmpty(colValue))
                                {
                                    break;
                                }

                                switch (receiptField[arrIndex].ToLower())
                                {

                                    case "vessel name":
                                        dictPairs.Add("vesselname", colValue);
                                        break;
                                    case "imn / imsi":
                                        dictPairs.Add("imn/imsi", colValue);
                                        break;
                                    case "system":
                                        dictPairs.Add("system", colValue);
                                        break;
                                    case "date":
                                        double dates = Convert.ToDouble(colValue);
                                        DateTime dateTimes = DateTime.FromOADate(dates);
                                        dictPairs.Add("date", dateTimes);
                                        break;

                                    case "time":
                                        if (!string.IsNullOrEmpty(colValue))
                                        {
                                            try
                                            {
                                                TimeSpan? time = new TimeSpan();
                                                if (IsValidTimeFormat(colValue))
                                                {
                                                    TimeSpan tempTime;
                                                    if (!TimeSpan.TryParse(colValue, out tempTime))
                                                    {
                                                        time = tempTime;
                                                    }
                                                    else
                                                        time = TimeSpan.Parse(colValue);
                                                }
                                                else
                                                {
                                                    time = DateTime.FromOADate(Convert.ToDouble(colValue)).TimeOfDay;
                                                }

                                                dictPairs.Add("time", time);
                                            }
                                            catch (Exception rr)
                                            {
                                                dictPairs.Add("time", string.Empty);
                                            }
                                        }
                                        else
                                            dictPairs.Add("time", string.Empty);
                                        break;

                                    case "ocean region":
                                        dictPairs.Add("oceanregion", colValue);
                                        break;
                                    case "time zone":
                                        dictPairs.Add("timezone", colValue);
                                        break;

                                    case "service type":
                                        dictPairs.Add("servicetype", colValue.TrimStart().TrimEnd());
                                        break;
                                    case "mrn":
                                        dictPairs.Add("mrn", colValue);
                                        break;

                                    case "country":
                                        dictPairs.Add("country", colValue);
                                        break;
                                    case "destination":
                                        dictPairs.Add("destination", colValue);
                                        break;
                                    case "units":
                                        try
                                        {
                                            dictPairs.Add("units", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("units", Convert.ToDecimal(0));
                                        }
                                        break;
                                    case "units type":
                                        dictPairs.Add("unitstype", colValue);
                                        break;
                                    case "amount due (usd)":
                                        try
                                        {
                                            dictPairs.Add("amountdue", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("amountdue", Convert.ToDecimal(0));
                                        }

                                        break;
                                }
                            }

                            if (dictPairs.Count > 0)
                            {
                                if (!dictPairs["servicetype"].Contains("Discount"))
                                    lstPairs.Add(dictPairs);
                            }
                        }
                    }
                }
                return lstPairs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string RejectionCondition_OteSat(List<int> ID, string errorEsg)
        {
            System.Text.StringBuilder returnMsg = new System.Text.StringBuilder();
            returnMsg.Append(errorEsg).AppendLine();

            foreach (int id in ID)
            {
                System.Text.StringBuilder Msg = new System.Text.StringBuilder();

                string ImarSatID = string.Empty;
                int SimID = 0;
                decimal? CDR_RecuFee = 0;

                List<BCtB> lstCtRecord = new List<BCtB>();
                List<BsPtSPServiceDest> lstLES_ServiceType = new List<BsPtSPServiceDest>();

                decimal? SMTS_TotalCost = 0;
                decimal? Les_TotalCostCP = 0;
                decimal? Cus_TotalCostSP = 0;

                int? LES_ContractID = 0;
                int LES_RegID = 0;

                decimal? LES_DataPlan = 0;
                decimal? LES_OutbundleRate = 0;
                decimal? LES_RecurFee = 0;
                decimal? LES_DataRateper1MB = 0;
                decimal? LES_VoiceCellular = 0;
                decimal? LES_VoiceFixed = 0;
                decimal? LES_VoiceFBB = 0;
                decimal? LES_VoiceIridium = 0;
                decimal? LES_ISDN = 0;
                decimal? LES_SMS = 0;

                decimal? LES_InvRptDataCostTot = 0;

                int CUS_Id = 0;
                int CUS_ContractID = 0;
                int CUS_RegsID = 0;

                decimal? Cus_DataPlan = 0;
                decimal? Cus_OutbundleRate = 0;
                decimal? CUS_RecurFee = 0;
                decimal? CUS_DataRateper1MB = 0;
                decimal? CUS_VoiceCellular = 0;
                decimal? CUS_VoiceFixed = 0;
                decimal? CUS_VoiceFBB = 0;
                decimal? CUS_VoiceIridium = 0;
                decimal? CUS_ISDN = 0;
                decimal? CUS_SMS = 0;

                DateTime? LES_ContrRegStrDate = null;
                DateTime? LES_ContrRegEndDate = null;

                DateTime? LES_ContrBarStrDate = null;
                DateTime? LES_ContrBarEndDate = null;

                DateTime? LES_ContrSusStrDate = null;
                DateTime? LES_ContrSusEndDate = null;

                DateTime? LES_ContrLayoutStrDate = null;
                DateTime? LES_ContrLayoutEndDate = null;

                DateTime? CUS_ContrRegStrDate = null;
                DateTime? CUS_ContrRegEndDate = null;

                DateTime? CUS_ContrBarStrDate = null;
                DateTime? CUS_ContrBarEndDate = null;

                DateTime? CUS_ContrSusStrDate = null;
                DateTime? CUS_ContrSusEndDate = null;

                DateTime? CUS_ContrLayoutStrDate = null;
                DateTime? CUS_ContrLayoutEndDate = null;

                DateTime? CDR_StartDate = null;
                DateTime? CDR_EndDate = null;

                bool is_LESGBPlan = false;
                bool is_CUSGBPlan = false;
                bool isNoTransData = false;

                int ServTypeID = 0;
                int ServTypeMobile = 0;
                int ServTypeVce2Fixed = 0;
                int ServTypeVce2FBB = 0;
                int LES_ServTypeVce2Iridium = 0;
                int ServTypeVce2ISDN = 0;
                int ServTypeSMS = 0;

                int CUS_ServTypeID = 0;
                int CUS_ServTypeMobile = 0;
                int CUS_ServTypeVce2Fixed = 0;
                int CUS_ServTypeVce2FBB = 0;
                int CUS_ServTypeVce2Iridium = 0;
                int CUS_ServTypeVce2ISDN = 0;
                int CUS_ServTypeSMS = 0;

                //using (BillingSystemEntities db = new BillingSystemEntities())
                //{
                //    try
                //    {
                //        lstCtRecord = db.BCtBs.Where(t => t.BCtBfkFbID == id).ToList();

                //        var CT_Fbs = db.BCtAs.Where(t => t.BCtAID.Equals(id)).FirstOrDefault();

                //        SimID = CT_Fbs.BCtAfkSimID;
                //        CDR_RecuFee = CT_Fbs.BCtAFbRecurFee;
                //        ImarSatID = db.BptFbSims.FirstOrDefault(t => t.Sim_ID == SimID).BPtFBimMsisdn;

                //        try
                //        {
                //            if (lstCtRecord.Count > 0)
                //            {
                //                CDR_StartDate = lstCtRecord.Min(t => t.BCtBStartDate);
                //                CDR_EndDate = lstCtRecord.Max(t => t.BCtBStartDate);
                //            }
                //            else
                //                isNoTransData = true;
                //        }
                //        catch { }

                //        // getting Customer price details ---------------------------------------------------------------------------------------------
                //        #region Cus
                //        var cusRegs = db.BsPtCusRegs_.FirstOrDefault(t => t.fkSimID == SimID);
                //        if (cusRegs != null)
                //        {
                //            CUS_Id = Convert.ToInt32(cusRegs.fkCusID);
                //            CUS_ContractID = Convert.ToInt32(cusRegs.fkCuscID);
                //            CUS_RegsID = Convert.ToInt32(cusRegs.Cus_RegID);

                //            CUS_RecurFee = cusRegs.BPtCusRgnFbRecurFee;

                //            Cus_DataPlan = Convert.ToDecimal(cusRegs.BPtCusRgnFBDataPlanIn);

                //            CUS_DataRateper1MB = cusRegs.BPtCusRgnFbInbundleDataRate;
                //            Cus_OutbundleRate = cusRegs.BPtCusRgnFbOutBundleDataRate;

                //            CUS_ContrRegStrDate = cusRegs.BsPtCusRegStartdate;
                //            CUS_ContrRegEndDate = cusRegs.BsPtCusRegEnd;

                //            CUS_ContrBarStrDate = cusRegs.BsPtCusRegFBBarDate;
                //            CUS_ContrBarEndDate = cusRegs.BsPtCusRegFbBarDateLifted;

                //            CUS_ContrSusStrDate = cusRegs.BsPtCusRegFBSusDate;
                //            CUS_ContrSusEndDate = cusRegs.BsPtCusRegFBSusDateLifted;

                //            CUS_ContrLayoutStrDate = cusRegs.BsPtCusRegFBLayUpDate;
                //            CUS_ContrLayoutEndDate = cusRegs.BsPtCusRegFBLayUpDateLifted;

                //            string unit = cusRegs.BsMtUnit.Units;
                //            if (unit.Contains("GB"))
                //                is_CUSGBPlan = true;
                //            else
                //                is_CUSGBPlan = false;
                //        }
                //        else
                //        {
                //            Msg.AppendFormat("This msi {0} id not registered in customer registered, please check.<br />", ImarSatID).AppendLine();
                //        }

                //        List<BsPtCusServiceDest> lstSP = db.BsPtCusServiceDests.Where(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID).ToList();

                //        try
                //        {
                //            CUS_VoiceCellular = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                //                                                     t.BsPtCusServiceTypes.Equals("Voice  to Cellular")).BsPtCusServiceSellerPrice;
                //        }
                //        catch { CUS_VoiceCellular = 0; }

                //        try
                //        {
                //            CUS_VoiceFixed = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                //                                                  t.BsPtCusServiceTypes.Equals("Voice  to Fixed")).BsPtCusServiceSellerPrice;
                //        }
                //        catch { CUS_VoiceFixed = 0; }

                //        try
                //        {
                //            CUS_VoiceFBB = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                //                                                t.BsPtCusServiceTypes.Equals("Voice  to FleetBroadband")).BsPtCusServiceSellerPrice;
                //        }
                //        catch { CUS_VoiceFBB = 0; }

                //        try
                //        {
                //            CUS_VoiceIridium = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                //                                                    t.BsPtCusServiceTypes.Equals("Voice  to Iridium")).BsPtCusServiceSellerPrice;
                //        }
                //        catch { CUS_VoiceIridium = 0; }

                //        try
                //        {
                //            CUS_ISDN = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                //                                            t.BsPtCusServiceTypes.Equals("ISDN")).BsPtCusServiceSellerPrice;
                //        }
                //        catch { CUS_ISDN = 0; }

                //        try
                //        {
                //            CUS_SMS = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                //                                           t.BsPtCusServiceTypes.Equals("SMS")).BsPtCusServiceSellerPrice;
                //        }
                //        catch { CUS_SMS = 0; }
                //        #endregion
                //        //------------------------------------------------------------------------------------------------------------------------------

                //        #region LES Details
                //        // Getting LES price details from LES Reg ---------------------------------------------------------------------------------------

                //        var LES_Services = db.BsPtSPServiceDests.ToList();
                //        try
                //        {

                //            try
                //            {
                //                //var Les_Reg = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSPID == LesID && t.fkSimID == SimID);
                //                var Les_Reg = db.BsPtSPLesRegs.FirstOrDefault(t => t.SPLesRegID == CT_Fbs.BCtAfkLesRegID);

                //                LES_RegID = Les_Reg.SPLesRegID;
                //                LES_ContractID = Les_Reg.fkspcID;

                //                LES_RecurFee = Les_Reg.BPtLesRgnFbRecurFee;
                //                LES_DataPlan = Convert.ToDecimal(Les_Reg.BPtLesRgnFBDataPlanIn);
                //                LES_DataRateper1MB = Les_Reg.BPtLesRgnFbInbundleDataRate;
                //                LES_OutbundleRate = Les_Reg.BPtLesRgnFbOutBundleDataRate;

                //                LES_ContrRegStrDate = Les_Reg.BsPtlesRegStartdate;
                //                LES_ContrRegEndDate = Les_Reg.BsPtlesRegEnd;

                //                LES_ContrBarStrDate = Les_Reg.BsPtLesRegFBBarDate;
                //                LES_ContrBarEndDate = Les_Reg.BsPtLesRegFbBarDateLifted;

                //                LES_ContrSusStrDate = Les_Reg.BsPtLesRegFBSusDate;
                //                LES_ContrSusEndDate = Les_Reg.BsPtLesRegFBSusDateLifted;

                //                LES_ContrLayoutStrDate = Les_Reg.BsPtLesRegFBLayUpDate;
                //                LES_ContrLayoutEndDate = Les_Reg.BsPtLesRegFBLayUpDateLifted;

                //                string unit = Les_Reg.BsMtUnit.Units;
                //                if (unit.Contains("GB"))
                //                    is_LESGBPlan = true;
                //                else
                //                    is_LESGBPlan = false;
                //            }
                //            catch { LES_RecurFee = 0; }

                //            lstLES_ServiceType = (from ls in LES_Services
                //                                  where ls.fkSpID == LesID && ls.BPtServTfkLesRegID == LES_RegID
                //                                  select ls).ToList();

                //            try
                //            {
                //                LES_VoiceCellular = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                //                                                         t.BsPtServiceTypes.Equals("Voice  to Cellular")).BsPtServiceCostPrice;
                //            }
                //            catch { LES_VoiceCellular = 0; }

                //            try
                //            {
                //                LES_VoiceFixed = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                //                                                      t.BsPtServiceTypes.Equals("Voice  to Fixed")).BsPtServiceCostPrice;
                //            }
                //            catch { LES_VoiceFixed = 0; }

                //            try
                //            {
                //                LES_VoiceFBB = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                //                                                    t.BsPtServiceTypes.Equals("Voice  to FleetBroadband")).BsPtServiceCostPrice;
                //            }
                //            catch { LES_VoiceFBB = 0; }

                //            try
                //            {
                //                LES_VoiceIridium = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                //                                                        t.BsPtServiceTypes.Equals("Voice  to Iridium")).BsPtServiceCostPrice;
                //            }
                //            catch { LES_VoiceIridium = 0; }

                //            try
                //            {
                //                LES_ISDN = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                //                                                t.BsPtServiceTypes.Equals("ISDN")).BsPtServiceCostPrice;
                //            }
                //            catch { LES_ISDN = 0; }

                //            try
                //            {
                //                LES_SMS = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                //                                               t.BsPtServiceTypes.Equals("SMS")).BsPtServiceCostPrice;
                //            }
                //            catch { LES_SMS = 0; }
                //        }
                //        catch (Exception e)
                //        {
                //            Msg.AppendFormat("Error:{0}", e.Message.ToString());
                //        }
                //        //-------------------------------------------------------------------------------------------------------------------------------
                //        #endregion

                //        // Data ------------------------------------------------------------------------------------------------------------------------------
                //        #region Standard IP
                //        try
                //        {
                //            ServTypeID = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals("Standard IP")).SPServDID;
                //        }
                //        catch
                //        {
                //            ServTypeID = 0;
                //            Msg.Append("The Standard IP is Registed in the LES Service Description<br />.").AppendLine();
                //        }

                //        try
                //        {
                //            CUS_ServTypeID = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Standard IP")).CusServDID;
                //        }
                //        catch
                //        {
                //            CUS_ServTypeID = 0;
                //            Msg.Append("The Standard IP is Registed in the CUS Service Description<br />.").AppendLine();
                //        }

                //        decimal? DataUnits_CDR = lstCtRecord.AsEnumerable()
                //                                      .Where(t => t.BCtBfkCommType == 1 && t.BCtBLesServTypeID == ServTypeID && (t.BCtBfkFbID.Equals(id)))
                //                                      .GroupBy(t => t.BCtBfkFbID == id)
                //                                      .Select(t => t.Sum(s => s.BCtBTransUsage)).FirstOrDefault();
                //        DataUnits_CDR = Math.Round(Convert.ToDecimal(DataUnits_CDR), 2, MidpointRounding.AwayFromZero);

                //        decimal? CDR_DataCP = lstCtRecord.AsEnumerable()
                //                                                .Where(t => t.BCtBfkCommType == 1 && t.BCtBLesServTypeID == ServTypeID &&
                //                                                        (t.BCtBfkFbID.Equals(id)))
                //                                                .GroupBy(t => t.BCtBfkFbID == id)
                //                                                .Select(t => t.Sum(s => s.BCtBTotalUsage)).FirstOrDefault();
                //        CDR_DataCP = Math.Round(Convert.ToDecimal(CDR_DataCP), 2, MidpointRounding.AwayFromZero);

                //        SMTS_TotalCost += CDR_DataCP;
                //        #endregion
                //        //---- End Data Process

                //        #region Basic voice - Mobile
                //        // <--------------------------- Basic voice - Mobile  ----------------------------->
                //        ServTypeMobile = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals("Voice  to Cellular")).SPServDID;
                //        CUS_ServTypeMobile = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Voice  to Cellular")).CusServDID;

                //        decimal? CDR_VceCellularUintsCT = lstCtRecord.AsEnumerable()
                //                                                    .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeMobile &&
                //                                                        (t.BCtBfkFbID.Equals(id)))
                //                                                    .GroupBy(t => t.BCtBfkFbID.Equals(id))
                //                                                    .Select(t => t.Sum(s => s.BCtBTransUsage)).FirstOrDefault();

                //        if (CDR_VceCellularUintsCT != null)
                //            CDR_VceCellularUintsCT = Math.Round(Convert.ToDecimal(CDR_VceCellularUintsCT), 2, MidpointRounding.AwayFromZero);

                //        decimal? CDR_VceCellularCostPriceCT = lstCtRecord.AsEnumerable()
                //                                                    .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeMobile &&
                //                                                        (t.BCtBfkFbID.Equals(id)))
                //                                                    .GroupBy(t => t.BCtBfkFbID.Equals(id))
                //                                                    .Select(t => t.Sum(s => s.BCtBTotalUsage)).FirstOrDefault();

                //        if (CDR_VceCellularCostPriceCT != null)
                //            CDR_VceCellularCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceCellularCostPriceCT), 2, MidpointRounding.AwayFromZero);

                //        SMTS_TotalCost += CDR_VceCellularCostPriceCT;
                //        // <--------------------------- End Basic voice -FBB/Cellular  ----------------------------->
                //        #endregion

                //        #region Basic Voice  to Fixed
                //        // <--------------------------- Basic Voice  to Fixed  ----------------------------->
                //        ServTypeVce2Fixed = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals("Voice  to Fixed")).SPServDID;
                //        CUS_ServTypeVce2Fixed = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Voice  to Fixed")).CusServDID;

                //        decimal? CDR_VceFixedUintsCT = lstCtRecord.AsEnumerable()
                //                                                    .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeVce2Fixed &&
                //                                                        (t.BCtBfkFbID.Equals(id)))
                //                                                    .GroupBy(t => t.BCtBfkFbID.Equals(id))
                //                                                    .Select(t => t.Sum(s => s.BCtBTransUsage)).FirstOrDefault();

                //        if (CDR_VceFixedUintsCT != null)
                //            CDR_VceFixedUintsCT = Math.Round(Convert.ToDecimal(CDR_VceFixedUintsCT), 2, MidpointRounding.AwayFromZero);

                //        decimal? CDR_VceFixedCostPriceCT = lstCtRecord.AsEnumerable()
                //                                                    .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeVce2Fixed &&
                //                                                        (t.BCtBfkFbID.Equals(id)))
                //                                                    .GroupBy(t => t.BCtBfkFbID.Equals(id))
                //                                                    .Select(t => t.Sum(s => s.BCtBTotalUsage)).FirstOrDefault();

                //        if (CDR_VceFixedCostPriceCT != null)
                //            CDR_VceFixedCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceFixedCostPriceCT), 2, MidpointRounding.AwayFromZero);

                //        SMTS_TotalCost += CDR_VceFixedCostPriceCT;
                //        // <--------------------------- End Basic Voice  to Fixed  ----------------------------->
                //        #endregion

                //        #region Voice to FleetBroadband
                //        // <--------------------------- Voice to FleetBroadband  ----------------------------->

                //        try
                //        {
                //            ServTypeVce2FBB = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals("Voice to FleetBroadband")).SPServDID;
                //        }
                //        catch
                //        {
                //            ServTypeVce2FBB = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals("Voice  to FleetBroadband")).SPServDID;
                //        }

                //        try
                //        {
                //            CUS_ServTypeVce2FBB = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Voice to FleetBroadband")).CusServDID;
                //        }
                //        catch
                //        {
                //            CUS_ServTypeVce2FBB = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Voice  to FleetBroadband")).CusServDID;
                //        }

                //        decimal? CDR_VceFBBUintsCT = lstCtRecord.AsEnumerable()
                //                                                    .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeVce2FBB &&
                //                                                        (t.BCtBfkFbID.Equals(id)))
                //                                                    .GroupBy(t => t.BCtBfkFbID.Equals(id))
                //                                                    .Select(t => t.Sum(s => s.BCtBTransUsage)).FirstOrDefault();

                //        if (CDR_VceFBBUintsCT != null)
                //            CDR_VceFBBUintsCT = Math.Round(Convert.ToDecimal(CDR_VceFBBUintsCT), 2, MidpointRounding.AwayFromZero);

                //        decimal? CDR_VceFBBCostPriceCT = lstCtRecord.AsEnumerable()
                //                                                    .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeVce2FBB &&
                //                                                        (t.BCtBfkFbID.Equals(id)))
                //                                                    .GroupBy(t => t.BCtBfkFbID.Equals(id))
                //                                                    .Select(t => t.Sum(s => s.BCtBTotalUsage)).FirstOrDefault();

                //        if (CDR_VceFBBCostPriceCT != null)
                //            CDR_VceFBBCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceFBBCostPriceCT), 2, MidpointRounding.AwayFromZero);

                //        SMTS_TotalCost += CDR_VceFBBCostPriceCT;
                //        // <--------------------------- End Voice to FleetBroadband  ----------------------------->
                //        #endregion

                //        #region Voice  to Iridium
                //        // <--------------------------- Voice  to Iridium  ----------------------------->
                //        LES_ServTypeVce2Iridium = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals("Voice  to Iridium")).SPServDID;
                //        CUS_ServTypeVce2Iridium = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("Voice  to Iridium")).CusServDID;

                //        decimal? CDR_VceIridiumUintsCT = lstCtRecord.AsEnumerable()
                //                                                    .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == LES_ServTypeVce2Iridium &&
                //                                                        (t.BCtBfkFbID.Equals(id)))
                //                                                    .GroupBy(t => t.BCtBfkFbID.Equals(id))
                //                                                    .Select(t => t.Sum(s => s.BCtBTransUsage)).FirstOrDefault();

                //        if (CDR_VceIridiumUintsCT != null)
                //            CDR_VceIridiumUintsCT = Math.Round(Convert.ToDecimal(CDR_VceIridiumUintsCT), 2, MidpointRounding.AwayFromZero);

                //        decimal? CDR_VceIridiumCostPriceCT = lstCtRecord.AsEnumerable()
                //                                                    .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == LES_ServTypeVce2Iridium &&
                //                                                        (t.BCtBfkFbID.Equals(id)))
                //                                                    .GroupBy(t => t.BCtBfkFbID.Equals(id))
                //                                                    .Select(t => t.Sum(s => s.BCtBTotalUsage)).FirstOrDefault();

                //        if (CDR_VceIridiumCostPriceCT != null)
                //            CDR_VceIridiumCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceIridiumCostPriceCT), 2, MidpointRounding.AwayFromZero);

                //        SMTS_TotalCost += CDR_VceIridiumCostPriceCT;
                //        // <--------------------------- End Voice  to Iridium  ----------------------------->
                //        #endregion

                //        #region ISDN
                //        // <--------------------------- ISDN  ----------------------------->
                //        ServTypeVce2ISDN = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals("ISDN")).SPServDID;
                //        CUS_ServTypeVce2ISDN = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("ISDN")).CusServDID;

                //        decimal? CDR_ISDNUintsCT = lstCtRecord.AsEnumerable()
                //                                                    .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeVce2ISDN &&
                //                                                        (t.BCtBfkFbID.Equals(id)))
                //                                                    .GroupBy(t => t.BCtBfkFbID.Equals(id))
                //                                                    .Select(t => t.Sum(s => s.BCtBTransUsage)).FirstOrDefault();

                //        if (CDR_ISDNUintsCT != null)
                //            CDR_ISDNUintsCT = Math.Round(Convert.ToDecimal(CDR_ISDNUintsCT), 2, MidpointRounding.AwayFromZero);

                //        decimal? CDR_ISDNCostPriceCT = lstCtRecord.AsEnumerable()
                //                                                    .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeVce2ISDN &&
                //                                                        (t.BCtBfkFbID.Equals(id)))
                //                                                    .GroupBy(t => t.BCtBfkFbID.Equals(id))
                //                                                    .Select(t => t.Sum(s => s.BCtBTotalUsage)).FirstOrDefault();

                //        if (CDR_ISDNCostPriceCT != null)
                //            CDR_ISDNCostPriceCT = Math.Round(Convert.ToDecimal(CDR_ISDNCostPriceCT), 2, MidpointRounding.AwayFromZero);

                //        SMTS_TotalCost += CDR_ISDNCostPriceCT;
                //        // <--------------------------- End ISDN  ----------------------------->
                //        #endregion

                //        #region SMS
                //        // <--------------------------- ISDN  ----------------------------->
                //        ServTypeSMS = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals("SMS")).SPServDID;
                //        CUS_ServTypeSMS = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("SMS")).CusServDID;

                //        decimal? CDR_SMSUintsCT = lstCtRecord.AsEnumerable()
                //                                                    .Where(t => t.BCtBfkCommType == 4 && t.BCtBLesServTypeID == ServTypeSMS &&
                //                                                        (t.BCtBfkFbID.Equals(id)))
                //                                                    .GroupBy(t => t.BCtBfkFbID.Equals(id))
                //                                                    .Select(t => t.Sum(s => s.BCtBTransUsage)).FirstOrDefault();

                //        if (CDR_SMSUintsCT != null)
                //            CDR_SMSUintsCT = Math.Round(Convert.ToDecimal(CDR_SMSUintsCT), 2, MidpointRounding.AwayFromZero);

                //        decimal? CDR_SMSCostPriceCT = lstCtRecord.AsEnumerable()
                //                                                    .Where(t => t.BCtBfkCommType == 4 && t.BCtBLesServTypeID == ServTypeSMS &&
                //                                                        (t.BCtBfkFbID.Equals(id)))
                //                                                    .GroupBy(t => t.BCtBfkFbID.Equals(id))
                //                                                    .Select(t => t.Sum(s => s.BCtBTotalUsage)).FirstOrDefault();

                //        if (CDR_SMSCostPriceCT != null)
                //            CDR_SMSCostPriceCT = Math.Round(Convert.ToDecimal(CDR_SMSCostPriceCT), 2, MidpointRounding.AwayFromZero);

                //        SMTS_TotalCost += CDR_SMSCostPriceCT;
                //        // <--------------------------- End ISDN  ----------------------------->
                //        #endregion

                //        bool isRecurFeeMached = false;
                //        bool isDataMatched = false;
                //        bool isVoiceCellurlarMatched = false;
                //        bool isVoiceFixedMatched = false;
                //        bool isVoiceFbbMatched = false;
                //        bool isVoiceIridiumMatched = false;
                //        bool isISDNMatched = false;
                //        bool isSMSMatched = false;

                //        bool isOutbundleUsed = false;
                //        decimal? OutbundleUsedData = 0;
                //        decimal? CDR_OutbundleRate = 0;

                //        Les_TotalCostCP += LES_RecurFee;
                //        SMTS_TotalCost += CDR_RecuFee;

                //        if ((CDR_RecuFee < LES_RecurFee || CDR_RecuFee.Equals(LES_RecurFee))
                //                              && CDR_RecuFee < CUS_RecurFee)
                //        {
                //            isRecurFeeMached = true;
                //        }
                //        else
                //        {
                //            isRecurFeeMached = false;
                //            Msg.AppendFormat("This Inmarsat id {0}: Recuring Feee is not match.<br />", ImarSatID).AppendLine();
                //            Msg.AppendFormat("Recuring Feee from LES CDR Price:{0}.<br />", CDR_RecuFee).AppendLine();
                //            Msg.AppendFormat("Recuring Feee from LES Reg CostPrice:{0}.<br />", LES_RecurFee).AppendLine();
                //            Msg.AppendFormat("Recuring Feee from Cus Reg SellerPrice:{0}.<br />", CUS_RecurFee).AppendLine();
                //            returnMsg.AppendFormat("This Inmarsat id {0}: Recuring Feee is not match.<br />", ImarSatID).AppendLine();
                //        }

                //        if (DataUnits_CDR != null)
                //        {
                //            long data = Convert.ToInt64(Convert.ToDecimal(DataUnits_CDR));
                //            decimal? CDR_DataTotalUsageMB = (decimal)Converters.ConvertKilobytesToMegabytes(data);

                //            if (CDR_DataTotalUsageMB != null)
                //                CDR_DataTotalUsageMB = Math.Round(Convert.ToDecimal(CDR_DataTotalUsageMB), 2, MidpointRounding.AwayFromZero);

                //            if (is_LESGBPlan)
                //            {
                //                LES_DataPlan = LES_DataPlan * 1024;
                //            }

                //            if (LES_DataPlan < CDR_DataTotalUsageMB)
                //            {
                //                isOutbundleUsed = true;
                //                OutbundleUsedData = CDR_DataTotalUsageMB - LES_DataPlan;
                //                OutbundleUsedData = Math.Round(Convert.ToDecimal(OutbundleUsedData), 2, MidpointRounding.AwayFromZero);
                //            }

                //            decimal? LES_DataCP = CDR_DataTotalUsageMB * LES_DataRateper1MB;
                //            decimal? Cus_DataSP = CDR_DataTotalUsageMB * CUS_DataRateper1MB;
                //            decimal? LES_OutbundleDataCP = OutbundleUsedData * LES_OutbundleRate;
                //            decimal? Cus_OutbundleDataSP = OutbundleUsedData * Cus_OutbundleRate;

                //            if (Cus_DataSP != null)
                //                Cus_DataSP = Math.Round(Convert.ToDecimal(Cus_DataSP), 2, MidpointRounding.AwayFromZero);

                //            if (LES_DataCP != null)
                //                LES_DataCP = Math.Round(Convert.ToDecimal(LES_DataCP), 2, MidpointRounding.AwayFromZero);

                //            if (LES_OutbundleDataCP != null)
                //                LES_OutbundleDataCP = Math.Round(Convert.ToDecimal(LES_OutbundleDataCP), 2, MidpointRounding.AwayFromZero);

                //            if (Cus_OutbundleDataSP != null)
                //                Cus_OutbundleDataSP = Math.Round(Convert.ToDecimal(Cus_OutbundleDataSP), 2, MidpointRounding.AwayFromZero);

                //            if (isOutbundleUsed)
                //            {
                //                CDR_OutbundleRate = CDR_DataCP - CDR_RecuFee;
                //                CDR_OutbundleRate = Math.Round(Convert.ToDecimal(CDR_OutbundleRate), 2, MidpointRounding.AwayFromZero);

                //                //if ((CDR_DataCP < LES_OutbundleDataCP || CDR_DataCP.Equals(LES_OutbundleDataCP)) && (CDR_DataCP < Cus_OutbundleDataSP))
                //                if ((CDR_OutbundleRate < LES_OutbundleDataCP || CDR_OutbundleRate.Equals(LES_OutbundleDataCP)) && (CDR_OutbundleRate < Cus_OutbundleDataSP))
                //                {
                //                    isDataMatched = true;
                //                    Les_TotalCostCP += LES_OutbundleDataCP;
                //                    Cus_TotalCostSP += Cus_OutbundleDataSP;
                //                }
                //                else
                //                {
                //                    isDataMatched = false;
                //                    Msg.AppendFormat("This Inmarsat id {0}: Data of Background IP data rate is not match.<br />", ImarSatID).AppendLine();
                //                    Msg.AppendFormat("Outbundle Data Rate from LES CDR Price:{0}.<br />", CDR_OutbundleRate).AppendLine();
                //                    Msg.AppendFormat("Outbundle Data Rate from LES Reg CostPrice:{0}.<br />", LES_OutbundleDataCP).AppendLine();
                //                    Msg.AppendFormat("Outbundle Data Rate from Cus Reg SellerPrice:{0}.<br />", Cus_OutbundleDataSP).AppendLine();
                //                    returnMsg.AppendFormat("This Inmarsat id {0}: Data of Background IP data rate is not match.<br />", ImarSatID).AppendLine();
                //                    Les_TotalCostCP += LES_OutbundleDataCP;
                //                    Cus_TotalCostSP += Cus_OutbundleDataSP;

                //                }
                //            }
                //            else
                //            {
                //                //Les_TotalCostCP += LES_DataCP;
                //                Cus_TotalCostSP += Cus_DataSP;
                //                if (CDR_DataCP == 0 || Cus_DataSP == 0)
                //                {
                //                    isDataMatched = true;
                //                }
                //                else
                //                {
                //                    isDataMatched = false;
                //                    Msg.AppendFormat("This Inmarsat id {0}: Data of Background IP data rate is not match.<br />", ImarSatID).AppendLine();
                //                    Msg.AppendFormat("Data Rate from LES CDR Price:{0}.<br />", CDR_DataCP).AppendLine();
                //                    Msg.AppendFormat("Data Rate from LES Reg CostPrice:{0}.<br />", LES_DataCP).AppendLine();
                //                    Msg.AppendFormat("Data Rate from Cus Reg SellerPrice:{0}.<br />", Cus_DataSP).AppendLine();
                //                    returnMsg.AppendFormat("This Inmarsat id {0}: Data of Background IP data rate is not match.<br />", ImarSatID).AppendLine();
                //                }
                //            }
                //        }
                //        else
                //            isDataMatched = true;

                //        if (CDR_VceCellularUintsCT != null)
                //        {
                //            decimal V2CellTotalUnitsCT = 0;

                //            var v2cCT = lstCtRecord.AsEnumerable().Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeMobile && (t.BCtBfkFbID.Equals(id)));

                //            foreach (var ct in v2cCT)
                //            {
                //                if (ct.BCtBTransUsage != null)
                //                {
                //                    V2CellTotalUnitsCT += Convert.ToDecimal(getMinutes(ct.BCtBTransUsage));
                //                }
                //            }

                //            double lngUnits = Convert.ToDouble(V2CellTotalUnitsCT);
                //            decimal? CDR_VceCellulartotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                //            decimal? LES_VceCellCPperMin = CDR_VceCellulartotalUnits_Minutes * LES_VoiceCellular;
                //            decimal? CUS_VceCellSPperMin = CDR_VceCellulartotalUnits_Minutes * CUS_VoiceCellular;

                //            if (CDR_VceCellulartotalUnits_Minutes != null)
                //                CDR_VceCellulartotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceCellulartotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);



                //            if (LES_VceCellCPperMin != null)
                //                LES_VceCellCPperMin = Math.Round(Convert.ToDecimal(LES_VceCellCPperMin), 2, MidpointRounding.AwayFromZero);

                //            if (CUS_VceCellSPperMin != null)
                //                CUS_VceCellSPperMin = Math.Round(Convert.ToDecimal(CUS_VceCellSPperMin), 2, MidpointRounding.AwayFromZero);

                //            Les_TotalCostCP += LES_VceCellCPperMin;
                //            Cus_TotalCostSP += CUS_VceCellSPperMin;

                //            if ((LES_VceCellCPperMin.Equals(CDR_VceCellularCostPriceCT) || LES_VceCellCPperMin > CDR_VceCellularCostPriceCT)
                //                && (CDR_VceCellularCostPriceCT < CUS_VceCellSPperMin))
                //            {
                //                isVoiceCellurlarMatched = true;
                //            }
                //            else
                //            {
                //                isVoiceCellurlarMatched = false;
                //                Msg.AppendFormat("This Inmarsat {0}: Voice to Cellular data rate is not match.<br />", ImarSatID).AppendLine();
                //                Msg.AppendFormat("Voice to Cellular Rate from LES CDR Price:{0}.<br />", CDR_VceCellularCostPriceCT).AppendLine();
                //                Msg.AppendFormat("Voice to Cellular Rate from LES Reg CostPrice:{0}.<br />", LES_VceCellCPperMin).AppendLine();
                //                Msg.AppendFormat("Voice to Cellular Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VceCellSPperMin).AppendLine();
                //                returnMsg.AppendFormat("This Inmarsat {0}: Voice to Cellular data rate is not match.<br />", ImarSatID).AppendLine();
                //            }
                //        }
                //        else
                //            isVoiceCellurlarMatched = true;

                //        if (CDR_VceFixedUintsCT != null)
                //        {
                //            decimal V2FixedTotalUnitsCT = 0;

                //            var v2FCT = lstCtRecord.AsEnumerable().Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeVce2Fixed && (t.BCtBfkFbID.Equals(id)));

                //            foreach (var ct in v2FCT)
                //            {
                //                if (ct.BCtBTransUsage != null)
                //                {
                //                    V2FixedTotalUnitsCT += Convert.ToDecimal(getMinutes(ct.BCtBTransUsage));
                //                }
                //            }

                //            double lngUnits = Convert.ToDouble(V2FixedTotalUnitsCT);
                //            decimal? CDR_VceFixedTotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                //            if (CDR_VceFixedTotalUnits_Minutes != null)
                //                CDR_VceFixedTotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceFixedTotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

                //            decimal? LES_VceFixedCPperMin = CDR_VceFixedTotalUnits_Minutes * LES_VoiceFixed;
                //            decimal? CUS_VceFixedSPperMin = CDR_VceFixedTotalUnits_Minutes * CUS_VoiceFixed;

                //            if (LES_VceFixedCPperMin != null)
                //                LES_VceFixedCPperMin = Math.Round(Convert.ToDecimal(LES_VceFixedCPperMin), 2, MidpointRounding.AwayFromZero);

                //            if (CUS_VceFixedSPperMin != null)
                //                CUS_VceFixedSPperMin = Math.Round(Convert.ToDecimal(CUS_VceFixedSPperMin), 2, MidpointRounding.AwayFromZero);

                //            Les_TotalCostCP += LES_VceFixedCPperMin;
                //            Cus_TotalCostSP += CUS_VceFixedSPperMin;

                //            if ((LES_VceFixedCPperMin.Equals(CDR_VceFixedCostPriceCT) || LES_VceFixedCPperMin > CDR_VceFixedCostPriceCT)
                //                && (CDR_VceFixedCostPriceCT < CUS_VceFixedSPperMin))
                //            {
                //                isVoiceFixedMatched = true;
                //            }
                //            else
                //            {
                //                isVoiceFixedMatched = false;
                //                Msg.AppendFormat("This Inmarsat {0}: Voice to Fixed data rate is not match.<br />", ImarSatID).AppendLine();
                //                Msg.AppendFormat("Voice to Fixed Rate from LES CDR Price:{0}.<br />", CDR_VceFixedCostPriceCT).AppendLine();
                //                Msg.AppendFormat("Voice to Fixed Rate from LES Reg CostPrice:{0}.<br />", LES_VceFixedCPperMin).AppendLine();
                //                Msg.AppendFormat("Voice to Fixed Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VceFixedSPperMin).AppendLine();
                //                returnMsg.AppendFormat("This Inmarsat {0}: Voice to Fixed data rate is not match.<br />", ImarSatID).AppendLine();
                //            }
                //        }
                //        else
                //            isVoiceFixedMatched = true;

                //        if (CDR_VceFBBUintsCT != null)
                //        {
                //            decimal V2FBBTotalUnitsCT = 0;

                //            var v2FBBCT = lstCtRecord.AsEnumerable().Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeVce2FBB && (t.BCtBfkFbID.Equals(id)));

                //            foreach (var ct in v2FBBCT)
                //            {
                //                if (ct.BCtBTransUsage != null)
                //                {
                //                    V2FBBTotalUnitsCT += Convert.ToDecimal(getMinutes(ct.BCtBTransUsage));
                //                }
                //            }

                //            double lngUnits = Convert.ToDouble(V2FBBTotalUnitsCT);
                //            decimal? CDR_VceFBBTotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                //            if (CDR_VceFBBTotalUnits_Minutes != null)
                //                CDR_VceFBBTotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceFBBTotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

                //            decimal? LES_VceFBBCPperMin = CDR_VceFBBTotalUnits_Minutes * LES_VoiceFBB;
                //            decimal? CUS_VceFBBSPperMin = CDR_VceFBBTotalUnits_Minutes * CUS_VoiceFBB;

                //            if (LES_VceFBBCPperMin != null)
                //                LES_VceFBBCPperMin = Math.Round(Convert.ToDecimal(LES_VceFBBCPperMin), 2, MidpointRounding.AwayFromZero);

                //            if (CUS_VceFBBSPperMin != null)
                //                CUS_VceFBBSPperMin = Math.Round(Convert.ToDecimal(CUS_VceFBBSPperMin), 2, MidpointRounding.AwayFromZero);

                //            Les_TotalCostCP += LES_VceFBBCPperMin;
                //            Cus_TotalCostSP += CUS_VceFBBSPperMin;

                //            if ((LES_VceFBBCPperMin.Equals(CDR_VceFBBCostPriceCT) || LES_VceFBBCPperMin > CDR_VceFBBCostPriceCT)
                //                && (CDR_VceFBBCostPriceCT < CUS_VceFBBSPperMin))
                //            {
                //                isVoiceFbbMatched = true;
                //            }
                //            else
                //            {
                //                isVoiceFbbMatched = false;
                //                Msg.AppendFormat("This Inmarsat {0}: Voice to FleetBroadband data rate is not match.<br />", ImarSatID).AppendLine();
                //                Msg.AppendFormat("Voice to FleetBroadband Rate from LES CDR Price:{0}.<br />", CDR_VceFBBCostPriceCT).AppendLine();
                //                Msg.AppendFormat("Voice to FleetBroadband Rate from LES Reg CostPrice:{0}.<br />", LES_VceFBBCPperMin).AppendLine();
                //                Msg.AppendFormat("Voice to FleetBroadband Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VceFBBSPperMin).AppendLine();
                //                returnMsg.AppendFormat("This Inmarsat {0}: Voice to FleetBroadband data rate is not match.<br />", ImarSatID).AppendLine();
                //            }
                //        }
                //        else
                //            isVoiceFbbMatched = true;

                //        if (CDR_VceIridiumUintsCT != null)
                //        {
                //            decimal V2IriTotalUnitsCT = 0;

                //            var v2IriCT = lstCtRecord.AsEnumerable().Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == LES_ServTypeVce2Iridium && (t.BCtBfkFbID.Equals(id)));

                //            foreach (var ct in v2IriCT)
                //            {
                //                if (ct.BCtBTransUsage != null)
                //                {
                //                    V2IriTotalUnitsCT += Convert.ToDecimal(getMinutes(ct.BCtBTransUsage));
                //                }
                //            }

                //            double lngUnits = Convert.ToDouble(V2IriTotalUnitsCT);
                //            decimal? CDR_VceIridiumTotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                //            if (CDR_VceIridiumTotalUnits_Minutes != null)
                //                CDR_VceIridiumTotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceIridiumTotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

                //            decimal? LES_VceIridiumCPperMin = CDR_VceIridiumTotalUnits_Minutes * LES_VoiceIridium;
                //            decimal? CUS_VceIridiumSPperMin = CDR_VceIridiumTotalUnits_Minutes * CUS_VoiceIridium;

                //            if (LES_VceIridiumCPperMin != null)
                //                LES_VceIridiumCPperMin = Math.Round(Convert.ToDecimal(LES_VceIridiumCPperMin), 2, MidpointRounding.AwayFromZero);

                //            if (CUS_VceIridiumSPperMin != null)
                //                CUS_VceIridiumSPperMin = Math.Round(Convert.ToDecimal(CUS_VceIridiumSPperMin), 2, MidpointRounding.AwayFromZero);

                //            Les_TotalCostCP += LES_VceIridiumCPperMin;
                //            Cus_TotalCostSP += CUS_VceIridiumSPperMin;

                //            if ((LES_VceIridiumCPperMin.Equals(CDR_VceIridiumCostPriceCT) || LES_VceIridiumCPperMin > CDR_VceIridiumCostPriceCT)
                //                && (CDR_VceIridiumCostPriceCT < CUS_VceIridiumSPperMin))
                //            {
                //                isVoiceIridiumMatched = true;
                //            }
                //            else
                //            {
                //                isVoiceIridiumMatched = false;
                //                Msg.AppendFormat("This Inmarsat {0}: Voice to Iridium data rate is not match.<br />", ImarSatID).AppendLine();
                //                Msg.AppendFormat("Voice to Iridium Rate from LES CDR Price:{0}.<br />", CDR_VceIridiumCostPriceCT).AppendLine();
                //                Msg.AppendFormat("Voice to Iridium Rate from LES Reg CostPrice:{0}.<br />", LES_VceIridiumCPperMin).AppendLine();
                //                Msg.AppendFormat("Voice to Iridium Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VceIridiumSPperMin).AppendLine();
                //                returnMsg.AppendFormat("This Inmarsat {0}: Voice to Iridium data rate is not match.<br />", ImarSatID).AppendLine();
                //            }
                //        }
                //        else
                //            isVoiceIridiumMatched = true;

                //        if (CDR_ISDNUintsCT != null)
                //        {
                //            decimal V2ISDNTotalUnitsCT = 0;

                //            var v2ISDNCT = lstCtRecord.AsEnumerable().Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeVce2ISDN && (t.BCtBfkFbID.Equals(id)));

                //            foreach (var ct in v2ISDNCT)
                //            {
                //                if (ct.BCtBTransUsage != null)
                //                {
                //                    V2ISDNTotalUnitsCT += Convert.ToDecimal(getMinutes(ct.BCtBTransUsage));
                //                }
                //            }

                //            double lngUnits = Convert.ToDouble(V2ISDNTotalUnitsCT);
                //            decimal? CDR_ISDNTotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                //            if (CDR_ISDNTotalUnits_Minutes != null)
                //                CDR_ISDNTotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_ISDNTotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

                //            decimal? LES_ISDNCPperMin = CDR_ISDNTotalUnits_Minutes * LES_ISDN;
                //            decimal? CUS_ISDNSPperMin = CDR_ISDNTotalUnits_Minutes * CUS_ISDN;

                //            if (LES_ISDNCPperMin != null)
                //                LES_ISDNCPperMin = Math.Round(Convert.ToDecimal(LES_ISDNCPperMin), 2, MidpointRounding.AwayFromZero);

                //            if (CUS_ISDNSPperMin != null)
                //                CUS_ISDNSPperMin = Math.Round(Convert.ToDecimal(CUS_ISDNSPperMin), 2, MidpointRounding.AwayFromZero);

                //            Les_TotalCostCP += LES_ISDNCPperMin;
                //            Cus_TotalCostSP += CUS_ISDNSPperMin;

                //            if ((LES_ISDNCPperMin.Equals(CDR_ISDNCostPriceCT) || LES_ISDNCPperMin > CDR_ISDNCostPriceCT)
                //                && (CDR_ISDNCostPriceCT < CUS_ISDNSPperMin))
                //            {
                //                isISDNMatched = true;
                //            }
                //            else
                //            {
                //                isISDNMatched = false;
                //                Msg.AppendFormat("This Inmarsat {0}: ISDN rate is not match.<br />", ImarSatID).AppendLine();
                //                Msg.AppendFormat("ISDN Rate from LES CDR Price:{0}.<br />", CDR_ISDNCostPriceCT).AppendLine();
                //                Msg.AppendFormat("ISDN Rate from LES Reg CostPrice:{0}.<br />", LES_ISDNCPperMin).AppendLine();
                //                Msg.AppendFormat("ISDN Rate from Cus Reg SellerPrice:{0}.<br />", CUS_ISDNSPperMin).AppendLine();
                //                returnMsg.AppendFormat("This Inmarsat {0}: ISDN rate is not match.<br />", ImarSatID).AppendLine();
                //            }
                //        }
                //        else
                //            isISDNMatched = true;

                //        if (CDR_SMSUintsCT != null)
                //        {

                //            decimal? CDR_SMSTotalUnits_Msg = CDR_SMSUintsCT;

                //            if (CDR_SMSTotalUnits_Msg != null)
                //                CDR_SMSTotalUnits_Msg = Math.Round(Convert.ToDecimal(CDR_SMSTotalUnits_Msg), 2, MidpointRounding.AwayFromZero);

                //            decimal? LES_SMSCPperMin = CDR_SMSTotalUnits_Msg * LES_SMS;
                //            decimal? CUS_SMSSPperMin = CDR_SMSTotalUnits_Msg * CUS_SMS;

                //            if (LES_SMSCPperMin != null)
                //                LES_SMSCPperMin = Math.Round(Convert.ToDecimal(LES_SMSCPperMin), 2, MidpointRounding.AwayFromZero);

                //            if (CUS_SMSSPperMin != null)
                //                CUS_SMSSPperMin = Math.Round(Convert.ToDecimal(CUS_SMSSPperMin), 2, MidpointRounding.AwayFromZero);

                //            Les_TotalCostCP += LES_SMSCPperMin;
                //            Cus_TotalCostSP += CUS_SMSSPperMin;

                //            if ((LES_SMSCPperMin.Equals(CDR_SMSCostPriceCT) || LES_SMSCPperMin > CDR_SMSCostPriceCT)
                //                && (CDR_SMSCostPriceCT < CUS_SMSSPperMin))
                //            {
                //                isSMSMatched = true;
                //            }
                //            else
                //            {
                //                isISDNMatched = false;
                //                Msg.AppendFormat("This Inmarsat {0}: SMS rate is not match.<br />", ImarSatID).AppendLine();
                //                Msg.AppendFormat("SMS Rate from LES CDR Price:{0}.<br />", CDR_SMSCostPriceCT).AppendLine();
                //                Msg.AppendFormat("SMS Rate from LES Reg CostPrice:{0}.<br />", LES_SMSCPperMin).AppendLine();
                //                Msg.AppendFormat("SMS Rate from Cus Reg SellerPrice:{0}.<br />", CUS_SMSSPperMin).AppendLine();
                //                returnMsg.AppendFormat("This Inmarsat {0}: SMS rate is not match.<br />", ImarSatID).AppendLine();
                //            }
                //        }
                //        else
                //            isSMSMatched = true;

                //        string InvoiceNo = string.Empty;

                //        try
                //        {
                //            InvoiceNo = db.BS2MtCusT.Max(t => t.BSMtCusInvNo);

                //            if (!string.IsNullOrEmpty(InvoiceNo))
                //            {
                //                if (InvoiceNo.Contains("SM"))
                //                {
                //                    string tempInv = InvoiceNo.Replace("SM", string.Empty);
                //                    if (int.TryParse(tempInv, out int result))
                //                    {
                //                        InvoiceNo = BillingHelpers.GenInvoice(false, result);
                //                    }
                //                }
                //            }
                //            else
                //                InvoiceNo = BillingHelpers.GenInvoice(false, 0);
                //        }
                //        catch (Exception error)
                //        {
                //            Msg.AppendFormat("MSI:{0} Error Msg: {1} Stack Trace: {2}. <br />", ImarSatID, error.Message.ToString(), error.StackTrace);
                //        }

                //        bool isLES_ContrRegDate = false;
                //        bool isLES_ContrBarDate = false;
                //        bool isLES_ContrSusDate = false;
                //        bool isLES_ContrLayUpDate = false;

                //        bool isCUS_ContrRegDate = false;
                //        bool isCUS_ContrBarDate = false;
                //        bool isCUS_ContrSusDate = false;
                //        bool isCUS_ContrLayUpDate = false;

                //        if ((CDR_StartDate >= LES_ContrRegStrDate && CDR_EndDate <= LES_ContrRegEndDate) || isNoTransData)
                //            isLES_ContrRegDate = true;
                //        else
                //        {
                //            isLES_ContrRegDate = false;
                //            Msg.AppendFormat("This Inmarsat {0}: Contract CDR Start/End date Less than les Reg Start/End Date.<br />", ImarSatID).AppendLine();
                //            Msg.AppendFormat("Contract CDR Start Date {0}: LES Reg Strart Date {1}.<br />", CDR_StartDate, LES_ContrRegStrDate).AppendLine();
                //            Msg.AppendFormat("Contract CDR Start Date {0}: LES Reg End Date {1}.<br />", CDR_StartDate, LES_ContrRegEndDate).AppendLine();
                //            returnMsg.AppendFormat("This Inmarsat {0}: Contract Reg start date Less than les Reg start Date.<br />", ImarSatID).AppendLine();
                //        }

                //        if ((LES_ContrBarStrDate == null && LES_ContrBarEndDate == null) || (CDR_StartDate > LES_ContrBarStrDate && CDR_EndDate > LES_ContrBarEndDate))
                //            isLES_ContrBarDate = true;
                //        else
                //        {
                //            isLES_ContrBarDate = false;
                //            Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than lES Bar Start/End Date.<br />", ImarSatID).AppendLine();
                //            Msg.AppendFormat("Contract CDR Start Date {0}: LES Bar Strart Date {1}.<br />", CDR_StartDate, LES_ContrBarStrDate).AppendLine();
                //            Msg.AppendFormat("Contract CDR Start Date {0}: LES Bar End Date {1}.<br />", CDR_StartDate, LES_ContrBarEndDate).AppendLine();
                //            returnMsg.AppendFormat("This Inmarsat {0}: Contract CCDR start date Greater than lES Bar start Date.<br />", ImarSatID).AppendLine();
                //        }

                //        if ((LES_ContrSusStrDate == null && LES_ContrSusEndDate == null) || (CDR_StartDate > LES_ContrSusStrDate && CDR_EndDate > LES_ContrSusEndDate))
                //            isLES_ContrSusDate = true;
                //        else
                //        {
                //            isLES_ContrBarDate = false;
                //            Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than lES Suspenstion Start/End Date.<br />", ImarSatID).AppendLine();
                //            Msg.AppendFormat("Contract CDR Start Date {0}: LES Suspenstion Start Date {1}.<br />", CDR_StartDate, LES_ContrSusStrDate).AppendLine();
                //            Msg.AppendFormat("Contract CDR Start Date {0}: LES Suspenstion End Date {1}.<br />", CDR_StartDate, LES_ContrSusEndDate).AppendLine();
                //            returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than lES Suspenstion start Date.<br />", ImarSatID).AppendLine();
                //        }

                //        if ((LES_ContrLayoutStrDate == null && LES_ContrLayoutEndDate == null) || (CDR_StartDate > LES_ContrLayoutStrDate && CDR_EndDate > LES_ContrLayoutEndDate))
                //            isLES_ContrLayUpDate = true;
                //        else
                //        {
                //            isLES_ContrLayUpDate = true;
                //            Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than LES LayUp Start/End Date.<br />", ImarSatID).AppendLine();
                //            Msg.AppendFormat("Contract CDR Start Date {0}: LES LayUp Start Date {1}.<br />", CDR_StartDate, LES_ContrSusStrDate).AppendLine();
                //            Msg.AppendFormat("Contract CDR Start Date {0}: LES LayUp End Date {1}.<br />", CDR_StartDate, LES_ContrSusEndDate).AppendLine();
                //            returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than LayUp Start/End Date.<br />", ImarSatID).AppendLine();
                //        }

                //        if ((CDR_StartDate >= CUS_ContrRegStrDate && CDR_EndDate <= CUS_ContrRegEndDate) || isNoTransData)
                //            isCUS_ContrRegDate = true;
                //        else
                //        {
                //            isCUS_ContrRegDate = false;
                //            Msg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Less than CUS Reg Start/End Date.<br />", ImarSatID).AppendLine();
                //            Msg.AppendFormat("Contract CDR Start Date {0}: CUS Reg Strart Date {1}.<br />", CDR_StartDate, CUS_ContrRegStrDate).AppendLine();
                //            Msg.AppendFormat("Contract CDR Start Date {0}: CUS Reg End Date {1}.<br />", CDR_StartDate, CUS_ContrRegEndDate).AppendLine();
                //            returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Less than CUS Reg Start/End Date.<br />", ImarSatID).AppendLine();
                //        }

                //        if ((CUS_ContrBarStrDate == null && CUS_ContrBarEndDate == null) || (CDR_StartDate > CUS_ContrBarStrDate && CDR_EndDate > CUS_ContrBarEndDate))
                //            isCUS_ContrBarDate = true;
                //        else
                //        {
                //            isCUS_ContrBarDate = false;
                //            Msg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Greater than CUS Bar Start/End Date.<br />", ImarSatID).AppendLine();
                //            Msg.AppendFormat("Contract CDR Start Date {0}: CUS Bar Strart Date {1}.<br />", CDR_StartDate, CUS_ContrBarStrDate).AppendLine();
                //            Msg.AppendFormat("Contract CDR Start Date {0}: CUS Bar End Date {1}.<br />", CDR_StartDate, CUS_ContrBarStrDate).AppendLine();
                //            returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Greater than CUS Bar Start/End Date.<br />", ImarSatID).AppendLine();
                //        }

                //        if ((CUS_ContrSusStrDate == null && CUS_ContrSusEndDate == null) || (CDR_StartDate > CUS_ContrSusStrDate && CDR_EndDate > CUS_ContrSusEndDate))
                //            isCUS_ContrSusDate = true;
                //        else
                //        {
                //            isCUS_ContrSusDate = false;
                //            Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS Suspenstion Start/End Date.<br />", ImarSatID).AppendLine();
                //            Msg.AppendFormat("Contract CDR Start Date {0}: CUS Suspenstion Start Date {1}.<br />", CDR_StartDate, CUS_ContrSusStrDate).AppendLine();
                //            Msg.AppendFormat("Contract CDR Start Date {0}: CUS Suspenstion End Date {1}.<br />", CDR_StartDate, CUS_ContrSusEndDate).AppendLine();
                //            returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS Suspenstion Start/End Date.<br />", ImarSatID).AppendLine();
                //        }

                //        if ((CUS_ContrLayoutStrDate == null && CUS_ContrLayoutEndDate == null) || (CDR_StartDate > CUS_ContrLayoutStrDate && CDR_EndDate > CUS_ContrLayoutEndDate))
                //            isCUS_ContrLayUpDate = true;
                //        else
                //        {
                //            isCUS_ContrLayUpDate = true;
                //            Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS LayUp Start/End Date.<br />", ImarSatID).AppendLine();
                //            Msg.AppendFormat("Contract CDR Start Date {0}: CUS LayUp Start Date {1}.<br />", CDR_StartDate, LES_ContrSusStrDate).AppendLine();
                //            Msg.AppendFormat("Contract CDR Start Date {0}: CUS LayUp End Date {1}.<br />", CDR_StartDate, LES_ContrSusEndDate).AppendLine();
                //            returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS LayUp Start/End Date.<br />", ImarSatID).AppendLine();
                //        }

                //        if (isRecurFeeMached && isDataMatched && isVoiceCellurlarMatched && isVoiceFixedMatched && isVoiceFbbMatched && isVoiceIridiumMatched &&
                //            isISDNMatched && isLES_ContrRegDate && isLES_ContrBarDate && isLES_ContrSusDate && isLES_ContrLayUpDate && isCUS_ContrRegDate &&
                //            isCUS_ContrBarDate && isCUS_ContrSusDate && isCUS_ContrLayUpDate && isSMSMatched)
                //        {
                //            if (!string.IsNullOrEmpty(InvoiceNo))
                //            {
                //                BS2MtCusT SecMtCust = new BS2MtCusT();

                //                SecMtCust.BSMtCusInvNo = InvoiceNo;
                //                SecMtCust.fkSPID = CT_Fbs.BCtAfkLESID;
                //                SecMtCust.fkCusID = CUS_Id;
                //                SecMtCust.fkCusConID = CUS_ContractID;
                //                SecMtCust.fkCusRegID = CUS_RegsID;
                //                SecMtCust.fkspcID = CT_Fbs.BCtAfkLESCID;
                //                SecMtCust.fkSimID = SimID;
                //                SecMtCust.fkLesRefID = CT_Fbs.fkLesRefID;
                //                SecMtCust.fkSatTypeID = CT_Fbs.fkSatTypeID;
                //                SecMtCust.fkEquipTypeID = CT_Fbs.fkEquipType;
                //                SecMtCust.BSMtCusLesCode = CT_Fbs.BCtAFbLesCode;
                //                SecMtCust.BSMtCusCdrCode = CT_Fbs.BCtAFbCdrCode;
                //                SecMtCust.BSMtCusSatType = null;
                //                SecMtCust.BSMtCusServDest = CT_Fbs.BCtAFbServDest;
                //                SecMtCust.BSMtCusServType = CT_Fbs.BCtAFbServType;
                //                SecMtCust.BSMtCusTransUsage = CT_Fbs.BCtAFbTransUsage;
                //                SecMtCust.BSMtCusUOM = CT_Fbs.BCtAFbUOM;
                //                SecMtCust.BSMtCusRecurFee = CUS_RecurFee;
                //                Cus_TotalCostSP += SecMtCust.BSMtCusRecurFee;
                //                SecMtCust.BSMtCusRecurFeeType = CT_Fbs.BCtAFbRecurFeeType;
                //                SecMtCust.BSMtCusCDRRef = CT_Fbs.BCtAFbCDRRef;
                //                SecMtCust.BSMtCusBillNo = CT_Fbs.BCtAFbBillNo;
                //                SecMtCust.BSMtCusBillPeriod = CT_Fbs.BCtAFbBillPeriod;
                //                SecMtCust.BSMtCusBillDate = CT_Fbs.BCtAFbBillDate;

                //                if (CDR_RecuFee < CDR_DataCP && isOutbundleUsed)
                //                {
                //                    LES_InvRptDataCostTot = CDR_DataCP - CDR_RecuFee;
                //                    LES_InvRptDataCostTot = Math.Round(Convert.ToDecimal(LES_InvRptDataCostTot), 2, MidpointRounding.AwayFromZero);
                //                }
                //                else
                //                    LES_InvRptDataCostTot = 0;



                //                db.BS2MtCusT.Add(SecMtCust);
                //                db.SaveChanges();

                //                int SecFBBID = SecMtCust.BSMtCusTID;

                //                // Get value from BS2MtCusT and save to BInvRprt table
                //                BInvLstReport Invreport = new BInvLstReport();

                //                if (SecMtCust.BSMtCusServType != null)
                //                {
                //                    Invreport.fkBCus2ID = SecFBBID;
                //                    Invreport.BInvRprtSerType = SecMtCust.BSMtCusServType;
                //                    Invreport.BInvRprtTotBil = SecMtCust.BSMtCusRecurFee;
                //                    Invreport.BInvRprtTotCst = CT_Fbs.BCtAFbRecurFee;
                //                    db.BInvLstReports.Add(Invreport);
                //                }

                //                List<BCtB> lstDateCT = lstCtRecord.AsEnumerable()
                //                                     .Where(t => t.BCtBfkCommType == 1 && t.BCtBLesServTypeID == ServTypeID &&
                //                                     (t.BCtBfkFbID == id)).ToList();
                //                Bs2Sec bs2Sec;
                //                decimal? outbindleData = 0;
                //                decimal? LES_RptDataCost = 0;
                //                decimal? CUS_RptDataCost = 0;

                //                if (is_CUSGBPlan)
                //                {
                //                    Cus_DataPlan = Cus_DataPlan * 1024;
                //                }

                //                foreach (var records in lstDateCT)
                //                {
                //                    bs2Sec = new Bs2Sec();
                //                    bs2Sec.fkCTFbID = SecFBBID;
                //                    bs2Sec.BsCtStartDate = records.BCtBStartDate;
                //                    bs2Sec.BsCtStarTime = records.BCtBStartTime;
                //                    bs2Sec.BsCtOrigion = records.BCtBOrigin;
                //                    bs2Sec.BsCtDestination = records.BCtBDestination;
                //                    bs2Sec.fkSPServTypeID = CUS_ServTypeID;

                //                    long dataconvertToMP = Convert.ToInt64(records.BCtBTransUsage);
                //                    bs2Sec.BsCtTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertKilobytesToMegabytes(dataconvertToMP)), 2, MidpointRounding.AwayFromZero);
                //                    bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("MB")).UID;
                //                    outbindleData += bs2Sec.BsCtTransUsage;

                //                    if (Cus_DataPlan <= outbindleData)
                //                    {
                //                        bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * Cus_OutbundleRate), 2, MidpointRounding.AwayFromZero);

                //                        if (Invreport.BInvRprtSerType != null)
                //                            CUS_RptDataCost += bs2Sec.BsCtTotalUsage;
                //                    }
                //                    else
                //                    {
                //                        bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * 0), 2, MidpointRounding.AwayFromZero);

                //                        if (Invreport.BInvRprtSerType != null)
                //                            CUS_RptDataCost += bs2Sec.BsCtTotalUsage;
                //                    }
                //                    bs2Sec.fkCommType = 1;

                //                    db.Bs2Sec.Add(bs2Sec);
                //                }

                //                // Invoice List Report Add in BInvLstReports
                //                if (lstDateCT.Count > 0)
                //                {
                //                    Invreport = new BInvLstReport();
                //                    try
                //                    {
                //                        Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeID).BsPtCusServiceTypes;
                //                    }
                //                    catch { Invreport.BInvRprtSerType = null; }

                //                    if (Invreport.BInvRprtSerType != null)
                //                    {
                //                        Invreport.fkBCus2ID = SecFBBID;
                //                        Invreport.BInvRprtTotCst = LES_InvRptDataCostTot;
                //                        Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptDataCost), 2, MidpointRounding.AwayFromZero);
                //                        db.BInvLstReports.Add(Invreport);
                //                    }
                //                }


                //                List<BCtB> lstVceCellCT = lstCtRecord.AsEnumerable()
                //                                     .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeMobile &&
                //                                     (t.BCtBfkFbID == id)).ToList();

                //                decimal? LES_RptV2cCost = CDR_VceCellularCostPriceCT;
                //                decimal? CUS_RptV2cCost = 0;

                //                foreach (var records in lstVceCellCT)
                //                {
                //                    bs2Sec = new Bs2Sec();
                //                    bs2Sec.fkCTFbID = SecFBBID;
                //                    bs2Sec.BsCtStartDate = records.BCtBStartDate;
                //                    bs2Sec.BsCtStarTime = records.BCtBStartTime;
                //                    bs2Sec.BsCtOrigion = records.BCtBOrigin;
                //                    bs2Sec.BsCtDestination = records.BCtBDestination;
                //                    bs2Sec.fkSPServTypeID = CUS_ServTypeMobile;

                //                    decimal? v2CTransSec = 0;
                //                    if (records.BCtBTransUsage != null)
                //                    {
                //                        v2CTransSec = getMinutes(records.BCtBTransUsage);
                //                    }
                //                    long SecconvertToMin = Convert.ToInt64(v2CTransSec);
                //                    bs2Sec.BsCtTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                //                    bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                //                    bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_VoiceCellular), 2, MidpointRounding.AwayFromZero);

                //                    if (Invreport.BInvRprtSerType != null)
                //                        CUS_RptV2cCost += bs2Sec.BsCtTotalUsage;
                //                    bs2Sec.fkCommType = 2;

                //                    db.Bs2Sec.Add(bs2Sec);
                //                }

                //                // Invoice List Report Add in BInvLstReports
                //                if (lstVceCellCT.Count > 0)
                //                {
                //                    Invreport = new BInvLstReport();
                //                    try
                //                    {
                //                        Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeMobile).BsPtCusServiceTypes;
                //                    }
                //                    catch { Invreport.BInvRprtSerType = null; }

                //                    if (Invreport.BInvRprtSerType != null)
                //                    {
                //                        Invreport.fkBCus2ID = SecFBBID;
                //                        Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2cCost), 2, MidpointRounding.AwayFromZero);
                //                        Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2cCost), 2, MidpointRounding.AwayFromZero);
                //                        db.BInvLstReports.Add(Invreport);
                //                    }

                //                }


                //                List<BCtB> lstVceFixedCT = lstCtRecord.AsEnumerable()
                //                                 .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeVce2Fixed &&
                //                                 (t.BCtBfkFbID.Equals(id))).ToList();

                //                decimal? LES_RptV2fCost = CDR_VceFixedCostPriceCT;
                //                decimal? CUS_RptV2fCost = 0;

                //                foreach (var records in lstVceFixedCT)
                //                {
                //                    bs2Sec = new Bs2Sec();
                //                    bs2Sec.fkCTFbID = SecFBBID;
                //                    bs2Sec.BsCtStartDate = records.BCtBStartDate;
                //                    bs2Sec.BsCtStarTime = records.BCtBStartTime;
                //                    bs2Sec.BsCtOrigion = records.BCtBOrigin;
                //                    bs2Sec.BsCtDestination = records.BCtBDestination;
                //                    bs2Sec.fkSPServTypeID = CUS_ServTypeVce2Fixed;

                //                    decimal? v2FixedTransSec = 0;
                //                    if (records.BCtBTransUsage != null)
                //                    {
                //                        v2FixedTransSec = getMinutes(records.BCtBTransUsage);
                //                    }

                //                    long SecconvertToMin = Convert.ToInt64(v2FixedTransSec);
                //                    bs2Sec.BsCtTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                //                    bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                //                    bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_VoiceFixed), 2, MidpointRounding.AwayFromZero);

                //                    if (Invreport.BInvRprtSerType != null)
                //                        CUS_RptV2fCost += bs2Sec.BsCtTotalUsage;
                //                    bs2Sec.fkCommType = 2;

                //                    db.Bs2Sec.Add(bs2Sec);
                //                }

                //                // Invoice List Report Add in BInvLstReports
                //                if (lstVceFixedCT.Count > 0)
                //                {
                //                    Invreport = new BInvLstReport();

                //                    try
                //                    {
                //                        Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeVce2Fixed).BsPtCusServiceTypes;
                //                    }
                //                    catch { Invreport.BInvRprtSerType = null; }

                //                    if (Invreport.BInvRprtSerType != null)
                //                    {
                //                        Invreport.fkBCus2ID = SecFBBID;
                //                        Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2fCost), 2, MidpointRounding.AwayFromZero);
                //                        Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2fCost), 2, MidpointRounding.AwayFromZero);
                //                        db.BInvLstReports.Add(Invreport);
                //                    }

                //                }


                //                List<BCtB> lstVceFBBCT = lstCtRecord.AsEnumerable()
                //                                     .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeVce2FBB &&
                //                                     (t.BCtBfkFbID.Equals(id))).ToList();

                //                decimal? LES_RptV2fbCost = CDR_VceFBBCostPriceCT;
                //                decimal? CUS_RptV2fbCost = 0;

                //                foreach (var records in lstVceFBBCT)
                //                {
                //                    bs2Sec = new Bs2Sec();
                //                    bs2Sec.fkCTFbID = SecFBBID;
                //                    bs2Sec.BsCtStartDate = records.BCtBStartDate;
                //                    bs2Sec.BsCtStarTime = records.BCtBStartTime;
                //                    bs2Sec.BsCtOrigion = records.BCtBOrigin;
                //                    bs2Sec.BsCtDestination = records.BCtBDestination;
                //                    bs2Sec.fkSPServTypeID = CUS_ServTypeVce2FBB;

                //                    decimal? v2FBBTransSec = 0;
                //                    if (records.BCtBTransUsage != null)
                //                    {
                //                        v2FBBTransSec = getMinutes(records.BCtBTransUsage);
                //                    }

                //                    long SecconvertToMin = Convert.ToInt64(v2FBBTransSec);
                //                    bs2Sec.BsCtTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                //                    bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                //                    bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_VoiceFBB), 2, MidpointRounding.AwayFromZero);

                //                    if (Invreport.BInvRprtSerType != null)
                //                        CUS_RptV2fbCost += bs2Sec.BsCtTotalUsage;
                //                    bs2Sec.fkCommType = 2;

                //                    db.Bs2Sec.Add(bs2Sec);
                //                }

                //                if (lstVceFBBCT.Count > 0)
                //                {
                //                    Invreport = new BInvLstReport();
                //                    try
                //                    {
                //                        Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeVce2FBB).BsPtCusServiceTypes;
                //                    }
                //                    catch { Invreport.BInvRprtSerType = null; }

                //                    if (Invreport.BInvRprtSerType != null)
                //                    {
                //                        Invreport.fkBCus2ID = SecFBBID;
                //                        Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2fbCost), 2, MidpointRounding.AwayFromZero);
                //                        Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2fbCost), 2, MidpointRounding.AwayFromZero);
                //                        db.BInvLstReports.Add(Invreport);
                //                    }
                //                }


                //                List<BCtB> lstVceIridiumCT = lstCtRecord.AsEnumerable()
                //                                     .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == LES_ServTypeVce2Iridium &&
                //                                     (t.BCtBfkFbID.Equals(id))).ToList();

                //                decimal? LES_RptV2irdCost = CDR_VceIridiumCostPriceCT;
                //                decimal? CUS_RptV2irdCost = 0;

                //                foreach (var records in lstVceIridiumCT)
                //                {
                //                    bs2Sec = new Bs2Sec();
                //                    bs2Sec.fkCTFbID = SecFBBID;
                //                    bs2Sec.BsCtStartDate = records.BCtBStartDate;
                //                    bs2Sec.BsCtStarTime = records.BCtBStartTime;
                //                    bs2Sec.BsCtOrigion = records.BCtBOrigin;
                //                    bs2Sec.BsCtDestination = records.BCtBDestination;
                //                    bs2Sec.fkSPServTypeID = CUS_ServTypeVce2Iridium;

                //                    decimal? v2IriTransSec = 0;
                //                    if (records.BCtBTransUsage != null)
                //                    {
                //                        v2IriTransSec = getMinutes(records.BCtBTransUsage);
                //                    }

                //                    long SecconvertToMin = Convert.ToInt64(v2IriTransSec);

                //                    bs2Sec.BsCtTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                //                    bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                //                    bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_VoiceIridium), 2, MidpointRounding.AwayFromZero);

                //                    if (Invreport.BInvRprtSerType != null)
                //                        CUS_RptV2irdCost += bs2Sec.BsCtTotalUsage;
                //                    bs2Sec.fkCommType = 2;

                //                    db.Bs2Sec.Add(bs2Sec);
                //                }

                //                // Invoice List Report Add in BInvLstReports
                //                if (lstVceIridiumCT.Count > 0)
                //                {
                //                    Invreport = new BInvLstReport();
                //                    try
                //                    {
                //                        Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeVce2Iridium).BsPtCusServiceTypes;
                //                    }
                //                    catch { Invreport.BInvRprtSerType = null; }

                //                    if (Invreport.BInvRprtSerType != null)
                //                    {
                //                        Invreport.fkBCus2ID = SecFBBID;
                //                        Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2irdCost), 2, MidpointRounding.AwayFromZero);
                //                        Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2irdCost), 2, MidpointRounding.AwayFromZero);
                //                        db.BInvLstReports.Add(Invreport);
                //                    }
                //                }

                //                List<BCtB> lstISDNCT = lstCtRecord.AsEnumerable()
                //                                    .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeVce2ISDN &&
                //                                    (t.BCtBfkFbID.Equals(id))).ToList();

                //                decimal? LES_RptV2isdnCost = CDR_ISDNCostPriceCT;
                //                decimal? CUS_RptV2isdnCost = 0;

                //                foreach (var records in lstISDNCT)
                //                {
                //                    bs2Sec = new Bs2Sec();
                //                    bs2Sec.fkCTFbID = SecFBBID;
                //                    bs2Sec.BsCtStartDate = records.BCtBStartDate;
                //                    bs2Sec.BsCtStarTime = records.BCtBStartTime;
                //                    bs2Sec.BsCtOrigion = records.BCtBOrigin;
                //                    bs2Sec.BsCtDestination = records.BCtBDestination;
                //                    bs2Sec.fkSPServTypeID = CUS_ServTypeVce2ISDN;

                //                    decimal? v2ISDNTransSec = 0;
                //                    if (records.BCtBTransUsage != null)
                //                    {
                //                        v2ISDNTransSec = getMinutes(records.BCtBTransUsage);
                //                    }

                //                    long SecconvertToMin = Convert.ToInt64(v2ISDNTransSec);

                //                    bs2Sec.BsCtTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                //                    bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                //                    bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_ISDN), 2, MidpointRounding.AwayFromZero);

                //                    if (Invreport.BInvRprtSerType != null)
                //                        CUS_RptV2isdnCost += bs2Sec.BsCtTotalUsage;
                //                    bs2Sec.fkCommType = 2;

                //                    db.Bs2Sec.Add(bs2Sec);
                //                }

                //                // Invoice List Report Add in BInvLstReports
                //                if (lstISDNCT.Count > 0)
                //                {
                //                    Invreport = new BInvLstReport();
                //                    try
                //                    {
                //                        Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeVce2ISDN).BsPtCusServiceTypes;
                //                    }
                //                    catch { Invreport.BInvRprtSerType = null; }

                //                    if (Invreport.BInvRprtSerType != null)
                //                    {
                //                        Invreport.fkBCus2ID = SecFBBID;
                //                        Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2isdnCost), 2, MidpointRounding.AwayFromZero);
                //                        Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2isdnCost), 2, MidpointRounding.AwayFromZero);
                //                        db.BInvLstReports.Add(Invreport);
                //                    }
                //                }

                //                int commtype = 1;
                //                try
                //                {
                //                    commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                //                }
                //                catch
                //                {
                //                    commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                //                }
                //                List<BCtB> lstSMSCT = lstCtRecord.AsEnumerable()
                //                                  .Where(t => t.BCtBfkCommType == commtype && t.BCtBLesServTypeID == ServTypeSMS &&
                //                                  (t.BCtBfkFbID.Equals(id))).ToList();

                //                decimal? LES_RptV2smsCost = CDR_SMSCostPriceCT;
                //                decimal? CUS_RptV2smsCost = 0;

                //                foreach (var records in lstSMSCT)
                //                {
                //                    bs2Sec = new Bs2Sec();
                //                    bs2Sec.fkCTFbID = SecFBBID;
                //                    bs2Sec.BsCtStartDate = records.BCtBStartDate;
                //                    bs2Sec.BsCtStarTime = records.BCtBStartTime;
                //                    bs2Sec.BsCtOrigion = records.BCtBOrigin;
                //                    bs2Sec.BsCtDestination = records.BCtBDestination;
                //                    bs2Sec.fkSPServTypeID = CUS_ServTypeSMS;

                //                    //long SecconvertToMin = Convert.ToInt64(records.BsCtBillIncre);
                //                    bs2Sec.BsCtTransUsage = Convert.ToInt32(records.BCtBTransUsage); //(decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                //                    try
                //                    {
                //                        bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("message")).UID;
                //                    }
                //                    catch
                //                    {
                //                        bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Message")).UID;
                //                    }

                //                    bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_SMS), 2, MidpointRounding.AwayFromZero);

                //                    if (Invreport.BInvRprtSerType != null)
                //                        CUS_RptV2smsCost += bs2Sec.BsCtTotalUsage;
                //                    bs2Sec.fkCommType = commtype;

                //                    db.Bs2Sec.Add(bs2Sec);
                //                }

                //                // Invoice List Report Add in BInvLstReports
                //                if (lstSMSCT.Count > 0)
                //                {
                //                    Invreport = new BInvLstReport();
                //                    try
                //                    {
                //                        Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeSMS).BsPtCusServiceTypes;
                //                    }
                //                    catch { Invreport.BInvRprtSerType = null; }
                //                    if (Invreport.BInvRprtSerType != null)
                //                    {
                //                        Invreport.fkBCus2ID = SecFBBID;
                //                        Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2smsCost), 2, MidpointRounding.AwayFromZero);
                //                        Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2smsCost), 2, MidpointRounding.AwayFromZero);
                //                        db.BInvLstReports.Add(Invreport);
                //                    }
                //                }

                //                // List of Satatement 
                //                Bs2Statement statment = null;
                //                try
                //                {
                //                    statment = db.Bs2Statement.FirstOrDefault(t => t.fkBs2CusID == SecFBBID);
                //                }
                //                catch { }

                //                if (statment == null)
                //                {
                //                    statment = new Bs2Statement();

                //                    statment.fkBs2CusID = SecFBBID;
                //                    statment.fkSimID = SimID;
                //                    statment.Bs2StTotalCP = Les_TotalCostCP;
                //                    statment.Bs2StTotalSP = Cus_TotalCostSP;
                //                    statment.Bs2StSMTSCost = SMTS_TotalCost;
                //                    statment.fkSPID = CT_Fbs.BCtAfkLESID;
                //                    statment.fkCusID = CUS_Id;
                //                    statment.Bs2StIvoiceDate = DateTime.Now.Date;
                //                    statment.Bs2StDuration = CT_Fbs.BCtAFbServDest;

                //                    if (statment.Bs2StTotalCP > 0 && statment.Bs2StTotalSP > 0)
                //                    {
                //                        decimal CrossProfit = Convert.ToDecimal(statment.Bs2StTotalSP - statment.Bs2StSMTSCost);
                //                        decimal Margin = (CrossProfit / Convert.ToDecimal(statment.Bs2StTotalSP) * 100);
                //                        statment.Bs2stMargin = Margin > 0 ? Convert.ToInt32(Margin) : (int)Math.Abs(Margin);
                //                    }

                //                    db.Bs2Statement.Add(statment);
                //                }

                //                try
                //                {
                //                    db.SaveChanges();
                //                }
                //                catch (Exception error) { throw error; }
                //                returnMsg.AppendFormat("This InmarSat {0}: Successfully stored to Invoice.<br />", ImarSatID);
                //            }
                //        }
                //        else
                //        {
                //            BS1ALessT SecRejLes = new BS1ALessT();

                //            SecRejLes.fkSPID = CT_Fbs.BCtAfkLESID;
                //            SecRejLes.fkCusID = CUS_Id;
                //            SecRejLes.fkCusConID = CUS_ContractID;
                //            SecRejLes.fkCusRegID = CUS_RegsID;
                //            SecRejLes.fkspcID = CT_Fbs.BCtAfkLESCID;
                //            SecRejLes.fkSimID = SimID;
                //            SecRejLes.fkLesRefID = CT_Fbs.fkLesRefID;
                //            SecRejLes.fkSatTypeID = CT_Fbs.fkSatTypeID;
                //            SecRejLes.fkEquipTypeID = CT_Fbs.fkEquipType;
                //            SecRejLes.BS1ALesLesCode = CT_Fbs.BCtAFbLesCode;
                //            SecRejLes.BS1ALesCdrCode = CT_Fbs.BCtAFbCdrCode;
                //            SecRejLes.BS1ALesServDest = CT_Fbs.BCtAFbServDest;
                //            SecRejLes.BS1ALesServType = CT_Fbs.BCtAFbServType;
                //            SecRejLes.BS1ALesTransUsage = CT_Fbs.BCtAFbTransUsage;
                //            SecRejLes.BS1ALesUOM = CT_Fbs.BCtAFbUOM;
                //            SecRejLes.BS1ALesRecurFee = LES_RecurFee;
                //            SecRejLes.BS1ALesRecurFeeType = CT_Fbs.BCtAFbRecurFeeType;
                //            SecRejLes.BS1ALesCDRRef = CT_Fbs.BCtAFbCDRRef;
                //            SecRejLes.BS1ALesBillNo = CT_Fbs.BCtAFbBillNo;
                //            SecRejLes.BS1ALesBillPeriod = CT_Fbs.BCtAFbBillPeriod;
                //            SecRejLes.BS1ALesBillDate = CT_Fbs.BCtAFbBillDate;

                //            db.BS1ALessT.Add(SecRejLes);
                //            try
                //            {
                //                db.SaveChanges();
                //            }
                //            catch (Exception err)
                //            {

                //            }

                //            int SecRejID = SecRejLes.BS1ALesID;

                //            if (CDR_RecuFee < CDR_DataCP && isOutbundleUsed)
                //            {
                //                LES_InvRptDataCostTot = CDR_DataCP - CDR_RecuFee;
                //                LES_InvRptDataCostTot = Math.Round(Convert.ToDecimal(LES_InvRptDataCostTot), 2, MidpointRounding.AwayFromZero);
                //            }
                //            else
                //                LES_InvRptDataCostTot = 0;

                //            BInvLstReport InvreportCost = new BInvLstReport();
                //            if (SecRejLes.BS1ALesServType != null)
                //            {
                //                InvreportCost.BInvRprtLes1ID = SecRejID;
                //                InvreportCost.BInvRprtSerType = SecRejLes.BS1ALesServType;
                //                InvreportCost.BInvRprtTotCst = CDR_RecuFee;
                //                InvreportCost.BInvRprtTotBil = null;

                //                db.BInvLstReports.Add(InvreportCost);
                //            }

                //            List<BCtB> lstDateCT = lstCtRecord.AsEnumerable()
                //                                 .Where(t => t.BCtBfkCommType == 1 && t.BCtBLesServTypeID == ServTypeID &&
                //                                 (t.BCtBfkFbID == id)).ToList();

                //            BS1LesRejLog lesRejLog = new BS1LesRejLog();
                //            lesRejLog.fkBs1LesReg = SecRejID;
                //            lesRejLog.Bs1lesRejection = Msg.ToString();
                //            lesRejLog.Bs1lesRejRemark = "";
                //            db.BS1LesRejLog.Add(lesRejLog);

                //            Bs1BLesT lesRej1;
                //            foreach (var records in lstDateCT)
                //            {
                //                lesRej1 = new Bs1BLesT();
                //                lesRej1.fkBs1BLesID = SecRejID;
                //                lesRej1.Bs1BLesStartDate = records.BCtBStartDate;
                //                lesRej1.Bs1BLesStarTime = records.BCtBStartTime;
                //                lesRej1.Bs1BLesOrigion = records.BCtBOrigin;
                //                lesRej1.Bs1BLesDestination = records.BCtBDestination;
                //                lesRej1.fkSPServTypeID = records.BCtBLesServTypeID;

                //                long dataconvertToMP = Convert.ToInt64(records.BCtBTransUsage);
                //                lesRej1.Bs1BLesTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertKilobytesToMegabytes(dataconvertToMP)), 2, MidpointRounding.AwayFromZero);
                //                lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("MB")).UID;
                //                lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_DataRateper1MB), 2, MidpointRounding.AwayFromZero);
                //                lesRej1.fkCommType = 1;

                //                db.Bs1BLesT.Add(lesRej1);
                //            }

                //            // Invoice List Report Add in BInvLstReports
                //            if (lstDateCT.Count > 0)
                //            {
                //                InvreportCost = new BInvLstReport();
                //                try
                //                {
                //                    InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeID).BsPtServiceTypes;
                //                }
                //                catch { InvreportCost.BInvRprtSerType = null; }

                //                if (InvreportCost.BInvRprtSerType != null)
                //                {
                //                    InvreportCost.BInvRprtLes1ID = SecRejID;
                //                    InvreportCost.fkBCus2ID = null;
                //                    InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_InvRptDataCostTot), 2, MidpointRounding.AwayFromZero);
                //                    InvreportCost.BInvRprtTotBil = null;
                //                    db.BInvLstReports.Add(InvreportCost);
                //                }

                //            }

                //            List<BCtB> lstVceCellCT = lstCtRecord.AsEnumerable()
                //                                 .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeMobile &&
                //                                 (t.BCtBfkFbID == id)).ToList();

                //            foreach (var records in lstVceCellCT)
                //            {
                //                lesRej1 = new Bs1BLesT();
                //                lesRej1.fkBs1BLesID = SecRejID;
                //                lesRej1.Bs1BLesStartDate = records.BCtBStartDate;
                //                lesRej1.Bs1BLesStarTime = records.BCtBStartTime;
                //                lesRej1.Bs1BLesOrigion = records.BCtBOrigin;
                //                lesRej1.Bs1BLesDestination = records.BCtBDestination;
                //                lesRej1.fkSPServTypeID = records.BCtBLesServTypeID;

                //                decimal? v2CTransSec = 0;
                //                if (records.BCtBTransUsage != null)
                //                {
                //                    v2CTransSec = getMinutes(records.BCtBTransUsage);
                //                }

                //                long SecconvertToMin = Convert.ToInt64(v2CTransSec);
                //                lesRej1.Bs1BLesTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                //                lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                //                lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_VoiceCellular), 2, MidpointRounding.AwayFromZero);
                //                lesRej1.fkCommType = 2;

                //                db.Bs1BLesT.Add(lesRej1);
                //            }

                //            // Invoice List Report Add in BInvLstReports
                //            if (lstVceCellCT.Count > 0)
                //            {
                //                InvreportCost = new BInvLstReport();
                //                try
                //                {
                //                    InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeMobile).BsPtServiceTypes;
                //                }
                //                catch { InvreportCost.BInvRprtSerType = null; }

                //                if (InvreportCost.BInvRprtSerType != null)
                //                {
                //                    InvreportCost.BInvRprtLes1ID = SecRejID;
                //                    InvreportCost.fkBCus2ID = null;
                //                    InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_VceCellularCostPriceCT), 2, MidpointRounding.AwayFromZero);
                //                    InvreportCost.BInvRprtTotBil = null;
                //                    db.BInvLstReports.Add(InvreportCost);
                //                }
                //            }

                //            List<BCtB> lstVceFixedCT = lstCtRecord.AsEnumerable()
                //                                 .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeVce2Fixed &&
                //                                 (t.BCtBfkFbID == id)).ToList();

                //            foreach (var records in lstVceFixedCT)
                //            {
                //                lesRej1 = new Bs1BLesT();
                //                lesRej1.fkBs1BLesID = SecRejID;
                //                lesRej1.Bs1BLesStartDate = records.BCtBStartDate;
                //                lesRej1.Bs1BLesStarTime = records.BCtBStartTime;
                //                lesRej1.Bs1BLesOrigion = records.BCtBOrigin;
                //                lesRej1.Bs1BLesDestination = records.BCtBDestination;
                //                lesRej1.fkSPServTypeID = records.BCtBLesServTypeID;

                //                decimal? v2FixedTransSec = 0;
                //                if (records.BCtBTransUsage != null)
                //                {
                //                    v2FixedTransSec = getMinutes(records.BCtBTransUsage);
                //                }

                //                long SecconvertToMin = Convert.ToInt64(v2FixedTransSec);
                //                lesRej1.Bs1BLesTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                //                lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                //                lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_VoiceFixed), 2, MidpointRounding.AwayFromZero);
                //                lesRej1.fkCommType = 2;

                //                db.Bs1BLesT.Add(lesRej1);
                //            }

                //            // Invoice List Report Add in BInvLstReports
                //            if (lstVceFixedCT.Count > 0)
                //            {
                //                InvreportCost = new BInvLstReport();
                //                try
                //                {
                //                    InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeVce2Fixed).BsPtServiceTypes;
                //                }
                //                catch { InvreportCost.BInvRprtSerType = null; }

                //                if (InvreportCost.BInvRprtSerType != null)
                //                {
                //                    InvreportCost.BInvRprtLes1ID = SecRejID;
                //                    InvreportCost.fkBCus2ID = null;
                //                    InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_VceFixedCostPriceCT), 2, MidpointRounding.AwayFromZero);
                //                    InvreportCost.BInvRprtTotBil = null;
                //                    db.BInvLstReports.Add(InvreportCost);
                //                }
                //            }

                //            List<BCtB> lstVceFBBCT = lstCtRecord.AsEnumerable()
                //                                 .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeVce2FBB &&
                //                                 (t.BCtBfkFbID == id)).ToList();

                //            foreach (var records in lstVceFBBCT)
                //            {
                //                lesRej1 = new Bs1BLesT();
                //                lesRej1.fkBs1BLesID = SecRejID;
                //                lesRej1.Bs1BLesStartDate = records.BCtBStartDate;
                //                lesRej1.Bs1BLesStarTime = records.BCtBStartTime;
                //                lesRej1.Bs1BLesOrigion = records.BCtBOrigin;
                //                lesRej1.Bs1BLesDestination = records.BCtBDestination;
                //                lesRej1.fkSPServTypeID = records.BCtBLesServTypeID;

                //                decimal? v2FBBTransSec = 0;
                //                if (records.BCtBTransUsage != null)
                //                {
                //                    v2FBBTransSec = getMinutes(records.BCtBTransUsage);
                //                }

                //                long SecconvertToMin = Convert.ToInt64(v2FBBTransSec);
                //                lesRej1.Bs1BLesTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                //                lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                //                lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_VoiceFBB), 2, MidpointRounding.AwayFromZero);
                //                lesRej1.fkCommType = 2;

                //                db.Bs1BLesT.Add(lesRej1);
                //            }


                //            // Invoice List Report Add in BInvLstReports
                //            if (lstVceFBBCT.Count > 0)
                //            {
                //                InvreportCost = new BInvLstReport();
                //                try
                //                {
                //                    InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeVce2FBB).BsPtServiceTypes;
                //                }
                //                catch { InvreportCost.BInvRprtSerType = null; }

                //                if (InvreportCost.BInvRprtSerType != null)
                //                {
                //                    InvreportCost.BInvRprtLes1ID = SecRejID;
                //                    InvreportCost.fkBCus2ID = null;
                //                    InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_VceFBBCostPriceCT), 2, MidpointRounding.AwayFromZero);
                //                    InvreportCost.BInvRprtTotBil = null;
                //                    db.BInvLstReports.Add(InvreportCost);
                //                }
                //            }

                //            List<BCtB> lstVceIridiumCT = lstCtRecord.AsEnumerable()
                //                                .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == LES_ServTypeVce2Iridium &&
                //                                (t.BCtBfkFbID == id)).ToList();

                //            foreach (var records in lstVceIridiumCT)
                //            {
                //                lesRej1 = new Bs1BLesT();
                //                lesRej1.fkBs1BLesID = SecRejID;
                //                lesRej1.Bs1BLesStartDate = records.BCtBStartDate;
                //                lesRej1.Bs1BLesStarTime = records.BCtBStartTime;
                //                lesRej1.Bs1BLesOrigion = records.BCtBOrigin;
                //                lesRej1.Bs1BLesDestination = records.BCtBDestination;
                //                lesRej1.fkSPServTypeID = records.BCtBLesServTypeID;

                //                decimal? v2IriTransSec = 0;
                //                if (records.BCtBTransUsage != null)
                //                {
                //                    v2IriTransSec = getMinutes(records.BCtBTransUsage);
                //                }

                //                long SecconvertToMin = Convert.ToInt64(v2IriTransSec);

                //                lesRej1.Bs1BLesTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                //                lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                //                lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_VoiceIridium), 2, MidpointRounding.AwayFromZero);
                //                lesRej1.fkCommType = 2;

                //                db.Bs1BLesT.Add(lesRej1);
                //            }

                //            // Invoice List Report Add in BInvLstReports
                //            if (lstVceIridiumCT.Count > 0)
                //            {
                //                InvreportCost = new BInvLstReport();
                //                try
                //                {
                //                    InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == LES_ServTypeVce2Iridium).BsPtServiceTypes;
                //                }
                //                catch { InvreportCost.BInvRprtSerType = null; }

                //                if (InvreportCost.BInvRprtSerType != null)
                //                {
                //                    InvreportCost.BInvRprtLes1ID = SecRejID;
                //                    InvreportCost.fkBCus2ID = null;
                //                    InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_VceIridiumCostPriceCT), 2, MidpointRounding.AwayFromZero);
                //                    InvreportCost.BInvRprtTotBil = null;
                //                    db.BInvLstReports.Add(InvreportCost);
                //                }
                //            }

                //            List<BCtB> lstVceISDNCT = lstCtRecord.AsEnumerable()
                //                               .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeVce2ISDN &&
                //                               (t.BCtBfkCommType == id)).ToList();

                //            foreach (var records in lstVceISDNCT)
                //            {
                //                lesRej1 = new Bs1BLesT();
                //                lesRej1.fkBs1BLesID = SecRejID;
                //                lesRej1.Bs1BLesStartDate = records.BCtBStartDate;
                //                lesRej1.Bs1BLesStarTime = records.BCtBStartTime;
                //                lesRej1.Bs1BLesOrigion = records.BCtBOrigin;
                //                lesRej1.Bs1BLesDestination = records.BCtBDestination;
                //                lesRej1.fkSPServTypeID = records.BCtBLesServTypeID;

                //                decimal? v2ISDNTransSec = 0;
                //                if (records.BCtBTransUsage != null)
                //                {
                //                    v2ISDNTransSec = getMinutes(records.BCtBTransUsage);
                //                }

                //                long SecconvertToMin = Convert.ToInt64(v2ISDNTransSec);

                //                lesRej1.Bs1BLesTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                //                lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                //                lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_ISDN), 2, MidpointRounding.AwayFromZero);
                //                lesRej1.fkCommType = 2;

                //                db.Bs1BLesT.Add(lesRej1);
                //            }

                //            // Invoice List Report Add in BInvLstReports
                //            if (lstVceISDNCT.Count > 0)
                //            {
                //                InvreportCost = new BInvLstReport();
                //                try
                //                {
                //                    InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeVce2ISDN).BsPtServiceTypes;
                //                }
                //                catch { InvreportCost.BInvRprtSerType = null; }

                //                if (InvreportCost.BInvRprtSerType != null)
                //                {
                //                    InvreportCost.BInvRprtLes1ID = SecRejID;
                //                    InvreportCost.fkBCus2ID = null;
                //                    InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_ISDNCostPriceCT), 2, MidpointRounding.AwayFromZero);
                //                    InvreportCost.BInvRprtTotBil = null;
                //                    db.BInvLstReports.Add(InvreportCost);
                //                }
                //            }

                //            int commtype = 1;
                //            try
                //            {
                //                commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                //            }
                //            catch
                //            {
                //                commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                //            }

                //            List<BCtB> lstSMSCT = lstCtRecord.AsEnumerable()
                //                              .Where(t => t.BCtBfkCommType == commtype && t.BCtBLesServTypeID == ServTypeSMS &&
                //                              (t.BCtBfkFbID == id)).ToList();

                //            foreach (var records in lstSMSCT)
                //            {
                //                lesRej1 = new Bs1BLesT();
                //                lesRej1.fkBs1BLesID = SecRejID;
                //                lesRej1.Bs1BLesStartDate = records.BCtBStartDate;
                //                lesRej1.Bs1BLesStarTime = records.BCtBStartTime;
                //                lesRej1.Bs1BLesOrigion = records.BCtBOrigin;
                //                lesRej1.Bs1BLesDestination = records.BCtBDestination;
                //                lesRej1.fkSPServTypeID = records.BCtBLesServTypeID;

                //                //long SecconvertToMin = Convert.ToInt64(records.BCtBTransUsage);
                //                lesRej1.Bs1BLesTransUsage = Convert.ToInt32(records.BCtBTransUsage);//(decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                //                try
                //                {
                //                    lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("message")).UID;
                //                }
                //                catch
                //                {
                //                    lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Message")).UID;
                //                }

                //                lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_SMS), 2, MidpointRounding.AwayFromZero);
                //                lesRej1.fkCommType = commtype;

                //                db.Bs1BLesT.Add(lesRej1);
                //            }

                //            // Invoice List Report Add in BInvLstReports
                //            if (lstSMSCT.Count > 0)
                //            {
                //                InvreportCost = new BInvLstReport();
                //                try
                //                {
                //                    InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeSMS).BsPtServiceTypes;
                //                }
                //                catch { InvreportCost.BInvRprtSerType = null; }

                //                if (InvreportCost.BInvRprtSerType != null)
                //                {
                //                    InvreportCost.BInvRprtLes1ID = SecRejID;
                //                    InvreportCost.fkBCus2ID = null;
                //                    InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_SMSCostPriceCT), 2, MidpointRounding.AwayFromZero);
                //                    InvreportCost.BInvRprtTotBil = null;
                //                    db.BInvLstReports.Add(InvreportCost);
                //                }
                //            }

                //            db.SaveChanges();
                //            returnMsg.AppendFormat("This msi {0}: Successfully stored Rejection Les price table.<br />", ImarSatID).AppendLine();
                //        }
                //    }
                //    catch (Exception error)
                //    {
                //        Msg.AppendFormat("Error:ID:{0}, {1}", ImarSatID, error.Message.ToString());
                //    }
                //}

            }

            return returnMsg.ToString();
        }

        // Get Les primary Key ID
        public static int getLesID()
        {
            try
            {
                using (BillingSystemEntities context = new BillingSystemEntities())
                {
                    return context.BsMtServiceProviders.FirstOrDefault(t => t.ServiceProvider.Equals(LesName)).spID;
                }
            }
            catch
            {
                return 0;
            }
        }

        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;

            try
            {
                string value = cell.CellValue.InnerXml;

                if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                {
                    return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
                }
                else
                {
                    return value;
                }
            }
            catch { return string.Empty; }


        }

        public static Tuple<DateTime?, TimeSpan?> splitDateTime(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                try
                {
                    DateTime dateValue = DateTime.Parse(input);
                    DateTime date = dateValue.Date;
                    TimeSpan time = dateValue.TimeOfDay;
                    return new Tuple<DateTime?, TimeSpan?>(date, time);

                }
                catch (FormatException)
                {
                    return new Tuple<DateTime?, TimeSpan?>(null, null);
                }
            }
            else
                return new Tuple<DateTime?, TimeSpan?>(null, null);
        }

        public bool MoveRejection_Invoice(BillingSystemEntities context, BS1ALessT rejMtLes)
        {
            try
            {
                decimal? SMTS_TotalCost = 0;
                decimal? Les_TotalCostCP = 0;
                decimal? Cus_TotalCostSP = 0;

                try
                {
                    var secLes = rejMtLes;
                    if (secLes != null)
                    {
                        int CUS_Id = 0;
                        int CUS_ContractID = 0;
                        int CUS_RegsID = 0;

                        decimal? Cus_DataPlan = 0;
                        decimal? Cus_OutbundleRate = 0;
                        decimal? CUS_RecurFee = 0;
                        decimal? CUS_DataRateper1MB = 0;
                        decimal? CUS_VoiceCellular = 0;
                        decimal? CUS_VoiceFixed = 0;
                        decimal? CUS_VoiceFBB = 0;
                        decimal? CUS_VoiceIridium = 0;
                        decimal? CUS_ISDN = 0;
                        decimal? CUS_SMS = 0;

                        int? LES_ContractID = 0;
                        int LES_RegsID = 0;

                        decimal? LES_DataPlan = 0;
                        decimal? LES_OutbundleRate = 0;
                        decimal? LES_RecurFee = 0;
                        decimal? LES_DataRateper1MB = 0;
                        decimal? LES_VoiceCellular = 0;
                        decimal? LES_VoiceFixed = 0;
                        decimal? LES_VoiceFBB = 0;
                        decimal? LES_VoiceIridium = 0;
                        decimal? LES_ISDN = 0;
                        decimal? LES_SMS = 0;

                        bool is_CUSGBPlan = false;
                        bool is_LESGBPlan = false;
                        string duration = string.Empty;

                        var cusRegs = context.BsPtCusRegs_.FirstOrDefault(t => t.fkSimID == secLes.fkSimID);
                        if (cusRegs != null)
                        {
                            CUS_Id = Convert.ToInt32(cusRegs.fkCusID);
                            CUS_ContractID = Convert.ToInt32(cusRegs.fkCuscID);
                            CUS_RegsID = Convert.ToInt32(cusRegs.Cus_RegID);

                            CUS_RecurFee = cusRegs.BPtCusRgnFbRecurFee;

                            Cus_DataPlan = Convert.ToDecimal(cusRegs.BPtCusRgnFBDataPlanIn);
                            Cus_OutbundleRate = cusRegs.BPtCusRgnFbOutBundleDataRate;

                            CUS_DataRateper1MB = cusRegs.BPtCusRgnFbInbundleDataRate;


                            string unit = cusRegs.BsMtUnit.Units;

                            if (unit.Contains("GB"))
                                is_CUSGBPlan = true;
                            else
                                is_CUSGBPlan = false;
                        }

                        List<BsPtCusServiceDest> lstSP = context.BsPtCusServiceDests.Where(t => t.fkSpID == LesID).ToList();


                        try
                        {
                            CUS_VoiceCellular = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                     t.BsPtCusServiceTypes.Equals("Voice  to Cellular")).BsPtCusServiceSellerPrice;
                        }
                        catch { CUS_VoiceCellular = 0; }

                        try
                        {
                            CUS_VoiceFixed = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                  t.BsPtCusServiceTypes.Equals("Voice  to Fixed")).BsPtCusServiceSellerPrice;
                        }
                        catch { CUS_VoiceFixed = 0; }

                        try
                        {
                            CUS_VoiceFBB = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                t.BsPtCusServiceTypes.Equals("Voice  to FleetBroadband")).BsPtCusServiceSellerPrice;
                        }
                        catch { CUS_VoiceFBB = 0; }

                        try
                        {
                            CUS_VoiceIridium = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                    t.BsPtCusServiceTypes.Equals("Voice  to Iridium")).BsPtCusServiceSellerPrice;
                        }
                        catch { CUS_VoiceIridium = 0; }

                        try
                        {
                            CUS_ISDN = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                            t.BsPtCusServiceTypes.Equals("ISDN")).BsPtCusServiceSellerPrice;
                        }
                        catch { CUS_ISDN = 0; }

                        try
                        {
                            CUS_SMS = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                           t.BsPtCusServiceTypes.Equals("SMS")).BsPtCusServiceSellerPrice;
                        }
                        catch { CUS_SMS = 0; }

                        List<BsPtSPServiceDest> lstCP = context.BsPtSPServiceDests.Where(t => t.fkSpID == LesID).ToList();
                        try
                        {

                            var Les_Reg = context.BsPtSPLesRegs.FirstOrDefault(t => t.fkSPID == LesID && t.fkSimID == secLes.fkSimID);
                            LES_RegsID = Les_Reg.SPLesRegID;
                            LES_ContractID = Les_Reg.fkspcID;
                            LES_RecurFee = Les_Reg.BPtLesRgnFbRecurFee;
                            LES_DataPlan = Convert.ToDecimal(Les_Reg.BPtLesRgnFBDataPlanIn);

                            LES_DataRateper1MB = Les_Reg.BPtLesRgnFbInbundleDataRate;
                            LES_OutbundleRate = Les_Reg.BPtLesRgnFbOutBundleDataRate;

                            string unit = Les_Reg.BsMtUnit.Units;
                            if (unit.Contains("GB"))
                                is_LESGBPlan = true;
                            else
                                is_LESGBPlan = false;

                            Les_TotalCostCP += LES_RecurFee;
                        }
                        catch { LES_RecurFee = 0; }

                        try
                        {
                            LES_VoiceCellular = lstCP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegsID &&
                                                                     t.BsPtServiceTypes.Equals("Voice  to Cellular")).BsPtServiceCostPrice;
                        }
                        catch { LES_VoiceCellular = 0; }
                        try
                        {
                            LES_VoiceFixed = lstCP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegsID &&
                                                                  t.BsPtServiceTypes.Equals("Voice  to Fixed")).BsPtServiceCostPrice;
                        }
                        catch { LES_VoiceFixed = 0; }
                        try
                        {
                            LES_VoiceFBB = lstCP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegsID &&
                                                                t.BsPtServiceTypes.Equals("Voice  to FleetBroadband")).BsPtServiceCostPrice;
                        }
                        catch { LES_VoiceFBB = 0; }
                        try
                        {
                            LES_VoiceIridium = lstCP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegsID &&
                                                                    t.BsPtServiceTypes.Equals("Voice  to Iridium")).BsPtServiceCostPrice;
                        }
                        catch { LES_VoiceIridium = 0; }
                        try
                        {
                            LES_ISDN = lstCP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegsID &&
                                                            t.BsPtServiceTypes.Equals("ISDN")).BsPtServiceCostPrice;
                        }
                        catch { LES_ISDN = 0; }
                        try
                        {
                            LES_SMS = lstCP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegsID &&
                                                           t.BsPtServiceTypes.Equals("SMS")).BsPtServiceCostPrice;
                        }
                        catch { LES_SMS = 0; }

                        string InvoiceNo = string.Empty;

                        try
                        {
                            InvoiceNo = context.BS2MtCusT.Max(t => t.BSMtCusInvNo);

                            if (!string.IsNullOrEmpty(InvoiceNo))
                            {
                                if (InvoiceNo.Contains("SM"))
                                {
                                    string tempInv = InvoiceNo.Replace("SM", string.Empty);
                                    if (int.TryParse(tempInv, out int result))
                                    {
                                        InvoiceNo = BillingHelpers.GenInvoice(false, result);
                                    }
                                }
                            }
                            else
                                InvoiceNo = BillingHelpers.GenInvoice(false, 0);
                        }
                        catch (Exception error)
                        {
                            //ErrorMsg.AppendFormat("MSI:{0} Error Msg: {1} Stack Trace: {2}. <br />", Imsi, error.Message.ToString(), error.StackTrace);
                        }

                        if (!string.IsNullOrEmpty(InvoiceNo))
                        {
                            BS2MtCusT SecMtCust = new BS2MtCusT();

                            SecMtCust.BSMtCusInvNo = InvoiceNo;
                            SecMtCust.fkSPID = secLes.fkSPID;
                            SecMtCust.fkCusID = CUS_Id;
                            SecMtCust.fkCusConID = CUS_ContractID;
                            SecMtCust.fkCusRegID = CUS_RegsID;
                            SecMtCust.fkspcID = secLes.fkspcID;
                            SecMtCust.fkSimID = secLes.fkSimID;
                            SecMtCust.fkLesRefID = secLes.fkLesRefID;
                            SecMtCust.fkSatTypeID = secLes.fkSatTypeID;
                            SecMtCust.fkEquipTypeID = secLes.fkEquipTypeID;
                            SecMtCust.BSMtCusLesCode = secLes.BS1ALesLesCode;
                            SecMtCust.BSMtCusCdrCode = secLes.BS1ALesCdrCode;
                            SecMtCust.BSMtCusSatType = null;
                            SecMtCust.BSMtCusServDest = secLes.BS1ALesServDest;
                            duration = secLes.BS1ALesServDest;
                            SecMtCust.BSMtCusServType = secLes.BS1ALesServType;
                            SecMtCust.BSMtCusTransUsage = secLes.BS1ALesTransUsage;
                            SecMtCust.BSMtCusUOM = secLes.BS1ALesUOM;
                            SecMtCust.BSMtCusRecurFee = CUS_RecurFee;
                            Cus_TotalCostSP += SecMtCust.BSMtCusRecurFee;
                            SecMtCust.BSMtCusRecurFeeType = secLes.BS1ALesRecurFeeType;
                            SecMtCust.BSMtCusCDRRef = secLes.BS1ALesCDRRef;
                            SecMtCust.BSMtCusBillNo = secLes.BS1ALesBillNo;
                            SecMtCust.BSMtCusBillPeriod = secLes.BS1ALesBillPeriod;
                            SecMtCust.BSMtCusBillDate = secLes.BS1ALesBillDate;

                            context.BS2MtCusT.Add(SecMtCust);
                            context.SaveChanges();

                            int id = SecMtCust.BSMtCusTID;

                            int LES_StndIPServiceIP = 0;
                            int LES_V2CellServiceID = 0;
                            int LES_V2FixedServiceID = 0;
                            int LES_V2FBBServiceID = 0;
                            int LES_V2IridiServiceID = 0;
                            int LES_ISDNServiceID = 0;
                            int LES_SMSServiceID = 0;

                            int CUS_StndIPServiceIP = 0;
                            int CUS_V2CellServiceID = 0;
                            int CUS_V2FixedServiceID = 0;
                            int CUS_V2FBBServiceID = 0;
                            int CUS_V2IridiServiceID = 0;
                            int CUS_ISDNServiceID = 0;
                            int CUS_SMSServiceID = 0;

                            BInvLstReport InvReport = new BInvLstReport();
                            List<BInvLstReport> lstInvreport = context.BInvLstReports.Where(t => t.BInvRprtLes1ID == secLes.BS1ALesID).ToList();

                            try
                            {
                                InvReport = lstInvreport.FirstOrDefault(t => t.BInvRprtLes1ID == secLes.BS1ALesID && t.BInvRprtSerType.ToLower().Equals(SecMtCust.BSMtCusServType.ToLower()));
                                if (InvReport != null)
                                {
                                    InvReport.fkBCus2ID = SecMtCust.BSMtCusTID;
                                    InvReport.BInvRprtSerType = SecMtCust.BSMtCusServType;
                                    InvReport.BInvRprtTotBil = SecMtCust.BSMtCusRecurFee;
                                }
                            }
                            catch { }



                            List<Bs1BLesT> lstRecord = context.Bs1BLesT.Where(t => t.fkBs1BLesID == secLes.BS1ALesID).ToList();

                            CUS_StndIPServiceIP = context.BsPtCusServiceDests.FirstOrDefault(t => t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                             t.BsPtCusServiceTypes.ToLower().Equals("standard ip")).CusServDID;

                            try
                            {
                                LES_StndIPServiceIP = context.BsPtSPServiceDests.FirstOrDefault(t => t.BPtServTfkLesRegID == LES_RegsID &&
                                                                                                t.BsPtServiceTypes.ToLower().Equals("standard ip")).SPServDID;


                            }
                            catch
                            {
                                LES_StndIPServiceIP = lstRecord.AsEnumerable().FirstOrDefault(t => t.BsPtSPServiceDest.BsPtServiceTypes.ToLower().Equals("standard ip")).fkSPServTypeID;
                            }

                            List<Bs1BLesT> lstDateCT = lstRecord.AsEnumerable()
                                                 .Where(t => t.fkCommType == 1 && t.fkSPServTypeID == LES_StndIPServiceIP && (t.fkBs1BLesID == secLes.BS1ALesID)).ToList();
                            Bs2Sec bs2Sec;

                            if (is_CUSGBPlan)
                                Cus_DataPlan = Cus_DataPlan * 1024;

                            if (is_LESGBPlan)
                                LES_DataPlan = LES_DataPlan * 1024;

                            decimal? outbundleData = 0;
                            decimal? Cus_InvRptTotalBill = 0;
                            foreach (var records in lstDateCT)
                            {
                                bs2Sec = new Bs2Sec();
                                bs2Sec.fkCTFbID = id;
                                bs2Sec.BsCtStartDate = records.Bs1BLesStartDate;
                                bs2Sec.BsCtStarTime = records.Bs1BLesStarTime;
                                bs2Sec.BsCtOrigion = records.Bs1BLesOrigion;
                                bs2Sec.BsCtDestination = records.Bs1BLesDestination;
                                bs2Sec.fkSPServTypeID = CUS_StndIPServiceIP;

                                //long dataconvertToMP = Convert.ToInt64(records.Bs1LesBillIncre);
                                bs2Sec.BsCtTransUsage = records.Bs1BLesTransUsage;
                                bs2Sec.fkUOMID = context.BsMtUnits.FirstOrDefault(t => t.Units.Equals("MB")).UID;
                                outbundleData += bs2Sec.BsCtBillIncre;

                                if (Cus_DataPlan <= outbundleData)
                                {
                                    bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * Cus_OutbundleRate), 2, MidpointRounding.AwayFromZero);
                                    Cus_InvRptTotalBill += bs2Sec.BsCtTotalUsage;
                                }
                                else
                                {
                                    bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * 0), 2, MidpointRounding.AwayFromZero);
                                    Cus_InvRptTotalBill += bs2Sec.BsCtTotalUsage;
                                }

                                bs2Sec.fkCommType = 1;

                                Cus_TotalCostSP += bs2Sec.BsCtTotalUsage;

                                if (LES_DataPlan <= outbundleData)
                                    Les_TotalCostCP += Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * LES_OutbundleRate), 2, MidpointRounding.AwayFromZero);
                                else
                                    Les_TotalCostCP += Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * 0), 2, MidpointRounding.AwayFromZero);

                                context.Bs2Sec.Add(bs2Sec);
                                // context.Bs1LesT.Remove(records);
                            }

                            try
                            {
                                if (lstDateCT.Count() > 0)
                                {
                                    InvReport = new BInvLstReport();
                                    InvReport = lstInvreport.FirstOrDefault(t => t.BInvRprtSerType.ToLower().Equals("standard ip"));
                                    if (InvReport != null)
                                    {
                                        InvReport.fkBCus2ID = SecMtCust.BSMtCusTID;
                                        InvReport.BInvRprtLes1ID = null;
                                        InvReport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(Cus_InvRptTotalBill), 2, MidpointRounding.AwayFromZero);
                                    }
                                }
                            }
                            catch { }

                            CUS_V2CellServiceID = context.BsPtCusServiceDests.FirstOrDefault(t => t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                t.BsPtCusServiceTypes.ToLower().Equals("voice  to cellular")).CusServDID;

                            try
                            {
                                LES_V2CellServiceID = context.BsPtSPServiceDests.FirstOrDefault(t => t.BPtServTfkLesRegID == LES_RegsID &&
                                                                                                t.BsPtServiceTypes.ToLower().Equals("voice  to cellular")).SPServDID;
                            }
                            catch
                            {
                                LES_V2CellServiceID = lstRecord.AsEnumerable().FirstOrDefault(t => t.BsPtSPServiceDest.BsPtServiceTypes.ToLower().Equals("voice  to cellular")).fkSPServTypeID;
                            }

                            List<Bs1BLesT> lstVceCell = lstRecord.AsEnumerable()
                                                 .Where(t => t.fkCommType == 2 && t.fkSPServTypeID == LES_V2CellServiceID &&
                                                 (t.fkBs1BLesID == secLes.BS1ALesID)).ToList();

                            decimal? Cus_InvRptTotalBill_V2Cell = 0;

                            foreach (var records in lstVceCell)
                            {
                                bs2Sec = new Bs2Sec();
                                bs2Sec.fkCTFbID = id;
                                bs2Sec.BsCtStartDate = records.Bs1BLesStartDate;
                                bs2Sec.BsCtStarTime = records.Bs1BLesStarTime;
                                bs2Sec.BsCtOrigion = records.Bs1BLesOrigion;
                                bs2Sec.BsCtDestination = records.Bs1BLesDestination;
                                bs2Sec.fkSPServTypeID = CUS_V2CellServiceID;

                                //long SecconvertToMin = Convert.ToInt64(records.BsCtBillIncre);
                                bs2Sec.BsCtTransUsage = records.Bs1BLesTransUsage;// (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkUOMID = context.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_VoiceCellular), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkCommType = 2;

                                Cus_InvRptTotalBill_V2Cell += bs2Sec.BsCtTotalUsage;
                                Cus_TotalCostSP += bs2Sec.BsCtTotalUsage;
                                Les_TotalCostCP += Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * LES_VoiceCellular), 2, MidpointRounding.AwayFromZero);

                                context.Bs2Sec.Add(bs2Sec);

                                //   context.Bs1LesT.Remove(records);
                            }

                            // Voice to Cell invoice list
                            try
                            {
                                if (lstVceCell.Count() > 0)
                                {
                                    InvReport = new BInvLstReport();
                                    InvReport = lstInvreport.FirstOrDefault(t => t.BInvRprtSerType.ToLower().Equals("voice  to cellular"));
                                    if (InvReport != null)
                                    {
                                        InvReport.fkBCus2ID = SecMtCust.BSMtCusTID;
                                        InvReport.BInvRprtLes1ID = null;
                                        InvReport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(Cus_InvRptTotalBill_V2Cell), 2, MidpointRounding.AwayFromZero);
                                    }
                                }
                            }
                            catch { }

                            CUS_V2FixedServiceID = context.BsPtCusServiceDests.FirstOrDefault(t => t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                t.BsPtCusServiceTypes.ToLower().Equals("voice  to fixed")).CusServDID;
                            try
                            {
                                LES_V2FixedServiceID = context.BsPtSPServiceDests.FirstOrDefault(t => t.BPtServTfkLesRegID == LES_RegsID &&
                                                                                                t.BsPtServiceTypes.ToLower().Equals("voice  to fixed")).SPServDID;
                            }
                            catch
                            {
                                LES_V2FixedServiceID = lstRecord.AsEnumerable().FirstOrDefault(t => t.BsPtSPServiceDest.BsPtServiceTypes.ToLower().Equals("voice  to fixed")).fkSPServTypeID;
                            }

                            List<Bs1BLesT> lstVceFixed = lstRecord.AsEnumerable()
                                                 .Where(t => t.fkCommType == 2 && t.fkSPServTypeID == LES_V2FixedServiceID &&
                                                 (t.fkBs1BLesID == secLes.BS1ALesID)).ToList();

                            decimal? Cus_InvRptTotalBill_V2Fixed = 0;
                            foreach (var records in lstVceFixed)
                            {
                                bs2Sec = new Bs2Sec();
                                bs2Sec.fkCTFbID = id;
                                bs2Sec.BsCtStartDate = records.Bs1BLesStartDate;
                                bs2Sec.BsCtStarTime = records.Bs1BLesStarTime;
                                bs2Sec.BsCtOrigion = records.Bs1BLesOrigion;
                                bs2Sec.BsCtDestination = records.Bs1BLesDestination;
                                bs2Sec.fkSPServTypeID = CUS_V2FixedServiceID;

                                //long SecconvertToMin = Convert.ToInt64(records.Bs1LesBillIncre);
                                bs2Sec.BsCtTransUsage = records.Bs1BLesTransUsage; //(decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkUOMID = context.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_VoiceFixed), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkCommType = 2;

                                Cus_TotalCostSP += bs2Sec.BsCtTotalUsage;
                                Cus_InvRptTotalBill_V2Fixed += bs2Sec.BsCtTotalUsage;
                                Les_TotalCostCP += Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * LES_VoiceFixed), 2, MidpointRounding.AwayFromZero);

                                context.Bs2Sec.Add(bs2Sec);
                            }

                            try
                            {
                                // voice to fixed invoice list
                                if (lstVceFixed.Count() > 0)
                                {
                                    InvReport = new BInvLstReport();
                                    InvReport = lstInvreport.FirstOrDefault(t => t.BInvRprtSerType.ToLower().Equals("voice  to fixed"));
                                    if (InvReport != null)
                                    {
                                        InvReport.fkBCus2ID = SecMtCust.BSMtCusTID;
                                        InvReport.BInvRprtLes1ID = null;
                                        InvReport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(Cus_InvRptTotalBill_V2Fixed), 2, MidpointRounding.AwayFromZero);
                                    }
                                }
                            }
                            catch { }

                            CUS_V2FBBServiceID = context.BsPtCusServiceDests.FirstOrDefault(t => t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                t.BsPtCusServiceTypes.ToLower().Equals("voice  to fleetbroadband")).CusServDID;

                            try
                            {
                                LES_V2FBBServiceID = context.BsPtSPServiceDests.FirstOrDefault(t => t.BPtServTfkLesRegID == LES_RegsID &&
                                                                                                t.BsPtServiceTypes.ToLower().Equals("voice  to fleetbroadband")).SPServDID;
                            }
                            catch
                            {
                                LES_V2FBBServiceID = lstRecord.AsEnumerable().FirstOrDefault(t => t.BsPtSPServiceDest.BsPtServiceTypes.ToLower().Equals("voice  to fleetbroadband")).fkSPServTypeID;
                            }

                            List<Bs1BLesT> lstVce2FBB = lstRecord.AsEnumerable()
                                                 .Where(t => t.fkCommType == 2 && t.fkSPServTypeID == LES_V2FBBServiceID && (t.fkBs1BLesID == secLes.BS1ALesID)).ToList();

                            decimal? Cus_InvRptTotalBill_V2FBB = 0;
                            foreach (var records in lstVce2FBB)
                            {
                                bs2Sec = new Bs2Sec();
                                bs2Sec.fkCTFbID = id;
                                bs2Sec.BsCtStartDate = records.Bs1BLesStartDate;
                                bs2Sec.BsCtStarTime = records.Bs1BLesStarTime;
                                bs2Sec.BsCtOrigion = records.Bs1BLesOrigion;
                                bs2Sec.BsCtDestination = records.Bs1BLesDestination;
                                bs2Sec.fkSPServTypeID = CUS_V2FBBServiceID;

                                //long SecconvertToMin = Convert.ToInt64(records.Bs1LesBillIncre);
                                bs2Sec.BsCtTransUsage = records.Bs1BLesTransUsage; //(decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkUOMID = context.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_VoiceFBB), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkCommType = 2;

                                Cus_TotalCostSP += bs2Sec.BsCtTotalUsage;
                                Cus_InvRptTotalBill_V2FBB += bs2Sec.BsCtTotalUsage;
                                Les_TotalCostCP += Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * LES_VoiceFBB), 2, MidpointRounding.AwayFromZero);

                                context.Bs2Sec.Add(bs2Sec);

                                //context.Bs1LesT.Remove(records);
                            }

                            try
                            {
                                // voice to fbb invoice list
                                if (lstVce2FBB.Count() > 0)
                                {
                                    InvReport = new BInvLstReport();
                                    InvReport = lstInvreport.FirstOrDefault(t => t.BInvRprtSerType.ToLower().Equals("voice  to fleetbroadband"));
                                    if (InvReport != null)
                                    {
                                        InvReport.fkBCus2ID = SecMtCust.BSMtCusTID;
                                        InvReport.BInvRprtLes1ID = null;
                                        InvReport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(Cus_InvRptTotalBill_V2FBB), 2, MidpointRounding.AwayFromZero);
                                    }
                                }
                            }
                            catch { }

                            CUS_V2IridiServiceID = context.BsPtCusServiceDests.FirstOrDefault(t => t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                t.BsPtCusServiceTypes.ToLower().Equals("voice  to iridium")).CusServDID;

                            try
                            {
                                LES_V2IridiServiceID = context.BsPtSPServiceDests.FirstOrDefault(t => t.BPtServTfkLesRegID == LES_RegsID &&
                                                                                                t.BsPtServiceTypes.ToLower().Equals("voice  to iridium")).SPServDID;
                            }
                            catch
                            {
                                LES_V2IridiServiceID = lstRecord.AsEnumerable().FirstOrDefault(t => t.BsPtSPServiceDest.BsPtServiceTypes.ToLower().Equals("voice  to iridium")).fkSPServTypeID;
                            }

                            List<Bs1BLesT> lstVce2Iridium = lstRecord.AsEnumerable()
                                               .Where(t => t.fkCommType == 2 && t.fkSPServTypeID == LES_V2IridiServiceID &&
                                               (t.fkBs1BLesID == secLes.BS1ALesID)).ToList();

                            decimal? Cus_InvRptTotalBill_V2Iridium = 0;
                            foreach (var records in lstVce2Iridium)
                            {
                                bs2Sec = new Bs2Sec();
                                bs2Sec.fkCTFbID = id;
                                bs2Sec.BsCtStartDate = records.Bs1BLesStartDate;
                                bs2Sec.BsCtStarTime = records.Bs1BLesStarTime;
                                bs2Sec.BsCtOrigion = records.Bs1BLesOrigion;
                                bs2Sec.BsCtDestination = records.Bs1BLesDestination;
                                bs2Sec.fkSPServTypeID = CUS_V2IridiServiceID;

                                //long SecconvertToMin = Convert.ToInt64(records.Bs1LesBillIncre);
                                bs2Sec.BsCtTransUsage = records.Bs1BLesTransUsage; //(decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkUOMID = context.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_VoiceIridium), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkCommType = 2;

                                Cus_TotalCostSP += bs2Sec.BsCtTotalUsage;
                                Cus_InvRptTotalBill_V2Iridium += bs2Sec.BsCtTotalUsage;
                                Les_TotalCostCP += Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * LES_VoiceIridium), 2, MidpointRounding.AwayFromZero);

                                context.Bs2Sec.Add(bs2Sec);
                            }

                            try
                            {
                                // voice to iridium invoice listing
                                if (lstVce2Iridium.Count() > 0)
                                {
                                    InvReport = new BInvLstReport();
                                    InvReport = lstInvreport.FirstOrDefault(t => t.BInvRprtSerType.ToLower().Equals("voice  to iridium"));
                                    if (InvReport != null)
                                    {
                                        InvReport.fkBCus2ID = SecMtCust.BSMtCusTID;
                                        InvReport.BInvRprtLes1ID = null;
                                        InvReport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(Cus_InvRptTotalBill_V2Iridium), 2, MidpointRounding.AwayFromZero);
                                    }
                                }
                            }
                            catch { }

                            CUS_ISDNServiceID = context.BsPtCusServiceDests.FirstOrDefault(t => t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                               t.BsPtCusServiceTypes.ToUpper().Equals("ISDN")).CusServDID;

                            try
                            {
                                LES_ISDNServiceID = context.BsPtSPServiceDests.FirstOrDefault(t => t.BPtServTfkLesRegID == LES_RegsID &&
                                                                                                t.BsPtServiceTypes.ToUpper().Equals("ISDN")).SPServDID;
                            }
                            catch
                            {
                                LES_ISDNServiceID = lstRecord.AsEnumerable().FirstOrDefault(t => t.BsPtSPServiceDest.BsPtServiceTypes.ToUpper().Equals("ISDN")).fkSPServTypeID;
                            }

                            List<Bs1BLesT> lstISDN = lstRecord.AsEnumerable()
                                              .Where(t => t.fkCommType == 2 && t.fkSPServTypeID == LES_ISDNServiceID &&
                                              (t.fkBs1BLesID == secLes.BS1ALesID)).ToList();

                            decimal? Cus_InvRptTotalBill_isdn = 0;
                            foreach (var records in lstISDN)
                            {
                                bs2Sec = new Bs2Sec();
                                bs2Sec.fkCTFbID = id;
                                bs2Sec.BsCtStartDate = records.Bs1BLesStartDate;
                                bs2Sec.BsCtStarTime = records.Bs1BLesStarTime;
                                bs2Sec.BsCtOrigion = records.Bs1BLesOrigion;
                                bs2Sec.BsCtDestination = records.Bs1BLesDestination;
                                bs2Sec.fkSPServTypeID = CUS_ISDNServiceID;

                                //long SecconvertToMin = Convert.ToInt64(records.Bs1LesBillIncre);
                                bs2Sec.BsCtTransUsage = records.Bs1BLesTransUsage; //(decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkUOMID = context.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_ISDN), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkCommType = 2;

                                Cus_TotalCostSP += bs2Sec.BsCtTotalUsage;
                                Cus_InvRptTotalBill_isdn += bs2Sec.BsCtTotalUsage;
                                Les_TotalCostCP += Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * LES_ISDN), 2, MidpointRounding.AwayFromZero);

                                context.Bs2Sec.Add(bs2Sec);
                            }

                            try
                            {
                                // voice to isdn invoice listing
                                if (lstISDN.Count() > 0)
                                {
                                    InvReport = new BInvLstReport();
                                    InvReport = lstInvreport.FirstOrDefault(t => t.BInvRprtSerType.ToLower().Equals("isdn"));
                                    if (InvReport != null)
                                    {
                                        InvReport.fkBCus2ID = SecMtCust.BSMtCusTID;
                                        InvReport.BInvRprtLes1ID = null;
                                        InvReport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(Cus_InvRptTotalBill_isdn), 2, MidpointRounding.AwayFromZero);
                                    }
                                }
                            }
                            catch { }

                            int commtype = 1;
                            try
                            {
                                commtype = context.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                            }
                            catch
                            {
                                commtype = context.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                            }

                            CUS_SMSServiceID = context.BsPtCusServiceDests.FirstOrDefault(t => t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                t.BsPtCusServiceTypes.ToUpper().Equals("SMS")).CusServDID;

                            try
                            {
                                LES_SMSServiceID = context.BsPtSPServiceDests.FirstOrDefault(t => t.BPtServTfkLesRegID == LES_RegsID &&
                                                                                                t.BsPtServiceTypes.ToUpper().Equals("SMS")).SPServDID;
                            }
                            catch
                            {
                                LES_SMSServiceID = lstRecord.AsEnumerable().FirstOrDefault(t => t.BsPtSPServiceDest.BsPtServiceTypes.ToUpper().Equals("SMS")).fkSPServTypeID;
                            }

                            List<Bs1BLesT> lstSMS = lstRecord.AsEnumerable()
                                            .Where(t => t.fkCommType == commtype && t.fkSPServTypeID == LES_SMSServiceID &&
                                            (t.fkBs1BLesID == secLes.BS1ALesID)).ToList();

                            decimal? Cus_InvRptTotalBill_sms = 0;
                            foreach (var records in lstSMS)
                            {
                                bs2Sec = new Bs2Sec();
                                bs2Sec.fkCTFbID = id;
                                bs2Sec.BsCtStartDate = records.Bs1BLesStartDate;
                                bs2Sec.BsCtStarTime = records.Bs1BLesStarTime;
                                bs2Sec.BsCtOrigion = records.Bs1BLesOrigion;
                                bs2Sec.BsCtDestination = records.Bs1BLesDestination;
                                bs2Sec.fkSPServTypeID = CUS_SMSServiceID;

                                //long SecconvertToMin = Convert.ToInt64(records.Bs1LesBillIncre);
                                bs2Sec.BsCtTransUsage = records.Bs1BLesTransUsage; //(decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                try
                                {
                                    bs2Sec.fkUOMID = context.BsMtUnits.FirstOrDefault(t => t.Units.Equals("message")).UID;
                                }
                                catch
                                {
                                    bs2Sec.fkUOMID = context.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Message")).UID;
                                }

                                bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_SMS), 2, MidpointRounding.AwayFromZero);
                                bs2Sec.fkCommType = commtype;

                                Cus_InvRptTotalBill_sms += bs2Sec.BsCtTotalUsage;
                                Cus_TotalCostSP += bs2Sec.BsCtTotalUsage;
                                Les_TotalCostCP += Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * LES_SMS), 2, MidpointRounding.AwayFromZero);

                                context.Bs2Sec.Add(bs2Sec);
                            }

                            try
                            {
                                // sms invoice listing
                                if (lstSMS.Count() > 0)
                                {
                                    InvReport = new BInvLstReport();
                                    InvReport = lstInvreport.FirstOrDefault(t => t.BInvRprtSerType.ToLower().Equals("sms"));
                                    if (InvReport != null)
                                    {
                                        InvReport.fkBCus2ID = SecMtCust.BSMtCusTID;
                                        InvReport.BInvRprtLes1ID = null;
                                        InvReport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(Cus_InvRptTotalBill_sms), 2, MidpointRounding.AwayFromZero);
                                    }
                                }
                            }
                            catch { }

                            foreach (var lessT in lstDateCT)
                                context.Bs1BLesT.Remove(lessT);

                            foreach (var lessT in lstVceCell)
                                context.Bs1BLesT.Remove(lessT);

                            foreach (var lessT in lstVceFixed)
                                context.Bs1BLesT.Remove(lessT);

                            foreach (var lessT in lstVce2FBB)
                                context.Bs1BLesT.Remove(lessT);

                            foreach (var lessT in lstVce2Iridium)
                                context.Bs1BLesT.Remove(lessT);

                            foreach (var lessT in lstISDN)
                                context.Bs1BLesT.Remove(lessT);

                            foreach (var lessT in lstSMS)
                                context.Bs1BLesT.Remove(lessT);

                            var rejLog = context.BS1LesRejLog.Where(t => t.fkBs1LesReg == secLes.BS1ALesID).ToList();
                            foreach (var rej in rejLog)
                                context.BS1LesRejLog.Remove(rej);

                            context.BS1ALessT.Remove(secLes);

                            // List of Satatement 
                            Bs2Statement statment = null;
                            try
                            {
                                statment = context.Bs2Statement.FirstOrDefault(t => t.fkBs2CusID == id);
                            }
                            catch { }


                            //if (statment == null)
                            //{
                            //    statment = new Bs2Statement();

                            //    statment.fkBs2CusID = id;
                            //    statment.fkSimID = secLes.fkSimID;
                            //    statment.Bs2StTotalCP = Les_TotalCostCP;
                            //    statment.Bs2StTotalSP = Cus_TotalCostSP;
                            //    try
                            //    {
                            //        int smtscp = context.BCtAs.FirstOrDefault(t => t.BCtAFbServDest.Equals(duration)).BCtAID;
                            //        decimal? recurFee = context.BCtAs.FirstOrDefault(t => t.BCtAID == smtscp).BCtAFbRecurFee;
                            //        recurFee += context.BCtBs.Where(t => t.BCtBfkFbID == smtscp).Sum(t => t.BCtBTotalUsage);
                            //        SMTS_TotalCost += Math.Round(Convert.ToDecimal(recurFee), 2, MidpointRounding.AwayFromZero);
                            //    }
                            //    catch { }
                            //    statment.Bs2StSMTSCost = SMTS_TotalCost;
                            //    statment.fkSPID = secLes.fkSPID;
                            //    statment.fkCusID = CUS_Id;
                            //    statment.Bs2StIvoiceDate = DateTime.Now.Date;
                            //    statment.Bs2StDuration = duration;

                            //    if (statment.Bs2StTotalCP > 0 && statment.Bs2StTotalSP > 0)
                            //    {
                            //        decimal CrossProfit = Convert.ToDecimal(statment.Bs2StTotalSP - statment.Bs2StSMTSCost);
                            //        decimal Margin = (CrossProfit / Convert.ToDecimal(statment.Bs2StTotalSP) * 100);
                            //        statment.Bs2stMargin = Margin > 0 ? Convert.ToInt32(Margin) : (int)Math.Abs(Margin);
                            //    }

                            //    context.Bs2Statement.Add(statment);
                            //}
                        }
                    }
                }
                catch (Exception erorr)
                {
                    throw erorr;
                }

                context.SaveChanges();
                return true;
            }
            catch (Exception error)
            {
                throw;
            }
        }

        public Tuple<Stream, string> GnerateManualInvoice(List<OteSatCtVM> lstID)
        {
            System.IO.Stream spreadsheetstream = new System.IO.MemoryStream();

            string Invoiceno = string.Empty;

            int value = 0;

            try
            {
                string imsiID = string.Empty;
                string invoiceNo = string.Empty;
                string ImarsatID = string.Empty;

                string buildingName = string.Empty;
                string street = string.Empty;
                string district = string.Empty;
                string city = string.Empty;
                string country = string.Empty;

                string VesselName = string.Empty;

                string CustomerName = string.Empty;

                BillingSystemEntities db = new BillingSystemEntities();

                DateTime dt = DateTime.Now;
                string Period = dt.ToString("MMM");
                DateTime dy = DateTime.Today;
                string currYear = dy.Year.ToString();

                string CurrPeriod = Period + '-' + currYear;
                string InvoiceFileName = invoiceNo + '_' + buildingName + '_' + VesselName;

                string pathid = Guid.NewGuid().ToString() + " " + DateTime.Now.Date.ToString("dd-MM-yyyy");

                #region WorkBook Prepartion

                XLWorkbook xLWorkbook = new XLWorkbook();
                IXLWorksheet workSheet = xLWorkbook.Worksheets.Add("Invoice" + Invoiceno);


                // Work Sheet Header
                workSheet.Cell(1, 1).Value = "ServiceProvider";
                workSheet.Cell(1, 2).Value = "BPtFBImImsi";
                workSheet.Cell(1, 3).Value = "BCtAFbContNo";
                workSheet.Cell(1, 4).Value = "BCtAFbCdrCode";
                workSheet.Cell(1, 5).Value = "BCtFbLesRefCode";
                workSheet.Cell(1, 6).Value = "BCtFbSatCode";
                workSheet.Cell(1, 7).Value = "BCtFbEquipmentCode";
                workSheet.Cell(1, 8).Value = "BCtAFbRecurFee";
                workSheet.Cell(1, 9).Value = "BCtAFbRecurFeeType";
                workSheet.Cell(1, 10).Value = "BCtBStartDate";
                workSheet.Cell(1, 11).Value = "BCtBEndDate";
                workSheet.Cell(1, 12).Value = "BCtBStartTime";
                workSheet.Cell(1, 13).Value = "BCtBOrigin";

                workSheet.Cell(1, 14).Value = "BCtAFbServType";
                workSheet.Cell(1, 15).Value = "BCtAFbServDest";
                workSheet.Cell(1, 16).Value = "BCtBTransUsage";
                workSheet.Cell(1, 17).Value = "BCtBUnits";
                workSheet.Cell(1, 18).Value = "BCtBTotalUsage";
                workSheet.Cell(1, 19).Value = "BCtBComTypes";
                workSheet.Cell(1, 20).Value = "BCtBBillIncr";
                workSheet.Cell(1, 21).Value = "BCtAFbBillNo";
                workSheet.Cell(1, 22).Value = "BCtAFbBillPeriod";
                workSheet.Cell(1, 23).Value = "BCtAFbBillDate";
                workSheet.Cell(1, 24).Value = "BCtAFbDualSim";
                workSheet.Cell(1, 25).Value = "BCtAFbContNo";
                workSheet.Cell(1, 26).Value = "BCtAFbContStart";
                workSheet.Cell(1, 27).Value = "BCtAFbContEnd";
                workSheet.Cell(1, 28).Value = "BCtAFbTerm";
                workSheet.Cell(1, 29).Value = "BCtAFbRgnFee";
                workSheet.Cell(1, 30).Value = "BCtAFbActFee";
                workSheet.Cell(1, 31).Value = "BCtAFbCardFee";


                int row = 2;
                decimal? totals = 0;
                int sno = 1;

                foreach (var ct in lstID)
                {
                    workSheet.Cell(row, 1).Value = "OteSat";
                    workSheet.Cell(row, 2).Style.NumberFormat.Format = "@";
                    workSheet.Cell(row, 2).Value = ct.BCtFBImImsi;

                    workSheet.Cell(row, 3).Value = ct.BCtFbContNo;

                    string satCode = string.Empty;
                    string EquipCode = string.Empty;
                    string LesRefCode = string.Empty;
                    workSheet.Cell(row, 4).Value = ct.BCtFbCdrCode;
                    try
                    {
                        LesRefCode = db.BsMtLESRefs.FirstOrDefault(t => t.LesRefID == ct.BCtFbLesRefCode).LESCode;
                    }
                    catch { }
                    workSheet.Cell(row, 5).Value = LesRefCode;

                    try
                    {
                        satCode = db.BsMtSatTypes.FirstOrDefault(t => t.MsSatType == ct.BCtFbSatCode).Satcode;
                    }
                    catch
                    {
                    }
                    workSheet.Cell(row, 6).Value = satCode;

                    try
                    {
                        EquipCode = db.BsMtEquipTypes.FirstOrDefault(t => t.EquipTypeID == ct.BCtFbEquipmentCode).EquipmentCode;
                    }
                    catch { }

                    workSheet.Cell(row, 7).Value = EquipCode;

                    if (ct.BCtTranServType.Contains("Subscription Fee"))
                        workSheet.Cell(row, 8).Value = ct.BCtTranTotalUsage;

                    workSheet.Cell(row, 9).Value = ct.BCtAFbRecurFeeType;

                    workSheet.Cell(row, 10).Style.NumberFormat.Format = "dd-mm-yyyy"; ;
                    workSheet.Cell(row, 10).Value = ct.BCtTranStartDate;
                    workSheet.Cell(row, 11).Style.NumberFormat.Format = "dd-mm-yyyy"; ;
                    workSheet.Cell(row, 11).Value = ct.BCtTranEndDate;

                    workSheet.Cell(row, 12).Style.NumberFormat.Format = "hh:mm:ss";
                    workSheet.Cell(row, 12).Value = ct.BCtTranStartTime;
                    workSheet.Cell(row, 13).Value = ct.BCtTranOrigin;

                    workSheet.Cell(row, 14).Value = ct.BCtTranServType;

                    workSheet.Cell(row, 15).Value = ct.BCtTranServDest;

                    workSheet.Cell(row, 16).Style.NumberFormat.Format = "0.00";
                    workSheet.Cell(row, 16).DataType = XLDataType.Number;
                    workSheet.Cell(row, 16).Value = ct.BCtTranUnitUsage;
                    workSheet.Cell(row, 17).Value = ct.BCtTranUnitType;
                    if (!ct.BCtTranServType.Contains("Subscription Fee"))
                    {
                        workSheet.Cell(row, 18).Style.NumberFormat.Format = "0.00"; ;
                        workSheet.Cell(row, 18).DataType = XLDataType.Number;
                        workSheet.Cell(row, 18).Value = ct.BCtTranTotalUsage;
                    }
                    else
                    {
                        workSheet.Cell(row, 18).Style.NumberFormat.Format = "0.00"; ;
                        workSheet.Cell(row, 18).DataType = XLDataType.Number;
                        workSheet.Cell(row, 18).Value = 0.00;
                    }


                    workSheet.Cell(row, 19).Value = "";

                    workSheet.Cell(row, 20).Value = ct.BCtTranBillIncr;

                    workSheet.Cell(row, 21).Value = ct.BCtFbBillNo;
                    workSheet.Cell(row, 22).Value = ct.BCtFbBillPeriod;
                    workSheet.Cell(row, 23).Value = ct.BCtFbBillDate;
                    workSheet.Cell(row, 24).Value = ct.BCtFbDualSim;
                    workSheet.Cell(row, 25).Value = ct.BCtFbContNo;
                    workSheet.Cell(row, 26).Value = ct.BCtFbContStartDate;
                    workSheet.Cell(row, 27).Value = ct.BCtFbContEndDate;
                    workSheet.Cell(row, 28).Value = ct.BCtFbTerm;
                    workSheet.Cell(row, 29).Value = ct.BCtFbRgnFee;
                    workSheet.Cell(row, 30).Value = ct.BCtFbActFee;
                    workSheet.Cell(row, 31).Value = ct.BCtFbCardFee;

                    row++;

                }
                #endregion

                // Save Work sheet into workbook
                xLWorkbook.SaveAs(spreadsheetstream);
                spreadsheetstream.Position = 0;

                //InvoiceFileName = temppath + "\\" + InvoiceFileName;

                //FileStream fileStream = new FileStream(InvoiceFileName, FileMode.Create);

                return new Tuple<Stream, string>(spreadsheetstream, "CommonTableDatas.xlsx");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            throw new Exception("The master primary is not an interger, please check and update the issue.");
        }

        public static string SendEmail(string to, string file, string subject, string body, string ccmails, string otherfile)
        {
            string emailid = "Shipsmts@ldksatcom.in";
            string password = "smtstestship";
            try
            {
                SmtpMail oMail = new SmtpMail("TryIt");
                SmtpClient oSmtp = new SmtpClient();

                // Set sender email address, please change it to yours
                oMail.From = emailid;

                // oMail.To = to;

                // Set email subject
                oMail.Subject = subject;

                // Set email body
                //oMail.TextBody = body;
                oMail.HtmlBody = body;

                oMail.AddAttachment(file);

                if (string.IsNullOrEmpty(otherfile))
                {
                    //oMail.AddAttachment(file);
                }
                else
                {
                    if(otherfile.Contains("||"))
                    {
                        string[] attach = otherfile.Split(new string[]{ "||" },StringSplitOptions.None);
                        if(attach.Length > 1)
                        {
                            oMail.AddAttachment(attach[0]);
                            oMail.AddAttachment(attach[1]);
                        }
                    }
                    else
                    {
                        oMail.AddAttachment(otherfile);
                    }
                }

                // Your SMTP server address
                SmtpServer oServer = new SmtpServer("mail.ldksatcom.in");
                // User and password for ESMTP authentication, if your server doesn't require
                // User authentication, please remove the following codes.
                oServer.User = emailid;

                oServer.Password = password;

                string[] arccmails = new string[5];

                if (!string.IsNullOrEmpty(ccmails))
                {
                    if (ccmails.Contains(';'))
                    {
                        arccmails = ccmails.Split(';');
                    }
                    else if (ccmails.Contains(','))
                    {
                        arccmails = ccmails.Split(',');
                    }
                    else
                        arccmails[0] = ccmails;
                }

                foreach (string ccmail in arccmails)
                {
                    if (!string.IsNullOrEmpty(ccmail))
                    {
                        oMail.Cc.Add(new MailAddress(ccmail));
                    }

                }

                // Set recipient email address, please change it to yours
                string[] artomail = new string[3];

                if (!string.IsNullOrEmpty(to))
                {
                    if (to.Contains(';'))
                    {
                        artomail = to.Split(';');
                    }
                    else if (to.Contains(','))
                    {
                        artomail = to.Split(',');
                    }
                    else
                        artomail[0] = to;
                }

                foreach (string tomail in artomail)
                {
                    if (!string.IsNullOrEmpty(tomail))
                    {
                        oMail.To.Add(new MailAddress(tomail));
                    }
                }



                // If your smtp server requires TLS connection, please add this line
                // oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;

                // If your smtp server requires implicit SSL connection on 465 port, please add this line
                //oServer.Port = 465;
                // oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;



                try
                {
                    oSmtp.SendMail(oServer, oMail);
                    return "Email Send Successfully";
                }
                catch (System.Exception ep)
                {
                    return "failed to send email with the following error:" + ep.Message;
                }
            }
            catch (System.Exception error)
            {
                return "failed to send email with the following error:" + error.Message;
            }
        }


        public static List<OteSatCtVM> CsvToDictionary(string csv_file_path)
        {
            List<Dictionary<string, dynamic>> keyValues = new List<Dictionary<string, dynamic>>();
            List<OteSatCtVM> lstVM = new List<OteSatCtVM>();
            try
            {
                List<string> columnFields = new List<string>();
                using (Microsoft.VisualBasic.FileIO.TextFieldParser csvReader = new Microsoft.VisualBasic.FileIO.TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields = csvReader.ReadFields();
                    foreach (string column in colFields)
                    {
                        string columnName = string.Empty;
                        if (column.StartsWith(" "))
                        {
                            columnName = column.Substring(1);
                            columnName = columnName.TrimEnd();
                        }
                        columnName = column.TrimEnd();
                        columnFields.Add(System.Text.RegularExpressions.Regex.Replace(columnName, @"\t|\n|\r", ""));

                        //DataColumn datecolumn = new DataColumn(column);
                        //datecolumn.AllowDBNull = true;
                        //csvData.Columns.Add(datecolumn);
                    }
                    Dictionary<string, dynamic> dictRecord = new Dictionary<string, dynamic>();

                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        dictRecord = new Dictionary<string, dynamic>();
                        OteSatCtVM vM = new OteSatCtVM();
                        //Making empty value as null
                        for (int i = 0, colIndex = 0; i < fieldData.Length; i++, colIndex++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = string.Empty;
                            }
                            vM = storeCollection_temp(columnFields[colIndex], fieldData[i], vM);
                            //dictRecord.Add(columnFields[colIndex], fieldData[i]);
                        }
                        //csvData.Rows.Add(fieldData);
                        if (vM.BCtTranServType != string.Empty && vM.BCtTranServType != "" && !string.IsNullOrEmpty(vM.BCtTranServType))
                            lstVM.Add(vM);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstVM;
        }

        public static otesatcdrVM storeCollection(string index, string value, otesatcdrVM vum)
        {
            switch (index)
            {
                case "Vessel Name":
                    vum.VesselName = value;
                    break;
                case "IMN / IMSI":
                    vum.IMNIMSI = value;
                    break;
                case "System":
                    vum.System = value;
                    break;
                case "Date":
                    if (!string.IsNullOrEmpty(value))
                    {
                        DateTime val;
                        bool result = DateTime.TryParse(value, out val);
                        if (result)
                            vum.Date = val;
                        else
                            vum.Date = null;
                    }
                    break;
                case "Time":
                    if (!string.IsNullOrEmpty(value))
                    {
                        TimeSpan val;
                        bool result = TimeSpan.TryParse(value, out val);
                        if (result)
                            vum.Time = val;
                        else
                            vum.Time = null;
                    }
                    break;
                case "Ocean Region":
                    vum.OceanRegion = value;
                    break;
                case "Time Zone":
                    vum.TimeZone = value;
                    break;
                case "Service Type":
                    if (!string.IsNullOrEmpty(value))
                    {
                        if (value.ToLower().Contains("discount"))
                            vum.ServiceType = string.Empty;
                        else
                            vum.ServiceType = value;
                    }
                    break;
                case "MRN":
                    vum.MRN = value;
                    break;
                case "Country":
                    vum.Country = value;
                    break;
                case "Destination":
                    vum.Destination = value;
                    break;
                case "Units":
                    if (!string.IsNullOrEmpty(value))
                    {
                        decimal val = 0;
                        bool result = decimal.TryParse(value, out val);
                        if (result)
                            vum.Units = val;
                        else
                            vum.Units = null;
                    }
                    break;
                case "Units Type":
                    vum.UnitsType = value;
                    break;
                case "Amount Due (USD)":
                    if (!string.IsNullOrEmpty(value))
                    {
                        decimal val = 0;
                        bool result = decimal.TryParse(value, out val);
                        if (result)
                            vum.AmountDueUSD = val;
                        else
                            vum.AmountDueUSD = null;
                    }
                    break;
            }
            return vum;
        }

        public static OteSatCtVM storeCollection_temp(string index, string value, OteSatCtVM vum)
        {
            try
            {
                switch (index)
                {
                    case "Vessel Name":
                        //vum.VesselName = value;
                        vum.BCtFbbLES = "OteSat";
                        break;
                    case "IMN / IMSI":
                        vum.BCtFBImImsi = value;
                        break;
                    case "System":
                        if (!string.IsNullOrEmpty(value) && value.Contains("Inmarsat-FleetBroadband"))
                        {
                            vum.BCtFbLesRefCode = 1;
                            vum.BCtFbEquipmentCode = 3;
                            vum.BCtFbSatCode = 1;
                        }
                        else if (!string.IsNullOrEmpty(value) && value.Contains("Inmarsat-FleetOne"))
                        {
                            vum.BCtFbLesRefCode = 1;
                            vum.BCtFbEquipmentCode = 3;
                            vum.BCtFbSatCode = 1;
                        }
                        else if (!string.IsNullOrEmpty(value) && value.Contains("Inmarsat-GSPS"))
                        {
                            vum.BCtFbLesRefCode = 1;
                            vum.BCtFbEquipmentCode = 3;
                            vum.BCtFbSatCode = 1;
                        }
                        break;
                    case "Date":
                        if (!string.IsNullOrEmpty(value))
                        {
                            DateTime val;
                            bool result = DateTime.TryParse(value, out val);
                            if (result)
                                vum.BCtTranStartDate = val;
                            else
                                vum.BCtTranStartDate = null;
                        }
                        break;
                    case "Time":
                        if (!string.IsNullOrEmpty(value))
                        {
                            TimeSpan val;
                            bool result = TimeSpan.TryParse(value, out val);
                            if (result)
                                vum.BCtTranStartTime = val;
                            else
                                vum.BCtTranStartTime = null;
                        }
                        break;
                    case "Ocean Region":
                        //vum.OceanRegion = value;
                        break;
                    case "Time Zone":
                        //vum.TimeZone = value;
                        break;
                    case "Service Type":
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (value.ToLower().Contains("discount"))
                                vum.BCtTranServType = value;
                            else
                            {
                                vum.BCtTranServType = value;
                            }
                        }
                        else
                            vum.BCtTranServType = null;

                        break;
                    case "MRN":
                        //vum.MRN = value;
                        break;
                    case "Country":
                        //vum.Country = value;
                        break;
                    case "Destination":
                        vum.BCtTranServDest = value;
                        break;
                    case "Units":
                        if (!string.IsNullOrEmpty(value))
                        {
                            decimal val = 0;
                            bool result = decimal.TryParse(value, out val);
                            if (result)
                            {
                                if (!string.IsNullOrEmpty(vum.BCtTranServType) && vum.BCtTranServType.Equals("Standard IP"))
                                {
                                    long dat = Convert.ToInt64(val);
                                    double ip = Converters.ConvertKilobytesToBit(dat);
                                    double bytes = 0;
                                    bool res = double.TryParse(ip.ToString(), out bytes);
                                    if (res)
                                    {
                                        vum.BCtTranUnitUsage = Convert.ToDecimal(ip);
                                    }
                                    else
                                    {
                                        vum.BCtTranUnitUsage = val;
                                    }
                                }
                                else
                                {
                                    vum.BCtTranUnitUsage = val;
                                }
                            }
                            else
                            {
                                vum.BCtTranUnitUsage = null;
                            }
                        }
                        break;
                    case "Units Type":
                        if (!string.IsNullOrEmpty(vum.BCtTranServType) && vum.BCtTranServType.Equals("Standard IP"))
                        {
                            vum.BCtTranUnitType = "Bits";
                        }
                        else
                        {
                            vum.BCtTranUnitType = value;
                        }
                        break;
                    case "Amount Due (USD)":
                        if (!string.IsNullOrEmpty(value))
                        {
                            decimal val = 0;
                            bool result = decimal.TryParse(value, out val);
                            if (result)
                                vum.BCtTranTotalUsage = val;
                            else
                                vum.BCtTranTotalUsage = null;
                        }
                        break;
                }
            }
            catch (Exception rer)
            {
                throw rer;
            }
            return vum;
        }

        public Tuple<List<int>, string> OteSat2CT_VM(List<otesatcdrVM> lstVM, int LesRefID)
        {
            List<int> ctMasterID = new List<int>();
            StringBuilder ErrMsg = new StringBuilder();
            StringBuilder time = new StringBuilder();
            var stopwatch = new Stopwatch();
            try
            {
                BillingSystemEntities db = new BillingSystemEntities();
                List<Dictionary<string, dynamic>> pairs = new List<Dictionary<string, dynamic>>();//GetDictfromExcel(fileName);
                var imsi = lstVM.Select(ims => ims.IMNIMSI).Distinct().ToList();

                int ctID = 0;

                foreach (var id in imsi)
                {
                    stopwatch.Start();

                    int LesRegID = 0;
                    int lesContractID = 0;
                    bool isNotExist = false;
                    int primarykeyID = 0;
                    BCtA1 ctMaster = new BCtA1();
                    List<BCtB1> lstBCtB = new List<BCtB1>();

                    var keyValues = lstVM.Where(t => t.IMNIMSI.Equals(id)).ToList();

                    var dictFee = keyValues.Where(t => t.IMNIMSI.Equals(id) && t.ServiceType.Equals("Subscription Fee")).ToList();

                    // Subscribtion Fe to store FBB Master table
                    foreach (var dictRecord in dictFee)
                    {
                        if (dictRecord.ServiceType != string.Empty)
                        {
                            string types = dictRecord.ServiceType;
                            switch (types.ToLower())
                            {
                                case "subscription fee":
                                    ctMaster.BCtAFbStartDate = dictRecord.Date;
                                    ctMaster.BCtAfkLESID = LesID;
                                    ctMaster.BCtAfkLESCID = db.BsPtSPContracts.FirstOrDefault(t => t.fkSPID == LesID).spcID;
                                    lesContractID = ctMaster.BCtAfkLESCID;
                                    string imsiID = Convert.ToString(id);
                                    try
                                    {
                                        var SimReg = db.BptFbSims.FirstOrDefault(t => t.BPtFBImImsi.Equals(imsiID));
                                        if (SimReg != null)
                                        {
                                            ctMaster.BCtAfkSimID = SimReg.Sim_ID;
                                            ctMaster.fkLesRefID = LesRefID;
                                            ctMaster.fkSatTypeID = SimReg.fkSatTypeID;
                                            ctMaster.fkEquipType = SimReg.fkEquipTypeID;
                                            ctMaster.BCtAfkLesRegID = SimReg.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == SimReg.Sim_ID).SPLesRegID;
                                            LesRegID = ctMaster.BCtAfkLesRegID;
                                        }
                                        else
                                        {
                                            ErrMsg.AppendFormat("The IMSI:{0} no SIM not register in our database, please check.", id).AppendLine();
                                            isNotExist = true;
                                            continue;
                                        }
                                    }
                                    catch (Exception eror)
                                    {
                                        ErrMsg.AppendFormat("The IMSI:{0} getting error: {0}, please rerun the transaction.", eror.Message.ToString()).AppendLine();
                                        isNotExist = true;
                                        continue;
                                    }

                                    ctMaster.BCtAFbLesCode = null;
                                    //ctMaster.BCMtFbCdrCode = "";
                                    ctMaster.BCtAFbServDest = dictRecord.Destination;
                                    ctMaster.BCtAFbServType = types;
                                    ctMaster.BCtAFbTransUsage = Convert.ToInt32(dictRecord.Units);

                                    ctMaster.BCtAFbUOM = dictRecord.UnitsType;

                                    ctMaster.BCtAFbRecurFee = dictRecord.AmountDueUSD;

                                    db.BCtAs1.Add(ctMaster);
                                    //db.SaveChanges();
                                    //ctID = ctMaster.BCtAID;
                                    //primarykeyID = ctMaster.BCtAID;
                                    ctMasterID.Add(ctID);
                                    break;
                            }
                        }
                    }

                    // End process of ct master table 

                    keyValues = lstVM.Where(t => t.IMNIMSI.Equals(id) && !t.ServiceType.Equals("Subscription Fee")).ToList();

                    // Call data record store to FBB common table
                    if (!isNotExist)
                    {
                        foreach (var dictRecord in keyValues)
                        {
                            if (dictRecord.ServiceType != null)
                            {
                                string types = dictRecord.ServiceType;

                                if (!string.IsNullOrEmpty(types))
                                {
                                    BCtB1 ctRecord = new BCtB1();
                                    ctRecord.BCtAfkSimID = ctMaster.BCtAfkSimID;
                                    ctRecord.BCtBStartDate = dictRecord.Date;
                                    //ctRecord.BsCtEndtDate
                                    ctRecord.BCtBStartTime = dictRecord.Time;
                                    //ctRecord.BsCtEndTime 
                                    //ctRecord.BsCtOrigion
                                    ctRecord.BCtBDestination = dictRecord.Destination;
                                    string ServiceTypes = dictRecord.ServiceType;
                                    try
                                    {
                                        ctRecord.BCtBLesServTypeID = db.BsPtSPServiceDests.FirstOrDefault(t => t.BPtServTfkLesRegID == LesRegID && t.BsPtServiceTypes.ToLower().Equals(ServiceTypes.ToLower())).SPServDID;
                                    }
                                    catch { }

                                    ctRecord.BCtBTransUsage = dictRecord.Units;
                                    try
                                    {
                                        string units = dictRecord.UnitsType.ToLower();
                                        if (units.ToLower().Contains("kbytes"))
                                            units = "KB";
                                        else if (units.ToLower().Contains("sec"))
                                            units = "Sec";

                                        ctRecord.BCtBfkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals(units)).UID;
                                    }
                                    catch (Exception rr)
                                    { }
                                    ctRecord.BCtBTotalUsage = dictRecord.AmountDueUSD;
                                    try
                                    {
                                        ctRecord.BCtBfkCommType = Convert.ToInt32(db.BsPtSPServiceDests.FirstOrDefault(t => t.SPServDID == ctRecord.BCtBLesServTypeID).ComTypeID);

                                    }
                                    catch (Exception err)
                                    {
                                        throw err;
                                    }

                                    db.BCtBs1.Add(ctRecord);
                                    lstBCtB.Add(ctRecord);
                                }
                            }
                        }
                        stopwatch.Stop();

                        var elapsed_time = stopwatch.ElapsedMilliseconds;
                        time.AppendFormat("For ID: {0}, to ADD Table {1}", id, elapsed_time);
                    }
                }

                // end process of ct records
                try
                {
                    stopwatch = new Stopwatch();
                    stopwatch.Start();
                    db.SaveChanges();
                    var elapsed_time = stopwatch.ElapsedMilliseconds;
                    time.AppendFormat("For ID: {0}, to ADD Table {1}", "Save Changes()", elapsed_time);
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
                    string errormsg = string.Empty;
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        errormsg = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors: ", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            errormsg = string.Format(" - Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    ErrMsg.AppendFormat("MSI NO:, Error:{0}", errormsg).AppendLine();
                }
                catch (Exception error)
                {
                    ErrMsg.AppendFormat("MSI NO:, Error:{0}", error.Message.ToString() + error.StackTrace).AppendLine();
                }

                ErrMsg.AppendLine(time.ToString());
                return new Tuple<List<int>, string>(ctMasterID, ErrMsg.ToString());
            }
            catch (Exception e)
            {
                return new Tuple<List<int>, string>(ctMasterID, e.Message.ToString() + e.StackTrace);
            }
        }

        public string OteSat2CT_temp(List<otesatcdrVM> lstVM, int LesRefID)
        {
            string msg = string.Empty;
            try
            {
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["AdoConnstr"].ConnectionString))
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();

                    using (var bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.Default, transaction))
                    {
                        bulkCopy.BatchSize = 100;
                        bulkCopy.DestinationTableName = "dbo.OteSatCDR";
                        try
                        {
                            var stopwatch = new Stopwatch();
                            stopwatch.Start();

                            bulkCopy.WriteToServer(lstVM.AsDataTable());

                            stopwatch.Stop();
                            msg += "Time: " + stopwatch.ElapsedMilliseconds + " ";
                        }
                        catch (Exception e)
                        {
                            transaction.Rollback();
                            connection.Close();
                            msg = "faild to store common table";
                            return msg;
                        }
                    }

                    transaction.Commit();
                    msg = "successfully store common table";
                    return msg;
                }
            }
            catch (Exception error)
            {
                msg = "faild to store common table eorro: " + error.Message.ToString();
                return msg;
            }
        }

        public Tuple<string, List<OteSatCtVM>> OteSat2CT_model(List<OteSatCtVM> lstVM, int LesRefID)
        {
            string msg = string.Empty;
            try
            {
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["AdoConnstr"].ConnectionString))
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();

                    using (var bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.Default, transaction))
                    {
                        bulkCopy.BatchSize = 100;
                        bulkCopy.DestinationTableName = "dbo.BCtFbbTrans";
                        try
                        {
                            var stopwatch = new Stopwatch();
                            stopwatch.Start();

                            bulkCopy.WriteToServer(lstVM.AsDataTable());

                            stopwatch.Stop();
                            msg += "Time: " + stopwatch.ElapsedMilliseconds + " ";
                        }
                        catch (Exception e)
                        {
                            transaction.Rollback();
                            connection.Close();
                            msg = "faild to store common table";
                            return new Tuple<string, List<OteSatCtVM>>(msg, lstVM);
                        }
                    }

                    transaction.Commit();
                    msg = "successfully store common table";
                    return new Tuple<string, List<OteSatCtVM>>(msg, lstVM);
                }
            }
            catch (Exception error)
            {
                msg = "faild to store common table eorro: " + error.Message.ToString();
                return new Tuple<string, List<OteSatCtVM>>(msg, lstVM);
            }
        }

        public string RejectionOtesat_New(string bctles, string errorEsg)
        {
            System.Text.StringBuilder returnMsg = new System.Text.StringBuilder();
            returnMsg.Append(errorEsg).AppendLine();

            System.Text.StringBuilder Msg = new System.Text.StringBuilder();

            List<BCtFbbTran> lstCtRecord = new List<BCtFbbTran>();
            List<BsPtSPServiceDest> lstLES_ServiceType = new List<BsPtSPServiceDest>();

            using (BillingSystemEntities db = new BillingSystemEntities())
            {
                try
                {
                    lstCtRecord = db.BCtFbbTrans.Where(t => t.BCtFbbLES.Equals(bctles)).ToList();

                    var lstSubscripRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("Subscription Fee")).ToList();
                    var lstStdIPRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("Standard IP")).ToList();
                    var lstV2CellRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("Voice  to Cellular")).ToList();
                    var lstV2FixedRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("Voice to Fixed")).ToList();
                    var lstV2FBBRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("Voice  to FleetBroadband")).ToList();
                    var lstV2IdridumRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("Voice  to Iridium")).ToList();
                    var lstISDNRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("ISDN")).ToList();
                    var lstSMSRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("SMS")).ToList();

                    if (lstSubscripRecords.Count > 0)
                    {
                        foreach (var CT_Fbs in lstSubscripRecords)
                        {
                            string ImarSatID = string.Empty;
                            try
                            {
                                #region Varaiable declarations

                                string ImsiID = string.Empty;
                                int SimID = 0;
                                decimal? CDR_RecuFee = 0;

                                decimal? SMTS_TotalCost = 0;
                                decimal? Les_TotalCostCP = 0;
                                decimal? Cus_TotalCostSP = 0;

                                int? LES_ContractID = 0;
                                int LES_RegID = 0;

                                decimal? LES_DataPlan = 0;
                                decimal? LES_OutbundleRate = 0;
                                decimal? LES_RecurFee = 0;
                                decimal? LES_DataRateper1MB = 0;
                                decimal? LES_VoiceCellular = 0;
                                decimal? LES_VoiceFixed = 0;
                                decimal? LES_VoiceFBB = 0;
                                decimal? LES_VoiceIridium = 0;
                                decimal? LES_ISDN = 0;
                                decimal? LES_SMS = 0;

                                decimal? LES_InvRptDataCostTot = 0;

                                int CUS_Id = 0;
                                int CUS_ContractID = 0;
                                int CUS_RegsID = 0;

                                decimal? Cus_DataPlan = 0;
                                decimal? Cus_OutbundleRate = 0;
                                decimal? CUS_RecurFee = 0;
                                decimal? CUS_DataRateper1MB = 0;
                                decimal? CUS_VoiceCellular = 0;
                                decimal? CUS_VoiceFixed = 0;
                                decimal? CUS_VoiceFBB = 0;
                                decimal? CUS_VoiceIridium = 0;
                                decimal? CUS_ISDN = 0;
                                decimal? CUS_SMS = 0;

                                DateTime? LES_ContrRegStrDate = null;
                                DateTime? LES_ContrRegEndDate = null;

                                DateTime? LES_ContrBarStrDate = null;
                                DateTime? LES_ContrBarEndDate = null;

                                DateTime? LES_ContrSusStrDate = null;
                                DateTime? LES_ContrSusEndDate = null;

                                DateTime? LES_ContrLayoutStrDate = null;
                                DateTime? LES_ContrLayoutEndDate = null;

                                DateTime? CUS_ContrRegStrDate = null;
                                DateTime? CUS_ContrRegEndDate = null;

                                DateTime? CUS_ContrBarStrDate = null;
                                DateTime? CUS_ContrBarEndDate = null;

                                DateTime? CUS_ContrSusStrDate = null;
                                DateTime? CUS_ContrSusEndDate = null;

                                DateTime? CUS_ContrLayoutStrDate = null;
                                DateTime? CUS_ContrLayoutEndDate = null;

                                DateTime? CDR_StartDate = null;
                                DateTime? CDR_EndDate = null;

                                bool is_LESGBPlan = false;
                                bool is_CUSGBPlan = false;
                                bool isNoTransData = false;

                                int ServTypeID = 0;
                                int ServTypeMobile = 0;
                                int ServTypeVce2Fixed = 0;
                                int ServTypeVce2FBB = 0;
                                int LES_ServTypeVce2Iridium = 0;
                                int ServTypeVce2ISDN = 0;
                                int ServTypeSMS = 0;

                                int CUS_ServTypeID = 0;
                                int CUS_ServTypeMobile = 0;
                                int CUS_ServTypeVce2Fixed = 0;
                                int CUS_ServTypeVce2FBB = 0;
                                int CUS_ServTypeVce2Iridium = 0;
                                int CUS_ServTypeVce2ISDN = 0;
                                int CUS_ServTypeSMS = 0;
                                #endregion

                                var tblSim = db.BptFbSims.FirstOrDefault(t => t.BPtFBImImsi.Equals(CT_Fbs.BCtFBImImsi));

                                var lstNotSubscripRecords = lstCtRecord.Where(t => t.BCtFBImImsi.Equals(CT_Fbs.BCtFBImImsi)).ToList();

                                if (tblSim != null)
                                {
                                    ImarSatID = tblSim.BPtFBimMsisdn;
                                    SimID = tblSim.Sim_ID;
                                }
                                else
                                {
                                    Msg.AppendFormat("This IMSI id {0} is not registered in our database, please check it.<br />", CT_Fbs.BCtFBImImsi).AppendLine();
                                    returnMsg.AppendFormat("This IMSI id {0} is not registered in our database, please check it.<br />", CT_Fbs.BCtFBImImsi).AppendLine();
                                }

                                CDR_RecuFee = CT_Fbs.BCtFbRecurFee;

                                if (lstNotSubscripRecords.Count > 0)
                                {
                                    CDR_StartDate = lstNotSubscripRecords.Min(t => t.BCtTranStartDate);
                                    CDR_EndDate = lstNotSubscripRecords.Max(t => t.BCtTranStartDate);
                                }
                                else
                                    isNoTransData = true;

                                var lstStdIP_Trans = lstStdIPRecords.Where(t => t.BCtFBImImsi.Equals(CT_Fbs.BCtFBImImsi)).ToList();
                                var lstV2Cell_Trans = lstV2CellRecords.Where(t => t.BCtFBImImsi.Equals(CT_Fbs.BCtFBImImsi)).ToList();
                                var lstV2Fixed_Trans = lstV2FixedRecords.Where(t => t.BCtFBImImsi.Equals(CT_Fbs.BCtFBImImsi)).ToList();
                                var lstV2Fbb_Trans = lstV2FBBRecords.Where(t => t.BCtFBImImsi.Equals(CT_Fbs.BCtFBImImsi)).ToList();
                                var lstV2Iridium_Trans = lstV2IdridumRecords.Where(t => t.BCtFBImImsi.Equals(CT_Fbs.BCtFBImImsi)).ToList();
                                var lstISDN_Trans = lstISDNRecords.Where(t => t.BCtFBImImsi.Equals(CT_Fbs.BCtFBImImsi)).ToList();
                                var lstSMS_Trans = lstSMSRecords.Where(t => t.BCtFBImImsi.Equals(CT_Fbs.BCtFBImImsi)).ToList();

                                #region get Cus Register tbl info

                                var cusRegs = db.BsPtCusRegs_.FirstOrDefault(t => t.fkSimID == SimID);
                                if (cusRegs != null)
                                {
                                    CUS_Id = Convert.ToInt32(cusRegs.fkCusID);
                                    CUS_ContractID = Convert.ToInt32(cusRegs.fkCuscID);
                                    CUS_RegsID = Convert.ToInt32(cusRegs.Cus_RegID);

                                    CUS_RecurFee = cusRegs.BPtCusRgnFbRecurFee;

                                    Cus_DataPlan = Convert.ToDecimal(cusRegs.BPtCusRgnFBDataPlanIn);

                                    CUS_DataRateper1MB = cusRegs.BPtCusRgnFbInbundleDataRate;
                                    Cus_OutbundleRate = cusRegs.BPtCusRgnFbOutBundleDataRate;

                                    CUS_ContrRegStrDate = cusRegs.BsPtCusRegStartdate;
                                    CUS_ContrRegEndDate = cusRegs.BsPtCusRegEnd;

                                    CUS_ContrBarStrDate = cusRegs.BsPtCusRegFBBarDate;
                                    CUS_ContrBarEndDate = cusRegs.BsPtCusRegFbBarDateLifted;

                                    CUS_ContrSusStrDate = cusRegs.BsPtCusRegFBSusDate;
                                    CUS_ContrSusEndDate = cusRegs.BsPtCusRegFBSusDateLifted;

                                    CUS_ContrLayoutStrDate = cusRegs.BsPtCusRegFBLayUpDate;
                                    CUS_ContrLayoutEndDate = cusRegs.BsPtCusRegFBLayUpDateLifted;

                                    string unit = cusRegs.BsMtUnit.Units;
                                    if (unit.Contains("GB"))
                                        is_CUSGBPlan = true;
                                    else
                                        is_CUSGBPlan = false;
                                }
                                else
                                {
                                    Msg.AppendFormat("This msi {0} id not registered in customer registered, please check.<br />", ImarSatID).AppendLine();
                                }

                                List<BsPtCusServiceDest> lstSP = db.BsPtCusServiceDests.Where(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID).ToList();

                                try
                                {
                                    CUS_VoiceCellular = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                             t.BsPtCusServiceTypes.Equals("Voice  to Cellular")).BsPtCusServiceSellerPrice;
                                }
                                catch { CUS_VoiceCellular = 0; }

                                try
                                {
                                    CUS_VoiceFixed = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                          t.BsPtCusServiceTypes.Equals("Voice  to Fixed")).BsPtCusServiceSellerPrice;
                                }
                                catch { CUS_VoiceFixed = 0; }

                                try
                                {
                                    CUS_VoiceFBB = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                        t.BsPtCusServiceTypes.Equals("Voice  to FleetBroadband")).BsPtCusServiceSellerPrice;
                                }
                                catch { CUS_VoiceFBB = 0; }

                                try
                                {
                                    CUS_VoiceIridium = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                            t.BsPtCusServiceTypes.Equals("Voice  to Iridium")).BsPtCusServiceSellerPrice;
                                }
                                catch { CUS_VoiceIridium = 0; }

                                try
                                {
                                    CUS_ISDN = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                    t.BsPtCusServiceTypes.Equals("ISDN")).BsPtCusServiceSellerPrice;
                                }
                                catch { CUS_ISDN = 0; }

                                try
                                {
                                    CUS_SMS = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                   t.BsPtCusServiceTypes.Equals("SMS")).BsPtCusServiceSellerPrice;
                                }
                                catch { CUS_SMS = 0; }
                                #endregion

                                #region GET LES Details

                                var LES_Services = db.BsPtSPServiceDests.ToList();
                                try
                                {

                                    try
                                    {
                                        var Les_Reg = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSPID == LesID && t.fkSimID == SimID);
                                        //var Les_Reg = db.BsPtSPLesRegs.FirstOrDefault(t => t.SPLesRegID == CT_Fbs.BCtAfkLesRegID);

                                        LES_RegID = Les_Reg.SPLesRegID;
                                        LES_ContractID = Les_Reg.fkspcID;

                                        LES_RecurFee = Les_Reg.BPtLesRgnFbRecurFee;
                                        LES_DataPlan = Convert.ToDecimal(Les_Reg.BPtLesRgnFBDataPlanIn);
                                        LES_DataRateper1MB = Les_Reg.BPtLesRgnFbInbundleDataRate;
                                        LES_OutbundleRate = Les_Reg.BPtLesRgnFbOutBundleDataRate;

                                        LES_ContrRegStrDate = Les_Reg.BsPtlesRegStartdate;
                                        LES_ContrRegEndDate = Les_Reg.BsPtlesRegEnd;

                                        LES_ContrBarStrDate = Les_Reg.BsPtLesRegFBBarDate;
                                        LES_ContrBarEndDate = Les_Reg.BsPtLesRegFbBarDateLifted;

                                        LES_ContrSusStrDate = Les_Reg.BsPtLesRegFBSusDate;
                                        LES_ContrSusEndDate = Les_Reg.BsPtLesRegFBSusDateLifted;

                                        LES_ContrLayoutStrDate = Les_Reg.BsPtLesRegFBLayUpDate;
                                        LES_ContrLayoutEndDate = Les_Reg.BsPtLesRegFBLayUpDateLifted;

                                        string unit = Les_Reg.BsMtUnit.Units;
                                        if (unit.Contains("GB"))
                                            is_LESGBPlan = true;
                                        else
                                            is_LESGBPlan = false;
                                    }
                                    catch { LES_RecurFee = 0; }

                                    lstLES_ServiceType = (from ls in LES_Services
                                                          where ls.fkSpID == LesID && ls.BPtServTfkLesRegID == LES_RegID
                                                          select ls).ToList();

                                    try
                                    {
                                        LES_VoiceCellular = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                 t.BsPtServiceTypes.Equals("Voice  to Cellular")).BsPtServiceCostPrice;
                                    }
                                    catch { LES_VoiceCellular = 0; }

                                    try
                                    {
                                        LES_VoiceFixed = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                              t.BsPtServiceTypes.Equals("Voice  to Fixed")).BsPtServiceCostPrice;
                                    }
                                    catch { LES_VoiceFixed = 0; }

                                    try
                                    {
                                        LES_VoiceFBB = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                            t.BsPtServiceTypes.Equals("Voice  to FleetBroadband")).BsPtServiceCostPrice;
                                    }
                                    catch { LES_VoiceFBB = 0; }

                                    try
                                    {
                                        LES_VoiceIridium = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                t.BsPtServiceTypes.Equals("Voice  to Iridium")).BsPtServiceCostPrice;
                                    }
                                    catch { LES_VoiceIridium = 0; }

                                    try
                                    {
                                        LES_ISDN = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                        t.BsPtServiceTypes.Equals("ISDN")).BsPtServiceCostPrice;
                                    }
                                    catch { LES_ISDN = 0; }

                                    try
                                    {
                                        LES_SMS = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                       t.BsPtServiceTypes.Equals("SMS")).BsPtServiceCostPrice;
                                    }
                                    catch { LES_SMS = 0; }
                                }
                                catch (Exception e)
                                {
                                    Msg.AppendFormat("Error:{0}", e.Message.ToString());
                                }
                                //-------------------------------------------------------------------------------------------------------------------------------
                                #endregion

                                #region Standard IP

                                try
                                {
                                    ServTypeID = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                        t.BsPtServiceTypes.Equals("Standard IP")).SPServDID;
                                }
                                catch
                                {
                                    ServTypeID = 0;
                                    Msg.Append("The Standard IP is Registed in the LES Service Description<br />.").AppendLine();
                                }

                                try
                                {
                                    CUS_ServTypeID = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                            t.BsPtCusServiceTypes.Equals("Standard IP")).CusServDID;
                                }
                                catch
                                {
                                    CUS_ServTypeID = 0;
                                    Msg.Append("The Standard IP is Registed in the CUS Service Description<br />.").AppendLine();
                                }

                                decimal? DataUnits_CDR = null;
                                decimal? CDR_DataCP = null;
                                if (lstStdIP_Trans.Count > 0)
                                {
                                    DataUnits_CDR = lstStdIP_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    DataUnits_CDR = Math.Round(Convert.ToDecimal(DataUnits_CDR), 2, MidpointRounding.AwayFromZero);

                                    CDR_DataCP = lstCtRecord.AsEnumerable().Sum(s => s.BCtTranTotalUsage);

                                    CDR_DataCP = Math.Round(Convert.ToDecimal(CDR_DataCP), 2, MidpointRounding.AwayFromZero);
                                }

                                SMTS_TotalCost += CDR_DataCP;

                                #endregion

                                #region Basic voice - Cellular

                                // <--------------------------- Basic voice - Mobile  ----------------------------->
                                try
                                {
                                    ServTypeMobile = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                            t.BsPtServiceTypes.Equals("Voice  to Cellular")).SPServDID;
                                }
                                catch { }

                                try
                                {
                                    CUS_ServTypeMobile = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                t.BsPtCusServiceTypes.Equals("Voice  to Cellular")).CusServDID;
                                }
                                catch { }

                                decimal? CDR_VceCellularUintsCT = null;
                                decimal? CDR_VceCellularCostPriceCT = null;

                                if (lstV2Cell_Trans.Count > 0)
                                {
                                    CDR_VceCellularUintsCT = lstV2Cell_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    if (CDR_VceCellularUintsCT != null)
                                        CDR_VceCellularUintsCT = Math.Round(Convert.ToDecimal(CDR_VceCellularUintsCT), 2, MidpointRounding.AwayFromZero);

                                    CDR_VceCellularCostPriceCT = lstV2Cell_Trans.AsEnumerable().Sum(s => s.BCtTranTotalUsage);

                                    if (CDR_VceCellularCostPriceCT != null)
                                        CDR_VceCellularCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceCellularCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                }

                                SMTS_TotalCost += CDR_VceCellularCostPriceCT;
                                // <--------------------------- End Basic voice -FBB/Cellular  ----------------------------->
                                #endregion

                                #region Basic Voice  to Fixed

                                // <--------------------------- Basic Voice  to Fixed  ----------------------------->
                                try
                                {
                                    ServTypeVce2Fixed = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID
                                                                                                && t.BsPtServiceTypes.Equals("Voice  to Fixed")).SPServDID;
                                }
                                catch { }

                                try
                                {
                                    CUS_ServTypeVce2Fixed = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                    t.BsPtCusServiceTypes.Equals("Voice  to Fixed")).CusServDID;
                                }
                                catch { }

                                decimal? CDR_VceFixedUintsCT = null;
                                decimal? CDR_VceFixedCostPriceCT = null;

                                if (lstV2Fixed_Trans.Count > 0)
                                {
                                    CDR_VceFixedUintsCT = lstV2Fixed_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    if (CDR_VceFixedUintsCT != null)
                                        CDR_VceFixedUintsCT = Math.Round(Convert.ToDecimal(CDR_VceFixedUintsCT), 2, MidpointRounding.AwayFromZero);

                                    CDR_VceFixedCostPriceCT = lstV2Fixed_Trans.AsEnumerable().Sum(s => s.BCtTranTotalUsage);

                                    if (CDR_VceFixedCostPriceCT != null)
                                        CDR_VceFixedCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceFixedCostPriceCT), 2, MidpointRounding.AwayFromZero);

                                    SMTS_TotalCost += CDR_VceFixedCostPriceCT;
                                }
                                // <--------------------------- End Basic Voice  to Fixed  ----------------------------->

                                #endregion

                                #region Voice to FleetBroadband
                                // <--------------------------- Voice to FleetBroadband  ----------------------------->

                                try
                                {
                                    ServTypeVce2FBB = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                            t.BsPtServiceTypes.Equals("Voice to FleetBroadband")).SPServDID;
                                }
                                catch
                                {
                                    ServTypeVce2FBB = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                                t.BsPtServiceTypes.Equals("Voice  to FleetBroadband")).SPServDID;
                                }

                                try
                                {
                                    CUS_ServTypeVce2FBB = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                t.BsPtCusServiceTypes.Equals("Voice to FleetBroadband")).CusServDID;
                                }
                                catch
                                {
                                    CUS_ServTypeVce2FBB = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                t.BsPtCusServiceTypes.Equals("Voice  to FleetBroadband")).CusServDID;
                                }

                                decimal? CDR_VceFBBUintsCT = null;
                                decimal? CDR_VceFBBCostPriceCT = null;

                                if (lstV2Fbb_Trans.Count > 0)
                                {
                                    CDR_VceFBBUintsCT = lstV2Fbb_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    if (CDR_VceFBBUintsCT != null)
                                        CDR_VceFBBUintsCT = Math.Round(Convert.ToDecimal(CDR_VceFBBUintsCT), 2, MidpointRounding.AwayFromZero);

                                    CDR_VceFBBCostPriceCT = lstV2Fbb_Trans.AsEnumerable().Sum(s => s.BCtTranTotalUsage);

                                    if (CDR_VceFBBCostPriceCT != null)
                                        CDR_VceFBBCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceFBBCostPriceCT), 2, MidpointRounding.AwayFromZero);

                                    SMTS_TotalCost += CDR_VceFBBCostPriceCT;
                                }
                                // <--------------------------- End Voice to FleetBroadband  ----------------------------->
                                #endregion

                                #region Voice  to Iridium
                                // <--------------------------- Voice  to Iridium  ----------------------------->
                                try
                                {
                                    LES_ServTypeVce2Iridium = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                                   t.BsPtServiceTypes.Equals("Voice  to Iridium")).SPServDID;
                                }
                                catch { }
                                try
                                {
                                    CUS_ServTypeVce2Iridium = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                    t.BsPtCusServiceTypes.Equals("Voice  to Iridium")).CusServDID;
                                }
                                catch { }

                                decimal? CDR_VceIridiumUintsCT = null;
                                decimal? CDR_VceIridiumCostPriceCT = null;

                                if (lstV2Iridium_Trans.Count > 0)
                                {
                                    CDR_VceIridiumUintsCT = lstV2Iridium_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    if (CDR_VceIridiumUintsCT != null)
                                        CDR_VceIridiumUintsCT = Math.Round(Convert.ToDecimal(CDR_VceIridiumUintsCT), 2, MidpointRounding.AwayFromZero);

                                    CDR_VceIridiumCostPriceCT = lstV2Iridium_Trans.AsEnumerable().Sum(s => s.BCtTranTotalUsage);

                                    if (CDR_VceIridiumCostPriceCT != null)
                                        CDR_VceIridiumCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceIridiumCostPriceCT), 2, MidpointRounding.AwayFromZero);

                                    SMTS_TotalCost += CDR_VceIridiumCostPriceCT;
                                }
                                // <--------------------------- End Voice  to Iridium  ----------------------------->
                                #endregion

                                #region ISDN
                                // <--------------------------- ISDN  ----------------------------->
                                try
                                {
                                    ServTypeVce2ISDN = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                                    t.BsPtServiceTypes.Equals("ISDN")).SPServDID;
                                }
                                catch { }
                                try
                                {
                                    CUS_ServTypeVce2ISDN = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                    t.BsPtCusServiceTypes.Equals("ISDN")).CusServDID;
                                }
                                catch { }

                                decimal? CDR_ISDNUintsCT = null;
                                decimal? CDR_ISDNCostPriceCT = null;
                                if (lstISDN_Trans.Count > 0)
                                {
                                    CDR_ISDNUintsCT = lstISDN_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    if (CDR_ISDNUintsCT != null)
                                        CDR_ISDNUintsCT = Math.Round(Convert.ToDecimal(CDR_ISDNUintsCT), 2, MidpointRounding.AwayFromZero);

                                    CDR_ISDNCostPriceCT = lstISDN_Trans.AsEnumerable().Sum(s => s.BCtTranTotalUsage);

                                    if (CDR_ISDNCostPriceCT != null)
                                        CDR_ISDNCostPriceCT = Math.Round(Convert.ToDecimal(CDR_ISDNCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                }
                                SMTS_TotalCost += CDR_ISDNCostPriceCT;
                                // <--------------------------- End ISDN  ----------------------------->
                                #endregion

                                #region SMS
                                // <--------------------------- ISDN  ----------------------------->
                                try
                                {
                                    ServTypeSMS = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals("SMS")).SPServDID;
                                }
                                catch { }
                                try
                                {
                                    CUS_ServTypeSMS = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("SMS")).CusServDID;
                                }
                                catch { }

                                decimal? CDR_SMSUintsCT = null;
                                decimal? CDR_SMSCostPriceCT = null;
                                if (lstSMS_Trans.Count > 0)
                                {
                                    CDR_SMSUintsCT = lstSMS_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    if (CDR_SMSUintsCT != null)
                                        CDR_SMSUintsCT = Math.Round(Convert.ToDecimal(CDR_SMSUintsCT), 2, MidpointRounding.AwayFromZero);

                                    CDR_SMSCostPriceCT = lstSMS_Trans.AsEnumerable().Sum(s => s.BCtTranTotalUsage);


                                    if (CDR_SMSCostPriceCT != null)
                                        CDR_SMSCostPriceCT = Math.Round(Convert.ToDecimal(CDR_SMSCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                }

                                SMTS_TotalCost += CDR_SMSCostPriceCT;
                                // <--------------------------- End ISDN  ----------------------------->
                                #endregion

                                bool isRecurFeeMached = false;
                                bool isDataMatched = false;
                                bool isVoiceCellurlarMatched = false;
                                bool isVoiceFixedMatched = false;
                                bool isVoiceFbbMatched = false;
                                bool isVoiceIridiumMatched = false;
                                bool isISDNMatched = false;
                                bool isSMSMatched = false;

                                bool isOutbundleUsed = false;
                                decimal? OutbundleUsedData = 0;
                                decimal? CDR_OutbundleRate = 0;

                                Les_TotalCostCP += LES_RecurFee;
                                SMTS_TotalCost += CDR_RecuFee;

                                if ((CDR_RecuFee < LES_RecurFee || CDR_RecuFee.Equals(LES_RecurFee))
                                                      && CDR_RecuFee < CUS_RecurFee)
                                {
                                    isRecurFeeMached = true;
                                }
                                else
                                {
                                    isRecurFeeMached = false;
                                    Msg.AppendFormat("This Inmarsat id {0}: Recuring Feee is Less then LES/CUS Recur Fee.<br />", ImarSatID).AppendLine();
                                    Msg.AppendFormat("Recuring Feee from LES CDR Price:{0}.<br />", CDR_RecuFee).AppendLine();
                                    Msg.AppendFormat("Recuring Feee from LES Reg CostPrice:{0}.<br />", LES_RecurFee).AppendLine();
                                    Msg.AppendFormat("Recuring Feee from Cus Reg SellerPrice:{0}.<br />", CUS_RecurFee).AppendLine();
                                    returnMsg.AppendFormat("This Inmarsat id {0}: Recuring Feee is Less then LES/CUS Recur Fee.<br />", ImarSatID).AppendLine();
                                }

                                #region Standard IP rate calculation
                                if (DataUnits_CDR != null)
                                {
                                    long data = Convert.ToInt64(Convert.ToDecimal(DataUnits_CDR));
                                    decimal? CDR_DataTotalUsageMB = (decimal)Converters.ConvertKilobytesToMegabytes(data);

                                    if (CDR_DataTotalUsageMB != null)
                                        CDR_DataTotalUsageMB = Math.Round(Convert.ToDecimal(CDR_DataTotalUsageMB), 2, MidpointRounding.AwayFromZero);

                                    if (is_LESGBPlan)
                                    {
                                        LES_DataPlan = LES_DataPlan * 1024;
                                    }

                                    if (LES_DataPlan < CDR_DataTotalUsageMB)
                                    {
                                        isOutbundleUsed = true;
                                        OutbundleUsedData = CDR_DataTotalUsageMB - LES_DataPlan;
                                        OutbundleUsedData = Math.Round(Convert.ToDecimal(OutbundleUsedData), 2, MidpointRounding.AwayFromZero);
                                    }

                                    decimal? LES_DataCP = CDR_DataTotalUsageMB * LES_DataRateper1MB;
                                    decimal? Cus_DataSP = CDR_DataTotalUsageMB * CUS_DataRateper1MB;
                                    decimal? LES_OutbundleDataCP = OutbundleUsedData * LES_OutbundleRate;
                                    decimal? Cus_OutbundleDataSP = OutbundleUsedData * Cus_OutbundleRate;

                                    if (Cus_DataSP != null)
                                        Cus_DataSP = Math.Round(Convert.ToDecimal(Cus_DataSP), 2, MidpointRounding.AwayFromZero);

                                    if (LES_DataCP != null)
                                        LES_DataCP = Math.Round(Convert.ToDecimal(LES_DataCP), 2, MidpointRounding.AwayFromZero);

                                    if (LES_OutbundleDataCP != null)
                                        LES_OutbundleDataCP = Math.Round(Convert.ToDecimal(LES_OutbundleDataCP), 2, MidpointRounding.AwayFromZero);

                                    if (Cus_OutbundleDataSP != null)
                                        Cus_OutbundleDataSP = Math.Round(Convert.ToDecimal(Cus_OutbundleDataSP), 2, MidpointRounding.AwayFromZero);

                                    if (isOutbundleUsed)
                                    {
                                        CDR_OutbundleRate = CDR_DataCP - CDR_RecuFee;
                                        CDR_OutbundleRate = Math.Round(Convert.ToDecimal(CDR_OutbundleRate), 2, MidpointRounding.AwayFromZero);

                                        //if ((CDR_DataCP < LES_OutbundleDataCP || CDR_DataCP.Equals(LES_OutbundleDataCP)) && (CDR_DataCP < Cus_OutbundleDataSP))
                                        if ((CDR_OutbundleRate < LES_OutbundleDataCP || CDR_OutbundleRate.Equals(LES_OutbundleDataCP)) && (CDR_OutbundleRate < Cus_OutbundleDataSP))
                                        {
                                            isDataMatched = true;
                                            Les_TotalCostCP += LES_OutbundleDataCP;
                                            Cus_TotalCostSP += Cus_OutbundleDataSP;
                                        }
                                        else
                                        {
                                            isDataMatched = false;
                                            Msg.AppendFormat("This Inmarsat id {0}: Data of Background IP data rate is not match.<br />", ImarSatID).AppendLine();
                                            Msg.AppendFormat("Outbundle Data Rate from LES CDR Price:{0}.<br />", CDR_OutbundleRate).AppendLine();
                                            Msg.AppendFormat("Outbundle Data Rate from LES Reg CostPrice:{0}.<br />", LES_OutbundleDataCP).AppendLine();
                                            Msg.AppendFormat("Outbundle Data Rate from Cus Reg SellerPrice:{0}.<br />", Cus_OutbundleDataSP).AppendLine();
                                            returnMsg.AppendFormat("This Inmarsat id {0}: Data of Background IP data rate is not match.<br />", ImarSatID).AppendLine();
                                            Les_TotalCostCP += LES_OutbundleDataCP;
                                            Cus_TotalCostSP += Cus_OutbundleDataSP;

                                        }
                                    }
                                    else
                                    {
                                        //Les_TotalCostCP += LES_DataCP;
                                        Cus_TotalCostSP += Cus_DataSP;
                                        if (CDR_DataCP == 0 || Cus_DataSP == 0)
                                        {
                                            isDataMatched = true;
                                        }
                                        else
                                        {
                                            isDataMatched = false;
                                            Msg.AppendFormat("This Inmarsat id {0}: Data of Background IP data rate is not match.<br />", ImarSatID).AppendLine();
                                            Msg.AppendFormat("Data Rate from LES CDR Price:{0}.<br />", CDR_DataCP).AppendLine();
                                            Msg.AppendFormat("Data Rate from LES Reg CostPrice:{0}.<br />", LES_DataCP).AppendLine();
                                            Msg.AppendFormat("Data Rate from Cus Reg SellerPrice:{0}.<br />", Cus_DataSP).AppendLine();
                                            returnMsg.AppendFormat("This Inmarsat id {0}: Data of Background IP data rate is not match.<br />", ImarSatID).AppendLine();
                                        }
                                    }
                                }
                                else
                                    isDataMatched = true;
                                #endregion

                                #region V2Cellular rate calculation
                                if (CDR_VceCellularUintsCT != null)
                                {
                                    decimal V2CellTotalUnitsCT = 0;

                                    foreach (var ct in lstV2Cell_Trans)
                                    {
                                        if (ct.BCtTranUnitUsage != null)
                                        {
                                            V2CellTotalUnitsCT += Convert.ToDecimal(getMinutes(ct.BCtTranUnitUsage));
                                        }
                                    }

                                    double lngUnits = Convert.ToDouble(V2CellTotalUnitsCT);
                                    decimal? CDR_VceCellulartotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                                    decimal? LES_VceCellCPperMin = CDR_VceCellulartotalUnits_Minutes * LES_VoiceCellular;
                                    decimal? CUS_VceCellSPperMin = CDR_VceCellulartotalUnits_Minutes * CUS_VoiceCellular;

                                    if (CDR_VceCellulartotalUnits_Minutes != null)
                                        CDR_VceCellulartotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceCellulartotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);



                                    if (LES_VceCellCPperMin != null)
                                        LES_VceCellCPperMin = Math.Round(Convert.ToDecimal(LES_VceCellCPperMin), 2, MidpointRounding.AwayFromZero);

                                    if (CUS_VceCellSPperMin != null)
                                        CUS_VceCellSPperMin = Math.Round(Convert.ToDecimal(CUS_VceCellSPperMin), 2, MidpointRounding.AwayFromZero);

                                    Les_TotalCostCP += LES_VceCellCPperMin;
                                    Cus_TotalCostSP += CUS_VceCellSPperMin;

                                    if ((LES_VceCellCPperMin.Equals(CDR_VceCellularCostPriceCT) || LES_VceCellCPperMin > CDR_VceCellularCostPriceCT)
                                        && (CDR_VceCellularCostPriceCT < CUS_VceCellSPperMin))
                                    {
                                        isVoiceCellurlarMatched = true;
                                    }
                                    else
                                    {
                                        isVoiceCellurlarMatched = false;
                                        Msg.AppendFormat("This Inmarsat {0}: Voice to Cellular data rate is less than LES/CUS rate.<br />", ImarSatID).AppendLine();
                                        Msg.AppendFormat("Voice to Cellular Rate from LES CDR Price:{0}.<br />", CDR_VceCellularCostPriceCT).AppendLine();
                                        Msg.AppendFormat("Voice to Cellular Rate from LES Reg CostPrice:{0}.<br />", LES_VceCellCPperMin).AppendLine();
                                        Msg.AppendFormat("Voice to Cellular Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VceCellSPperMin).AppendLine();
                                        returnMsg.AppendFormat("This Inmarsat {0}: Voice to Cellular data rate is less than LES/CUS rate.<br />", ImarSatID).AppendLine();
                                    }
                                }
                                else
                                    isVoiceCellurlarMatched = true;
                                #endregion

                                #region V2Fixed rate calculations
                                if (CDR_VceFixedUintsCT != null)
                                {
                                    decimal V2FixedTotalUnitsCT = 0;

                                    foreach (var ct in lstV2Fixed_Trans)
                                    {
                                        if (ct.BCtTranUnitUsage != null)
                                        {
                                            V2FixedTotalUnitsCT += Convert.ToDecimal(getMinutes(ct.BCtTranUnitUsage));
                                        }
                                    }

                                    double lngUnits = Convert.ToDouble(V2FixedTotalUnitsCT);
                                    decimal? CDR_VceFixedTotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                                    if (CDR_VceFixedTotalUnits_Minutes != null)
                                        CDR_VceFixedTotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceFixedTotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

                                    decimal? LES_VceFixedCPperMin = CDR_VceFixedTotalUnits_Minutes * LES_VoiceFixed;
                                    decimal? CUS_VceFixedSPperMin = CDR_VceFixedTotalUnits_Minutes * CUS_VoiceFixed;

                                    if (LES_VceFixedCPperMin != null)
                                        LES_VceFixedCPperMin = Math.Round(Convert.ToDecimal(LES_VceFixedCPperMin), 2, MidpointRounding.AwayFromZero);

                                    if (CUS_VceFixedSPperMin != null)
                                        CUS_VceFixedSPperMin = Math.Round(Convert.ToDecimal(CUS_VceFixedSPperMin), 2, MidpointRounding.AwayFromZero);

                                    Les_TotalCostCP += LES_VceFixedCPperMin;
                                    Cus_TotalCostSP += CUS_VceFixedSPperMin;

                                    if ((LES_VceFixedCPperMin.Equals(CDR_VceFixedCostPriceCT) || LES_VceFixedCPperMin > CDR_VceFixedCostPriceCT)
                                        && (CDR_VceFixedCostPriceCT < CUS_VceFixedSPperMin))
                                    {
                                        isVoiceFixedMatched = true;
                                    }
                                    else
                                    {
                                        isVoiceFixedMatched = false;
                                        Msg.AppendFormat("This Inmarsat {0}: Voice to Fixed data rate is not match.<br />", ImarSatID).AppendLine();
                                        Msg.AppendFormat("Voice to Fixed Rate from LES CDR Price:{0}.<br />", CDR_VceFixedCostPriceCT).AppendLine();
                                        Msg.AppendFormat("Voice to Fixed Rate from LES Reg CostPrice:{0}.<br />", LES_VceFixedCPperMin).AppendLine();
                                        Msg.AppendFormat("Voice to Fixed Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VceFixedSPperMin).AppendLine();
                                        returnMsg.AppendFormat("This Inmarsat {0}: Voice to Fixed data rate is not match.<br />", ImarSatID).AppendLine();
                                    }
                                }
                                else
                                    isVoiceFixedMatched = true;
                                #endregion

                                #region v2FBB rate Calculations

                                if (CDR_VceFBBUintsCT != null)
                                {
                                    decimal V2FBBTotalUnitsCT = 0;

                                    foreach (var ct in lstV2Fbb_Trans)
                                    {
                                        if (ct.BCtTranUnitUsage != null)
                                        {
                                            V2FBBTotalUnitsCT += Convert.ToDecimal(getMinutes(ct.BCtTranUnitUsage));
                                        }
                                    }

                                    double lngUnits = Convert.ToDouble(V2FBBTotalUnitsCT);
                                    decimal? CDR_VceFBBTotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                                    if (CDR_VceFBBTotalUnits_Minutes != null)
                                        CDR_VceFBBTotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceFBBTotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

                                    decimal? LES_VceFBBCPperMin = CDR_VceFBBTotalUnits_Minutes * LES_VoiceFBB;
                                    decimal? CUS_VceFBBSPperMin = CDR_VceFBBTotalUnits_Minutes * CUS_VoiceFBB;

                                    if (LES_VceFBBCPperMin != null)
                                        LES_VceFBBCPperMin = Math.Round(Convert.ToDecimal(LES_VceFBBCPperMin), 2, MidpointRounding.AwayFromZero);

                                    if (CUS_VceFBBSPperMin != null)
                                        CUS_VceFBBSPperMin = Math.Round(Convert.ToDecimal(CUS_VceFBBSPperMin), 2, MidpointRounding.AwayFromZero);

                                    Les_TotalCostCP += LES_VceFBBCPperMin;
                                    Cus_TotalCostSP += CUS_VceFBBSPperMin;

                                    if ((LES_VceFBBCPperMin.Equals(CDR_VceFBBCostPriceCT) || LES_VceFBBCPperMin > CDR_VceFBBCostPriceCT)
                                        && (CDR_VceFBBCostPriceCT < CUS_VceFBBSPperMin))
                                    {
                                        isVoiceFbbMatched = true;
                                    }
                                    else
                                    {
                                        isVoiceFbbMatched = false;
                                        Msg.AppendFormat("This Inmarsat {0}: Voice to FleetBroadband data rate is less than LES/Cus rate.<br />", ImarSatID).AppendLine();
                                        Msg.AppendFormat("Voice to FleetBroadband Rate from LES CDR Price:{0}.<br />", CDR_VceFBBCostPriceCT).AppendLine();
                                        Msg.AppendFormat("Voice to FleetBroadband Rate from LES Reg CostPrice:{0}.<br />", LES_VceFBBCPperMin).AppendLine();
                                        Msg.AppendFormat("Voice to FleetBroadband Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VceFBBSPperMin).AppendLine();
                                        returnMsg.AppendFormat("This Inmarsat {0}: Voice to FleetBroadband data rate is less than LES/Cus rate.<br />", ImarSatID).AppendLine();
                                    }
                                }
                                else
                                    isVoiceFbbMatched = true;
                                #endregion

                                #region V2Iridium rate calculations

                                if (CDR_VceIridiumUintsCT != null)
                                {
                                    decimal V2IriTotalUnitsCT = 0;

                                    foreach (var ct in lstV2Iridium_Trans)
                                    {
                                        if (ct.BCtTranUnitUsage != null)
                                        {
                                            V2IriTotalUnitsCT += Convert.ToDecimal(getMinutes(ct.BCtTranUnitUsage));
                                        }
                                    }

                                    double lngUnits = Convert.ToDouble(V2IriTotalUnitsCT);
                                    decimal? CDR_VceIridiumTotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                                    if (CDR_VceIridiumTotalUnits_Minutes != null)
                                        CDR_VceIridiumTotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceIridiumTotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

                                    decimal? LES_VceIridiumCPperMin = CDR_VceIridiumTotalUnits_Minutes * LES_VoiceIridium;
                                    decimal? CUS_VceIridiumSPperMin = CDR_VceIridiumTotalUnits_Minutes * CUS_VoiceIridium;

                                    if (LES_VceIridiumCPperMin != null)
                                        LES_VceIridiumCPperMin = Math.Round(Convert.ToDecimal(LES_VceIridiumCPperMin), 2, MidpointRounding.AwayFromZero);

                                    if (CUS_VceIridiumSPperMin != null)
                                        CUS_VceIridiumSPperMin = Math.Round(Convert.ToDecimal(CUS_VceIridiumSPperMin), 2, MidpointRounding.AwayFromZero);

                                    Les_TotalCostCP += LES_VceIridiumCPperMin;
                                    Cus_TotalCostSP += CUS_VceIridiumSPperMin;

                                    if ((LES_VceIridiumCPperMin.Equals(CDR_VceIridiumCostPriceCT) || LES_VceIridiumCPperMin > CDR_VceIridiumCostPriceCT)
                                        && (CDR_VceIridiumCostPriceCT < CUS_VceIridiumSPperMin))
                                    {
                                        isVoiceIridiumMatched = true;
                                    }
                                    else
                                    {
                                        isVoiceIridiumMatched = false;
                                        Msg.AppendFormat("This Inmarsat {0}: Voice to Iridium data rate is less than LES/CUS rate.<br />", ImarSatID).AppendLine();
                                        Msg.AppendFormat("Voice to Iridium Rate from LES CDR Price:{0}.<br />", CDR_VceIridiumCostPriceCT).AppendLine();
                                        Msg.AppendFormat("Voice to Iridium Rate from LES Reg CostPrice:{0}.<br />", LES_VceIridiumCPperMin).AppendLine();
                                        Msg.AppendFormat("Voice to Iridium Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VceIridiumSPperMin).AppendLine();
                                        returnMsg.AppendFormat("This Inmarsat {0}: Voice to Iridium data rate is less than LES/CUS rate.<br />", ImarSatID).AppendLine();
                                    }
                                }
                                else
                                    isVoiceIridiumMatched = true;
                                #endregion

                                #region ISDN Rate Calulations

                                if (CDR_ISDNUintsCT != null)
                                {
                                    decimal V2ISDNTotalUnitsCT = 0;

                                    foreach (var ct in lstISDN_Trans)
                                    {
                                        if (ct.BCtTranUnitUsage != null)
                                        {
                                            V2ISDNTotalUnitsCT += Convert.ToDecimal(getMinutes(ct.BCtTranUnitUsage));
                                        }
                                    }

                                    double lngUnits = Convert.ToDouble(V2ISDNTotalUnitsCT);
                                    decimal? CDR_ISDNTotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                                    if (CDR_ISDNTotalUnits_Minutes != null)
                                        CDR_ISDNTotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_ISDNTotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

                                    decimal? LES_ISDNCPperMin = CDR_ISDNTotalUnits_Minutes * LES_ISDN;
                                    decimal? CUS_ISDNSPperMin = CDR_ISDNTotalUnits_Minutes * CUS_ISDN;

                                    if (LES_ISDNCPperMin != null)
                                        LES_ISDNCPperMin = Math.Round(Convert.ToDecimal(LES_ISDNCPperMin), 2, MidpointRounding.AwayFromZero);

                                    if (CUS_ISDNSPperMin != null)
                                        CUS_ISDNSPperMin = Math.Round(Convert.ToDecimal(CUS_ISDNSPperMin), 2, MidpointRounding.AwayFromZero);

                                    Les_TotalCostCP += LES_ISDNCPperMin;
                                    Cus_TotalCostSP += CUS_ISDNSPperMin;

                                    if ((LES_ISDNCPperMin.Equals(CDR_ISDNCostPriceCT) || LES_ISDNCPperMin > CDR_ISDNCostPriceCT)
                                        && (CDR_ISDNCostPriceCT < CUS_ISDNSPperMin))
                                    {
                                        isISDNMatched = true;
                                    }
                                    else
                                    {
                                        isISDNMatched = false;
                                        Msg.AppendFormat("This Inmarsat {0}: ISDN rate is less than LES/Cus rate.<br />", ImarSatID).AppendLine();
                                        Msg.AppendFormat("ISDN Rate from LES CDR Price:{0}.<br />", CDR_ISDNCostPriceCT).AppendLine();
                                        Msg.AppendFormat("ISDN Rate from LES Reg CostPrice:{0}.<br />", LES_ISDNCPperMin).AppendLine();
                                        Msg.AppendFormat("ISDN Rate from Cus Reg SellerPrice:{0}.<br />", CUS_ISDNSPperMin).AppendLine();
                                        returnMsg.AppendFormat("This Inmarsat {0}: ISDN rate is less than LES/Cus rate.<br />", ImarSatID).AppendLine();
                                    }
                                }
                                else
                                    isISDNMatched = true;
                                #endregion

                                #region SMS rate calculations

                                if (CDR_SMSUintsCT != null)
                                {

                                    decimal? CDR_SMSTotalUnits_Msg = CDR_SMSUintsCT;

                                    if (CDR_SMSTotalUnits_Msg != null)
                                        CDR_SMSTotalUnits_Msg = Math.Round(Convert.ToDecimal(CDR_SMSTotalUnits_Msg), 2, MidpointRounding.AwayFromZero);

                                    decimal? LES_SMSCPperMin = CDR_SMSTotalUnits_Msg * LES_SMS;
                                    decimal? CUS_SMSSPperMin = CDR_SMSTotalUnits_Msg * CUS_SMS;

                                    if (LES_SMSCPperMin != null)
                                        LES_SMSCPperMin = Math.Round(Convert.ToDecimal(LES_SMSCPperMin), 2, MidpointRounding.AwayFromZero);

                                    if (CUS_SMSSPperMin != null)
                                        CUS_SMSSPperMin = Math.Round(Convert.ToDecimal(CUS_SMSSPperMin), 2, MidpointRounding.AwayFromZero);

                                    Les_TotalCostCP += LES_SMSCPperMin;
                                    Cus_TotalCostSP += CUS_SMSSPperMin;

                                    if ((LES_SMSCPperMin.Equals(CDR_SMSCostPriceCT) || LES_SMSCPperMin > CDR_SMSCostPriceCT)
                                        && (CDR_SMSCostPriceCT < CUS_SMSSPperMin))
                                    {
                                        isSMSMatched = true;
                                    }
                                    else
                                    {
                                        isISDNMatched = false;
                                        Msg.AppendFormat("This Inmarsat {0}: SMS rate is less than LES/CUS rate.<br />", ImarSatID).AppendLine();
                                        Msg.AppendFormat("SMS Rate from LES CDR Price:{0}.<br />", CDR_SMSCostPriceCT).AppendLine();
                                        Msg.AppendFormat("SMS Rate from LES Reg CostPrice:{0}.<br />", LES_SMSCPperMin).AppendLine();
                                        Msg.AppendFormat("SMS Rate from Cus Reg SellerPrice:{0}.<br />", CUS_SMSSPperMin).AppendLine();
                                        returnMsg.AppendFormat("This Inmarsat {0}: SMS rate is less than LES/CUS rate.<br />", ImarSatID).AppendLine();
                                    }
                                }
                                else
                                    isSMSMatched = true;
                                #endregion

                                string InvoiceNo = string.Empty;

                                #region Get Invoice Number
                                try
                                {
                                    InvoiceNo = db.BS2MtCusT.Max(t => t.BSMtCusInvNo);

                                    if (!string.IsNullOrEmpty(InvoiceNo))
                                    {
                                        if (InvoiceNo.Contains("SM"))
                                        {
                                            string tempInv = InvoiceNo.Replace("SM", string.Empty);
                                            if (int.TryParse(tempInv, out int result))
                                            {
                                                InvoiceNo = BillingHelpers.GenInvoice(false, result);
                                            }
                                        }
                                    }
                                    else
                                        InvoiceNo = BillingHelpers.GenInvoice(false, 0);
                                }
                                catch (Exception error)
                                {
                                    Msg.AppendFormat("MSI:{0} Error Msg: {1} Stack Trace: {2}. <br />", ImarSatID, error.Message.ToString(), error.StackTrace);
                                }
                                #endregion

                                bool isLES_ContrRegDate = false;
                                bool isLES_ContrBarDate = false;
                                bool isLES_ContrSusDate = false;
                                bool isLES_ContrLayUpDate = false;

                                bool isCUS_ContrRegDate = false;
                                bool isCUS_ContrBarDate = false;
                                bool isCUS_ContrSusDate = false;
                                bool isCUS_ContrLayUpDate = false;

                                #region check Rejection condition and store bool value
                                if ((CDR_StartDate >= LES_ContrRegStrDate && CDR_EndDate <= LES_ContrRegEndDate) || isNoTransData)
                                    isLES_ContrRegDate = true;
                                else
                                {
                                    isLES_ContrRegDate = false;
                                    Msg.AppendFormat("This Inmarsat {0}: Contract CDR Start/End date Less than les Reg Start/End Date.<br />", ImarSatID).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: LES Reg Strart Date {1}.<br />", CDR_StartDate, LES_ContrRegStrDate).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: LES Reg End Date {1}.<br />", CDR_StartDate, LES_ContrRegEndDate).AppendLine();
                                    returnMsg.AppendFormat("This Inmarsat {0}: Contract Reg start date Less than les Reg start Date.<br />", ImarSatID).AppendLine();
                                }

                                if ((LES_ContrBarStrDate == null && LES_ContrBarEndDate == null) || (CDR_StartDate > LES_ContrBarStrDate && CDR_EndDate > LES_ContrBarEndDate))
                                    isLES_ContrBarDate = true;
                                else
                                {
                                    isLES_ContrBarDate = false;
                                    Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than lES Bar Start/End Date.<br />", ImarSatID).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: LES Bar Strart Date {1}.<br />", CDR_StartDate, LES_ContrBarStrDate).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: LES Bar End Date {1}.<br />", CDR_StartDate, LES_ContrBarEndDate).AppendLine();
                                    returnMsg.AppendFormat("This Inmarsat {0}: Contract CCDR start date Greater than lES Bar start Date.<br />", ImarSatID).AppendLine();
                                }

                                if ((LES_ContrSusStrDate == null && LES_ContrSusEndDate == null) || (CDR_StartDate > LES_ContrSusStrDate && CDR_EndDate > LES_ContrSusEndDate))
                                    isLES_ContrSusDate = true;
                                else
                                {
                                    isLES_ContrBarDate = false;
                                    Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than lES Suspenstion Start/End Date.<br />", ImarSatID).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: LES Suspenstion Start Date {1}.<br />", CDR_StartDate, LES_ContrSusStrDate).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: LES Suspenstion End Date {1}.<br />", CDR_StartDate, LES_ContrSusEndDate).AppendLine();
                                    returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than lES Suspenstion start Date.<br />", ImarSatID).AppendLine();
                                }

                                if ((LES_ContrLayoutStrDate == null && LES_ContrLayoutEndDate == null) || (CDR_StartDate > LES_ContrLayoutStrDate && CDR_EndDate > LES_ContrLayoutEndDate))
                                    isLES_ContrLayUpDate = true;
                                else
                                {
                                    isLES_ContrLayUpDate = true;
                                    Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than LES LayUp Start/End Date.<br />", ImarSatID).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: LES LayUp Start Date {1}.<br />", CDR_StartDate, LES_ContrSusStrDate).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: LES LayUp End Date {1}.<br />", CDR_StartDate, LES_ContrSusEndDate).AppendLine();
                                    returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than LayUp Start/End Date.<br />", ImarSatID).AppendLine();
                                }

                                if ((CDR_StartDate >= CUS_ContrRegStrDate && CDR_EndDate <= CUS_ContrRegEndDate) || isNoTransData)
                                    isCUS_ContrRegDate = true;
                                else
                                {
                                    isCUS_ContrRegDate = false;
                                    Msg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Less than CUS Reg Start/End Date.<br />", ImarSatID).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: CUS Reg Strart Date {1}.<br />", CDR_StartDate, CUS_ContrRegStrDate).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: CUS Reg End Date {1}.<br />", CDR_StartDate, CUS_ContrRegEndDate).AppendLine();
                                    returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Less than CUS Reg Start/End Date.<br />", ImarSatID).AppendLine();
                                }

                                if ((CUS_ContrBarStrDate == null && CUS_ContrBarEndDate == null) || (CDR_StartDate > CUS_ContrBarStrDate && CDR_EndDate > CUS_ContrBarEndDate))
                                    isCUS_ContrBarDate = true;
                                else
                                {
                                    isCUS_ContrBarDate = false;
                                    Msg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Greater than CUS Bar Start/End Date.<br />", ImarSatID).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: CUS Bar Strart Date {1}.<br />", CDR_StartDate, CUS_ContrBarStrDate).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: CUS Bar End Date {1}.<br />", CDR_StartDate, CUS_ContrBarStrDate).AppendLine();
                                    returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Greater than CUS Bar Start/End Date.<br />", ImarSatID).AppendLine();
                                }

                                if ((CUS_ContrSusStrDate == null && CUS_ContrSusEndDate == null) || (CDR_StartDate > CUS_ContrSusStrDate && CDR_EndDate > CUS_ContrSusEndDate))
                                    isCUS_ContrSusDate = true;
                                else
                                {
                                    isCUS_ContrSusDate = false;
                                    Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS Suspenstion Start/End Date.<br />", ImarSatID).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: CUS Suspenstion Start Date {1}.<br />", CDR_StartDate, CUS_ContrSusStrDate).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: CUS Suspenstion End Date {1}.<br />", CDR_StartDate, CUS_ContrSusEndDate).AppendLine();
                                    returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS Suspenstion Start/End Date.<br />", ImarSatID).AppendLine();
                                }

                                if ((CUS_ContrLayoutStrDate == null && CUS_ContrLayoutEndDate == null) || (CDR_StartDate > CUS_ContrLayoutStrDate && CDR_EndDate > CUS_ContrLayoutEndDate))
                                    isCUS_ContrLayUpDate = true;
                                else
                                {
                                    isCUS_ContrLayUpDate = true;
                                    Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS LayUp Start/End Date.<br />", ImarSatID).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: CUS LayUp Start Date {1}.<br />", CDR_StartDate, LES_ContrSusStrDate).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: CUS LayUp End Date {1}.<br />", CDR_StartDate, LES_ContrSusEndDate).AppendLine();
                                    returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS LayUp Start/End Date.<br />", ImarSatID).AppendLine();
                                }
                                #endregion


                                if (isRecurFeeMached && isDataMatched && isVoiceCellurlarMatched && isVoiceFixedMatched && isVoiceFbbMatched && isVoiceIridiumMatched &&
                                    isISDNMatched && isLES_ContrRegDate && isLES_ContrBarDate && isLES_ContrSusDate && isLES_ContrLayUpDate && isCUS_ContrRegDate &&
                                    isCUS_ContrBarDate && isCUS_ContrSusDate && isCUS_ContrLayUpDate && isSMSMatched)
                                {
                                    if (!string.IsNullOrEmpty(InvoiceNo))
                                    {
                                        BS2MtCusT SecMtCust = new BS2MtCusT();

                                        SecMtCust.BSMtCusInvNo = InvoiceNo;
                                        SecMtCust.fkSPID = LesID;
                                        SecMtCust.fkCusID = CUS_Id;
                                        SecMtCust.fkCusConID = CUS_ContractID;
                                        SecMtCust.fkCusRegID = CUS_RegsID;
                                        //SecMtCust.fkspcID = 
                                        SecMtCust.fkSimID = SimID;
                                        SecMtCust.fkLesRefID = CT_Fbs.BCtFbLesRefCode;
                                        SecMtCust.fkSatTypeID = CT_Fbs.BCtFbSatCode;
                                        SecMtCust.fkEquipTypeID = CT_Fbs.BCtFbEquipmentCode;
                                        //SecMtCust.BSMtCusLesCode 
                                        SecMtCust.BSMtCusCdrCode = CT_Fbs.BCtFbCdrCode;
                                        SecMtCust.BSMtCusSatType = null;
                                        SecMtCust.BSMtCusServDest = CT_Fbs.BCtTranServDest;
                                        SecMtCust.BSMtCusServType = CT_Fbs.BCtTranServType;
                                        SecMtCust.BSMtCusTransUsage = Convert.ToInt32(CT_Fbs.BCtTranUnitUsage);
                                        SecMtCust.BSMtCusUOM = CT_Fbs.BCtTranUnitType;
                                        SecMtCust.BSMtCusRecurFee = CUS_RecurFee;
                                        Cus_TotalCostSP += SecMtCust.BSMtCusRecurFee;
                                        SecMtCust.BSMtCusRecurFeeType = CT_Fbs.BCtAFbRecurFeeType;
                                        //SecMtCust.BSMtCusCDRRef = CT_Fbs.BCtAFbCDRRef;
                                        SecMtCust.BSMtCusBillNo = CT_Fbs.BCtFbBillNo;
                                        SecMtCust.BSMtCusBillPeriod = CT_Fbs.BCtFbBillPeriod;
                                        SecMtCust.BSMtCusBillDate = CT_Fbs.BCtFbBillDate;

                                        if (CDR_RecuFee < CDR_DataCP && isOutbundleUsed)
                                        {
                                            LES_InvRptDataCostTot = CDR_DataCP - CDR_RecuFee;
                                            LES_InvRptDataCostTot = Math.Round(Convert.ToDecimal(LES_InvRptDataCostTot), 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                            LES_InvRptDataCostTot = 0;

                                        db.BS2MtCusT.Add(SecMtCust);
                                        db.SaveChanges();

                                        int SecFBBID = SecMtCust.BSMtCusTID;

                                        // Get value from BS2MtCusT and save to BInvRprt table
                                        BInvLstReport Invreport = new BInvLstReport();

                                        if (SecMtCust.BSMtCusServType != null)
                                        {
                                            Invreport.fkBCus2ID = SecFBBID;
                                            Invreport.BInvRprtSerType = SecMtCust.BSMtCusServType;
                                            Invreport.BInvRprtTotBil = SecMtCust.BSMtCusRecurFee;
                                            Invreport.BInvRprtTotCst = CT_Fbs.BCtFbRecurFee;
                                            db.BInvLstReports.Add(Invreport);
                                        }

                                        Bs2Sec bs2Sec;
                                        decimal? outbindleData = 0;
                                        decimal? LES_RptDataCost = 0;
                                        decimal? CUS_RptDataCost = 0;

                                        if (is_CUSGBPlan)
                                        {
                                            Cus_DataPlan = Cus_DataPlan * 1024;
                                        }

                                        foreach (var records in lstStdIP_Trans)
                                        {
                                            bs2Sec = new Bs2Sec();
                                            bs2Sec.fkCTFbID = SecFBBID;
                                            bs2Sec.BsCtStartDate = records.BCtTranStartDate;
                                            bs2Sec.BsCtStarTime = records.BCtTranStartTime;
                                            bs2Sec.BsCtOrigion = records.BCtTranOrigin;
                                            bs2Sec.BsCtDestination = records.BCtTranServDest;
                                            bs2Sec.fkSPServTypeID = CUS_ServTypeID;

                                            double dataconvertToMP = Convert.ToDouble(records.BCtTranUnitUsage);
                                            bs2Sec.BsCtTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertBitToMegabytes(dataconvertToMP)), 3, MidpointRounding.AwayFromZero);
                                            bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("MB")).UID;
                                            outbindleData += bs2Sec.BsCtTransUsage;

                                            if (Cus_DataPlan <= outbindleData)
                                            {
                                                bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * Cus_OutbundleRate), 3, MidpointRounding.AwayFromZero);

                                                if (Invreport.BInvRprtSerType != null)
                                                    CUS_RptDataCost += bs2Sec.BsCtTotalUsage;
                                            }
                                            else
                                            {
                                                bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * 0), 3, MidpointRounding.AwayFromZero);

                                                if (Invreport.BInvRprtSerType != null)
                                                    CUS_RptDataCost += bs2Sec.BsCtTotalUsage;
                                            }
                                            bs2Sec.fkCommType = 1;

                                            db.Bs2Sec.Add(bs2Sec);
                                        }

                                        // Invoice List Report Add in BInvLstReports
                                        if (lstStdIP_Trans.Count > 0)
                                        {
                                            Invreport = new BInvLstReport();
                                            try
                                            {
                                                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeID).BsPtCusServiceTypes;
                                            }
                                            catch { Invreport.BInvRprtSerType = null; }

                                            if (Invreport.BInvRprtSerType != null)
                                            {
                                                Invreport.fkBCus2ID = SecFBBID;
                                                Invreport.BInvRprtTotCst = LES_InvRptDataCostTot;
                                                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptDataCost), 3, MidpointRounding.AwayFromZero);
                                                db.BInvLstReports.Add(Invreport);
                                            }
                                        }

                                        decimal? LES_RptV2cCost = CDR_VceCellularCostPriceCT;
                                        decimal? CUS_RptV2cCost = 0;

                                        foreach (var records in lstV2Cell_Trans)
                                        {
                                            bs2Sec = new Bs2Sec();
                                            bs2Sec.fkCTFbID = SecFBBID;
                                            bs2Sec.BsCtStartDate = records.BCtTranStartDate;
                                            bs2Sec.BsCtStarTime = records.BCtTranStartTime;
                                            bs2Sec.BsCtOrigion = records.BCtTranOrigin;
                                            bs2Sec.BsCtDestination = records.BCtTranServDest;
                                            bs2Sec.fkSPServTypeID = CUS_ServTypeMobile;

                                            decimal? v2CTransSec = 0;
                                            if (records.BCtTranUnitUsage != null)
                                            {
                                                v2CTransSec = getMinutes(records.BCtTranUnitUsage);
                                            }
                                            long SecconvertToMin = Convert.ToInt64(v2CTransSec);
                                            bs2Sec.BsCtTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 3, MidpointRounding.AwayFromZero);
                                            bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                            bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_VoiceCellular), 3, MidpointRounding.AwayFromZero);

                                            if (Invreport.BInvRprtSerType != null)
                                                CUS_RptV2cCost += bs2Sec.BsCtTotalUsage;
                                            bs2Sec.fkCommType = 2;

                                            db.Bs2Sec.Add(bs2Sec);
                                        }

                                        // Invoice List Report Add in BInvLstReports
                                        if (lstV2Cell_Trans.Count > 0)
                                        {
                                            Invreport = new BInvLstReport();
                                            try
                                            {
                                                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeMobile).BsPtCusServiceTypes;
                                            }
                                            catch { Invreport.BInvRprtSerType = null; }

                                            if (Invreport.BInvRprtSerType != null)
                                            {
                                                Invreport.fkBCus2ID = SecFBBID;
                                                Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2cCost), 2, MidpointRounding.AwayFromZero);
                                                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2cCost), 2, MidpointRounding.AwayFromZero);
                                                db.BInvLstReports.Add(Invreport);
                                            }

                                        }

                                        decimal? LES_RptV2fCost = CDR_VceFixedCostPriceCT;
                                        decimal? CUS_RptV2fCost = 0;

                                        foreach (var records in lstV2Fixed_Trans)
                                        {
                                            bs2Sec = new Bs2Sec();
                                            bs2Sec.fkCTFbID = SecFBBID;
                                            bs2Sec.BsCtStartDate = records.BCtTranStartDate;
                                            bs2Sec.BsCtStarTime = records.BCtTranStartTime;
                                            bs2Sec.BsCtOrigion = records.BCtTranOrigin;
                                            bs2Sec.BsCtDestination = records.BCtTranServDest;
                                            bs2Sec.fkSPServTypeID = CUS_ServTypeVce2Fixed;

                                            decimal? v2FixedTransSec = 0;
                                            if (records.BCtTranUnitUsage != null)
                                            {
                                                v2FixedTransSec = getMinutes(records.BCtTranUnitUsage);
                                            }

                                            long SecconvertToMin = Convert.ToInt64(v2FixedTransSec);
                                            bs2Sec.BsCtTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 3, MidpointRounding.AwayFromZero);
                                            bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                            bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_VoiceFixed), 3, MidpointRounding.AwayFromZero);

                                            if (Invreport.BInvRprtSerType != null)
                                                CUS_RptV2fCost += bs2Sec.BsCtTotalUsage;
                                            bs2Sec.fkCommType = 2;

                                            db.Bs2Sec.Add(bs2Sec);
                                        }

                                        // Invoice List Report Add in BInvLstReports
                                        if (lstV2Fixed_Trans.Count > 0)
                                        {
                                            Invreport = new BInvLstReport();

                                            try
                                            {
                                                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeVce2Fixed).BsPtCusServiceTypes;
                                            }
                                            catch { Invreport.BInvRprtSerType = null; }

                                            if (Invreport.BInvRprtSerType != null)
                                            {
                                                Invreport.fkBCus2ID = SecFBBID;
                                                Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2fCost), 3, MidpointRounding.AwayFromZero);
                                                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2fCost), 3, MidpointRounding.AwayFromZero);
                                                db.BInvLstReports.Add(Invreport);
                                            }
                                        }

                                        decimal? LES_RptV2fbCost = CDR_VceFBBCostPriceCT;
                                        decimal? CUS_RptV2fbCost = 0;

                                        foreach (var records in lstV2Fbb_Trans)
                                        {
                                            bs2Sec = new Bs2Sec();
                                            bs2Sec.fkCTFbID = SecFBBID;
                                            bs2Sec.BsCtStartDate = records.BCtTranStartDate;
                                            bs2Sec.BsCtStarTime = records.BCtTranStartTime;
                                            bs2Sec.BsCtOrigion = records.BCtTranOrigin;
                                            bs2Sec.BsCtDestination = records.BCtTranServDest;
                                            bs2Sec.fkSPServTypeID = CUS_ServTypeVce2FBB;

                                            decimal? v2FBBTransSec = 0;
                                            if (records.BCtTranUnitUsage != null)
                                            {
                                                v2FBBTransSec = getMinutes(records.BCtTranUnitUsage);
                                            }

                                            long SecconvertToMin = Convert.ToInt64(v2FBBTransSec);
                                            bs2Sec.BsCtTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 3, MidpointRounding.AwayFromZero);
                                            bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                            bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_VoiceFBB), 3, MidpointRounding.AwayFromZero);

                                            if (Invreport.BInvRprtSerType != null)
                                                CUS_RptV2fbCost += bs2Sec.BsCtTotalUsage;
                                            bs2Sec.fkCommType = 2;

                                            db.Bs2Sec.Add(bs2Sec);
                                        }

                                        if (lstV2Fbb_Trans.Count > 0)
                                        {
                                            Invreport = new BInvLstReport();
                                            try
                                            {
                                                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeVce2FBB).BsPtCusServiceTypes;
                                            }
                                            catch { Invreport.BInvRprtSerType = null; }

                                            if (Invreport.BInvRprtSerType != null)
                                            {
                                                Invreport.fkBCus2ID = SecFBBID;
                                                Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2fbCost), 3, MidpointRounding.AwayFromZero);
                                                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2fbCost), 3, MidpointRounding.AwayFromZero);
                                                db.BInvLstReports.Add(Invreport);
                                            }
                                        }

                                        decimal? LES_RptV2irdCost = CDR_VceIridiumCostPriceCT;
                                        decimal? CUS_RptV2irdCost = 0;

                                        foreach (var records in lstV2Iridium_Trans)
                                        {
                                            bs2Sec = new Bs2Sec();
                                            bs2Sec.fkCTFbID = SecFBBID;
                                            bs2Sec.BsCtStartDate = records.BCtTranStartDate;
                                            bs2Sec.BsCtStarTime = records.BCtTranStartTime;
                                            bs2Sec.BsCtOrigion = records.BCtTranOrigin;
                                            bs2Sec.BsCtDestination = records.BCtTranServDest;
                                            bs2Sec.fkSPServTypeID = CUS_ServTypeVce2Iridium;

                                            decimal? v2IriTransSec = 0;
                                            if (records.BCtTranUnitUsage != null)
                                            {
                                                v2IriTransSec = getMinutes(records.BCtTranUnitUsage);
                                            }

                                            long SecconvertToMin = Convert.ToInt64(v2IriTransSec);

                                            bs2Sec.BsCtTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 3, MidpointRounding.AwayFromZero);
                                            bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                            bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_VoiceIridium), 3, MidpointRounding.AwayFromZero);

                                            if (Invreport.BInvRprtSerType != null)
                                                CUS_RptV2irdCost += bs2Sec.BsCtTotalUsage;
                                            bs2Sec.fkCommType = 2;

                                            db.Bs2Sec.Add(bs2Sec);
                                        }

                                        // Invoice List Report Add in BInvLstReports
                                        if (lstV2Iridium_Trans.Count > 0)
                                        {
                                            Invreport = new BInvLstReport();
                                            try
                                            {
                                                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeVce2Iridium).BsPtCusServiceTypes;
                                            }
                                            catch { Invreport.BInvRprtSerType = null; }

                                            if (Invreport.BInvRprtSerType != null)
                                            {
                                                Invreport.fkBCus2ID = SecFBBID;
                                                Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2irdCost), 3, MidpointRounding.AwayFromZero);
                                                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2irdCost), 3, MidpointRounding.AwayFromZero);
                                                db.BInvLstReports.Add(Invreport);
                                            }
                                        }

                                        decimal? LES_RptV2isdnCost = CDR_ISDNCostPriceCT;
                                        decimal? CUS_RptV2isdnCost = 0;

                                        foreach (var records in lstISDN_Trans)
                                        {
                                            bs2Sec = new Bs2Sec();
                                            bs2Sec.fkCTFbID = SecFBBID;
                                            bs2Sec.BsCtStartDate = records.BCtTranStartDate;
                                            bs2Sec.BsCtStarTime = records.BCtTranStartTime;
                                            bs2Sec.BsCtOrigion = records.BCtTranOrigin;
                                            bs2Sec.BsCtDestination = records.BCtTranServDest;
                                            bs2Sec.fkSPServTypeID = CUS_ServTypeVce2ISDN;

                                            decimal? v2ISDNTransSec = 0;
                                            if (records.BCtTranUnitUsage != null)
                                            {
                                                v2ISDNTransSec = getMinutes(records.BCtTranUnitUsage);
                                            }

                                            long SecconvertToMin = Convert.ToInt64(v2ISDNTransSec);

                                            bs2Sec.BsCtTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 3, MidpointRounding.AwayFromZero);
                                            bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                            bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_ISDN), 3, MidpointRounding.AwayFromZero);

                                            if (Invreport.BInvRprtSerType != null)
                                                CUS_RptV2isdnCost += bs2Sec.BsCtTotalUsage;
                                            bs2Sec.fkCommType = 2;

                                            db.Bs2Sec.Add(bs2Sec);
                                        }

                                        // Invoice List Report Add in BInvLstReports
                                        if (lstISDN_Trans.Count > 0)
                                        {
                                            Invreport = new BInvLstReport();
                                            try
                                            {
                                                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeVce2ISDN).BsPtCusServiceTypes;
                                            }
                                            catch { Invreport.BInvRprtSerType = null; }

                                            if (Invreport.BInvRprtSerType != null)
                                            {
                                                Invreport.fkBCus2ID = SecFBBID;
                                                Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2isdnCost), 3, MidpointRounding.AwayFromZero);
                                                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2isdnCost), 3, MidpointRounding.AwayFromZero);
                                                db.BInvLstReports.Add(Invreport);
                                            }
                                        }

                                        int commtype = 1;
                                        try
                                        {
                                            commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                                        }
                                        catch
                                        {
                                            commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                                        }

                                        decimal? LES_RptV2smsCost = CDR_SMSCostPriceCT;
                                        decimal? CUS_RptV2smsCost = 0;

                                        foreach (var records in lstSMS_Trans)
                                        {
                                            bs2Sec = new Bs2Sec();
                                            bs2Sec.fkCTFbID = SecFBBID;
                                            bs2Sec.BsCtStartDate = records.BCtTranStartDate;
                                            bs2Sec.BsCtStarTime = records.BCtTranStartTime;
                                            bs2Sec.BsCtOrigion = records.BCtTranOrigin;
                                            bs2Sec.BsCtDestination = records.BCtTranServDest;
                                            bs2Sec.fkSPServTypeID = CUS_ServTypeSMS;

                                            //long SecconvertToMin = Convert.ToInt64(records.BsCtBillIncre);
                                            bs2Sec.BsCtTransUsage = Convert.ToInt32(records.BCtTranUnitUsage); //(decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                            try
                                            {
                                                bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("message")).UID;
                                            }
                                            catch
                                            {
                                                bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Message")).UID;
                                            }

                                            bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtTransUsage * CUS_SMS), 3, MidpointRounding.AwayFromZero);

                                            if (Invreport.BInvRprtSerType != null)
                                                CUS_RptV2smsCost += bs2Sec.BsCtTotalUsage;
                                            bs2Sec.fkCommType = commtype;

                                            db.Bs2Sec.Add(bs2Sec);
                                        }

                                        // Invoice List Report Add in BInvLstReports
                                        if (lstSMS_Trans.Count > 0)
                                        {
                                            Invreport = new BInvLstReport();
                                            try
                                            {
                                                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeSMS).BsPtCusServiceTypes;
                                            }
                                            catch { Invreport.BInvRprtSerType = null; }
                                            if (Invreport.BInvRprtSerType != null)
                                            {
                                                Invreport.fkBCus2ID = SecFBBID;
                                                Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2smsCost), 3, MidpointRounding.AwayFromZero);
                                                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2smsCost), 3, MidpointRounding.AwayFromZero);
                                                db.BInvLstReports.Add(Invreport);
                                            }
                                        }

                                        // List of Satatement 
                                        Bs2Statement statment = null;
                                        try
                                        {
                                            statment = db.Bs2Statement.FirstOrDefault(t => t.fkBs2CusID == SecFBBID);
                                        }
                                        catch { }

                                        if (statment == null)
                                        {
                                            statment = new Bs2Statement();

                                            statment.fkBs2CusID = SecFBBID;
                                            statment.fkSimID = SimID;
                                            statment.Bs2StTotalCP = Les_TotalCostCP;
                                            statment.Bs2StTotalSP = Cus_TotalCostSP;
                                            statment.Bs2StSMTSCost = SMTS_TotalCost;
                                            statment.fkSPID = LesID;
                                            statment.fkCusID = CUS_Id;
                                            statment.Bs2StIvoiceDate = DateTime.Now.Date;
                                            statment.Bs2StDuration = CT_Fbs.BCtTranServDest;

                                            if (statment.Bs2StTotalCP > 0 && statment.Bs2StTotalSP > 0)
                                            {
                                                decimal CrossProfit = Convert.ToDecimal(statment.Bs2StTotalSP - statment.Bs2StSMTSCost);
                                                decimal Margin = (CrossProfit / Convert.ToDecimal(statment.Bs2StTotalSP) * 100);
                                                statment.Bs2stMargin = Margin > 0 ? Convert.ToInt32(Margin) : (int)Math.Abs(Margin);
                                            }

                                            db.Bs2Statement.Add(statment);
                                        }

                                        try
                                        {
                                            db.SaveChanges();
                                        }
                                        catch (Exception error) { throw error; }
                                        returnMsg.AppendFormat("This InmarSat {0}: Successfully stored to Invoice.<br />", ImarSatID);
                                    }
                                }
                                else
                                {
                                    BS1ALessT SecRejLes = new BS1ALessT();

                                    SecRejLes.fkSPID = LesID;
                                    SecRejLes.fkCusID = CUS_Id;
                                    SecRejLes.fkCusConID = CUS_ContractID;
                                    SecRejLes.fkCusRegID = CUS_RegsID;
                                    //SecRejLes.fkspcID = CT_Fbs.BCtAfkLESCID;
                                    SecRejLes.fkSimID = SimID;
                                    SecRejLes.fkLesRefID = CT_Fbs.BCtFbLesRefCode;
                                    SecRejLes.fkSatTypeID = CT_Fbs.BCtFbSatCode;
                                    SecRejLes.fkEquipTypeID = CT_Fbs.BCtFbEquipmentCode;
                                    //SecRejLes.BS1ALesLesCode = CT_Fbs.BCtAFbLesCode;
                                    SecRejLes.BS1ALesCdrCode = CT_Fbs.BCtFbCdrCode;
                                    SecRejLes.BS1ALesServDest = CT_Fbs.BCtTranServDest;
                                    SecRejLes.BS1ALesServType = CT_Fbs.BCtTranServType;
                                    SecRejLes.BS1ALesTransUsage = Convert.ToInt32(CT_Fbs.BCtTranUnitUsage);
                                    SecRejLes.BS1ALesUOM = CT_Fbs.BCtTranUnitType;
                                    SecRejLes.BS1ALesRecurFee = LES_RecurFee;
                                    SecRejLes.BS1ALesRecurFeeType = CT_Fbs.BCtAFbRecurFeeType;
                                    //SecRejLes.BS1ALesCDRRef = CT_Fbs.BCtAFbCDRRef;
                                    SecRejLes.BS1ALesBillNo = CT_Fbs.BCtFbBillNo;
                                    SecRejLes.BS1ALesBillPeriod = CT_Fbs.BCtFbBillPeriod;
                                    SecRejLes.BS1ALesBillDate = CT_Fbs.BCtFbBillDate;

                                    db.BS1ALessT.Add(SecRejLes);
                                    try
                                    {
                                        db.SaveChanges();
                                    }
                                    catch (Exception err)
                                    {

                                    }

                                    int SecRejID = SecRejLes.BS1ALesID;

                                    if (CDR_RecuFee < CDR_DataCP && isOutbundleUsed)
                                    {
                                        LES_InvRptDataCostTot = CDR_DataCP - CDR_RecuFee;
                                        LES_InvRptDataCostTot = Math.Round(Convert.ToDecimal(LES_InvRptDataCostTot), 2, MidpointRounding.AwayFromZero);
                                    }
                                    else
                                        LES_InvRptDataCostTot = 0;

                                    BInvLstReport InvreportCost = new BInvLstReport();
                                    if (SecRejLes.BS1ALesServType != null)
                                    {
                                        InvreportCost.BInvRprtLes1ID = SecRejID;
                                        InvreportCost.BInvRprtSerType = SecRejLes.BS1ALesServType;
                                        InvreportCost.BInvRprtTotCst = CDR_RecuFee;
                                        InvreportCost.BInvRprtTotBil = null;

                                        db.BInvLstReports.Add(InvreportCost);
                                    }

                                    BS1LesRejLog lesRejLog = new BS1LesRejLog();
                                    lesRejLog.fkBs1LesReg = SecRejID;
                                    lesRejLog.Bs1lesRejection = Msg.ToString();
                                    lesRejLog.Bs1lesRejRemark = "";
                                    db.BS1LesRejLog.Add(lesRejLog);

                                    #region Standard IP Tranasction

                                    Bs1BLesT lesRej1;
                                    foreach (var records in lstStdIP_Trans)
                                    {
                                        lesRej1 = new Bs1BLesT();
                                        lesRej1.fkBs1BLesID = SecRejID;
                                        lesRej1.Bs1BLesStartDate = records.BCtTranStartDate;
                                        lesRej1.Bs1BLesStarTime = records.BCtTranStartTime;
                                        lesRej1.Bs1BLesOrigion = records.BCtTranOrigin;
                                        lesRej1.Bs1BLesDestination = records.BCtTranServDest;
                                        lesRej1.fkSPServTypeID = ServTypeID;

                                        double dataconvertToMP = Convert.ToDouble(records.BCtTranUnitUsage);
                                        lesRej1.Bs1BLesTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertBitToMegabytes(dataconvertToMP)), 2, MidpointRounding.AwayFromZero);
                                        lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("MB")).UID;
                                        lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_DataRateper1MB), 2, MidpointRounding.AwayFromZero);
                                        lesRej1.fkCommType = 1;

                                        db.Bs1BLesT.Add(lesRej1);
                                    }

                                    // Invoice List Report Add in BInvLstReports
                                    if (lstStdIP_Trans.Count > 0)
                                    {
                                        InvreportCost = new BInvLstReport();
                                        try
                                        {
                                            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeID).BsPtServiceTypes;
                                        }
                                        catch { InvreportCost.BInvRprtSerType = null; }

                                        if (InvreportCost.BInvRprtSerType != null)
                                        {
                                            InvreportCost.BInvRprtLes1ID = SecRejID;
                                            InvreportCost.fkBCus2ID = null;
                                            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_InvRptDataCostTot), 2, MidpointRounding.AwayFromZero);
                                            InvreportCost.BInvRprtTotBil = null;
                                            db.BInvLstReports.Add(InvreportCost);
                                        }

                                    }
                                    #endregion

                                    #region Reject2 V2 Cellular Trans

                                    foreach (var records in lstV2Cell_Trans)
                                    {
                                        lesRej1 = new Bs1BLesT();
                                        lesRej1.fkBs1BLesID = SecRejID;
                                        lesRej1.Bs1BLesStartDate = records.BCtTranStartDate;
                                        lesRej1.Bs1BLesStarTime = records.BCtTranStartTime;
                                        lesRej1.Bs1BLesOrigion = records.BCtTranOrigin;
                                        lesRej1.Bs1BLesDestination = records.BCtTranServDest;
                                        lesRej1.fkSPServTypeID = ServTypeMobile;

                                        decimal? v2CTransSec = 0;
                                        if (records.BCtTranUnitUsage != null)
                                        {
                                            v2CTransSec = getMinutes(records.BCtTranUnitUsage);
                                        }

                                        long SecconvertToMin = Convert.ToInt64(v2CTransSec);
                                        lesRej1.Bs1BLesTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                        lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                        lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_VoiceCellular), 2, MidpointRounding.AwayFromZero);
                                        lesRej1.fkCommType = 2;

                                        db.Bs1BLesT.Add(lesRej1);
                                    }

                                    // Invoice List Report Add in BInvLstReports
                                    if (lstV2Cell_Trans.Count > 0)
                                    {
                                        InvreportCost = new BInvLstReport();
                                        try
                                        {
                                            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeMobile).BsPtServiceTypes;
                                        }
                                        catch { InvreportCost.BInvRprtSerType = null; }

                                        if (InvreportCost.BInvRprtSerType != null)
                                        {
                                            InvreportCost.BInvRprtLes1ID = SecRejID;
                                            InvreportCost.fkBCus2ID = null;
                                            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_VceCellularCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                            InvreportCost.BInvRprtTotBil = null;
                                            db.BInvLstReports.Add(InvreportCost);
                                        }
                                    }
                                    #endregion

                                    #region Reject V2Fixed Trans

                                    foreach (var records in lstV2Fixed_Trans)
                                    {
                                        lesRej1 = new Bs1BLesT();
                                        lesRej1.fkBs1BLesID = SecRejID;
                                        lesRej1.Bs1BLesStartDate = records.BCtTranStartDate;
                                        lesRej1.Bs1BLesStarTime = records.BCtTranStartTime;
                                        lesRej1.Bs1BLesOrigion = records.BCtTranOrigin;
                                        lesRej1.Bs1BLesDestination = records.BCtTranServDest;
                                        lesRej1.fkSPServTypeID = ServTypeVce2Fixed;

                                        decimal? v2FixedTransSec = 0;
                                        if (records.BCtTranUnitUsage != null)
                                        {
                                            v2FixedTransSec = getMinutes(records.BCtTranUnitUsage);
                                        }

                                        long SecconvertToMin = Convert.ToInt64(v2FixedTransSec);
                                        lesRej1.Bs1BLesTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                        lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                        lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_VoiceFixed), 2, MidpointRounding.AwayFromZero);
                                        lesRej1.fkCommType = 2;

                                        db.Bs1BLesT.Add(lesRej1);
                                    }

                                    // Invoice List Report Add in BInvLstReports
                                    if (lstV2Fixed_Trans.Count > 0)
                                    {
                                        InvreportCost = new BInvLstReport();
                                        try
                                        {
                                            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeVce2Fixed).BsPtServiceTypes;
                                        }
                                        catch { InvreportCost.BInvRprtSerType = null; }

                                        if (InvreportCost.BInvRprtSerType != null)
                                        {
                                            InvreportCost.BInvRprtLes1ID = SecRejID;
                                            InvreportCost.fkBCus2ID = null;
                                            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_VceFixedCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                            InvreportCost.BInvRprtTotBil = null;
                                            db.BInvLstReports.Add(InvreportCost);
                                        }
                                    }
                                    #endregion

                                    #region Reject V2FBB Trans

                                    foreach (var records in lstV2Fixed_Trans)
                                    {
                                        lesRej1 = new Bs1BLesT();
                                        lesRej1.fkBs1BLesID = SecRejID;
                                        lesRej1.Bs1BLesStartDate = records.BCtTranStartDate;
                                        lesRej1.Bs1BLesStarTime = records.BCtTranStartTime;
                                        lesRej1.Bs1BLesOrigion = records.BCtTranOrigin;
                                        lesRej1.Bs1BLesDestination = records.BCtTranServDest;
                                        lesRej1.fkSPServTypeID = ServTypeVce2FBB;

                                        decimal? v2FBBTransSec = 0;
                                        if (records.BCtTranUnitUsage != null)
                                        {
                                            v2FBBTransSec = getMinutes(records.BCtTranUnitUsage);
                                        }

                                        long SecconvertToMin = Convert.ToInt64(v2FBBTransSec);
                                        lesRej1.Bs1BLesTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                        lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                        lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_VoiceFBB), 2, MidpointRounding.AwayFromZero);
                                        lesRej1.fkCommType = 2;

                                        db.Bs1BLesT.Add(lesRej1);
                                    }


                                    // Invoice List Report Add in BInvLstReports
                                    if (lstV2Fixed_Trans.Count > 0)
                                    {
                                        InvreportCost = new BInvLstReport();
                                        try
                                        {
                                            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeVce2FBB).BsPtServiceTypes;
                                        }
                                        catch { InvreportCost.BInvRprtSerType = null; }

                                        if (InvreportCost.BInvRprtSerType != null)
                                        {
                                            InvreportCost.BInvRprtLes1ID = SecRejID;
                                            InvreportCost.fkBCus2ID = null;
                                            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_VceFBBCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                            InvreportCost.BInvRprtTotBil = null;
                                            db.BInvLstReports.Add(InvreportCost);
                                        }
                                    }
                                    #endregion

                                    #region Reject V2Iridium

                                    foreach (var records in lstV2Iridium_Trans)
                                    {
                                        lesRej1 = new Bs1BLesT();
                                        lesRej1.fkBs1BLesID = SecRejID;
                                        lesRej1.Bs1BLesStartDate = records.BCtTranStartDate;
                                        lesRej1.Bs1BLesStarTime = records.BCtTranStartTime;
                                        lesRej1.Bs1BLesOrigion = records.BCtTranOrigin;
                                        lesRej1.Bs1BLesDestination = records.BCtTranServDest;
                                        lesRej1.fkSPServTypeID = LES_ServTypeVce2Iridium;

                                        decimal? v2IriTransSec = 0;
                                        if (records.BCtTranUnitUsage != null)
                                        {
                                            v2IriTransSec = getMinutes(records.BCtTranUnitUsage);
                                        }

                                        long SecconvertToMin = Convert.ToInt64(v2IriTransSec);

                                        lesRej1.Bs1BLesTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                        lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                        lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_VoiceIridium), 2, MidpointRounding.AwayFromZero);
                                        lesRej1.fkCommType = 2;

                                        db.Bs1BLesT.Add(lesRej1);
                                    }

                                    // Invoice List Report Add in BInvLstReports
                                    if (lstV2Iridium_Trans.Count > 0)
                                    {
                                        InvreportCost = new BInvLstReport();
                                        try
                                        {
                                            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == LES_ServTypeVce2Iridium).BsPtServiceTypes;
                                        }
                                        catch { InvreportCost.BInvRprtSerType = null; }

                                        if (InvreportCost.BInvRprtSerType != null)
                                        {
                                            InvreportCost.BInvRprtLes1ID = SecRejID;
                                            InvreportCost.fkBCus2ID = null;
                                            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_VceIridiumCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                            InvreportCost.BInvRprtTotBil = null;
                                            db.BInvLstReports.Add(InvreportCost);
                                        }
                                    }
                                    #endregion

                                    #region RejectionConditionISDN Trans

                                    foreach (var records in lstISDN_Trans)
                                    {
                                        lesRej1 = new Bs1BLesT();
                                        lesRej1.fkBs1BLesID = SecRejID;
                                        lesRej1.Bs1BLesStartDate = records.BCtTranStartDate;
                                        lesRej1.Bs1BLesStarTime = records.BCtTranStartTime;
                                        lesRej1.Bs1BLesOrigion = records.BCtTranOrigin;
                                        lesRej1.Bs1BLesDestination = records.BCtTranServDest;
                                        lesRej1.fkSPServTypeID = ServTypeVce2ISDN;

                                        decimal? v2ISDNTransSec = 0;
                                        if (records.BCtTranUnitUsage != null)
                                        {
                                            v2ISDNTransSec = getMinutes(records.BCtTranUnitUsage);
                                        }

                                        long SecconvertToMin = Convert.ToInt64(v2ISDNTransSec);

                                        lesRej1.Bs1BLesTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                        lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                        lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_ISDN), 2, MidpointRounding.AwayFromZero);
                                        lesRej1.fkCommType = 2;

                                        db.Bs1BLesT.Add(lesRej1);
                                    }

                                    // Invoice List Report Add in BInvLstReports
                                    if (lstISDN_Trans.Count > 0)
                                    {
                                        InvreportCost = new BInvLstReport();
                                        try
                                        {
                                            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeVce2ISDN).BsPtServiceTypes;
                                        }
                                        catch { InvreportCost.BInvRprtSerType = null; }

                                        if (InvreportCost.BInvRprtSerType != null)
                                        {
                                            InvreportCost.BInvRprtLes1ID = SecRejID;
                                            InvreportCost.fkBCus2ID = null;
                                            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_ISDNCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                            InvreportCost.BInvRprtTotBil = null;
                                            db.BInvLstReports.Add(InvreportCost);
                                        }
                                    }
                                    #endregion

                                    #region Reject SMS Trans

                                    int commtype = 1;
                                    try
                                    {
                                        commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                                    }
                                    catch
                                    {
                                        commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                                    }

                                    foreach (var records in lstSMS_Trans)
                                    {
                                        lesRej1 = new Bs1BLesT();
                                        lesRej1.fkBs1BLesID = SecRejID;
                                        lesRej1.Bs1BLesStartDate = records.BCtTranStartDate;
                                        lesRej1.Bs1BLesStarTime = records.BCtTranStartTime;
                                        lesRej1.Bs1BLesOrigion = records.BCtTranOrigin;
                                        lesRej1.Bs1BLesDestination = records.BCtTranServDest;
                                        lesRej1.fkSPServTypeID = ServTypeSMS;

                                        //long SecconvertToMin = Convert.ToInt64(records.BCtBTransUsage);
                                        lesRej1.Bs1BLesTransUsage = Convert.ToInt32(records.BCtTranUnitUsage);//(decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                        try
                                        {
                                            lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("message")).UID;
                                        }
                                        catch
                                        {
                                            lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Message")).UID;
                                        }

                                        lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_SMS), 2, MidpointRounding.AwayFromZero);
                                        lesRej1.fkCommType = commtype;

                                        db.Bs1BLesT.Add(lesRej1);
                                    }

                                    // Invoice List Report Add in BInvLstReports
                                    if (lstSMS_Trans.Count > 0)
                                    {
                                        InvreportCost = new BInvLstReport();
                                        try
                                        {
                                            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeSMS).BsPtServiceTypes;
                                        }
                                        catch { InvreportCost.BInvRprtSerType = null; }

                                        if (InvreportCost.BInvRprtSerType != null)
                                        {
                                            InvreportCost.BInvRprtLes1ID = SecRejID;
                                            InvreportCost.fkBCus2ID = null;
                                            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_SMSCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                            InvreportCost.BInvRprtTotBil = null;
                                            db.BInvLstReports.Add(InvreportCost);
                                        }
                                    }

                                    #endregion

                                    db.SaveChanges();
                                    returnMsg.AppendFormat("This msi {0}: Successfully stored Rejection Les price table.<br />", ImarSatID).AppendLine();
                                }
                            }
                            catch (Exception error)
                            {
                                Msg.AppendFormat("Error:ID:{0}, {1}", ImarSatID, error.Message.ToString());
                            }
                        }

                    }
                }
                catch (Exception error)
                {
                    throw error;
                    //Msg.AppendFormat("Error:ID:{0}, {1}", ImarSatID, error.Message.ToString());
                }
            }



            return returnMsg.ToString();
        }

        public string RejectionOtesat_Speed(string bctles, string errorEsg)
        {
            System.Text.StringBuilder returnMsg = new System.Text.StringBuilder();
            returnMsg.Append(errorEsg).AppendLine();

            System.Text.StringBuilder Msg = new System.Text.StringBuilder();

            // Create new stopwatch.
            Stopwatch stopwatch = new Stopwatch();
            // Begin timing.
            stopwatch.Start();

            List<BCtFbbTran> lstCtRecord = new List<BCtFbbTran>();
            List<BsPtSPServiceDest> lstLES_ServiceType = new List<BsPtSPServiceDest>();

            List<BSt1FbbTrans> lstRejecttbl = new List<BSt1FbbTrans>();
            List<BSt2FbbTrans> lstInvoicetbl = new List<BSt2FbbTrans>();

            using (BillingSystemEntities db = new BillingSystemEntities())
            {
                try
                {
                    lstCtRecord = db.BCtFbbTrans.Where(t => t.BCtFbbLES.Equals(bctles)).ToList();

                    var lstSubscripRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("Subscription Fee")).ToList();
                    var lstStdIPRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("Standard IP")).ToList();
                    var lstV2CellRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("Voice  to Cellular")).ToList();
                    var lstV2FixedRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("Voice to Fixed")).ToList();
                    var lstV2FBBRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("Voice  to FleetBroadband")).ToList();
                    var lstV2IdridumRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("Voice  to Iridium")).ToList();
                    var lstISDNRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("ISDN")).ToList();
                    var lstSMSRecords = lstCtRecord.Where(t => t.BCtTranServType.Equals("SMS")).ToList();

                    var lstCusRegs = db.BsPtCusRegs_.ToList();
                    var lstLesRegs = db.BsPtSPLesRegs.ToList();
                    var LES_Services = db.BsPtSPServiceDests.ToList();
                    var lstUnityType = db.BsMtUnits.ToList();

                    if (lstSubscripRecords.Count > 0)
                    {
                        foreach (var CT_Fbs in lstSubscripRecords)
                        {
                            string ImarSatID = string.Empty;
                            try
                            {
                                #region Varaiable declarations

                                string ImsiID = string.Empty;
                                int SimID = 0;
                                decimal? CDR_RecuFee = 0;

                                decimal? SMTS_TotalCost = 0;
                                decimal? Les_TotalCostCP = 0;
                                decimal? Cus_TotalCostSP = 0;

                                int? LES_ContractID = 0;
                                int LES_RegID = 0;

                                decimal? LES_DataPlan = 0;
                                decimal? LES_OutbundleRate = 0;
                                decimal? LES_RecurFee = 0;
                                decimal? LES_DataRateper1MB = 0;
                                decimal? LES_VoiceCellular = 0;
                                decimal? LES_VoiceFixed = 0;
                                decimal? LES_VoiceFBB = 0;
                                decimal? LES_VoiceIridium = 0;
                                decimal? LES_ISDN = 0;
                                decimal? LES_SMS = 0;

                                decimal? LES_InvRptDataCostTot = 0;

                                int CUS_Id = 0;
                                int CUS_ContractID = 0;
                                int CUS_RegsID = 0;

                                decimal? Cus_DataPlan = 0;
                                decimal? Cus_OutbundleRate = 0;
                                decimal? CUS_RecurFee = 0;
                                decimal? CUS_DataRateper1MB = 0;
                                decimal? CUS_VoiceCellular = 0;
                                decimal? CUS_VoiceFixed = 0;
                                decimal? CUS_VoiceFBB = 0;
                                decimal? CUS_VoiceIridium = 0;
                                decimal? CUS_ISDN = 0;
                                decimal? CUS_SMS = 0;

                                DateTime? LES_ContrRegStrDate = null;
                                DateTime? LES_ContrRegEndDate = null;

                                DateTime? LES_ContrBarStrDate = null;
                                DateTime? LES_ContrBarEndDate = null;

                                DateTime? LES_ContrSusStrDate = null;
                                DateTime? LES_ContrSusEndDate = null;

                                DateTime? LES_ContrLayoutStrDate = null;
                                DateTime? LES_ContrLayoutEndDate = null;

                                DateTime? CUS_ContrRegStrDate = null;
                                DateTime? CUS_ContrRegEndDate = null;

                                DateTime? CUS_ContrBarStrDate = null;
                                DateTime? CUS_ContrBarEndDate = null;

                                DateTime? CUS_ContrSusStrDate = null;
                                DateTime? CUS_ContrSusEndDate = null;

                                DateTime? CUS_ContrLayoutStrDate = null;
                                DateTime? CUS_ContrLayoutEndDate = null;

                                DateTime? CDR_StartDate = null;
                                DateTime? CDR_EndDate = null;

                                bool is_LESGBPlan = false;
                                bool is_CUSGBPlan = false;
                                bool isNoTransData = false;

                                int ServTypeID = 0;
                                int ServTypeMobile = 0;
                                int ServTypeVce2Fixed = 0;
                                int ServTypeVce2FBB = 0;
                                int LES_ServTypeVce2Iridium = 0;
                                int ServTypeVce2ISDN = 0;
                                int ServTypeSMS = 0;

                                int CUS_ServTypeID = 0;
                                int CUS_ServTypeMobile = 0;
                                int CUS_ServTypeVce2Fixed = 0;
                                int CUS_ServTypeVce2FBB = 0;
                                int CUS_ServTypeVce2Iridium = 0;
                                int CUS_ServTypeVce2ISDN = 0;
                                int CUS_ServTypeSMS = 0;

                                bool isLES_ContrRegDate = false;
                                bool isLES_ContrBarDate = false;
                                bool isLES_ContrSusDate = false;
                                bool isLES_ContrLayUpDate = false;

                                bool isCUS_ContrRegDate = false;
                                bool isCUS_ContrBarDate = false;
                                bool isCUS_ContrSusDate = false;
                                bool isCUS_ContrLayUpDate = false;

                                #endregion

                                var tblSim = db.BptFbSims.FirstOrDefault(t => t.BPtFBImImsi.Equals(CT_Fbs.BCtFBImImsi));

                                var lstNotSubscripRecords = lstCtRecord.Where(t => t.BCtFBImImsi.Equals(CT_Fbs.BCtFBImImsi)).ToList();

                                if (tblSim != null)
                                {
                                    ImarSatID = tblSim.BPtFBimMsisdn;
                                    SimID = tblSim.Sim_ID;
                                }
                                else
                                {
                                    Msg.AppendFormat("This IMSI id {0} is not registered in our database, please check it.<br />", CT_Fbs.BCtFBImImsi).AppendLine();
                                    returnMsg.AppendFormat("This IMSI id {0} is not registered in our database, please check it.<br />", CT_Fbs.BCtFBImImsi).AppendLine();
                                }

                                CDR_RecuFee = CT_Fbs.BCtFbRecurFee;

                                if (lstNotSubscripRecords.Count > 0)
                                {
                                    CDR_StartDate = lstNotSubscripRecords.Min(t => t.BCtTranStartDate);
                                    CDR_EndDate = lstNotSubscripRecords.Max(t => t.BCtTranStartDate);
                                }
                                else
                                    isNoTransData = true;

                                var lstStdIP_Trans = lstStdIPRecords.Where(t => t.BCtFBImImsi.Equals(CT_Fbs.BCtFBImImsi)).ToList();
                                var lstV2Cell_Trans = lstV2CellRecords.Where(t => t.BCtFBImImsi.Equals(CT_Fbs.BCtFBImImsi)).ToList();
                                var lstV2Fixed_Trans = lstV2FixedRecords.Where(t => t.BCtFBImImsi.Equals(CT_Fbs.BCtFBImImsi)).ToList();
                                var lstV2Fbb_Trans = lstV2FBBRecords.Where(t => t.BCtFBImImsi.Equals(CT_Fbs.BCtFBImImsi)).ToList();
                                var lstV2Iridium_Trans = lstV2IdridumRecords.Where(t => t.BCtFBImImsi.Equals(CT_Fbs.BCtFBImImsi)).ToList();
                                var lstISDN_Trans = lstISDNRecords.Where(t => t.BCtFBImImsi.Equals(CT_Fbs.BCtFBImImsi)).ToList();
                                var lstSMS_Trans = lstSMSRecords.Where(t => t.BCtFBImImsi.Equals(CT_Fbs.BCtFBImImsi)).ToList();

                                #region get Cus Register tbl info

                                var cusRegs = lstCusRegs.FirstOrDefault(t => t.fkSimID == SimID);
                                if (cusRegs != null)
                                {
                                    CUS_Id = Convert.ToInt32(cusRegs.fkCusID);
                                    CUS_ContractID = Convert.ToInt32(cusRegs.fkCuscID);
                                    CUS_RegsID = Convert.ToInt32(cusRegs.Cus_RegID);

                                    CUS_RecurFee = cusRegs.BPtCusRgnFbRecurFee;

                                    Cus_DataPlan = Convert.ToDecimal(cusRegs.BPtCusRgnFBDataPlanIn);

                                    CUS_DataRateper1MB = cusRegs.BPtCusRgnFbInbundleDataRate;
                                    Cus_OutbundleRate = cusRegs.BPtCusRgnFbOutBundleDataRate;

                                    CUS_ContrRegStrDate = cusRegs.BsPtCusRegStartdate;
                                    CUS_ContrRegEndDate = cusRegs.BsPtCusRegEnd;

                                    CUS_ContrBarStrDate = cusRegs.BsPtCusRegFBBarDate;
                                    CUS_ContrBarEndDate = cusRegs.BsPtCusRegFbBarDateLifted;

                                    CUS_ContrSusStrDate = cusRegs.BsPtCusRegFBSusDate;
                                    CUS_ContrSusEndDate = cusRegs.BsPtCusRegFBSusDateLifted;

                                    CUS_ContrLayoutStrDate = cusRegs.BsPtCusRegFBLayUpDate;
                                    CUS_ContrLayoutEndDate = cusRegs.BsPtCusRegFBLayUpDateLifted;

                                    string unit = cusRegs.BsMtUnit.Units;
                                    if (unit.Contains("GB"))
                                        is_CUSGBPlan = true;
                                    else
                                        is_CUSGBPlan = false;
                                }
                                else
                                {
                                    Msg.AppendFormat("This msi {0} id not registered in customer registered, please check.<br />", ImarSatID).AppendLine();
                                }

                                List<BsPtCusServiceDest> lstSP = db.BsPtCusServiceDests.Where(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID).ToList();

                                try
                                {
                                    CUS_VoiceCellular = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                             t.BsPtCusServiceTypes.Equals("Voice  to Cellular")).BsPtCusServiceSellerPrice;
                                }
                                catch { CUS_VoiceCellular = 0; }

                                try
                                {
                                    CUS_VoiceFixed = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                          t.BsPtCusServiceTypes.Equals("Voice  to Fixed")).BsPtCusServiceSellerPrice;
                                }
                                catch { CUS_VoiceFixed = 0; }

                                try
                                {
                                    CUS_VoiceFBB = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                        t.BsPtCusServiceTypes.Equals("Voice  to FleetBroadband")).BsPtCusServiceSellerPrice;
                                }
                                catch { CUS_VoiceFBB = 0; }

                                try
                                {
                                    CUS_VoiceIridium = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                            t.BsPtCusServiceTypes.Equals("Voice  to Iridium")).BsPtCusServiceSellerPrice;
                                }
                                catch { CUS_VoiceIridium = 0; }

                                try
                                {
                                    CUS_ISDN = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                    t.BsPtCusServiceTypes.Equals("ISDN")).BsPtCusServiceSellerPrice;
                                }
                                catch { CUS_ISDN = 0; }

                                try
                                {
                                    CUS_SMS = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                   t.BsPtCusServiceTypes.Equals("SMS")).BsPtCusServiceSellerPrice;
                                }
                                catch { CUS_SMS = 0; }
                                #endregion

                                #region GET LES Details
                                try
                                {
                                    try
                                    {
                                        var Les_Reg = lstLesRegs.FirstOrDefault(t => t.fkSPID == LesID && t.fkSimID == SimID);
                                        //var Les_Reg = db.BsPtSPLesRegs.FirstOrDefault(t => t.SPLesRegID == CT_Fbs.BCtAfkLesRegID);

                                        LES_RegID = Les_Reg.SPLesRegID;
                                        LES_ContractID = Les_Reg.fkspcID;

                                        LES_RecurFee = Les_Reg.BPtLesRgnFbRecurFee;
                                        LES_DataPlan = Convert.ToDecimal(Les_Reg.BPtLesRgnFBDataPlanIn);
                                        LES_DataRateper1MB = Les_Reg.BPtLesRgnFbInbundleDataRate;
                                        LES_OutbundleRate = Les_Reg.BPtLesRgnFbOutBundleDataRate;

                                        LES_ContrRegStrDate = Les_Reg.BsPtlesRegStartdate;
                                        LES_ContrRegEndDate = Les_Reg.BsPtlesRegEnd;

                                        LES_ContrBarStrDate = Les_Reg.BsPtLesRegFBBarDate;
                                        LES_ContrBarEndDate = Les_Reg.BsPtLesRegFbBarDateLifted;

                                        LES_ContrSusStrDate = Les_Reg.BsPtLesRegFBSusDate;
                                        LES_ContrSusEndDate = Les_Reg.BsPtLesRegFBSusDateLifted;

                                        LES_ContrLayoutStrDate = Les_Reg.BsPtLesRegFBLayUpDate;
                                        LES_ContrLayoutEndDate = Les_Reg.BsPtLesRegFBLayUpDateLifted;

                                        string unit = Les_Reg.BsMtUnit.Units;
                                        if (unit.Contains("GB"))
                                            is_LESGBPlan = true;
                                        else
                                            is_LESGBPlan = false;
                                    }
                                    catch { LES_RecurFee = 0; }

                                    lstLES_ServiceType = (from ls in LES_Services
                                                          where ls.fkSpID == LesID && ls.BPtServTfkLesRegID == LES_RegID
                                                          select ls).ToList();

                                    try
                                    {
                                        LES_VoiceCellular = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                 t.BsPtServiceTypes.Equals("Voice  to Cellular")).BsPtServiceCostPrice;
                                    }
                                    catch { LES_VoiceCellular = 0; }

                                    try
                                    {
                                        LES_VoiceFixed = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                              t.BsPtServiceTypes.Equals("Voice  to Fixed")).BsPtServiceCostPrice;
                                    }
                                    catch { LES_VoiceFixed = 0; }

                                    try
                                    {
                                        LES_VoiceFBB = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                            t.BsPtServiceTypes.Equals("Voice  to FleetBroadband")).BsPtServiceCostPrice;
                                    }
                                    catch { LES_VoiceFBB = 0; }

                                    try
                                    {
                                        LES_VoiceIridium = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                t.BsPtServiceTypes.Equals("Voice  to Iridium")).BsPtServiceCostPrice;
                                    }
                                    catch { LES_VoiceIridium = 0; }

                                    try
                                    {
                                        LES_ISDN = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                        t.BsPtServiceTypes.Equals("ISDN")).BsPtServiceCostPrice;
                                    }
                                    catch { LES_ISDN = 0; }

                                    try
                                    {
                                        LES_SMS = lstLES_ServiceType.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                       t.BsPtServiceTypes.Equals("SMS")).BsPtServiceCostPrice;
                                    }
                                    catch { LES_SMS = 0; }
                                }
                                catch (Exception e)
                                {
                                    Msg.AppendFormat("Error:{0}", e.Message.ToString());
                                }
                                //-------------------------------------------------------------------------------------------------------------------------------
                                #endregion

                                #region Standard IP

                                try
                                {
                                    ServTypeID = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                        t.BsPtServiceTypes.Equals("Standard IP")).SPServDID;
                                }
                                catch
                                {
                                    ServTypeID = 0;
                                    Msg.Append("The Standard IP is Registed in the LES Service Description<br />.").AppendLine();
                                }

                                try
                                {
                                    CUS_ServTypeID = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                            t.BsPtCusServiceTypes.Equals("Standard IP")).CusServDID;
                                }
                                catch
                                {
                                    CUS_ServTypeID = 0;
                                    Msg.Append("The Standard IP is Registed in the CUS Service Description<br />.").AppendLine();
                                }

                                decimal? DataUnits_CDR = null;
                                decimal? CDR_DataCP = null;
                                if (lstStdIP_Trans.Count > 0)
                                {
                                    DataUnits_CDR = lstStdIP_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    DataUnits_CDR = Math.Round(Convert.ToDecimal(DataUnits_CDR), 2, MidpointRounding.AwayFromZero);

                                    CDR_DataCP = lstCtRecord.AsEnumerable().Sum(s => s.BCtTranTotalUsage);

                                    CDR_DataCP = Math.Round(Convert.ToDecimal(CDR_DataCP), 2, MidpointRounding.AwayFromZero);
                                }

                                SMTS_TotalCost += CDR_DataCP;

                                #endregion

                                #region Basic voice - Cellular

                                // <--------------------------- Basic voice - Mobile  ----------------------------->
                                try
                                {
                                    ServTypeMobile = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                            t.BsPtServiceTypes.Equals("Voice  to Cellular")).SPServDID;
                                }
                                catch { }

                                try
                                {
                                    CUS_ServTypeMobile = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                t.BsPtCusServiceTypes.Equals("Voice  to Cellular")).CusServDID;
                                }
                                catch { }

                                decimal? CDR_VceCellularUintsCT = null;
                                decimal? CDR_VceCellularCostPriceCT = null;

                                if (lstV2Cell_Trans.Count > 0)
                                {
                                    CDR_VceCellularUintsCT = lstV2Cell_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    if (CDR_VceCellularUintsCT != null)
                                        CDR_VceCellularUintsCT = Math.Round(Convert.ToDecimal(CDR_VceCellularUintsCT), 2, MidpointRounding.AwayFromZero);

                                    CDR_VceCellularCostPriceCT = lstV2Cell_Trans.AsEnumerable().Sum(s => s.BCtTranTotalUsage);

                                    if (CDR_VceCellularCostPriceCT != null)
                                        CDR_VceCellularCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceCellularCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                }

                                SMTS_TotalCost += CDR_VceCellularCostPriceCT;
                                // <--------------------------- End Basic voice -FBB/Cellular  ----------------------------->
                                #endregion

                                #region Basic Voice  to Fixed

                                // <--------------------------- Basic Voice  to Fixed  ----------------------------->
                                try
                                {
                                    ServTypeVce2Fixed = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID
                                                                                                && t.BsPtServiceTypes.Equals("Voice  to Fixed")).SPServDID;
                                }
                                catch { }

                                try
                                {
                                    CUS_ServTypeVce2Fixed = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                    t.BsPtCusServiceTypes.Equals("Voice  to Fixed")).CusServDID;
                                }
                                catch { }

                                decimal? CDR_VceFixedUintsCT = null;
                                decimal? CDR_VceFixedCostPriceCT = null;

                                if (lstV2Fixed_Trans.Count > 0)
                                {
                                    CDR_VceFixedUintsCT = lstV2Fixed_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    if (CDR_VceFixedUintsCT != null)
                                        CDR_VceFixedUintsCT = Math.Round(Convert.ToDecimal(CDR_VceFixedUintsCT), 2, MidpointRounding.AwayFromZero);

                                    CDR_VceFixedCostPriceCT = lstV2Fixed_Trans.AsEnumerable().Sum(s => s.BCtTranTotalUsage);

                                    if (CDR_VceFixedCostPriceCT != null)
                                        CDR_VceFixedCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceFixedCostPriceCT), 2, MidpointRounding.AwayFromZero);

                                    SMTS_TotalCost += CDR_VceFixedCostPriceCT;
                                }
                                // <--------------------------- End Basic Voice  to Fixed  ----------------------------->

                                #endregion

                                #region Voice to FleetBroadband
                                // <--------------------------- Voice to FleetBroadband  ----------------------------->

                                try
                                {
                                    ServTypeVce2FBB = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                            t.BsPtServiceTypes.Equals("Voice to FleetBroadband")).SPServDID;
                                }
                                catch
                                {
                                    ServTypeVce2FBB = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                                t.BsPtServiceTypes.Equals("Voice  to FleetBroadband")).SPServDID;
                                }

                                try
                                {
                                    CUS_ServTypeVce2FBB = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                t.BsPtCusServiceTypes.Equals("Voice to FleetBroadband")).CusServDID;
                                }
                                catch
                                {
                                    CUS_ServTypeVce2FBB = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                t.BsPtCusServiceTypes.Equals("Voice  to FleetBroadband")).CusServDID;
                                }

                                decimal? CDR_VceFBBUintsCT = null;
                                decimal? CDR_VceFBBCostPriceCT = null;

                                if (lstV2Fbb_Trans.Count > 0)
                                {
                                    CDR_VceFBBUintsCT = lstV2Fbb_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    if (CDR_VceFBBUintsCT != null)
                                        CDR_VceFBBUintsCT = Math.Round(Convert.ToDecimal(CDR_VceFBBUintsCT), 2, MidpointRounding.AwayFromZero);

                                    CDR_VceFBBCostPriceCT = lstV2Fbb_Trans.AsEnumerable().Sum(s => s.BCtTranTotalUsage);

                                    if (CDR_VceFBBCostPriceCT != null)
                                        CDR_VceFBBCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceFBBCostPriceCT), 2, MidpointRounding.AwayFromZero);

                                    SMTS_TotalCost += CDR_VceFBBCostPriceCT;
                                }
                                // <--------------------------- End Voice to FleetBroadband  ----------------------------->
                                #endregion

                                #region Voice  to Iridium
                                // <--------------------------- Voice  to Iridium  ----------------------------->
                                try
                                {
                                    LES_ServTypeVce2Iridium = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                                   t.BsPtServiceTypes.Equals("Voice  to Iridium")).SPServDID;
                                }
                                catch { }
                                try
                                {
                                    CUS_ServTypeVce2Iridium = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                    t.BsPtCusServiceTypes.Equals("Voice  to Iridium")).CusServDID;
                                }
                                catch { }

                                decimal? CDR_VceIridiumUintsCT = null;
                                decimal? CDR_VceIridiumCostPriceCT = null;

                                if (lstV2Iridium_Trans.Count > 0)
                                {
                                    CDR_VceIridiumUintsCT = lstV2Iridium_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    if (CDR_VceIridiumUintsCT != null)
                                        CDR_VceIridiumUintsCT = Math.Round(Convert.ToDecimal(CDR_VceIridiumUintsCT), 2, MidpointRounding.AwayFromZero);

                                    CDR_VceIridiumCostPriceCT = lstV2Iridium_Trans.AsEnumerable().Sum(s => s.BCtTranTotalUsage);

                                    if (CDR_VceIridiumCostPriceCT != null)
                                        CDR_VceIridiumCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceIridiumCostPriceCT), 2, MidpointRounding.AwayFromZero);

                                    SMTS_TotalCost += CDR_VceIridiumCostPriceCT;
                                }
                                // <--------------------------- End Voice  to Iridium  ----------------------------->
                                #endregion

                                #region ISDN
                                // <--------------------------- ISDN  ----------------------------->
                                try
                                {
                                    ServTypeVce2ISDN = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID &&
                                                                                                    t.BsPtServiceTypes.Equals("ISDN")).SPServDID;
                                }
                                catch { }
                                try
                                {
                                    CUS_ServTypeVce2ISDN = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID &&
                                                                                                    t.BsPtCusServiceTypes.Equals("ISDN")).CusServDID;
                                }
                                catch { }

                                decimal? CDR_ISDNUintsCT = null;
                                decimal? CDR_ISDNCostPriceCT = null;
                                if (lstISDN_Trans.Count > 0)
                                {
                                    CDR_ISDNUintsCT = lstISDN_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    if (CDR_ISDNUintsCT != null)
                                        CDR_ISDNUintsCT = Math.Round(Convert.ToDecimal(CDR_ISDNUintsCT), 2, MidpointRounding.AwayFromZero);

                                    CDR_ISDNCostPriceCT = lstISDN_Trans.AsEnumerable().Sum(s => s.BCtTranTotalUsage);

                                    if (CDR_ISDNCostPriceCT != null)
                                        CDR_ISDNCostPriceCT = Math.Round(Convert.ToDecimal(CDR_ISDNCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                }
                                SMTS_TotalCost += CDR_ISDNCostPriceCT;
                                // <--------------------------- End ISDN  ----------------------------->
                                #endregion

                                #region SMS
                                // <--------------------------- ISDN  ----------------------------->
                                try
                                {
                                    ServTypeSMS = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtServTfkLesRegID == LES_RegID && t.BsPtServiceTypes.Equals("SMS")).SPServDID;
                                }
                                catch { }
                                try
                                {
                                    CUS_ServTypeSMS = db.BsPtCusServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BPtCusServfkCusRegID == CUS_RegsID && t.BsPtCusServiceTypes.Equals("SMS")).CusServDID;
                                }
                                catch { }

                                decimal? CDR_SMSUintsCT = null;
                                decimal? CDR_SMSCostPriceCT = null;
                                if (lstSMS_Trans.Count > 0)
                                {
                                    CDR_SMSUintsCT = lstSMS_Trans.AsEnumerable().Sum(s => s.BCtTranUnitUsage);

                                    if (CDR_SMSUintsCT != null)
                                        CDR_SMSUintsCT = Math.Round(Convert.ToDecimal(CDR_SMSUintsCT), 2, MidpointRounding.AwayFromZero);

                                    CDR_SMSCostPriceCT = lstSMS_Trans.AsEnumerable().Sum(s => s.BCtTranTotalUsage);


                                    if (CDR_SMSCostPriceCT != null)
                                        CDR_SMSCostPriceCT = Math.Round(Convert.ToDecimal(CDR_SMSCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                }

                                SMTS_TotalCost += CDR_SMSCostPriceCT;
                                // <--------------------------- End ISDN  ----------------------------->
                                #endregion

                                bool isRecurFeeMached = false;
                                bool isDataMatched = false;
                                bool isVoiceCellurlarMatched = false;
                                bool isVoiceFixedMatched = false;
                                bool isVoiceFbbMatched = false;
                                bool isVoiceIridiumMatched = false;
                                bool isISDNMatched = false;
                                bool isSMSMatched = false;

                                bool isOutbundleUsed = false;
                                decimal? OutbundleUsedData = 0;
                                decimal? CDR_OutbundleRate = 0;

                                Les_TotalCostCP += LES_RecurFee;
                                SMTS_TotalCost += CDR_RecuFee;

                                if ((CDR_RecuFee < LES_RecurFee || CDR_RecuFee.Equals(LES_RecurFee))
                                                      && CDR_RecuFee < CUS_RecurFee)
                                {
                                    isRecurFeeMached = true;
                                }
                                else
                                {
                                    isRecurFeeMached = false;
                                    Msg.AppendFormat("This Inmarsat id {0}: Recuring Feee is Less then LES/CUS Recur Fee.<br />", ImarSatID).AppendLine();
                                    Msg.AppendFormat("Recuring Feee from LES CDR Price:{0}.<br />", CDR_RecuFee).AppendLine();
                                    Msg.AppendFormat("Recuring Feee from LES Reg CostPrice:{0}.<br />", LES_RecurFee).AppendLine();
                                    Msg.AppendFormat("Recuring Feee from Cus Reg SellerPrice:{0}.<br />", CUS_RecurFee).AppendLine();
                                    returnMsg.AppendFormat("This Inmarsat id {0}: Recuring Feee is Less then LES/CUS Recur Fee.<br />", ImarSatID).AppendLine();
                                }

                                #region Standard IP rate calculation
                                if (DataUnits_CDR != null)
                                {
                                    long data = Convert.ToInt64(Convert.ToDecimal(DataUnits_CDR));
                                    decimal? CDR_DataTotalUsageMB = (decimal)Converters.ConvertKilobytesToMegabytes(data);

                                    if (CDR_DataTotalUsageMB != null)
                                        CDR_DataTotalUsageMB = Math.Round(Convert.ToDecimal(CDR_DataTotalUsageMB), 2, MidpointRounding.AwayFromZero);

                                    if (is_LESGBPlan)
                                    {
                                        LES_DataPlan = LES_DataPlan * 1024;
                                    }

                                    if (LES_DataPlan < CDR_DataTotalUsageMB)
                                    {
                                        isOutbundleUsed = true;
                                        OutbundleUsedData = CDR_DataTotalUsageMB - LES_DataPlan;
                                        OutbundleUsedData = Math.Round(Convert.ToDecimal(OutbundleUsedData), 2, MidpointRounding.AwayFromZero);
                                    }

                                    decimal? LES_DataCP = CDR_DataTotalUsageMB * LES_DataRateper1MB;
                                    decimal? Cus_DataSP = CDR_DataTotalUsageMB * CUS_DataRateper1MB;
                                    decimal? LES_OutbundleDataCP = OutbundleUsedData * LES_OutbundleRate;
                                    decimal? Cus_OutbundleDataSP = OutbundleUsedData * Cus_OutbundleRate;

                                    if (Cus_DataSP != null)
                                        Cus_DataSP = Math.Round(Convert.ToDecimal(Cus_DataSP), 2, MidpointRounding.AwayFromZero);

                                    if (LES_DataCP != null)
                                        LES_DataCP = Math.Round(Convert.ToDecimal(LES_DataCP), 2, MidpointRounding.AwayFromZero);

                                    if (LES_OutbundleDataCP != null)
                                        LES_OutbundleDataCP = Math.Round(Convert.ToDecimal(LES_OutbundleDataCP), 2, MidpointRounding.AwayFromZero);

                                    if (Cus_OutbundleDataSP != null)
                                        Cus_OutbundleDataSP = Math.Round(Convert.ToDecimal(Cus_OutbundleDataSP), 2, MidpointRounding.AwayFromZero);

                                    if (isOutbundleUsed)
                                    {
                                        CDR_OutbundleRate = CDR_DataCP - CDR_RecuFee;
                                        CDR_OutbundleRate = Math.Round(Convert.ToDecimal(CDR_OutbundleRate), 2, MidpointRounding.AwayFromZero);

                                        //if ((CDR_DataCP < LES_OutbundleDataCP || CDR_DataCP.Equals(LES_OutbundleDataCP)) && (CDR_DataCP < Cus_OutbundleDataSP))
                                        if ((CDR_OutbundleRate < LES_OutbundleDataCP || CDR_OutbundleRate.Equals(LES_OutbundleDataCP)) && (CDR_OutbundleRate < Cus_OutbundleDataSP))
                                        {
                                            isDataMatched = true;
                                            Les_TotalCostCP += LES_OutbundleDataCP;
                                            Cus_TotalCostSP += Cus_OutbundleDataSP;
                                        }
                                        else
                                        {
                                            isDataMatched = false;
                                            Msg.AppendFormat("This Inmarsat id {0}: Data of Background IP data rate is not match.<br />", ImarSatID).AppendLine();
                                            Msg.AppendFormat("Outbundle Data Rate from LES CDR Price:{0}.<br />", CDR_OutbundleRate).AppendLine();
                                            Msg.AppendFormat("Outbundle Data Rate from LES Reg CostPrice:{0}.<br />", LES_OutbundleDataCP).AppendLine();
                                            Msg.AppendFormat("Outbundle Data Rate from Cus Reg SellerPrice:{0}.<br />", Cus_OutbundleDataSP).AppendLine();
                                            returnMsg.AppendFormat("This Inmarsat id {0}: Data of Background IP data rate is not match.<br />", ImarSatID).AppendLine();
                                            Les_TotalCostCP += LES_OutbundleDataCP;
                                            Cus_TotalCostSP += Cus_OutbundleDataSP;

                                        }
                                    }
                                    else
                                    {
                                        //Les_TotalCostCP += LES_DataCP;
                                        Cus_TotalCostSP += Cus_DataSP;
                                        if (CDR_DataCP == 0 || Cus_DataSP == 0)
                                        {
                                            isDataMatched = true;
                                        }
                                        else
                                        {
                                            isDataMatched = false;
                                            Msg.AppendFormat("This Inmarsat id {0}: Data of Background IP data rate is not match.<br />", ImarSatID).AppendLine();
                                            Msg.AppendFormat("Data Rate from LES CDR Price:{0}.<br />", CDR_DataCP).AppendLine();
                                            Msg.AppendFormat("Data Rate from LES Reg CostPrice:{0}.<br />", LES_DataCP).AppendLine();
                                            Msg.AppendFormat("Data Rate from Cus Reg SellerPrice:{0}.<br />", Cus_DataSP).AppendLine();
                                            returnMsg.AppendFormat("This Inmarsat id {0}: Data of Background IP data rate is not match.<br />", ImarSatID).AppendLine();
                                        }
                                    }
                                }
                                else
                                    isDataMatched = true;
                                #endregion

                                #region V2Cellular rate calculation
                                if (CDR_VceCellularUintsCT != null)
                                {
                                    decimal V2CellTotalUnitsCT = 0;

                                    foreach (var ct in lstV2Cell_Trans)
                                    {
                                        if (ct.BCtTranUnitUsage != null)
                                        {
                                            V2CellTotalUnitsCT += Convert.ToDecimal(getMinutes(ct.BCtTranUnitUsage));
                                        }
                                    }

                                    double lngUnits = Convert.ToDouble(V2CellTotalUnitsCT);
                                    decimal? CDR_VceCellulartotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                                    decimal? LES_VceCellCPperMin = CDR_VceCellulartotalUnits_Minutes * LES_VoiceCellular;
                                    decimal? CUS_VceCellSPperMin = CDR_VceCellulartotalUnits_Minutes * CUS_VoiceCellular;

                                    if (CDR_VceCellulartotalUnits_Minutes != null)
                                        CDR_VceCellulartotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceCellulartotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);



                                    if (LES_VceCellCPperMin != null)
                                        LES_VceCellCPperMin = Math.Round(Convert.ToDecimal(LES_VceCellCPperMin), 2, MidpointRounding.AwayFromZero);

                                    if (CUS_VceCellSPperMin != null)
                                        CUS_VceCellSPperMin = Math.Round(Convert.ToDecimal(CUS_VceCellSPperMin), 2, MidpointRounding.AwayFromZero);

                                    Les_TotalCostCP += LES_VceCellCPperMin;
                                    Cus_TotalCostSP += CUS_VceCellSPperMin;

                                    if ((LES_VceCellCPperMin.Equals(CDR_VceCellularCostPriceCT) || LES_VceCellCPperMin > CDR_VceCellularCostPriceCT)
                                        && (CDR_VceCellularCostPriceCT < CUS_VceCellSPperMin))
                                    {
                                        isVoiceCellurlarMatched = true;
                                    }
                                    else
                                    {
                                        isVoiceCellurlarMatched = false;
                                        Msg.AppendFormat("This Inmarsat {0}: Voice to Cellular data rate is less than LES/CUS rate.<br />", ImarSatID).AppendLine();
                                        Msg.AppendFormat("Voice to Cellular Rate from LES CDR Price:{0}.<br />", CDR_VceCellularCostPriceCT).AppendLine();
                                        Msg.AppendFormat("Voice to Cellular Rate from LES Reg CostPrice:{0}.<br />", LES_VceCellCPperMin).AppendLine();
                                        Msg.AppendFormat("Voice to Cellular Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VceCellSPperMin).AppendLine();
                                        returnMsg.AppendFormat("This Inmarsat {0}: Voice to Cellular data rate is less than LES/CUS rate.<br />", ImarSatID).AppendLine();
                                    }
                                }
                                else
                                    isVoiceCellurlarMatched = true;
                                #endregion

                                #region V2Fixed rate calculations
                                if (CDR_VceFixedUintsCT != null)
                                {
                                    decimal V2FixedTotalUnitsCT = 0;

                                    foreach (var ct in lstV2Fixed_Trans)
                                    {
                                        if (ct.BCtTranUnitUsage != null)
                                        {
                                            V2FixedTotalUnitsCT += Convert.ToDecimal(getMinutes(ct.BCtTranUnitUsage));
                                        }
                                    }

                                    double lngUnits = Convert.ToDouble(V2FixedTotalUnitsCT);
                                    decimal? CDR_VceFixedTotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                                    if (CDR_VceFixedTotalUnits_Minutes != null)
                                        CDR_VceFixedTotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceFixedTotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

                                    decimal? LES_VceFixedCPperMin = CDR_VceFixedTotalUnits_Minutes * LES_VoiceFixed;
                                    decimal? CUS_VceFixedSPperMin = CDR_VceFixedTotalUnits_Minutes * CUS_VoiceFixed;

                                    if (LES_VceFixedCPperMin != null)
                                        LES_VceFixedCPperMin = Math.Round(Convert.ToDecimal(LES_VceFixedCPperMin), 2, MidpointRounding.AwayFromZero);

                                    if (CUS_VceFixedSPperMin != null)
                                        CUS_VceFixedSPperMin = Math.Round(Convert.ToDecimal(CUS_VceFixedSPperMin), 2, MidpointRounding.AwayFromZero);

                                    Les_TotalCostCP += LES_VceFixedCPperMin;
                                    Cus_TotalCostSP += CUS_VceFixedSPperMin;

                                    if ((LES_VceFixedCPperMin.Equals(CDR_VceFixedCostPriceCT) || LES_VceFixedCPperMin > CDR_VceFixedCostPriceCT)
                                        && (CDR_VceFixedCostPriceCT < CUS_VceFixedSPperMin))
                                    {
                                        isVoiceFixedMatched = true;
                                    }
                                    else
                                    {
                                        isVoiceFixedMatched = false;
                                        Msg.AppendFormat("This Inmarsat {0}: Voice to Fixed data rate is not match.<br />", ImarSatID).AppendLine();
                                        Msg.AppendFormat("Voice to Fixed Rate from LES CDR Price:{0}.<br />", CDR_VceFixedCostPriceCT).AppendLine();
                                        Msg.AppendFormat("Voice to Fixed Rate from LES Reg CostPrice:{0}.<br />", LES_VceFixedCPperMin).AppendLine();
                                        Msg.AppendFormat("Voice to Fixed Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VceFixedSPperMin).AppendLine();
                                        returnMsg.AppendFormat("This Inmarsat {0}: Voice to Fixed data rate is not match.<br />", ImarSatID).AppendLine();
                                    }
                                }
                                else
                                    isVoiceFixedMatched = true;
                                #endregion

                                #region v2FBB rate Calculations

                                if (CDR_VceFBBUintsCT != null)
                                {
                                    decimal V2FBBTotalUnitsCT = 0;

                                    foreach (var ct in lstV2Fbb_Trans)
                                    {
                                        if (ct.BCtTranUnitUsage != null)
                                        {
                                            V2FBBTotalUnitsCT += Convert.ToDecimal(getMinutes(ct.BCtTranUnitUsage));
                                        }
                                    }

                                    double lngUnits = Convert.ToDouble(V2FBBTotalUnitsCT);
                                    decimal? CDR_VceFBBTotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                                    if (CDR_VceFBBTotalUnits_Minutes != null)
                                        CDR_VceFBBTotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceFBBTotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

                                    decimal? LES_VceFBBCPperMin = CDR_VceFBBTotalUnits_Minutes * LES_VoiceFBB;
                                    decimal? CUS_VceFBBSPperMin = CDR_VceFBBTotalUnits_Minutes * CUS_VoiceFBB;

                                    if (LES_VceFBBCPperMin != null)
                                        LES_VceFBBCPperMin = Math.Round(Convert.ToDecimal(LES_VceFBBCPperMin), 2, MidpointRounding.AwayFromZero);

                                    if (CUS_VceFBBSPperMin != null)
                                        CUS_VceFBBSPperMin = Math.Round(Convert.ToDecimal(CUS_VceFBBSPperMin), 2, MidpointRounding.AwayFromZero);

                                    Les_TotalCostCP += LES_VceFBBCPperMin;
                                    Cus_TotalCostSP += CUS_VceFBBSPperMin;

                                    if ((LES_VceFBBCPperMin.Equals(CDR_VceFBBCostPriceCT) || LES_VceFBBCPperMin > CDR_VceFBBCostPriceCT)
                                        && (CDR_VceFBBCostPriceCT < CUS_VceFBBSPperMin))
                                    {
                                        isVoiceFbbMatched = true;
                                    }
                                    else
                                    {
                                        isVoiceFbbMatched = false;
                                        Msg.AppendFormat("This Inmarsat {0}: Voice to FleetBroadband data rate is less than LES/Cus rate.<br />", ImarSatID).AppendLine();
                                        Msg.AppendFormat("Voice to FleetBroadband Rate from LES CDR Price:{0}.<br />", CDR_VceFBBCostPriceCT).AppendLine();
                                        Msg.AppendFormat("Voice to FleetBroadband Rate from LES Reg CostPrice:{0}.<br />", LES_VceFBBCPperMin).AppendLine();
                                        Msg.AppendFormat("Voice to FleetBroadband Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VceFBBSPperMin).AppendLine();
                                        returnMsg.AppendFormat("This Inmarsat {0}: Voice to FleetBroadband data rate is less than LES/Cus rate.<br />", ImarSatID).AppendLine();
                                    }
                                }
                                else
                                    isVoiceFbbMatched = true;
                                #endregion

                                #region V2Iridium rate calculations

                                if (CDR_VceIridiumUintsCT != null)
                                {
                                    decimal V2IriTotalUnitsCT = 0;

                                    foreach (var ct in lstV2Iridium_Trans)
                                    {
                                        if (ct.BCtTranUnitUsage != null)
                                        {
                                            V2IriTotalUnitsCT += Convert.ToDecimal(getMinutes(ct.BCtTranUnitUsage));
                                        }
                                    }

                                    double lngUnits = Convert.ToDouble(V2IriTotalUnitsCT);
                                    decimal? CDR_VceIridiumTotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                                    if (CDR_VceIridiumTotalUnits_Minutes != null)
                                        CDR_VceIridiumTotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceIridiumTotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

                                    decimal? LES_VceIridiumCPperMin = CDR_VceIridiumTotalUnits_Minutes * LES_VoiceIridium;
                                    decimal? CUS_VceIridiumSPperMin = CDR_VceIridiumTotalUnits_Minutes * CUS_VoiceIridium;

                                    if (LES_VceIridiumCPperMin != null)
                                        LES_VceIridiumCPperMin = Math.Round(Convert.ToDecimal(LES_VceIridiumCPperMin), 2, MidpointRounding.AwayFromZero);

                                    if (CUS_VceIridiumSPperMin != null)
                                        CUS_VceIridiumSPperMin = Math.Round(Convert.ToDecimal(CUS_VceIridiumSPperMin), 2, MidpointRounding.AwayFromZero);

                                    Les_TotalCostCP += LES_VceIridiumCPperMin;
                                    Cus_TotalCostSP += CUS_VceIridiumSPperMin;

                                    if ((LES_VceIridiumCPperMin.Equals(CDR_VceIridiumCostPriceCT) || LES_VceIridiumCPperMin > CDR_VceIridiumCostPriceCT)
                                        && (CDR_VceIridiumCostPriceCT < CUS_VceIridiumSPperMin))
                                    {
                                        isVoiceIridiumMatched = true;
                                    }
                                    else
                                    {
                                        isVoiceIridiumMatched = false;
                                        Msg.AppendFormat("This Inmarsat {0}: Voice to Iridium data rate is less than LES/CUS rate.<br />", ImarSatID).AppendLine();
                                        Msg.AppendFormat("Voice to Iridium Rate from LES CDR Price:{0}.<br />", CDR_VceIridiumCostPriceCT).AppendLine();
                                        Msg.AppendFormat("Voice to Iridium Rate from LES Reg CostPrice:{0}.<br />", LES_VceIridiumCPperMin).AppendLine();
                                        Msg.AppendFormat("Voice to Iridium Rate from Cus Reg SellerPrice:{0}.<br />", CUS_VceIridiumSPperMin).AppendLine();
                                        returnMsg.AppendFormat("This Inmarsat {0}: Voice to Iridium data rate is less than LES/CUS rate.<br />", ImarSatID).AppendLine();
                                    }
                                }
                                else
                                    isVoiceIridiumMatched = true;
                                #endregion

                                #region ISDN Rate Calulations

                                if (CDR_ISDNUintsCT != null)
                                {
                                    decimal V2ISDNTotalUnitsCT = 0;

                                    foreach (var ct in lstISDN_Trans)
                                    {
                                        if (ct.BCtTranUnitUsage != null)
                                        {
                                            V2ISDNTotalUnitsCT += Convert.ToDecimal(getMinutes(ct.BCtTranUnitUsage));
                                        }
                                    }

                                    double lngUnits = Convert.ToDouble(V2ISDNTotalUnitsCT);
                                    decimal? CDR_ISDNTotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                                    if (CDR_ISDNTotalUnits_Minutes != null)
                                        CDR_ISDNTotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_ISDNTotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

                                    decimal? LES_ISDNCPperMin = CDR_ISDNTotalUnits_Minutes * LES_ISDN;
                                    decimal? CUS_ISDNSPperMin = CDR_ISDNTotalUnits_Minutes * CUS_ISDN;

                                    if (LES_ISDNCPperMin != null)
                                        LES_ISDNCPperMin = Math.Round(Convert.ToDecimal(LES_ISDNCPperMin), 2, MidpointRounding.AwayFromZero);

                                    if (CUS_ISDNSPperMin != null)
                                        CUS_ISDNSPperMin = Math.Round(Convert.ToDecimal(CUS_ISDNSPperMin), 2, MidpointRounding.AwayFromZero);

                                    Les_TotalCostCP += LES_ISDNCPperMin;
                                    Cus_TotalCostSP += CUS_ISDNSPperMin;

                                    if ((LES_ISDNCPperMin.Equals(CDR_ISDNCostPriceCT) || LES_ISDNCPperMin > CDR_ISDNCostPriceCT)
                                        && (CDR_ISDNCostPriceCT < CUS_ISDNSPperMin))
                                    {
                                        isISDNMatched = true;
                                    }
                                    else
                                    {
                                        isISDNMatched = false;
                                        Msg.AppendFormat("This Inmarsat {0}: ISDN rate is less than LES/Cus rate.<br />", ImarSatID).AppendLine();
                                        Msg.AppendFormat("ISDN Rate from LES CDR Price:{0}.<br />", CDR_ISDNCostPriceCT).AppendLine();
                                        Msg.AppendFormat("ISDN Rate from LES Reg CostPrice:{0}.<br />", LES_ISDNCPperMin).AppendLine();
                                        Msg.AppendFormat("ISDN Rate from Cus Reg SellerPrice:{0}.<br />", CUS_ISDNSPperMin).AppendLine();
                                        returnMsg.AppendFormat("This Inmarsat {0}: ISDN rate is less than LES/Cus rate.<br />", ImarSatID).AppendLine();
                                    }
                                }
                                else
                                    isISDNMatched = true;
                                #endregion

                                #region SMS rate calculations

                                if (CDR_SMSUintsCT != null)
                                {

                                    decimal? CDR_SMSTotalUnits_Msg = CDR_SMSUintsCT;

                                    if (CDR_SMSTotalUnits_Msg != null)
                                        CDR_SMSTotalUnits_Msg = Math.Round(Convert.ToDecimal(CDR_SMSTotalUnits_Msg), 2, MidpointRounding.AwayFromZero);

                                    decimal? LES_SMSCPperMin = CDR_SMSTotalUnits_Msg * LES_SMS;
                                    decimal? CUS_SMSSPperMin = CDR_SMSTotalUnits_Msg * CUS_SMS;

                                    if (LES_SMSCPperMin != null)
                                        LES_SMSCPperMin = Math.Round(Convert.ToDecimal(LES_SMSCPperMin), 2, MidpointRounding.AwayFromZero);

                                    if (CUS_SMSSPperMin != null)
                                        CUS_SMSSPperMin = Math.Round(Convert.ToDecimal(CUS_SMSSPperMin), 2, MidpointRounding.AwayFromZero);

                                    Les_TotalCostCP += LES_SMSCPperMin;
                                    Cus_TotalCostSP += CUS_SMSSPperMin;

                                    if ((LES_SMSCPperMin.Equals(CDR_SMSCostPriceCT) || LES_SMSCPperMin > CDR_SMSCostPriceCT)
                                        && (CDR_SMSCostPriceCT < CUS_SMSSPperMin))
                                    {
                                        isSMSMatched = true;
                                    }
                                    else
                                    {
                                        isISDNMatched = false;
                                        Msg.AppendFormat("This Inmarsat {0}: SMS rate is less than LES/CUS rate.<br />", ImarSatID).AppendLine();
                                        Msg.AppendFormat("SMS Rate from LES CDR Price:{0}.<br />", CDR_SMSCostPriceCT).AppendLine();
                                        Msg.AppendFormat("SMS Rate from LES Reg CostPrice:{0}.<br />", LES_SMSCPperMin).AppendLine();
                                        Msg.AppendFormat("SMS Rate from Cus Reg SellerPrice:{0}.<br />", CUS_SMSSPperMin).AppendLine();
                                        returnMsg.AppendFormat("This Inmarsat {0}: SMS rate is less than LES/CUS rate.<br />", ImarSatID).AppendLine();
                                    }
                                }
                                else
                                    isSMSMatched = true;
                                #endregion

                                string InvoiceNo = string.Empty;

                                #region Get Invoice Number
                                try
                                {
                                    InvoiceNo = db.BS2MtCusT.Max(t => t.BSMtCusInvNo);

                                    if (!string.IsNullOrEmpty(InvoiceNo))
                                    {
                                        if (InvoiceNo.Contains("SM"))
                                        {
                                            string tempInv = InvoiceNo.Replace("SM", string.Empty);
                                            if (int.TryParse(tempInv, out int result))
                                            {
                                                InvoiceNo = BillingHelpers.GenInvoice(false, result);
                                            }
                                        }
                                    }
                                    else
                                        InvoiceNo = BillingHelpers.GenInvoice(false, 0);
                                }
                                catch (Exception error)
                                {
                                    Msg.AppendFormat("MSI:{0} Error Msg: {1} Stack Trace: {2}. <br />", ImarSatID, error.Message.ToString(), error.StackTrace);
                                }
                                #endregion

                                #region check Rejection condition and store bool value

                                if ((CDR_StartDate >= LES_ContrRegStrDate && CDR_EndDate <= LES_ContrRegEndDate) || isNoTransData)
                                    isLES_ContrRegDate = true;
                                else
                                {
                                    isLES_ContrRegDate = false;
                                    Msg.AppendFormat("This Inmarsat {0}: Contract CDR Start/End date Less than les Reg Start/End Date.<br />", ImarSatID).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: LES Reg Strart Date {1}.<br />", CDR_StartDate, LES_ContrRegStrDate).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: LES Reg End Date {1}.<br />", CDR_StartDate, LES_ContrRegEndDate).AppendLine();
                                    returnMsg.AppendFormat("This Inmarsat {0}: Contract Reg start date Less than les Reg start Date.<br />", ImarSatID).AppendLine();
                                }

                                if ((LES_ContrBarStrDate == null && LES_ContrBarEndDate == null) || (CDR_StartDate > LES_ContrBarStrDate && CDR_EndDate > LES_ContrBarEndDate))
                                    isLES_ContrBarDate = true;
                                else
                                {
                                    isLES_ContrBarDate = false;
                                    Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than lES Bar Start/End Date.<br />", ImarSatID).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: LES Bar Strart Date {1}.<br />", CDR_StartDate, LES_ContrBarStrDate).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: LES Bar End Date {1}.<br />", CDR_StartDate, LES_ContrBarEndDate).AppendLine();
                                    returnMsg.AppendFormat("This Inmarsat {0}: Contract CCDR start date Greater than lES Bar start Date.<br />", ImarSatID).AppendLine();
                                }

                                if ((LES_ContrSusStrDate == null && LES_ContrSusEndDate == null) || (CDR_StartDate > LES_ContrSusStrDate && CDR_EndDate > LES_ContrSusEndDate))
                                    isLES_ContrSusDate = true;
                                else
                                {
                                    isLES_ContrBarDate = false;
                                    Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than lES Suspenstion Start/End Date.<br />", ImarSatID).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: LES Suspenstion Start Date {1}.<br />", CDR_StartDate, LES_ContrSusStrDate).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: LES Suspenstion End Date {1}.<br />", CDR_StartDate, LES_ContrSusEndDate).AppendLine();
                                    returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than lES Suspenstion start Date.<br />", ImarSatID).AppendLine();
                                }

                                if ((LES_ContrLayoutStrDate == null && LES_ContrLayoutEndDate == null) || (CDR_StartDate > LES_ContrLayoutStrDate && CDR_EndDate > LES_ContrLayoutEndDate))
                                    isLES_ContrLayUpDate = true;
                                else
                                {
                                    isLES_ContrLayUpDate = true;
                                    Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than LES LayUp Start/End Date.<br />", ImarSatID).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: LES LayUp Start Date {1}.<br />", CDR_StartDate, LES_ContrSusStrDate).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: LES LayUp End Date {1}.<br />", CDR_StartDate, LES_ContrSusEndDate).AppendLine();
                                    returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than LayUp Start/End Date.<br />", ImarSatID).AppendLine();
                                }

                                if ((CDR_StartDate >= CUS_ContrRegStrDate && CDR_EndDate <= CUS_ContrRegEndDate) || isNoTransData)
                                    isCUS_ContrRegDate = true;
                                else
                                {
                                    isCUS_ContrRegDate = false;
                                    Msg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Less than CUS Reg Start/End Date.<br />", ImarSatID).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: CUS Reg Strart Date {1}.<br />", CDR_StartDate, CUS_ContrRegStrDate).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: CUS Reg End Date {1}.<br />", CDR_StartDate, CUS_ContrRegEndDate).AppendLine();
                                    returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Less than CUS Reg Start/End Date.<br />", ImarSatID).AppendLine();
                                }

                                if ((CUS_ContrBarStrDate == null && CUS_ContrBarEndDate == null) || (CDR_StartDate > CUS_ContrBarStrDate && CDR_EndDate > CUS_ContrBarEndDate))
                                    isCUS_ContrBarDate = true;
                                else
                                {
                                    isCUS_ContrBarDate = false;
                                    Msg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Greater than CUS Bar Start/End Date.<br />", ImarSatID).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: CUS Bar Strart Date {1}.<br />", CDR_StartDate, CUS_ContrBarStrDate).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: CUS Bar End Date {1}.<br />", CDR_StartDate, CUS_ContrBarStrDate).AppendLine();
                                    returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start/End date Greater than CUS Bar Start/End Date.<br />", ImarSatID).AppendLine();
                                }

                                if ((CUS_ContrSusStrDate == null && CUS_ContrSusEndDate == null) || (CDR_StartDate > CUS_ContrSusStrDate && CDR_EndDate > CUS_ContrSusEndDate))
                                    isCUS_ContrSusDate = true;
                                else
                                {
                                    isCUS_ContrSusDate = false;
                                    Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS Suspenstion Start/End Date.<br />", ImarSatID).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: CUS Suspenstion Start Date {1}.<br />", CDR_StartDate, CUS_ContrSusStrDate).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: CUS Suspenstion End Date {1}.<br />", CDR_StartDate, CUS_ContrSusEndDate).AppendLine();
                                    returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS Suspenstion Start/End Date.<br />", ImarSatID).AppendLine();
                                }

                                if ((CUS_ContrLayoutStrDate == null && CUS_ContrLayoutEndDate == null) || (CDR_StartDate > CUS_ContrLayoutStrDate && CDR_EndDate > CUS_ContrLayoutEndDate))
                                    isCUS_ContrLayUpDate = true;
                                else
                                {
                                    isCUS_ContrLayUpDate = true;
                                    Msg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS LayUp Start/End Date.<br />", ImarSatID).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: CUS LayUp Start Date {1}.<br />", CDR_StartDate, LES_ContrSusStrDate).AppendLine();
                                    Msg.AppendFormat("Contract CDR Start Date {0}: CUS LayUp End Date {1}.<br />", CDR_StartDate, LES_ContrSusEndDate).AppendLine();
                                    returnMsg.AppendFormat("This Inmarsat {0}: Contract CDR start date Greater than CUS LayUp Start/End Date.<br />", ImarSatID).AppendLine();
                                }
                                #endregion

                                if (isRecurFeeMached && isDataMatched && isVoiceCellurlarMatched && isVoiceFixedMatched && isVoiceFbbMatched && isVoiceIridiumMatched &&
                                    isISDNMatched && isLES_ContrRegDate && isLES_ContrBarDate && isLES_ContrSusDate && isLES_ContrLayUpDate && isCUS_ContrRegDate &&
                                    isCUS_ContrBarDate && isCUS_ContrSusDate && isCUS_ContrLayUpDate && isSMSMatched)
                                {
                                    if (!string.IsNullOrEmpty(InvoiceNo))
                                    {
                                        BSt2FbbTrans SecMtCust = new BSt2FbbTrans();

                                        SecMtCust.Bt2CusFbInvNo = InvoiceNo;
                                        SecMtCust.Bt2CusFbLES = Convert.ToString(LesID);
                                        SecMtCust.Bt2CusFbCusRegID = CUS_RegsID;
                                        SecMtCust.Bt2CusFbIMSIId = ImsiID;
                                        SecMtCust.Bt2CusFbMsidnId = ImarSatID;
                                        SecMtCust.Bt2CusFbLesRefCode = CT_Fbs.BCtFbLesRefCode;
                                        SecMtCust.Bt2CusFbSatCode = CT_Fbs.BCtFbSatCode;
                                        SecMtCust.Bt2CusFbEquipmentCode = CT_Fbs.BCtFbEquipmentCode;
                                        //SecMtCust.BSMtCusLesCode 
                                        SecMtCust.Bt2CusFbCdrCode = CT_Fbs.BCtFbCdrCode;
                                        //SecMtCust.BSMtCusSatType = null;
                                        SecMtCust.Bt2CusFbTranDest = CT_Fbs.BCtTranServDest;
                                        SecMtCust.Bt2CusFbTranServType = null;
                                        SecMtCust.Bt2CusFbServiceType = CT_Fbs.BCtTranServType;
                                        SecMtCust.Bt2CusFbTranUsage = CT_Fbs.BCtTranUnitUsage;
                                        SecMtCust.Bt2CusFbTranUnitType = 10;
                                        SecMtCust.Bt2CusFbRecurFee = CUS_RecurFee;
                                        Cus_TotalCostSP += SecMtCust.Bt2CusFbRecurFee;
                                        SecMtCust.Bt2CusFbRecurFeeType = CT_Fbs.BCtAFbRecurFeeType;
                                        //SecMtCust.BSMtCusCDRRef = CT_Fbs.BCtAFbCDRRef;
                                        SecMtCust.Bt2CusFbBillno = CT_Fbs.BCtFbBillNo;
                                        SecMtCust.Bt2CusFbBillPeriod = CT_Fbs.BCtFbBillPeriod;
                                        SecMtCust.Bt2CusFbBillDate = CT_Fbs.BCtFbBillDate;

                                        if (CDR_RecuFee < CDR_DataCP && isOutbundleUsed)
                                        {
                                            LES_InvRptDataCostTot = CDR_DataCP - CDR_RecuFee;
                                            LES_InvRptDataCostTot = Math.Round(Convert.ToDecimal(LES_InvRptDataCostTot), 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                            LES_InvRptDataCostTot = 0;

                                        Guid guid = new Guid();
                                        SecMtCust.Bt2CusFbTranGUID = guid.ToString();

                                        db.BSt2FbbTrans.Add(SecMtCust);

                                        // Get value from BS2MtCusT and save to BInvRprt table
                                        BPtInvLstReport Invreport = new BPtInvLstReport();

                                        if (SecMtCust.Bt2CusFbServiceType != null)
                                        {
                                            Invreport.BInvRptGuID = SecMtCust.Bt2CusFbTranGUID;
                                            Invreport.BInvRprtSerType = SecMtCust.Bt2CusFbServiceType;
                                            Invreport.BInvRprtTotBil = SecMtCust.Bt2CusFbRecurFee;
                                            Invreport.BInvRprtTotCst = CT_Fbs.BCtFbRecurFee;
                                            db.BPtInvLstReports.Add(Invreport);
                                        }

                                        #region Inv Standard IP rate calculations

                                        decimal? outbindleData = 0;
                                        decimal? LES_RptDataCost = 0;
                                        decimal? CUS_RptDataCost = 0;

                                        if (is_CUSGBPlan)
                                        {
                                            Cus_DataPlan = Cus_DataPlan * 1024;
                                        }

                                        foreach (var records in lstStdIP_Trans)
                                        {
                                            BSt2FbbTrans bs2Sec = new BSt2FbbTrans();
                                            bs2Sec.Bt2CusFbTranStartDate = records.BCtTranStartDate;
                                            bs2Sec.Bt2CusFbTranStartTime = records.BCtTranStartTime;
                                            bs2Sec.Bt2CusFbTranDOrig = records.BCtTranOrigin;
                                            bs2Sec.Bt2CusFbTranDest = records.BCtTranServDest;
                                            bs2Sec.Bt2CusFbTranServType = CUS_ServTypeID;

                                            double dataconvertToMP = Convert.ToDouble(records.BCtTranUnitUsage);
                                            bs2Sec.Bt2CusFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertBitToMegabytes(dataconvertToMP)), 2, MidpointRounding.AwayFromZero);
                                            bs2Sec.Bt2CusFbTranUnitType = lstUnityType.FirstOrDefault(t => t.Units.Equals("MB")).UID;
                                            outbindleData += bs2Sec.Bt2CusFbTranUsage;

                                            if (Cus_DataPlan <= outbindleData)
                                            {
                                                bs2Sec.Bt2CusFbTranTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.Bt2CusFbTranUsage * Cus_OutbundleRate), 3, MidpointRounding.AwayFromZero);

                                                if (Invreport.BInvRprtSerType != null)
                                                    CUS_RptDataCost += bs2Sec.Bt2CusFbTranTotalUsage;
                                            }
                                            else
                                            {
                                                bs2Sec.Bt2CusFbTranTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.Bt2CusFbTranUsage * 0), 3, MidpointRounding.AwayFromZero);

                                                if (Invreport.BInvRprtSerType != null)
                                                    CUS_RptDataCost += bs2Sec.Bt2CusFbTranTotalUsage;
                                            }
                                            bs2Sec.Bt2CusFbTranComTypes = 1;

                                            db.BSt2FbbTrans.Add(bs2Sec);
                                        }

                                        // Invoice List Report Add in BInvLstReports
                                        if (lstStdIP_Trans.Count > 0)
                                        {
                                            Invreport = new BPtInvLstReport();
                                            try
                                            {
                                                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeID).BsPtCusServiceTypes;
                                            }
                                            catch { Invreport.BInvRprtSerType = null; }

                                            if (Invreport.BInvRprtSerType != null)
                                            {
                                                Invreport.BInvRptGuID = SecMtCust.Bt2CusFbTranGUID;
                                                Invreport.BInvRprtTotCst = LES_InvRptDataCostTot;
                                                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptDataCost), 3, MidpointRounding.AwayFromZero);
                                                db.BPtInvLstReports.Add(Invreport);
                                            }
                                        }
                                        #endregion

                                        #region Invoice Voice to Cellular rate Calculations

                                        decimal? LES_RptV2cCost = CDR_VceCellularCostPriceCT;
                                        decimal? CUS_RptV2cCost = 0;

                                        foreach (var records in lstV2Cell_Trans)
                                        {
                                            BSt2FbbTrans bs2Sec = new BSt2FbbTrans();
                                            bs2Sec.Bt2CusFbTranStartDate = records.BCtTranStartDate;
                                            bs2Sec.Bt2CusFbTranStartTime = records.BCtTranStartTime;
                                            bs2Sec.Bt2CusFbTranDOrig = records.BCtTranOrigin;
                                            bs2Sec.Bt2CusFbTranDest = records.BCtTranServDest;
                                            bs2Sec.Bt2CusFbTranServType = CUS_ServTypeMobile;

                                            decimal? v2CTransSec = 0;
                                            if (records.BCtTranUnitUsage != null)
                                            {
                                                v2CTransSec = getMinutes(records.BCtTranUnitUsage);
                                            }
                                            long SecconvertToMin = Convert.ToInt64(v2CTransSec);
                                            bs2Sec.Bt2CusFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                            bs2Sec.Bt2CusFbTranUnitType = lstUnityType.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                            bs2Sec.Bt2CusFbTranTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.Bt2CusFbTranUsage * CUS_VoiceCellular), 3, MidpointRounding.AwayFromZero);

                                            if (Invreport.BInvRprtSerType != null)
                                                CUS_RptV2cCost += bs2Sec.Bt2CusFbTranTotalUsage;

                                            bs2Sec.Bt2CusFbTranComTypes = 2;

                                            db.BSt2FbbTrans.Add(bs2Sec);
                                        }

                                        // Invoice List Report Add in BInvLstReports
                                        if (lstV2Cell_Trans.Count > 0)
                                        {
                                            Invreport = new BPtInvLstReport();
                                            try
                                            {
                                                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeMobile).BsPtCusServiceTypes;
                                            }
                                            catch { Invreport.BInvRprtSerType = null; }

                                            if (Invreport.BInvRprtSerType != null)
                                            {
                                                Invreport.BInvRptGuID = SecMtCust.Bt2CusFbTranGUID;
                                                Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2cCost), 2, MidpointRounding.AwayFromZero);
                                                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2cCost), 2, MidpointRounding.AwayFromZero);
                                                db.BPtInvLstReports.Add(Invreport);
                                            }
                                        }
                                        #endregion

                                        #region Invoice Voice to Fixed rate calculations

                                        decimal? LES_RptV2fCost = CDR_VceFixedCostPriceCT;
                                        decimal? CUS_RptV2fCost = 0;

                                        foreach (var records in lstV2Fixed_Trans)
                                        {
                                            BSt2FbbTrans bs2Sec = new BSt2FbbTrans();
                                            bs2Sec.Bt2CusFbTranStartDate = records.BCtTranStartDate;
                                            bs2Sec.Bt2CusFbTranStartTime = records.BCtTranStartTime;
                                            bs2Sec.Bt2CusFbTranDOrig = records.BCtTranOrigin;
                                            bs2Sec.Bt2CusFbTranDest = records.BCtTranServDest;
                                            bs2Sec.Bt2CusFbTranServType = CUS_ServTypeVce2Fixed;

                                            decimal? v2FixedTransSec = 0;
                                            if (records.BCtTranUnitUsage != null)
                                            {
                                                v2FixedTransSec = getMinutes(records.BCtTranUnitUsage);
                                            }

                                            long SecconvertToMin = Convert.ToInt64(v2FixedTransSec);
                                            bs2Sec.Bt2CusFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                            bs2Sec.Bt2CusFbTranUnitType = lstUnityType.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                            bs2Sec.Bt2CusFbTranTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.Bt2CusFbTranUsage * CUS_VoiceFixed), 3, MidpointRounding.AwayFromZero);

                                            if (Invreport.BInvRprtSerType != null)
                                                CUS_RptV2fCost += bs2Sec.Bt2CusFbTranTotalUsage;

                                            bs2Sec.Bt2CusFbTranComTypes = 2;

                                            db.BSt2FbbTrans.Add(bs2Sec);
                                        }

                                        // Invoice List Report Add in BInvLstReports
                                        if (lstV2Fixed_Trans.Count > 0)
                                        {
                                            Invreport = new BPtInvLstReport();

                                            try
                                            {
                                                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeVce2Fixed).BsPtCusServiceTypes;
                                            }
                                            catch { Invreport.BInvRprtSerType = null; }

                                            if (Invreport.BInvRprtSerType != null)
                                            {
                                                Invreport.BInvRptGuID = SecMtCust.Bt2CusFbTranGUID;
                                                Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2fCost), 3, MidpointRounding.AwayFromZero);
                                                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2fCost), 3, MidpointRounding.AwayFromZero);
                                                db.BPtInvLstReports.Add(Invreport);
                                            }
                                        }
                                        #endregion

                                        #region Invoice V2FBB rate calculations

                                        decimal? LES_RptV2fbCost = CDR_VceFBBCostPriceCT;
                                        decimal? CUS_RptV2fbCost = 0;

                                        foreach (var records in lstV2Fbb_Trans)
                                        {
                                            BSt2FbbTrans bs2Sec = new BSt2FbbTrans();
                                            bs2Sec.Bt2CusFbTranStartDate = records.BCtTranStartDate;
                                            bs2Sec.Bt2CusFbTranStartTime = records.BCtTranStartTime;
                                            bs2Sec.Bt2CusFbTranDOrig = records.BCtTranOrigin;
                                            bs2Sec.Bt2CusFbTranDest = records.BCtTranServDest;
                                            bs2Sec.Bt2CusFbTranServType = CUS_ServTypeVce2FBB;

                                            decimal? v2FBBTransSec = 0;
                                            if (records.BCtTranUnitUsage != null)
                                            {
                                                v2FBBTransSec = getMinutes(records.BCtTranUnitUsage);
                                            }

                                            long SecconvertToMin = Convert.ToInt64(v2FBBTransSec);
                                            bs2Sec.Bt2CusFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                            bs2Sec.Bt2CusFbTranUnitType = lstUnityType.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                            bs2Sec.Bt2CusFbTranTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.Bt2CusFbTranUsage * CUS_VoiceFBB), 3, MidpointRounding.AwayFromZero);

                                            if (Invreport.BInvRprtSerType != null)
                                                CUS_RptV2fbCost += bs2Sec.Bt2CusFbTranTotalUsage;

                                            bs2Sec.Bt2CusFbTranComTypes = 2;

                                            db.BSt2FbbTrans.Add(bs2Sec);
                                        }

                                        if (lstV2Fbb_Trans.Count > 0)
                                        {
                                            Invreport = new BPtInvLstReport();
                                            try
                                            {
                                                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeVce2FBB).BsPtCusServiceTypes;
                                            }
                                            catch { Invreport.BInvRprtSerType = null; }

                                            if (Invreport.BInvRprtSerType != null)
                                            {
                                                Invreport.BInvRptGuID = SecMtCust.Bt2CusFbTranGUID;
                                                Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2fbCost), 3, MidpointRounding.AwayFromZero);
                                                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2fbCost), 3, MidpointRounding.AwayFromZero);
                                                db.BPtInvLstReports.Add(Invreport);
                                            }
                                        }
                                        #endregion

                                        #region Invoice V2 Iridium rate calc

                                        decimal? LES_RptV2irdCost = CDR_VceIridiumCostPriceCT;
                                        decimal? CUS_RptV2irdCost = 0;

                                        foreach (var records in lstV2Iridium_Trans)
                                        {
                                            BSt2FbbTrans bs2Sec = new BSt2FbbTrans();
                                            bs2Sec.Bt2CusFbTranStartDate = records.BCtTranStartDate;
                                            bs2Sec.Bt2CusFbTranStartTime = records.BCtTranStartTime;
                                            bs2Sec.Bt2CusFbTranDOrig = records.BCtTranOrigin;
                                            bs2Sec.Bt2CusFbTranDest = records.BCtTranServDest;
                                            bs2Sec.Bt2CusFbTranServType = CUS_ServTypeVce2Iridium;

                                            decimal? v2IriTransSec = 0;
                                            if (records.BCtTranUnitUsage != null)
                                            {
                                                v2IriTransSec = getMinutes(records.BCtTranUnitUsage);
                                            }

                                            long SecconvertToMin = Convert.ToInt64(v2IriTransSec);

                                            bs2Sec.Bt2CusFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 3, MidpointRounding.AwayFromZero);
                                            bs2Sec.Bt2CusFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                            bs2Sec.Bt2CusFbTranTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.Bt2CusFbTranUsage * CUS_VoiceIridium), 3, MidpointRounding.AwayFromZero);

                                            if (Invreport.BInvRprtSerType != null)
                                                CUS_RptV2irdCost += bs2Sec.Bt2CusFbTranTotalUsage;

                                            bs2Sec.Bt2CusFbTranComTypes = 2;

                                            db.BSt2FbbTrans.Add(bs2Sec);
                                        }

                                        // Invoice List Report Add in BInvLstReports
                                        if (lstV2Iridium_Trans.Count > 0)
                                        {
                                            Invreport = new BPtInvLstReport();
                                            try
                                            {
                                                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeVce2Iridium).BsPtCusServiceTypes;
                                            }
                                            catch { Invreport.BInvRprtSerType = null; }

                                            if (Invreport.BInvRprtSerType != null)
                                            {
                                                Invreport.BInvRptGuID = SecMtCust.Bt2CusFbTranGUID;
                                                Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2irdCost), 3, MidpointRounding.AwayFromZero);
                                                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2irdCost), 3, MidpointRounding.AwayFromZero);
                                                db.BPtInvLstReports.Add(Invreport);
                                            }
                                        }
                                        #endregion

                                        #region Invoice ISDN rate calc

                                        decimal? LES_RptV2isdnCost = CDR_ISDNCostPriceCT;
                                        decimal? CUS_RptV2isdnCost = 0;

                                        foreach (var records in lstISDN_Trans)
                                        {
                                            BSt2FbbTrans bs2Sec = new BSt2FbbTrans();
                                            bs2Sec.Bt2CusFbTranStartDate = records.BCtTranStartDate;
                                            bs2Sec.Bt2CusFbTranStartTime = records.BCtTranStartTime;
                                            bs2Sec.Bt2CusFbTranDOrig = records.BCtTranOrigin;
                                            bs2Sec.Bt2CusFbTranDest = records.BCtTranServDest;
                                            bs2Sec.Bt2CusFbTranServType = CUS_ServTypeVce2ISDN;

                                            decimal? v2ISDNTransSec = 0;
                                            if (records.BCtTranUnitUsage != null)
                                            {
                                                v2ISDNTransSec = getMinutes(records.BCtTranUnitUsage);
                                            }

                                            long SecconvertToMin = Convert.ToInt64(v2ISDNTransSec);

                                            bs2Sec.Bt2CusFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 3, MidpointRounding.AwayFromZero);
                                            bs2Sec.Bt2CusFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                            bs2Sec.Bt2CusFbTranTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.Bt2CusFbTranUsage * CUS_ISDN), 3, MidpointRounding.AwayFromZero);

                                            if (Invreport.BInvRprtSerType != null)
                                                CUS_RptV2isdnCost += bs2Sec.Bt2CusFbTranTotalUsage;

                                            bs2Sec.Bt2CusFbTranComTypes = 2;

                                            db.BSt2FbbTrans.Add(bs2Sec);
                                        }

                                        // Invoice List Report Add in BInvLstReports
                                        if (lstISDN_Trans.Count > 0)
                                        {
                                            Invreport = new BPtInvLstReport();
                                            try
                                            {
                                                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeVce2ISDN).BsPtCusServiceTypes;
                                            }
                                            catch { Invreport.BInvRprtSerType = null; }

                                            if (Invreport.BInvRprtSerType != null)
                                            {
                                                Invreport.BInvRptGuID = SecMtCust.Bt2CusFbTranGUID;
                                                Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2isdnCost), 3, MidpointRounding.AwayFromZero);
                                                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2isdnCost), 3, MidpointRounding.AwayFromZero);
                                                db.BPtInvLstReports.Add(Invreport);
                                            }
                                        }
                                        #endregion

                                        #region Invoice SMS rate Calc

                                        int commtype = 1;
                                        try
                                        {
                                            commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                                        }
                                        catch
                                        {
                                            commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                                        }

                                        decimal? LES_RptV2smsCost = CDR_SMSCostPriceCT;
                                        decimal? CUS_RptV2smsCost = 0;

                                        foreach (var records in lstSMS_Trans)
                                        {
                                            BSt2FbbTrans bs2Sec = new BSt2FbbTrans();
                                            bs2Sec.Bt2CusFbTranStartDate = records.BCtTranStartDate;
                                            bs2Sec.Bt2CusFbTranStartTime = records.BCtTranStartTime;
                                            bs2Sec.Bt2CusFbTranDOrig = records.BCtTranOrigin;
                                            bs2Sec.Bt2CusFbTranDest = records.BCtTranServDest;
                                            bs2Sec.Bt2CusFbTranServType = CUS_ServTypeSMS;

                                            //long SecconvertToMin = Convert.ToInt64(records.BsCtBillIncre);
                                            bs2Sec.Bt2CusFbTranUsage = Convert.ToInt32(records.BCtTranUnitUsage); //(decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                            try
                                            {
                                                bs2Sec.Bt2CusFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("message")).UID;
                                            }
                                            catch
                                            {
                                                bs2Sec.Bt2CusFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Message")).UID;
                                            }

                                            bs2Sec.Bt2CusFbTranTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.Bt2CusFbTranUsage * CUS_SMS), 3, MidpointRounding.AwayFromZero);

                                            if (Invreport.BInvRprtSerType != null)
                                                CUS_RptV2smsCost += bs2Sec.Bt2CusFbTranTotalUsage;

                                            bs2Sec.Bt2CusFbTranComTypes = commtype;

                                            db.BSt2FbbTrans.Add(bs2Sec);
                                        }

                                        // Invoice List Report Add in BInvLstReports
                                        if (lstSMS_Trans.Count > 0)
                                        {
                                            Invreport = new BPtInvLstReport();
                                            try
                                            {
                                                Invreport.BInvRprtSerType = lstSP.FirstOrDefault(t => t.CusServDID == CUS_ServTypeSMS).BsPtCusServiceTypes;
                                            }
                                            catch { Invreport.BInvRprtSerType = null; }
                                            if (Invreport.BInvRprtSerType != null)
                                            {
                                                Invreport.BInvRptGuID = SecMtCust.Bt2CusFbTranGUID;
                                                Invreport.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_RptV2smsCost), 3, MidpointRounding.AwayFromZero);
                                                Invreport.BInvRprtTotBil = Math.Round(Convert.ToDecimal(CUS_RptV2smsCost), 3, MidpointRounding.AwayFromZero);
                                                db.BPtInvLstReports.Add(Invreport);
                                            }
                                        }

                                        #endregion

                                        #region Invoice statement
                                        // List of Satatement 
                                        //Bs2Statement statment = null;
                                        //try
                                        //{
                                        //    statment = db.Bs2Statement.FirstOrDefault(t => t.fkBs2CusID == SecFBBID);
                                        //}
                                        //catch { }

                                        //if (statment == null)
                                        //{
                                        //    statment = new Bs2Statement();

                                        //    statment.fkBs2CusID = SecFBBID;
                                        //    statment.fkSimID = SimID;
                                        //    statment.Bs2StTotalCP = Les_TotalCostCP;
                                        //    statment.Bs2StTotalSP = Cus_TotalCostSP;
                                        //    statment.Bs2StSMTSCost = SMTS_TotalCost;
                                        //    statment.fkSPID = LesID;
                                        //    statment.fkCusID = CUS_Id;
                                        //    statment.Bs2StIvoiceDate = DateTime.Now.Date;
                                        //    statment.Bs2StDuration = CT_Fbs.BCtTranServDest;

                                        //    if (statment.Bs2StTotalCP > 0 && statment.Bs2StTotalSP > 0)
                                        //    {
                                        //        decimal CrossProfit = Convert.ToDecimal(statment.Bs2StTotalSP - statment.Bs2StSMTSCost);
                                        //        decimal Margin = (CrossProfit / Convert.ToDecimal(statment.Bs2StTotalSP) * 100);
                                        //        statment.Bs2stMargin = Margin > 0 ? Convert.ToInt32(Margin) : (int)Math.Abs(Margin);
                                        //    }

                                        //    db.Bs2Statement.Add(statment);
                                        //}
                                        #endregion

                                    }
                                }
                                else
                                {
                                    BSt1FbbTrans SecRejLes = new BSt1FbbTrans();

                                    SecRejLes.Bt1LesFbLES = Convert.ToString(LesID);
                                    SecRejLes.Bt1LesFbLesRegID = LES_RegID;
                                    SecRejLes.Bt1LesFbCusRegID = CUS_RegsID;
                                    SecRejLes.Bt1LesFbMsidnId = ImarSatID;
                                    SecRejLes.Bt1LesFbIMSIId = ImsiID;
                                    SecRejLes.Bt1LesFbLesRefCode = CT_Fbs.BCtFbLesRefCode;
                                    SecRejLes.Bt1LesFbSatCode = CT_Fbs.BCtFbSatCode;
                                    SecRejLes.Bt1LesFbEquipmentCode = CT_Fbs.BCtFbEquipmentCode;
                                    SecRejLes.Bt1LesFbCdrCode = CT_Fbs.BCtFbCdrCode;
                                    SecRejLes.Bt1LesFbTranDest = CT_Fbs.BCtTranServDest;
                                    SecRejLes.Bt1LesFbServiceType = CT_Fbs.BCtTranServType;
                                    SecRejLes.Bt1LesFbTranServType = null;
                                    SecRejLes.Bt1LesFbTranUsage = Convert.ToInt32(CT_Fbs.BCtTranUnitUsage);
                                    SecRejLes.Bt1LesFbTranUnitType = 10;
                                    SecRejLes.Bt1LesFbRecurFee = LES_RecurFee;
                                    SecRejLes.Bt1LesFbRecurFeeType = CT_Fbs.BCtAFbRecurFeeType;
                                    SecRejLes.Bt1LesFbBillno = CT_Fbs.BCtFbBillNo;
                                    SecRejLes.Bt1LesFbBillPeriod = CT_Fbs.BCtFbBillPeriod;
                                    SecRejLes.Bt1LesFbBillDate = CT_Fbs.BCtFbBillDate;

                                    Guid guid = new Guid();
                                    SecRejLes.Bt1LesGUID = guid.ToString();

                                    db.BSt1FbbTrans.Add(SecRejLes);


                                    if (CDR_RecuFee < CDR_DataCP && isOutbundleUsed)
                                    {
                                        LES_InvRptDataCostTot = CDR_DataCP - CDR_RecuFee;
                                        LES_InvRptDataCostTot = Math.Round(Convert.ToDecimal(LES_InvRptDataCostTot), 2, MidpointRounding.AwayFromZero);
                                    }
                                    else
                                        LES_InvRptDataCostTot = 0;

                                    BPtInvLstReport InvreportCost = new BPtInvLstReport();
                                    if (SecRejLes.Bt1LesFbServiceType != null)
                                    {
                                        InvreportCost.BInvRptGuID = SecRejLes.Bt1LesGUID;
                                        InvreportCost.BInvRprtSerType = SecRejLes.Bt1LesFbServiceType;
                                        InvreportCost.BInvRprtTotCst = CDR_RecuFee;
                                        InvreportCost.BInvRprtTotBil = null;

                                        db.BPtInvLstReports.Add(InvreportCost);
                                    }

                                    BSt1LesRejLogs lesRejLog = new BSt1LesRejLogs();
                                    lesRejLog.Bs1LesRejGuID = SecRejLes.Bt1LesGUID;
                                    lesRejLog.Bs1lesRejection = Msg.ToString();
                                    lesRejLog.Bs1lesRejRemark = "";
                                    db.BSt1LesRejLogs.Add(lesRejLog);

                                    #region Standard IP Tranasction

                                    BSt1FbbTrans lesRej1;
                                    foreach (var records in lstStdIP_Trans)
                                    {
                                        lesRej1 = new BSt1FbbTrans();
                                        lesRej1.Bt1LesFbTranStartDate = records.BCtTranStartDate;
                                        lesRej1.Bt1LesFbTranStartTime = records.BCtTranStartTime;
                                        lesRej1.Bt1LesFbTranDOrig = records.BCtTranOrigin;
                                        lesRej1.Bt1LesFbTranDest = records.BCtTranServDest;
                                        lesRej1.Bt1LesFbTranServType = ServTypeID;

                                        double dataconvertToMP = Convert.ToDouble(records.BCtTranUnitUsage);
                                        lesRej1.Bt1LesFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertBitToMegabytes(dataconvertToMP)), 2, MidpointRounding.AwayFromZero);
                                        lesRej1.Bt1LesFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("MB")).UID;
                                        lesRej1.Bt1LesFbTranTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bt1LesFbTranUsage * LES_DataRateper1MB), 3, MidpointRounding.AwayFromZero);
                                        lesRej1.Bt1LesFbTranComTypes = 1;

                                        db.BSt1FbbTrans.Add(lesRej1);
                                    }

                                    // Invoice List Report Add in BInvLstReports
                                    if (lstStdIP_Trans.Count > 0)
                                    {
                                        InvreportCost = new BPtInvLstReport();
                                        try
                                        {
                                            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeID).BsPtServiceTypes;
                                        }
                                        catch { InvreportCost.BInvRprtSerType = null; }

                                        if (InvreportCost.BInvRprtSerType != null)
                                        {
                                            InvreportCost.BInvRptGuID = SecRejLes.Bt1LesGUID;
                                            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(LES_InvRptDataCostTot), 2, MidpointRounding.AwayFromZero);
                                            InvreportCost.BInvRprtTotBil = null;
                                            db.BPtInvLstReports.Add(InvreportCost);
                                        }
                                    }
                                    #endregion

                                    #region Reject2 V2 Cellular Trans

                                    foreach (var records in lstV2Cell_Trans)
                                    {
                                        lesRej1 = new BSt1FbbTrans();
                                        lesRej1.Bt1LesFbTranStartDate = records.BCtTranStartDate;
                                        lesRej1.Bt1LesFbTranStartTime = records.BCtTranStartTime;
                                        lesRej1.Bt1LesFbTranDOrig = records.BCtTranOrigin;
                                        lesRej1.Bt1LesFbTranDest = records.BCtTranServDest;
                                        lesRej1.Bt1LesFbTranServType = ServTypeMobile;

                                        decimal? v2CTransSec = 0;
                                        if (records.BCtTranUnitUsage != null)
                                        {
                                            v2CTransSec = getMinutes(records.BCtTranUnitUsage);
                                        }

                                        long SecconvertToMin = Convert.ToInt64(v2CTransSec);
                                        lesRej1.Bt1LesFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                        lesRej1.Bt1LesFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                        lesRej1.Bt1LesFbTranTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bt1LesFbTranUsage * LES_VoiceCellular), 3, MidpointRounding.AwayFromZero);
                                        lesRej1.Bt1LesFbTranComTypes = 2;

                                        db.BSt1FbbTrans.Add(lesRej1);
                                    }

                                    // Invoice List Report Add in BInvLstReports
                                    if (lstV2Cell_Trans.Count > 0)
                                    {
                                        InvreportCost = new BPtInvLstReport();
                                        try
                                        {
                                            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeMobile).BsPtServiceTypes;
                                        }
                                        catch { InvreportCost.BInvRprtSerType = null; }

                                        if (InvreportCost.BInvRprtSerType != null)
                                        {
                                            InvreportCost.BInvRptGuID = SecRejLes.Bt1LesGUID;
                                            //InvreportCost.fkBCus2ID = null;
                                            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_VceCellularCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                            InvreportCost.BInvRprtTotBil = null;
                                            db.BPtInvLstReports.Add(InvreportCost);
                                        }
                                    }
                                    #endregion

                                    #region Reject V2Fixed Trans

                                    foreach (var records in lstV2Fixed_Trans)
                                    {
                                        lesRej1 = new BSt1FbbTrans();
                                        lesRej1.Bt1LesFbTranStartDate = records.BCtTranStartDate;
                                        lesRej1.Bt1LesFbTranStartTime = records.BCtTranStartTime;
                                        lesRej1.Bt1LesFbTranDOrig = records.BCtTranOrigin;
                                        lesRej1.Bt1LesFbTranDest = records.BCtTranServDest;
                                        lesRej1.Bt1LesFbTranServType = ServTypeVce2Fixed;

                                        decimal? v2FixedTransSec = 0;
                                        if (records.BCtTranUnitUsage != null)
                                        {
                                            v2FixedTransSec = getMinutes(records.BCtTranUnitUsage);
                                        }

                                        long SecconvertToMin = Convert.ToInt64(v2FixedTransSec);
                                        lesRej1.Bt1LesFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                        lesRej1.Bt1LesFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                        lesRej1.Bt1LesFbTranTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bt1LesFbTranUsage * LES_VoiceFixed), 3, MidpointRounding.AwayFromZero);
                                        lesRej1.Bt1LesFbTranComTypes = 2;

                                        db.BSt1FbbTrans.Add(lesRej1);
                                    }

                                    // Invoice List Report Add in BInvLstReports
                                    if (lstV2Fixed_Trans.Count > 0)
                                    {
                                        InvreportCost = new BPtInvLstReport();
                                        try
                                        {
                                            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeVce2Fixed).BsPtServiceTypes;
                                        }
                                        catch { InvreportCost.BInvRprtSerType = null; }

                                        if (InvreportCost.BInvRprtSerType != null)
                                        {
                                            InvreportCost.BInvRptGuID = SecRejLes.Bt1LesGUID;
                                            //InvreportCost.fkBCus2ID = null;
                                            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_VceFixedCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                            InvreportCost.BInvRprtTotBil = null;
                                            db.BPtInvLstReports.Add(InvreportCost);
                                        }
                                    }
                                    #endregion

                                    #region Reject V2FBB Trans

                                    foreach (var records in lstV2Fixed_Trans)
                                    {
                                        lesRej1 = new BSt1FbbTrans();
                                        lesRej1.Bt1LesFbTranStartDate = records.BCtTranStartDate;
                                        lesRej1.Bt1LesFbTranStartTime = records.BCtTranStartTime;
                                        lesRej1.Bt1LesFbTranDOrig = records.BCtTranOrigin;
                                        lesRej1.Bt1LesFbTranDest = records.BCtTranServDest;
                                        lesRej1.Bt1LesFbTranServType = ServTypeVce2FBB;

                                        decimal? v2FBBTransSec = 0;
                                        if (records.BCtTranUnitUsage != null)
                                        {
                                            v2FBBTransSec = getMinutes(records.BCtTranUnitUsage);
                                        }

                                        long SecconvertToMin = Convert.ToInt64(v2FBBTransSec);
                                        lesRej1.Bt1LesFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                        lesRej1.Bt1LesFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                        lesRej1.Bt1LesFbTranTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bt1LesFbTranUsage * LES_VoiceFBB), 3, MidpointRounding.AwayFromZero);
                                        lesRej1.Bt1LesFbTranComTypes = 2;

                                        db.BSt1FbbTrans.Add(lesRej1);
                                    }

                                    // Invoice List Report Add in BInvLstReports
                                    if (lstV2Fixed_Trans.Count > 0)
                                    {
                                        InvreportCost = new BPtInvLstReport();
                                        try
                                        {
                                            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeVce2FBB).BsPtServiceTypes;
                                        }
                                        catch { InvreportCost.BInvRprtSerType = null; }

                                        if (InvreportCost.BInvRprtSerType != null)
                                        {
                                            InvreportCost.BInvRptGuID = SecRejLes.Bt1LesGUID;
                                            //InvreportCost.fkBCus2ID = null;
                                            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_VceFBBCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                            InvreportCost.BInvRprtTotBil = null;
                                            db.BPtInvLstReports.Add(InvreportCost);
                                        }
                                    }
                                    #endregion

                                    #region Reject V2Iridium

                                    foreach (var records in lstV2Iridium_Trans)
                                    {
                                        lesRej1 = new BSt1FbbTrans();
                                        lesRej1.Bt1LesFbTranStartDate = records.BCtTranStartDate;
                                        lesRej1.Bt1LesFbTranStartTime = records.BCtTranStartTime;
                                        lesRej1.Bt1LesFbTranDOrig = records.BCtTranOrigin;
                                        lesRej1.Bt1LesFbTranDest = records.BCtTranServDest;
                                        lesRej1.Bt1LesFbTranServType = LES_ServTypeVce2Iridium;

                                        decimal? v2IriTransSec = 0;
                                        if (records.BCtTranUnitUsage != null)
                                        {
                                            v2IriTransSec = getMinutes(records.BCtTranUnitUsage);
                                        }

                                        long SecconvertToMin = Convert.ToInt64(v2IriTransSec);

                                        lesRej1.Bt1LesFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                        lesRej1.Bt1LesFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                        lesRej1.Bt1LesFbTranTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bt1LesFbTranUsage * LES_VoiceIridium), 3, MidpointRounding.AwayFromZero);
                                        lesRej1.Bt1LesFbTranComTypes = 2;

                                        db.BSt1FbbTrans.Add(lesRej1);
                                    }

                                    // Invoice List Report Add in BInvLstReports
                                    if (lstV2Iridium_Trans.Count > 0)
                                    {
                                        InvreportCost = new BPtInvLstReport();
                                        try
                                        {
                                            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == LES_ServTypeVce2Iridium).BsPtServiceTypes;
                                        }
                                        catch { InvreportCost.BInvRprtSerType = null; }

                                        if (InvreportCost.BInvRprtSerType != null)
                                        {
                                            InvreportCost.BInvRptGuID = SecRejLes.Bt1LesGUID;
                                            //InvreportCost.fkBCus2ID = null;
                                            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_VceIridiumCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                            InvreportCost.BInvRprtTotBil = null;
                                            db.BPtInvLstReports.Add(InvreportCost);
                                        }
                                    }
                                    #endregion

                                    #region RejectionConditionISDN Trans

                                    foreach (var records in lstISDN_Trans)
                                    {
                                        lesRej1 = new BSt1FbbTrans();
                                        lesRej1.Bt1LesFbTranStartDate = records.BCtTranStartDate;
                                        lesRej1.Bt1LesFbTranStartTime = records.BCtTranStartTime;
                                        lesRej1.Bt1LesFbTranDOrig = records.BCtTranOrigin;
                                        lesRej1.Bt1LesFbTranDest = records.BCtTranServDest;
                                        lesRej1.Bt1LesFbTranServType = ServTypeVce2ISDN;

                                        decimal? v2ISDNTransSec = 0;
                                        if (records.BCtTranUnitUsage != null)
                                        {
                                            v2ISDNTransSec = getMinutes(records.BCtTranUnitUsage);
                                        }

                                        long SecconvertToMin = Convert.ToInt64(v2ISDNTransSec);

                                        lesRej1.Bt1LesFbTranUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                        lesRej1.Bt1LesFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                                        lesRej1.Bt1LesFbTranTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bt1LesFbTranUsage * LES_ISDN), 3, MidpointRounding.AwayFromZero);
                                        lesRej1.Bt1LesFbTranComTypes = 2;

                                        db.BSt1FbbTrans.Add(lesRej1);
                                    }

                                    // Invoice List Report Add in BInvLstReports
                                    if (lstISDN_Trans.Count > 0)
                                    {
                                        InvreportCost = new BPtInvLstReport();
                                        try
                                        {
                                            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeVce2ISDN).BsPtServiceTypes;
                                        }
                                        catch { InvreportCost.BInvRprtSerType = null; }

                                        if (InvreportCost.BInvRprtSerType != null)
                                        {
                                            InvreportCost.BInvRptGuID = SecRejLes.Bt1LesGUID;
                                            //InvreportCost.fkBCus2ID = null;
                                            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_ISDNCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                            InvreportCost.BInvRprtTotBil = null;
                                            db.BPtInvLstReports.Add(InvreportCost);
                                        }
                                    }
                                    #endregion

                                    #region Reject SMS Trans

                                    int commtype = 1;
                                    try
                                    {
                                        commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                                    }
                                    catch
                                    {
                                        commtype = db.BsMtCommunicationTypes.FirstOrDefault(t => t.ComTypes.Equals("SMS")).ComID;
                                    }

                                    foreach (var records in lstSMS_Trans)
                                    {
                                        lesRej1 = new BSt1FbbTrans();
                                        lesRej1.Bt1LesFbTranStartDate = records.BCtTranStartDate;
                                        lesRej1.Bt1LesFbTranStartTime = records.BCtTranStartTime;
                                        lesRej1.Bt1LesFbTranDOrig = records.BCtTranOrigin;
                                        lesRej1.Bt1LesFbTranDest = records.BCtTranServDest;
                                        lesRej1.Bt1LesFbTranServType = ServTypeSMS;

                                        //long SecconvertToMin = Convert.ToInt64(records.BCtBTransUsage);
                                        lesRej1.Bt1LesFbTranUsage = Convert.ToInt32(records.BCtTranUnitUsage);//(decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                                        try
                                        {
                                            lesRej1.Bt1LesFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("message")).UID;
                                        }
                                        catch
                                        {
                                            lesRej1.Bt1LesFbTranUnitType = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Message")).UID;
                                        }

                                        lesRej1.Bt1LesFbTranTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bt1LesFbTranUsage * LES_SMS), 3, MidpointRounding.AwayFromZero);
                                        lesRej1.Bt1LesFbTranComTypes = commtype;

                                        db.BSt1FbbTrans.Add(lesRej1);
                                    }

                                    // Invoice List Report Add in BInvLstReports
                                    if (lstSMS_Trans.Count > 0)
                                    {
                                        InvreportCost = new BPtInvLstReport();
                                        try
                                        {
                                            InvreportCost.BInvRprtSerType = LES_Services.FirstOrDefault(t => t.SPServDID == ServTypeSMS).BsPtServiceTypes;
                                        }
                                        catch { InvreportCost.BInvRprtSerType = null; }

                                        if (InvreportCost.BInvRprtSerType != null)
                                        {
                                            InvreportCost.BInvRptGuID = SecRejLes.Bt1LesGUID;
                                            //InvreportCost.fkBCus2ID = null;
                                            InvreportCost.BInvRprtTotCst = Math.Round(Convert.ToDecimal(CDR_SMSCostPriceCT), 2, MidpointRounding.AwayFromZero);
                                            InvreportCost.BInvRprtTotBil = null;
                                            db.BPtInvLstReports.Add(InvreportCost);
                                        }
                                    }

                                    #endregion
                                }
                            }
                            catch (Exception error)
                            {
                                Msg.AppendFormat("Error:ID:{0}, {1}", ImarSatID, error.Message.ToString());
                            }
                        }

                        stopwatch.Stop();
                        returnMsg.AppendFormat("Collectiong all record into DB took Time: {0}", stopwatch.ElapsedMilliseconds);
                        try
                        {
                            stopwatch = new Stopwatch();
                            stopwatch.Start();
                            db.SaveChanges();
                            stopwatch.Stop();
                            returnMsg.AppendFormat("Save Changes all record into DB took Time: {0}", stopwatch.ElapsedMilliseconds);
                        }
                        catch (Exception error)
                        {
                            throw error;
                        }
                    }
                }
                catch (Exception error)
                {
                    throw error;
                    //Msg.AppendFormat("Error:ID:{0}, {1}", ImarSatID, error.Message.ToString());
                }
            }
            return returnMsg.ToString();
        }

        public static List<OteSatCtVM> CsvToVMFleetOne(string csv_file_path, int lesrefid)
        {
            List<Dictionary<string, dynamic>> keyValues = new List<Dictionary<string, dynamic>>();
            List<OteSatCtVM> lstVM = new List<OteSatCtVM>();
            try
            {
                List<string> columnFields = new List<string>();
                using (Microsoft.VisualBasic.FileIO.TextFieldParser csvReader = new Microsoft.VisualBasic.FileIO.TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields = csvReader.ReadFields();
                    foreach (string column in colFields)
                    {
                        string columnName = string.Empty;
                        if (column.StartsWith(" "))
                        {
                            columnName = column.Substring(1);
                            columnName = columnName.TrimEnd();
                        }
                        columnName = column.TrimEnd();
                        columnFields.Add(System.Text.RegularExpressions.Regex.Replace(columnName, @"\t|\n|\r", ""));

                        //DataColumn datecolumn = new DataColumn(column);
                        //datecolumn.AllowDBNull = true;
                        //csvData.Columns.Add(datecolumn);
                    }
                    Dictionary<string, dynamic> dictRecord = new Dictionary<string, dynamic>();

                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        dictRecord = new Dictionary<string, dynamic>();
                        OteSatCtVM vM = new OteSatCtVM();
                        //Making empty value as null
                        for (int i = 0, colIndex = 0; i < fieldData.Length; i++, colIndex++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = string.Empty;
                            }
                            vM = storeCollection_OteSatFleetOne(columnFields[colIndex], fieldData[i], vM, lesrefid);
                            //dictRecord.Add(columnFields[colIndex], fieldData[i]);
                        }
                        //csvData.Rows.Add(fieldData);
                        if (vM.BCtTranServType != string.Empty && vM.BCtTranServType != "" && !string.IsNullOrEmpty(vM.BCtTranServType))
                            lstVM.Add(vM);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstVM;
        }

        public static OteSatCtVM storeCollection_OteSatFleetOne(string index, string value, OteSatCtVM vum, int lesrefid)
        {
            try
            {
                switch (index)
                {
                    case "Vessel Name":
                        //vum.VesselName = value;
                        vum.BCtFbbLES = "OteSat";
                        break;
                    case "MSISDN / IMSI":
                        vum.BCtFBImImsi = value;
                        break;
                    case "Sim Type":
                        if (!string.IsNullOrEmpty(value) && value.Contains("Postpaid SIM Card"))
                        {
                            vum.BCtFbLesRefCode = lesrefid;
                            //vum.BCtFbEquipmentCode = 3;
                            vum.BCtFbSatCode = 1;
                        }
                        else if (!string.IsNullOrEmpty(value) && value.Contains("OpenPort SIM Card"))
                        {
                            vum.BCtFbLesRefCode = lesrefid;
                            //vum.BCtFbEquipmentCode = 3;
                            vum.BCtFbSatCode = 1;
                        }

                        break;
                    case "Date":
                        if (!string.IsNullOrEmpty(value))
                        {
                            DateTime val;
                            bool result = DateTime.TryParse(value, out val);
                            if (result)
                                vum.BCtTranStartDate = val;
                            else
                                vum.BCtTranStartDate = null;
                        }
                        break;
                    case "Time":
                        if (!string.IsNullOrEmpty(value))
                        {
                            TimeSpan val;
                            bool result = TimeSpan.TryParse(value, out val);
                            if (result)
                                vum.BCtTranStartTime = val;
                            else
                                vum.BCtTranStartTime = null;
                        }
                        break;
                    case "Country Code":
                        vum.BCtTranOrigin = value;
                        break;
                    case "Time Zone":
                        //vum.TimeZone = value;
                        break;
                    case "Service Type":
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (value.ToLower().Contains("discount"))
                                vum.BCtTranServType = value;
                            else
                            {
                                vum.BCtTranServType = value;
                            }
                        }
                        else
                            vum.BCtTranServType = null;

                        break;
                    case "MRN":
                        //vum.MRN = value;
                        break;
                    case "Country":
                        //vum.Country = value;
                        break;
                    case "Destination":
                        vum.BCtTranServDest = value;
                        break;
                    case "Units":
                        if (!string.IsNullOrEmpty(value))
                        {
                            decimal val = 0;
                            bool result = decimal.TryParse(value, out val);
                            if (result)
                            {
                                if (!string.IsNullOrEmpty(vum.BCtTranServType) && vum.BCtTranServType.Equals("Data Call"))
                                {
                                    long dat = Convert.ToInt64(val);
                                    double ip = Converters.ConvertBytesToBites(dat);
                                    double bytes = 0;
                                    bool res = double.TryParse(ip.ToString(), out bytes);
                                    if (res)
                                    {
                                        vum.BCtTranUnitUsage = Convert.ToDecimal(ip);
                                    }
                                    else
                                    {
                                        vum.BCtTranUnitUsage = val;
                                    }
                                }
                                else
                                {
                                    vum.BCtTranUnitUsage = val;
                                }
                            }
                            else
                            {
                                vum.BCtTranUnitUsage = null;
                            }
                        }
                        break;
                    case "Units Type":
                        if (!string.IsNullOrEmpty(vum.BCtTranServType) && vum.BCtTranServType.Equals("Data Call"))
                        {
                            vum.BCtTranUnitType = "Bits";
                        }
                        else
                        {
                            vum.BCtTranUnitType = value;
                        }
                        break;
                    case "Amount Due (USD)":
                        if (!string.IsNullOrEmpty(value))
                        {
                            decimal val = 0;
                            bool result = decimal.TryParse(value, out val);
                            if (result)
                            {
                                if (!string.IsNullOrEmpty(vum.BCtTranServType) && vum.BCtTranServType.Contains("Subscription"))
                                    vum.BCtFbRecurFee = val;
                                else
                                    vum.BCtTranTotalUsage = val;
                            }
                            else
                                vum.BCtTranTotalUsage = null;
                        }
                        break;
                }
            }
            catch (Exception rer)
            {
                throw rer;
            }
            return vum;
        }

        public Tuple<Stream, string> GenreateCTExcelFleetOne(List<OteSatCtVM> lstID)
        {
            System.IO.Stream spreadsheetstream = new System.IO.MemoryStream();

            string Invoiceno = string.Empty;

            int value = 0;

            try
            {
                string imsiID = string.Empty;
                string invoiceNo = string.Empty;
                string ImarsatID = string.Empty;

                string buildingName = string.Empty;
                string street = string.Empty;
                string district = string.Empty;
                string city = string.Empty;
                string country = string.Empty;

                string VesselName = string.Empty;

                string CustomerName = string.Empty;

                BillingSystemEntities db = new BillingSystemEntities();

                DateTime dt = DateTime.Now;
                string Period = dt.ToString("MMM");
                DateTime dy = DateTime.Today;
                string currYear = dy.Year.ToString();

                string CurrPeriod = Period + '-' + currYear;
                string InvoiceFileName = invoiceNo + '_' + buildingName + '_' + VesselName;

                string pathid = Guid.NewGuid().ToString() + " " + DateTime.Now.Date.ToString("dd-MM-yyyy");

                #region WorkBook Prepartion

                XLWorkbook xLWorkbook = new XLWorkbook();
                IXLWorksheet workSheet = xLWorkbook.Worksheets.Add("Invoice" + Invoiceno);


                // Work Sheet Header
                workSheet.Cell(1, 1).Value = "ServiceProvider";
                workSheet.Cell(1, 2).Value = "BPtFBImImsi";
                workSheet.Cell(1, 3).Value = "BCtAFbContNo";
                workSheet.Cell(1, 4).Value = "BCtAFbCdrCode";
                workSheet.Cell(1, 5).Value = "BCtFbLesRefCode";
                workSheet.Cell(1, 6).Value = "BCtFbSatCode";
                workSheet.Cell(1, 7).Value = "BCtFbEquipmentCode";
                workSheet.Cell(1, 8).Value = "BCtAFbRecurFee";
                workSheet.Cell(1, 9).Value = "BCtAFbRecurFeeType";
                workSheet.Cell(1, 10).Value = "BCtBStartDate";
                workSheet.Cell(1, 11).Value = "BCtBEndDate";
                workSheet.Cell(1, 12).Value = "BCtBStartTime";
                workSheet.Cell(1, 13).Value = "BCtBOrigin";

                workSheet.Cell(1, 14).Value = "BCtAFbServType";
                workSheet.Cell(1, 15).Value = "BCtAFbServDest";
                workSheet.Cell(1, 16).Value = "BCtBTransUsage";
                workSheet.Cell(1, 17).Value = "BCtBUnits";
                workSheet.Cell(1, 18).Value = "BCtBTotalUsage";
                workSheet.Cell(1, 19).Value = "BCtBComTypes";
                workSheet.Cell(1, 20).Value = "BCtBBillIncr";
                workSheet.Cell(1, 21).Value = "BCtAFbBillNo";
                workSheet.Cell(1, 22).Value = "BCtAFbBillPeriod";
                workSheet.Cell(1, 23).Value = "BCtAFbBillDate";
                workSheet.Cell(1, 24).Value = "BCtAFbDualSim";
                workSheet.Cell(1, 25).Value = "BCtAFbContNo";
                workSheet.Cell(1, 26).Value = "BCtAFbContStart";
                workSheet.Cell(1, 27).Value = "BCtAFbContEnd";
                workSheet.Cell(1, 28).Value = "BCtAFbTerm";
                workSheet.Cell(1, 29).Value = "BCtAFbRgnFee";
                workSheet.Cell(1, 30).Value = "BCtAFbActFee";
                workSheet.Cell(1, 31).Value = "BCtAFbCardFee";


                int row = 2;
                decimal? totals = 0;
                int sno = 1;

                foreach (var ct in lstID)
                {
                    workSheet.Cell(row, 1).Value = "OteSat";
                    workSheet.Cell(row, 2).Style.NumberFormat.Format = "@";
                    workSheet.Cell(row, 2).Value = ct.BCtFBImImsi;

                    workSheet.Cell(row, 3).Value = ct.BCtFbContNo;

                    string satCode = string.Empty;
                    string EquipCode = string.Empty;
                    string LesRefCode = string.Empty;
                    workSheet.Cell(row, 4).Value = ct.BCtFbCdrCode;
                    try
                    {
                        LesRefCode = db.BsMtLESRefs.FirstOrDefault(t => t.LesRefID == ct.BCtFbLesRefCode).LESCode;
                    }
                    catch { }
                    workSheet.Cell(row, 5).Value = LesRefCode;

                    try
                    {
                        satCode = db.BsMtSatTypes.FirstOrDefault(t => t.MsSatType == ct.BCtFbSatCode).Satcode;
                    }
                    catch
                    {
                    }
                    workSheet.Cell(row, 6).Value = satCode;

                    try
                    {
                        EquipCode = db.BsMtEquipTypes.FirstOrDefault(t => t.EquipTypeID == ct.BCtFbEquipmentCode).EquipmentCode;
                    }
                    catch { }

                    workSheet.Cell(row, 7).Value = EquipCode;

                    if (ct.BCtTranServType.Contains("Subscription"))
                        workSheet.Cell(row, 8).Value = ct.BCtFbRecurFee;

                    workSheet.Cell(row, 9).Value = ct.BCtAFbRecurFeeType;

                    workSheet.Cell(row, 10).Style.NumberFormat.Format = "dd-mm-yyyy"; ;
                    workSheet.Cell(row, 10).Value = ct.BCtTranStartDate;
                    workSheet.Cell(row, 11).Style.NumberFormat.Format = "dd-mm-yyyy"; ;
                    workSheet.Cell(row, 11).Value = ct.BCtTranEndDate;

                    workSheet.Cell(row, 12).Style.NumberFormat.Format = "hh:mm:ss";
                    workSheet.Cell(row, 12).Value = ct.BCtTranStartTime;
                    workSheet.Cell(row, 13).Value = ct.BCtTranOrigin;

                    workSheet.Cell(row, 14).Value = ct.BCtTranServType;

                    workSheet.Cell(row, 15).Value = ct.BCtTranServDest;

                    workSheet.Cell(row, 16).Style.NumberFormat.Format = "0.00";
                    workSheet.Cell(row, 16).DataType = XLDataType.Number;
                    workSheet.Cell(row, 16).Value = ct.BCtTranUnitUsage;
                    workSheet.Cell(row, 17).Value = ct.BCtTranUnitType;
                    if (!ct.BCtTranServType.Contains("Subscription"))
                    {
                        workSheet.Cell(row, 18).Style.NumberFormat.Format = "0.00"; ;
                        workSheet.Cell(row, 18).DataType = XLDataType.Number;
                        workSheet.Cell(row, 18).Value = ct.BCtTranTotalUsage;
                    }
                    else
                    {
                        workSheet.Cell(row, 18).Style.NumberFormat.Format = "0.00"; ;
                        workSheet.Cell(row, 18).DataType = XLDataType.Number;
                        workSheet.Cell(row, 18).Value = 0.00;
                    }


                    workSheet.Cell(row, 19).Value = "";

                    workSheet.Cell(row, 20).Value = ct.BCtTranBillIncr;

                    workSheet.Cell(row, 21).Value = ct.BCtFbBillNo;
                    workSheet.Cell(row, 22).Value = ct.BCtFbBillPeriod;
                    workSheet.Cell(row, 23).Value = ct.BCtFbBillDate;
                    workSheet.Cell(row, 24).Value = ct.BCtFbDualSim;
                    workSheet.Cell(row, 25).Value = ct.BCtFbContNo;
                    workSheet.Cell(row, 26).Value = ct.BCtFbContStartDate;
                    workSheet.Cell(row, 27).Value = ct.BCtFbContEndDate;
                    workSheet.Cell(row, 28).Value = ct.BCtFbTerm;
                    workSheet.Cell(row, 29).Value = ct.BCtFbRgnFee;
                    workSheet.Cell(row, 30).Value = ct.BCtFbActFee;
                    workSheet.Cell(row, 31).Value = ct.BCtFbCardFee;

                    row++;

                }
                #endregion

                // Save Work sheet into workbook
                xLWorkbook.SaveAs(spreadsheetstream);
                spreadsheetstream.Position = 0;

                //InvoiceFileName = temppath + "\\" + InvoiceFileName;

                //FileStream fileStream = new FileStream(InvoiceFileName, FileMode.Create);

                return new Tuple<Stream, string>(spreadsheetstream, "CommonTableDatas.xlsx");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            throw new Exception("The master primary is not an interger, please check and update the issue.");
        }
    }

    public static class IEnumerableExtensions
    {
        public static DataTable AsDataTable<T>(this IEnumerable<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
    }
}