﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using FormUpdated.Areas.BillingSystem.Models;

namespace FormUpdated.Areas.BillingSystem
{
    public class helper_ImarSat
    {

        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;

            try
            {
                string value = cell.CellValue.InnerXml;

                if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                {
                    return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
                }
                else
                {
                    return value;
                }
            }
            catch { return string.Empty; }


        }

        public static List<OteSatCtVM> CsvToViewModel(string csv_file_path, int lesrefid)
        {
            List<Dictionary<string, dynamic>> keyValues = new List<Dictionary<string, dynamic>>();
            List<OteSatCtVM> lstVM = new List<OteSatCtVM>();
            try
            {
                List<string> columnFields = new List<string>();
                using (Microsoft.VisualBasic.FileIO.TextFieldParser csvReader = new Microsoft.VisualBasic.FileIO.TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields = csvReader.ReadFields();
                    foreach (string column in colFields)
                    {
                        string columnName = string.Empty;
                        if (column.StartsWith(" "))
                        {
                            columnName = column.Substring(1);
                            columnName = columnName.TrimEnd();
                        }
                        columnName = column.TrimEnd();
                        columnFields.Add(System.Text.RegularExpressions.Regex.Replace(columnName, @"\t|\n|\r", ""));
                    }
                    Dictionary<string, dynamic> dictRecord = new Dictionary<string, dynamic>();

                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        dictRecord = new Dictionary<string, dynamic>();
                        OteSatCtVM vM = new OteSatCtVM();
                        //Making empty value as null
                        for (int i = 0, colIndex = 0; i < fieldData.Length; i++, colIndex++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = string.Empty;
                            }
                            vM = ConvertToCollection(columnFields[colIndex], fieldData[i], vM, lesrefid);
                        }
                        if (vM.BCtTranServType != string.Empty && vM.BCtTranServType != "" && !string.IsNullOrEmpty(vM.BCtTranServType))
                            lstVM.Add(vM);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstVM;
        }

        public static OteSatCtVM ConvertToCollection(string index, string value, OteSatCtVM vum, int lesrefid)
        {
            switch (index)
            {
                case "ICCID":
                    vum.BCtFbbLES = "IMARSAT";
                    break;
                case "CDR ID":
                    vum.BCtFbCdrCode = value;
                    break;
                case "MSISDN":
                    vum.BCtFBImImsi = value;
                    break;
                case "Product":
                    if (!string.IsNullOrEmpty(value) && value.Contains("FBB"))
                    {
                        vum.BCtFbLesRefCode = lesrefid;
                        vum.BCtFbEquipmentCode = 3;
                        vum.BCtFbSatCode = 1;
                    }
                    else if (!string.IsNullOrEmpty(value) && value.Contains("Inmarsat-FleetOne"))
                    {
                        vum.BCtFbLesRefCode = lesrefid;
                        vum.BCtFbEquipmentCode = 3;
                        vum.BCtFbSatCode = 1;
                    }
                    else if (!string.IsNullOrEmpty(value) && value.Contains("Inmarsat-GSPS"))
                    {
                        vum.BCtFbLesRefCode = lesrefid;
                        vum.BCtFbEquipmentCode = 3;
                        vum.BCtFbSatCode = 1;
                    }
                    break;
                case "Session Start":
                    if (!string.IsNullOrEmpty(value))
                    {                        
                        DateTime val;
                        bool result = DateTime.TryParse(value, out val);
                        if (result)
                        {
                            var splitdatetime = helper_NSSLGlobal.splitDateTime(value);
                            vum.BCtTranStartDate = splitdatetime.Item1;
                            vum.BCtTranStartTime = splitdatetime.Item2;
                        }
                            
                        else
                            vum.BCtTranStartDate = null;
                    }
                    break;
                case "call_time":
                    if (!string.IsNullOrEmpty(value))
                    {
                        TimeSpan val;
                        bool result = TimeSpan.TryParse(value, out val);
                        if (result)
                            vum.BCtTranStartTime = val;
                        else
                            vum.BCtTranStartTime = null;
                    }
                    break;
                case "Originating Country":
                    vum.BCtTranOrigin = value;
                    break;
                case "Time Zone":
                    //vum.TimeZone = value;
                    break;
                case "Traffic Type":
                    if (!string.IsNullOrEmpty(value))
                    {
                        vum.BCtTranServType = value;
                    }
                    else
                        vum.BCtTranServType = null;

                    break;
                case "Destination Phone":
                    vum.BCtTranServDest = value;
                    break;
                case "Rounded Volume Total":
                    if (!string.IsNullOrEmpty(value))
                    {
                        decimal val = 0;
                        bool result = decimal.TryParse(value, out val);
                        if (result)
                            vum.BCtTranUnitUsage = val;
                        else
                            vum.BCtTranUnitUsage = val;
                    }
                    else
                    {
                        vum.BCtTranUnitUsage = null;
                    }

                    break;
                case "Unit Type":
                    vum.BCtTranUnitType = value;
                    break;
                case "Price Total":
                    if (!string.IsNullOrEmpty(value))
                    {
                        decimal val = 0;
                        bool result = decimal.TryParse(value, out val);
                        if (result)
                            vum.BCtTranTotalUsage = val;
                        else
                            vum.BCtTranTotalUsage = null;
                    }
                    break;
            }
            return vum;
        }

        public string ImarSattoCT(string fileName)
        {
            try
            {
                using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fs, false))
                    {
                        WorkbookPart workbookPart = doc.WorkbookPart;
                        SharedStringTablePart sstpart = workbookPart.GetPartsOfType<SharedStringTablePart>().First();
                        SharedStringTable sst = sstpart.SharedStringTable;

                        WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                        Worksheet sheet = worksheetPart.Worksheet;

                        SheetData sheetData = sheet.GetFirstChild<SheetData>();
                        IEnumerable<Row> rows = sheetData.Descendants<Row>();

                        List<string> receiptField = new List<string>();
                        string ColName = string.Empty;

                        foreach (Cell cell in rows.ElementAt(0))
                        {
                            ColName = GetCellValue(doc, cell);
                            if (ColName.StartsWith(" "))
                                ColName = ColName.Substring(1);
                            receiptField.Add(System.Text.RegularExpressions.Regex.Replace(ColName, @"\t|\n|\r", ""));
                        }

                        BillingSystemEntities billingSystemEntities = new BillingSystemEntities();

                        BCtA bCtA = null;
                        BCtB bCtB;

                        string colValue = string.Empty;

                        foreach (Row row in rows)
                        {
                            if (row.RowIndex != 1)
                            {
                                bCtA = new BCtA();
                                bCtB = new BCtB();
                                for (int i = 0, arrIndex = 0; i < receiptField.Count; i++, arrIndex++) ///row.Descendants<Cell>().Count() replaced
                                {
                                    colValue = GetCellValue(doc, row.Descendants<Cell>().ElementAt(i));

                                    switch (colValue.ToLower())
                                    {
                                        case "account period":
                                            break;
                                        case "customer a/r":
                                            break;
                                        case "customer code":
                                            break;
                                        case "customer name":
                                            break;
                                        case "invoice number":
                                            break;
                                        case "invoice date":
                                            break;
                                        case "originating country name":
                                            break;
                                        case "originating country code":
                                            break;
                                        case "les code":
                                            break;
                                        case "originating service id":
                                            break;
                                        case "originating id name":
                                            break;
                                        case "call sign":
                                            break;
                                        case "originator imsi":
                                            break;
                                        case "originator imei":
                                            break;
                                        case "cdr type":
                                            break;
                                        case "cdr start date":
                                            break;
                                        case "cdr start time":
                                            break;
                                        case "session close date":
                                            break;
                                        case "session close time":
                                            break;
                                        case "iccid":
                                            break;
                                        case "session id":
                                            break;
                                        case "destination platform":
                                            break;
                                        case "destination zone":
                                            break;
                                        case "destination country code":
                                            break;
                                        case "destination number/email id":
                                            break;
                                        case "destination country name":
                                            break;
                                        case "message id":
                                            break;
                                        case "message ref nr":
                                            break;
                                        case "billingid":
                                            break;
                                        case "customer per unit rate":
                                            break;
                                        case "list per unit rate":
                                            break;
                                        case "decimal unit rate":
                                            break;
                                        case "cdr currency":
                                            break;
                                        case "pricing scheme code":
                                            break;
                                        case "cdr value":
                                            break;
                                        case "charge":
                                            break;
                                        case "charge decimal":
                                            break;
                                        case "state/provincial tax":
                                            break;
                                        case "country tax":
                                            break;
                                        case "billable duration":
                                            break;
                                        case "billable volume":
                                            break;
                                        case "billable uom":
                                            break;
                                        case "rounded duration":
                                            break;
                                        case "rounded volume":
                                            break;
                                        case "rounded uom":
                                            break;
                                        case "units decimal":
                                            break;
                                        case "rounded uplink duration":
                                            break;
                                        case "rounded downlink duration":
                                            break;
                                        case "rounded uplink volume":
                                            break;
                                        case "rounded downlink volume":
                                            break;
                                        case "in bundle rounded duration":
                                            break;
                                        case "out bundle rounded duration":
                                            break;
                                        case "in bundle rounded vol":
                                            break;
                                        case "out bundle rounded vol":
                                            break;
                                        case "in bundle charge":
                                            break;
                                        case "out bundle charge":
                                            break;
                                        case "overage":
                                            break;
                                        case "special billing code":
                                            break;
                                        case "shortcode":
                                            break;
                                        case "ip address":
                                            break;
                                        case "cdkind":
                                            break;
                                        case "cdfaci":
                                            break;
                                        case "service code":
                                            break;
                                        case "event type":
                                            break;
                                        case "event subtype":
                                            break;
                                        case "direction":
                                            break;
                                        case "rate period":
                                            break;
                                        case "antenna size":
                                            break;
                                        case "scap/group id":
                                            break;
                                        case "package name":
                                            break;
                                        case "subscription name":
                                            break;
                                        case "spot beam":
                                            break;
                                        case "dnid member number":
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return "";
        }
    }
}