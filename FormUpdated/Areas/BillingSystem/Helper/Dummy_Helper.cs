﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data;
using System.Text.RegularExpressions;
using ClosedXML.Excel;
using FormUpdated.Areas.BillingSystem.Models;
using FormUpdated.Areas.BillingSystem.ViewModel;

namespace FormUpdated.Areas.BillingSystem.Helper
{
    public class Dummy_Helper
    {
        public static string LesName { get; set; } = "EvoSat";
        public int LesID { get; set; } = getLesID();

        public static int getLesID()
        {
            try
            {
                using (BillingSystemEntities context = new BillingSystemEntities())
                {
                    return context.BsMtServiceProviders.FirstOrDefault(t => t.ServiceProvider.Equals(LesName)).spID;
                }
            }
            catch
            {
                return 0;
            }
        }

        public ViewModel.LesRegPriceEditVM getLesRegVM(int lesregid, int cusid, int shipid, int simid)
        {
            ViewModel.LesRegPriceEditVM editVM = new ViewModel.LesRegPriceEditVM();

            using (BillingSystemEntities db = new BillingSystemEntities())
            {
                var LesReg = db.BsPtSPLesRegs.FirstOrDefault(t => t.SPLesRegID == lesregid);
                var CusName = db.BsMtCustomers.FirstOrDefault(t => t.CusID == cusid);
                var Vessel = db.BPtShips.FirstOrDefault(t => t.Ship_ID == shipid);
                var Sim = db.BptFbSims.FirstOrDefault(t => t.Sim_ID == simid);
                if (LesReg != null)
                {
                    var serviceTypes = db.BsPtSPServiceDests.Where(t => t.BPtServTfkLesRegID == LesReg.SPLesRegID).ToList();

                    editVM.LesRegID = LesReg.SPLesRegID;
                    editVM.CusID = CusName.CusID;
                    editVM.ShipID = Vessel.Ship_ID;
                    editVM.SimID = Sim.Sim_ID;
                    editVM.Customers = CusName.Customers;
                    editVM.Vessel = Vessel.BPtShipName;
                    editVM.InMarsatID = Sim.BPtFBimMsisdn;
                    editVM.IMSIID = Sim.BPtFBImImsi;
                    editVM.RecurFee = LesReg.BPtLesRgnFbRecurFee;
                    editVM.DataPlan = Convert.ToInt32(LesReg.BPtLesRgnFBDataPlanIn);
                    editVM.InBundleRate = LesReg.BPtLesRgnFbInbundleDataRate;
                    editVM.OutBundleRate = LesReg.BPtLesRgnFbOutBundleDataRate;
                    editVM.StartDate = LesReg.BsPtlesRegStartdate;
                    editVM.EndDate = LesReg.BsPtlesRegEnd;
                    editVM.BarDate = LesReg.BsPtLesRegFBBarDate;
                    editVM.BarDateLifted = LesReg.BsPtLesRegFbBarDateLifted;
                    editVM.SusDate = LesReg.BsPtLesRegFBSusDate;
                    editVM.susDateLifted = LesReg.BsPtLesRegFBSusDateLifted;
                    editVM.LayupDate = LesReg.BsPtLesRegFBLayUpDate;
                    editVM.LayupDateLifted = LesReg.BsPtLesRegFBLayUpDateLifted;

                    foreach (var lst in serviceTypes)
                    {
                        editVM.VoicetoCellular = serviceTypes.FirstOrDefault(t => t.BsPtServiceTypes.Equals("Voice  to Cellular")).BsPtServiceCostPrice;
                        editVM.VoicetoFixed = serviceTypes.FirstOrDefault(t => t.BsPtServiceTypes.Equals("Voice  to Fixed")).BsPtServiceCostPrice;
                        editVM.VoicetoFleetBroadBand = serviceTypes.FirstOrDefault(t => t.BsPtServiceTypes.Equals("Voice  to FleetBroadband")).BsPtServiceCostPrice;
                        editVM.VoicetoVoicemail = serviceTypes.FirstOrDefault(t => t.BsPtServiceTypes.Equals("Voice  to Voicemail")).BsPtServiceCostPrice;
                        editVM.VoicetoIridium = serviceTypes.FirstOrDefault(t => t.BsPtServiceTypes.Equals("Voice  to Iridium")).BsPtServiceCostPrice;
                        editVM.ISDN = serviceTypes.FirstOrDefault(t => t.BsPtServiceTypes.Equals("ISDN")).BsPtServiceCostPrice;
                        editVM.SMS = serviceTypes.FirstOrDefault(t => t.BsPtServiceTypes.Equals("SMS")).BsPtServiceCostPrice;
                        editVM.VoicetoBegan = serviceTypes.FirstOrDefault(t => t.BsPtServiceTypes.Equals("Voice to Began")).BsPtServiceCostPrice;
                        editVM.VoicetoGSPS = serviceTypes.FirstOrDefault(t => t.BsPtServiceTypes.Equals("Voice  to GSPS")).BsPtServiceCostPrice;
                        editVM.StandardIP = serviceTypes.FirstOrDefault(t => t.BsPtServiceTypes.Equals("Standard IP")).BsPtServiceCostPrice;
                    }

                }
            }
            return editVM;
        }

        public ViewModel.CusRegPriceEditVM getCusRegVM(int CusregID, int Cusid, int Shipid, int Simid)
        {
            ViewModel.CusRegPriceEditVM UpdateDB = new ViewModel.CusRegPriceEditVM();

            using (BillingSystemEntities db = new BillingSystemEntities())
            {
                var Cusregid = db.BsPtCusRegs_.FirstOrDefault(t => t.Cus_RegID == CusregID);
                var CusName = db.BsMtCustomers.FirstOrDefault(t => t.CusID == Cusid);
                var Vessel = db.BPtShips.FirstOrDefault(t => t.Ship_ID == Shipid);
                var Sim = db.BptFbSims.FirstOrDefault(t => t.Sim_ID == Simid);

                if (Cusregid != null)
                {
                    var serviceType = db.BsPtCusServiceDests.Where(t => t.BPtCusServfkCusRegID == Cusregid.Cus_RegID).ToList();

                    UpdateDB.CusRegID = Cusregid.Cus_RegID;
                    UpdateDB.CusID = CusName.CusID;
                    UpdateDB.ShipID = Vessel.Ship_ID;
                    UpdateDB.SimID = Sim.Sim_ID;
                    UpdateDB.Customer = CusName.Customers;
                    UpdateDB.Vessel = Vessel.BPtShipName;
                    UpdateDB.InmarsatID = Sim.BPtFBimMsisdn;
                    UpdateDB.ImsiID = Sim.BPtFBImImsi;
                    UpdateDB.IccID = Sim.BPtFBimIccid;
                    UpdateDB.Dataplan = Convert.ToInt32(Cusregid.BPtCusRgnFBDataPlanIn);
                    UpdateDB.Recurfee = Cusregid.BPtCusRgnFbRecurFee;
                    UpdateDB.InbundleRate = Cusregid.BPtCusRgnFbInbundleDataRate;
                    UpdateDB.OutbundleRate = Cusregid.BPtCusRgnFbOutBundleDataRate;
                    UpdateDB.StartDate = Convert.ToDateTime(Cusregid.BsPtCusRegStartdate);
                    UpdateDB.EndDate = Convert.ToDateTime(Cusregid.BsPtCusRegEnd);
                    UpdateDB.BarDate = Cusregid.BsPtCusRegFBBarDate;
                    UpdateDB.BarDateLifted = Cusregid.BsPtCusRegFbBarDateLifted;
                    UpdateDB.SusDate = Cusregid.BsPtCusRegFBSusDate;
                    UpdateDB.SusDateLifted = Cusregid.BsPtCusRegFBSusDateLifted;
                    UpdateDB.LayupDate = Cusregid.BsPtCusRegFBLayUpDate;
                    UpdateDB.LayupDateLifted = Cusregid.BsPtCusRegFBLayUpDateLifted;

                    foreach (var item in serviceType)
                    {
                        UpdateDB.VoicetoCellular = serviceType.FirstOrDefault(t => t.BsPtCusServiceTypes.Equals("Voice  to Cellular")).BsPtCusServiceSellerPrice;
                        UpdateDB.VoicetoFixed = serviceType.FirstOrDefault(t => t.BsPtCusServiceTypes.Equals("Voice  to Fixed")).BsPtCusServiceSellerPrice;
                        UpdateDB.VoicetoFleetBroadband = serviceType.FirstOrDefault(t => t.BsPtCusServiceTypes.Equals("Voice  to FleetBroadband")).BsPtCusServiceSellerPrice;
                        UpdateDB.VoicetoVoicemail = serviceType.FirstOrDefault(t => t.BsPtCusServiceTypes.Equals("Voice  to Voicemail")).BsPtCusServiceSellerPrice;
                        UpdateDB.VoicetoIridium = serviceType.FirstOrDefault(t => t.BsPtCusServiceTypes.Equals("Voice  to Iridium")).BsPtCusServiceSellerPrice;
                        UpdateDB.ISDN = serviceType.FirstOrDefault(t => t.BsPtCusServiceTypes.Equals("ISDN")).BsPtCusServiceSellerPrice;
                        UpdateDB.SMS = serviceType.FirstOrDefault(t => t.BsPtCusServiceTypes.Equals("SMS")).BsPtCusServiceSellerPrice;
                        UpdateDB.VoicetoBegan = serviceType.FirstOrDefault(t => t.BsPtCusServiceTypes.Equals("Voice to Began")).BsPtCusServiceSellerPrice;
                        UpdateDB.VoicetoGSPS = serviceType.FirstOrDefault(t => t.BsPtCusServiceTypes.Equals("Voice  to GSPS")).BsPtCusServiceSellerPrice;
                        UpdateDB.StandardIP = serviceType.FirstOrDefault(t => t.BsPtCusServiceTypes.Equals("Standard IP")).BsPtCusServiceSellerPrice;
                    }
                }
            }
            return UpdateDB;
        }

        public string NewOteSat2CT(string fileName, int lesrefID)
        {
            List<int> ctMasterID = new List<int>();
            string ErrMsg = string.Empty;
            try
            {
                BillingSystemEntities db = new BillingSystemEntities();
                List<Dictionary<string, dynamic>> pairs = GetDictfromExcel(fileName);
                var imsi = pairs.Select(ims => ims["imn/imsi"]).Distinct().ToList();
                List<BsMtLESRef> lstLesRef = db.BsMtLESRefs.ToList();
                List<BsMtSatType> lstSatType = db.BsMtSatTypes.ToList();
                List<BsFBBServiceType> lstServiceType = db.BsFBBServiceTypes.ToList();
                List<BsMtEquipType> lstEquipType = db.BsMtEquipTypes.ToList();

                foreach (var id in imsi)
                {
                    List<Dictionary<string, dynamic>> keyValues = pairs.Where(t => t["imn/imsi"].Equals(id)).ToList();

                    List<BsNewCT> lstNewCT = new List<BsNewCT>();
                    string Vessel = string.Empty;
                    foreach (Dictionary<string, dynamic> dictRecord in keyValues)
                    {
                        if (dictRecord.Count > 0)
                        {
                            if (dictRecord.ContainsKey("servicetype"))
                            {
                                Vessel = dictRecord["vesselname"];
                                string types = dictRecord["servicetype"];
                                if (!string.IsNullOrEmpty(types))
                                {
                                    BsNewCT newCT = new BsNewCT();
                                    newCT.ID = dictRecord["imn/imsi"];

                                    try
                                    {
                                        if (dictRecord.ContainsKey("system") && dictRecord["system"] != null)
                                        {
                                            string Systems = dictRecord["system"];
                                            if (Systems.Contains("-"))
                                            {
                                                var type = Systems.Split('-');
                                                try
                                                {
                                                    newCT.fkSatTypeID = lstSatType.FirstOrDefault(t => t.SatTypes.ToLower().Equals(type[0].ToLower())).MsSatType;
                                                }
                                                catch { }
                                                try
                                                {
                                                    newCT.fkEquipTypeID = lstEquipType.FirstOrDefault(t => t.Equipments.ToLower().Equals(type[1].ToLower())).EquipTypeID;
                                                }
                                                catch
                                                {
                                                    if (type[1].ToLower().Contains("fleetbroadband"))
                                                    {
                                                        newCT.fkEquipTypeID = lstEquipType.FirstOrDefault(t => t.Equipments.ToLower().Equals("inmarsat fbb 150")).EquipTypeID;
                                                    }
                                                    else if (type[1].ToLower().Contains("gsps"))
                                                    {
                                                        newCT.fkEquipTypeID = lstEquipType.FirstOrDefault(t => t.Equipments.ToLower().Equals("isat phone")).EquipTypeID;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception error)
                                    {
                                        ErrMsg += string.Format("The Imsi {0} and Vessel {1} Error: {2}.", newCT.ID, dictRecord["vesselname"], error.Message.ToString());
                                    }

                                    newCT.fkLesRefID = lesrefID; //lstLesRef.FirstOrDefault(t => t.Description.Equals("Otesat FBB")).LesRefID;
                                    try
                                    {
                                        newCT.Date = dictRecord["date"];
                                        newCT.TimeStart = dictRecord["time"];
                                    }
                                    catch (Exception error)
                                    {
                                        ErrMsg += string.Format("The Imsi {0} and Vessel {1} Error: {2}.", newCT.ID, dictRecord["vesselname"], error.Message.ToString());
                                    }

                                    //newCT.TimeEnd = dictRecord["msn/imsi"];
                                    newCT.Source = string.Empty;
                                    newCT.Destination = dictRecord["destination"];
                                    //newCT.fkSatTypeID = SatID;

                                    types = dictRecord["servicetype"];
                                    try
                                    {
                                        int serviceTypeID = 0;
                                        if (types.ToLower().Contains("subscription"))
                                        {
                                            serviceTypeID = lstServiceType.FirstOrDefault(t => t.ServiceTypes.ToLower().Contains("monthly subscription charge")).ServiceTypeID;
                                        }
                                        else
                                        {
                                            types = types.TrimStart().TrimEnd().Replace(" ", string.Empty);
                                            serviceTypeID = lstServiceType.FirstOrDefault(t => t.ServiceTypes.Replace(" ", string.Empty).ToLower().Contains(types.ToLower())).ServiceTypeID;
                                        }
                                        newCT.fkFBBServTypeID = serviceTypeID;
                                    }
                                    catch (Exception err)
                                    {
                                        ErrMsg += string.Format("The Imsi {0} and Vessel {1} Service Type {2} doesn't contain Service Type Table please Check.", newCT.ID, dictRecord["vesselname"], types);
                                    }

                                    //newCT.fkEquipTypeID = EquipID;
                                    newCT.TimeDuration = string.Empty;
                                    newCT.UnitRate = dictRecord["units"];
                                    newCT.UnityType = dictRecord["unitstype"]; ;
                                    newCT.AmountDue = dictRecord["amountdue"];
                                    db.BsNewCTs.Add(newCT);
                                }
                            }
                        }
                    }

                    ErrMsg += string.Format("The Imsi {0} and Vessel {1}, has successfully added common table.", id, Vessel);
                }
                try
                {
                    db.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
                    string errormsg = string.Empty;
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        errormsg = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors: ", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            errormsg = string.Format(" - Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw e;
                }
                catch (Exception error)
                {
                    ErrMsg += string.Format("Error:{0}", error.Message.ToString() + error.StackTrace);
                }
                return ErrMsg.ToString();
            }
            catch (Exception e)
            {
                return ErrMsg += string.Format("Error:{0}", e.Message.ToString() + e.StackTrace).ToString();
            }
        }

        public static bool IsValidTimeFormat(string input)
        {
            TimeSpan dummyOutput;
            return TimeSpan.TryParse(input, out dummyOutput);
        }

        public List<Dictionary<string, dynamic>> GetDictfromExcel(string filename)
        {
            List<Dictionary<string, dynamic>> lstPairs = new List<Dictionary<string, dynamic>>();
            try
            {
                DataTable dt = new DataTable();
                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(filename, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();

                    List<string> receiptField = new List<string>();
                    string ColName = string.Empty;
                    string colValue = string.Empty;

                    foreach (Cell cell in rows.ElementAt(0))
                    {
                        ColName = helper_NSSLGlobal.GetCellValue(spreadSheetDocument, cell);
                        if (ColName.StartsWith(" "))
                            ColName = ColName.Substring(1);
                        receiptField.Add(System.Text.RegularExpressions.Regex.Replace(ColName, @"\t|\n|\r", ""));
                    }

                    foreach (Row row in rows) //this will also include your header row...
                    {
                        if (row.RowIndex != 1)
                        {
                            Dictionary<string, dynamic> dictPairs = new Dictionary<string, dynamic>();

                            string[] tempData = new string[receiptField.Count];
                            int columnIndex = 0;

                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                // Gets the column index of the cell with data
                                int cellColumnIndex = (int)helper_NSSLGlobal.GetColumnIndexFromName(helper_NSSLGlobal.GetColumnName(cell.CellReference));
                                cellColumnIndex--; //zero based index
                                if (columnIndex < cellColumnIndex)
                                {
                                    do
                                    {
                                        tempData[columnIndex] = ""; //Insert blank data here;
                                        columnIndex++;
                                    }
                                    while (columnIndex < cellColumnIndex);
                                }
                                tempData[columnIndex] = helper_NSSLGlobal.GetCellValue(spreadSheetDocument, cell);

                                columnIndex++;
                            }

                            for (int i = 0, arrIndex = 0; i < receiptField.Count; i++, arrIndex++) ///row.Descendants<Cell>().Count() replaced
                            {
                                try
                                {
                                    colValue = tempData[arrIndex];
                                }
                                catch
                                {

                                }

                                if (receiptField[arrIndex].ToLower().Equals("vessel name") && string.IsNullOrEmpty(colValue))
                                {
                                    break;
                                }

                                switch (receiptField[arrIndex].ToLower())
                                {

                                    case "vessel name":
                                        dictPairs.Add("vesselname", colValue);
                                        break;
                                    case "imn / imsi":
                                        dictPairs.Add("imn/imsi", colValue);
                                        break;
                                    case "system":
                                        dictPairs.Add("system", colValue);
                                        break;
                                    case "date":
                                        double dates = Convert.ToDouble(colValue);
                                        DateTime dateTimes = DateTime.FromOADate(dates);
                                        dictPairs.Add("date", dateTimes);
                                        break;

                                    case "time":
                                        if (!string.IsNullOrEmpty(colValue))
                                        {
                                            try
                                            {
                                                TimeSpan? time = new TimeSpan();
                                                if (IsValidTimeFormat(colValue))
                                                {
                                                    TimeSpan tempTime;
                                                    if (!TimeSpan.TryParse(colValue, out tempTime))
                                                    {
                                                        time = tempTime;
                                                    }
                                                    else
                                                        time = TimeSpan.Parse(colValue);
                                                }
                                                else
                                                {
                                                    time = DateTime.FromOADate(Convert.ToDouble(colValue)).TimeOfDay;
                                                }

                                                dictPairs.Add("time", time);
                                            }
                                            catch (Exception rr)
                                            {
                                                dictPairs.Add("time", string.Empty);
                                            }
                                        }
                                        else
                                            dictPairs.Add("time", string.Empty);
                                        break;

                                    case "ocean region":
                                        dictPairs.Add("oceanregion", colValue);
                                        break;
                                    case "time zone":
                                        dictPairs.Add("timezone", colValue);
                                        break;

                                    case "service type":
                                        dictPairs.Add("servicetype", colValue.TrimStart().TrimEnd());
                                        break;
                                    case "mrn":
                                        dictPairs.Add("mrn", colValue);
                                        break;

                                    case "country":
                                        dictPairs.Add("country", colValue);
                                        break;
                                    case "destination":
                                        dictPairs.Add("destination", colValue);
                                        break;
                                    case "units":
                                        try
                                        {
                                            dictPairs.Add("units", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("units", Convert.ToDecimal(0));
                                        }
                                        break;
                                    case "units type":
                                        dictPairs.Add("unitstype", colValue);
                                        break;
                                    case "amount due (usd)":
                                        try
                                        {
                                            dictPairs.Add("amountdue", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("amountdue", Convert.ToDecimal(0));
                                        }

                                        break;
                                }
                            }

                            if (dictPairs.Count > 0)
                            {
                                if (!dictPairs["servicetype"].Contains("Discount"))
                                    lstPairs.Add(dictPairs);
                            }
                        }
                    }
                }
                return lstPairs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Dictionary<string, dynamic>> ExcelDataToDictionary(string filename)
        {
            List<Dictionary<string, dynamic>> lstPairs = new List<Dictionary<string, dynamic>>();
            try
            {
                DataTable dt = new DataTable();
                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(filename, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();

                    List<string> receiptField = new List<string>();
                    string ColName = string.Empty;
                    string colValue = string.Empty;

                    foreach (Cell cell in rows.ElementAt(0))
                    {
                        ColName = helper_NSSLGlobal.GetCellValue(spreadSheetDocument, cell);
                        if (ColName.StartsWith(" "))
                            ColName = ColName.Substring(1);
                        ColName = ColName.TrimEnd();
                        receiptField.Add(System.Text.RegularExpressions.Regex.Replace(ColName, @"\t|\n|\r", ""));
                    }

                    foreach (Row row in rows) //this will also include your header row...
                    {
                        if (row.RowIndex != 1)
                        {
                            Dictionary<string, dynamic> dictPairs = new Dictionary<string, dynamic>();

                            string[] tempData = new string[receiptField.Count];
                            int columnIndex = 0;

                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                // Gets the column index of the cell with data
                                int cellColumnIndex = (int)helper_NSSLGlobal.GetColumnIndexFromName(helper_NSSLGlobal.GetColumnName(cell.CellReference));
                                cellColumnIndex--; //zero based index
                                if (columnIndex < cellColumnIndex)
                                {
                                    do
                                    {
                                        tempData[columnIndex] = ""; //Insert blank data here;
                                        columnIndex++;
                                    }
                                    while (columnIndex < cellColumnIndex);
                                }
                                tempData[columnIndex] = helper_NSSLGlobal.GetCellValue(spreadSheetDocument, cell);

                                columnIndex++;
                            }

                            for (int i = 0, arrIndex = 0; i < receiptField.Count; i++, arrIndex++) ///row.Descendants<Cell>().Count() replaced
                            {
                                try
                                {
                                    colValue = tempData[arrIndex];
                                }
                                catch
                                {

                                }

                                if (receiptField[arrIndex].ToLower().Equals("msisdn") && string.IsNullOrEmpty(colValue))
                                {
                                    break;
                                }

                                dictPairs.Add(receiptField[arrIndex].ToLower(), colValue.TrimEnd());

                            }

                            if (dictPairs.Count > 0)
                            {
                                lstPairs.Add(dictPairs);
                            }
                        }
                    }
                }
                return lstPairs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Dictionary<string, dynamic>> CsvToDictionary(string csv_file_path)
        {
            List<Dictionary<string, dynamic>> keyValues = new List<Dictionary<string, dynamic>>();

            DataTable csvData = new DataTable();
            try
            {
                List<string> columnFields = new List<string>();
                using (Microsoft.VisualBasic.FileIO.TextFieldParser csvReader = new Microsoft.VisualBasic.FileIO.TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields = csvReader.ReadFields();
                    foreach (string column in colFields)
                    {
                        string columnName = string.Empty;
                        if (column.StartsWith(" "))
                        {
                            columnName = column.Substring(1);
                            columnName = columnName.TrimEnd();
                        }
                        columnName = column.TrimEnd();
                        columnFields.Add(System.Text.RegularExpressions.Regex.Replace(columnName, @"\t|\n|\r", ""));

                        //DataColumn datecolumn = new DataColumn(column);
                        //datecolumn.AllowDBNull = true;
                        //csvData.Columns.Add(datecolumn);
                    }
                    Dictionary<string, dynamic> dictRecord = new Dictionary<string, dynamic>();

                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        dictRecord = new Dictionary<string, dynamic>();
                        //Making empty value as null
                        for (int i = 0, colIndex = 0; i < fieldData.Length; i++, colIndex++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = string.Empty;
                            }
                            dictRecord.Add(columnFields[colIndex], fieldData[i]);
                        }
                        //csvData.Rows.Add(fieldData);
                        if (dictRecord.Count > 0)
                            keyValues.Add(dictRecord);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return keyValues;
        }

        public string NICDRToCT(string fileName, int lesrefID)
        {
            List<Dictionary<string, dynamic>> pairs = ExcelDataToDictionary(fileName);
            return "";
        }

        public List<int> EvoSatCDR2CT(string fileName, int LesRefID)
        {
            System.Text.StringBuilder Statusbuilder = new System.Text.StringBuilder();
            List<int> ctMasterID = new List<int>();

            List<Dictionary<string, dynamic>> pairs = new List<Dictionary<string, dynamic>>();

            if (!string.IsNullOrEmpty(fileName))
            {
                string fileExten = Path.GetExtension(fileName);
                if (fileExten.Contains(".csv"))
                {
                    pairs = CsvToDictionary(fileName);
                }
                else if (fileExten.Contains(".xlsx"))
                    pairs = ExcelDataToDictionary(fileName);
                else
                    Statusbuilder.Append("the file format is not support, please choose either .csv or .xlsx.").AppendLine();

                string ErrMsg = string.Empty;
                int ctID = 0;
                try
                {
                    BillingSystemEntities db = new BillingSystemEntities();
                    var msisdn = pairs.Select(ims => ims["msisdn"]).Distinct().ToList();
                    var Maxdate = pairs.Select(dur => dur["call_date"]).Max();
                    var Mindate = pairs.Select(dur => dur["call_date"]).Min();

                    List<BsMtSatType> lstSatType = db.BsMtSatTypes.ToList();
                    List<BsFBBServiceType> lstServiceType = db.BsFBBServiceTypes.ToList();
                    List<BsMtEquipType> lstEquipType = db.BsMtEquipTypes.ToList();
                    List<BsMtUnit> lstUnits = db.BsMtUnits.ToList();

                    //foreach (var id in msisdn)
                    //{
                    //    List<Dictionary<string, dynamic>> keyValues = pairs.Where(t => t["msisdn"].Equals(id)).ToList();

                    //    Dictionary<string, dynamic> fbbRecord = keyValues[0];

                    //    BCtA ctMaster = new BCtA();
                    //    ctMaster.BCtAfkLESID = LesID;
                    //    ctMaster.BCtAfkLESCID = db.BsPtSPContracts.FirstOrDefault(t => t.fkSPID == LesID).spcID;
                    //    var LesContractReg = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSPID == LesID);
                    //    ctMaster.BCtAfkLesRegID = LesContractReg.SPLesRegID;
                    //    string msisdnID = Convert.ToString(id);

                    //    var SimReg = db.BptFbSims.FirstOrDefault(t => t.BPtFBimMsisdn.Equals(msisdnID));
                    //    ctMaster.BCtAfkSimID = SimReg.Sim_ID;
                    //    ctMaster.BCtAFbLesCode = null;
                    //    ctMaster.BCtAFbCdrCode = fbbRecord["id"];
                    //    //ctMaster.BCMtFbSatType = 
                    //    if (Maxdate != null && Mindate != null)
                    //        ctMaster.BCtAFbServDest = Mindate + "-" + Maxdate;

                    //    ctMaster.BCtAFbServType = string.Format("FBB {0}mb Monthly Subscription Fee", LesContractReg.BPtLesRgnFBDataPlanIn);
                    //    if (fbbRecord.ContainsKey("quantity_charged") && fbbRecord["quantity_charged"] != null)
                    //        ctMaster.BCtABillIncr = Convert.ToInt32(fbbRecord["quantity_charged"]);

                    //    ctMaster.BCtAFbUOM = "days";

                    //    ctMaster.BCtAFbRecurFee = LesContractReg.BPtLesRgnFbRecurFee;

                    //    ctMaster.fkLesRefID = SimReg.fkLesRefID;
                    //    ctMaster.fkSatTypeID = SimReg.fkSatTypeID;
                    //    ctMaster.fkEquipType = SimReg.fkEquipTypeID;

                    //    //ctMaster.BCMtFbRecurFeeType ="";
                    //    //ctMaster.BCMtFbSimSingle = 0;
                    //    //ctMaster.BCMtFbSimDual = 0;
                    //    //ctMaster.BCMtFbMultiVce = 0
                    //    //ctMaster.BCMtFbContNo = "";
                    //    //ctMaster.BCMtFbCardFee = 0;
                    //    //ctMaster.BCMtFbActFee = 0;
                    //    //ctMaster.BCMtFbRgnFee = 0;
                    //    db.BCtAs.Add(ctMaster);
                    //    db.SaveChanges();
                    //    ctID = ctMaster.BCtAID;
                    //    ctMasterID.Add(ctID);

                    //    string Vessel = string.Empty;
                    //    foreach (Dictionary<string, dynamic> dictRecord in keyValues)
                    //    {
                    //        if (dictRecord.Count > 0)
                    //        {
                    //            if (dictRecord.ContainsKey("call_type") && !string.IsNullOrEmpty(dictRecord["call_type"]))
                    //            {
                    //                string callType = dictRecord["call_type"].ToLower();

                    //                BCtB ctRecord = new BCtB();
                    //                ctRecord.BCtBfkFbID = ctID;

                    //                DateTime? dateTime;
                    //                //var splitdatetime = (dateTime);
                    //                if (dictRecord.ContainsKey("call_date") && !string.IsNullOrEmpty(dictRecord["call_date"]))
                    //                {
                    //                    dateTime = Convert.ToDateTime(dictRecord["call_date"]);
                    //                    ctRecord.BCtBStartDate = dateTime;
                    //                    //ctRecord.BsCtEndtDate
                    //                }

                    //                if (dictRecord.ContainsKey("call_time") && !string.IsNullOrEmpty(dictRecord["call_time"]))
                    //                {
                    //                    try
                    //                    {
                    //                        ctRecord.BCtBStartTime = TimeSpan.Parse(dictRecord["call_time"]);
                    //                        //ctRecord.BsCtEndTiTime 
                    //                    }
                    //                    catch
                    //                    {
                    //                        throw new Exception("The call_time data is not TimeSpan data, please check and update.");
                    //                    }
                    //                }


                    //                //ctRecord.BsCtOrigion
                    //                ctRecord.BCtBDestination = dictRecord["destination"];

                    //                string ServiceTypes = dictRecord["call_type"];
                    //                try
                    //                {
                    //                    ctRecord.BCtBLesServTypeID = db.BsPtSPServiceDests.FirstOrDefault(t => t.BsPtServiceTypes.ToLower().Equals(ServiceTypes.ToLower()) && t.fkSpID == LesID).SPServDID;
                    //                }
                    //                catch (Exception rr)
                    //                {
                    //                    throw rr;
                    //                }
                    //                if (dictRecord.ContainsKey("units") && dictRecord["units"] != null)
                    //                {
                    //                    try
                    //                    {
                    //                        ctRecord.BCtBBillIncr = Convert.ToDecimal(dictRecord["units"]);
                    //                    }
                    //                    catch
                    //                    {
                    //                        ctRecord.BCtBBillIncr = 0;
                    //                    }
                    //                }

                    //                try
                    //                {
                    //                    string units = dictRecord["label"].ToLower();
                    //                    if (units.Contains("bytes"))
                    //                        units = "Bytes";
                    //                    else if (units.Contains("seconds"))
                    //                        units = "Seconds";

                    //                    ctRecord.BCtBfkUOMID = lstUnits.FirstOrDefault(t => t.Descriptions.Contains(units)).UID;
                    //                }
                    //                catch { }

                    //                if (dictRecord.ContainsKey("bill") && dictRecord["bill"] != null)
                    //                {
                    //                    try
                    //                    {
                    //                        ctRecord.BCtBTotalUsage = Convert.ToDecimal(dictRecord["bill"]);
                    //                    }
                    //                    catch
                    //                    {
                    //                        ctRecord.BCtBTotalUsage = 0;
                    //                    }
                    //                }

                    //                try
                    //                {
                    //                    ctRecord.BCtBfkCommType = Convert.ToInt32(db.BsPtSPServiceDests.FirstOrDefault(t => t.SPServDID == ctRecord.BCtBLesServTypeID).ComTypeID);

                    //                }
                    //                catch (Exception err)
                    //                {
                    //                    throw err;
                    //                }

                    //                db.BCtBs.Add(ctRecord);
                    //            }
                    //        }
                    //    }

                    //    try
                    //    {
                    //        db.SaveChanges();
                    //    }
                    //    catch (Exception rr)
                    //    {
                    //        throw rr;
                    //    }
                    //}
                }
                catch (Exception ee)
                {
                    throw ee;
                }
                return ctMasterID;
            }
            else
                return ctMasterID;
        }

        public string RejectionCondition_EvoSat(List<int> ID)
        {
            System.Text.StringBuilder Msg = new System.Text.StringBuilder();

            foreach (int id in ID)
            {
                int SimID = 0;
                decimal? CDR_RecuFee = 0;
                string ImarSatID = string.Empty;
                List<BCtB> lstCtRecord = new List<BCtB>();

                decimal? SMTS_TotalCost = 0;
                decimal? Les_TotalCostCP = 0;
                decimal? Cus_TotalCostSP = 0;

                decimal? LES_RecurFee = 0;
                decimal? LES_DataRateper1MB = 0;
                decimal? LES_VoiceFBBCellular = 0;
                decimal? LES_VoiceFBBPSTN = 0;
                decimal? LES_PrepaidServer = 0;

                int CUS_Id = 0;
                int CUS_ContractID = 0;
                int CUS_RegsID = 0;
                decimal? CUS_RecurFee = 0;
                decimal? CUS_DataRateper1MB = 0;
                decimal? CUS_VoiceFBBCellular = 0;
                decimal? CUS_VoiceFBBPSTN = 0;
                decimal? CUS_PrepaidServer = 0;

                //using (BillingSystemEntities db = new BillingSystemEntities())
                //{
                //    try
                //    {
                //        lstCtRecord = db.BCtBs.ToList();

                //        var CT_Fbs = db.BCtAs.Where(t => t.BCtAID.Equals(id)).FirstOrDefault();

                //        SimID = CT_Fbs.BCtAfkSimID;
                //        CDR_RecuFee = CT_Fbs.BCtAFbRecurFee;
                //        ImarSatID = db.BptFbSims.FirstOrDefault(t => t.Sim_ID == SimID).BPtFBimMsisdn;

                //        // Data ------------------------------------------------------------------------------------------------------------------------------
                //        int ServTypeID = 0;

                //        try
                //        {
                //            ServTypeID = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BsPtServiceTypes.Equals("Background IP")).SPServDID;
                //        }
                //        catch
                //        {
                //            ServTypeID = 0;
                //            Msg.Append("The Background IP is Registed in the LES Service Description<br />.").AppendLine();
                //        }

                //        decimal? DataUnits_CDR = lstCtRecord.AsEnumerable()
                //                                      .Where(t => t.BCtBfkCommType == 1 && t.BCtBLesServTypeID == ServTypeID && (t.BCtBfkFbID.Equals(id)))
                //                                      .GroupBy(t => t.BCtBfkFbID == id)
                //                                      .Select(t => t.Sum(s => s.BCtBTransUsage)).FirstOrDefault();
                //        DataUnits_CDR = Math.Round(Convert.ToDecimal(DataUnits_CDR), 2, MidpointRounding.AwayFromZero);

                //        decimal? CDR_DataCP = lstCtRecord.AsEnumerable()
                //                                                .Where(t => t.BCtBfkCommType == 1 && t.BCtBLesServTypeID == ServTypeID &&
                //                                                        (t.BCtBfkFbID.Equals(id)))
                //                                                .GroupBy(t => t.BCtBfkFbID == id)
                //                                                .Select(t => t.Sum(s => s.BCtBTotalUsage)).FirstOrDefault();
                //        CDR_DataCP = Math.Round(Convert.ToDecimal(CDR_DataCP), 2, MidpointRounding.AwayFromZero);

                //        SMTS_TotalCost += CDR_DataCP;
                //        //---- End Data Process

                //        // <--------------------------- Basic voice - Mobile  ----------------------------->
                //        int ServTypeMobile = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BsPtServiceTypes.Equals("Basic voice - Mobile")).SPServDID;

                //        decimal? CDR_VceFbbMobiledUintsCT = lstCtRecord.AsEnumerable()
                //                                                    .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeMobile &&
                //                                                        (t.BCtBfkFbID.Equals(id)))
                //                                                    .GroupBy(t => t.BCtBfkFbID.Equals(id))
                //                                                    .Select(t => t.Sum(s => s.BCtBTransUsage)).FirstOrDefault();

                //        if (CDR_VceFbbMobiledUintsCT != null)
                //            CDR_VceFbbMobiledUintsCT = Math.Round(Convert.ToDecimal(CDR_VceFbbMobiledUintsCT), 2, MidpointRounding.AwayFromZero);

                //        decimal? CDR_VceFbbMobileCostPriceCT = lstCtRecord.AsEnumerable()
                //                                                    .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeMobile &&
                //                                                        (t.BCtBfkFbID.Equals(id)))
                //                                                    .GroupBy(t => t.BCtBfkFbID.Equals(id))
                //                                                    .Select(t => t.Sum(s => s.BCtBTotalUsage)).FirstOrDefault();

                //        if (CDR_VceFbbMobileCostPriceCT != null)
                //            CDR_VceFbbMobileCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceFbbMobileCostPriceCT), 2, MidpointRounding.AwayFromZero);

                //        SMTS_TotalCost += CDR_VceFbbMobileCostPriceCT;
                //        // <--------------------------- End Basic voice -FBB/Cellular  ----------------------------->

                //        // <--------------------------- Basic voice -FBB/PSTN  ----------------------------->
                //        int ServTypePSTN = db.BsPtSPServiceDests.FirstOrDefault(t => t.fkSpID == LesID && t.BsPtServiceTypes.Equals("Basic Voice - PSTN")).SPServDID;

                //        decimal? CDR_VceFbbPsyndUintsCT = lstCtRecord.AsEnumerable()
                //                                                    .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypePSTN &&
                //                                                        (t.BCtBfkFbID.Equals(id)))
                //                                                    .GroupBy(t => t.BCtBfkFbID.Equals(id))
                //                                                    .Select(t => t.Sum(s => s.BCtBTransUsage)).FirstOrDefault();

                //        if (CDR_VceFbbPsyndUintsCT != null)
                //            CDR_VceFbbPsyndUintsCT = Math.Round(Convert.ToDecimal(CDR_VceFbbPsyndUintsCT), 2, MidpointRounding.AwayFromZero);

                //        decimal? CDR_VceFbbPSTNCostPriceCT = lstCtRecord.AsEnumerable()
                //                                                    .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypePSTN &&
                //                                                        (t.BCtBfkFbID.Equals(id)))
                //                                                    .GroupBy(t => t.BCtBfkFbID.Equals(id))
                //                                                    .Select(t => t.Sum(s => s.BCtBTotalUsage)).FirstOrDefault();

                //        if (CDR_VceFbbPSTNCostPriceCT != null)
                //            CDR_VceFbbPSTNCostPriceCT = Math.Round(Convert.ToDecimal(CDR_VceFbbPSTNCostPriceCT), 2, MidpointRounding.AwayFromZero);

                //        SMTS_TotalCost += CDR_VceFbbPSTNCostPriceCT;
                //        // <--------------------------- End Basic voice -FBB/PSTN  ----------------------------->

                //        try
                //        {
                //            int LES_ContractID = 0;

                //            List<BsPtSPServiceDest> lstCP = db.BsPtSPServiceDests.Where(t => t.fkSpID == LesID).ToList();
                //            try
                //            {
                //                LES_RecurFee = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSPID == LesID && t.fkSimID == SimID).BPtLesRgnFbRecurFee;
                //            }
                //            catch { LES_RecurFee = 0; }
                //            try
                //            {
                //                LES_DataRateper1MB = lstCP.FirstOrDefault(t => t.fkSpID == LesID && t.BsPtServiceTypes.Equals("Background IP")).BsPtServiceCostPrice;
                //            }
                //            catch { LES_DataRateper1MB = 0; }
                //            try
                //            {
                //                LES_VoiceFBBCellular = lstCP.FirstOrDefault(t => t.fkSpID == LesID && t.BsPtServiceTypes.Equals("Basic voice - Mobile")).BsPtServiceCostPrice;
                //            }
                //            catch { LES_VoiceFBBCellular = 0; }
                //            try
                //            {
                //                LES_VoiceFBBPSTN = lstCP.FirstOrDefault(t => t.fkSpID == LesID && t.BsPtServiceTypes.Equals("Basic Voice - PSTN")).BsPtServiceCostPrice;
                //            }
                //            catch { LES_VoiceFBBPSTN = 0; }

                //            var cusRegs = db.BsPtCusRegs_.FirstOrDefault(t => t.fkSimID == SimID);
                //            if (cusRegs != null)
                //            {
                //                CUS_Id = Convert.ToInt32(cusRegs.fkCusID);
                //                CUS_ContractID = Convert.ToInt32(cusRegs.fkCuscID);
                //                CUS_RegsID = Convert.ToInt32(cusRegs.Cus_RegID);

                //                CUS_RecurFee = cusRegs.BPtCusRgnFbRecurFee;
                //            }
                //            else
                //            {
                //                Msg.AppendFormat("This msi {0} id not registered in customer registered, please check.<br />", ImarSatID).AppendLine();
                //            }

                //            List<BsPtCusServiceDest> lstSP = db.BsPtCusServiceDests.Where(t => t.fkSpID == LesID).ToList();
                //            try
                //            {
                //                CUS_DataRateper1MB = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BsPtCusServiceTypes.Equals("Background IP")).BsPtCusServiceSellerPrice;
                //            }
                //            catch { CUS_DataRateper1MB = 0; }
                //            try
                //            {
                //                CUS_VoiceFBBCellular = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BsPtCusServiceTypes.Equals("Basic voice - Mobile")).BsPtCusServiceSellerPrice;
                //            }
                //            catch { CUS_VoiceFBBCellular = 0; }
                //            try
                //            {
                //                CUS_VoiceFBBPSTN = lstSP.FirstOrDefault(t => t.fkSpID == LesID && t.BsPtCusServiceTypes.Equals("Basic Voice - PSTN")).BsPtCusServiceSellerPrice;
                //            }
                //            catch { CUS_VoiceFBBPSTN = 0; }

                //        }
                //        catch (Exception e)
                //        {
                //            Msg.AppendFormat("Error:{0}", e.Message.ToString());
                //        }

                //        bool isRecurFeeMached = false;
                //        bool isDataMatched = false;
                //        bool isVoiceFbbCellurlarMatched = false;
                //        bool isVoiceFbbPSTNMatched = false;
                //        bool isPrepaidServerMatched = false;

                //        Les_TotalCostCP += LES_RecurFee;
                //        SMTS_TotalCost += CDR_RecuFee;

                //        if ((CDR_RecuFee < LES_RecurFee || CDR_RecuFee.Equals(LES_RecurFee))
                //                              && CDR_RecuFee < CUS_RecurFee)
                //        {
                //            isRecurFeeMached = true;
                //        }

                //        if (DataUnits_CDR != null)
                //        {
                //            long data = Convert.ToInt64(Convert.ToDecimal(DataUnits_CDR));
                //            decimal? CDR_DataTotalUsageMB = (decimal)Converters.ConvertBytesToMegabytes(data);

                //            if (CDR_DataTotalUsageMB != null)
                //                CDR_DataTotalUsageMB = Math.Round(Convert.ToDecimal(CDR_DataTotalUsageMB), 2, MidpointRounding.AwayFromZero);

                //            decimal? LES_DataCP = CDR_DataTotalUsageMB * LES_DataRateper1MB;
                //            decimal? Cus_DataSP = CDR_DataTotalUsageMB * CUS_DataRateper1MB;

                //            if (Cus_DataSP != null)
                //                Cus_DataSP = Math.Round(Convert.ToDecimal(Cus_DataSP), 2, MidpointRounding.AwayFromZero);

                //            if (LES_DataCP != null)
                //                LES_DataCP = Math.Round(Convert.ToDecimal(LES_DataCP), 2, MidpointRounding.AwayFromZero);

                //            Les_TotalCostCP += LES_DataCP;
                //            Cus_TotalCostSP += Cus_DataSP;

                //            if ((CDR_DataCP < LES_DataCP || CDR_DataCP.Equals(LES_DataCP)) && (CDR_DataCP < Cus_DataSP))
                //            {
                //                isDataMatched = true;
                //            }
                //            else
                //            {
                //                isDataMatched = false;
                //                Msg.AppendFormat("This msi id {0}: Data of Background IP data rate is not match.<br />", ImarSatID).AppendLine();
                //            }
                //        }
                //        else
                //            isDataMatched = true;

                //        if (CDR_VceFbbMobiledUintsCT != null)
                //        {
                //            double lngUnits = Convert.ToDouble(CDR_VceFbbMobiledUintsCT);
                //            decimal? CDR_VceFbbCelltotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                //            if (CDR_VceFbbCelltotalUnits_Minutes != null)
                //                CDR_VceFbbCelltotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceFbbCelltotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

                //            decimal? LES_VceFbbCellCPperMin = CDR_VceFbbCelltotalUnits_Minutes * LES_VoiceFBBCellular;
                //            decimal? CUS_VceFbbCellSPperMin = CDR_VceFbbCelltotalUnits_Minutes * CUS_VoiceFBBCellular;

                //            if (LES_VceFbbCellCPperMin != null)
                //                LES_VceFbbCellCPperMin = Math.Round(Convert.ToDecimal(LES_VceFbbCellCPperMin), 2, MidpointRounding.AwayFromZero);

                //            if (CUS_VceFbbCellSPperMin != null)
                //                CUS_VceFbbCellSPperMin = Math.Round(Convert.ToDecimal(CUS_VceFbbCellSPperMin), 2, MidpointRounding.AwayFromZero);

                //            Les_TotalCostCP += LES_VceFbbCellCPperMin;
                //            Cus_TotalCostSP += CUS_VceFbbCellSPperMin;

                //            if ((LES_VceFbbCellCPperMin.Equals(CDR_VceFbbMobileCostPriceCT) || LES_VceFbbCellCPperMin > CDR_VceFbbMobileCostPriceCT)
                //                && (LES_VceFbbCellCPperMin < CUS_VceFbbCellSPperMin))
                //            {
                //                isVoiceFbbCellurlarMatched = true;
                //            }
                //            else
                //            {
                //                isVoiceFbbCellurlarMatched = false;
                //                Msg.AppendFormat("This msi {0}: Voice of Basic 'Basic voice - Mobile' data rate is not match.<br />", ImarSatID).AppendLine();
                //            }
                //        }
                //        else
                //            isVoiceFbbCellurlarMatched = false;

                //        if (CDR_VceFbbPsyndUintsCT != null)
                //        {
                //            double lngUnits = Convert.ToDouble(CDR_VceFbbPsyndUintsCT);
                //            decimal? CDR_VceFbbPSTNtotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

                //            if (CDR_VceFbbPSTNtotalUnits_Minutes != null)
                //                CDR_VceFbbPSTNtotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_VceFbbPSTNtotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

                //            decimal? LES_VceFbbPSTNCPperMin = CDR_VceFbbPSTNtotalUnits_Minutes * LES_VoiceFBBPSTN;
                //            decimal? CUS_VceFbbPSTNSPperMin = CDR_VceFbbPSTNtotalUnits_Minutes * CUS_VoiceFBBPSTN;

                //            if (LES_VceFbbPSTNCPperMin != null)
                //                LES_VceFbbPSTNCPperMin = Math.Round(Convert.ToDecimal(LES_VceFbbPSTNCPperMin), 2, MidpointRounding.AwayFromZero);

                //            if (CUS_VceFbbPSTNSPperMin != null)
                //                CUS_VceFbbPSTNSPperMin = Math.Round(Convert.ToDecimal(CUS_VceFbbPSTNSPperMin), 2, MidpointRounding.AwayFromZero);

                //            Les_TotalCostCP += LES_VceFbbPSTNCPperMin;
                //            Cus_TotalCostSP += CUS_VceFbbPSTNSPperMin;

                //            if ((LES_VceFbbPSTNCPperMin.Equals(CDR_VceFbbPSTNCostPriceCT) || LES_VceFbbPSTNCPperMin > CDR_VceFbbPSTNCostPriceCT)
                //                && (CDR_VceFbbPSTNCostPriceCT < CUS_VceFbbPSTNSPperMin))
                //            {
                //                isVoiceFbbPSTNMatched = true;
                //            }
                //            else
                //            {
                //                isVoiceFbbPSTNMatched = false;
                //                Msg.AppendFormat("This imsi {0}: Voice of Basic 'Basic Voice - PSTN' data rate is not match.<br />", ImarSatID).AppendLine();
                //            }
                //        }
                //        else
                //            isVoiceFbbPSTNMatched = true;

                //        string InvoiceNo = string.Empty;

                //        try
                //        {
                //            InvoiceNo = db.BS2MtCusT.Max(t => t.BSMtCusInvNo);

                //            if (!string.IsNullOrEmpty(InvoiceNo))
                //            {
                //                if (InvoiceNo.Contains("SM"))
                //                {
                //                    string tempInv = InvoiceNo.Replace("SM", string.Empty);
                //                    if (int.TryParse(tempInv, out int result))
                //                    {
                //                        InvoiceNo = BillingHelpers.GenInvoice(false, result);
                //                    }
                //                }
                //            }
                //            else
                //                InvoiceNo = BillingHelpers.GenInvoice(false, 0);
                //        }
                //        catch (Exception error)
                //        {
                //            Msg.AppendFormat("MSI:{0} Error Msg: {1} Stack Trace: {2}. <br />", ImarSatID, error.Message.ToString(), error.StackTrace);
                //        }

                //        //if ((startDateLes > cdrStartDate && contractStartDateLes > cdrStartDate && barDateLes > cdrStartDate
                //        //                  && suspensionDateLes > cdrStartDate && layupDateLes > cdrStartDate) && (
                //        if (isRecurFeeMached && isDataMatched && isVoiceFbbCellurlarMatched && isVoiceFbbPSTNMatched)
                //        {
                //            if (!string.IsNullOrEmpty(InvoiceNo))
                //            {
                //                BS2MtCusT SecMtCust = new BS2MtCusT();

                //                SecMtCust.BSMtCusInvNo = InvoiceNo;
                //                SecMtCust.fkSPID = CT_Fbs.BCtAfkLESID;
                //                SecMtCust.fkCusID = CUS_Id;
                //                SecMtCust.fkCusConID = CUS_ContractID;
                //                SecMtCust.fkCusRegID = CUS_RegsID;
                //                SecMtCust.fkspcID = CT_Fbs.BCtAfkLESCID;
                //                SecMtCust.fkSimID = SimID;
                //                SecMtCust.fkLesRefID = CT_Fbs.fkLesRefID;
                //                SecMtCust.fkSatTypeID = CT_Fbs.fkSatTypeID;
                //                SecMtCust.fkEquipTypeID = CT_Fbs.fkEquipType;
                //                SecMtCust.BSMtCusLesCode = CT_Fbs.BCtAFbLesCode;
                //                SecMtCust.BSMtCusCdrCode = CT_Fbs.BCtAFbCdrCode;
                //                SecMtCust.BSMtCusSatType = null;
                //                SecMtCust.BSMtCusServDest = CT_Fbs.BCtAFbServDest;
                //                SecMtCust.BSMtCusServType = CT_Fbs.BCtAFbServType;
                //                SecMtCust.BSMtCusTransUsage = CT_Fbs.BCtAFbTransUsage;
                //                SecMtCust.BSMtCusUOM = "Months";
                //                SecMtCust.BSMtCusRecurFee = CUS_RecurFee;
                //                Cus_TotalCostSP += SecMtCust.BSMtCusRecurFee;
                //                SecMtCust.BSMtCusRecurFeeType = CT_Fbs.BCtAFbRecurFeeType;
                //                SecMtCust.BSMtCusCDRRef = CT_Fbs.BCtAFbCDRRef;
                //                SecMtCust.BSMtCusBillNo = CT_Fbs.BCtAFbBillNo;
                //                SecMtCust.BSMtCusBillPeriod = CT_Fbs.BCtAFbBillPeriod;
                //                SecMtCust.BSMtCusBillDate = CT_Fbs.BCtAFbBillDate;

                //                db.BS2MtCusT.Add(SecMtCust);
                //                db.SaveChanges();

                //                int SecFBBID = SecMtCust.BSMtCusTID;

                //                List<BCtB> lstDateCT = lstCtRecord.AsEnumerable()
                //                                     .Where(t => t.BCtBfkCommType == 1 && t.BCtBLesServTypeID == ServTypeID &&
                //                                     (t.BCtBfkFbID == id)).ToList();
                //                Bs2Sec bs2Sec;
                //                foreach (var records in lstDateCT)
                //                {
                //                    bs2Sec = new Bs2Sec();
                //                    bs2Sec.fkCTFbID = SecFBBID;
                //                    bs2Sec.BsCtStartDate = records.BCtBStartDate;
                //                    bs2Sec.BsCtStarTime = records.BCtBStartTime;
                //                    bs2Sec.BsCtOrigion = records.BCtBOrigin;
                //                    bs2Sec.BsCtDestination = records.BCtBDestination;
                //                    bs2Sec.fkSPServTypeID = records.BCtBLesServTypeID;

                //                    long dataconvertToMP = Convert.ToInt64(records.BCtBBillIncr);
                //                    bs2Sec.BsCtBillIncre = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertBytesToMegabytes(dataconvertToMP)), 2, MidpointRounding.AwayFromZero);
                //                    bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("MB")).UID;
                //                    bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtBillIncre * CUS_DataRateper1MB), 2, MidpointRounding.AwayFromZero);
                //                    bs2Sec.fkCommType = 1;

                //                    db.Bs2Sec.Add(bs2Sec);
                //                }

                //                List<BCtB> lstVceFbbCellCT = lstCtRecord.AsEnumerable()
                //                                     .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypeMobile &&
                //                                     (t.BCtBfkFbID == id)).ToList();

                //                foreach (var records in lstVceFbbCellCT)
                //                {
                //                    bs2Sec = new Bs2Sec();
                //                    bs2Sec.fkCTFbID = SecFBBID;
                //                    bs2Sec.BsCtStartDate = records.BCtBStartDate;
                //                    bs2Sec.BsCtStarTime = records.BCtBStartTime;
                //                    bs2Sec.BsCtOrigion = records.BCtBOrigin;
                //                    bs2Sec.BsCtDestination = records.BCtBDestination;
                //                    bs2Sec.fkSPServTypeID = records.BCtBLesServTypeID;

                //                    long SecconvertToMin = Convert.ToInt64(records.BCtBBillIncr);
                //                    bs2Sec.BsCtBillIncre = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                //                    bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                //                    bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtBillIncre * CUS_VoiceFBBCellular), 2, MidpointRounding.AwayFromZero);
                //                    bs2Sec.fkCommType = 2;

                //                    db.Bs2Sec.Add(bs2Sec);
                //                }

                //                List<BCtB> lstVceFbbPSTNCT = lstCtRecord.AsEnumerable()
                //                                     .Where(t => t.BCtBfkCommType == 2 && t.BCtBLesServTypeID == ServTypePSTN &&
                //                                     (t.BCtBfkFbID.Equals(id))).ToList();

                //                foreach (var records in lstVceFbbPSTNCT)
                //                {
                //                    bs2Sec = new Bs2Sec();
                //                    bs2Sec.fkCTFbID = SecFBBID;
                //                    bs2Sec.BsCtStartDate = records.BCtBStartDate;
                //                    bs2Sec.BsCtStarTime = records.BCtBStartTime;
                //                    bs2Sec.BsCtOrigion = records.BCtBOrigin;
                //                    bs2Sec.BsCtDestination = records.BCtBDestination;
                //                    bs2Sec.fkSPServTypeID = records.BCtBLesServTypeID;

                //                    long SecconvertToMin = Convert.ToInt64(records.BCtBBillIncr);
                //                    bs2Sec.BsCtBillIncre = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                //                    bs2Sec.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                //                    bs2Sec.BsCtTotalUsage = Math.Round(Convert.ToDecimal(bs2Sec.BsCtBillIncre * CUS_VoiceFBBPSTN), 2, MidpointRounding.AwayFromZero);
                //                    bs2Sec.fkCommType = 2;

                //                    db.Bs2Sec.Add(bs2Sec);
                //                }

                //                // List of Satatement 
                //                Bs2Statement statment = null;
                //                try
                //                {
                //                    statment = db.Bs2Statement.FirstOrDefault(t => t.fkBs2CusID == SecFBBID);
                //                }
                //                catch { }


                //                if (statment == null)
                //                {
                //                    statment = new Bs2Statement();

                //                    statment.fkBs2CusID = SecFBBID;
                //                    statment.fkSimID = SimID;
                //                    statment.Bs2StTotalCP = Les_TotalCostCP;
                //                    statment.Bs2StTotalSP = Cus_TotalCostSP;
                //                    statment.Bs2StSMTSCost = SMTS_TotalCost;
                //                    statment.fkSPID = CT_Fbs.BCtAfkLESID;
                //                    statment.fkCusID = CUS_Id;
                //                    statment.Bs2StIvoiceDate = DateTime.Now.Date;
                //                    statment.Bs2StDuration = CT_Fbs.BCtAFbServDest;

                //                    if (statment.Bs2StTotalCP > 0 && statment.Bs2StTotalSP > 0)
                //                    {
                //                        decimal CrossProfit = Convert.ToDecimal(statment.Bs2StTotalSP - statment.Bs2StSMTSCost);
                //                        decimal Margin = (CrossProfit / Convert.ToDecimal(statment.Bs2StTotalSP) * 100);
                //                        statment.Bs2stMargin = Margin > 0 ? Convert.ToInt32(Margin) : (int)Math.Abs(Margin);
                //                    }

                //                    db.Bs2Statement.Add(statment);
                //                }

                //                try
                //                {
                //                    db.SaveChanges();
                //                }
                //                catch { }
                //                Msg.AppendFormat("This msi {0}: Successfully stored to Invoice.<br />", ImarSatID);
                //            }
                //        }
                //        else
                //        {
                //            BS1ALessT SecRejLes = new BS1ALessT();

                //            SecRejLes.fkSPID = CT_Fbs.BCtAfkLESID;
                //            SecRejLes.fkCusID = CUS_Id;
                //            SecRejLes.fkCusConID = CUS_ContractID;
                //            SecRejLes.fkCusRegID = CUS_RegsID;
                //            SecRejLes.fkspcID = CT_Fbs.BCtAfkLESCID;
                //            SecRejLes.fkSimID = SimID;
                //            SecRejLes.fkLesRefID = CT_Fbs.fkLesRefID;
                //            SecRejLes.fkSatTypeID = CT_Fbs.fkSatTypeID;
                //            SecRejLes.fkEquipTypeID = CT_Fbs.fkEquipType;
                //            SecRejLes.BS1ALesLesCode = CT_Fbs.BCtAFbLesCode;
                //            SecRejLes.BS1ALesCdrCode = CT_Fbs.BCtAFbCdrCode;
                //            SecRejLes.BS1ALesServDest = CT_Fbs.BCtAFbServDest;
                //            SecRejLes.BS1ALesServType = CT_Fbs.BCtAFbServType;
                //            SecRejLes.BS1ALesTransUsage = CT_Fbs.BCtAFbTransUsage;
                //            SecRejLes.BS1ALesUOM = "Months";
                //            SecRejLes.BS1ALesRecurFee = LES_RecurFee;
                //            SecRejLes.BS1ALesRecurFeeType = CT_Fbs.BCtAFbRecurFeeType;
                //            SecRejLes.BS1ALesCDRRef = CT_Fbs.BCtAFbCDRRef;
                //            SecRejLes.BS1ALesBillNo = CT_Fbs.BCtAFbBillNo;
                //            SecRejLes.BS1ALesBillPeriod = CT_Fbs.BCtAFbBillPeriod;
                //            SecRejLes.BS1ALesBillDate = CT_Fbs.BCtAFbBillDate;

                //            db.BS1ALessT.Add(SecRejLes);
                //            db.SaveChanges();

                //            int SecRejID = SecRejLes.BS1ALesID;

                //            List<BCtB> lstDateCT = lstCtRecord.AsEnumerable()
                //                                 .Where(t => t.BCtBfkCommType == 1 && t.BsPtSPServiceDest.BsPtServiceTypes.Equals("Background IP") &&
                //                                 (t.BCtBfkFbID == id)).ToList();

                //            BS1LesRejLog lesRejLog = new BS1LesRejLog();
                //            lesRejLog.fkBs1LesReg = SecRejID;
                //            lesRejLog.Bs1lesRejection = Msg.ToString();
                //            lesRejLog.Bs1lesRejRemark = "";
                //            db.BS1LesRejLog.Add(lesRejLog);

                //            Bs1BLesT lesRej1;
                //            foreach (var records in lstDateCT)
                //            {
                //                lesRej1 = new Bs1BLesT();
                //                lesRej1.fkBs1BLesID = SecRejID;
                //                lesRej1.Bs1BLesStartDate = records.BCtBStartDate;
                //                lesRej1.Bs1BLesStarTime = records.BCtBStartTime;
                //                lesRej1.Bs1BLesOrigion = records.BCtBOrigin;
                //                lesRej1.Bs1BLesDestination = records.BCtBDestination;
                //                lesRej1.fkSPServTypeID = records.BCtBLesServTypeID;

                //                long dataconvertToMP = Convert.ToInt64(records.BCtBTotalUsage);
                //                lesRej1.Bs1BLesTransUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertBytesToMegabytes(dataconvertToMP)), 2, MidpointRounding.AwayFromZero);
                //                lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("MB")).UID;
                //                lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTransUsage * LES_DataRateper1MB), 2, MidpointRounding.AwayFromZero);
                //                lesRej1.fkCommType = 1;

                //                db.Bs1BLesT.Add(lesRej1);
                //            }

                //            List<BCtB> lstVceFbbCellCT = lstCtRecord.AsEnumerable()
                //                                 .Where(t => t.BCtBfkCommType == 2 && t.BsPtSPServiceDest.BsPtServiceTypes.Equals("Basic voice - Mobile") &&
                //                                 (t.BCtBfkFbID == id)).ToList();

                //            foreach (var records in lstVceFbbCellCT)
                //            {
                //                lesRej1 = new Bs1BLesT();
                //                lesRej1.fkBs1BLesID = SecRejID;
                //                lesRej1.Bs1BLesStartDate = records.BCtBStartDate;
                //                lesRej1.Bs1BLesStarTime = records.BCtBStartTime;
                //                lesRej1.Bs1BLesOrigion = records.BCtBOrigin;
                //                lesRej1.Bs1BLesDestination = records.BCtBDestination;
                //                lesRej1.fkSPServTypeID = records.BCtBLesServTypeID;

                //                long SecconvertToMin = Convert.ToInt64(records.BCtBTotalUsage);
                //                lesRej1.Bs1BLesTotalUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                //                lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                //                lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTotalUsage * LES_VoiceFBBCellular), 2, MidpointRounding.AwayFromZero);
                //                lesRej1.fkCommType = 2;

                //                db.Bs1BLesT.Add(lesRej1);
                //            }

                //            List<BCtB> lstVceFbbPSTNCT = lstCtRecord.AsEnumerable()
                //                                 .Where(t => t.BCtBfkCommType == 2 && t.BsPtSPServiceDest.BsPtServiceTypes.Equals("Basic Voice - PSTN") &&
                //                                 (t.BCtBfkFbID == id)).ToList();

                //            foreach (var records in lstVceFbbPSTNCT)
                //            {
                //                lesRej1 = new Bs1BLesT();
                //                lesRej1.fkBs1BLesID = SecRejID;
                //                lesRej1.Bs1BLesStartDate = records.BCtBStartDate;
                //                lesRej1.Bs1BLesStarTime = records.BCtBStartTime;
                //                lesRej1.Bs1BLesOrigion = records.BCtBOrigin;
                //                lesRej1.Bs1BLesDestination = records.BCtBDestination;
                //                lesRej1.fkSPServTypeID = records.BCtBLesServTypeID;

                //                long SecconvertToMin = Convert.ToInt64(records.BCtBTotalUsage);
                //                lesRej1.Bs1BLesTotalUsage = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(SecconvertToMin)), 2, MidpointRounding.AwayFromZero);
                //                lesRej1.fkUOMID = db.BsMtUnits.FirstOrDefault(t => t.Units.Equals("Min")).UID;
                //                lesRej1.Bs1BLesTotalUsage = Math.Round(Convert.ToDecimal(lesRej1.Bs1BLesTotalUsage * LES_VoiceFBBPSTN), 2, MidpointRounding.AwayFromZero);
                //                lesRej1.fkCommType = 2;

                //                db.Bs1BLesT.Add(lesRej1);
                //            }

                //            db.SaveChanges();
                //            Msg.AppendFormat("This msi {0}: Successfully stored Rejection Les price table.<br />", ImarSatID).AppendLine();
                //        }

                //    }
                //    catch (Exception error)
                //    {
                //        Msg.AppendFormat("Error:ID:{0}, {1}", ImarSatID, error.Message.ToString());
                //    }
                //}
                return Msg.ToString();
            }

            return Msg.ToString();
        }

        public string CusRegUpload(string fileName)
        {
            try
            {
                List<Dictionary<string, dynamic>> pairs = CUS_GetDictfromExcel(fileName);

                foreach (var dict in pairs)
                {
                    int cusregid = 0, fkcusid = 0, fkcusconid = 0, fksimid = 0;

                    if (dict.ContainsKey("cusregid") && dict.ContainsKey("fkcusid") && dict.ContainsKey("fkcuscid") && dict.ContainsKey("fksimid"))
                    {
                        cusregid = Convert.ToInt32(dict["cusregid"]);
                        fkcusid = Convert.ToInt32(dict["fkcusid"]);
                        fkcusconid = Convert.ToInt32(dict["fkcuscid"]);
                        fksimid = Convert.ToInt32(dict["fksimid"]);

                        string buildingName = string.Empty;
                        string street =  string.Empty;
                        string district =  string.Empty;
                        string city =  string.Empty;
                        string country =  string.Empty;

                        if (cusregid > 0 && fkcusid > 0 && fkcusconid > 0 && fksimid > 0)
                        {
                            using (BillingSystemEntities db = new BillingSystemEntities())
                            {
                                var cusreg = db.BsPtCusRegs_.FirstOrDefault(t => t.Cus_RegID == cusregid && t.fkCusID == fkcusid && t.fkCuscID == fkcusconid && t.fkSimID == fksimid);

                                if(cusreg != null)
                                {
                                    cusreg.BsPtCusRegStartdate = dict["startdate"];
                                    cusreg.BsPtCusRegEnd = dict["enddate"];
                                    if (!string.IsNullOrEmpty(dict["fbterm"]) && dict["fbterm"] != "NULL")
                                        cusreg.BsPtCusRegFBTerm = dict["fbterm"];
                                    else
                                        cusreg.BsPtCusRegFBTerm = null;

                                    if (!string.IsNullOrEmpty(dict["cardfee"]) && dict["cardfee"] != "NULL")
                                        cusreg.BsPtCusRegFBCardFee = dict["cardfee"];
                                    else
                                        cusreg.BsPtCusRegFBCardFee = null;

                                    if (!string.IsNullOrEmpty(dict["actfee"]) && dict["actfee"] != "NULL")
                                        cusreg.BsPtCusRegFBActFee = dict["actfee"];
                                    else
                                        cusreg.BsPtCusRegFBActFee = null;

                                    if (!string.IsNullOrEmpty(dict["regnfee"]) && dict["regnfee"] != "NULL")
                                        cusreg.BsPtCusRegFBRgnFee = dict["regnfee"];
                                    else
                                        cusreg.BsPtCusRegFBRgnFee = null;

                                    if (!string.IsNullOrEmpty(dict["multivce"]) && dict["multivce"] != "NULL")
                                    {
                                        if (dict["multivce"] == "0")
                                            cusreg.BPtCusRgnFBMultiVce = false;
                                        else
                                            cusreg.BPtCusRgnFBMultiVce = true;
                                    }
                                        
                                    else
                                        cusreg.BPtCusRgnFBMultiVce = false;
                                    
                                        cusreg.BPtCusRgnFBDataPlanIn = dict["dataplan"];
                                    
                                    if (!string.IsNullOrEmpty(dict["lesren"]) && dict["lesren"] != "NULL")
                                        cusreg.BPtCusRgnFbVcePlanOPtLesRgn = dict["lesren"];
                                    else
                                        cusreg.BPtCusRgnFbVcePlanOPtLesRgn = null;

                                    if (!string.IsNullOrEmpty(dict["recurfee"]) && dict["recurfee"] != "NULL")
                                        cusreg.BPtCusRgnFbRecurFee = Convert.ToDecimal(dict["recurfee"]);
                                    
                                    if (!string.IsNullOrEmpty(dict["recurfeetype"]) && dict["recurfeetype"] != "NULL")
                                        cusreg.BPtCusRgnFbRecurFeeType = dict["recurfeetype"];
                                    else
                                        cusreg.BPtCusRgnFbRecurFeeType = null;

                                    cusreg.BPtCusRgnFbInbundleDataRate = Convert.ToDecimal(dict["inrate"]);
                                    cusreg.BPtCusRgnFbOutBundleDataRate = Convert.ToDecimal(dict["outrate"]);
                                    cusreg.BsPtCusRegDataRateUnitType = Convert.ToInt32(dict["unittype"]);

                                    if (dict["bardate"] != "NULL")
                                        cusreg.BsPtCusRegFBBarDate = dict["bardate"];
                                    else
                                        cusreg.BsPtCusRegFBBarDate = dict["bardate"];
                                    if (dict["bardatel"] != "NULL")
                                        cusreg.BsPtCusRegFbBarDateLifted = dict["bardatel"];
                                    else
                                        cusreg.BsPtCusRegFbBarDateLifted = null;
                                    if (dict["susdate"] != "NULL")
                                        cusreg.BsPtCusRegFBSusDate = dict["susdate"];
                                    else
                                        cusreg.BsPtCusRegFBSusDate = null;
                                    if (dict["susdatel"] != "NULL")
                                        cusreg.BsPtCusRegFBSusDateLifted = dict["susdatel"];
                                    else
                                        cusreg.BsPtCusRegFBSusDateLifted = null;
                                    if (dict["SusMB$Lmt"] != "NULL")
                                        cusreg.BsPtCusRegFBSusMB_Lmt = dict["SusMB$Lmt"];
                                    else
                                        cusreg.BsPtCusRegFBSusMB_Lmt = null;
                                    if (dict["SusSus$Lmt"] != "NULL")
                                        cusreg.BsPtCusRegFBSusUS_Lmt = dict["SusSus$Lmt"];
                                    else
                                        cusreg.BsPtCusRegFBSusUS_Lmt = null;
                                    if (dict["laydate"] != "NULL")
                                        cusreg.BsPtCusRegFBLayUpDate = dict["laydate"];
                                    else
                                        cusreg.BsPtCusRegFBLayUpDate = null;
                                    if (dict["laydatel"] != "NULL")
                                        cusreg.BsPtCusRegFBLayUpDateLifted = dict["laydatel"];
                                    else
                                        cusreg.BsPtCusRegFBLayUpDateLifted = null;
                                    if (dict["laydateyr"] != "NULL")
                                        cusreg.BsPtCusRegFBLayUpNosPerYr = dict["laydateyr"];
                                    else
                                        cusreg.BsPtCusRegFBLayUpNosPerYr = null;
                                    if (dict["laydatep"] != "NULL")
                                        cusreg.BsPtCusRegFBLayUpPeriod = dict["laydatep"];
                                    else
                                        cusreg.BsPtCusRegFBLayUpPeriod = null;

                                    cusreg.BsPtCusAddr = dict["addres"];
                                    if (!string.IsNullOrEmpty(cusreg.BsPtCusAddr))
                                    {
                                        string[] addressparts = cusreg.BsPtCusAddr.Split(',');

                                        buildingName = addressparts.Length > 0 ? addressparts[0] : string.Empty;
                                        street = addressparts.Length > 1 ? addressparts[1] : string.Empty;
                                        district = addressparts.Length > 2 ? addressparts[2] : string.Empty;
                                        city = addressparts.Length > 3 ? addressparts[3] : string.Empty;
                                        country = addressparts.Length > 4 ? addressparts[4] : string.Empty;
                                    }
                                    cusreg.BsPtCusRegFbContNo = buildingName + fksimid;

                                    if(dict.ContainsKey("vcel") && dict["vcel"] != null)
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        try
                                        {
                                            st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        }
                                        catch(Exception rr)
                                        {
                                            st.fkSpID = LesID;
                                        }
                                        st.BsPtCusServiceTypes = "Voice  to Cellular";
                                        st.BsPtCusServiceSellerPrice = Convert.ToDecimal(dict["vcel"]);
                                        st.ComTypeID = 2;

                                        db.BsPtCusServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vcf") && dict["vcf"] != null)
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        st.BsPtCusServiceTypes = "Voice  to Fixed";
                                        st.BsPtCusServiceSellerPrice = Convert.ToDecimal(dict["vcf"]);
                                        st.ComTypeID = 2;

                                        db.BsPtCusServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vcfbb") && dict["vcfbb"] != null)
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        st.BsPtCusServiceTypes = "Voice  to FleetBroadband";
                                        st.BsPtCusServiceSellerPrice = Convert.ToDecimal(dict["vcfbb"]);
                                        st.ComTypeID = 2;

                                        db.BsPtCusServiceDests.Add(st);

                                    }
                                    
                                    if (dict.ContainsKey("vcmail") && dict["vcmail"] != null)
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        st.BsPtCusServiceTypes = "Voice  to Voicemail";
                                        st.BsPtCusServiceSellerPrice = Convert.ToDecimal(dict["vcmail"]);
                                        st.ComTypeID = 2;

                                        db.BsPtCusServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vciridi") && dict["vciridi"] != null)
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        st.BsPtCusServiceTypes = "Voice  to Iridium";
                                        st.BsPtCusServiceSellerPrice = Convert.ToDecimal(dict["vciridi"]);
                                        st.ComTypeID = 2;

                                        db.BsPtCusServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("isdn") && dict["isdn"] != null)
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        st.BsPtCusServiceTypes = "ISDN";
                                        st.BsPtCusServiceSellerPrice = Convert.ToDecimal(dict["isdn"]);
                                        st.ComTypeID = 2;

                                        db.BsPtCusServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("sms") && dict["sms"] != null)
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        st.BsPtCusServiceTypes = "SMS";
                                        st.BsPtCusServiceSellerPrice =Convert.ToDecimal(dict["sms"]);
                                        st.ComTypeID = 4;

                                        db.BsPtCusServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vcbegan") && dict["vcbegan"] != null)
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        st.BsPtCusServiceTypes = "Voice to Began";
                                        st.BsPtCusServiceSellerPrice = Convert.ToDecimal(dict["vcbegan"]);
                                        st.ComTypeID = 2;

                                        db.BsPtCusServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vcgsps") && dict["vcgsps"] != null)
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        st.BsPtCusServiceTypes = "Voice  to GSPS";
                                        st.BsPtCusServiceSellerPrice = Convert.ToDecimal(dict["vcgsps"]);
                                        st.ComTypeID = 2;

                                        db.BsPtCusServiceDests.Add(st);

                                    }
                                    
                                    try
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        st.BsPtCusServiceTypes = "Standard IP";
                                        st.BsPtCusServiceSellerPrice = cusreg.BPtCusRgnFbInbundleDataRate;
                                        st.ComTypeID = 1;

                                        db.BsPtCusServiceDests.Add(st);
                                    }
                                    catch { }

                                    db.SaveChanges();
                                }
                                else
                                {

                                    cusreg = new BsPtCusRegs_();
                                    cusreg.fkCusID = fkcusid;
                                    cusreg.fkCuscID = fkcusconid;
                                    cusreg.fkSimID = fksimid;
                                    cusreg.BsPtCusRegStartdate = dict["startdate"];
                                    cusreg.BsPtCusRegEnd = dict["enddate"];
                                    if (!string.IsNullOrEmpty(dict["fbterm"]) && dict["fbterm"] != "NULL")
                                        cusreg.BsPtCusRegFBTerm = dict["fbterm"];
                                    else
                                        cusreg.BsPtCusRegFBTerm = null;

                                    if (!string.IsNullOrEmpty(dict["cardfee"]) && dict["cardfee"] != "NULL")
                                        cusreg.BsPtCusRegFBCardFee = dict["cardfee"];
                                    else
                                        cusreg.BsPtCusRegFBCardFee = null;

                                    if (!string.IsNullOrEmpty(dict["actfee"]) && dict["actfee"] != "NULL")
                                        cusreg.BsPtCusRegFBActFee = dict["actfee"];
                                    else
                                        cusreg.BsPtCusRegFBActFee = null;

                                    if (!string.IsNullOrEmpty(dict["regnfee"]) && dict["regnfee"] != "NULL")
                                        cusreg.BsPtCusRegFBRgnFee = dict["regnfee"];
                                    else
                                        cusreg.BsPtCusRegFBRgnFee = null;

                                    if (!string.IsNullOrEmpty(dict["multivce"]) && dict["multivce"] != "NULL")
                                    {
                                        if(dict["multivce"] == "0")
                                            cusreg.BPtCusRgnFBMultiVce = false;
                                        else
                                            cusreg.BPtCusRgnFBMultiVce = true;
                                    }
                                    else
                                        cusreg.BPtCusRgnFBMultiVce = false;

                                    cusreg.BPtCusRgnFBDataPlanIn = dict["dataplan"];

                                    if (!string.IsNullOrEmpty(dict["lesren"]) && dict["lesren"] != "NULL")
                                        cusreg.BPtCusRgnFbVcePlanOPtLesRgn = dict["lesren"];
                                    else
                                        cusreg.BPtCusRgnFbVcePlanOPtLesRgn = null;

                                    if (!string.IsNullOrEmpty(dict["recurfee"]) && dict["recurfee"] != "NULL")
                                        cusreg.BPtCusRgnFbRecurFee = Convert.ToDecimal(dict["recurfee"]);

                                    if (!string.IsNullOrEmpty(dict["recurfeetype"]) && dict["recurfeetype"] != "NULL")
                                        cusreg.BPtCusRgnFbRecurFeeType = dict["recurfeetype"];
                                    else
                                        cusreg.BPtCusRgnFbRecurFeeType = null;

                                    cusreg.BPtCusRgnFbInbundleDataRate = Convert.ToDecimal(dict["inrate"]);
                                    cusreg.BPtCusRgnFbOutBundleDataRate = Convert.ToDecimal(dict["outrate"]);
                                    cusreg.BsPtCusRegDataRateUnitType = Convert.ToInt32(dict["unittype"]);

                                    if (dict["bardate"] != "NULL")
                                        cusreg.BsPtCusRegFBBarDate = dict["bardate"];
                                    else
                                        cusreg.BsPtCusRegFBBarDate = dict["bardate"];
                                    if (dict["bardatel"] != "NULL")
                                        cusreg.BsPtCusRegFbBarDateLifted = dict["bardatel"];
                                    else
                                        cusreg.BsPtCusRegFbBarDateLifted = null;
                                    if (dict["susdate"] != "NULL")
                                        cusreg.BsPtCusRegFBSusDate = dict["susdate"];
                                    else
                                        cusreg.BsPtCusRegFBSusDate = null;
                                    if (dict["susdatel"] != "NULL")
                                        cusreg.BsPtCusRegFBSusDateLifted = dict["susdatel"];
                                    else
                                        cusreg.BsPtCusRegFBSusDateLifted = null;
                                    if (dict["SusMB$Lmt"] != "NULL")
                                        cusreg.BsPtCusRegFBSusMB_Lmt = dict["SusMB$Lmt"];
                                    else
                                        cusreg.BsPtCusRegFBSusMB_Lmt = null;
                                    if (dict["SusSus$Lmt"] != "NULL")
                                        cusreg.BsPtCusRegFBSusUS_Lmt = dict["SusSus$Lmt"];
                                    else
                                        cusreg.BsPtCusRegFBSusUS_Lmt = null;
                                    if (dict["laydate"] != "NULL")
                                        cusreg.BsPtCusRegFBLayUpDate = dict["laydate"];
                                    else
                                        cusreg.BsPtCusRegFBLayUpDate = null;
                                    if (dict["laydatel"] != "NULL")
                                        cusreg.BsPtCusRegFBLayUpDateLifted = dict["laydatel"];
                                    else
                                        cusreg.BsPtCusRegFBLayUpDateLifted = null;
                                    if (dict["laydateyr"] != "NULL")
                                        cusreg.BsPtCusRegFBLayUpNosPerYr = dict["laydateyr"];
                                    else
                                        cusreg.BsPtCusRegFBLayUpNosPerYr = null;
                                    if (dict["laydatep"] != "NULL")
                                        cusreg.BsPtCusRegFBLayUpPeriod = dict["laydatep"];
                                    else
                                        cusreg.BsPtCusRegFBLayUpPeriod = null;
                                    cusreg.BsPtCusAddr = dict["addres"];
                                    if (!string.IsNullOrEmpty(cusreg.BsPtCusAddr))
                                    {
                                        string[] addressparts = cusreg.BsPtCusAddr.Split(',');

                                        buildingName = addressparts.Length > 0 ? addressparts[0] : string.Empty;
                                        street = addressparts.Length > 1 ? addressparts[1] : string.Empty;
                                        district = addressparts.Length > 2 ? addressparts[2] : string.Empty;
                                        city = addressparts.Length > 3 ? addressparts[3] : string.Empty;
                                        country = addressparts.Length > 4 ? addressparts[4] : string.Empty;
                                    }
                                    cusreg.BsPtCusRegFbContNo = buildingName+fksimid;

                                    db.BsPtCusRegs_.Add(cusreg);
                                    db.SaveChanges();

                                    if (dict.ContainsKey("vcel") && dict["vcel"] != null)
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        st.BsPtCusServiceTypes = "Voice  to Cellular";
                                        st.BsPtCusServiceSellerPrice = Convert.ToDecimal(dict["vcel"]);

                                        db.BsPtCusServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vcf") && dict["vcf"] != null)
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        st.BsPtCusServiceTypes = "Voice  to Fixed";
                                        st.BsPtCusServiceSellerPrice = Convert.ToDecimal(dict["vcf"]);

                                        db.BsPtCusServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vcfbb") && dict["vcfbb"] != null)
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        st.BsPtCusServiceTypes = "Voice  to FleetBroadband";
                                        st.BsPtCusServiceSellerPrice = Convert.ToDecimal(dict["vcfbb"]);

                                        db.BsPtCusServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vcmail") && dict["vcmail"] != null)
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        st.BsPtCusServiceTypes = "Voice  to Voicemail";
                                        st.BsPtCusServiceSellerPrice = Convert.ToDecimal(dict["vcmail"]);

                                        db.BsPtCusServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vciridi") && dict["vciridi"] != null)
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        st.BsPtCusServiceTypes = "Voice  to Iridium";
                                        st.BsPtCusServiceSellerPrice = Convert.ToDecimal(dict["vciridi"]);

                                        db.BsPtCusServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("isdn") && dict["isdn"] != null)
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        st.BsPtCusServiceTypes = "ISDN";
                                        st.BsPtCusServiceSellerPrice = Convert.ToDecimal(dict["isdn"]);

                                        db.BsPtCusServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("sms") && dict["sms"] != null)
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        st.BsPtCusServiceTypes = "SMS";
                                        st.BsPtCusServiceSellerPrice = Convert.ToDecimal(dict["sms"]);

                                        db.BsPtCusServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vcbegan") && dict["vcbegan"] != null)
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        st.BsPtCusServiceTypes = "Voice to Began";
                                        st.BsPtCusServiceSellerPrice = Convert.ToDecimal(dict["vcbegan"]);

                                        db.BsPtCusServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vcgsps") && dict["vcgsps"] != null)
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        st.BsPtCusServiceTypes = "Voice  to GSPS";
                                        st.BsPtCusServiceSellerPrice = Convert.ToDecimal(dict["vcgsps"]);

                                        db.BsPtCusServiceDests.Add(st);

                                    }

                                    try
                                    {
                                        BsPtCusServiceDest st = new BsPtCusServiceDest();
                                        st.fkCusID = cusreg.fkCusID;
                                        st.BPtCusServfkCusRegID = cusreg.Cus_RegID;
                                        st.fkSpID = db.BsPtSPLesRegs.FirstOrDefault(t => t.fkSimID == cusreg.fkSimID).fkSPID;
                                        st.BsPtCusServiceTypes = "Standard IP";
                                        st.BsPtCusServiceSellerPrice = cusreg.BPtCusRgnFbInbundleDataRate;

                                        db.BsPtCusServiceDests.Add(st);
                                    }
                                    catch { }

                                    try
                                    {
                                        db.SaveChanges();
                                    }
                                    catch (Exception error)
                                    {
                                        return error.Message.ToString();
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception error)
            {
                return error.Message.ToString() + error.StackTrace.ToString(); 
            }
            return "Successfully Upload";
        }

        public string LesRegUpload(string fileName)
        {
            try
            {
                List<Dictionary<string, dynamic>> pairs = LES_GetDictfromExcel(fileName);

                foreach (var dict in pairs)
                {
                    int lesregid = 0, fklesid = 0, fklesconid = 0, fksimid = 0;

                    if (dict.ContainsKey("splesregid") && dict.ContainsKey("fkspid") && dict.ContainsKey("fkspcid") && dict.ContainsKey("fksimid"))
                    {
                        lesregid = Convert.ToInt32(dict["splesregid"]);
                        fklesid = Convert.ToInt32(dict["fkspid"]);
                        fklesconid = Convert.ToInt32(dict["fkspcid"]);
                        fksimid = Convert.ToInt32(dict["fksimid"]);

                        string buildingName = string.Empty;
                        string street = string.Empty;
                        string district = string.Empty;
                        string city = string.Empty;
                        string country = string.Empty;

                        if (lesregid > 0 && fklesid > 0 && fklesconid > 0 && fksimid > 0)
                        {
                            using (BillingSystemEntities db = new BillingSystemEntities())
                            {
                                var lesreg = db.BsPtSPLesRegs.FirstOrDefault(t => t.SPLesRegID == lesregid && t.fkSPID == fklesid && t.fkspcID == fklesconid && t.fkSimID == fksimid);

                                if (lesreg != null)
                                {
                                    lesreg.BsPtlesRegStartdate = dict["startdate"];
                                    lesreg.BsPtlesRegEnd = dict["enddate"];
                                    if (!string.IsNullOrEmpty(dict["fbterm"]) && dict["fbterm"] != "NULL")
                                        lesreg.BsPtLesRegFBTerm = dict["fbterm"];
                                    else
                                        lesreg.BsPtLesRegFBTerm = null;

                                    if (!string.IsNullOrEmpty(dict["cardfee"]) && dict["cardfee"] != "NULL")
                                        lesreg.BsPtLesRegFBCardFee = dict["cardfee"];
                                    else
                                        lesreg.BsPtLesRegFBCardFee = null;

                                    if (!string.IsNullOrEmpty(dict["actfee"]) && dict["actfee"] != "NULL")
                                        lesreg.BsPtLesRegFBActFee = dict["actfee"];
                                    else
                                        lesreg.BsPtLesRegFBActFee = null;

                                    if (!string.IsNullOrEmpty(dict["regnfee"]) && dict["regnfee"] != "NULL")
                                        lesreg.BsPtLesRegFBRgnFee = dict["regnfee"];
                                    else
                                        lesreg.BsPtLesRegFBRgnFee = null;

                                    if (!string.IsNullOrEmpty(dict["multivce"]) && dict["multivce"] != "NULL")
                                    {
                                        if (dict["multivce"] == "0")
                                            lesreg.BPtLesRgnFBMultiVce = false;
                                        else
                                            lesreg.BPtLesRgnFBMultiVce = true;
                                    }
                                    else
                                        lesreg.BPtLesRgnFBMultiVce = false;

                                    lesreg.BPtLesRgnFBDataPlanIn = dict["dataplan"];

                                    if (!string.IsNullOrEmpty(dict["lesren"]) && dict["lesren"] != "NULL")
                                        lesreg.BPtLesRgnFbVcePlanOPtLesRgn = dict["lesren"];
                                    else
                                        lesreg.BPtLesRgnFbVcePlanOPtLesRgn = null;

                                    if (!string.IsNullOrEmpty(dict["recurfee"]) && dict["recurfee"] != "NULL")
                                        lesreg.BPtLesRgnFbRecurFee = Convert.ToDecimal(dict["recurfee"]);
                                    else
                                        lesreg.BPtLesRgnFbRecurFee = 0;

                                    if (!string.IsNullOrEmpty(dict["recurfeetype"]) && dict["recurfeetype"] != "NULL")
                                        lesreg.BPtLesRgnFbRecurFeeType = dict["recurfeetype"];
                                    else
                                        lesreg.BPtLesRgnFbRecurFeeType = null;

                                    if (!string.IsNullOrEmpty(dict["inrate"]) && dict["inrate"] != "NULL")
                                        lesreg.BPtLesRgnFbInbundleDataRate = Convert.ToDecimal(dict["inrate"]);
                                    else
                                        lesreg.BPtLesRgnFbInbundleDataRate = 0;

                                    if (!string.IsNullOrEmpty(dict["outrate"]) && dict["outrate"] != "NULL")
                                        lesreg.BPtLesRgnFbOutBundleDataRate = Convert.ToDecimal(dict["outrate"]);
                                    else
                                        lesreg.BPtLesRgnFbOutBundleDataRate = 0;

                                    if (!string.IsNullOrEmpty(dict["unittype"]) && dict["unittype"] != "NULL")
                                        lesreg.BsPtLesRegDataRateUnitType = Convert.ToInt32(dict["unittype"]);
                                    else
                                        lesreg.BsPtLesRegDataRateUnitType = Convert.ToInt32(1);

                                    if (dict["bardate"] != "NULL")
                                        lesreg.BsPtLesRegFBBarDate = dict["bardate"];
                                    else
                                        lesreg.BsPtLesRegFBBarDate = dict["bardate"];
                                    if (dict["bardatel"] != "NULL")
                                        lesreg.BsPtLesRegFbBarDateLifted = dict["bardatel"];
                                    else
                                        lesreg.BsPtLesRegFbBarDateLifted = null;
                                    if (dict["susdate"] != "NULL")
                                        lesreg.BsPtLesRegFBSusDate = dict["susdate"];
                                    else
                                        lesreg.BsPtLesRegFBSusDate = null;
                                    if (dict["susdatel"] != "NULL")
                                        lesreg.BsPtLesRegFBSusDateLifted = dict["susdatel"];
                                    else
                                        lesreg.BsPtLesRegFBSusDateLifted = null;
                                    if (dict["SusMB$Lmt"] != "NULL")
                                        lesreg.BsPtLesRegFBSusMB_Lmt = dict["SusMB$Lmt"];
                                    else
                                        lesreg.BsPtLesRegFBSusMB_Lmt = null;
                                    if (dict["SusSus$Lmt"] != "NULL")
                                        lesreg.BsPtLesRegFBSusUS_Lmt = dict["SusSus$Lmt"];
                                    else
                                        lesreg.BsPtLesRegFBSusUS_Lmt = null;
                                    if (dict["laydate"] != "NULL")
                                        lesreg.BsPtLesRegFBLayUpDate = dict["laydate"];
                                    else
                                        lesreg.BsPtLesRegFBLayUpDate = null;
                                    if (dict["laydatel"] != "NULL")
                                        lesreg.BsPtLesRegFBLayUpDateLifted = dict["laydatel"];
                                    else
                                        lesreg.BsPtLesRegFBLayUpDateLifted = null;
                                    if (dict["laydateyr"] != "NULL")
                                        lesreg.BsPtLesRegFBLayUpNosPerYr = dict["laydateyr"];
                                    else
                                        lesreg.BsPtLesRegFBLayUpNosPerYr = null;
                                    if (dict["laydatepd"] != "NULL")
                                        lesreg.BsPtLesRegFBLayUpPeriod = dict["laydatepd"];
                                    else
                                        lesreg.BsPtLesRegFBLayUpPeriod = null;

                                    if (dict.ContainsKey("vcel") && dict["vcel"] != null)
                                    {
                                        BsPtSPServiceDest st = new BsPtSPServiceDest();
                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 2;
                                        st.BsPtServiceTypes = "Voice  to Cellular";
                                        st.BsPtServiceCostPrice = Convert.ToDecimal(dict["vcel"]);
                                        st.fkUnitsType = 7;

                                        db.BsPtSPServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vcf") && dict["vcf"] != null)
                                    {
                                        BsPtSPServiceDest st = new BsPtSPServiceDest();

                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 2;
                                        st.BsPtServiceTypes = "Voice  to Fixed";
                                        st.BsPtServiceCostPrice =Convert.ToDecimal(dict["vcf"]);
                                        st.fkUnitsType = 7;

                                        db.BsPtSPServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vcfbb") && dict["vcfbb"] != null)
                                    {
                                        BsPtSPServiceDest st = new BsPtSPServiceDest();

                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 2;
                                        st.BsPtServiceTypes = "Voice  to FleetBroadband";
                                        st.BsPtServiceCostPrice = Convert.ToDecimal(dict["vcfbb"]);
                                        st.fkUnitsType = 7;

                                        db.BsPtSPServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vcmail") && dict["vcmail"] != null)
                                    {
                                        BsPtSPServiceDest st = new BsPtSPServiceDest();

                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 2;
                                        st.BsPtServiceTypes = "Voice  to Voicemail";
                                        st.BsPtServiceCostPrice = Convert.ToDecimal(dict["vcmail"]);
                                        st.fkUnitsType = 7;

                                        db.BsPtSPServiceDests.Add(st);
                                    }

                                    if (dict.ContainsKey("vciridi") && dict["vciridi"] != null)
                                    {
                                        BsPtSPServiceDest st = new BsPtSPServiceDest();

                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 2;
                                        st.BsPtServiceTypes = "Voice  to Iridium";
                                        st.BsPtServiceCostPrice = Convert.ToDecimal(dict["vciridi"]);
                                        st.fkUnitsType = 7;

                                        db.BsPtSPServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("isdn") && dict["isdn"] != null)
                                    {
                                        BsPtSPServiceDest st = new BsPtSPServiceDest();

                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 2;
                                        st.BsPtServiceTypes = "ISDN";
                                        st.BsPtServiceCostPrice = Convert.ToDecimal(dict["isdn"]);
                                        st.fkUnitsType = 7;

                                        db.BsPtSPServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("sms") && dict["sms"] != null)
                                    {
                                        BsPtSPServiceDest st = new BsPtSPServiceDest();

                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 4;
                                        st.BsPtServiceTypes = "SMS";
                                        st.BsPtServiceCostPrice = Convert.ToDecimal(dict["sms"]);
                                        st.fkUnitsType = 8;

                                        db.BsPtSPServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vcbegan") && dict["vcbegan"] != null)
                                    {
                                        BsPtSPServiceDest st = new BsPtSPServiceDest();

                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 2;
                                        st.BsPtServiceTypes = "Voice to Began";
                                        st.BsPtServiceCostPrice = Convert.ToDecimal(dict["vcbegan"]);
                                        st.fkUnitsType = 7;

                                        db.BsPtSPServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vcgsps") && dict["vcgsps"] != null)
                                    {

                                        BsPtSPServiceDest st = new BsPtSPServiceDest();

                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 2;
                                        st.BsPtServiceTypes = "Voice  to GSPS";
                                        st.BsPtServiceCostPrice = Convert.ToDecimal(dict["vcgsps"]);
                                        st.fkUnitsType = 7;

                                        db.BsPtSPServiceDests.Add(st);

                                    }

                                    try
                                    {

                                        BsPtSPServiceDest st = new BsPtSPServiceDest();

                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 1;
                                        st.BsPtServiceTypes = "Standard IP";
                                        st.BsPtServiceCostPrice = lesreg.BPtLesRgnFbInbundleDataRate;
                                        st.fkUnitsType = 2;

                                        db.BsPtSPServiceDests.Add(st);
                                    }
                                    catch { }

                                    db.SaveChanges();
                                }
                                else
                                {
                                    lesreg = new BsPtSPLesReg();
                                    lesreg.fkSPID = fklesid;
                                    lesreg.fkspcID = fklesconid;
                                    lesreg.fkSimID = fksimid;

                                    lesreg.BsPtlesRegStartdate = dict["startdate"];
                                    lesreg.BsPtlesRegEnd = dict["enddate"];
                                    if (!string.IsNullOrEmpty(dict["fbterm"]) && dict["fbterm"] != "NULL")
                                        lesreg.BsPtLesRegFBTerm = dict["fbterm"];
                                    else
                                        lesreg.BsPtLesRegFBTerm = null;

                                    if (!string.IsNullOrEmpty(dict["cardfee"]) && dict["cardfee"] != "NULL")
                                        lesreg.BsPtLesRegFBCardFee = dict["cardfee"];
                                    else
                                        lesreg.BsPtLesRegFBCardFee = null;

                                    if (!string.IsNullOrEmpty(dict["actfee"]) && dict["actfee"] != "NULL")
                                        lesreg.BsPtLesRegFBActFee = dict["actfee"];
                                    else
                                        lesreg.BsPtLesRegFBActFee = null;

                                    if (!string.IsNullOrEmpty(dict["regnfee"]) && dict["regnfee"] != "NULL")
                                        lesreg.BsPtLesRegFBRgnFee = dict["regnfee"];
                                    else
                                        lesreg.BsPtLesRegFBRgnFee = null;

                                    if (!string.IsNullOrEmpty(dict["multivce"]) && dict["multivce"] != "NULL")
                                    {
                                        if (dict["multivce"] == "0")
                                            lesreg.BPtLesRgnFBMultiVce = false;
                                        else
                                            lesreg.BPtLesRgnFBMultiVce = true;
                                    }
                                    else
                                        lesreg.BPtLesRgnFBMultiVce = false;

                                    lesreg.BPtLesRgnFBDataPlanIn = dict["dataplan"];

                                    if (!string.IsNullOrEmpty(dict["lesren"]) && dict["lesren"] != "NULL")
                                        lesreg.BPtLesRgnFbVcePlanOPtLesRgn = dict["lesren"];
                                    else
                                        lesreg.BPtLesRgnFbVcePlanOPtLesRgn = null;

                                    if (!string.IsNullOrEmpty(dict["recurfee"]) && dict["recurfee"] != "NULL")
                                        lesreg.BPtLesRgnFbRecurFee = Convert.ToDecimal(dict["recurfee"]);

                                    if (!string.IsNullOrEmpty(dict["recurfeetype"]) && dict["recurfeetype"] != "NULL")
                                        lesreg.BPtLesRgnFbRecurFeeType = dict["recurfeetype"];
                                    else
                                        lesreg.BPtLesRgnFbRecurFeeType = null;

                                    if (!string.IsNullOrEmpty(dict["inrate"]) && dict["inrate"] != "NULL")
                                        lesreg.BPtLesRgnFbInbundleDataRate = Convert.ToDecimal(dict["inrate"]);
                                    else
                                        lesreg.BPtLesRgnFbInbundleDataRate = null;

                                    if (!string.IsNullOrEmpty(dict["outrate"]) && dict["outrate"] != "NULL")
                                        lesreg.BPtLesRgnFbOutBundleDataRate = Convert.ToDecimal(dict["outrate"]);
                                    else
                                        lesreg.BPtLesRgnFbOutBundleDataRate = null;

                                    if (!string.IsNullOrEmpty(dict["unittype"]) && dict["unittype"] != "NULL")
                                        lesreg.BsPtLesRegDataRateUnitType = Convert.ToInt32(dict["unittype"]);
                                    else
                                        lesreg.BsPtLesRegDataRateUnitType = null;

                                    if (dict["bardate"] != "NULL")
                                        lesreg.BsPtLesRegFBBarDate = dict["bardate"];
                                    else
                                        lesreg.BsPtLesRegFBBarDate = dict["bardate"];
                                    if (dict["bardatel"] != "NULL")
                                        lesreg.BsPtLesRegFbBarDateLifted = dict["bardatel"];
                                    else
                                        lesreg.BsPtLesRegFbBarDateLifted = null;
                                    if (dict["susdate"] != "NULL")
                                        lesreg.BsPtLesRegFBSusDate = dict["susdate"];
                                    else
                                        lesreg.BsPtLesRegFBSusDate = null;
                                    if (dict["susdatel"] != "NULL")
                                        lesreg.BsPtLesRegFBSusDateLifted = dict["susdatel"];
                                    else
                                        lesreg.BsPtLesRegFBSusDateLifted = null;
                                    if (dict["SusMB$Lmt"] != "NULL")
                                        lesreg.BsPtLesRegFBSusMB_Lmt = dict["SusMB$Lmt"];
                                    else
                                        lesreg.BsPtLesRegFBSusMB_Lmt = null;
                                    if (dict["SusSus$Lmt"] != "NULL")
                                        lesreg.BsPtLesRegFBSusUS_Lmt = dict["SusSus$Lmt"];
                                    else
                                        lesreg.BsPtLesRegFBSusUS_Lmt = null;
                                    if (dict["laydate"] != "NULL")
                                        lesreg.BsPtLesRegFBLayUpDate = dict["laydate"];
                                    else
                                        lesreg.BsPtLesRegFBLayUpDate = null;
                                    if (dict["laydatel"] != "NULL")
                                        lesreg.BsPtLesRegFBLayUpDateLifted = dict["laydatel"];
                                    else
                                        lesreg.BsPtLesRegFBLayUpDateLifted = null;
                                    if (dict["laydateyr"] != "NULL")
                                        lesreg.BsPtLesRegFBLayUpNosPerYr = dict["laydateyr"];
                                    else
                                        lesreg.BsPtLesRegFBLayUpNosPerYr = null;
                                    if (dict["laydatepd"] != "NULL")
                                        lesreg.BsPtLesRegFBLayUpPeriod = dict["laydatepd"];
                                    else
                                        lesreg.BsPtLesRegFBLayUpPeriod = null;

                                    db.BsPtSPLesRegs.Add(lesreg);
                                    db.SaveChanges();

                                    if (dict.ContainsKey("vcel") && dict["vcel"] != null)
                                    {

                                        BsPtSPServiceDest st = new BsPtSPServiceDest();
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 2;
                                        st.BsPtServiceTypes = "Voice  to Cellular";
                                        st.BsPtServiceCostPrice = Convert.ToDecimal(dict["vcel"]);
                                        st.fkUnitsType = 7;

                                        db.BsPtSPServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vcf") && dict["vcf"] != null)
                                    {
                                        
                                        BsPtSPServiceDest st = new BsPtSPServiceDest();
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 2;
                                        st.BsPtServiceTypes = "Voice  to Fixed";
                                        st.BsPtServiceCostPrice = Convert.ToDecimal(dict["vcf"]);
                                        st.fkUnitsType = 7;

                                        db.BsPtSPServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vcfbb") && dict["vcfbb"] != null)
                                    {

                                        BsPtSPServiceDest st = new BsPtSPServiceDest();
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 2;
                                        st.BsPtServiceTypes = "Voice  to FleetBroadband";
                                        st.BsPtServiceCostPrice = Convert.ToDecimal(dict["vcfbb"]);
                                        st.fkUnitsType = 7;

                                        db.BsPtSPServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vcmail") && dict["vcmail"] != null)
                                    {
                                        
                                        BsPtSPServiceDest st = new BsPtSPServiceDest();
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 2;
                                        st.BsPtServiceTypes = "Voice  to Voicemail";
                                        st.BsPtServiceCostPrice = Convert.ToDecimal(dict["vcmail"]);
                                        st.fkUnitsType = 7;

                                        db.BsPtSPServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vciridi") && dict["vciridi"] != null)
                                    {
                                        
                                        BsPtSPServiceDest st = new BsPtSPServiceDest();
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 2;
                                        st.BsPtServiceTypes = "Voice  to Iridium";
                                        st.BsPtServiceCostPrice = Convert.ToDecimal(dict["vciridi"]);
                                        st.fkUnitsType = 7;

                                        db.BsPtSPServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("isdn") && dict["isdn"] != null)
                                    {
                                        
                                        BsPtSPServiceDest st = new BsPtSPServiceDest();
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 2;
                                        st.BsPtServiceTypes = "ISDN";
                                        st.BsPtServiceCostPrice = Convert.ToDecimal(dict["isdn"]);
                                        st.fkUnitsType = 7;

                                        db.BsPtSPServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("sms") && dict["sms"] != null)
                                    {
                                        
                                        BsPtSPServiceDest st = new BsPtSPServiceDest();
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 4;
                                        st.BsPtServiceTypes = "SMS";
                                        st.BsPtServiceCostPrice = Convert.ToDecimal(dict["sms"]);
                                        st.fkUnitsType = 8;

                                        db.BsPtSPServiceDests.Add(st);

                                    }

                                    if (dict.ContainsKey("vcbegan") && dict["vcbegan"] != null)
                                    {
                                        
                                        BsPtSPServiceDest st = new BsPtSPServiceDest();
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 2;
                                        st.BsPtServiceTypes = "Voice to Began";
                                        st.BsPtServiceCostPrice = Convert.ToDecimal(dict["vcbegan"]);
                                        st.fkUnitsType = 7;

                                        db.BsPtSPServiceDests.Add(st);


                                    }

                                    if (dict.ContainsKey("vcgsps") && dict["vcgsps"] != null)
                                    {
                                        
                                        BsPtSPServiceDest st = new BsPtSPServiceDest();
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 2;
                                        st.BsPtServiceTypes = "Voice  to GSPS";
                                        st.BsPtServiceCostPrice = Convert.ToDecimal(dict["vcgsps"]);
                                        st.fkUnitsType = 7;

                                        db.BsPtSPServiceDests.Add(st);

                                    }

                                    try
                                    {
                                        BsPtSPServiceDest st = new BsPtSPServiceDest();
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.fkSpID = fklesid;
                                        st.BPtServTfkLesRegID = lesreg.SPLesRegID;
                                        st.ComTypeID = 1;
                                        st.BsPtServiceTypes = "Standard IP";
                                        st.BsPtServiceCostPrice = lesreg.BPtLesRgnFbInbundleDataRate;
                                        st.fkUnitsType = 2;

                                        db.BsPtSPServiceDests.Add(st);
                                    }
                                    catch { }

                                    try
                                    {
                                        db.SaveChanges();
                                    }
                                    catch(Exception error)
                                    {
                                        return error.Message.ToString();
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception error)
            {
                return error.Message.ToString();
            }
            return "Suseess";
        }

        public List<Dictionary<string, dynamic>> CUS_GetDictfromExcel(string filename)
        {
            List<Dictionary<string, dynamic>> lstPairs = new List<Dictionary<string, dynamic>>();
            try
            {
                DataTable dt = new DataTable();
                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(filename, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();

                    List<string> receiptField = new List<string>();
                    string ColName = string.Empty;
                    string colValue = string.Empty;

                    foreach (Cell cell in rows.ElementAt(0))
                    {
                        ColName = helper_NSSLGlobal.GetCellValue(spreadSheetDocument, cell);
                        if (ColName.StartsWith(" "))
                            ColName = ColName.Substring(1);
                        receiptField.Add(System.Text.RegularExpressions.Regex.Replace(ColName, @"\t|\n|\r", ""));
                    }

                    foreach (Row row in rows) //this will also include your header row...
                    {
                        if (row.RowIndex != 1)
                        {
                            Dictionary<string, dynamic> dictPairs = new Dictionary<string, dynamic>();

                            string[] tempData = new string[receiptField.Count];
                            int columnIndex = 0;

                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                // Gets the column index of the cell with data
                                int cellColumnIndex = (int)helper_NSSLGlobal.GetColumnIndexFromName(helper_NSSLGlobal.GetColumnName(cell.CellReference));
                                cellColumnIndex--; //zero based index
                                if (columnIndex < cellColumnIndex)
                                {
                                    do
                                    {
                                        tempData[columnIndex] = ""; //Insert blank data here;
                                        columnIndex++;
                                    }
                                    while (columnIndex < cellColumnIndex);
                                }
                                tempData[columnIndex] = helper_NSSLGlobal.GetCellValue(spreadSheetDocument, cell);

                                columnIndex++;
                            }

                            for (int i = 0, arrIndex = 0; i < receiptField.Count; i++, arrIndex++) ///row.Descendants<Cell>().Count() replaced
                            {
                                try
                                {
                                    colValue = tempData[arrIndex];
                                }
                                catch
                                {

                                }

                                if (receiptField[arrIndex].Equals("Cus_RegID") && string.IsNullOrEmpty(colValue))
                                {
                                    break;
                                }

                                switch (receiptField[arrIndex])
                                {

                                    case "Cus_RegID":
                                        dictPairs.Add("cusregid", colValue);
                                        break;
                                    case "fkCusID":
                                        dictPairs.Add("fkcusid", colValue);
                                        break;
                                    case "fkCuscID":
                                        dictPairs.Add("fkcuscid", colValue);
                                        break;
                                    case "fkSimID":
                                        dictPairs.Add("fksimid", colValue);
                                        break;
                                    case "IMSI":
                                        dictPairs.Add("imsi", colValue);
                                        break;
                                    case "BsPtCusRegFbContNo":
                                        dictPairs.Add("contrno", colValue);
                                        break;
                                    case "BsPtCusRegStartdate":
                                        double dates = Convert.ToDouble(colValue);
                                        DateTime dateTimes = DateTime.FromOADate(dates);
                                        dictPairs.Add("startdate", dateTimes);
                                        break;
                                    case "BsPtCusRegEnd":
                                        double dateEnd = Convert.ToDouble(colValue);
                                        DateTime dateTimesEnd = DateTime.FromOADate(dateEnd);
                                        dictPairs.Add("enddate", dateTimesEnd);
                                        break;
                                    case "BsPtCusRegFbTerm":
                                        dictPairs.Add("fbterm", colValue);
                                        break;
                                    case "BsPtCusRegFBCardFee":
                                        dictPairs.Add("cardfee", colValue);
                                        break;
                                    case "BsPtCusRegFbActFee":
                                        dictPairs.Add("actfee", colValue);
                                        break;
                                    case "BsPtCusRegFbRgnfee":
                                        dictPairs.Add("regnfee", colValue);
                                        break;
                                    case "BPtCusRgnFbMultiVce":
                                        dictPairs.Add("multivce", colValue);
                                        break;
                                    case "BPtCusRgnFBDataPlanIn":
                                        dictPairs.Add("dataplan", colValue);
                                        break;
                                    case "BPtCusRgnFbVcePlanOPtLesRgn":
                                        dictPairs.Add("lesren", colValue);
                                        break;
                                    case "BPtCusRgnFbRecurFee":
                                        dictPairs.Add("recurfee", colValue);
                                        break;
                                    case "BPtCusRgnFbRecurFeeType":
                                        dictPairs.Add("recurfeetype", colValue);
                                        break;
                                    case "BPtCusRgnFbInbundleDataRate":
                                        dictPairs.Add("inrate", colValue);
                                        break;
                                    case "BPtCusRgnFbOutbundleDataRate":
                                        dictPairs.Add("outrate", colValue);
                                        break;
                                    case "BsPtCusRegDataRateUnitType":
                                        dictPairs.Add("unittype", colValue);
                                        break;
                                    case "BsPtCusRegFBBarDate":
                                        if (colValue != "NULL" && !string.IsNullOrEmpty(colValue))
                                        {
                                            double dateBar = Convert.ToDouble(colValue);
                                            DateTime dateTimesBar = DateTime.FromOADate(dateBar);
                                            dictPairs.Add("bardate", dateTimesBar);
                                        }
                                        else
                                            dictPairs.Add("bardate", null);

                                        break;
                                    case "BsPtCusRegFBBarDateLifted":
                                        if (colValue != "NULL" && !string.IsNullOrEmpty(colValue))
                                        {
                                            double dateBarl = Convert.ToDouble(colValue);
                                            DateTime dateTimesBarl = DateTime.FromOADate(dateBarl);
                                            dictPairs.Add("bardatel", dateTimesBarl);
                                        }
                                        else
                                            dictPairs.Add("bardatel", null);

                                        break;

                                    case "BsPtCusRegFBSusDate":
                                        if (colValue != "NULL" && !string.IsNullOrEmpty(colValue))
                                        {
                                            double datesus = Convert.ToDouble(colValue);
                                            DateTime dateTimessus = DateTime.FromOADate(datesus);
                                            dictPairs.Add("susdate", dateTimessus);
                                        }
                                        else
                                            dictPairs.Add("susdate", null);

                                        break;
                                    case "BsPtCusRegFBSusDateLifted":
                                        if (colValue != "NULL" && !string.IsNullOrEmpty(colValue))
                                        {
                                            double datesusl = Convert.ToDouble(colValue);
                                            DateTime dateTimessusl = DateTime.FromOADate(datesusl);
                                            dictPairs.Add("susdatel", dateTimessusl);
                                        }
                                        else
                                            dictPairs.Add("susdatel", null);

                                        break;
                                    case "BsPtCusRegFBSusMB$Lmt":
                                        dictPairs.Add("SusMB$Lmt", colValue);
                                        break;
                                    case "BsPtCusRegFBSusUS$Lmt":
                                        dictPairs.Add("SusSus$Lmt", colValue);
                                        break;
                                    case "BsPtCusRegFBLayUpDate":
                                        if (colValue != "NULL" && !string.IsNullOrEmpty(colValue))
                                        {
                                            double dateslay = Convert.ToDouble(colValue);
                                            DateTime dateTimeslay = DateTime.FromOADate(dateslay);
                                            dictPairs.Add("laydate", dateTimeslay);
                                        }
                                        else
                                            dictPairs.Add("laydate", null);

                                        break;
                                    case "BsPtCusRegFBLayUpDateLifted":
                                        if (colValue != "NULL" && !string.IsNullOrEmpty(colValue))
                                        {
                                            double dateslayl = Convert.ToDouble(colValue);
                                            DateTime dateTimeslayl = DateTime.FromOADate(dateslayl);
                                            dictPairs.Add("laydatel", dateTimeslayl);
                                        }
                                        else
                                            dictPairs.Add("laydatel", null);

                                        break;
                                    case "BsPtCusRegFBLayUpPeriod":
                                        if (colValue != "NULL" && !string.IsNullOrEmpty(colValue))
                                        {
                                            double dateslayp = Convert.ToDouble(colValue);
                                            DateTime dateTimeslayp = DateTime.FromOADate(dateslayp);
                                            dictPairs.Add("laydatep", dateTimeslayp);
                                        }
                                        else
                                            dictPairs.Add("laydatep", null);

                                        break;
                                    case "BsPtCusRegFBLayUpNosPerYr":
                                        dictPairs.Add("laydateyr", colValue);
                                        break;
                                    case "BsPtCusAddr":
                                        dictPairs.Add("addres", colValue);
                                        break;

                                    case "Voice to cellular":
                                        try
                                        {
                                            dictPairs.Add("vcel", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("vcel", Convert.ToDecimal(0));
                                        }
                                        break;

                                    case "Voice to fixed":
                                        try
                                        {
                                            dictPairs.Add("vcf", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("vcf", Convert.ToDecimal(0));
                                        }
                                        break;
                                    case "Voice to FBB":
                                        try
                                        {
                                            dictPairs.Add("vcfbb", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("vcfbb", Convert.ToDecimal(0));
                                        }
                                        break;
                                    case "V2Voice mail":
                                        try
                                        {
                                            dictPairs.Add("vcmail", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("vcmail", Convert.ToDecimal(0));
                                        }
                                        break;
                                    case "V2 Iridium":
                                        try
                                        {
                                            dictPairs.Add("vciridi", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("vciridi", Convert.ToDecimal(0));
                                        }
                                        break;
                                    case "ISDN":
                                        try
                                        {
                                            dictPairs.Add("isdn", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("isdn", Convert.ToDecimal(0));
                                        }
                                        break;
                                    case "SMS":
                                        try
                                        {
                                            dictPairs.Add("sms", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("sms", Convert.ToDecimal(0));
                                        }
                                        break;
                                    case "V2Began":
                                        try
                                        {
                                            dictPairs.Add("vcbegan", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("vcbegan", Convert.ToDecimal(0));
                                        }
                                        break;

                                    case "V2GSPS":
                                        try
                                        {
                                            dictPairs.Add("vcgsps", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("vcgsps", Convert.ToDecimal(0));
                                        }
                                        break;
                                }
                            }

                            if (dictPairs.Count > 0)
                            {
                                lstPairs.Add(dictPairs);
                            }
                        }
                    }
                }
                return lstPairs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Dictionary<string, dynamic>> LES_GetDictfromExcel(string filename)
        {
            List<Dictionary<string, dynamic>> lstPairs = new List<Dictionary<string, dynamic>>();
            try
            {
                DataTable dt = new DataTable();
                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(filename, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();

                    List<string> receiptField = new List<string>();
                    string ColName = string.Empty;
                    string colValue = string.Empty;

                    foreach (Cell cell in rows.ElementAt(0))
                    {
                        ColName = helper_NSSLGlobal.GetCellValue(spreadSheetDocument, cell);
                        if (ColName.StartsWith(" "))
                            ColName = ColName.Substring(1);
                        receiptField.Add(System.Text.RegularExpressions.Regex.Replace(ColName, @"\t|\n|\r", ""));
                    }

                    foreach (Row row in rows) //this will also include your header row...
                    {
                        if (row.RowIndex != 1)
                        {
                            Dictionary<string, dynamic> dictPairs = new Dictionary<string, dynamic>();

                            string[] tempData = new string[receiptField.Count];
                            int columnIndex = 0;

                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                // Gets the column index of the cell with data
                                int cellColumnIndex = (int)helper_NSSLGlobal.GetColumnIndexFromName(helper_NSSLGlobal.GetColumnName(cell.CellReference));
                                cellColumnIndex--; //zero based index
                                if (columnIndex < cellColumnIndex)
                                {
                                    do
                                    {
                                        tempData[columnIndex] = ""; //Insert blank data here;
                                        columnIndex++;
                                    }
                                    while (columnIndex < cellColumnIndex);
                                }
                                tempData[columnIndex] = helper_NSSLGlobal.GetCellValue(spreadSheetDocument, cell);

                                columnIndex++;
                            }

                            for (int i = 0, arrIndex = 0; i < receiptField.Count; i++, arrIndex++) ///row.Descendants<Cell>().Count() replaced
                            {
                                try
                                {
                                    colValue = tempData[arrIndex];
                                }
                                catch
                                {

                                }

                                if (receiptField[arrIndex].ToLower().Equals("Cus_RegID") && string.IsNullOrEmpty(colValue))
                                {
                                    break;
                                }

                                switch (receiptField[arrIndex])
                                {

                                    case "SPLesRegID":
                                        dictPairs.Add("splesregid", colValue);
                                        break;
                                    case "fkSPID":
                                        dictPairs.Add("fkspid", colValue);
                                        break;
                                    case "fkspcID":
                                        dictPairs.Add("fkspcid", colValue);
                                        break;
                                    case "fkSimID":
                                        dictPairs.Add("fksimid", colValue);
                                        break;
                                    case "IMSI":
                                        dictPairs.Add("imsi", colValue);
                                        break;
                                    case "BsPtlesRegFbContNo":
                                        dictPairs.Add("contrno", colValue);
                                        break;
                                    case "BsPtlesRegStartdate":
                                        double dates = Convert.ToDouble(colValue);
                                        DateTime dateTimes = DateTime.FromOADate(dates);
                                        dictPairs.Add("startdate", dateTimes);
                                        break;
                                    case "BsPtlesRegEnd":
                                        double dateEnd = Convert.ToDouble(colValue);
                                        DateTime dateTimesEnd = DateTime.FromOADate(dateEnd);
                                        dictPairs.Add("enddate", dateTimesEnd);
                                        break;
                                    case "BsPtLesRegFBTerm":
                                        dictPairs.Add("fbterm", colValue);
                                        break;
                                    case "BsPtLesRegFBCardFee":
                                        dictPairs.Add("cardfee", colValue);
                                        break;
                                    case "BsPtLesRegFBActFee":
                                        dictPairs.Add("actfee", colValue);
                                        break;
                                    case "BsPtLesRegFBRgnFee":
                                        dictPairs.Add("regnfee", colValue);
                                        break;
                                    case "BPtLesRgnFBMultiVce":
                                        dictPairs.Add("multivce", colValue);
                                        break;
                                    case "BPtLesRgnFbDataPlanln":
                                        dictPairs.Add("dataplan", colValue);
                                        break;
                                    case "BPtLesRgnFbVcePlanOPtLesRgn":
                                        dictPairs.Add("lesren", colValue);
                                        break;
                                    case "BPtLesRgnFbRecurFee":
                                        dictPairs.Add("recurfee", colValue);
                                        break;
                                    case "BPtLesRgnFbRecurFeeType":
                                        dictPairs.Add("recurfeetype", colValue);
                                        break;
                                    case "BPtLesRgnFbInbundleDataRate":
                                        dictPairs.Add("inrate", colValue);
                                        break;
                                    case "BPtLesRgnFbOutbundleDataRate":
                                        dictPairs.Add("outrate", colValue);
                                        break;
                                    case "BPtLesRegDataRateUnitType":
                                        dictPairs.Add("unittype", colValue);
                                        break;
                                    case "BPtLesRegFBBarDate":
                                        if(colValue != "NULL" && !string.IsNullOrEmpty(colValue))
                                        {
                                            double dateBar = Convert.ToDouble(colValue);
                                            DateTime dateTimesBar = DateTime.FromOADate(dateBar);
                                            dictPairs.Add("bardate", dateTimesBar);
                                        }
                                        else
                                            dictPairs.Add("bardate", null);

                                        break;
                                    case "BPtLesRegFbBarDateLifted":                                        
                                        if (colValue != "NULL" && !string.IsNullOrEmpty(colValue))
                                        {
                                            double dateBarl = Convert.ToDouble(colValue);
                                            DateTime dateTimesBarl = DateTime.FromOADate(dateBarl);
                                            dictPairs.Add("bardatel", dateTimesBarl);
                                        }
                                        else
                                            dictPairs.Add("bardatel", null);

                                        break;

                                    case "BPtLesRegFbSusDate":                                        
                                        if (colValue != "NULL" && !string.IsNullOrEmpty(colValue))
                                        {
                                            double datesus = Convert.ToDouble(colValue);
                                            DateTime dateTimessus = DateTime.FromOADate(datesus);
                                            dictPairs.Add("susdate", dateTimessus);
                                        }
                                        else
                                            dictPairs.Add("susdate", null);

                                        break;
                                    case "BPtLesRegFbSusDateLifted":
                                        if (colValue != "NULL" && !string.IsNullOrEmpty(colValue))
                                        {
                                            double datesusl = Convert.ToDouble(colValue);
                                            DateTime dateTimessusl = DateTime.FromOADate(datesusl);
                                            dictPairs.Add("susdatel", dateTimessusl);
                                        }
                                        else
                                            dictPairs.Add("susdatel", null);

                                        break;
                                    case "BPtLesRegFbSusMB$Lmt":
                                        dictPairs.Add("SusMB$Lmt", colValue);
                                        break;
                                    case "BPtLesRegFbSusUS$Lmt":
                                        dictPairs.Add("SusSus$Lmt", colValue);
                                        break;
                                    case "BPtLesRegFbLayUpDate":
                                        if (colValue != "NULL" && !string.IsNullOrEmpty(colValue))
                                        {
                                            double dateslay = Convert.ToDouble(colValue);
                                            DateTime dateTimeslay = DateTime.FromOADate(dateslay);
                                            dictPairs.Add("laydate", dateTimeslay);
                                        }
                                        else
                                            dictPairs.Add("laydate", null);

                                        break;
                                    case "BPtLesRegFbLayUpDateLifted":
                                        if (colValue != "NULL" && !string.IsNullOrEmpty(colValue))
                                        {
                                            double dateslayl = Convert.ToDouble(colValue);
                                            DateTime dateTimeslayl = DateTime.FromOADate(dateslayl);
                                            dictPairs.Add("laydatel", dateTimeslayl);
                                        }
                                        else
                                            dictPairs.Add("laydatel", null);

                                        break;
                                    case "BPtLesRegFbLayUpNosPerYr":
                                        
                                        if (colValue != "NULL" && !string.IsNullOrEmpty(colValue))
                                        {
                                            double dateslayp = Convert.ToDouble(colValue);
                                            DateTime dateTimeslayp = DateTime.FromOADate(dateslayp);
                                            dictPairs.Add("laydateyr", dateTimeslayp);
                                        }
                                        else
                                            dictPairs.Add("laydateyr", null);

                                        break;
                                    case "BPtLesRegFbLayUpPeriod":
                                        dictPairs.Add("laydatepd", colValue);
                                        break;
                                    case "Voice to cellular":
                                        try
                                        {
                                            dictPairs.Add("vcel", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("vcel", Convert.ToDecimal(0));
                                        }
                                        break;

                                    case "Voice to fixed":
                                        try
                                        {
                                            dictPairs.Add("vcf", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("vcf", Convert.ToDecimal(0));
                                        }
                                        break;
                                    case "Voice to FBb":
                                        try
                                        {
                                            dictPairs.Add("vcfbb", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("vcfbb", Convert.ToDecimal(0));
                                        }
                                        break;
                                    case "V2Voice mail":
                                        try
                                        {
                                            dictPairs.Add("vcmail", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("vcmail", Convert.ToDecimal(0));
                                        }
                                        break;
                                    case "V2 Iridium":
                                        try
                                        {
                                            dictPairs.Add("vciridi", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("vciridi", Convert.ToDecimal(0));
                                        }
                                        break;
                                    case "ISDN":
                                        try
                                        {
                                            dictPairs.Add("isdn", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("isdn", Convert.ToDecimal(0));
                                        }
                                        break;
                                    case "SMS":
                                        try
                                        {
                                            dictPairs.Add("sms", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("sms", Convert.ToDecimal(0));
                                        }
                                        break;
                                    case "V2Began":
                                        try
                                        {
                                            dictPairs.Add("vcbegan", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("vcbegan", Convert.ToDecimal(0));
                                        }
                                        break;

                                    case "V2GSPS":
                                        try
                                        {
                                            dictPairs.Add("vcgsps", Convert.ToDecimal(colValue));
                                        }
                                        catch
                                        {
                                            dictPairs.Add("vcgsps", Convert.ToDecimal(0));
                                        }
                                        break;
                                }
                            }

                            if (dictPairs.Count > 0)
                            {
                                lstPairs.Add(dictPairs);
                            }
                        }
                    }
                }
                return lstPairs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}