﻿using ClosedXML.Excel;
using FormUpdated.Areas.BillingSystem.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace FormUpdated.Areas.BillingSystem
{
    public class helper_NI
    {
        public static string LesName { get; set; } = "NI";
        public int LesID { get; set; } = getLesID();

        public static int getLesID()
        {
            try
            {
                using (BillingSystemEntities context = new BillingSystemEntities())
                {
                    return context.BsMtServiceProviders.FirstOrDefault(t => t.ServiceProvider.Equals(LesName)).spID;
                }
            }
            catch
            {
                return 0;
            }
        }

        public Tuple<string> getLes(int lesrefid)
        {
            int lesid = 0;
            string lesName = string.Empty;

            using (BillingSystemEntities context = new BillingSystemEntities())
            {
                lesName = context.BsMtLESRefs.FirstOrDefault(t => t.LesRefID == lesrefid).BsMtServiceProvider.ServiceProvider;
            }

            return new Tuple<string>(lesName);
        }

        public Tuple<Stream, string, string, string> CDRtoCT(string fileName, int lesrefID)
        {
            var les = getLes(lesrefID);
            if (!string.IsNullOrEmpty(les.Item1))
            {
                switch (les.Item1)
                {
                    case "IMARSAT":
                        helper_ImarSat imarSat = new helper_ImarSat();
                        Tuple<string, List<OteSatCtVM>> tuplesInmarsat = new Tuple<string, List<OteSatCtVM>>(string.Empty, new List<OteSatCtVM>());
                        string msgInmarsat = string.Empty;

                        if (Path.GetExtension(fileName) == ".csv")
                        {
                            List<OteSatCtVM> lstVM = helper_ImarSat.CsvToViewModel(fileName, lesrefID);

                            tuplesInmarsat = NItoCT_BulkCopy(lstVM);

                            //msg = oteSat.RejectionOtesat_Speed("OteSat", tuplesInmarsat.Item1);
                            //msg = oteSat.RejectionOtesat_New("OteSat", tupleslst.Item1);
                        }
                        else if (Path.GetExtension(fileName) == ".xlsx")
                        {

                        }
                        var spreadsheetInmarSat = WriteCTRecords2Excel(tuplesInmarsat.Item2);
                        return new Tuple<Stream, string, string, string>(spreadsheetInmarSat.Item1, spreadsheetInmarSat.Item2, msgInmarsat, les.Item1);
                        
                    case "NSSL GLOBAL":
                        break;
                    case "OteSat":
                        helper_OteSat oteSat = new helper_OteSat();
                        Tuple<string, List<OteSatCtVM>> tupleslst = new Tuple<string, List<OteSatCtVM>>(string.Empty, new List<OteSatCtVM>());
                        string msg = string.Empty;
                        string code = string.Empty;
                        using(BillingSystemEntities db = new BillingSystemEntities())
                        {
                            code = db.BsMtLESRefs.FirstOrDefault(t => t.LesRefID == lesrefID).LESCode;
                        }
                        if(code.Equals("LOte004"))
                        {
                            if (Path.GetExtension(fileName) == ".csv")
                            {
                                List<OteSatCtVM> lstVM = helper_OteSat.CsvToVMFleetOne(fileName, lesrefID);

                                tupleslst = oteSat.OteSat2CT_model(lstVM, lesrefID);

                                //msg = oteSat.RejectionOtesat_Speed("OteSat", tupleslst.Item1);
                                //msg = oteSat.RejectionOtesat_New("OteSat", tupleslst.Item1);
                            }
                            else if (Path.GetExtension(fileName) == ".xlsx")
                            {

                            }
                            var spreadsheet = oteSat.GenreateCTExcelFleetOne(tupleslst.Item2);
                            return new Tuple<Stream, string, string, string>(spreadsheet.Item1, spreadsheet.Item2, msg, les.Item1);
                        }
                        else
                        {
                            if (Path.GetExtension(fileName) == ".csv")
                            {
                                List<OteSatCtVM> lstVM = helper_OteSat.CsvToDictionary(fileName);

                                tupleslst = oteSat.OteSat2CT_model(lstVM, lesrefID);

                                //msg = oteSat.RejectionOtesat_Speed("OteSat", tupleslst.Item1);
                                //msg = oteSat.RejectionOtesat_New("OteSat", tupleslst.Item1);


                            }
                            else if (Path.GetExtension(fileName) == ".xlsx")
                            {

                            }
                            var spreadsheet = oteSat.GnerateManualInvoice(tupleslst.Item2);
                            return new Tuple<Stream, string, string, string>(spreadsheet.Item1, spreadsheet.Item2, msg, les.Item1);
                        }

                        
                    case "NI":
                        Tuple<string, List<OteSatCtVM>> tuplesNI = new Tuple<string, List<OteSatCtVM>>(string.Empty, new List<OteSatCtVM>());

                        if (Path.GetExtension(fileName) == ".csv")
                        {
                            List<OteSatCtVM> lstVM = CsvToDictionary(fileName, lesrefID);

                            tuplesNI = NItoCT_BulkCopy(lstVM);
                        }
                        else if (Path.GetExtension(fileName) == ".xlsx")
                        {

                        }
                        var spreadsheetNI = WriteCTRecords2Excel(tuplesNI.Item2);
                        return new Tuple<Stream, string, string, string>(spreadsheetNI.Item1, spreadsheetNI.Item2, tuplesNI.Item1, les.Item1);
                    case "EvoSat":
                        break;
                }
            }

            Stream stream = null;
            return new Tuple<Stream, string, string, string>(stream, string.Empty, string.Empty, string.Empty);
        }

        public static List<OteSatCtVM> CsvToDictionary(string csv_file_path, int lesrefid)
        {
            List<Dictionary<string, dynamic>> keyValues = new List<Dictionary<string, dynamic>>();
            List<OteSatCtVM> lstVM = new List<OteSatCtVM>();
            try
            {
                List<string> columnFields = new List<string>();
                using (Microsoft.VisualBasic.FileIO.TextFieldParser csvReader = new Microsoft.VisualBasic.FileIO.TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields = csvReader.ReadFields();
                    foreach (string column in colFields)
                    {
                        string columnName = string.Empty;
                        if (column.StartsWith(" "))
                        {
                            columnName = column.Substring(1);
                            columnName = columnName.TrimEnd();
                        }
                        columnName = column.TrimEnd();
                        columnFields.Add(System.Text.RegularExpressions.Regex.Replace(columnName, @"\t|\n|\r", ""));

                        //DataColumn datecolumn = new DataColumn(column);
                        //datecolumn.AllowDBNull = true;
                        //csvData.Columns.Add(datecolumn);
                    }
                    Dictionary<string, dynamic> dictRecord = new Dictionary<string, dynamic>();

                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        dictRecord = new Dictionary<string, dynamic>();
                        OteSatCtVM vM = new OteSatCtVM();
                        //Making empty value as null
                        for (int i = 0, colIndex = 0; i < fieldData.Length; i++, colIndex++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = string.Empty;
                            }
                            vM = storeCollection_NI(columnFields[colIndex], fieldData[i], vM, lesrefid);
                            //dictRecord.Add(columnFields[colIndex], fieldData[i]);
                        }
                        //csvData.Rows.Add(fieldData);
                        if (vM.BCtTranServType != string.Empty && vM.BCtTranServType != "" && !string.IsNullOrEmpty(vM.BCtTranServType))
                            lstVM.Add(vM);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstVM;
        }

        public static OteSatCtVM storeCollection_NI(string index, string value, OteSatCtVM vum, int lesrefid)
        {
            switch (index)
            {
                case "imei":
                    //vum.VesselName = value;
                    vum.BCtFbbLES = "NI";
                    break;
                case "id":
                    //vum.VesselName = value;
                    vum.BCtFbCdrCode = value;
                    break;
                case "imsi":
                    vum.BCtFBImImsi = value;
                    break;
                case "System":
                    if (!string.IsNullOrEmpty(value) && value.Contains("Inmarsat-FleetBroadband"))
                    {
                        vum.BCtFbLesRefCode = lesrefid;
                        vum.BCtFbEquipmentCode = 3;
                        vum.BCtFbSatCode = 1;
                    }
                    else if (!string.IsNullOrEmpty(value) && value.Contains("Inmarsat-FleetOne"))
                    {
                        vum.BCtFbLesRefCode = lesrefid;
                        vum.BCtFbEquipmentCode = 3;
                        vum.BCtFbSatCode = 1;
                    }
                    else if (!string.IsNullOrEmpty(value) && value.Contains("Inmarsat-GSPS"))
                    {
                        vum.BCtFbLesRefCode = lesrefid;
                        vum.BCtFbEquipmentCode = 3;
                        vum.BCtFbSatCode = 1;
                    }
                    break;
                case "call_date":
                    if (!string.IsNullOrEmpty(value))
                    {
                        DateTime val;
                        bool result = DateTime.TryParse(value, out val);
                        if (result)
                            vum.BCtTranStartDate = val;
                        else
                            vum.BCtTranStartDate = null;
                    }
                    break;
                case "call_time":
                    if (!string.IsNullOrEmpty(value))
                    {
                        TimeSpan val;
                        bool result = TimeSpan.TryParse(value, out val);
                        if (result)
                            vum.BCtTranStartTime = val;
                        else
                            vum.BCtTranStartTime = null;
                    }
                    break;
                case "origin":
                    vum.BCtTranOrigin = value;
                    break;
                case "Time Zone":
                    //vum.TimeZone = value;
                    break;
                case "description":
                    if (!string.IsNullOrEmpty(value))
                    {
                        if (value.ToLower().Contains("discount"))
                            vum.BCtTranServType = string.Empty;
                        else
                        {
                            vum.BCtTranServType = value;
                        }

                        if (!string.IsNullOrEmpty(value) && value.Contains("FleetBroadband"))
                        {
                            vum.BCtFbLesRefCode = lesrefid;
                            vum.BCtFbEquipmentCode = 3;
                            vum.BCtFbSatCode = 1;
                        }
                        else if (!string.IsNullOrEmpty(value) && value.Contains("Fleet BB Background IP"))
                        {
                            vum.BCtFbLesRefCode = lesrefid;
                            vum.BCtFbEquipmentCode = 3;
                            vum.BCtFbSatCode = 1;
                        }
                        else if (!string.IsNullOrEmpty(value) && value.Contains("Basic Voice - Fleet BB"))
                        {
                            vum.BCtFbLesRefCode = lesrefid;
                            vum.BCtFbEquipmentCode = 3;
                            vum.BCtFbSatCode = 1;
                        }
                    }
                    else
                        vum.BCtTranServType = null;

                    break;
                case "destination":
                    vum.BCtTranServDest = value;
                    break;
                case "quantity_charged":
                    if (!string.IsNullOrEmpty(value))
                    {
                        decimal val = 0;
                        bool result = decimal.TryParse(value, out val);
                        if (result)
                        {
                            if (!string.IsNullOrEmpty(vum.BCtTranServType) && vum.BCtTranServType.Equals("Fleet BB Background IP"))
                            {
                                double dat = Convert.ToDouble(val);
                                double ip = Converters.ConvertMegabytetoBit(dat);
                                double bytes = 0;
                                bool res = double.TryParse(ip.ToString(), out bytes);
                                if (res)
                                {
                                    vum.BCtTranUnitUsage = Convert.ToDecimal(ip);
                                }
                                else
                                {
                                    vum.BCtTranUnitUsage = val;
                                }
                            }
                            else
                            {
                                double dsec = Convert.ToDouble(val);
                                double tsec = Converters.ConvertMinutesToSeconds(dsec);
                                if (double.TryParse(tsec.ToString(), out tsec))
                                    vum.BCtTranUnitUsage = Convert.ToDecimal(tsec);
                                else
                                    vum.BCtTranUnitUsage = val;
                            }
                        }
                        else
                        {
                            vum.BCtTranUnitUsage = null;
                        }
                    }
                    break;
                case "unit_type":
                    if (!string.IsNullOrEmpty(vum.BCtTranServType) && vum.BCtTranServType.Equals("Fleet BB Background IP"))
                    {
                        vum.BCtTranUnitType = "Bits";
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(value) && value.Contains("MIN"))
                            vum.BCtTranUnitType = "Sec";
                        else
                            vum.BCtTranUnitType = value;
                    }
                    break;
                case "isp_price":
                    if (!string.IsNullOrEmpty(value))
                    {
                        decimal val = 0;
                        bool result = decimal.TryParse(value, out val);
                        if (result)
                            vum.BCtTranTotalUsage = val;
                        else
                            vum.BCtTranTotalUsage = null;
                    }
                    break;
            }
            return vum;
        }

        public static Tuple<string, List<OteSatCtVM>> NItoCT_BulkCopy(List<OteSatCtVM> lstVM)
        {
            string msg = string.Empty;
            try
            {
                using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["AdoConnstr"].ConnectionString))
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();

                    using (var bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.Default, transaction))
                    {
                        bulkCopy.BatchSize = 100;
                        bulkCopy.DestinationTableName = "dbo.BCtFbbTrans";
                        try
                        {
                            bulkCopy.WriteToServer(lstVM.AsDataTable());
                        }
                        catch (Exception e)
                        {
                            transaction.Rollback();
                            connection.Close();
                            msg = "faild to store common table";
                            return new Tuple<string, List<OteSatCtVM>>(msg, lstVM);
                        }
                    }

                    transaction.Commit();
                    msg = "successfully store common table";
                    return new Tuple<string, List<OteSatCtVM>>(msg, lstVM);
                }
            }
            catch (Exception error)
            {
                msg = "faild to store common table eorro: " + error.Message.ToString();
                return new Tuple<string, List<OteSatCtVM>>(msg, lstVM);
            }
        }

        public static Tuple<Stream, string> WriteCTRecords2Excel(List<OteSatCtVM> lstID)
        {
            System.IO.Stream spreadsheetstream = new System.IO.MemoryStream();

            string Invoiceno = string.Empty;

            int value = 0;

            try
            {
                string imsiID = string.Empty;
                string invoiceNo = string.Empty;
                string ImarsatID = string.Empty;

                string buildingName = string.Empty;
                string street = string.Empty;
                string district = string.Empty;
                string city = string.Empty;
                string country = string.Empty;

                string VesselName = string.Empty;

                string CustomerName = string.Empty;

                BillingSystemEntities db = new BillingSystemEntities();

                DateTime dt = DateTime.Now;
                string Period = dt.ToString("MMM");
                DateTime dy = DateTime.Today;
                string currYear = dy.Year.ToString();

                string CurrPeriod = Period + '-' + currYear;
                string InvoiceFileName = invoiceNo + '_' + buildingName + '_' + VesselName;

                string pathid = Guid.NewGuid().ToString() + " " + DateTime.Now.Date.ToString("dd-MM-yyyy");

                #region WorkBook Prepartion

                XLWorkbook xLWorkbook = new XLWorkbook();
                IXLWorksheet workSheet = xLWorkbook.Worksheets.Add("Invoice" + Invoiceno);


                // Work Sheet Header
                workSheet.Cell(1, 1).Value = "BCtALES";
                workSheet.Cell(1, 2).Value = "BCtAFBImImsi";
                workSheet.Cell(1, 3).Value = "BCtAFbContNo";
                workSheet.Cell(1, 4).Value = "BCtAFbCdrCode";
                workSheet.Cell(1, 5).Value = "BCtFbLesRefCode";
                workSheet.Cell(1, 6).Value = "BCtFbSatCode";
                workSheet.Cell(1, 7).Value = "BCtFbEquipmentCode";
                workSheet.Cell(1, 8).Value = "BCtAFbRecurFee";
                workSheet.Cell(1, 9).Value = "BCtAFbRecurFeeType";
                workSheet.Cell(1, 10).Value = "BCtBStartDate";
                workSheet.Cell(1, 11).Value = "BCtBEndDate";
                workSheet.Cell(1, 12).Value = "BCtBStartTime";
                workSheet.Cell(1, 13).Value = "BCtBOrigin";

                workSheet.Cell(1, 14).Value = "BCtAFbServType";
                workSheet.Cell(1, 15).Value = "BCtAFbServDest";
                workSheet.Cell(1, 16).Value = "BCtBTransUsage";
                workSheet.Cell(1, 17).Value = "BCtBUnits";
                workSheet.Cell(1, 18).Value = "BCtBTotalUsage";
                workSheet.Cell(1, 19).Value = "BCtBComTypes";
                workSheet.Cell(1, 20).Value = "BCtBBillIncr";
                workSheet.Cell(1, 21).Value = "BCtAFbBillNo";
                workSheet.Cell(1, 22).Value = "BCtAFbBillPeriod";
                workSheet.Cell(1, 23).Value = "BCtAFbBillDate";
                workSheet.Cell(1, 24).Value = "BCtAFbDualSim";
                workSheet.Cell(1, 25).Value = "BCtAFbContNo";
                workSheet.Cell(1, 26).Value = "BCtAFbContStart";
                workSheet.Cell(1, 27).Value = "BCtAFbContEnd";
                workSheet.Cell(1, 28).Value = "BCtAFbTerm";
                workSheet.Cell(1, 29).Value = "BCtAFbRgnFee";
                workSheet.Cell(1, 30).Value = "BCtAFbActFee";
                workSheet.Cell(1, 31).Value = "BCtAFbCardFee";


                int row = 2;
                decimal? totals = 0;
                int sno = 1;

                foreach (var ct in lstID)
                {
                    workSheet.Cell(row, 1).Value = ct.BCtFbbLES;
                    workSheet.Cell(row, 2).Style.NumberFormat.Format = "@";
                    workSheet.Cell(row, 2).Value = ct.BCtFBImImsi;

                    workSheet.Cell(row, 3).Value = ct.BCtFbContNo;

                    workSheet.Cell(row, 4).Value = ct.BCtFbCdrCode;
                    workSheet.Cell(row, 5).Value = db.BsMtLESRefs.FirstOrDefault(t => t.LesRefID == ct.BCtFbLesRefCode).LESCode;
                    workSheet.Cell(row, 6).Value = db.BsMtSatTypes.FirstOrDefault(t => t.MsSatType == ct.BCtFbSatCode).Satcode;
                    workSheet.Cell(row, 7).Value = db.BsMtEquipTypes.FirstOrDefault(t => t.EquipTypeID == ct.BCtFbEquipmentCode).EquipmentCode;

                    if (ct.BCtTranServType.Contains("Subscription Fee"))
                        workSheet.Cell(row, 8).Value = ct.BCtTranTotalUsage;

                    workSheet.Cell(row, 9).Value = ct.BCtAFbRecurFeeType;

                    workSheet.Cell(row, 10).Style.NumberFormat.Format = "dd-mm-yyyy"; ;
                    workSheet.Cell(row, 10).Value = ct.BCtTranStartDate;
                    workSheet.Cell(row, 11).Style.NumberFormat.Format = "dd-mm-yyyy"; ;
                    workSheet.Cell(row, 11).Value = ct.BCtTranEndDate;

                    workSheet.Cell(row, 12).Style.NumberFormat.Format = "hh:mm:ss";
                    workSheet.Cell(row, 12).Value = ct.BCtTranStartTime;
                    workSheet.Cell(row, 13).Value = ct.BCtTranOrigin;

                    workSheet.Cell(row, 14).Value = ct.BCtTranServType;

                    workSheet.Cell(row, 15).Value = ct.BCtTranServDest;

                    workSheet.Cell(row, 16).Style.NumberFormat.Format = "0.00";
                    workSheet.Cell(row, 16).DataType = XLDataType.Number;
                    workSheet.Cell(row, 16).Value = ct.BCtTranUnitUsage;
                    workSheet.Cell(row, 17).Value = ct.BCtTranUnitType;
                    if (!ct.BCtTranServType.Contains("Subscription Fee"))
                    {
                        workSheet.Cell(row, 18).Style.NumberFormat.Format = "0.00"; ;
                        workSheet.Cell(row, 18).DataType = XLDataType.Number;
                        workSheet.Cell(row, 18).Value = ct.BCtTranTotalUsage;
                    }
                    else
                    {
                        workSheet.Cell(row, 18).Style.NumberFormat.Format = "0.00"; ;
                        workSheet.Cell(row, 18).DataType = XLDataType.Number;
                        workSheet.Cell(row, 18).Value = 0.00;
                    }


                    workSheet.Cell(row, 19).Value = "";

                    workSheet.Cell(row, 20).Value = ct.BCtTranBillIncr;

                    workSheet.Cell(row, 21).Value = ct.BCtFbBillNo;
                    workSheet.Cell(row, 22).Value = ct.BCtFbBillPeriod;
                    workSheet.Cell(row, 23).Value = ct.BCtFbBillDate;
                    workSheet.Cell(row, 24).Value = ct.BCtFbDualSim;
                    workSheet.Cell(row, 25).Value = ct.BCtFbContNo;
                    workSheet.Cell(row, 26).Value = ct.BCtFbContStartDate;
                    workSheet.Cell(row, 27).Value = ct.BCtFbContEndDate;
                    workSheet.Cell(row, 28).Value = ct.BCtFbTerm;
                    workSheet.Cell(row, 29).Value = ct.BCtFbRgnFee;
                    workSheet.Cell(row, 30).Value = ct.BCtFbActFee;
                    workSheet.Cell(row, 31).Value = ct.BCtFbCardFee;

                    row++;

                }
                #endregion

                // Save Work sheet into workbook
                xLWorkbook.SaveAs(spreadsheetstream);
                spreadsheetstream.Position = 0;

                //InvoiceFileName = temppath + "\\" + InvoiceFileName;

                //FileStream fileStream = new FileStream(InvoiceFileName, FileMode.Create);

                return new Tuple<Stream, string>(spreadsheetstream, "CommonTableDatas.xlsx");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            throw new Exception("The master primary is not an interger, please check and update the issue.");
        }
    }
}