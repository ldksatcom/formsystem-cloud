//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FormUpdated.Areas.BillingSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class BsPtLesReg
    {
        public int LesReg_ID { get; set; }
        public string BPtLesRgnFBimSingle { get; set; }
        public string BPtLesRgnFBimDual { get; set; }
        public string BPtLesRgnFbMultiVce { get; set; }
        public string BPtLesRgnFbContNo { get; set; }
        public Nullable<System.DateTime> BPtLesRgnFbContStart { get; set; }
        public string NA6 { get; set; }
        public string NA7 { get; set; }
        public Nullable<System.DateTime> BPtLesRgnFbContEnd { get; set; }
        public Nullable<System.DateTime> BPtLesRgnFBtartDate { get; set; }
        public Nullable<System.DateTime> BPtLesRgnpFbEndDate { get; set; }
        public string BPtLesRgnFbTerm { get; set; }
        public Nullable<decimal> BPtLesRgnFbCardFee { get; set; }
        public Nullable<decimal> BPtLesRgnFbActFee { get; set; }
        public Nullable<decimal> BPtLesRgnFbRgnFee { get; set; }
        public string NA15 { get; set; }
        public string NA16 { get; set; }
        public string NA17 { get; set; }
        public string NA18 { get; set; }
        public string NA19 { get; set; }
        public string BPtLesRgnFbDataInBundleId { get; set; }
        public string BPtLesRgnFbDataMSIDNId { get; set; }
        public string BPtLesRgnFbDataSimardId { get; set; }
        public string NA23 { get; set; }
        public string NA24 { get; set; }
        public string BPtLesRgnFbDataPlanIn { get; set; }
        public string BPtLesRgnFbVcePlanOPtLesRgn { get; set; }
        public Nullable<decimal> BPtLesRgnFbRecurFee { get; set; }
        public Nullable<decimal> BPtLesRgnFbRecurFeeType { get; set; }
        public Nullable<decimal> BPtLesRgnFbDataRate { get; set; }
        public Nullable<decimal> BPtLesRgnFbBilIncre { get; set; }
        public string BPtLesRgnFbunitType { get; set; }
        public string NA32 { get; set; }
        public string NA33 { get; set; }
        public string NA34 { get; set; }
        public string BPtLesRgnFbTranDServType { get; set; }
        public Nullable<System.DateTime> BPtLesRgnFbTranDStartDate { get; set; }
        public Nullable<System.TimeSpan> BPtLesRgnFbTranDStartTime { get; set; }
        public Nullable<System.TimeSpan> BPtLesRgnFbTranDEndTime { get; set; }
        public string BPtLesRgnFbTranDOrig { get; set; }
        public string BPtLesRgnFbTranDDest { get; set; }
        public Nullable<decimal> BPtLesRgnFbTranDUsage { get; set; }
        public string NA42 { get; set; }
        public string NA43 { get; set; }
        public string NA44 { get; set; }
        public string NA45 { get; set; }
        public string NA46 { get; set; }
        public string NA47 { get; set; }
        public string NA48 { get; set; }
        public string NA49 { get; set; }
        public Nullable<decimal> BPtLesRgnFbVce2Fixed { get; set; }
        public Nullable<decimal> BPtLesRgnFbVce2Cell { get; set; }
        public Nullable<decimal> BPtLesRgnFbVc2Bgan { get; set; }
        public Nullable<decimal> BPtLesRgnFbVce2Fb { get; set; }
        public Nullable<decimal> BPtLesRgnFbVce2Gpsp { get; set; }
        public Nullable<decimal> BPtLesRgnFbVce2VceMail { get; set; }
        public Nullable<decimal> BPtLesRgnFbVce2Others { get; set; }
        public Nullable<decimal> BPtLesRgnFbVcebillincre { get; set; }
        public string BPtLesRgnFbVceunitytype { get; set; }
        public string NA59 { get; set; }
        public string BPtLesRgnFbTranVServType { get; set; }
        public Nullable<System.DateTime> BPtLesRgnFbTranVStartDate { get; set; }
        public Nullable<System.TimeSpan> BPtLesRgnFbTranVStartTime { get; set; }
        public Nullable<System.TimeSpan> BPtLesRgnFbTranVEndTime { get; set; }
        public string BPtLesRgnFbTranVOrig { get; set; }
        public string BPtLesRgnFbTranVDest { get; set; }
        public Nullable<decimal> BPtLesRgnFbTranVUsage { get; set; }
        public string NA67 { get; set; }
        public string NA68 { get; set; }
        public string BPtLesRgnStrInBundkeFbId { get; set; }
        public string BPtLesRgnFbStrMSIDNId { get; set; }
        public string BPtLesRgnFbStrSimCardId { get; set; }
        public string NA72 { get; set; }
        public Nullable<decimal> BPtLesRgnFb8kbps { get; set; }
        public Nullable<decimal> BPtLesRgnFb16kbps { get; set; }
        public Nullable<decimal> BPtLesRgnFb32kbps { get; set; }
        public Nullable<decimal> BPtLesRgnFb64kbps { get; set; }
        public Nullable<decimal> BPtLesRgnFb128kbps { get; set; }
        public Nullable<decimal> BPtLesRgnFb256kbps { get; set; }
        public Nullable<decimal> BPtLesRgnFBtBillIncre { get; set; }
        public string BPtLesRgnFBtBillingUnittype { get; set; }
        public string NA81 { get; set; }
        public string NA82 { get; set; }
        public string BPtLesRgnFbTranStServType { get; set; }
        public Nullable<System.DateTime> BPtLesRgnFbTranStStartDate { get; set; }
        public Nullable<System.TimeSpan> BPtLesRgnFbTranStStartTime { get; set; }
        public Nullable<System.TimeSpan> BPtLesRgnFbTranStEndTime { get; set; }
        public string BPtLesRgnFbTranStOrig { get; set; }
        public string BPtLesRgnFbTranStDest { get; set; }
        public Nullable<decimal> BPtLesRgnFbTranStUsage { get; set; }
        public string NA90 { get; set; }
        public string NA91 { get; set; }
        public string NA92 { get; set; }
        public Nullable<decimal> BPtLesRgnFb2Bgan { get; set; }
        public Nullable<decimal> BPtLesRgnFb2FB { get; set; }
        public Nullable<decimal> BPtLesRgnFb2Gsps { get; set; }
        public Nullable<decimal> BPtLesRgnFb2Fleet { get; set; }
        public Nullable<decimal> BPtLesRgnFb2SwV { get; set; }
        public Nullable<decimal> BPtLesRgnFb2AeroV { get; set; }
        public Nullable<decimal> BPtLesRgnFb2Iridium { get; set; }
        public Nullable<decimal> BPtLesRgnFb2Thu { get; set; }
        public Nullable<decimal> BPtLesRgnFb2OMes { get; set; }
        public string NA102 { get; set; }
        public string BPtLesRgnFbTranOtherServType { get; set; }
        public Nullable<System.DateTime> BPtLesRgnFbTranOtherstartDate { get; set; }
        public Nullable<System.TimeSpan> BPtLesRgnFbTranOtherStartTime { get; set; }
        public Nullable<System.TimeSpan> BPtLesRgnFbTranOtherEndTime { get; set; }
        public string BPtLesRgnFbTranOtherOrig { get; set; }
        public string BPtLesRgnFbTranOtherDest { get; set; }
        public Nullable<decimal> BPtLesRgnFbTranOtherUsage { get; set; }
        public string NA110 { get; set; }
        public string NA111 { get; set; }
        public string NA112 { get; set; }
        public string BPtLesRgnFbSMSInBundleId { get; set; }
        public string BPtLesRgnFbSMSMSIDNId { get; set; }
        public string BPtLesRgnFbSMSSimCardId { get; set; }
        public Nullable<decimal> BPtLesRgnFBms { get; set; }
        public Nullable<decimal> BPtLesRgnFbIsdnFax { get; set; }
        public Nullable<decimal> BPtLesRgnFbTranSmsServType { get; set; }
        public Nullable<System.DateTime> BPtLesRgnFbTranSmsstartDate { get; set; }
        public Nullable<System.TimeSpan> BPtLesRgnFbTranSmsStartTime { get; set; }
        public Nullable<System.TimeSpan> BPtLesRgnFbTranSmsEndTime { get; set; }
        public string BPtLesRgnFbTranSmsOrig { get; set; }
        public string BPtLesRgnFbTranSmsDest { get; set; }
        public Nullable<decimal> BPtLesRgnFbTranSmsUsage { get; set; }
        public string NA125 { get; set; }
        public string BPtLesRgnFbLesRegTCId { get; set; }
        public string BPtLesRgnFbLesRegTCMSIDNId { get; set; }
        public string BPtLesRgnFbLesRegTCSimCardId { get; set; }
        public string NA129 { get; set; }
        public string BPtLesRgnEndFbPenalty { get; set; }
        public Nullable<decimal> BPtLesRgnFbCurBill { get; set; }
        public string NA132 { get; set; }
        public Nullable<decimal> BPtLesRgnFbGst { get; set; }
        public string BPtLesRgnFbPayTerm { get; set; }
        public string NA135 { get; set; }
        public string NA136 { get; set; }
        public string NA137 { get; set; }
        public string NA138 { get; set; }
        public string NA139 { get; set; }
        public string BPtLesRgnFbIbvFmt { get; set; }
        public Nullable<System.DateTime> BPtLesRgnFbBarDate { get; set; }
        public Nullable<System.DateTime> BPtLesRgnFbBarDateLifted { get; set; }
        public Nullable<System.DateTime> BPtLesRgnFBusDate { get; set; }
        public Nullable<System.DateTime> BPtLesRgnFBusDateLifted { get; set; }
        public Nullable<int> BPtLesRgnFBusMB_Lmt { get; set; }
        public Nullable<decimal> BPtLesRgnFBusUS_Lmt { get; set; }
        public Nullable<System.DateTime> BPtLesRgnFblayUpDate { get; set; }
        public Nullable<System.DateTime> BPtLesRgnFbLayUpDateLifted { get; set; }
        public Nullable<int> BPtLesRgnFbLayUpNosPerYr { get; set; }
        public Nullable<int> BPtLesRgnFbLayUpPeriod { get; set; }
        public string BPtLesRgnFbBillrecip { get; set; }
        public string BPtLesRgnFbInvPWeight { get; set; }
        public string BPtLesRgnFEerrAdju { get; set; }
    }
}
