//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FormUpdated.Areas.BillingSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class BsMtLESRef
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BsMtLESRef()
        {
            this.BptFbSims = new HashSet<BptFbSim>();
            this.BS1ALessT = new HashSet<BS1ALessT>();
            this.BS2MtCusT = new HashSet<BS2MtCusT>();
            this.BsNewCTs = new HashSet<BsNewCT>();
        }
    
        public int LesRefID { get; set; }
        public Nullable<int> fkSPID { get; set; }
        public string SMTSCode { get; set; }
        public string LESCode { get; set; }
        public string Description { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BptFbSim> BptFbSims { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BS1ALessT> BS1ALessT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BS2MtCusT> BS2MtCusT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BsNewCT> BsNewCTs { get; set; }
        public virtual BsMtServiceProvider BsMtServiceProvider { get; set; }
    }
}
