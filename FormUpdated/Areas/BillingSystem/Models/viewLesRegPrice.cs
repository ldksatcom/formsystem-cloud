//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FormUpdated.Areas.BillingSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class viewLesRegPrice
    {
        public int CusID { get; set; }
        public string Customers { get; set; }
        public int Ship_ID { get; set; }
        public string BPtShipName { get; set; }
        public int Sim_ID { get; set; }
        public string BPtFBImImsi { get; set; }
        public string BPtFBimMsisdn { get; set; }
        public string BPtFBimIccid { get; set; }
        public int SPLesRegID { get; set; }
        public string BPtLesRgnFBDataPlanIn { get; set; }
        public Nullable<decimal> BPtLesRgnFbRecurFee { get; set; }
        public Nullable<decimal> BPtLesRgnFbInbundleDataRate { get; set; }
        public Nullable<decimal> BPtLesRgnFbOutBundleDataRate { get; set; }
        public int SPServDID { get; set; }
        public string BsPtServiceTypes { get; set; }
        public Nullable<decimal> BsPtServiceCostPrice { get; set; }
    }
}
