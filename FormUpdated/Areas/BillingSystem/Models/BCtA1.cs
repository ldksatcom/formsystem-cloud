//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FormUpdated.Areas.BillingSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class BCtA1
    {
        public int BCtAID { get; set; }
        public int BCtAfkLESID { get; set; }
        public int BCtAfkLESCID { get; set; }
        public int BCtAfkLesRegID { get; set; }
        public int BCtAfkSimID { get; set; }
        public string BCtAFbLesCode { get; set; }
        public string BCtAFbCdrCode { get; set; }
        public Nullable<int> fkLesRefID { get; set; }
        public Nullable<int> fkSatTypeID { get; set; }
        public Nullable<int> fkEquipType { get; set; }
        public string BCtAFbServDest { get; set; }
        public string BCtAFbServType { get; set; }
        public Nullable<int> BCtAFbTransUsage { get; set; }
        public string BCtAFbUOM { get; set; }
        public Nullable<decimal> BCtAFbRecurFee { get; set; }
        public string BCtAFbRecurFeeType { get; set; }
        public string BCtAFbCDRRef { get; set; }
        public string BCtAFbBillNo { get; set; }
        public string BCtAFbBillPeriod { get; set; }
        public Nullable<System.DateTime> BCtAFbBillDate { get; set; }
        public string BCtAFbSingleSim { get; set; }
        public string BCtAFbDualSim { get; set; }
        public string BCtAFbMultiVce { get; set; }
        public string BCtAFbContNo { get; set; }
        public Nullable<System.DateTime> BCtAFbContStart { get; set; }
        public Nullable<System.DateTime> BCtAFbContEnd { get; set; }
        public Nullable<System.DateTime> BCtAFbStartDate { get; set; }
        public Nullable<System.DateTime> BCtAFbEndDate { get; set; }
        public Nullable<int> BCtAFbTerm { get; set; }
        public Nullable<decimal> BCtAFbCardFee { get; set; }
        public Nullable<decimal> BCtAFbActFee { get; set; }
        public Nullable<decimal> BCtAFbRgnFee { get; set; }
    
        public virtual BsPtSPContract BsPtSPContract { get; set; }
        public virtual BsPtSPLesReg BsPtSPLesReg { get; set; }
        public virtual BptFbSim BptFbSim { get; set; }
        public virtual BsMtServiceProvider BsMtServiceProvider { get; set; }
    }
}
