//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FormUpdated.Areas.BillingSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class BSt2C
    {
        public int Bst2C_ID { get; set; }
        public string Bt2CusFbStreamIMSIIDNo { get; set; }
        public string Bt2CusFbStreamMSIDIdNo { get; set; }
        public string Bt2CusFbStreamSimIdNo { get; set; }
        public string NA { get; set; }
        public Nullable<decimal> Bt2CusFb8kbps { get; set; }
        public Nullable<decimal> Bt2CusFb16kbps { get; set; }
        public Nullable<decimal> Bt2CusFb32kbps { get; set; }
        public Nullable<decimal> Bt2CusFb64kbps { get; set; }
        public Nullable<decimal> Bt2CusFb128kbps { get; set; }
        public Nullable<decimal> Bt2CusFb256kbps { get; set; }
        public Nullable<int> Bt2CusFbStBillIncre { get; set; }
        public string NA1 { get; set; }
        public string NA2 { get; set; }
        public string NA3 { get; set; }
        public string Bt2CusFbTranStServType { get; set; }
        public Nullable<System.DateTime> Bt2CusFbTranStStartDate { get; set; }
        public Nullable<System.TimeSpan> Bt2CusFbTranStStartTime { get; set; }
        public Nullable<System.TimeSpan> Bt2CusFbTranStEndTime { get; set; }
        public string Bt2CusFbTranStOrig { get; set; }
        public string Bt2CusFbTranStDest { get; set; }
        public string Bt2CusFbTranStUsage { get; set; }
        public string NA4 { get; set; }
        public string NA5 { get; set; }
        public string NA6 { get; set; }
        public Nullable<decimal> BTCusFb2Bgan { get; set; }
        public Nullable<decimal> BTCusFb2FB { get; set; }
        public Nullable<decimal> BTCusb2Gsps { get; set; }
        public Nullable<decimal> BTCusFb2Fleet { get; set; }
        public Nullable<decimal> BTCusFb2SwV { get; set; }
        public Nullable<decimal> BTCusFb2AeroV { get; set; }
        public Nullable<decimal> BTCusFb2Iridium { get; set; }
        public Nullable<decimal> BTCusFb2Thu { get; set; }
        public Nullable<decimal> BTCusFb2OMes { get; set; }
        public string NA7 { get; set; }
        public string Bt2CusFbTranOtherServType { get; set; }
        public Nullable<System.DateTime> Bt2CusFbTranOtherstartDate { get; set; }
        public Nullable<System.TimeSpan> Bt2CusFbTranOtherStartTime { get; set; }
        public Nullable<System.TimeSpan> Bt2CusFbTranOtherEndTime { get; set; }
        public string Bt2CusFbTranOtherOrig { get; set; }
        public string Bt2CusFbTranOtherDest { get; set; }
        public string Bt2CusFbTranOtherUsage { get; set; }
        public string NA8 { get; set; }
        public string NA9 { get; set; }
        public string NA10 { get; set; }
        public string NA11 { get; set; }
        public string Bt2CusFbSmsImsiId { get; set; }
        public string Bt2CusFbSmsMSIDNId { get; set; }
        public string BSt2CusFbSmsSimId { get; set; }
        public Nullable<decimal> Bt2CusFBms { get; set; }
        public Nullable<decimal> Bt2CusFbIsdnFax { get; set; }
        public string Bt2CusFbTranSmsServType { get; set; }
        public Nullable<System.DateTime> Bt2CusFbTranSmsstartDate { get; set; }
        public Nullable<System.TimeSpan> Bt2CusFbTranSmsStartTime { get; set; }
        public Nullable<System.TimeSpan> Bt2CusFbTranSmsEndTime { get; set; }
        public string Bt2CusFbTranSmsOrig { get; set; }
        public string Bt2CusFbTranSmsDest { get; set; }
        public string Bt2CusFbTranSmsUsage { get; set; }
        public bool Bt2CusFbisOutBundle { get; set; }
    }
}
