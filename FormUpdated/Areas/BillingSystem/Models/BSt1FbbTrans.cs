//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FormUpdated.Areas.BillingSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class BSt1FbbTrans
    {
        public int Bt1LesFbID { get; set; }
        public string Bt1LesFbLES { get; set; }
        public Nullable<int> Bt1LesFbLesRegID { get; set; }
        public Nullable<int> Bt1LesFbCusRegID { get; set; }
        public string Bt1LesFbIMSIId { get; set; }
        public string Bt1LesFbMsidnId { get; set; }
        public string Bt1LesFbContNo { get; set; }
        public Nullable<System.DateTime> Bt1LesFbContStartDate { get; set; }
        public Nullable<System.DateTime> Bt1LesFbContEndDate { get; set; }
        public string Bt1LesFbTerm { get; set; }
        public string Bt1LesFbCdrCode { get; set; }
        public Nullable<int> Bt1LesFbLesRefCode { get; set; }
        public Nullable<int> Bt1LesFbSatCode { get; set; }
        public Nullable<int> Bt1LesFbEquipmentCode { get; set; }
        public Nullable<bool> Bt1LesFbSimSingle { get; set; }
        public Nullable<bool> Bt1LesFbSimDual { get; set; }
        public Nullable<bool> Bt1LesFbMultiVce { get; set; }
        public Nullable<decimal> Bt1LesFbRecurFee { get; set; }
        public string Bt1LesFbRecurFeeType { get; set; }
        public string Bt1LesFbServiceType { get; set; }
        public string NA1 { get; set; }
        public string NA2 { get; set; }
        public string NA3 { get; set; }
        public string NA4 { get; set; }
        public string NA5 { get; set; }
        public Nullable<System.DateTime> Bt1LesFbTranStartDate { get; set; }
        public Nullable<System.DateTime> Bt1LesFbTranEndDate { get; set; }
        public Nullable<System.TimeSpan> Bt1LesFbTranStartTime { get; set; }
        public Nullable<System.TimeSpan> Bt1LesFbTranEndTime { get; set; }
        public string Bt1LesFbTranDOrig { get; set; }
        public Nullable<int> Bt1LesFbTranServType { get; set; }
        public string Bt1LesFbTranDest { get; set; }
        public Nullable<decimal> Bt1LesFbTranUsage { get; set; }
        public Nullable<int> Bt1LesFbTranUnitType { get; set; }
        public Nullable<decimal> Bt1LesFbTranTotalUsage { get; set; }
        public Nullable<int> Bt1LesFbTranComTypes { get; set; }
        public Nullable<decimal> Bt1LesFbTranBillIncr { get; set; }
        public string NA6 { get; set; }
        public string NA7 { get; set; }
        public string NA8 { get; set; }
        public string Bt1LesFbBillno { get; set; }
        public string Bt1LesFbBillPeriod { get; set; }
        public Nullable<System.DateTime> Bt1LesFbBillDate { get; set; }
        public Nullable<decimal> Bt1LesFbCardFee { get; set; }
        public Nullable<decimal> Bt1LesFbActFee { get; set; }
        public Nullable<decimal> Bt1LesFbRgnFee { get; set; }
        public string Bt1LesGUID { get; set; }
    }
}
