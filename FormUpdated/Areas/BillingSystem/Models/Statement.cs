//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FormUpdated.Areas.BillingSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Statement
    {
        public int stmtID { get; set; }
        public string InvoiceNo { get; set; }
        public string Duration { get; set; }
        public string LES { get; set; }
        public string Customer { get; set; }
        public string Vessel { get; set; }
        public Nullable<decimal> TotalCP { get; set; }
        public Nullable<decimal> TotalSP { get; set; }
        public Nullable<int> Margin { get; set; }
        public Nullable<int> invFileID { get; set; }
        public Nullable<System.DateTime> IvoiceDate { get; set; }
        public string IMSIID { get; set; }
        public Nullable<decimal> SMTSCost { get; set; }
        public string Remarks { get; set; }
    }
}
