﻿using System.Web.Mvc;

namespace FormUpdated.Areas.BillingSystem
{
    public class BillingSystemAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "BillingSystem";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "BillingSystem_default",
                "BillingSystem/{controller}/{action}/{id}",
                new { Controller = "CDR", action = "OteSatCDR", id = UrlParameter.Optional }
                //new { action = "Create", id = UrlParameter.Optional },
                //new { controller = "CDR|CusRegs|LesRegs|Invoices|Rejections" },
                //new[] { "FormUpdated.Areas.BillingSystem.Controllers" }

            );
        }
    }
}