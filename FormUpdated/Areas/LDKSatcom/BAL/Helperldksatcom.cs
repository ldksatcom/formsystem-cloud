﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DocumentFormat.OpenXml.Packaging;
using FormUpdated.Areas.LDKSatcom.Models;

namespace FormUpdated.Areas.LDKSatcom
{
    public class Helperldksatcom
    {
        public static string GetOrderID()
        {
            string OrderKey = string.Empty;
            Guid obj = Guid.NewGuid();
            if (obj != null)
            {
                string[] guids = Convert.ToString(obj).Contains('-') ? Convert.ToString(obj).Split('-') : new string[0];
                if (guids.Length > 0 && !string.IsNullOrEmpty(guids[0]))
                    OrderKey = guids[0];

            }
            return OrderKey;
        }

        public static bool isDate(DateTime dateTime)
        {
            try
            {
                string[] formats = {"M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt",
                   "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss",
                   "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt",
                   "M/d/yyyy h:mm", "M/d/yyyy h:mm",
                   "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm"};

                DateTime parsedDateTime;
                return DateTime.TryParseExact(Convert.ToString(dateTime), formats, new System.Globalization.CultureInfo("en-US"),
                                               System.Globalization.DateTimeStyles.None, out parsedDateTime);
            }
            catch { return false; }
        }

        public static bool isTime(TimeSpan? input)
        {
            TimeSpan dummyOutput;
            return TimeSpan.TryParse(Convert.ToString(input), out dummyOutput);
        }

        public static bool isDecimal(decimal? value)
        {
            Decimal val;
            return Decimal.TryParse(Convert.ToString(value), out val);
        }

   
        private static List<string> GetColName()
        {
            List<string> lstCol = new List<string>();
            lstCol.Add("Crew Id");
            lstCol.Add("Crew Name");
            lstCol.Add("Crew Title");
            lstCol.Add("Crew Date of Joining");
            lstCol.Add("Crew Leaving Date");
            lstCol.Add("Access Level");
            lstCol.Add("Is Suspension");
            lstCol.Add("Username");
            lstCol.Add("Password");
            lstCol.Add("Email Id");
            lstCol.Add("Company Name");
            lstCol.Add("Ship Name");
            lstCol.Add("Type");
            lstCol.Add("To");
            lstCol.Add("CC");
            lstCol.Add("Is Admin");
            lstCol.Add("Order Key");
            lstCol.Add("Machine ID");
            return lstCol;
        }

        public static void WriteCrewDetails(List<CrewDetail> lstCrewDetails, string destination)
        {
            try
            {
                using (var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
                {
                    var workbookPart = workbook.AddWorkbookPart();

                    workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

                    workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();


                    var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                    var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                    sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                    DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                    string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                    uint sheetId = 1;
                    if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                    {
                        sheetId =
                            sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                    }

                    DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = "Noon Report" };
                    sheets.Append(sheet);

                    DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                    List<String> columns = new List<string>();
                    System.Data.DataTable table = new System.Data.DataTable();
                    List<string> arColName = GetColName();

                    DocumentFormat.OpenXml.UInt32Value rowIndex = 0;

                    foreach (string column in arColName)
                    {
                        columns.Add(column);

                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column);
                        headerRow.AppendChild(cell);
                    }
                    rowIndex++;
                    headerRow.RowIndex = rowIndex;
                    sheetData.AppendChild(headerRow);

                    int crewID = 0;

                    foreach (CrewDetail crew in lstCrewDetails)
                    {
                        // Create new Row in Excel sheet
                        DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                        foreach (string col in columns)
                        {
                            DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                            switch (col)
                            {
                                case "Crew Id":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(++crewID));
                                    newRow.AppendChild(cell);
                                    break;

                                case "Crew Name":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.Name);
                                    newRow.AppendChild(cell);
                                    break;

                                case "Crew Title":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.Title);
                                    newRow.AppendChild(cell);
                                    break;

                                case "Crew Date of Joining":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Date;

                                    if (crew.DateofJoining != null)
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToDateTime(crew.DateofJoining).Date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                    newRow.AppendChild(cell);
                                    break;
                                case "Crew Leaving Date":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Date;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToDateTime(crew.LeavingDate).Date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                                    newRow.AppendChild(cell);
                                    break;
                                case "Access Level":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (!string.IsNullOrEmpty(crew.AccessLevel))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.AccessLevel);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("partial");
                                    newRow.AppendChild(cell);
                                    break;
                                case "Is Suspension":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (crew.IsSuspension.Equals(true))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("1");
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("0");

                                    newRow.AppendChild(cell);
                                    break;
                                case "Username":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (!string.IsNullOrEmpty(crew.UserName))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.UserName);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();

                                    newRow.AppendChild(cell);
                                    break;
                                case "Password":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (!string.IsNullOrEmpty(crew.Password))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.Password);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(string.Empty);
                                    newRow.AppendChild(cell);
                                    break;
                                case "Email Id":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                    if (!string.IsNullOrEmpty(crew.EmailID))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.EmailID);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                    newRow.AppendChild(cell);
                                    break;
                                case "Company Name":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                    if (!string.IsNullOrEmpty(crew.CompanyName))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.CompanyName);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                    newRow.AppendChild(cell);
                                    break;

                                case "Ship Name":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (!string.IsNullOrEmpty(crew.ShipName))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.ShipName);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(string.Empty);
                                    newRow.AppendChild(cell);
                                    break;

                                case "Type":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (!string.IsNullOrEmpty(crew.Type))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.Type);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("Ship");
                                    newRow.AppendChild(cell);
                                    break;

                                case "To":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (!string.IsNullOrEmpty(crew.Tomail))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.Tomail);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                    newRow.AppendChild(cell);
                                    break;

                                case "CC":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                    if (!string.IsNullOrEmpty(crew.CCMail))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.CCMail);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                    newRow.AppendChild(cell);
                                    break;

                                case "Is Admin":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (crew.isAdmin.Equals(true))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString("1"));
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("0");
                                    newRow.AppendChild(cell);
                                    break;
                                case "Order Key":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (!string.IsNullOrEmpty(crew.OrderKey))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.OrderKey);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                    newRow.AppendChild(cell);
                                    break;

                                case "Machine ID":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (!string.IsNullOrEmpty(crew.MachineID))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.MachineID);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                    newRow.AppendChild(cell);
                                    break;
                            }
                        }

                        rowIndex++;
                        newRow.RowIndex = rowIndex;
                        // Append new row in Excel
                        sheetData.AppendChild(newRow);
                    }

                }
            }
            catch (System.Exception roor) { }
        }
    }
}