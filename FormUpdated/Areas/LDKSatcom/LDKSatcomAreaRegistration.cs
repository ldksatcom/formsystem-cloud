﻿using System.Web.Mvc;

namespace FormUpdated.Areas.LDKSatcom
{
    public class LDKSatcomAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "LDKSatcom";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "LDKSatcom_default",
                "LDKSatcom/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}