﻿using FormUpdated.Areas.LDKSatcom.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FormUpdated.Areas.LDKSatcom.Controllers
{
    public class DemoReportsController : Controller
    {
        private NoonReportFormatEntities db = new NoonReportFormatEntities();

        // GET: LDKSatcom/DemoReports
        public ActionResult shipDashboard(string companyName)
        {
            if (!string.IsNullOrEmpty(companyName))
            {
                MySession.curCompany = companyName;
                ViewBag.VesselNames = db.CrewDetails.Where(t => t.CompanyName.Equals(companyName)).Select(t => t.ShipName).Distinct().ToList();
            }
            else if (!string.IsNullOrEmpty(MySession.curCompany))
                ViewBag.VesselNames = db.CrewDetails.Where(t => t.CompanyName.Equals(MySession.curCompany)).Select(t => t.ShipName).Distinct().ToList();

            return View();
        }

        [AllowAnonymous]
        public ActionResult logout()
        {
            MySession.isAdmin = true;
            Response.Cache.SetNoStore();
            return RedirectToAction("Login", "demoNoonReports", new { area = "" });
            return Redirect("Login");
        }

        public ActionResult companyDashboard()
        {
            try
            {
                if (MySession.isAdmin)
                    ViewBag.CompanyNames = db.CrewDetails.Where(t => t.CompanyName != string.Empty).Select(t => t.CompanyName).Distinct().ToList();
                else
                    return RedirectToAction("shipDashboard", new { MySession.curCompany });
            }
            catch
            {
                ViewBag.CompanyNames = db.CrewDetails.Select(t => t.CompanyName).Distinct().ToList();
            }
            return View();
        }

        public ActionResult DashBoard(string shipName)
        {
            if (!string.IsNullOrEmpty(shipName))
            {
                TempData["VesselName"] = shipName;
                MySession.curVessel = shipName;
            }
            else
                TempData["VesselName"] = string.Empty;

            return View();
        }

        // GET: demoNoonReports
        public ActionResult Index(int id)
        {
            try
            {
                TempData["SelectReport"] = id;
                string shipName = string.Empty;
                if (TempData["VesselName"] != null && !string.IsNullOrEmpty(Convert.ToString(TempData["VesselName"])))
                {
                    shipName = TempData["VesselName"].ToString();
                    TempData.Keep("VesselName");
                }
                else
                {
                    shipName = MySession.curVessel;
                }

                if (!string.IsNullOrEmpty(shipName))
                {
                    if (id == 1)
                        return View(db.demoNoonReports.Where(t => t.ReportType.Equals("Morning") && t.ShipName.Equals(shipName)).OrderBy(t=> t.ReportDate).ToList());
                    else if (id == 2)
                        return View(db.demoNoonReports.Where(t => t.ReportType.Equals("Noon") && t.ShipName.Equals(shipName)).OrderBy(t => t.ReportDate).ToList());
                    else
                        return View(db.demoNoonReports.Where(t => t.ShipName.Equals(shipName)).OrderBy(t => t.ReportDate).ToList());
                }
                else
                    return View(db.demoNoonReports.Where(t => t.ShipName.Equals(shipName)).OrderBy(t => t.ReportDate).ToList());


            }
            catch (Exception error)
            {
                return View("DashBoard");
            }
        }

        [HttpGet]
        public virtual ActionResult Download(int id)
        {
            if (TempData["VesselName"] != null)
                TempData.Keep("VesselName");

            int ReportTypeID = 1;

            if (TempData["SelectReport"] != null)
                ReportTypeID = Convert.ToInt32(TempData["SelectReport"]);

            if (id > 0)
            {
                var attach = (from att in db.AttachFiles
                              where (att.NRid.Equals(id))
                              select new { att.FileNames, att.ContentType, att.Data }).ToList();
                if (attach != null)
                {
                    foreach (var attaches in attach)
                    {
                        return File(attaches.Data, attaches.ContentType, attaches.FileNames);
                    }
                    return RedirectToAction("Index", new { id = ReportTypeID });
                }
                else
                    return RedirectToAction("Index", new { id = ReportTypeID });
            }
            else
            {
                // Problem - Log the error, generate a blank file,
                //           redirect to another controller action - whatever fits with your application
                return RedirectToAction("Index", new { id = ReportTypeID });
            }
        }

        // GET: demoNoonReports/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            demoNoonReport demoNoonReport = db.demoNoonReports.Find(id);
            if (demoNoonReport == null)
            {
                return HttpNotFound();
            }
            return View(demoNoonReport);
        }

        // GET: demoNoonReports/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: demoNoonReports/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,ReportType,VoyageNo,ShipName,ReportDate,CallSign,Latitude,Longitude,NPTime,Remarks,VesselHeading,CTime,Remarks1,AverageSpeed_At_Noons,Forvoyagesincelastfullaway,MFOForMainEngine,MFOForBoiler,MGOForMainEngine,MGOForAuxiliaryEngines,MGOForBoiler,TotalMFO,TotalMGO,MFO,MGO,LOMainEnginesystem_Sump,LOMainEnginesystem_StorageTank")] demoNoonReport demoNoonReport)
        {
            if (ModelState.IsValid)
            {
                db.demoNoonReports.Add(demoNoonReport);
                db.SaveChanges();
                return RedirectToAction("Index", new { id = 3 });
            }

            return View(demoNoonReport);
        }

        // GET: demoNoonReports/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            demoNoonReport demoNoonReport = db.demoNoonReports.Find(id);
            if (demoNoonReport == null)
            {
                return HttpNotFound();
            }
            return View(demoNoonReport);
        }

        // POST: demoNoonReports/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,ReportType,VoyageNo,ShipName,ReportDate,CallSign,Latitude,Longitude,NPTime,Remarks,VesselHeading,CTime,Remarks1,AverageSpeed_At_Noons,Forvoyagesincelastfullaway,MFOForMainEngine,MFOForBoiler,MGOForMainEngine,MGOForAuxiliaryEngines,MGOForBoiler,TotalMFO,TotalMGO,MFO,MGO,LOMainEnginesystem_Sump,LOMainEnginesystem_StorageTank")] demoNoonReport demoNoonReport)
        {
            if (ModelState.IsValid)
            {
                db.Entry(demoNoonReport).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(demoNoonReport);
        }

        // GET: demoNoonReports/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            demoNoonReport demoNoonReport = db.demoNoonReports.Find(id);
            if (demoNoonReport == null)
            {
                return HttpNotFound();
            }
            return View(demoNoonReport);
        }

        // POST: demoNoonReports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            demoNoonReport demoNoonReport = db.demoNoonReports.Find(id);
            db.demoNoonReports.Remove(demoNoonReport);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult DownloadSetup()
        {
            System.IO.Compression.ZipArchive zip = null;
            try
            {
                //if (System.IO.File.Exists(Server.MapPath("~/zipfiles/bundle.zip")))
                //{
                //    System.IO.File.Delete(Server.MapPath
                //                  ("~/zipfiles/bundle.zip"));
                //}
                //zip = ZipFile.Open(Server.MapPath
                //         ("~/zipfiles/bundle.zip"), ZipArchiveMode.Create);

                if (System.IO.File.Exists(Server.MapPath("~/FormSystemExe/CrewDetails.xlsx")))
                {
                    System.IO.File.Delete(Server.MapPath
                                  ("~/FormSystemExe/CrewDetails.xlsx"));
                }

                string desinationPath = Server.MapPath("~/FormSystemExe/CrewDetails.xlsx");

                if (MySession.isAdmin)
                {
                    Helperldksatcom.WriteCrewDetails(db.CrewDetails.ToList(), desinationPath);
                }
                else if (MySession.AccessLevel.ToLower().Equals("full"))
                {
                    List<CrewDetail> crewDetails = db.CrewDetails.Where(t => t.CompanyName.Equals(MySession.curCompany)).ToList();
                    Helperldksatcom.WriteCrewDetails(crewDetails, desinationPath);
                }
                else if (MySession.AccessLevel.ToLower().Equals("partial"))
                {
                    List<CrewDetail> crewDetails = db.CrewDetails.Where(t => t.ShipName.Equals(MySession.curVessel)).ToList();
                    Helperldksatcom.WriteCrewDetails(crewDetails, desinationPath);
                }

                string[] files = System.IO.Directory.GetFiles(Server.MapPath("~/FormSystemExe"));

                List<string> downloads = new List<string>();

                foreach (string file in files)
                {
                    downloads.Add(System.IO.Path.GetFileName(file));
                }

                foreach (string file in downloads)
                {

                    //zip.CreateEntryFromFile(Server.MapPath
                    //     ("~/FormSystemExe/" + file), file);

                    AddFileToZip(Server.MapPath("~/zipfiles.zip"), Server.MapPath("~/FormSystemExe/" + file));
                }
                //zip.Dispose();
            }
            catch (Exception err)
            {
                //zip.Dispose();  
                return new EmptyResult();
            }
            //return File(Server.MapPath("~/zipfiles/bundle.zip"),
            //          "application/zip", "FormSystem.zip");
            return File(Server.MapPath("~/zipfiles.zip"),
                      "application/zip", "FormSystem.zip");
        }

        private const long BUFFER_SIZE = 4096;

        private static void AddFileToZip(string zipFilename, string fileToAdd)
        {
            //using (Package zip = System.IO.Packaging.Package.Open(zipFilename, FileMode.OpenOrCreate))
            //{
            //    string destFilename = ".\\" + Path.GetFileName(fileToAdd);
            //    Uri uri = PackUriHelper.CreatePartUri(new Uri(destFilename, UriKind.Relative));
            //    if (zip.PartExists(uri))
            //    {
            //        zip.DeletePart(uri);
            //    }
            //    PackagePart part = zip.CreatePart(uri, "", CompressionOption.Normal);
            //    using (FileStream fileStream = new FileStream(fileToAdd, FileMode.Open, FileAccess.Read))
            //    {
            //        using (Stream dest = part.GetStream())
            //        {
            //            CopyStream(fileStream, dest);
            //        }
            //    }
            //}
        }

        private static void CopyStream(System.IO.FileStream inputStream, System.IO.Stream outputStream)
        {
            long bufferSize = inputStream.Length < BUFFER_SIZE ? inputStream.Length : BUFFER_SIZE;
            byte[] buffer = new byte[bufferSize];
            int bytesRead = 0;
            long bytesWritten = 0;
            while ((bytesRead = inputStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                outputStream.Write(buffer, 0, bytesRead);
                bytesWritten += bufferSize;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}