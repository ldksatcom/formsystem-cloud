﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.LDKSatcom.Models;

namespace FormUpdated.Areas.LDKSatcom.Controllers
{
    public class CrewDetailsController : Controller
    {
        private NoonReportFormatEntities db = new NoonReportFormatEntities();

        // GET: LDKSatcom/CrewDetails
        public ActionResult Index()
        {
            return View(db.CrewDetails.ToList());
        }

        // GET: LDKSatcom/CrewDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CrewDetail crewDetail = db.CrewDetails.Find(id);
            if (crewDetail == null)
            {
                return HttpNotFound();
            }
            return View(crewDetail);
        }

        // GET: LDKSatcom/CrewDetails/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LDKSatcom/CrewDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Title,DateofJoining,LeavingDate,AccessLevel,IsSuspension,UserName,Password,EmailID,CompanyName,ShipName,Type,Tomail,CCMail,isAdmin,OrderKey,MachineID,isBilling")] CrewDetail crewDetail)
        {
            if (ModelState.IsValid)
            {
                db.CrewDetails.Add(crewDetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(crewDetail);
        }

        // GET: LDKSatcom/CrewDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CrewDetail crewDetail = db.CrewDetails.Find(id);
            if (crewDetail == null)
            {
                return HttpNotFound();
            }
            return View(crewDetail);
        }

        // POST: LDKSatcom/CrewDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Title,DateofJoining,LeavingDate,AccessLevel,IsSuspension,UserName,Password,EmailID,CompanyName,ShipName,Type,Tomail,CCMail,isAdmin,OrderKey,MachineID,isBilling")] CrewDetail crewDetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(crewDetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(crewDetail);
        }

        // GET: LDKSatcom/CrewDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CrewDetail crewDetail = db.CrewDetails.Find(id);
            if (crewDetail == null)
            {
                return HttpNotFound();
            }
            return View(crewDetail);
        }

        // POST: LDKSatcom/CrewDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CrewDetail crewDetail = db.CrewDetails.Find(id);
            db.CrewDetails.Remove(crewDetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
