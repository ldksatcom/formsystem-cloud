﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FormUpdated.Models
{
    public class EmailFormat
    {
        public Int32 ID { get; set; }
       
        [Required]
        [Display(Name ="To Email")]
        [DataType(DataType.EmailAddress)]
        public string Tomail { get; set; }

        [Display(Name ="CC Email")]
        [DataType(DataType.EmailAddress)]
        public string[] CCMail { get; set; }

        [Display(Name ="Subject")]
        public string Subject { get; set; }

        [Display(Name ="Body")]
        public string Body { get; set; }
        
        [Display(Name ="Attachements")]
        public string Attachement { get; set; }

        [Display(Name ="Compressed Percentage")]
        public string CompressedPercentage { get; set; }

        [Display(Name ="Original File Size")]
        public string OriginalFileSize { get; set; }

        [Display(Name ="Compressed File Size")]
        public string CompressedFileSize { get; set; }
    }
}