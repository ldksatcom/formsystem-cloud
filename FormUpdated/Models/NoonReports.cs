﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace FormUpdated.Models
{
   
    public  class NoonReports
    {
        public int ID { get; set; }
        [Display(Name = "Voyage No")]
        public string VoyageNo { get; set; }
        [Display(Name = "Ship Name")]
        public string ShipName { get; set; }

        //[Display(Name = "Date")]
        //[DataType(DataType.Date)]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/DD/YYYY}")]
        //public DateTime ReportDate { get; set; } 

        public string ReportDate { get; set; }

        [Display(Name = "Call Sign")]
        public string CallSign { get; set; }
        [Display(Name = "Latitude")]
        public String Latitude { get; set; }
        [Display(Name = "Longitude")]
        public string Longitude { get; set; }
        [Display(Name = "Time")]
        public TimeSpan NPTime { get; set; }
        [Display(Name = "Remarks")]
        public string Remarks { get; set; }
        [Display(Name = "Vessel Heading")]
        public string VesselHeading { get; set; }
        [Display(Name = "Time")]
        public TimeSpan CTime { get; set; }
        [Display(Name = "Remarks")]
        public string Remarks1 { get; set; }
        [Display(Name = "Average Speed")]
        public String AverageSpeed_At_Noons { get; set; }
        [Display(Name = "For Voyage since last full away")]
        public string Forvoyagesincelastfullaway { get; set; }
        [Display(Name = "At Noon")]
        public string AtNoon { get; set; }
        [Display(Name = "Remaining distance to pilot station")]
        public string C_rdpsdp { get; set; }
        [Display(Name = "For last 24 hours")]
        public string Forthelast24hrs { get; set; }
        [Display(Name = "For the voyage")]
        public string Forthevoyage { get; set; }

        //[Display(Name = "Date")]
        //[DataType(DataType.Date)]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/DD/YYYY}")]
        //public DateTime ETADate { get; set; }

        [Display(Name = "Date")]
        public string ETADate { get; set; }

        [Display(Name ="Local Time")]
        public TimeSpan LocalTime { get; set; }

        [Display(Name = "Country")]
        public string CountryName { get; set; }

        [NotMapped]
        public static int _countryID { get; set; }

        [NotMapped]
        public SelectList CountryID { get; set; } = new SelectList(GetCountry(), "ID", "Name", _countryID);

        public static IEnumerable<_Country> GetCountry()
        {
            List<_Country> lstCountry = new List<_Country>();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("select * from country",con);
                SqlDataReader sdr = cmd.ExecuteReader();
                while(sdr.Read())
                {
                    _Country objCountry = new _Country();
                    objCountry.ID = !sdr.IsDBNull(sdr.GetOrdinal("ID")) ? sdr.GetInt32(sdr.GetOrdinal("ID")) : 0;
                    objCountry.Name = !sdr.IsDBNull(sdr.GetOrdinal("Name")) ? sdr.GetString(sdr.GetOrdinal("Name")) : String.Empty;

                    if (objCountry.ID != 0)
                        lstCountry.Add(objCountry);
                }

            }
                return lstCountry.AsEnumerable();
        }

        [Display(Name ="Port")]
        public string Port { get; set; }

        [Display(Name ="For Main Engine")]
        public string MFOForMainEngine { get; set; }

        [Display(Name ="For Boiler")]
        public string MFOForBoiler { get; set; }

        [Display(Name ="For Main Engine")]
        public string MGOForMainEngine { get; set; }

        [Display(Name ="For Auxiliary Engines")]
        public string MGOForAuxiliaryEngines { get; set; }

        [Display(Name ="For Boiler")]
        public string MGOForBoiler { get; set; }

        [Display(Name ="Total MFO")]
        public string TotalMFO { get; set; }

        [Display(Name ="Total MGO")]
        public string TotalMGO { get; set; }

        [Display(Name ="MFO")]
        public string MFO { get; set; }
        [Display(Name ="MGO")]
        public string MGO { get; set; }

        [Display(Name ="Main Engine System Sump")]
        public Decimal? LOMainEnginesystem_Sump { get; set; }
        [Display(Name ="Main Engine System Storage Tank")]
        public Decimal? LOMainEnginesystem_StorageTank { get; set; }
        [Display(Name ="Main Engine CylinderOil")]
        public Decimal? LOMainEngineCylinderOil { get; set; }
        [Display(Name ="Auxiliary Engines")]
        public Decimal? LOAuxiliaryEngines { get; set; }
        [Display(Name ="Hydraulic Systems")]
        public Decimal? LOHydraulicSystem { get; set; }
        [Display(Name ="Main Engine Sump")]
        public Decimal? LOROPMainEnginesump { get; set; }
        [Display(Name ="ME SystemOil")]
        public Decimal? MEsystemoil { get; set; }
        [Display(Name ="Main Engine CylinderOil")]
        public Decimal? MainEnginecyclinderoil { get; set; }
        [Display(Name ="AE SystemOil")]
        public decimal? AEsystemoil { get; set; }
        [Display(Name ="Hydraulic System")]
        public decimal? LOROPHydraulicSystem { get; set; }
        [Display(Name ="FWG MeterReading")]
        public string Meterreading { get; set; }
        [Display(Name ="Running Hours")]
        public TimeSpan Runninghours { get; set; }
        [Display(Name ="Water Produced")]
        public string Waterproduced { get; set; }
        [Display(Name ="Total Water Used")]
        public string Totalwater { get; set; }
        [Display(Name ="APT(P&S)")]
        public string APT_PandS { get; set; }
        [Display(Name ="Feed WaterTank")]
        public string Feedwatertank { get; set; }
        [Display(Name ="CWT 3(p&S)")]
        public string CWT3_PandS { get; set; }
        [Display(Name ="Main")]
        public string Main { get; set; }
        [Display(Name ="FuelRackPosition For Units 1,2,3,4,5,6")]
        public string FuelRackPosUnits1t06 { get; set; }
        [Display(Name ="Exhaust Tempratures For Units 1,2,3,4,5,6")]
        public string Exhausttemptunit1to6 { get; set; }
        [Display(Name ="Main Engine")]
        public string DailyMainEngine { get; set; }
       
        public string AE2 { get; set; }
        
        public string AE1 { get; set; }
        public string Boiler { get; set; }
        [Display(Name ="Main Engine")]
        public string FuelMainEngine { get; set; }
        [Display(Name ="Auxiliary Engine")]
        public string FuelAuxiliraryEngine { get; set; }
        [Display(Name ="Boiler")]
        public String FuelBoiler { get; set; }
        [Display(Name ="Display ship has sufficient bunkers,lubes")]
        public bool AnswerYN { get; set; }
        public string Reason { get; set; }
        [Display(Name ="Event Company1")]
        public string EventCompany { get; set; }
        [Display(Name ="Event Name2")]
        public string EventName { get; set; }
        [Display(Name ="Event Purpose2")]
        public string EventPurpose { get; set; }

        //[Display(Name ="Event Date2")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/DD/YYYY}")]
        //public DateTime EventDate { get; set; }
        [Display(Name ="Event Date")]
        public string EventDate { get; set; }

        [Display(Name ="Event Time1")]
        public TimeSpan EventTimein { get; set; }
        [Display(Name ="Event Remarks1")]
        public string EventRemarks { get; set; }
        [Display(Name ="Event Company2")]
        public string Company2nd { get; set; }
        [Display(Name ="Event Name2")]
        public string Name2nd { get; set; }
        [Display(Name ="Event Purpose2")]
        public string Purpose2nd { get; set; }

        //[Display(Name ="Event Date2")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/DD/YYYY}")]
        //public DateTime Date_2nd { get; set; }

        [Display(Name ="Event Date2")]
        public string Date_2nd { get; set; }

        [Display(Name ="Event Time2")]
        public TimeSpan Timein2nd { get; set; }
        [Display(Name ="Event Remarks2")]
        public string Remarks2nd { get; set; }
        [Display(Name ="Crew Name1")]
        public string Name_1 { get; set; }
        [Display(Name ="Crew Rank1")]
        public String Rank_ { get; set; }
        [Display(Name ="Crew Department1")]
        public string Department { get; set; }

        //[Display(Name ="Crew Date1")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/DD/YYYY}")]
        //public DateTime Date1 { get; set; }

        [Display(Name = "Crew Date")]
        public string Date1 { get; set; }

        [Display(Name ="Crew Time1")]
        public TimeSpan Timein1 { get; set; }
        [Display(Name ="Crew Remarks1")]
        public string Remarks3 { get; set; }
        [Display(Name ="Crew Name2")]
        public string NameC { get; set; }
        [Display(Name ="Crew Rank2")]
        public string Rank_C { get; set; }
        [Display(Name ="Crew Department2")]
        public string DepartmentC { get; set; }

        //[Display(Name ="Crew Date2")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/DD/YYYY}")]
        //public DateTime DateC { get; set; }

        [Display(Name = "Crew Date2")]
        public string DateC { get; set; }

        [Display(Name ="Crew Time2")]
        public TimeSpan TimeinC { get; set; }
        [Display(Name ="Crew Remarks2")]
        public string RemarksC { get; set; }
        [Display(Name ="Supplier Company")]
        public string SuplierCompany { get; set; }
        [Display(Name ="Supplier Do")]
        public String SuplierDO { get; set; }
        [Display(Name ="Supplier Invoice")]
        public String SuplierInvoice { get; set; }
        [Display(Name ="Supplier Discription")]
        public String SuplierDiscription { get; set; }
        [Display(Name ="Supplier Remarks")]
        public string SuplierRemarks { get; set; }
        [Display(Name ="Item Delivery1")]
        public string ItemDelivery { get; set; }

        //[Display(Name ="Item Date1")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/DD/YYYY}")]
        //public DateTime ItemDate { get; set; }

        [Display(Name ="Item Date")]
        public string ItemDate { get; set; }

        [Display(Name ="Item Company1 ")]
        public string ItemCompany { get; set; }
        [Display(Name ="Item Discription1")]
        public string ItemDiscription { get; set; }
        [Display(Name ="Item PurchaseOrder1")]
        public string ItemPurchaseorder { get; set; }
        [Display(Name ="Item Qty1")]
        public string ItemQTY { get; set; }
        [Display(Name ="Item Delivery2")]
        public string DeliveryI { get; set; }

        //[Display(Name ="Item Date2 ")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/DD/YYYY}")]
        //public DateTime DateI { get; set; }

        [Display(Name ="Item Date2 ")]
        public string DateI { get; set; }

        [Display(Name ="Item Company2")]
        public string CompanyI { get; set; }
        [Display(Name ="Item Discription2")]
        public string DiscriptionI { get; set; }
        [Display(Name ="Item Qty2")]
        public string QTYI { get; set; }
        [Display(Name ="Item PurchaseOrder2")]
        public string PurchaseorderI { get; set; }
        public string ETA { get; set; }
        public string ETD { get; set; }


        


    }

    public class _Country
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }  
    }
}