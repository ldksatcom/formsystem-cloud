﻿using System;
using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data.Entity.Validation;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using FormUpdated.Areas.BillingSystem.Models;
using System.Configuration;
using System.Data;
using ClosedXML.Excel;

namespace FormUpdated
{
    public class BillingHelpers
    {
        public static string connectionstring { get; set; }
        private BillingSystemEntities db = new BillingSystemEntities();
        public BillingHelpers() { }
        public static bool IsValidTimeFormat(string input)
        {
            TimeSpan dummyOutput;
            return TimeSpan.TryParse(input, out dummyOutput);
        }

        public static string GenInvoice(bool isManualInv, int paramSP = 0)
        {
            string invno = string.Empty;
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString))
            using (var cmdd = new SqlCommand("GenerateInvNo", connection))
            {
                connection.Open();
                cmdd.CommandType = CommandType.StoredProcedure;
                if (paramSP > 0)
                    cmdd.Parameters.AddWithValue("@maxInvNo", paramSP);
                else
                    cmdd.Parameters.AddWithValue("@maxInvNo", 0);

                if(isManualInv)
                    cmdd.Parameters.AddWithValue("@isManual", 1);
                else
                    cmdd.Parameters.AddWithValue("@isManual", 0);

                SqlDataReader dataReader = cmdd.ExecuteReader();
                while (dataReader.Read())
                {
                    invno = (string)dataReader["Invoice_No"];
                    // BSt2A bSt2A = new BSt2A();
                    //bSt2A.InvoiceNo = invno;
                }
                dataReader.Close();
                connection.Close();
            }
            return invno;
        }

        public static void ExportDataSet(List<Areas.BillingSystem.Models.Statement> lstStatement, string destination)
        {
            try
            {
                using (var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
                {
                    var workbookPart = workbook.AddWorkbookPart();

                    workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

                    workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();

                    var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                    var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                    sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                    DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                    string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                    uint sheetId = 1;
                    if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                    {
                        sheetId =
                            sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                    }

                    DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = "List Of Statements" };
                    sheets.Append(sheet);

                    DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();



                    List<String> columns = new List<string>();
                    System.Data.DataTable table = new System.Data.DataTable();
                    List<string> arStmtName = GetStatementName();

                    foreach (string column in arStmtName)
                    {
                        columns.Add(column);

                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;
                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column);
                        headerRow.AppendChild(cell);
                    }

                    sheetData.AppendChild(headerRow);

                    foreach (Areas.BillingSystem.Models.Statement LstStatments in lstStatement)
                    {
                        DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                        foreach (string col in columns)
                        {
                            DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                            switch (col.ToLower())
                            {
                                case "invoice no":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(LstStatments.InvoiceNo);
                                    newRow.AppendChild(cell);
                                    break;

                                case "duration":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(LstStatments.Duration);
                                    //  cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToDateTime(LstStatments.Duration).Date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                                    newRow.AppendChild(cell);
                                    break;

                                //case "les":
                                //    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                //    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(LstStatments.LES);
                                //    newRow.AppendChild(cell);
                                //    break;

                                case "customer name":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(LstStatments.Customer);
                                    newRow.AppendChild(cell);
                                    break;

                                case "ship name":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(LstStatments.Vessel);
                                    newRow.AppendChild(cell);
                                    break;

                                case "total cost":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(LstStatments.TotalCP));
                                    newRow.AppendChild(cell);
                                    break;

                                case "inv amt(us$)":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(LstStatments.TotalSP));
                                    newRow.AppendChild(cell);
                                    break;

                                case "gross p/l":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(LstStatments.TotalSP - LstStatments.TotalCP));
                                    newRow.AppendChild(cell);
                                    break;

                                case "profit %":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(LstStatments.Margin) + "%");
                                    newRow.AppendChild(cell);
                                    break;

                                case "smts cost":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(LstStatments.SMTSCost));
                                    newRow.AppendChild(cell);
                                    break;

                                case "margin %":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(LstStatments.Margin) + "%");
                                    newRow.AppendChild(cell);
                                    break;

                                case "total smts costprice":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number;
                                    decimal SMTStotal_CP = Convert.ToDecimal((LstStatments.SMTSCost * LstStatments.Margin / 100) + LstStatments.SMTSCost);
                                    SMTStotal_CP = Math.Round(Convert.ToDecimal(SMTStotal_CP), 2, MidpointRounding.AwayFromZero);
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(SMTStotal_CP));
                                    newRow.AppendChild(cell);
                                    break;

                                case "remarks":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(LstStatments.Remarks));
                                    newRow.AppendChild(cell);
                                    break;
                                    //case "invoice date":
                                    //    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Date;
                                    //    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToDateTime(LstStatments.IvoiceDate).Date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                                    //    newRow.AppendChild(cell);
                                    //    break;

                                    //case "imsi id":
                                    //    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;
                                    //    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(LstStatments.IMSIID);
                                    //    newRow.AppendChild(cell);
                                    //    break;
                            }
                        }
                        sheetData.AppendChild(newRow);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private static List<string> GetStatementName()
        {
            List<string> lstCol = new List<string>();
            lstCol.Add("Customer Name");
            lstCol.Add("Ship Name");
            lstCol.Add("Invoice No");
            lstCol.Add("Duration");
            lstCol.Add("Inv Amt(US$)");
            lstCol.Add("Total Cost");
            lstCol.Add("Gross P/L");
            lstCol.Add("Profit %");
            lstCol.Add("SMTS Cost");
            lstCol.Add("Margin %");
            lstCol.Add("Total SMTS CostPrice");
            lstCol.Add("Remarks");

            //  lstCol.Add("LES");
            //  lstCol.Add("Total SP");
            //  lstCol.Add("Invoice Date");
            //  lstCol.Add("IMSI Id");

            return lstCol;
        }

        public Tuple<Stream, string> GnerateInvoice(string id)
        {
        //    System.IO.Stream spreadsheetstream = new System.IO.MemoryStream();
        //    List<BSt2A> bSt2 = db.BSt2A.ToList();
        //    List<BPtCustomer> bPtCustomers = db.BPtCustomers.ToList();

        //    string Invoiceno = string.Empty;

        //    int value = 0;

        //    if (int.TryParse(id, out value))
        //    {
        //        try
        //        {
        //            BSt2A bSt2A = db.BSt2A.FirstOrDefault(t => t.Bst2A_ID.Equals(value));
        //            string imsiID = string.Empty;
        //            string invoiceNo = string.Empty;
        //            string ImasatID = string.Empty;

        //            string buildingName = string.Empty;
        //            string street = string.Empty;
        //            string district = string.Empty;
        //            string city = string.Empty;
        //            string country = string.Empty;

        //            string VesselName = string.Empty;

        //            List<string> SDescription = new List<string>();
        //            Dictionary<string, decimal?> dictServiceDescription = new Dictionary<string, decimal?>();

        //            if (bSt2A != null)
        //            {
        //                imsiID = bSt2A.Bt2CusFbIMSIId;
        //                invoiceNo = bSt2A.InvoiceNo;
        //                ImasatID = bSt2A.Bt2CusFbMSIDNNo;
        //                if (!string.IsNullOrEmpty(imsiID))
        //                {
        //                    using (BillingSystemEntities db = new BillingSystemEntities())
        //                    {
        //                        VesselName = db.BPtShips.FirstOrDefault(t => t.BPtShipMMSI.Equals(imsiID)).BPtShipName;

        //                        var dataServType = db.BSt2B.Where(t => t.Bt2CusFbIMSIId.Equals(imsiID) && t.Bt2CusFbTranDServType != null)
        //                                                    .Select(t => t.Bt2CusFbTranDServType).Distinct().ToList();
        //                        foreach (string service in dataServType)
        //                        {
        //                            SDescription.Add(service);
        //                        }

        //                        var VceServType = db.BSt2B.Where(t => t.Bt2CusFbIMSIId.Equals(imsiID) && t.Bt2CusFbTranVServType != null)
        //                                                  .Select(t => t.Bt2CusFbTranVServType).Distinct().ToList();
        //                        foreach (string service in VceServType)
        //                        {
        //                            SDescription.Add(service);
        //                        }

        //                        foreach (var service in SDescription)
        //                        {
        //                            if (!string.IsNullOrEmpty(service))
        //                            {
        //                                if (service.ToLower().Equals("subscription fee"))
        //                                {
        //                                    try
        //                                    {
        //                                        decimal? totalAmountData = db.BSt2B.FirstOrDefault(t => t.Bt2CusFbTranDServType != null &&
        //                                                            t.Bt2CusFbTranDServType.Equals(service) && t.Bt2CusFbIMSIId.Equals(imsiID)).Bt2CusFbRecurFee;

        //                                        totalAmountData = Math.Round(Convert.ToDecimal(totalAmountData), 2, MidpointRounding.AwayFromZero);

        //                                        dictServiceDescription.Add(service, totalAmountData);
        //                                    }
        //                                    catch { }
        //                                }

        //                                if (service.ToLower().Equals("standard ip"))
        //                                {
        //                                    decimal? totalAmountData = db.BSt2B.AsEnumerable().Where(t => t.Bt2CusFbTranDServType != null &&
        //                                                        t.Bt2CusFbTranDServType.Equals(service) && t.Bt2CusFbIMSIId.Equals(imsiID) && t.Bt2CusFbisOutBundle)
        //                                                        .GroupBy(t => t.Bt2CusFbIMSIId.Equals(imsiID))
        //                                                        .Select(t => t.Sum(s => s.Bt2CusFbTranDUsage)).FirstOrDefault();
        //                                    totalAmountData = Math.Round(Convert.ToDecimal(totalAmountData), 2, MidpointRounding.AwayFromZero);

        //                                    dictServiceDescription.Add(service, totalAmountData);
        //                                }

        //                                if (service.ToLower().Contains("voice"))
        //                                {
        //                                    decimal? totalAmountVce = db.BSt2B.AsEnumerable().Where(t => t.Bt2CusFbTranVServType != null &&
        //                                                                                            t.Bt2CusFbTranVServType.Equals(service) && t.Bt2CusFbIMSIId.Equals(imsiID))
        //                                                                                     .GroupBy(t => t.Bt2CusFbIMSIId.Equals(imsiID))
        //                                                                                     .Select(t => t.Sum(s => s.Bt2CusFbTranVUsage)).FirstOrDefault();
        //                                    totalAmountVce = Math.Round(Convert.ToDecimal(totalAmountVce), 2, MidpointRounding.AwayFromZero);

        //                                    dictServiceDescription.Add(service, totalAmountVce);
        //                                }
        //                            }
        //                        }
        //                    }

        //                    string CusAddress = bSt2A.Bt2CusFbCustomerAddress;
        //                    if (!string.IsNullOrEmpty(CusAddress))
        //                    {
        //                        string[] addressparts = CusAddress.Split(',');

        //                        buildingName = addressparts.Length > 0 ? addressparts[0] : string.Empty;
        //                        street = addressparts.Length > 1 ? addressparts[1] : string.Empty;
        //                        district = addressparts.Length > 2 ? addressparts[2] : string.Empty;
        //                        city = addressparts.Length > 3 ? addressparts[3] : string.Empty;
        //                        country = addressparts.Length > 4 ? addressparts[4] : string.Empty;
        //                    }
        //                }

        //                DateTime dt = DateTime.Now;
        //                string Period = dt.ToString("MMM");
        //                DateTime dy = DateTime.Today;
        //                string currYear = dy.Year.ToString();

        //                string CurrPeriod = Period + '-' + currYear;
        //                string InvoiceFileName = invoiceNo + '_' + buildingName + '_' + VesselName;

        //                string pathid = Guid.NewGuid().ToString() + " " + DateTime.Now.Date.ToString("dd-MM-yyyy");

        //                XLWorkbook xLWorkbook = new XLWorkbook();
        //                IXLWorksheet workSheet = xLWorkbook.Worksheets.Add("Invoice" + Invoiceno);

        //                workSheet.Column(1).Width = 0.83;
        //                workSheet.Column(2).Width = 1.50;
        //                workSheet.Column(3).Width = 0.46;
        //                workSheet.Column(4).Width = 1.50;
        //                workSheet.Column(5).Width = 1.50;
        //                workSheet.Column(6).Width = 2.71;
        //                workSheet.Column(7).Width = 0.54;
        //                workSheet.Column(8).Width = 4.25;
        //                workSheet.Column(9).Width = 1.50;
        //                workSheet.Column(10).Width = 1.50;
        //                workSheet.Column(11).Width = 1.50;
        //                workSheet.Column(12).Width = 1.50;
        //                workSheet.Column(13).Width = 9.00;
        //                workSheet.Column(14).Width = 0.54;
        //                workSheet.Column(15).Width = 0.69;
        //                workSheet.Column(16).Width = 0.54;
        //                workSheet.Column(17).Width = 0.62;
        //                workSheet.Column(18).Width = 0.69;
        //                workSheet.Column(19).Width = 0.00;
        //                workSheet.Column(20).Width = 0.00;
        //                workSheet.Column(21).Width = 1.50;
        //                workSheet.Column(22).Width = 1.50;
        //                workSheet.Column(23).Width = 1.50;
        //                workSheet.Column(24).Width = 1.50;
        //                workSheet.Column(25).Width = 1.50;
        //                workSheet.Column(26).Width = 0.38;
        //                workSheet.Column(27).Width = 1.50;
        //                workSheet.Column(28).Width = 1.50;
        //                workSheet.Column(29).Width = 0.00;
        //                workSheet.Column(30).Width = 0.38;
        //                workSheet.Column(31).Width = 0.45;
        //                workSheet.Column(32).Width = 0.83;
        //                workSheet.Column(33).Width = 3.38;
        //                workSheet.Column(34).Width = 2.75;
        //                workSheet.Column(35).Width = 0.54;
        //                workSheet.Column(36).Width = 0.83;
        //                workSheet.Column(37).Width = 1.00;
        //                workSheet.Column(38).Width = 0.46;
        //                workSheet.Column(39).Width = 1.50;
        //                workSheet.Column(40).Width = 1.50;
        //                workSheet.Column(41).Width = 1.50;
        //                workSheet.Column(42).Width = 1.50;

        //                // Heading
        //                workSheet.Range("A3:AP3").Merge();

        //                //old code
        //                //workSheet.Cell(2, 1).Style.Font.Bold = true;
        //                //workSheet.Cell(4, 1).Style.Font.Bold = false;

        //                workSheet.Row(1).Height = 57.75;
        //                workSheet.Row(2).Height = 12.75;
        //                workSheet.Row(3).Height = 15.00;
        //                workSheet.Row(4).Height = 15.00;
        //                workSheet.Row(5).Height = 15.00;
        //                workSheet.Row(6).Height = 12.75;
        //                workSheet.Row(7).Height = 14.25;
        //                workSheet.Row(8).Height = 12.00;
        //                workSheet.Row(9).Height = 12.00;
        //                workSheet.Row(10).Height = 13.50;
        //                workSheet.Row(11).Height = 11.25;

        //                //---------------- Style Start --------------------------
        //                workSheet.Row(4).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(5).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(6).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(7).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(8).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(9).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(10).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(11).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(12).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(13).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(14).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(15).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(16).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(17).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(18).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(19).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(26).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(27).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(28).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(29).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(30).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(31).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(32).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(33).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(35).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(36).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(38).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(39).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(40).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(41).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(42).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(43).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(44).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(45).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(46).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(47).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(48).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(49).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(50).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(51).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(52).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(53).Style.Font.FontName = "Times New Roman";

        //                //--------------- Style End -------------------------------


        //                workSheet.Row(6).Style.Font.Bold = false;
        //                workSheet.Row(13).Style.Font.Bold = false;
        //                int j = 6;
        //                while (j < 78)
        //                {
        //                    workSheet.Row(j).Style.Font.FontSize = 10;
        //                    j++;
        //                }

        //                string Imagepath = MySession.InvoiceLogoPath;

        //                //IXLRanges myrange = (IXLRanges)workSheet.Range("A1:AO");

        //                var image = workSheet.AddPicture(Imagepath)
        //                              .MoveTo(workSheet.Cell(1, 1)).Scale(1.05);

        //                //workSheet.Cell(1, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

        //                //workSheet.Cell(1, 7).Value = "Pte Ltd";
        //                //workSheet.Cell(1, 7).Style.Font.Bold = true;
        //                //workSheet.Cell(1, 7).Style.Font.FontSize = 20;
        //                //workSheet.Cell(1, 7).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Top);

        //                //workSheet.Range("A2:B2").Merge();
        //                //workSheet.Cell(2, 2).Style.Font.Bold = true;

        //                //workSheet.Cell(6, 3).Style.Font.FontSize = 20;

        //                //workSheet.Cell(3, 1).Value = "32 Old Toh Tuck Road,#05-01 Ibiz Centre,Singapore 597658 Tel: +65 66726090 Fax: +65 64651656 Email: accounts@smtspl.com";
        //                //workSheet.Cell(4, 1).Value = "Mailing Address: Bukit Batok Central Post Office,PO Box 181,Singapore 916507";
        //                //workSheet.Cell(4, 1).Style.Font.Bold = false;
        //                //workSheet.Cell(5, 1).Value = "Co.Reg.No/GST No: 199901546C";
        //                //workSheet.Cell(5, 1).Style.Font.Bold = true;

        //                // Invoice Header
        //                workSheet.Range("A3:AO3").Merge();
        //                workSheet.Cell(3, 1).Value = "Invoice";
        //                workSheet.Cell(3, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                workSheet.Cell(3, 1).Style.Font.FontSize = 14;
        //                workSheet.Row(3).Style.Font.FontName = "Times New Roman";
        //                workSheet.Row(3).Style.Font.Bold = true;

        //                // Invoice Number format
        //                workSheet.Cell(4, 24).Value = "Invoice No :";
        //                workSheet.Cell(4, 24).Style.Font.Bold = true;
        //                //workSheet.Cell(4, 24).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
        //                workSheet.Range("AE4:AO4").Merge();
        //                workSheet.Cell(4, 31).Value = invoiceNo;
        //                workSheet.Cell(4, 31).Style.Font.Bold = true;

        //                // Invoice Date
        //                workSheet.Cell(5, 24).Value = "Date :";
        //                workSheet.Range("AE5:AO5").Merge();
        //                workSheet.Cell(5, 31).Value = DateTime.Now.Date.ToString();
        //                workSheet.Cell(5, 31).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);

        //                // Vessel and To
        //                workSheet.Cell(6, 24).Value = "Vessel :";
        //                //workSheet.Cell(6, 24).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
        //                workSheet.Range("AE6:AO6").Merge();
        //                workSheet.Cell(6, 31).Value = VesselName;
        //                workSheet.Cell(4, 1).Value = "To:";
        //                workSheet.Range("D4:V4").Merge();

        //                // Address
        //                workSheet.Range("D5:V5").Merge();
        //                workSheet.Cell(5, 4).Value = buildingName.ToString();
        //                workSheet.Cell(5, 4).Style.Font.Bold = true;
        //                //workSheet.Cell(5, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
        //                workSheet.Range("D6:V6").Merge();
        //                workSheet.Cell(6, 4).Value = street.ToString();
        //                workSheet.Range("D7:V7").Merge();
        //                workSheet.Cell(7, 4).Value = district.ToString();
        //                //workSheet.Cell(7, 4).Style.Font.Bold = false;
        //                workSheet.Range("D8:V8").Merge();
        //                workSheet.Cell(8, 4).Value = city.ToString();
        //                workSheet.Range("D9:V9").Merge();
        //                workSheet.Cell(9, 4).Value = country.ToString();

        //                // Account Code
        //                workSheet.Cell(7, 24).Value = "Account Code :";
        //                workSheet.Range("AE7:AO7").Merge();

        //                // Accounts Department
        //                workSheet.Cell(10, 1).Value = " Attn";
        //                workSheet.Cell(10, 4).Value = "Accounts Department";

        //                // One Space Row
        //                workSheet.Row(11).Height = 18;
        //                workSheet.Cell(11, 1).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Top);

        //                workSheet.Cell(12, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                workSheet.Cell(12, 1).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                workSheet.Cell(12, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                workSheet.Cell(12, 4).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                workSheet.Cell(12, 8).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                workSheet.Cell(12, 8).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                workSheet.Cell(12, 15).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                workSheet.Cell(12, 15).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                workSheet.Cell(12, 36).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                workSheet.Cell(12, 36).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);

        //                workSheet.Range("A12:B12").Merge();
        //                workSheet.Cell(12, 1).Value = "Sn";

        //                workSheet.Range("D12:F12").Merge();
        //                workSheet.Cell(12, 4).Value = "Period";

        //                workSheet.Range("H12:M12").Merge();
        //                workSheet.Cell(12, 8).Value = "InmarSat ID";

        //                workSheet.Range("O12:AH12").Merge();
        //                workSheet.Cell(12, 15).Value = "Service Description";

        //                workSheet.Range("AJ12:AO12").Merge();
        //                workSheet.Cell(12, 36).Value = "Amount";

        //                workSheet.Cell(12, 1).Style.Font.Bold = true;
        //                workSheet.Cell(12, 4).Style.Font.Bold = true;
        //                workSheet.Cell(12, 8).Style.Font.Bold = true;
        //                workSheet.Cell(12, 15).Style.Font.Bold = true;
        //                workSheet.Cell(12, 36).Style.Font.Bold = true;

        //                workSheet.Cell(12, 1).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 1).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 1).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 1).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 2).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 2).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 2).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 5).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 5).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 6).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 6).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 6).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 33).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 33).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 34).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 34).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 34).Style.Border.RightBorder = XLBorderStyleValues.Medium;

        //                workSheet.Cell(12, 4).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 4).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 4).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 4).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 9).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 9).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 10).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 10).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 11).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 11).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 12).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 12).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 13).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 13).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 13).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 13).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 8).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 8).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 8).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 8).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 15).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 15).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 15).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 15).Style.Border.RightBorder = XLBorderStyleValues.Medium;

        //                workSheet.Cell(12, 16).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 16).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 17).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 17).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 18).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 18).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 19).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 19).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 20).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 20).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 21).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 21).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 22).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 22).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 23).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 23).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 24).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 24).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 25).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 25).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 26).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 26).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 27).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 27).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 28).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 28).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 29).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 29).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 30).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 30).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 31).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 31).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 32).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 32).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 32).Style.Border.RightBorder = XLBorderStyleValues.Medium;

        //                workSheet.Cell(12, 36).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 36).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 36).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 36).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 37).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 37).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 38).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 38).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 39).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 39).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 40).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 40).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 41).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 41).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheet.Cell(12, 41).Style.Border.RightBorder = XLBorderStyleValues.Medium;

        //                workSheet.Cell(12, 1).Style.Font.FontSize = 12;
        //                workSheet.Cell(12, 4).Style.Font.FontSize = 12;
        //                workSheet.Cell(12, 8).Style.Font.FontSize = 12;
        //                workSheet.Cell(12, 15).Style.Font.FontSize = 12;
        //                workSheet.Cell(12, 36).Style.Font.FontSize = 12;

        //                // Empty Space Row
        //                workSheet.Row(13).Height = 6;

        //                workSheet.Cell(14, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                workSheet.Cell(14, 1).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                workSheet.Cell(14, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                workSheet.Cell(14, 2).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                workSheet.Cell(14, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                workSheet.Cell(14, 3).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                workSheet.Cell(14, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                workSheet.Cell(14, 4).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                workSheet.Cell(14, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                workSheet.Cell(14, 5).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);

        //                int row = 14;
        //                decimal? totals = 0;
        //                int sno = 1;
        //                foreach (var services in dictServiceDescription)
        //                {
        //                    workSheet.Range("A" + row + ":B" + row).Merge();
        //                    workSheet.Cell(row, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                    workSheet.Cell(row, 1).Value = sno++;

        //                    workSheet.Range("D" + row + ":F" + row).Merge();
        //                    workSheet.Cell(row, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                    workSheet.Cell(row, 4).Style.NumberFormat.Format = "@";
        //                    workSheet.Cell(row, 4).Value = CurrPeriod;

        //                    workSheet.Range("H" + row + ":M" + row).Merge();
        //                    workSheet.Cell(row, 8).Style.NumberFormat.Format = "@";
        //                    workSheet.Cell(row, 8).Value = ImasatID;

        //                    workSheet.Range("O" + row + ":AH" + row).Merge();
        //                    workSheet.Cell(row, 15).Value = services.Key;

        //                    workSheet.Range("AJ" + row + ":AO" + row).Merge();
        //                    workSheet.Cell(row, 36).Value = services.Value;

        //                    totals += services.Value;
        //                    row++;
        //                }

        //                // workSheet.Cells[13, 7].Style.Font.Size = 12;
        //                workSheet.SheetView.Freeze(12, 0);
        //                // workSheet.Row(15).Style.Font.Bold = false;
        //                // workSheet.Row(17).Style.Font.Bold = true;
        //                workSheet.Cell(18, 6).Style.Font.Bold = true;
        //                workSheet.Row(20).Style.Font.Bold = true;
        //                workSheet.Row(21).Style.Font.Bold = true;
        //                workSheet.Row(22).Style.Font.Bold = true;
        //                workSheet.Row(23).Style.Font.Bold = true;

        //                // workSheet.Cell(31, 22).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                //workSheet.Cell(31, 22).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                workSheet.Cell(31, 22).Value = "Total";

        //                workSheet.Cell(31, 36).Value = totals;
        //                workSheet.Cell(31, 36).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
        //                workSheet.Cell(31, 37).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
        //                workSheet.Cell(31, 38).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
        //                workSheet.Cell(31, 39).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
        //                workSheet.Cell(31, 40).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
        //                workSheet.Cell(31, 41).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);

        //                workSheet.Cell(32, 22).Value = "Exchange Rate";
        //                workSheet.Cell(33, 22).Value = "Administrative Fee";
        //                workSheet.Cell(33, 31).Value = "%";
        //                workSheet.Cell(33, 36).Value = "-";
        //                workSheet.Cell(33, 36).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
        //                workSheet.Row(38).Style.Font.FontSize = 9;
        //                workSheet.Row(39).Style.Font.FontSize = 9;
        //                workSheet.Row(40).Style.Font.FontSize = 9;
        //                workSheet.Row(41).Style.Font.FontSize = 9;
        //                workSheet.Row(42).Style.Font.FontSize = 9;
        //                workSheet.Row(43).Style.Font.FontSize = 9;
        //                workSheet.Row(44).Style.Font.FontSize = 9;
        //                workSheet.Row(45).Style.Font.FontSize = 9;
        //                workSheet.Row(46).Style.Font.FontSize = 9;
        //                workSheet.Row(47).Style.Font.FontSize = 9;
        //                workSheet.Row(48).Style.Font.FontSize = 9;
        //                workSheet.Row(49).Style.Font.FontSize = 9;
        //                workSheet.Row(50).Style.Font.FontSize = 9;
        //                workSheet.Row(51).Style.Font.FontSize = 9;
        //                workSheet.Row(52).Style.Font.FontSize = 9;
        //                workSheet.Row(53).Style.Font.FontSize = 9;
        //                workSheet.Cell(35, 27).Value = "Good Service Tax %";
        //                workSheet.Cell(36, 22).Value = "Total Payable";
        //                workSheet.Cell(36, 32).Value = "(USD)";
        //                workSheet.Range("AJ31:AO31").Merge();
        //                workSheet.Range("AJ32:AO32").Merge();
        //                workSheet.Range("AJ33:AO33").Merge();
        //                workSheet.Range("AJ35:AO35").Merge();
        //                workSheet.Range("AJ36:AO36").Merge();
        //                workSheet.Cell(36, 36).Value = totals;
        //                workSheet.Cell(36, 36).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
        //                workSheet.Cell(36, 37).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
        //                workSheet.Cell(36, 38).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
        //                workSheet.Cell(36, 39).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
        //                workSheet.Cell(36, 40).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
        //                workSheet.Cell(36, 41).Style.Border.SetTopBorder(XLBorderStyleValues.Thin);

        //                workSheet.Cell(36, 22).Style.Font.Bold = true;
        //                workSheet.Cell(36, 32).Style.Font.Bold = true;
        //                workSheet.Cell(36, 35).Style.Font.Bold = true;
        //                workSheet.Range("A37:AO37").Merge();
        //                workSheet.Range("A37:AO37").Style.Border.BottomBorder = XLBorderStyleValues.Thick;

        //                workSheet.Cell(38, 2).Value = "Important Notes";
        //                workSheet.Cell(38, 2).Style.Font.Bold = true;
        //                workSheet.Cell(38, 24).Value = "For Comptroller Purposes Only,Rate @ 1.37";
        //                workSheet.Cell(39, 2).Value = "1";
        //                workSheet.Cell(39, 4).Value = "Credit Terms: 30 days from Date of Invoice";
        //                workSheet.Cell(40, 2).Value = "2";
        //                workSheet.Cell(40, 4).Value = "Please quote clearly your account and invoice no when making payment";
        //                workSheet.Cell(41, 2).Value = "3";
        //                workSheet.Cell(41, 4).Value = "All Bank charges to be borne by remitter.";

        //                workSheet.Cell(42, 2).Value = "4";
        //                workSheet.Cell(42, 4).Value = "Please send all checks payment to";
        //                workSheet.Cell(43, 4).Value = "Bukit Batok Central Post Office PO Box 181 Singapore 916507";

        //                workSheet.Cell(44, 2).Value = "5";
        //                workSheet.Cell(44, 4).Value = "Payment by Telegraphic Transfer can also be made to";
        //                workSheet.Cell(45, 4).Value = "Beneficiary";
        //                workSheet.Cell(45, 14).Value = "SMTS Pte Ltd";
        //                workSheet.Cell(45, 14).Style.Font.Bold = true;
        //                workSheet.Cell(46, 4).Value = "USD Account no";
        //                workSheet.Cell(46, 14).Value = "651-000093-301";
        //                workSheet.Cell(46, 14).Style.Font.Bold = true;
        //                workSheet.Cell(47, 4).Value = "OCBC Swift Code";
        //                workSheet.Cell(47, 14).Value = "OCBCSGSG";
        //                workSheet.Cell(47, 14).Style.Font.Bold = true;
        //                workSheet.Cell(48, 4).Value = "Bank";
        //                workSheet.Cell(48, 14).Value = "Oversea-Chinese Banking Corporation Limited";
        //                workSheet.Cell(48, 14).Style.Font.Bold = true;
        //                workSheet.Cell(49, 4).Value = "Address";
        //                workSheet.Cell(49, 14).Value = "10 Marina Boulevard #01-04 Marina Bay Financial Center Tower 2";
        //                workSheet.Cell(49, 14).Style.Font.Bold = true;
        //                workSheet.Cell(50, 14).Value = "Singapore 018983";
        //                workSheet.Cell(50, 14).Style.Font.Bold = true;
        //                workSheet.Cell(51, 4).Value = "Agent Swift Code";
        //                workSheet.Cell(51, 14).Value = "CHASUS33";
        //                workSheet.Cell(51, 14).Style.Font.Bold = true;
        //                workSheet.Cell(52, 4).Value = "Agent Bank";
        //                workSheet.Cell(52, 14).Value = "JP Morgan";
        //                workSheet.Cell(52, 14).Style.Font.Bold = true;
        //                workSheet.Cell(53, 2).Value = "6";
        //                workSheet.Cell(53, 4).Value = "Please make full payment by the due date to avoid 1.5% interest for late payment charge per month";
        //                workSheet.Cell(53, 4).Style.Font.Bold = true;

        //                // Save Work sheet into workbook
        //                xLWorkbook.SaveAs(spreadsheetstream);
        //                spreadsheetstream.Position = 0;

        //                //InvoiceFileName = temppath + "\\" + InvoiceFileName;

        //                //FileStream fileStream = new FileStream(InvoiceFileName, FileMode.Create);

        //                return new Tuple<Stream, string>(spreadsheetstream, InvoiceFileName);

        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            //throw ex;
        //            return new Tuple<Stream, string>(spreadsheetstream, string.Empty);
        //        }
        //    }
            return new Tuple<Stream, string>(null, string.Empty);
        }

        public Tuple<Stream, string> GnerateCalllog(string id)
        {
        //    Stream SpreadsheetStream = new MemoryStream();

        //    List<BCtB> bctb = new List<BCtB>();
        //    List<BPtCustomer> bPtCustomers = db.BPtCustomers.ToList();

        //    int value = 0;
        //    string imsiID = string.Empty;
        //    string InvoiceNo = string.Empty;
        //    string CustomerAddress = string.Empty;
        //    string customerName = string.Empty;
        //    string CallLogName = string.Empty;
        //    string VesselName = string.Empty;

        //    if (int.TryParse(id, out value))
        //    {
        //        try
        //        {
        //            BSt2A bSt2A = db.BSt2A.FirstOrDefault(t => t.Bst2A_ID.Equals(value));
        //            if (bSt2A != null)
        //            {
        //                imsiID = bSt2A.Bt2CusFbIMSIId;
        //                InvoiceNo = bSt2A.InvoiceNo;
        //                CustomerAddress = bSt2A.Bt2CusFbCustomerAddress;
        //                VesselName = db.BPtShips.FirstOrDefault(t => t.BPtShipMMSI.Equals(imsiID)).BPtShipName;

        //                if (!string.IsNullOrEmpty(CustomerAddress))
        //                {
        //                    string[] addressparts = CustomerAddress.Split(',');

        //                    customerName = addressparts.Length > 0 ? addressparts[0] : string.Empty;
        //                    //street = addressparts.Length > 1 ? addressparts[1] : string.Empty;
        //                    //district = addressparts.Length > 2 ? addressparts[2] : string.Empty;
        //                    //city = addressparts.Length > 3 ? addressparts[3] : string.Empty;
        //                    //country = addressparts.Length > 4 ? addressparts[4] : string.Empty;
        //                }

        //                CallLogName = InvoiceNo + 'C' + '_' + customerName + '_' + VesselName;

        //                //ExcelPackage excel = new ExcelPackage();                        
        //                XLWorkbook xLWorkbook = new XLWorkbook();

        //                // Excel Sheet for CallLog Data 
        //                #region CallLog Data
        //                IXLWorksheet workSheetData = xLWorkbook.Worksheets.Add("Data");

        //                workSheetData.Column(1).Width = 16;
        //                workSheetData.Column(2).Width = 15;
        //                workSheetData.Column(3).Width = 25;
        //                workSheetData.Column(4).Width = 10;
        //                workSheetData.Column(5).Width = 10;
        //                workSheetData.Column(6).Width = 10;
        //                workSheetData.Column(7).Width = 10;
        //                workSheetData.Column(8).Width = 10;
        //                workSheetData.Cell(2, 1).Style.Font.Bold = true;
        //                workSheetData.Cell(4, 1).Style.Font.Bold = true;
        //                workSheetData.Cell(11, 1).Style.Font.Bold = true;
        //                workSheetData.Cell(12, 1).Style.Font.Bold = true;
        //                workSheetData.Cell(12, 2).Style.Font.Bold = true;
        //                workSheetData.Cell(3, 6).Style.Font.Bold = true;
        //                workSheetData.Row(6).Style.Font.Bold = true;

        //                int j = 6;
        //                while (j < 78)
        //                {
        //                    workSheetData.Row(j).Style.Font.FontSize = 10;
        //                    j++;
        //                }

        //                string Imagepath = MySession.InvoiceLogoPath;
        //                var image = workSheetData.AddPicture(Imagepath)
        //                             .MoveTo(workSheetData.Cell(2, 1)).Scale(1.05);

        //                //workSheet.Cell(2, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

        //                //workSheet.Cell(2, 1).Value = "Pte Ltd";
        //                //workSheet.Cell(2, 1).Style.Font.Bold = true;
        //                //workSheet.Cell(2, 1).Style.Font.FontSize = 20;
        //                workSheetData.Range("A2:B2").Merge();
        //                // workSheet.Cell(2, 1).Value = "SMTS Pte Ltd";
        //                //workSheet.Cell(2, 1).Style.Font.FontSize = 20;
        //                workSheetData.Cell(4, 1).Value = "Call Data Details";
        //                workSheetData.Cell(4, 1).Style.Font.FontSize = 20;
        //                workSheetData.Row(12).Style.Font.Bold = false;
        //                workSheetData.Cell(6, 1).Value = "Invoice No";
        //                workSheetData.Cell(6, 2).Value = InvoiceNo;
        //                workSheetData.Cell(6, 5).Value = "Customer";
        //                workSheetData.Cell(6, 6).Value = customerName;
        //                workSheetData.Row(6).Style.Font.Bold = true;
        //                workSheetData.Cell(7, 1).Value = "Invoice Date";
        //                workSheetData.Cell(7, 2).Value = DateTime.Now.Date;
        //                workSheetData.Cell(7, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);

        //                workSheetData.Cell(7, 5).Value = "LES";
        //                // workSheet.Cell(7, 6).Value = LES.ToString();
        //                workSheetData.Row(7).Style.Font.Bold = true;

        //                workSheetData.Cell(8, 5).Value = "Vessel";
        //                workSheetData.Cell(8, 6).Value = VesselName;
        //                workSheetData.Row(8).Style.Font.Bold = true;

        //                workSheetData.Cell(10, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                workSheetData.Cell(10, 1).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                workSheetData.Cell(10, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                workSheetData.Cell(10, 2).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                workSheetData.Cell(10, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                workSheetData.Cell(10, 3).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                workSheetData.Cell(10, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                workSheetData.Cell(10, 4).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                workSheetData.Cell(10, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                workSheetData.Cell(10, 5).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                workSheetData.Cell(10, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                workSheetData.Cell(10, 6).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                workSheetData.Cell(10, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                workSheetData.Cell(10, 7).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                workSheetData.Cell(10, 1).Value = "Date/Time";
        //                workSheetData.Cell(10, 2).Value = "LES";
        //                workSheetData.Cell(10, 3).Value = "Orgin//Destination";
        //                workSheetData.Cell(10, 4).Value = "Usage";
        //                workSheetData.Cell(10, 5).Value = "Unit";
        //                workSheetData.Cell(10, 6).Value = "Rate";
        //                workSheetData.Cell(10, 7).Value = "Charges(USD)";

        //                //workSheet.Cell(25, 6).Value = "Total(USD)";

        //                workSheetData.Row(10).Style.Font.Bold = true;

        //                workSheetData.Cell(10, 1).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 1).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 1).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 1).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 2).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 2).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 2).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 2).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 3).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 3).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 3).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 3).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 4).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 4).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 4).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 4).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 5).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 5).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 5).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 5).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 6).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 6).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 6).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 6).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 7).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 7).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 7).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                workSheetData.Cell(10, 7).Style.Border.LeftBorder = XLBorderStyleValues.Medium;

        //                workSheetData.Row(25).Style.Font.Bold = true;
        //                workSheetData.SheetView.Freeze(10, 0);

        //                int count = 11, cond = 11;

        //                int dataServType = 0, VceServType = 0;

        //                List<string> DataServiceDest = db.BSt2B.AsEnumerable().Where(t => t.Bt2CusFbIMSIId != null && t.Bt2CusFbTranDServType != null && t.Bt2CusFbIMSIId.Equals(imsiID))
        //                                            .Select(t => t.Bt2CusFbTranDServType).Distinct().ToList();
        //                List<string> VceServiceDest = db.BSt2B.AsEnumerable().Where(t => t.Bt2CusFbIMSIId != null && t.Bt2CusFbTranVServType != null && t.Bt2CusFbIMSIId.Equals(imsiID))
        //                                            .Select(t => t.Bt2CusFbTranVServType).Distinct().ToList();

        //                //if (DataServiceDest.Count > 1 && DataServiceDest.Contains("Subscription Fee"))
        //                //{
        //                //    DataServiceDest.Remove("Subscription Fee");
        //                //}

        //                List<BSt2B> lstInvoiceB;
        //                BPtFbbCusReg cusRegSP = null;
        //                BsPtLesReg lesRegCP = null;
        //                try
        //                {
        //                    cusRegSP = db.BPtFbbCusRegs.FirstOrDefault(t => t.BPtCusFBimSingle.Equals(imsiID));
        //                    lesRegCP = db.BsPtLesRegs.FirstOrDefault(t => t.BPtLesRgnFbDataSimardId.Equals(imsiID));
        //                }
        //                catch
        //                {
        //                    //ErrorMsg.AppendFormat("The IMSI: {0} not registerd in Customer Registeration Table.", Imsi).AppendLine();
        //                }

        //                decimal? TotalChargeUSD = 0;
        //                decimal? SMTStotalCharge_CP = 0;

        //                decimal? dateRate1MB_SP = 0;
        //                decimal? Vce2Fixed1MIN_SP = 0;
        //                decimal? Vce2Cell1Min_SP = 0;
        //                decimal? Vce2Fbb1Min_SP = 0;
        //                decimal? Vce2Iridium_SP = 0;
        //                decimal? Cus_OutBundleDataRateSP = 0;
        //                string Cus_DataPlanSP = string.Empty;

        //                decimal? SMTSdataRate1mb_CP = 0;
        //                decimal? SMTSVce2Fixed_CP = 0;
        //                decimal? SMTSVce2Cell_CP = 0;
        //                decimal? SMTSVce2Fbb_CP = 0;
        //                decimal? SMTSVce2Iridium_CP = 0;
        //                decimal? SMTSRecurFee_CP = 0;

        //                if (cusRegSP != null)
        //                {
        //                    dateRate1MB_SP = cusRegSP.BPtCusFbDataRate;
        //                    dateRate1MB_SP = Math.Round(Convert.ToDecimal(dateRate1MB_SP), 2, MidpointRounding.AwayFromZero);

        //                    Vce2Fixed1MIN_SP = cusRegSP.BPtCusFbVce2Fixed;
        //                    Vce2Fixed1MIN_SP = Math.Round(Convert.ToDecimal(Vce2Fixed1MIN_SP), 2, MidpointRounding.AwayFromZero);

        //                    Vce2Cell1Min_SP = cusRegSP.BPtCusFbVce2Cell;
        //                    Vce2Cell1Min_SP = Math.Round(Convert.ToDecimal(Vce2Cell1Min_SP), 2, MidpointRounding.AwayFromZero);

        //                    Vce2Fbb1Min_SP = cusRegSP.BPtCusFbVce2Fb;
        //                    Vce2Fbb1Min_SP = Math.Round(Convert.ToDecimal(Vce2Fbb1Min_SP), 2, MidpointRounding.AwayFromZero);

        //                    Vce2Iridium_SP = cusRegSP.BPtCusFb2Iridium;
        //                    Vce2Iridium_SP = Math.Round(Convert.ToDecimal(Vce2Iridium_SP), 2, MidpointRounding.AwayFromZero);

        //                    Cus_OutBundleDataRateSP = cusRegSP.BPtCusFbOutBundleDataRate;
        //                    Cus_OutBundleDataRateSP = Math.Round(Convert.ToDecimal(Cus_OutBundleDataRateSP), 2, MidpointRounding.AwayFromZero);

        //                    Cus_DataPlanSP = cusRegSP.BPtCusFbDataPlanIn;
        //                }

        //                if (lesRegCP != null)
        //                {
        //                    SMTSdataRate1mb_CP = lesRegCP.BPtLesRgnFbDataRate;
        //                    SMTSdataRate1mb_CP = Math.Round(Convert.ToDecimal(SMTSdataRate1mb_CP), 2, MidpointRounding.AwayFromZero);

        //                    SMTSVce2Fixed_CP = lesRegCP.BPtLesRgnFbVce2Fixed;
        //                    SMTSVce2Fixed_CP = Math.Round(Convert.ToDecimal(SMTSVce2Fixed_CP), 2, MidpointRounding.AwayFromZero);

        //                    SMTSVce2Cell_CP = lesRegCP.BPtLesRgnFbVce2Cell;
        //                    SMTSVce2Cell_CP = Math.Round(Convert.ToDecimal(SMTSVce2Cell_CP), 2, MidpointRounding.AwayFromZero);

        //                    SMTSVce2Fbb_CP = lesRegCP.BPtLesRgnFbVce2Fb;
        //                    SMTSVce2Fbb_CP = Math.Round(Convert.ToDecimal(SMTSVce2Fbb_CP), 2, MidpointRounding.AwayFromZero);

        //                    SMTSVce2Iridium_CP = lesRegCP.BPtLesRgnFb2Iridium;
        //                    SMTSVce2Iridium_CP = Math.Round(Convert.ToDecimal(SMTSVce2Iridium_CP), 2, MidpointRounding.AwayFromZero);

        //                    SMTSRecurFee_CP = lesRegCP.BPtLesRgnFbRecurFee;
        //                    SMTSRecurFee_CP = Math.Round(Convert.ToDecimal(SMTSRecurFee_CP), 2, MidpointRounding.AwayFromZero);
        //                }

        //                foreach (string services in DataServiceDest)
        //                {
        //                    dataServType++;

        //                    if (services.ToLower().Contains("subscription fee"))
        //                    {
        //                        workSheetData.Cell(count, 1).Value = "Service Type :" + services + " " + "Id No:" + imsiID;

        //                        //Data Call Log
        //                        lstInvoiceB = db.BSt2B.AsEnumerable()
        //                                        .Where(t => t.Bt2CusFbTranDServType != null
        //                                                && t.Bt2CusFbTranDServType.ToLower().Equals(services.ToLower())
        //                                                && (t.Bt2CusFbIMSIId.Equals(imsiID)) && !t.Bt2CusFbisOutBundle).ToList();

        //                        if (lstInvoiceB != null)
        //                        {
        //                            foreach (BSt2B objB in lstInvoiceB)
        //                            {
        //                                count++;
        //                                cond++;

        //                                // Les
        //                                //workSheet.Cell(count, 2).Value = objB.Bt2CusFbTranDStartDate;
        //                                // Origin or Destination
        //                                workSheetData.Cell(count, 3).Style.NumberFormat.SetNumberFormatId((int)XLPredefinedFormat.Number.Integer);
        //                                workSheetData.Cell(count, 3).Value = objB.Bt2CusFbTranDDest;
        //                                // Usage
        //                                decimal usage = Math.Round(Convert.ToDecimal(objB.Bt2CusFbBilIncre), 2, MidpointRounding.AwayFromZero);
        //                                workSheetData.Cell(count, 4).Value = usage;
        //                                // Unit Type
        //                                workSheetData.Cell(count, 5).Value = objB.Bt2CusFbBillUnitType;

        //                                // Total charge in USD
        //                                workSheetData.Cell(count, 7).Value = Math.Round(Convert.ToDecimal(objB.Bt2CusFbRecurFee), 2, MidpointRounding.AwayFromZero);
        //                                TotalChargeUSD += Math.Round(Convert.ToDecimal(objB.Bt2CusFbRecurFee), 2, MidpointRounding.AwayFromZero);

        //                                //SMTS Cost Price
        //                                SMTStotalCharge_CP += Math.Round(Convert.ToDecimal(SMTSRecurFee_CP), 2, MidpointRounding.AwayFromZero);
        //                            }
        //                            count++;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        workSheetData.Cell(count, 1).Value = "Service Type :" + services + " " + "Id No:" + imsiID;

        //                        //Data Call Log InBundle Data
        //                        lstInvoiceB = db.BSt2B.AsEnumerable()
        //                                        .Where(t => t.Bt2CusFbTranDServType != null
        //                                                && t.Bt2CusFbTranDServType.ToLower().Equals(services.ToLower())
        //                                                && (t.Bt2CusFbIMSIId.Equals(imsiID)) && !t.Bt2CusFbisOutBundle).ToList();

        //                        if (lstInvoiceB != null)
        //                        {
        //                            decimal stdIPSubtotal = 0;
        //                            foreach (BSt2B objB in lstInvoiceB)
        //                            {
        //                                count++;
        //                                cond++;

        //                                // Date or Time
        //                                if (objB.Bt2CusFbTranDStartDate != null)
        //                                    workSheetData.Cell(count, 1).Value = Convert.ToDateTime(objB.Bt2CusFbTranDStartDate).ToShortDateString() + " / " + objB.Bt2CusFbTranDStartTime;
        //                                else
        //                                    workSheetData.Cell(count, 1).Value = objB.Bt2CusFbTranDStartDate + " / " + objB.Bt2CusFbTranDStartTime;

        //                                // Les
        //                                //workSheet.Cell(count, 2).Value = objB.Bt2CusFbTranDStartDate;
        //                                // Origin or Destination
        //                                workSheetData.Cell(count, 3).Style.NumberFormat.SetNumberFormatId((int)XLPredefinedFormat.Number.Integer);
        //                                workSheetData.Cell(count, 3).Value = objB.Bt2CusFbTranDDest;
        //                                // Usage
        //                                decimal usage = Math.Round(Convert.ToDecimal(objB.Bt2CusFbBilIncre), 2, MidpointRounding.AwayFromZero);
        //                                workSheetData.Cell(count, 4).Value = usage;
        //                                // Unit Type
        //                                workSheetData.Cell(count, 5).Value = objB.Bt2CusFbBillUnitType;

        //                                // Rate
        //                                workSheetData.Cell(count, 6).Value = dateRate1MB_SP;

        //                                // Total charge in USD
        //                                workSheetData.Cell(count, 7).Value = 0.00;//Math.Round(Convert.ToDecimal((usage * dateRate1MB_SP)), 2, MidpointRounding.AwayFromZero);
        //                                //TotalChargeUSD += Math.Round(Convert.ToDecimal((usage * dateRate1MB_SP)), 2, MidpointRounding.AwayFromZero);

        //                                //SMTS Cost Price
        //                                //SMTStotalCharge_CP += Math.Round(Convert.ToDecimal((usage * SMTSdataRate1mb_CP)), 2, MidpointRounding.AwayFromZero);
        //                            }
        //                            count++;
        //                            if (stdIPSubtotal > 0)
        //                            {
        //                                workSheetData.Cell(count, 3).Value = string.Format("SUM({0}/{1})", services, imsiID);
        //                                workSheetData.Cell(count, 7).Value = 0.00;
        //                            }
        //                        }

        //                        //Data CallLog for OutBundle data Rate

        //                        lstInvoiceB = db.BSt2B.AsEnumerable()
        //                                        .Where(t => t.Bt2CusFbTranDServType != null
        //                                                && t.Bt2CusFbTranDServType.ToLower().Equals(services.ToLower())
        //                                                && (t.Bt2CusFbIMSIId.Equals(imsiID)) && t.Bt2CusFbisOutBundle).ToList();

        //                        if (lstInvoiceB != null)
        //                        {
        //                            workSheetData.Cell(count, 1).Value = "Service Type :" + services + " OutBundle " + "Id No:" + imsiID;

        //                            decimal stdIPOutbundleTotal = 0;

        //                            foreach (BSt2B objB in lstInvoiceB)
        //                            {
        //                                count++;
        //                                cond++;

        //                                // Date or Time
        //                                if (objB.Bt2CusFbTranDStartDate != null)
        //                                    workSheetData.Cell(count, 1).Value = Convert.ToDateTime(objB.Bt2CusFbTranDStartDate).ToShortDateString() + " / " + objB.Bt2CusFbTranDStartTime;
        //                                else
        //                                    workSheetData.Cell(count, 1).Value = objB.Bt2CusFbTranDStartDate + " / " + objB.Bt2CusFbTranDStartTime;

        //                                // Les
        //                                //workSheet.Cell(count, 2).Value = objB.Bt2CusFbTranDStartDate;
        //                                // Origin or Destination
        //                                workSheetData.Cell(count, 3).Style.NumberFormat.SetNumberFormatId((int)XLPredefinedFormat.Number.Integer);
        //                                workSheetData.Cell(count, 3).Value = objB.Bt2CusFbTranDDest;
        //                                // Usage
        //                                decimal usage = Math.Round(Convert.ToDecimal(objB.Bt2CusFbBilIncre), 2, MidpointRounding.AwayFromZero);
        //                                workSheetData.Cell(count, 4).Value = usage;
        //                                // Unit Type
        //                                workSheetData.Cell(count, 5).Value = objB.Bt2CusFbBillUnitType;

        //                                // Rate
        //                                workSheetData.Cell(count, 6).Value = Cus_OutBundleDataRateSP;

        //                                // Total charge in USD
        //                                workSheetData.Cell(count, 7).Value = Math.Round(Convert.ToDecimal((usage * Cus_OutBundleDataRateSP)), 2, MidpointRounding.AwayFromZero);
        //                                TotalChargeUSD += Math.Round(Convert.ToDecimal((usage * Cus_OutBundleDataRateSP)), 2, MidpointRounding.AwayFromZero);

        //                                stdIPOutbundleTotal += Math.Round(Convert.ToDecimal((usage * Cus_OutBundleDataRateSP)), 2, MidpointRounding.AwayFromZero);

        //                                //SMTS Cost Price
        //                                SMTStotalCharge_CP += Math.Round(Convert.ToDecimal((usage * SMTSdataRate1mb_CP)), 2, MidpointRounding.AwayFromZero);
        //                            }
        //                            count++;

        //                            if (stdIPOutbundleTotal > 0)
        //                            {
        //                                workSheetData.Cell(count, 3).Value = string.Format("SUM({0}/{1})", services, imsiID);
        //                                workSheetData.Cell(count, 7).Value = 0.00;
        //                            }
        //                        }
        //                    }
        //                }

        //                #endregion

        //                #region CallLog Voice
        //                // Worksheet for CallLog Voice
        //                IXLWorksheet WorksheetVoice = xLWorkbook.Worksheets.Add("CallLog Voice");

        //                string ImagePath = MySession.InvoiceLogoPath;
        //                var img = WorksheetVoice.AddPicture(ImagePath)
        //                             .MoveTo(WorksheetVoice.Cell(2, 1)).Scale(1.05);

        //                WorksheetVoice.Range("A2:B2").Merge();
        //                WorksheetVoice.Cell(4, 1).Value = "Call Data Details";
        //                WorksheetVoice.Cell(4, 1).Style.Font.FontSize = 20;
        //                WorksheetVoice.Cell(6, 5).Value = "Customer";
        //                WorksheetVoice.Cell(6, 6).Value = customerName;
        //                WorksheetVoice.Row(6).Style.Font.Bold = true;
        //                WorksheetVoice.Cell(7, 5).Value = "LES";
        //                // workSheet.Cell(7, 6).Value = LES.ToString();
        //                WorksheetVoice.Row(7).Style.Font.Bold = true;

        //                WorksheetVoice.Cell(8, 5).Value = "Vessel";
        //                WorksheetVoice.Cell(8, 6).Value = VesselName;
        //                WorksheetVoice.Row(8).Style.Font.Bold = true;

        //                WorksheetVoice.Column(1).Width = 16;
        //                WorksheetVoice.Column(2).Width = 15;
        //                WorksheetVoice.Column(3).Width = 25;
        //                WorksheetVoice.Column(4).Width = 10;
        //                WorksheetVoice.Column(5).Width = 10;
        //                WorksheetVoice.Column(6).Width = 10;
        //                WorksheetVoice.Column(7).Width = 12;
        //                WorksheetVoice.Column(8).Width = 10;

        //                WorksheetVoice.Cell(6, 1).Value = "Invoice No";
        //                WorksheetVoice.Cell(6, 2).Value = InvoiceNo.ToString();
        //                WorksheetVoice.Cell(7, 1).Value = "Invoice Date";
        //                WorksheetVoice.Cell(7, 2).Value = DateTime.Now.Date;
        //                WorksheetVoice.Cell(6, 1).Style.Font.Bold = true;
        //                WorksheetVoice.Cell(6, 2).Style.Font.Bold = true;
        //                WorksheetVoice.Cell(7, 1).Style.Font.Bold = true;
        //                WorksheetVoice.Cell(7, 2).Style.Font.Bold = true;
        //                WorksheetVoice.Cell(7, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);

        //                WorksheetVoice.SheetView.Freeze(10, 0);
        //                WorksheetVoice.Row(10).Style.Font.Bold = true;


        //                WorksheetVoice.Cell(10, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                WorksheetVoice.Cell(10, 1).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                WorksheetVoice.Cell(10, 2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                WorksheetVoice.Cell(10, 2).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                WorksheetVoice.Cell(10, 3).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                WorksheetVoice.Cell(10, 3).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                WorksheetVoice.Cell(10, 4).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                WorksheetVoice.Cell(10, 4).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                WorksheetVoice.Cell(10, 5).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                WorksheetVoice.Cell(10, 5).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                WorksheetVoice.Cell(10, 6).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                WorksheetVoice.Cell(10, 6).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                WorksheetVoice.Cell(10, 7).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        //                WorksheetVoice.Cell(10, 7).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
        //                WorksheetVoice.Cell(10, 1).Value = "Date/Time";
        //                WorksheetVoice.Cell(10, 2).Value = "LES";
        //                WorksheetVoice.Cell(10, 3).Value = "Orgin//Destination";
        //                WorksheetVoice.Cell(10, 4).Value = "Usage";
        //                WorksheetVoice.Cell(10, 5).Value = "Unit";
        //                WorksheetVoice.Cell(10, 6).Value = "Rate";
        //                WorksheetVoice.Cell(10, 7).Value = "Charges(USD)";


        //                WorksheetVoice.Cell(10, 1).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 1).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 1).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 1).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 2).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 2).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 2).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 2).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 3).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 3).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 3).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 3).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 4).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 4).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 4).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 4).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 5).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 5).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 5).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 5).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 6).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 6).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 6).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 6).Style.Border.LeftBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 7).Style.Border.TopBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 7).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 7).Style.Border.RightBorder = XLBorderStyleValues.Medium;
        //                WorksheetVoice.Cell(10, 7).Style.Border.LeftBorder = XLBorderStyleValues.Medium;

        //                int counts = 10, con = 10;

        //                foreach (string services in VceServiceDest)
        //                {
        //                    WorksheetVoice.Cell(++counts, 1).Value = "Service Type :" + services + " " + "Id No:" + imsiID;

        //                    // Voice Call log
        //                    lstInvoiceB = db.BSt2B.AsEnumerable()
        //                                 .Where(t => t.Bt2CusFbTranVServType != null && t.Bt2CusFbTranVServType.ToLower().Equals(services.ToLower())
        //                                        && (t.Bt2CusFbIMSIId.Equals(imsiID))).ToList();
        //                    if (lstInvoiceB != null)
        //                    {
        //                        foreach (BSt2B objB in lstInvoiceB)
        //                        {
        //                            counts++;
        //                            con++;

        //                            // Date or Time
        //                            if (objB.Bt2CusFbTranVStartDate != null)
        //                                WorksheetVoice.Cell(counts, 1).Value = Convert.ToDateTime(objB.Bt2CusFbTranVStartDate).ToShortDateString() + " / " + objB.Bt2CusFbTranVStartTime;
        //                            else
        //                                WorksheetVoice.Cell(counts, 1).Value = objB.Bt2CusFbTranVStartDate + " / " + objB.Bt2CusFbTranVStartTime;
        //                            // Les
        //                            //workSheet.Cell(count, 2).Value = objB.Bt2CusFbTranDStartDate;
        //                            // Origin or Destination
        //                            WorksheetVoice.Cell(counts, 3).Value = objB.Bt2CusFbTranVDest;
        //                            // Usage
        //                            decimal VceUsage = Math.Round(Convert.ToDecimal(objB.Bt2CusFbVcebillincre), 2, MidpointRounding.AwayFromZero);
        //                            WorksheetVoice.Cell(counts, 4).Value = VceUsage;
        //                            // Unit Type
        //                            WorksheetVoice.Cell(counts, 5).Value = objB.Bt2CusFbVcebillingUnittype;
        //                            // Rate
        //                            if (services.ToLower().Contains("fixed"))
        //                            {
        //                                WorksheetVoice.Cell(counts, 6).Value = Vce2Fixed1MIN_SP;
        //                                // Total charge in USD
        //                                WorksheetVoice.Cell(counts, 7).Value = Math.Round(Convert.ToDecimal((VceUsage * Vce2Fixed1MIN_SP)), 2, MidpointRounding.AwayFromZero);

        //                                TotalChargeUSD += Math.Round(Convert.ToDecimal((VceUsage * Vce2Fixed1MIN_SP)), 2, MidpointRounding.AwayFromZero);

        //                                //SMTS Cost Price
        //                                SMTStotalCharge_CP += Math.Round(Convert.ToDecimal((VceUsage * SMTSVce2Fixed_CP)), 2, MidpointRounding.AwayFromZero);
        //                            }
        //                            else if (services.ToLower().Contains("cell"))
        //                            {
        //                                WorksheetVoice.Cell(counts, 6).Value = Vce2Cell1Min_SP;
        //                                // Total charge in USD
        //                                WorksheetVoice.Cell(counts, 7).Value = Math.Round(Convert.ToDecimal((VceUsage * Vce2Cell1Min_SP)), 2, MidpointRounding.AwayFromZero);

        //                                TotalChargeUSD += Math.Round(Convert.ToDecimal((VceUsage * Vce2Cell1Min_SP)), 2, MidpointRounding.AwayFromZero);

        //                                //SMTS Cost Price
        //                                SMTStotalCharge_CP += Math.Round(Convert.ToDecimal((VceUsage * SMTSVce2Cell_CP)), 2, MidpointRounding.AwayFromZero);
        //                            }
        //                            else if (services.ToLower().Contains("fbb") || services.ToLower().Contains("boardband") || services.ToLower().Contains("fleetbroadband"))
        //                            {
        //                                WorksheetVoice.Cell(counts, 6).Value = Vce2Fbb1Min_SP;
        //                                // Total charge in USD
        //                                WorksheetVoice.Cell(counts, 7).Value = Math.Round(Convert.ToDecimal((VceUsage * Vce2Fbb1Min_SP)), 2, MidpointRounding.AwayFromZero);

        //                                TotalChargeUSD += Math.Round(Convert.ToDecimal((VceUsage * Vce2Fbb1Min_SP)), 2, MidpointRounding.AwayFromZero);

        //                                //SMTS Cost Price
        //                                SMTStotalCharge_CP += Math.Round(Convert.ToDecimal((VceUsage * SMTSVce2Fbb_CP)), 2, MidpointRounding.AwayFromZero);
        //                            }
        //                            else if (services.ToLower().Contains("iridium"))
        //                            {
        //                                WorksheetVoice.Cell(counts, 6).Value = Vce2Iridium_SP;
        //                                // Total charge in USD
        //                                WorksheetVoice.Cell(counts, 7).Value = Math.Round(Convert.ToDecimal((VceUsage * Vce2Iridium_SP)), 2, MidpointRounding.AwayFromZero);

        //                                TotalChargeUSD += Math.Round(Convert.ToDecimal((VceUsage * Vce2Iridium_SP)), 2, MidpointRounding.AwayFromZero);

        //                                //SMTS Cost Price
        //                                SMTStotalCharge_CP += Math.Round(Convert.ToDecimal((VceUsage * SMTSVce2Iridium_CP)), 2, MidpointRounding.AwayFromZero);
        //                            }
        //                        }
        //                    }
        //                }
        //                #endregion

        //                WorksheetVoice.Cell(counts + 2, 6).Value = "Total(USD)";
        //                WorksheetVoice.Cell(counts + 2, 7).Value = TotalChargeUSD;
        //                WorksheetVoice.Cell(counts + 2, 7).Style.Font.Bold = true;
        //                WorksheetVoice.Cell(counts + 2, 6).Style.Font.Bold = true;

        //                var statment = db.Statements.FirstOrDefault(t => t.IMSIID.Equals(imsiID));
        //                bool isnotexist = false;
        //                if (statment != null)
        //                {
        //                    if (statment.InvoiceNo != InvoiceNo)
        //                        isnotexist = true;
        //                }

        //                if (statment == null || isnotexist)
        //                {
        //                    statment = new Statement();

        //                    if (string.IsNullOrEmpty(statment.InvoiceNo))
        //                    {
        //                        statment.InvoiceNo = InvoiceNo;
        //                    }

        //                    statment.IMSIID = imsiID;
        //                    statment.TotalSP = TotalChargeUSD;
        //                    statment.Vessel = VesselName;
        //                    statment.Customer = customerName;
        //                    statment.IvoiceDate = DateTime.Now.Date;
        //                    statment.Duration = db.BCtBs.FirstOrDefault(t => t.BCtFbIMSIId.Equals(imsiID) && t.BCtFbTranDServType.ToLower().Equals("subscription fee")).BCtFbTranDDest;

        //                    using (BillingSystemEntities db = new BillingSystemEntities())
        //                    {
        //                        decimal? totalAmountMRC = db.BCtBs.FirstOrDefault(t => t.BCtFbTranDServType != null &&
        //                                                           t.BCtFbRecurFee != null && t.BCtFbIMSIId.Equals(imsiID)).BCtFbRecurFee;

        //                        totalAmountMRC = Math.Round(Convert.ToDecimal(totalAmountMRC), 2, MidpointRounding.AwayFromZero);

        //                        decimal? totalAmountData = db.BCtBs.AsEnumerable().Where(t => t.BCtFbTranDServType != null &&
        //                                                                                 t.BCtFbIMSIId.Equals(imsiID))
        //                                                                          .GroupBy(t => t.BCtFbIMSIId.Equals(imsiID))
        //                                                                          .Select(t => t.Sum(s => s.BCtFbTranDUsage)).FirstOrDefault();
        //                        totalAmountData = Math.Round(Convert.ToDecimal(totalAmountData), 2, MidpointRounding.AwayFromZero);

        //                        decimal? totalUnitsData = db.BCtBs.AsEnumerable().Where(t => t.BCtFbTranDServType != null &&
        //                                                                                t.BCtFbIMSIId.Equals(imsiID))
        //                                                                         .GroupBy(t => t.BCtFbIMSIId.Equals(imsiID))
        //                                                                         .Select(t => t.Sum(s => s.BCtFbBilIncre)).FirstOrDefault();
        //                        totalUnitsData = Math.Round(Convert.ToDecimal(totalUnitsData), 2, MidpointRounding.AwayFromZero);

        //                        decimal? totalAmountVce = db.BCtBs.AsEnumerable().Where(t => t.BCtFbTranVServType != null &&
        //                                                                                     t.BCtFbIMSIId.Equals(imsiID))
        //                                                                         .GroupBy(t => t.BCtFbIMSIId.Equals(imsiID))
        //                                                                         .Select(t => t.Sum(s => s.BCtFbTranVUsage)).FirstOrDefault();
        //                        totalAmountVce = Math.Round(Convert.ToDecimal(totalAmountVce), 2, MidpointRounding.AwayFromZero);

        //                        //statment.TotalCP = totalAmountMRC + totalAmountData + totalAmountVce;
        //                        statment.TotalCP = totalAmountMRC + totalAmountVce;
        //                    }


        //                    if (statment.TotalCP > 0 && statment.TotalSP > 0)
        //                    {
        //                        decimal CrossProfit = Convert.ToDecimal(statment.TotalSP - statment.TotalCP);
        //                        decimal Margin = (CrossProfit / Convert.ToDecimal(statment.TotalSP) * 100);
        //                        statment.Margin = Margin > 0 ? Convert.ToInt32(Margin) : (int)Math.Abs(Margin);
        //                    }

        //                    statment.SMTSCost = SMTStotalCharge_CP;

        //                    db.Statements.Add(statment);
        //                    try
        //                    {
        //                        db.SaveChanges();
        //                    }
        //                    catch { }
        //                }


        //                xLWorkbook.SaveAs(SpreadsheetStream);
        //                SpreadsheetStream.Position = 0;

        //                return new Tuple<Stream, string>(SpreadsheetStream, CallLogName);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            return new Tuple<Stream, string>(SpreadsheetStream, string.Empty);
        //        }
        //        return new Tuple<Stream, string>(SpreadsheetStream, string.Empty);
        //    }

            return new Tuple<Stream, string>(null, string.Empty);
        }

        public static Tuple<string, string, string> GetFileInfo(string fileName)
        {
            // Tuple<string,string,string> item one is Invoice no item 2 is Company Name item 3 is Vessel name
            string[] fileinfo = new string[2];

            string InvNo = string.Empty;
            string CompanyName = string.Empty;
            string VesselName = string.Empty;
            string filename = string.Empty;
            string fileExtend = Path.GetExtension(fileName);
            if (fileExtend.Contains(".xlsx") || fileExtend.Contains(".csv") || fileExtend.Contains(".pdf") || fileExtend.Contains(".xls") || fileExtend.Contains(".zip"))
                filename = System.IO.Path.GetFileNameWithoutExtension(fileName);
            else
                filename = fileName;

            if (!string.IsNullOrEmpty(filename))
            {
                if (filename.Contains('_'))
                {
                    fileinfo = filename.Split('_');
                }

                if (fileinfo.Length > 0)
                {
                    if (!string.IsNullOrEmpty(fileinfo[0]) && (fileinfo[0].Contains("SM") || fileinfo[0].Contains("II")))
                    {
                        InvNo = fileinfo[0];
                        CompanyName = fileinfo[1];
                        if (fileinfo.Length > 2)
                            VesselName = fileinfo[2];
                    }
                    else if (!string.IsNullOrEmpty(fileinfo[1]) && (fileinfo[1].Contains("SM") || fileinfo[1].Contains("II")))
                    {
                        InvNo = fileinfo[1];
                        CompanyName = fileinfo[0];
                        if (fileinfo.Length > 2)
                            VesselName = fileinfo[2];
                    }
                    else if ((fileinfo.Length > 2) && !string.IsNullOrEmpty(fileinfo[2]) && (fileinfo[2].Contains("SM") || fileinfo[2].Contains("II")))
                    {
                        InvNo = fileinfo[2];
                        CompanyName = fileinfo[0];
                        VesselName = fileinfo[1];
                    }
                }
            }

            return new Tuple<string, string, string>(InvNo, CompanyName, VesselName);
        }

        public Dictionary<string, string> OteSattoCT(string fileName)
        {
            //    try
            //    {
            //        Dictionary<string, string> dictDuration = new Dictionary<string, string>();

            //        using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            //        {
            //            using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fs, false))
            //            {
            //                WorkbookPart workbookPart = doc.WorkbookPart;
            //                SharedStringTablePart sstpart = workbookPart.GetPartsOfType<SharedStringTablePart>().First();
            //                SharedStringTable sst = sstpart.SharedStringTable;

            //                WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
            //                Worksheet sheet = worksheetPart.Worksheet;

            //                SheetData sheetData = sheet.GetFirstChild<SheetData>();
            //                IEnumerable<Row> rows = sheetData.Descendants<Row>();


            //                List<string> receiptField = new List<string>();
            //                string ColName = string.Empty;

            //                foreach (Cell cell in rows.ElementAt(0))
            //                {
            //                    ColName = GetCellValue(doc, cell);
            //                    if (ColName.StartsWith(" "))
            //                        ColName = ColName.Substring(1);
            //                    receiptField.Add(System.Text.RegularExpressions.Regex.Replace(ColName, @"\t|\n|\r", ""));
            //                }
            //                BillingSystemEntities billingSystemEntities = new BillingSystemEntities();

            //                BCtA bCtA = new BCtA();
            //                BCtB bCtB = new BCtB();
            //                BCtC bCtC = new BCtC();
            //                BCtD bCtD = new BCtD();

            //                BPtFbbCusReg bcusreg = new BPtFbbCusReg();
            //                BsPtLesReg bLesReg = new BsPtLesReg();

            //                string colValue = string.Empty;

            //                string VesselName, IMNIMSI = string.Empty, systems, Time, OceanRegion, TimeZone, MRN, country = string.Empty;
            //                TimeSpan? times = null;
            //                DateTime? dateTime = null;
            //                DateTime? contractStartDate = null;
            //                DateTime? barDate = null;
            //                DateTime? suspensionDate = null;
            //                DateTime? layupDate = null;
            //                string serviceType = string.Empty;
            //                Decimal? UnitType = null;

            //                List<Dictionary<string, string>> lstdict = new List<Dictionary<string, string>>();

            //                foreach (Row row in rows)
            //                {
            //                    if (row.RowIndex != 1)
            //                    {
            //                        bCtA = new BCtA();
            //                        bCtB = new BCtB();
            //                        bCtC = new BCtC();
            //                        bCtD = new BCtD();

            //                        for (int i = 0, arrIndex = 0; i < receiptField.Count; i++, arrIndex++) ///row.Descendants<Cell>().Count() replaced
            //                        {
            //                            colValue = GetCellValue(doc, row.Descendants<Cell>().ElementAt(i));

            //                            Dictionary<string, string> dict = new Dictionary<string, string>();

            //                            if ((receiptField[arrIndex].ToLower().Equals("vessel name")))
            //                                try
            //                                {
            //                                    VesselName = colValue;
            //                                }
            //                                catch (Exception ex)
            //                                {
            //                                    VesselName = null;
            //                                }
            //                            else if (receiptField[arrIndex].ToUpper().Contains("IMN / IMSI"))
            //                            {
            //                                try
            //                                {
            //                                    bCtB.BCtFbIMSIId = colValue;
            //                                    bCtB.BCtFbMSIDNNo = colValue;
            //                                    bCtB.BCtFbSimIdNo = colValue;
            //                                    bCtC.BCtFbStreamIMSIIDNo = colValue;
            //                                    bCtA.BCtFbIMSIId = colValue;
            //                                    bCtD.BCFbIMSIId = colValue;
            //                                    IMNIMSI = colValue;
            //                                }
            //                                catch (Exception ex)
            //                                {
            //                                    bCtB.BCtFbIMSIId = null;
            //                                    IMNIMSI = null;
            //                                    bCtC.BCtFbStreamIMSIIDNo = null;
            //                                    bCtA.BCtFbIMSIId = null;
            //                                    bCtD.BCFbIMSIId = null;
            //                                }
            //                            }
            //                            else if (receiptField[arrIndex].ToLower().Equals("system "))
            //                            {
            //                                try
            //                                {
            //                                    systems = colValue;
            //                                }
            //                                catch (Exception ex)
            //                                {
            //                                    systems = null;
            //                                }
            //                            }
            //                            else if (receiptField[arrIndex].ToLower().Equals("date"))
            //                            {
            //                                if (!string.IsNullOrEmpty(colValue))
            //                                {
            //                                    try
            //                                    {
            //                                        if (serviceType.ToLower().Trim().Equals("standard ip"))
            //                                        {
            //                                            double date = Convert.ToDouble(colValue);
            //                                            dateTime = DateTime.FromOADate(date);
            //                                            bCtB.BCtFbTranDStartDate = dateTime;
            //                                        }
            //                                        else if (serviceType.ToLower().Trim().Contains("voice"))
            //                                        {
            //                                            double date = Convert.ToDouble(colValue);
            //                                            dateTime = DateTime.FromOADate(date);
            //                                            bCtB.BCtFbTranVStartDate = dateTime;
            //                                        }
            //                                        else if (serviceType.ToLower().Trim().Equals("subscription fee"))
            //                                        {
            //                                            double date = Convert.ToDouble(colValue);
            //                                            dateTime = DateTime.FromOADate(date);
            //                                            bCtB.BCtFbTranDStartDate = dateTime;
            //                                        }
            //                                    }
            //                                    catch
            //                                    {
            //                                        bCtB.BCtFbTranDStartDate = null;
            //                                        bCtB.BCtFbTranVStartDate = null;
            //                                        //  bCtC.BCtFbTranStStartDate = null;
            //                                    }
            //                                }
            //                                else
            //                                {
            //                                    dateTime = null;
            //                                    bCtB.BCtFbTranDStartDate = null;
            //                                    bCtB.BCtFbTranVStartDate = null;
            //                                    //  bCtC.BCtFbTranStStartDate = null;
            //                                }
            //                            }
            //                            else if (receiptField[arrIndex].ToLower().Equals("time"))
            //                            {
            //                                if (!string.IsNullOrEmpty(colValue))
            //                                {
            //                                    try
            //                                    {
            //                                        TimeSpan? time = new TimeSpan();
            //                                        if (IsValidTimeFormat(colValue))
            //                                        {
            //                                            TimeSpan tempTime;
            //                                            if (!TimeSpan.TryParse(colValue, out tempTime))
            //                                            {
            //                                                time = tempTime;
            //                                            }
            //                                            else
            //                                                time = TimeSpan.Parse(colValue);
            //                                        }
            //                                        else
            //                                        {
            //                                            time = DateTime.FromOADate(Convert.ToDouble(colValue)).TimeOfDay;
            //                                        }

            //                                        times = time;

            //                                        //if (serviceType.ToLower().Trim().Equals("standard ip"))
            //                                        //    bCtB.BCtFbTranDStartTime = time;
            //                                        //else if (serviceType.ToLower().Trim().Equals("subscription fee"))
            //                                        //    bCtB.BCtFbTranDStartTime = time;
            //                                        //else if (serviceType.ToLower().Trim().Contains("voice"))
            //                                        //    bCtB.BCtFbTranVStartTime = time;
            //                                        //else if (serviceType.ToLower().Trim().Contains("streaming"))
            //                                        //    bCtC.BCtFbTranStStartTime = time;
            //                                    }
            //                                    catch (Exception rr)
            //                                    {
            //                                        bCtB.BCtFbTranDStartTime = null;
            //                                        bCtB.BCtFbTranVStartTime = null;
            //                                        bCtC.BCtFbTranStStartTime = null;
            //                                    }
            //                                }
            //                            }
            //                            else if (receiptField[arrIndex].ToLower().Equals("service type"))
            //                            {
            //                                if (!string.IsNullOrEmpty(colValue))
            //                                {
            //                                    try
            //                                    {
            //                                        serviceType = colValue;
            //                                        if (serviceType.ToLower().Trim().Equals("subscription fee"))
            //                                        {
            //                                            bCtB.BCtFbTranDServType = colValue;
            //                                            bCtB.BCtFbTranDStartTime = times;

            //                                            times = null;
            //                                            bCtB.BCtFbTranVServType = null;
            //                                            bCtC.BCtFbTranStServType = null;
            //                                        }
            //                                        else if (serviceType.ToLower().Trim().Equals("standard ip"))
            //                                        {
            //                                            bCtB.BCtFbTranDServType = colValue;
            //                                            bCtB.BCtFbTranDStartTime = times;
            //                                            bCtB.BCtFbTranDStartDate = dateTime;

            //                                            times = null;
            //                                            dateTime = null;
            //                                            bCtB.BCtFbTranVServType = null;
            //                                            bCtC.BCtFbTranStServType = null;
            //                                        }
            //                                        else if (serviceType.ToLower().Trim().Contains("voice"))
            //                                        {
            //                                            if (!string.IsNullOrEmpty(colValue))
            //                                            {
            //                                                try
            //                                                {
            //                                                    bCtB.BCtFbTranVServType = colValue;
            //                                                    bCtB.BCtFbTranVStartTime = times;
            //                                                    bCtB.BCtFbTranVStartDate = dateTime;
            //                                                    bCtB.NA14 = null;
            //                                                    times = null;
            //                                                    dateTime = null;
            //                                                    bCtB.BCtFbTranDServType = null;
            //                                                    bCtC.BCtFbTranStServType = null;
            //                                                }
            //                                                catch (Exception ex)
            //                                                { }
            //                                            }
            //                                        }
            //                                        else if (serviceType.ToLower().Equals("streaming"))
            //                                        {
            //                                            bCtC.BCtFbTranStServType = colValue;
            //                                            bCtC.BCtFbTranStStartTime = times;
            //                                            bCtC.BCtFbTranStStartDate = dateTime;

            //                                            times = null;
            //                                            dateTime = null;
            //                                            bCtB.BCtFbTranDServType = null;
            //                                            bCtB.BCtFbTranVServType = null;
            //                                        }
            //                                    }
            //                                    catch
            //                                    {
            //                                        bCtB.BCtFbTranDServType = null;
            //                                        bCtB.BCtFbTranVServType = null;
            //                                    }
            //                                }
            //                                else
            //                                {
            //                                    bCtB.BCtFbTranDServType = null;
            //                                    bCtB.BCtFbTranVServType = null;

            //                                    serviceType = null;
            //                                }
            //                            }
            //                            else if (receiptField[arrIndex].ToLower().Equals("sms"))
            //                            {
            //                                try
            //                                {
            //                                    bCtC.BCFBms = Convert.ToDecimal(colValue);
            //                                }
            //                                catch (Exception ex)
            //                                {

            //                                }
            //                            }
            //                            else if (receiptField[arrIndex].ToLower().Equals("curbill"))
            //                            {
            //                                try
            //                                {
            //                                    // bCtD.BCFbCurBill = colValue;
            //                                    bCtB.BCFbCurBill = Convert.ToDecimal(colValue);
            //                                }
            //                                catch (Exception ex)
            //                                {
            //                                    bCtB.BCFbCurBill = null;
            //                                }
            //                            }
            //                            else if (receiptField[arrIndex].ToLower().Equals("ocean region"))
            //                            {
            //                                OceanRegion = colValue;
            //                            }
            //                            else if (receiptField[arrIndex].ToLower().Equals("time zone"))
            //                            {
            //                                TimeZone = colValue;
            //                            }
            //                            else if (receiptField[arrIndex].ToLower().Equals("mrn"))
            //                            {
            //                                MRN = colValue;
            //                            }
            //                            else if (receiptField[arrIndex].ToLower().Equals("country"))
            //                            {
            //                                try
            //                                {
            //                                    country = colValue;
            //                                }
            //                                catch (Exception ex)
            //                                {
            //                                    country = null;
            //                                }
            //                            }
            //                            else if (receiptField[arrIndex].ToLower().Equals("destination"))
            //                            {

            //                                if (serviceType.ToLower().Trim().Equals("subscription fee"))
            //                                {
            //                                    bCtB.BCtFbTranDDest = colValue;
            //                                    if (!dictDuration.ContainsKey(IMNIMSI))
            //                                        dictDuration.Add(IMNIMSI, colValue);
            //                                }
            //                                else if (serviceType.ToLower().Trim().Equals("standard ip"))
            //                                {
            //                                    bCtB.BCtFbTranDDest = colValue;
            //                                }
            //                                else if (serviceType.ToLower().Trim().Contains("voice"))
            //                                {
            //                                    bCtB.BCtFbTranVDest = colValue;
            //                                }
            //                                else if (serviceType.ToLower().Equals("stream"))
            //                                {
            //                                    bCtC.BCtFbTranStDest = colValue;
            //                                }
            //                                else
            //                                {
            //                                    bCtB.BCtFbTranDDest = null;
            //                                    bCtB.BCtFbTranVDest = null;
            //                                    bCtC.BCtFbTranStDest = null;
            //                                }
            //                            }
            //                            else if (receiptField[arrIndex].ToLower().Equals("units"))
            //                            {
            //                                if (!string.IsNullOrEmpty(colValue))
            //                                {
            //                                    try
            //                                    {
            //                                        if (serviceType.ToLower().Trim().Equals("subscription fee"))
            //                                            bCtB.BCtFbBilIncre = Convert.ToDecimal(colValue);
            //                                        else if (serviceType.ToLower().Trim().Equals("standard ip"))
            //                                        {
            //                                            bCtB.BCtFbBilIncre = Convert.ToDecimal(colValue);

            //                                            if (!string.IsNullOrEmpty(colValue))
            //                                                bCtB.BCtFbVceBillIncre = Convert.ToDecimal(colValue);
            //                                        }
            //                                        else if (serviceType.ToLower().Trim().Equals("voice  to fixed"))
            //                                        {
            //                                            try
            //                                            {
            //                                                if (!string.IsNullOrEmpty(colValue))
            //                                                    bCtB.BCtFbVceBillIncre = Convert.ToDecimal(colValue);
            //                                            }
            //                                            catch { }
            //                                        }
            //                                        else if (serviceType.ToLower().Trim().Equals("voice  to cellular"))
            //                                        {
            //                                            try
            //                                            {
            //                                                if (!string.IsNullOrEmpty(colValue))
            //                                                    bCtB.BCtFbVceBillIncre = Convert.ToDecimal(colValue);
            //                                            }
            //                                            catch { }
            //                                        }
            //                                        else if (serviceType.ToLower().Trim().Equals("voice  to fleetbroadband"))
            //                                        {
            //                                            try
            //                                            {
            //                                                if (!string.IsNullOrEmpty(colValue))
            //                                                    bCtB.BCtFbVceBillIncre = Convert.ToDecimal(colValue);
            //                                            }
            //                                            catch { }
            //                                        }
            //                                        else if (serviceType.ToLower().Equals("streaming"))
            //                                        {
            //                                            bCtC.BCtFbStBillIncre = Convert.ToDecimal(colValue);
            //                                            bCtC.BCtFb8kbps = Convert.ToDecimal(colValue);
            //                                        }
            //                                        else if (serviceType.ToLower().Contains("iridium"))
            //                                        {
            //                                            try
            //                                            {
            //                                                if (!string.IsNullOrEmpty(colValue))
            //                                                    bCtB.BCtFbVceBillIncre = Convert.ToDecimal(colValue);
            //                                            }
            //                                            catch { }
            //                                        }
            //                                        else
            //                                        {
            //                                            bCtB.BCtFbBilIncre = null;
            //                                            bCtB.BCtFbVceBillIncre = null;
            //                                            bCtC.BCtFbStBillIncre = null;
            //                                        }
            //                                    }
            //                                    catch
            //                                    {
            //                                        bCtB.BCtFbBilIncre = null;
            //                                        bCtB.BCtFbVceBillIncre = null;
            //                                        bCtC.BCtFbStBillIncre = null;
            //                                    }
            //                                }
            //                                else
            //                                {
            //                                    bCtB.BCtFbBilIncre = null;
            //                                    bCtB.BCtFbVceBillIncre = null;
            //                                    bCtC.BCtFbStBillIncre = null;
            //                                }
            //                            }
            //                            else if (receiptField[arrIndex].ToLower().Equals("units type"))
            //                            {
            //                                if (!string.IsNullOrEmpty(colValue))
            //                                {
            //                                    try
            //                                    {
            //                                        if (serviceType.ToLower().Trim().Equals("subscription fee"))
            //                                            bCtB.BCtFbunitType = colValue;
            //                                        else if (serviceType.ToLower().Trim().Equals("standard ip"))
            //                                            bCtB.BCtFbunitType = colValue;
            //                                        else if (serviceType.ToLower().Trim().Contains("voice"))
            //                                            bCtB.BCtFbVceUnityTtype = colValue;
            //                                        else if (serviceType.ToLower().Equals("streaming"))
            //                                            bCtC.BCtFbStBillingUnittype = colValue;
            //                                        else
            //                                        {
            //                                            bCtB.BCtFbunitType = null;
            //                                            bCtB.BCtFbVceUnityTtype = null;
            //                                            bCtC.BCtFbStBillingUnittype = null;
            //                                        }
            //                                    }
            //                                    catch
            //                                    {
            //                                        bCtB.BCtFbunitType = null;
            //                                        bCtB.BCtFbVceUnityTtype = null;
            //                                        bCtC.BCtFbStBillingUnittype = null;
            //                                    }
            //                                }
            //                                else
            //                                {
            //                                    bCtB.BCtFbunitType = null;
            //                                    bCtB.BCtFbVceUnityTtype = null;
            //                                    bCtC.BCtFbStBillingUnittype = null;
            //                                }
            //                            }
            //                            else if (receiptField[arrIndex].ToLower().Trim().Contains("amount due"))
            //                            {
            //                                if (!string.IsNullOrEmpty(colValue))
            //                                {
            //                                    try
            //                                    {
            //                                        if (serviceType.ToLower().Trim().Equals("subscription fee"))
            //                                            bCtB.BCtFbRecurFee = Convert.ToDecimal(colValue);
            //                                        else if (serviceType.ToLower().Trim().Equals("standard ip"))
            //                                            bCtB.BCtFbTranDUsage = Convert.ToDecimal(colValue);
            //                                        else if (serviceType.ToLower().Trim().Contains("voice"))
            //                                            bCtB.BCtFbTranVUsage = Convert.ToDecimal(colValue);
            //                                        else if (serviceType.ToLower().Equals("streaming"))
            //                                            bCtC.BCtFbTranStUsage = Convert.ToDecimal(colValue);
            //                                        else
            //                                        {
            //                                            bCtB.BCtFbRecurFee = null;
            //                                            //bCtB.BCtFbTranDUsage = null;
            //                                            bCtB.BCtFbTranVUsage = null;
            //                                            bCtB.BCFbCurBill = null;
            //                                            bCtC.BCtFbTranStUsage = null;
            //                                        }
            //                                    }
            //                                    catch
            //                                    {
            //                                        bCtB.BCtFbTranDUsage = null;
            //                                        bCtB.BCtFbTranVUsage = null;
            //                                        bCtC.BCtFbTranStUsage = null;
            //                                    }
            //                                }
            //                                else
            //                                {
            //                                    bCtB.BCtFbTranDUsage = null;
            //                                    bCtB.BCtFbTranVUsage = null;
            //                                    bCtC.BCtFbTranStUsage = null;
            //                                }
            //                            }
            //                        }

            //                        if (!string.IsNullOrEmpty(serviceType))
            //                        {
            //                            try
            //                            {
            //                                if (serviceType.ToLower().Trim().Equals("subscription fee"))
            //                                {
            //                                    bCtB.BCtFbTranVStartDate = null;
            //                                    bCtC.BCtFbTranStStartDate = null;

            //                                    bCtB.BCtFbTranVStartTime = null;
            //                                    bCtC.BCtFbTranStStartTime = null;

            //                                    billingSystemEntities.BCtBs.Add(bCtB);
            //                                }
            //                                else if (serviceType.ToLower().Trim().Equals("standard ip"))
            //                                {
            //                                    bCtB.BCtFbTranVStartDate = null;
            //                                    bCtC.BCtFbTranStStartDate = null;

            //                                    bCtB.BCtFbTranVStartTime = null;
            //                                    bCtC.BCtFbTranStStartTime = null;

            //                                    billingSystemEntities.BCtBs.Add(bCtB);
            //                                }
            //                                else if (serviceType.ToLower().Trim().Contains("voice"))
            //                                {
            //                                    bCtB.BCtFbTranDStartDate = null;
            //                                    bCtC.BCtFbTranStStartDate = null;

            //                                    bCtB.BCtFbTranDStartTime = null;
            //                                    bCtC.BCtFbTranStStartTime = null;

            //                                    billingSystemEntities.BCtBs.Add(bCtB);
            //                                }
            //                                else if (serviceType.ToLower().Equals("stream"))
            //                                {
            //                                    bCtB.BCtFbTranDStartDate = null;
            //                                    bCtB.BCtFbTranVStartDate = null;

            //                                    bCtB.BCtFbTranDStartTime = null;
            //                                    bCtB.BCtFbTranVStartTime = null;

            //                                    billingSystemEntities.BCtCs.Add(bCtC);
            //                                }
            //                            }
            //                            catch
            //                            {
            //                            }
            //                        }
            //                    }
            //                }
            //                billingSystemEntities.SaveChanges();

            //                return dictDuration;
            //            }
            //        }
            //    }
            //    catch (DbEntityValidationException e)
            //    {
            //        string errormsg = string.Empty;
            //        foreach (var eve in e.EntityValidationErrors)
            //        {
            //            errormsg = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors: ", eve.Entry.Entity.GetType().Name, eve.Entry.State);
            //            foreach (var ve in eve.ValidationErrors)
            //            {
            //                errormsg = string.Format(" - Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
            //            }
            //        }
            //        throw e;
            //    }
            //    catch (Exception erro)
            //    {
            //        throw (new Exception(erro.Message.ToString()));
            //    }

            //    finally
            //    {
            //        if (File.Exists(fileName))
            //            File.Delete(fileName);
            //    }
            Dictionary<string, string> tedt = new Dictionary<string, string>();
            return tedt;
        }

        public string RejectCondition(Dictionary<string, string> dictDuration)
        {
            //    BillingSystemEntities db = new BillingSystemEntities();

            //    // CDR from LES to Retreive all record as List
            //    List<BCtA> lstbCtAs = db.BCtAs.ToList();
            //    List<BCtB> lstbCtBs = db.BCtBs.ToList();
            //    List<BCtC> lstbCtCs = db.BCtCs.ToList();
            //    List<BCtD> lstbCtDs = db.BCtDs.ToList();

            //    // Create new instance for Rejection Table
            //    Bst1A bst1A = new Bst1A();
            //    Bst1B bst1B = new Bst1B();
            //    BSt1C bSt1C = new BSt1C();

            //    //Create New Instance for Invoice Table
            //    BSt2A bSt2A = new BSt2A();
            //    BSt2B bSt2B = new BSt2B();
            //    BSt2C bSt2C = new BSt2C();

            //    BsPtLesReg bsPtLesReg = new BsPtLesReg();

            //    StringBuilder ErrorMsg = new StringBuilder();

            //    // Les Register Cost Price data as List
            //    List<BsPtLesReg> lstlesRegs = db.BsPtLesRegs.ToList();

            //    // Customer Seller price data as List
            //    List<BPtFbbCusReg> lstCusReg = db.BPtFbbCusRegs.ToList();


            //    /// Author: Karthi
            //    /// Checking Less Registeration CDR to Common CDR
            //    List<string> lstIMSI = db.BCtBs.Select(t => t.BCtFbIMSIId).Distinct().ToList();

            //    // foreach (var Imsi in lstIMSI)
            //    foreach (var Imsi in dictDuration.Keys)
            //    {
            //        if (lstlesRegs != null)
            //        {
            //            try
            //            {
            //                DateTime? cdrStartDate = null;
            //                DateTime? cdrEndDate = null;
            //                string InvPerid = string.Empty;
            //                int month;
            //                try
            //                {
            //                    if (dictDuration.ContainsKey(Imsi))
            //                    {
            //                        InvPerid = dictDuration[Imsi];
            //                    }
            //                    if (InvPerid.Contains('-'))
            //                    {
            //                        string[] dest = InvPerid.Split('-');
            //                        if (dest.Length > 0)
            //                        {
            //                            dest[0] = dest[0].Replace(" ", string.Empty);
            //                            cdrStartDate = DateTime.Parse(dest[0]);

            //                            dest[1] = dest[1].Replace(" ", string.Empty);
            //                            cdrEndDate = DateTime.Parse(dest[1]);
            //                        }
            //                        //if (dest.Length > 1)
            //                        //{
            //                        //    string curedate = dest[1].Replace(" ", string.Empty) + " 12:00 PM";
            //                        //    System.Globalization.CultureInfo provider = System.Globalization.CultureInfo.InvariantCulture;
            //                        //    cdrEndDate = DateTime.ParseExact(curedate, "d/M/yyyy HH:mm tt", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.NoCurrentDateDefault);
            //                        //    //cdrEndDate = Convert.ToDateTime(curedate).ToString("dd/mm/yyyy HH:mm tt");
            //                        //}
            //                    }
            //                }
            //                catch (FormatException roro) { ErrorMsg.AppendFormat("MSI:{0} Error Msg: {1} Stack Trace: {2}. <br />", Imsi, roro.Message.ToString(), roro.StackTrace); }
            //                catch (Exception rr) { ErrorMsg.AppendFormat("MSI:{0} Error Msg: {1} Stack Trace: {2}. <br />", Imsi, rr.Message.ToString(), rr.StackTrace); }

            //                var CT_SubscriptionFee = lstbCtBs.FirstOrDefault(t => t.BCtFbIMSIId.Equals(Imsi) && t.BCtFbTranDServType != null && t.BCtFbTranDDest.Equals(InvPerid) && t.BCtFbTranDServType.ToLower().Equals("subscription fee"));

            //                if (lstlesRegs.Select(t => t.BPtLesRgnFBimSingle).Contains(Imsi))
            //                {
            //                    // LES Registeration data for current Imsi id
            //                    BsPtLesReg imsiLesreg = lstlesRegs.FirstOrDefault(t => t.BPtLesRgnFBimSingle.Equals(Imsi));

            //                    decimal? lesRecurFee = imsiLesreg.BPtLesRgnFbRecurFee;
            //                    decimal? LES_DataRateper1MB = imsiLesreg.BPtLesRgnFbDataRate;
            //                    decimal? LES_VoicetoFixed = imsiLesreg.BPtLesRgnFbVce2Fixed;
            //                    decimal? LES_VoicetoCellur = imsiLesreg.BPtLesRgnFbVce2Cell;
            //                    decimal? LES_VoicetoFbb = imsiLesreg.BPtLesRgnFbVce2Fb;
            //                    decimal? LES_VoicetoIridium = imsiLesreg.BPtLesRgnFb2Iridium;
            //                    decimal? VoicetoGpsp = imsiLesreg.BPtLesRgnFbVce2Gpsp;
            //                    decimal? VoicetoVceMail = imsiLesreg.BPtLesRgnFbVce2VceMail;
            //                    decimal? VoicetoOthers = imsiLesreg.BPtLesRgnFbVce2Others;
            //                    decimal? VoicetoBillIncr = imsiLesreg.BPtLesRgnFbVcebillincre;

            //                    bool isDataMatched = false;
            //                    bool isVcetoFixedMatched = false;
            //                    bool isVcetoCellMatched = false;
            //                    bool isVcetoFbMatched = false;
            //                    bool isMRCMatched = false;
            //                    bool isVcetoIridium = false;

            //                    if (LES_DataRateper1MB != null)
            //                    {
            //                        // <-------------------------- Standard IP: Data Paln and Call data ------------------>
            //                        decimal? DataUnits_CDR = lstbCtBs.AsEnumerable()
            //                                                .Where(t => t.BCtFbTranDServType != null && t.BCtFbTranDServType.ToLower().Equals("standard ip") && (t.BCtFbIMSIId.Equals(Imsi)))
            //                                                .GroupBy(t => t.BCtFbIMSIId.Equals(Imsi))
            //                                                .Select(t => t.Sum(s => s.BCtFbBilIncre)).FirstOrDefault();
            //                        DataUnits_CDR = Math.Round(Convert.ToDecimal(DataUnits_CDR), 2, MidpointRounding.AwayFromZero);

            //                        decimal? CDR_DataCP = lstbCtBs.AsEnumerable()
            //                                                    .Where(t => t.BCtFbTranDServType != null && t.BCtFbTranDServType.ToLower().Equals("standard ip") && (t.BCtFbIMSIId.Equals(Imsi)))
            //                                                    .GroupBy(t => t.BCtFbIMSIId.Equals(Imsi))
            //                                                    .Select(t => t.Sum(s => s.BCtFbTranDUsage)).FirstOrDefault();
            //                        CDR_DataCP = Math.Round(Convert.ToDecimal(CDR_DataCP), 2, MidpointRounding.AwayFromZero);

            //                        // <--------------------------- Data Paln and Call data ----------------------------->

            //                        // <---------------------------  Voice to Fixed ----------------------------->
            //                        decimal? CDR_Vce2FixedCostPriceCT = lstbCtBs.AsEnumerable().Where(t => t.BCtFbTranVServType != null && t.BCtFbTranVServType.ToLower().Equals("voice  to fixed")
            //                                                                                         && (t.BCtFbIMSIId.Equals(Imsi)))
            //                                                                                 .GroupBy(t => t.BCtFbIMSIId.Equals(Imsi))
            //                                                                                 .Select(t => t.Sum(s => s.BCtFbTranVUsage)).FirstOrDefault();

            //                        if (CDR_Vce2FixedCostPriceCT != null)
            //                            CDR_Vce2FixedCostPriceCT = Math.Round(Convert.ToDecimal(CDR_Vce2FixedCostPriceCT), 2, MidpointRounding.AwayFromZero);

            //                        decimal? CDR_Vce2FixedUintsCT = lstbCtBs.AsEnumerable().Where(t => t.BCtFbTranVServType != null && t.BCtFbTranVServType.ToLower().Equals("voice  to fixed")
            //                                                                                        && (t.BCtFbIMSIId.Equals(Imsi)))
            //                                                                               .GroupBy(t => t.BCtFbIMSIId.Equals(Imsi))
            //                                                                               .Select(t => t.Sum(s => s.BCtFbVceBillIncre)).FirstOrDefault();

            //                        if (CDR_Vce2FixedUintsCT != null)
            //                            CDR_Vce2FixedUintsCT = Math.Round(Convert.ToDecimal(CDR_Vce2FixedUintsCT), 2, MidpointRounding.AwayFromZero);

            //                        // <---------------------------  Voice to Fixed ----------------------------->

            //                        //------------------- Voice to Cellur ---------------- Cost Price vise and Common CDR
            //                        decimal? CDR_Vce2CellurUnitsCT = db.BCtBs.AsEnumerable().Where(t => t.BCtFbTranVServType != null
            //                                                                                        && t.BCtFbTranVServType.ToLower().Equals("voice  to cellular")
            //                                                                                        && (t.BCtFbIMSIId.Equals(Imsi)))
            //                                                                                .GroupBy(t => t.BCtFbIMSIId.Equals(Imsi))
            //                                                                                .Select(t => t.Sum(s => s.BCtFbVceBillIncre)).FirstOrDefault();

            //                        if (CDR_Vce2CellurUnitsCT != null)
            //                            CDR_Vce2CellurUnitsCT = Math.Round(Convert.ToDecimal(CDR_Vce2CellurUnitsCT), 2, MidpointRounding.AwayFromZero);

            //                        decimal? CDR_Vce2CellCostPriceCT = db.BCtBs.AsEnumerable().Where(t => t.BCtFbTranVServType != null
            //                                                                                            && t.BCtFbTranVServType.ToLower().Equals("voice  to cellular")
            //                                                                                            && (t.BCtFbIMSIId.Equals(Imsi)))
            //                                                                                .GroupBy(t => t.BCtFbIMSIId.Equals(Imsi))
            //                                                                                .Select(t => t.Sum(s => s.BCtFbTranVUsage)).FirstOrDefault();

            //                        if (CDR_Vce2CellCostPriceCT != null)
            //                            CDR_Vce2CellCostPriceCT = Math.Round(Convert.ToDecimal(CDR_Vce2CellCostPriceCT), 2, MidpointRounding.AwayFromZero);

            //                        //---------------  Voice to Fleet Board Band -------------------- Cost Price vise and Common CDR
            //                        decimal? CDR_Vce2FbUnitsCT = db.BCtBs.AsEnumerable().Where(t => t.BCtFbTranVServType != null
            //                                                                                         && t.BCtFbTranVServType.ToLower().Equals("voice  to fleetbroadband")
            //                                                                                         && (t.BCtFbIMSIId.Equals(Imsi)))
            //                                                                            .GroupBy(t => t.BCtFbIMSIId.Equals(Imsi))
            //                                                                            .Select(t => t.Sum(s => s.BCtFbVceBillIncre)).FirstOrDefault();

            //                        if (CDR_Vce2FbUnitsCT != null)
            //                            CDR_Vce2FbUnitsCT = Math.Round(Convert.ToDecimal(CDR_Vce2FbUnitsCT), 2, MidpointRounding.AwayFromZero);

            //                        decimal? CDR_Vce2FbCostPriceCT = db.BCtBs.AsEnumerable().Where(t => t.BCtFbTranVServType != null
            //                                                                                        && t.BCtFbTranVServType.ToLower().Equals("voice  to fleetbroadband")
            //                                                                                        && (t.BCtFbIMSIId.Equals(Imsi)))
            //                                                                              .GroupBy(t => t.BCtFbIMSIId.Equals(Imsi))
            //                                                                              .Select(t => t.Sum(s => s.BCtFbTranVUsage)).FirstOrDefault();

            //                        if (CDR_Vce2FbCostPriceCT != null)
            //                            CDR_Vce2FbCostPriceCT = Math.Round(Convert.ToDecimal(CDR_Vce2FbCostPriceCT), 2, MidpointRounding.AwayFromZero);

            //                        //--------------------  Voice to Iridium ------------- Cost price from Common CDR
            //                        decimal? CDR_Vce2IridiumUnitsCT = db.BCtBs.AsEnumerable().Where(t => t.BCtFbTranVServType != null
            //                                                                                         && t.BCtFbTranVServType.ToLower().Equals("voice  to iridium")
            //                                                                                         && (t.BCtFbIMSIId.Equals(Imsi)))
            //                                                                            .GroupBy(t => t.BCtFbIMSIId.Equals(Imsi))
            //                                                                            .Select(t => t.Sum(s => s.BCtFbVceBillIncre)).FirstOrDefault();

            //                        if (CDR_Vce2IridiumUnitsCT != null)
            //                            CDR_Vce2IridiumUnitsCT = Math.Round(Convert.ToDecimal(CDR_Vce2IridiumUnitsCT), 2, MidpointRounding.AwayFromZero);

            //                        decimal? CDR_Vce2IridiumCostPriceCT = db.BCtBs.AsEnumerable().Where(t => t.BCtFbTranVServType != null
            //                                                                                        && t.BCtFbTranVServType.ToLower().Equals("voice  to iridium")
            //                                                                                        && (t.BCtFbIMSIId.Equals(Imsi)))
            //                                                                              .GroupBy(t => t.BCtFbIMSIId.Equals(Imsi))
            //                                                                              .Select(t => t.Sum(s => s.BCtFbTranVUsage)).FirstOrDefault();

            //                        if (CDR_Vce2IridiumCostPriceCT != null)
            //                            CDR_Vce2IridiumCostPriceCT = Math.Round(Convert.ToDecimal(CDR_Vce2IridiumCostPriceCT), 2, MidpointRounding.AwayFromZero);

            //                        //===================================== CDR Total Units and Cost from current month LES CDR ========================

            //                        // Customer Registeration sells prices for curretn imsi id

            //                        BPtFbbCusReg cusRegSP = null;

            //                        decimal? Cus_RecurFeeSP = 0;
            //                        decimal? Cus_dataRateper1MBSP = 0;
            //                        decimal? Cus_VoicetoFixedSP = 0;
            //                        decimal? Cus_VoicetoCellSP = 0;
            //                        decimal? Cus_VoicetoFbbSP = 0;
            //                        decimal? Cus_VoicetoGpspSP = 0;
            //                        decimal? Cus_VoicetoVceMailSP = 0;
            //                        decimal? Cus_VoicetoOthersSP = 0;
            //                        decimal? Cus_VoicetoBillIncrSP = 0;
            //                        decimal? Cus_VoicetoIridiumSP = 0;
            //                        decimal? Cus_OutBundleDataRateSP = 0;
            //                        decimal? Cus_DataPlanSP = 0;

            //                        try
            //                        {
            //                            cusRegSP = db.BPtFbbCusRegs.FirstOrDefault(t => t.BPtCusFBimSingle.Equals(Imsi));
            //                        }
            //                        catch
            //                        {
            //                            ErrorMsg.AppendFormat("The IMSI: {0} not registerd in Customer Registeration Table. <br />", Imsi).AppendLine();
            //                        }

            //                        if (cusRegSP != null)
            //                        {
            //                            Cus_dataRateper1MBSP = cusRegSP.BPtCusFbDataRate;
            //                            Cus_VoicetoFixedSP = cusRegSP.BPtCusFbVce2Fixed;
            //                            Cus_VoicetoCellSP = cusRegSP.BPtCusFbVce2Cell;
            //                            Cus_VoicetoFbbSP = cusRegSP.BPtCusFbVce2Fb;
            //                            Cus_VoicetoIridiumSP = cusRegSP.BPtCusFb2Iridium;
            //                            Cus_VoicetoGpspSP = cusRegSP.BPtCusFbVce2Gpsp;
            //                            Cus_VoicetoVceMailSP = cusRegSP.BPtCusFbVce2VceMail;
            //                            Cus_VoicetoOthersSP = cusRegSP.BPtCusFbVce2Others;
            //                            Cus_VoicetoBillIncrSP = cusRegSP.BPtCusFbVcebillincre;
            //                            Cus_RecurFeeSP = cusRegSP.BPtCusFbRecurFee;
            //                            Cus_OutBundleDataRateSP = cusRegSP.BPtCusFbOutBundleDataRate;
            //                            if (!string.IsNullOrEmpty(cusRegSP.BPtCusFbDataPlanIn))
            //                                Cus_DataPlanSP = Convert.ToDecimal(cusRegSP.BPtCusFbDataPlanIn);
            //                        }
            //                        else
            //                        {
            //                            ErrorMsg.AppendFormat("The IMSI: {0} not registerd in Customer Registeration Table. <br />", Imsi).AppendLine();
            //                            break;
            //                        }

            //                        if (DataUnits_CDR != null)
            //                        {
            //                            long data = Convert.ToInt64(Convert.ToDecimal(DataUnits_CDR));
            //                            decimal? CDR_DataTotalUsageMB = (decimal)Converters.ConvertKilobytesToMegabytes(data);

            //                            if (CDR_DataTotalUsageMB != null)
            //                                CDR_DataTotalUsageMB = Math.Round(Convert.ToDecimal(CDR_DataTotalUsageMB), 2, MidpointRounding.AwayFromZero);

            //                            decimal? LES_DataCP = LES_DataRateper1MB * CDR_DataTotalUsageMB;
            //                            decimal? Cus_DataSP = CDR_DataTotalUsageMB * Cus_dataRateper1MBSP;

            //                            if (Cus_DataSP != null)
            //                                Cus_DataSP = Math.Round(Convert.ToDecimal(Cus_DataSP), 2, MidpointRounding.AwayFromZero);

            //                            if (LES_DataCP != null)
            //                                LES_DataCP = Math.Round(Convert.ToDecimal(LES_DataCP), 2, MidpointRounding.AwayFromZero);

            //                            if ((CT_SubscriptionFee.BCtFbRecurFee < lesRecurFee || CT_SubscriptionFee.BCtFbRecurFee.Equals(lesRecurFee))
            //                                    && CT_SubscriptionFee.BCtFbRecurFee < Cus_RecurFeeSP)
            //                            {
            //                                if ((CDR_DataCP < LES_DataCP || CDR_DataCP.Equals(LES_DataCP)) && (CDR_DataCP < Cus_DataSP))
            //                                {
            //                                    isDataMatched = true;

            //                                    //---------------------- Voice to Fixed ------------------>
            //                                    if (CDR_Vce2FixedUintsCT != null)
            //                                    {
            //                                        double lngUnits = Convert.ToDouble(CDR_Vce2FixedUintsCT);
            //                                        decimal? CDR_Vce2FixedtotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

            //                                        if (CDR_Vce2FixedtotalUnits_Minutes != null)
            //                                            CDR_Vce2FixedtotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_Vce2FixedtotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

            //                                        decimal? LES_Vce2FixedCPperMin = CDR_Vce2FixedtotalUnits_Minutes * LES_VoicetoFixed;
            //                                        decimal? CUS_Vce2FixedSPperMin = CDR_Vce2FixedtotalUnits_Minutes * Cus_VoicetoFixedSP;

            //                                        if (LES_Vce2FixedCPperMin != null)
            //                                            LES_Vce2FixedCPperMin = Math.Round(Convert.ToDecimal(LES_Vce2FixedCPperMin), 2, MidpointRounding.AwayFromZero);

            //                                        if (CUS_Vce2FixedSPperMin != null)
            //                                            CUS_Vce2FixedSPperMin = Math.Round(Convert.ToDecimal(CUS_Vce2FixedSPperMin), 2, MidpointRounding.AwayFromZero);

            //                                        if ((LES_Vce2FixedCPperMin.Equals(CDR_Vce2FixedCostPriceCT) || LES_Vce2FixedCPperMin > CDR_Vce2FixedCostPriceCT)
            //                                            && (LES_Vce2FixedCPperMin < CUS_Vce2FixedSPperMin))
            //                                        {
            //                                            isVcetoFixedMatched = true;
            //                                        }
            //                                        else
            //                                        {
            //                                            isVcetoFixedMatched = false;
            //                                            ErrorMsg.AppendFormat("The IMSI: {0} Voice to Fixed rate is not match in LES/CUS registeration CDR. <br />", Imsi).AppendLine();
            //                                        }
            //                                    }
            //                                    else
            //                                        isVcetoFixedMatched = true;

            //                                    // --------------------- Voice to Cell ------------------->
            //                                    if (CDR_Vce2CellurUnitsCT != null)
            //                                    {
            //                                        double lngUints = Convert.ToDouble(CDR_Vce2CellurUnitsCT);
            //                                        decimal? CDR_Vce2CellTotalUnits_Minutes = (decimal)Converters.ConvertSecondsToMinutes(lngUints);

            //                                        if (CDR_Vce2CellTotalUnits_Minutes != null)
            //                                            CDR_Vce2CellTotalUnits_Minutes = Math.Round(Convert.ToDecimal(CDR_Vce2CellTotalUnits_Minutes), 2, MidpointRounding.AwayFromZero);

            //                                        decimal? LES_Vce2CellCPperMin = CDR_Vce2CellTotalUnits_Minutes * LES_VoicetoCellur;

            //                                        decimal? CUS_Vce2CellSPperMin = CDR_Vce2CellTotalUnits_Minutes * Cus_VoicetoCellSP;

            //                                        if (LES_Vce2CellCPperMin != null)
            //                                            LES_Vce2CellCPperMin = Math.Round(Convert.ToDecimal(LES_Vce2CellCPperMin), 2, MidpointRounding.AwayFromZero);

            //                                        if (CUS_Vce2CellSPperMin != null)
            //                                            CUS_Vce2CellSPperMin = Math.Round(Convert.ToDecimal(CUS_Vce2CellSPperMin), 2, MidpointRounding.AwayFromZero);

            //                                        if ((LES_Vce2CellCPperMin.Equals(CDR_Vce2CellCostPriceCT) || LES_Vce2CellCPperMin > CDR_Vce2CellCostPriceCT)
            //                                                && (LES_Vce2CellCPperMin < CUS_Vce2CellSPperMin))
            //                                        {
            //                                            isVcetoCellMatched = true;
            //                                        }
            //                                        else
            //                                        {
            //                                            isVcetoCellMatched = false;
            //                                            ErrorMsg.AppendFormat("The IMSI: {0} Voice to Cellular rate is not match in CUS/LES registeration CDR.", Imsi).AppendLine();
            //                                        }
            //                                    }
            //                                    else
            //                                        isVcetoCellMatched = false;

            //                                    // ------------------- Voice to FBB ---------------------->
            //                                    if (CDR_Vce2FbUnitsCT != null)
            //                                    {
            //                                        double lngUints = Convert.ToDouble(CDR_Vce2FbUnitsCT);
            //                                        decimal? CDR_Vce2FbTotalUnitsMin = (decimal)Converters.ConvertSecondsToMinutes(lngUints);

            //                                        if (CDR_Vce2FbTotalUnitsMin != null)
            //                                            CDR_Vce2FbTotalUnitsMin = Math.Round(Convert.ToDecimal(CDR_Vce2FbTotalUnitsMin), 2, MidpointRounding.AwayFromZero);

            //                                        decimal? LES_Vce2FbCPMin = CDR_Vce2FbTotalUnitsMin * LES_VoicetoFbb;

            //                                        decimal? Cus_Vce2FbSPMin = CDR_Vce2FbTotalUnitsMin * Cus_VoicetoFbbSP;

            //                                        if (CDR_Vce2FbTotalUnitsMin != null)
            //                                            CDR_Vce2FbTotalUnitsMin = Math.Round(Convert.ToDecimal(CDR_Vce2FbTotalUnitsMin), 2, MidpointRounding.AwayFromZero);

            //                                        if (Cus_Vce2FbSPMin != null)
            //                                            Cus_Vce2FbSPMin = Math.Round(Convert.ToDecimal(Cus_Vce2FbSPMin), 2, MidpointRounding.AwayFromZero);

            //                                        if ((LES_Vce2FbCPMin.Equals(CDR_Vce2FbCostPriceCT) || LES_Vce2FbCPMin > CDR_Vce2FbCostPriceCT)
            //                                                && (CDR_Vce2FbCostPriceCT < Cus_Vce2FbSPMin))
            //                                        {
            //                                            isVcetoFbMatched = true;
            //                                        }
            //                                        else
            //                                        {
            //                                            isVcetoFbMatched = false;
            //                                            ErrorMsg.AppendFormat("The IMSI: {0} Voice to fleetbroadband rate is not match in CDR/LES registeration CDR. <br />", Imsi).AppendLine();
            //                                        }
            //                                    }
            //                                    else
            //                                        isVcetoFbMatched = true;

            //                                    // -------------------------- Voice to Iridium --------------------->
            //                                    if (CDR_Vce2IridiumUnitsCT != null && CDR_Vce2IridiumUnitsCT > 0)
            //                                    {
            //                                        double lngUnits = Convert.ToDouble(CDR_Vce2IridiumUnitsCT);
            //                                        decimal? CDR_Vce2IridiumTotalUnitsMin = (decimal)Converters.ConvertSecondsToMinutes(lngUnits);

            //                                        if (CDR_Vce2IridiumTotalUnitsMin != null)
            //                                            CDR_Vce2IridiumTotalUnitsMin = Math.Round(Convert.ToDecimal(CDR_Vce2IridiumTotalUnitsMin), 2, MidpointRounding.AwayFromZero);

            //                                        decimal? LES_Vce2IridiumCPMin = CDR_Vce2IridiumTotalUnitsMin * LES_VoicetoIridium;

            //                                        decimal? Cus_Vce2IridiumSPMin = CDR_Vce2IridiumTotalUnitsMin * Cus_VoicetoIridiumSP;

            //                                        if (LES_Vce2IridiumCPMin != null)
            //                                            LES_Vce2IridiumCPMin = Math.Round(Convert.ToDecimal(LES_Vce2IridiumCPMin), 2, MidpointRounding.AwayFromZero);

            //                                        if (Cus_Vce2IridiumSPMin != null)
            //                                            Cus_Vce2IridiumSPMin = Math.Round(Convert.ToDecimal(Cus_Vce2IridiumSPMin), 2, MidpointRounding.AwayFromZero);

            //                                        if ((LES_Vce2IridiumCPMin.Equals(CDR_Vce2IridiumCostPriceCT) || LES_Vce2IridiumCPMin > CDR_Vce2IridiumCostPriceCT)
            //                                                && (CDR_Vce2IridiumCostPriceCT < Cus_Vce2IridiumSPMin))
            //                                        {

            //                                            isVcetoIridium = true;
            //                                        }
            //                                        else
            //                                        {
            //                                            isVcetoIridium = false;
            //                                            ErrorMsg.AppendFormat("The IMSI: {0} Voice to Iridium rate is not match in CDR/LES registeration CDR. <br />", Imsi).AppendLine();
            //                                        }
            //                                    }
            //                                    else
            //                                        isVcetoIridium = false;
            //                                }
            //                                else
            //                                {
            //                                    isDataMatched = false;
            //                                    ErrorMsg.AppendFormat("The IMSI: {0} data rate is less than the customer registeration rate. <br />", Imsi).AppendLine();
            //                                }
            //                            }
            //                            else
            //                            {
            //                                isMRCMatched = false;
            //                                ErrorMsg.AppendFormat("The IMSI: {0} Recurring Fee(MRC) is not match the cutomer registeration recurring fee(MRC). <br />", Imsi).AppendLine();
            //                            }

            //                            DateTime? startDateLes = imsiLesreg.BPtLesRgnFBtartDate;
            //                            DateTime? contractStartDateLes = imsiLesreg.BPtLesRgnFbContStart;
            //                            DateTime? barDateLes = imsiLesreg.BPtLesRgnFbBarDate;
            //                            DateTime? suspensionDateLes = imsiLesreg.BPtLesRgnFBusDate;
            //                            DateTime? layupDateLes = imsiLesreg.BPtLesRgnFblayUpDate;
            //                            Decimal? CurrBillLes = imsiLesreg.BPtLesRgnFbCurBill;
            //                            TimeSpan? StandardIpTimeLes = imsiLesreg.BPtLesRgnFbTranDStartTime;
            //                            string InvoiceNo = string.Empty;

            //                            try
            //                            {
            //                                InvoiceNo = db.BSt2A.Max(t => t.InvoiceNo);

            //                                if (!string.IsNullOrEmpty(InvoiceNo))
            //                                {
            //                                    if (InvoiceNo.Contains("SM"))
            //                                    {
            //                                        string tempInv = InvoiceNo.Replace("SM", string.Empty);
            //                                        if (int.TryParse(tempInv, out int result))
            //                                        {
            //                                            InvoiceNo = GenInvoice(false, result);
            //                                        }
            //                                    }
            //                                }
            //                                else
            //                                    InvoiceNo = GenInvoice(false, 0);
            //                            }
            //                            catch (Exception error)
            //                            {
            //                                ErrorMsg.AppendFormat("MSI:{0} Error Msg: {1} Stack Trace: {2}. <br />", Imsi, error.Message.ToString(), error.StackTrace);
            //                            }

            //                            if ((startDateLes > cdrStartDate && contractStartDateLes > cdrStartDate && barDateLes > cdrStartDate
            //                                && suspensionDateLes > cdrStartDate && layupDateLes > cdrStartDate) && (
            //                                isDataMatched && isVcetoFixedMatched && isVcetoCellMatched && isVcetoFbMatched))
            //                            {
            //                                bSt2A = new BSt2A();

            //                                if (!string.IsNullOrEmpty(InvoiceNo))
            //                                {
            //                                    //decimal? dateRate1KB_SP = dataRateper1MB_SP / 1024;
            //                                    //dateRate1KB_SP = Math.Round(Convert.ToDecimal(dateRate1KB_SP), 2, MidpointRounding.AwayFromZero);

            //                                    //decimal? Vce2Fixed1Sec_SP = VoicetoFixed_SP / 60;
            //                                    //Vce2Fixed1Sec_SP = Math.Round(Convert.ToDecimal(Vce2Fixed1Sec_SP), 2, MidpointRounding.AwayFromZero);

            //                                    //decimal? Vce2Cell1Sec_SP = VoicetoCell_SP / 60;
            //                                    //Vce2Cell1Sec_SP = Math.Round(Convert.ToDecimal(Vce2Cell1Sec_SP), 2, MidpointRounding.AwayFromZero);

            //                                    //decimal? Vce2Fbb1Sec_SP = VoicetoFbb_SP / 60;
            //                                    //Vce2Fbb1Sec_SP = Math.Round(Convert.ToDecimal(Vce2Fbb1Sec_SP), 2, MidpointRounding.AwayFromZero);

            //                                    bSt2A.InvoiceNo = InvoiceNo;
            //                                    bSt2A.Bt2CusFbIMSIId = Imsi;
            //                                    bSt2A.Bt2CusFbMSIDNNo = CT_SubscriptionFee.BCtFbMSIDNNo;
            //                                    bSt2A.Bt2CusFbSimIdNo = CT_SubscriptionFee.BCtFbSimIdNo;
            //                                    bSt2A.Bt2CusFbStartDate = cdrStartDate;
            //                                    bSt2A.Bt2CuspFbEndDate = cdrEndDate;
            //                                    bSt2A.Bt2CusFbServType = CT_SubscriptionFee.BCtFbTranDServType;
            //                                    bSt2A.Bt2CusFbRecurFee = Cus_RecurFeeSP;
            //                                    bSt2A.Bt2CusFbCusName = db.BPtFbbCusRegs.FirstOrDefault(t => t.BPtCusFBimSingle.Equals(Imsi)).BPtCusName;
            //                                    bSt2A.Bt2CusFbCustomerAddress = db.BPtFbbCusRegs.FirstOrDefault(t => t.BPtCusFBimSingle.Equals(Imsi)).BPtCusAddrSt;

            //                                    db.BSt2A.Add(bSt2A);

            //                                    if (CT_SubscriptionFee.BCtFbRecurFee < lesRecurFee || CT_SubscriptionFee.BCtFbRecurFee.Equals(lesRecurFee))
            //                                    {
            //                                        bSt2B = new BSt2B();
            //                                        bSt2B.Bt2CusFbIMSIId = CT_SubscriptionFee.BCtFbIMSIId;
            //                                        bSt2B.Bt2CusFbMSIDNNo = CT_SubscriptionFee.BCtFbMSIDNNo;
            //                                        bSt2B.Bt2CusFbSimIdNo = CT_SubscriptionFee.BCtFbSimIdNo;
            //                                        //
            //                                        bSt2B.Bt2CusFbTranDServType = CT_SubscriptionFee.BCtFbTranDServType;

            //                                        bSt2B.Bt2CusFbTranDStartDate = CT_SubscriptionFee.BCtFbTranDStartDate;

            //                                        bSt2B.Bt2CusFbTranDStartTime = CT_SubscriptionFee.BCtFbTranDStartTime;
            //                                        bSt2B.Bt2CusFbTranDEndTime = CT_SubscriptionFee.BCtFbTranDEndTime;

            //                                        bSt2B.Bt2CusFbTranDOrig = CT_SubscriptionFee.BCtFbTranDOrig;
            //                                        bSt2B.Bt2CusFbTranDDest = CT_SubscriptionFee.BCtFbTranDDest;

            //                                        bSt2B.Bt2CusFbBilIncre = CT_SubscriptionFee.BCtFbBilIncre;
            //                                        bSt2B.Bt2CusFbRecurFee = Cus_RecurFeeSP;
            //                                        bSt2B.Bt2CusFbBillUnitType = CT_SubscriptionFee.BCtFbunitType;

            //                                        db.BSt2B.Add(bSt2B);
            //                                    }

            //                                    List<BCtB> lstCTB = db.BCtBs.Where(t => t.BCtFbIMSIId.Equals(Imsi) && t.BCtFbTranDServType != null &&
            //                                                                        t.BCtFbTranDServType.ToLower().Equals("standard ip")).ToList();
            //                                    if (lstCTB != null)
            //                                    {
            //                                        decimal dataOutBundle = 0;
            //                                        foreach (var record in lstCTB)
            //                                        {
            //                                            bSt2B = new BSt2B();
            //                                            bSt2B.Bt2CusFbIMSIId = record.BCtFbIMSIId;
            //                                            bSt2B.Bt2CusFbMSIDNNo = record.BCtFbMSIDNNo;
            //                                            bSt2B.Bt2CusFbSimIdNo = record.BCtFbSimIdNo;
            //                                            //
            //                                            bSt2B.Bt2CusFbTranDServType = record.BCtFbTranDServType;

            //                                            bSt2B.Bt2CusFbTranDStartDate = record.BCtFbTranDStartDate;

            //                                            bSt2B.Bt2CusFbTranDStartTime = record.BCtFbTranDStartTime;
            //                                            bSt2B.Bt2CusFbTranDEndTime = record.BCtFbTranDEndTime;

            //                                            bSt2B.Bt2CusFbTranDOrig = record.BCtFbTranDOrig;
            //                                            bSt2B.Bt2CusFbTranDDest = record.BCtFbTranDDest;

            //                                            long dataconvertToMP = Convert.ToInt64(record.BCtFbBilIncre);

            //                                            bSt2B.Bt2CusFbBilIncre = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertKilobytesToMegabytes(dataconvertToMP)), 2, MidpointRounding.AwayFromZero);

            //                                            dataOutBundle += Convert.ToDecimal(bSt2B.Bt2CusFbBilIncre);

            //                                            if (Cus_DataPlanSP < dataOutBundle)
            //                                            {
            //                                                bSt2B.Bt2CusFbisOutBundle = true;
            //                                                bSt2B.Bt2CusFbTranDUsage = Math.Round(Convert.ToDecimal(bSt2B.Bt2CusFbBilIncre * Cus_OutBundleDataRateSP), 2, MidpointRounding.AwayFromZero);
            //                                            }
            //                                            else
            //                                            {
            //                                                bSt2B.Bt2CusFbisOutBundle = false;
            //                                                bSt2B.Bt2CusFbTranDUsage = Math.Round(Convert.ToDecimal(bSt2B.Bt2CusFbBilIncre * Cus_dataRateper1MBSP), 2, MidpointRounding.AwayFromZero);
            //                                            }

            //                                            bSt2B.Bt2CusFbBillUnitType = "mb";

            //                                            db.BSt2B.Add(bSt2B);
            //                                        }
            //                                    }

            //                                    lstCTB = new List<BCtB>();
            //                                    lstCTB = db.BCtBs.Where(t => t.BCtFbIMSIId.Equals(Imsi) && t.BCtFbTranVServType != null &&
            //                                                            t.BCtFbTranVServType.ToLower().Equals("voice  to fixed")).ToList();
            //                                    if (lstCTB != null)
            //                                    {
            //                                        foreach (var record in lstCTB)
            //                                        {
            //                                            bSt2B = new BSt2B();
            //                                            bSt2B.Bt2CusFbIMSIId = record.BCtFbIMSIId;
            //                                            bSt2B.Bt2CusFbMSIDNNo = record.BCtFbMSIDNNo;
            //                                            bSt2B.Bt2CusFbSimIdNo = record.BCtFbSimIdNo;
            //                                            //
            //                                            bSt2B.Bt2CusFbTranVServType = record.BCtFbTranVServType;

            //                                            bSt2B.Bt2CusFbTranVStartDate = record.BCtFbTranVStartDate;

            //                                            bSt2B.Bt2CusFbTranVStartTime = record.BCtFbTranVStartTime;
            //                                            bSt2B.Bt2CusFbTranVEndTime = record.BCtFbTranVEndTime;

            //                                            bSt2B.Bt2CusFbTranVOrig = record.BCtFbTranVOrig;
            //                                            bSt2B.Bt2CusFbTranVDest = record.BCtFbTranVDest;

            //                                            double VceconverttoMin = Convert.ToDouble(record.BCtFbVceBillIncre);
            //                                            bSt2B.Bt2CusFbVcebillincre = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(VceconverttoMin)),
            //                                                                                                2, MidpointRounding.AwayFromZero);
            //                                            bSt2B.Bt2CusFbTranVUsage = (decimal)Math.Round(Convert.ToDecimal(bSt2B.Bt2CusFbVcebillincre * Cus_VoicetoFixedSP), 2, MidpointRounding.AwayFromZero);
            //                                            bSt2B.Bt2CusFbVcebillingUnittype = "min";

            //                                            db.BSt2B.Add(bSt2B);
            //                                        }
            //                                    }

            //                                    lstCTB = new List<BCtB>();
            //                                    lstCTB = db.BCtBs.Where(t => t.BCtFbIMSIId.Equals(Imsi) && t.BCtFbTranVServType != null &&
            //                                                           t.BCtFbTranVServType.ToLower().Equals("voice  to cellular")).ToList();
            //                                    if (lstCTB != null)
            //                                    {
            //                                        foreach (var record in lstCTB)
            //                                        {
            //                                            bSt2B = new BSt2B();
            //                                            bSt2B.Bt2CusFbIMSIId = record.BCtFbIMSIId;
            //                                            bSt2B.Bt2CusFbMSIDNNo = record.BCtFbMSIDNNo;
            //                                            bSt2B.Bt2CusFbSimIdNo = record.BCtFbSimIdNo;
            //                                            //
            //                                            bSt2B.Bt2CusFbTranVServType = record.BCtFbTranVServType;

            //                                            bSt2B.Bt2CusFbTranVStartDate = record.BCtFbTranVStartDate;

            //                                            bSt2B.Bt2CusFbTranVStartTime = record.BCtFbTranVStartTime;
            //                                            bSt2B.Bt2CusFbTranVEndTime = record.BCtFbTranVEndTime;

            //                                            bSt2B.Bt2CusFbTranVOrig = record.BCtFbTranVOrig;
            //                                            bSt2B.Bt2CusFbTranVDest = record.BCtFbTranVDest;

            //                                            double VceconverttoMin = Convert.ToDouble(record.BCtFbVceBillIncre);

            //                                            bSt2B.Bt2CusFbVcebillincre = (decimal)Math.Round(Converters.ConvertSecondsToMinutes(VceconverttoMin), 2, MidpointRounding.AwayFromZero);
            //                                            bSt2B.Bt2CusFbTranVUsage = Math.Round(Convert.ToDecimal(bSt2B.Bt2CusFbVcebillincre * Cus_VoicetoCellSP), 2, MidpointRounding.AwayFromZero);
            //                                            bSt2B.Bt2CusFbVcebillingUnittype = "min";

            //                                            db.BSt2B.Add(bSt2B);
            //                                        }
            //                                    }

            //                                    lstCTB = new List<BCtB>();
            //                                    lstCTB = db.BCtBs.Where(t => t.BCtFbIMSIId.Equals(Imsi) && t.BCtFbTranVServType != null &&
            //                                                            t.BCtFbTranVServType.ToLower().Equals("voice  to fleetbroadband")).ToList();
            //                                    if (lstCTB != null)
            //                                    {
            //                                        foreach (var record in lstCTB)
            //                                        {
            //                                            bSt2B = new BSt2B();
            //                                            bSt2B.Bt2CusFbIMSIId = record.BCtFbIMSIId;
            //                                            bSt2B.Bt2CusFbMSIDNNo = record.BCtFbMSIDNNo;
            //                                            bSt2B.Bt2CusFbSimIdNo = record.BCtFbSimIdNo;
            //                                            //
            //                                            bSt2B.Bt2CusFbTranVServType = record.BCtFbTranVServType;

            //                                            bSt2B.Bt2CusFbTranVStartDate = record.BCtFbTranVStartDate;

            //                                            bSt2B.Bt2CusFbTranVStartTime = record.BCtFbTranVStartTime;
            //                                            bSt2B.Bt2CusFbTranVEndTime = record.BCtFbTranVEndTime;

            //                                            bSt2B.Bt2CusFbTranVOrig = record.BCtFbTranVOrig;
            //                                            bSt2B.Bt2CusFbTranVDest = record.BCtFbTranVDest;

            //                                            double VceconverttoMin = Convert.ToDouble(record.BCtFbVceBillIncre);

            //                                            bSt2B.Bt2CusFbVcebillincre = (decimal)Math.Round(Converters.ConvertSecondsToMinutes(VceconverttoMin), 2, MidpointRounding.AwayFromZero);
            //                                            bSt2B.Bt2CusFbTranVUsage = Math.Round(Convert.ToDecimal(bSt2B.Bt2CusFbVcebillincre * Cus_VoicetoFbbSP), 2, MidpointRounding.AwayFromZero);
            //                                            bSt2B.Bt2CusFbVcebillingUnittype = "min";

            //                                            db.BSt2B.Add(bSt2B);
            //                                        }
            //                                    }

            //                                    lstCTB = new List<BCtB>();
            //                                    lstCTB = db.BCtBs.Where(t => t.BCtFbIMSIId.Equals(Imsi) && t.BCtFbTranVServType != null &&
            //                                                            t.BCtFbTranVServType.ToLower().Equals("voice  to iridium")).ToList();
            //                                    if (lstCTB != null)
            //                                    {
            //                                        foreach (var record in lstCTB)
            //                                        {
            //                                            bSt2B = new BSt2B();
            //                                            bSt2B.Bt2CusFbIMSIId = record.BCtFbIMSIId;
            //                                            bSt2B.Bt2CusFbMSIDNNo = record.BCtFbMSIDNNo;
            //                                            bSt2B.Bt2CusFbSimIdNo = record.BCtFbSimIdNo;
            //                                            //
            //                                            bSt2B.Bt2CusFbTranVServType = record.BCtFbTranVServType;

            //                                            bSt2B.Bt2CusFbTranVStartDate = record.BCtFbTranVStartDate;

            //                                            bSt2B.Bt2CusFbTranVStartTime = record.BCtFbTranVStartTime;
            //                                            bSt2B.Bt2CusFbTranVEndTime = record.BCtFbTranVEndTime;

            //                                            bSt2B.Bt2CusFbTranVOrig = record.BCtFbTranVOrig;
            //                                            bSt2B.Bt2CusFbTranVDest = record.BCtFbTranVDest;

            //                                            double VceconverttoMin = Convert.ToDouble(record.BCtFbVceBillIncre);
            //                                            bSt2B.Bt2CusFbVcebillincre = (decimal)Math.Round(Convert.ToDecimal(Converters.ConvertSecondsToMinutes(VceconverttoMin)),
            //                                                                                                2, MidpointRounding.AwayFromZero);
            //                                            bSt2B.Bt2CusFbTranVUsage = (decimal)Math.Round(Convert.ToDecimal(bSt2B.Bt2CusFbVcebillincre * Cus_VoicetoFixedSP), 2, MidpointRounding.AwayFromZero);
            //                                            bSt2B.Bt2CusFbVcebillingUnittype = "min";

            //                                            db.BSt2B.Add(bSt2B);
            //                                        }
            //                                    }
            //                                }
            //                                ErrorMsg.AppendFormat("The IMSI: {0} Save successfully to Invoice Table. <br />", Imsi).AppendLine();
            //                            }
            //                            else
            //                            {
            //                                bst1A = new Bst1A();

            //                                string ErrorMessage = ErrorMsg.ToString();
            //                                char[] delimiters = new char[] { '\r', '\n' };
            //                                string[] text = ErrorMessage.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            //                                string NewOp = string.Empty;
            //                                string resultString = string.Empty;

            //                                for (int i = 0; i < text.Length; i++)
            //                                {
            //                                    string Output = text[i];

            //                                    if (Output.ToLower().Trim().Contains("not"))
            //                                    {
            //                                        NewOp = Output;

            //                                        String[] splitdata = NewOp.Split(new string[] { ":", "" }, StringSplitOptions.RemoveEmptyEntries);

            //                                        resultString = System.Text.RegularExpressions.Regex.Match(splitdata[1], @"\d+").Value;

            //                                        resultString = splitdata[1].Replace(resultString, string.Empty);
            //                                    }
            //                                    else
            //                                        NewOp = null;
            //                                }

            //                                string FinalSplit = resultString;

            //                                bst1A.Bt1LesFbCDRRef = FinalSplit;


            //                                if (!string.IsNullOrEmpty(InvoiceNo))
            //                                {
            //                                    bst1A.Bt1LesFbIMSIId = Imsi;
            //                                    bst1A.Bt1LesFbMSIDNNo = CT_SubscriptionFee.BCtFbMSIDNNo;
            //                                    bst1A.Bt1LesFbSimIdNo = CT_SubscriptionFee.BCtFbSimIdNo;
            //                                    bst1A.Bt1LesFbStartDate = cdrStartDate;
            //                                    bst1A.Bt1LespFbEndDate = cdrEndDate;
            //                                    bst1A.Bt1LesFbServType = CT_SubscriptionFee.BCtFbTranDServType;
            //                                    bst1A.Bt1LesFbCurBill = CT_SubscriptionFee.BCtFbRecurFee;

            //                                    db.Bst1A.Add(bst1A);

            //                                    bst1B = new Bst1B();
            //                                    bst1B.Bt1LesFbIMSIId = CT_SubscriptionFee.BCtFbIMSIId;
            //                                    bst1B.Bt1LesFbMSIDNNo = CT_SubscriptionFee.BCtFbMSIDNNo;
            //                                    bst1B.Bt1LesFbSimIdNo = CT_SubscriptionFee.BCtFbSimIdNo;
            //                                    //
            //                                    bst1B.Bt1LesFbTranDServType = CT_SubscriptionFee.BCtFbTranDServType;

            //                                    bst1B.Bt1LesFbTranDStartDate = cdrStartDate;

            //                                    bst1B.Bt1LesFbTranDStartTime = CT_SubscriptionFee.BCtFbTranDStartTime;
            //                                    bst1B.Bt1LesFbTranDEndTime = CT_SubscriptionFee.BCtFbTranDEndTime;

            //                                    bst1B.Bt1LesFbTranDOrig = CT_SubscriptionFee.BCtFbTranDOrig;
            //                                    bst1B.Bt1LesFbTranDDest = CT_SubscriptionFee.BCtFbTranDDest;

            //                                    bst1B.Bt1LesFbBilIncre = Convert.ToInt16(CT_SubscriptionFee.BCtFbBilIncre);
            //                                    bst1B.Bt1LesFbTranDUsage = CT_SubscriptionFee.BCtFbTranDUsage;
            //                                    bst1B.Bt1LesFbBillUnitType = CT_SubscriptionFee.BCtFbunitType;

            //                                    db.Bst1B.Add(bst1B);

            //                                    List<BCtB> lstCTB = db.BCtBs.Where(t => t.BCtFbIMSIId.Equals(Imsi) && t.BCtFbTranDServType != null &&
            //                                                                        t.BCtFbTranDServType.ToLower().Equals("standard ip")).ToList();
            //                                    if (lstCTB != null)
            //                                    {
            //                                        foreach (var record in lstCTB)
            //                                        {
            //                                            bst1B = new Bst1B();
            //                                            bst1B.Bt1LesFbIMSIId = record.BCtFbIMSIId;
            //                                            bst1B.Bt1LesFbMSIDNNo = record.BCtFbMSIDNNo;
            //                                            bst1B.Bt1LesFbSimIdNo = record.BCtFbSimIdNo;
            //                                            //
            //                                            bst1B.Bt1LesFbTranDServType = record.BCtFbTranDServType;

            //                                            bst1B.Bt1LesFbTranDStartDate = record.BCtFbTranDStartDate;

            //                                            bst1B.Bt1LesFbTranDStartTime = record.BCtFbTranDStartTime;
            //                                            bst1B.Bt1LesFbTranDEndTime = record.BCtFbTranDEndTime;

            //                                            bst1B.Bt1LesFbTranDOrig = record.BCtFbTranDOrig;
            //                                            bst1B.Bt1LesFbTranDDest = record.BCtFbTranDDest;

            //                                            bst1B.Bt1LesFbBilIncre = Convert.ToInt16(record.BCtFbBilIncre);
            //                                            bst1B.Bt1LesFbTranDUsage = record.BCtFbTranDUsage;
            //                                            bst1B.Bt1LesFbBillUnitType = record.BCtFbunitType;

            //                                            db.Bst1B.Add(bst1B);
            //                                        }
            //                                    }

            //                                    lstCTB = new List<BCtB>();
            //                                    lstCTB = db.BCtBs.Where(t => t.BCtFbIMSIId.Equals(Imsi) && t.BCtFbTranVServType != null &&
            //                                                            t.BCtFbTranVServType.ToLower().Equals("voice  to fixed")).ToList();
            //                                    if (lstCTB != null)
            //                                    {
            //                                        foreach (var record in lstCTB)
            //                                        {
            //                                            bst1B = new Bst1B();
            //                                            bst1B.Bt1LesFbIMSIId = record.BCtFbIMSIId;
            //                                            bst1B.Bt1LesFbMSIDNNo = record.BCtFbMSIDNNo;
            //                                            bst1B.Bt1LesFbSimIdNo = record.BCtFbSimIdNo;
            //                                            //
            //                                            bst1B.Bt1LesFbTranVServType = record.BCtFbTranVServType;

            //                                            bst1B.Bt1LesFbTranVStartDate = record.BCtFbTranVStartDate;

            //                                            bst1B.Bt1LesFbTranVStartTime = record.BCtFbTranVStartTime;
            //                                            bst1B.Bt1LesFbTranVEndTime = record.BCtFbTranVEndTime;

            //                                            bst1B.Bt1LesFbTranVOrig = record.BCtFbTranVOrig;
            //                                            bst1B.Bt1LesFbTranVDest = record.BCtFbTranVDest;

            //                                            //bst1B.Bt1LesFbTranVUsage = Convert.ToString(record.BCtFbVceBillIncre);
            //                                            bst1B.Bt1LesFbTranVUsage = record.BCtFbTranVUsage;
            //                                            bst1B.Bt1LesFbVcebillincre = Convert.ToInt32(record.BCtFbVceBillIncre);
            //                                            bst1B.Bt1LesFbVce2Fixed = record.BCFbCurBill;
            //                                            bst1B.Bt1LesFbVceBillUnitType = record.BCtFbVceUnityTtype;

            //                                            db.Bst1B.Add(bst1B);
            //                                        }
            //                                    }

            //                                    lstCTB = new List<BCtB>();
            //                                    lstCTB = db.BCtBs.Where(t => t.BCtFbIMSIId.Equals(Imsi) && t.BCtFbTranVServType != null &&
            //                                                                    t.BCtFbTranVServType.ToLower().Equals("voice  to cellular")).ToList();
            //                                    if (lstCTB != null)
            //                                    {
            //                                        foreach (var record in lstCTB)
            //                                        {
            //                                            bst1B = new Bst1B();
            //                                            bst1B.Bt1LesFbIMSIId = record.BCtFbIMSIId;
            //                                            bst1B.Bt1LesFbMSIDNNo = record.BCtFbMSIDNNo;
            //                                            bst1B.Bt1LesFbSimIdNo = record.BCtFbSimIdNo;
            //                                            //
            //                                            bst1B.Bt1LesFbTranVServType = record.BCtFbTranVServType;

            //                                            bst1B.Bt1LesFbTranVStartDate = record.BCtFbTranVStartDate;

            //                                            bst1B.Bt1LesFbTranVStartTime = record.BCtFbTranVStartTime;
            //                                            bst1B.Bt1LesFbTranVEndTime = record.BCtFbTranVEndTime;

            //                                            bst1B.Bt1LesFbTranVOrig = record.BCtFbTranVOrig;
            //                                            bst1B.Bt1LesFbTranVDest = record.BCtFbTranVDest;

            //                                            bst1B.Bt1LesFbTranVUsage = record.BCtFbTranVUsage;
            //                                            bst1B.Bt1LesFbVcebillincre = Convert.ToInt32(record.BCtFbVceBillIncre);
            //                                            //bst1B.Bt1LesFbVce2Cell = record.BCFbCurBill;
            //                                            bst1B.Bt1LesFbVceBillUnitType = record.BCtFbVceUnityTtype;

            //                                            db.Bst1B.Add(bst1B);
            //                                        }
            //                                    }

            //                                    lstCTB = new List<BCtB>();
            //                                    lstCTB = db.BCtBs.Where(t => t.BCtFbIMSIId.Equals(Imsi) && t.BCtFbTranVServType != null &&
            //                                                                t.BCtFbTranVServType.ToLower().Equals("voice  to fleetbroadband")).ToList();
            //                                    if (lstCTB != null)
            //                                    {
            //                                        foreach (var record in lstCTB)
            //                                        {
            //                                            bst1B = new Bst1B();
            //                                            bst1B.Bt1LesFbIMSIId = record.BCtFbIMSIId;
            //                                            bst1B.Bt1LesFbMSIDNNo = record.BCtFbMSIDNNo;
            //                                            bst1B.Bt1LesFbSimIdNo = record.BCtFbSimIdNo;
            //                                            //
            //                                            bst1B.Bt1LesFbTranVServType = record.BCtFbTranVServType;

            //                                            bst1B.Bt1LesFbTranVStartDate = record.BCtFbTranVStartDate;

            //                                            bst1B.Bt1LesFbTranVStartTime = record.BCtFbTranVStartTime;
            //                                            bst1B.Bt1LesFbTranVEndTime = record.BCtFbTranVEndTime;

            //                                            bst1B.Bt1LesFbTranVOrig = record.BCtFbTranVOrig;
            //                                            bst1B.Bt1LesFbTranVDest = record.BCtFbTranVDest;

            //                                            bst1B.Bt1LesFbTranVUsage = record.BCtFbTranVUsage;
            //                                            bst1B.Bt1LesFbVcebillincre = Convert.ToInt32(record.BCtFbVceBillIncre);
            //                                            //bst1B.Bt1LesFbVce2Fb = record.BCFbCurBill;
            //                                            bst1B.Bt1LesFbVceBillUnitType = record.BCtFbVceUnityTtype;

            //                                            db.Bst1B.Add(bst1B);
            //                                        }
            //                                    }

            //                                    lstCTB = new List<BCtB>();
            //                                    lstCTB = db.BCtBs.Where(t => t.BCtFbIMSIId.Equals(Imsi) && t.BCtFbTranVServType != null &&
            //                                                            t.BCtFbTranVServType.ToLower().Equals("voice  to iridium")).ToList();
            //                                    if (lstCTB != null)
            //                                    {
            //                                        foreach (var record in lstCTB)
            //                                        {
            //                                            bst1B = new Bst1B();
            //                                            bst1B.Bt1LesFbIMSIId = record.BCtFbIMSIId;
            //                                            bst1B.Bt1LesFbMSIDNNo = record.BCtFbMSIDNNo;
            //                                            bst1B.Bt1LesFbSimIdNo = record.BCtFbSimIdNo;
            //                                            //
            //                                            bst1B.Bt1LesFbTranVServType = record.BCtFbTranVServType;

            //                                            bst1B.Bt1LesFbTranVStartDate = record.BCtFbTranVStartDate;

            //                                            bst1B.Bt1LesFbTranVStartTime = record.BCtFbTranVStartTime;
            //                                            bst1B.Bt1LesFbTranVEndTime = record.BCtFbTranVEndTime;

            //                                            bst1B.Bt1LesFbTranVOrig = record.BCtFbTranVOrig;
            //                                            bst1B.Bt1LesFbTranVDest = record.BCtFbTranVDest;

            //                                            //bst1B.Bt1LesFbTranVUsage = Convert.ToString(record.BCtFbVceBillIncre);
            //                                            bst1B.Bt1LesFbTranVUsage = record.BCtFbTranVUsage;
            //                                            bst1B.Bt1LesFbVcebillincre = Convert.ToInt32(record.BCtFbVceBillIncre);
            //                                            bst1B.Bt1LesFbVce2Fixed = record.BCFbCurBill;
            //                                            bst1B.Bt1LesFbVceBillUnitType = record.BCtFbVceUnityTtype;

            //                                            db.Bst1B.Add(bst1B);
            //                                        }
            //                                    }
            //                                }
            //                                ErrorMsg.AppendFormat("The IMSI: {0} Saved successfully to Rejection Table. <br />", Imsi).AppendLine();
            //                            }

            //                            try
            //                            {
            //                                db.SaveChanges();
            //                            }
            //                            catch (DbEntityValidationException e)
            //                            {
            //                                string errormsg = string.Empty;
            //                                foreach (var eve in e.EntityValidationErrors)
            //                                {
            //                                    errormsg = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors: ", eve.Entry.Entity.GetType().Name, eve.Entry.State);
            //                                    foreach (var ve in eve.ValidationErrors)
            //                                    {
            //                                        errormsg = string.Format(" - Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
            //                                    }
            //                                }
            //                                throw;
            //                            }
            //                            catch (Exception error)
            //                            {
            //                                ErrorMsg.AppendFormat("MSI:{0} Error Msg: {1} Stack Trace: {2}. <br />", Imsi, error.Message.ToString(), error.StackTrace);
            //                            }
            //                        }
            //                        else
            //                            ErrorMsg.AppendFormat("The IMSI: {0} data rate is empty in common CDR. <br />", Imsi).AppendLine();
            //                    }
            //                    else
            //                        ErrorMsg.AppendFormat("The IMSI: {0} data rate is empty in LES Registeration CDR. <br />", Imsi).AppendLine();
            //                }
            //                else
            //                {
            //                    ErrorMsg.AppendFormat("The IMSI: {0} is Not into the LES Registeration CDR. <br />", Imsi).AppendLine();
            //                }
            //            }
            //            catch (Exception error)
            //            {
            //                ErrorMsg.AppendFormat("MSI:{0} Error Msg: {1} Stack Trace: {2}. <br />", Imsi, error.Message.ToString(), error.StackTrace);
            //            }
            //        }
            //        else
            //        {
            //            ErrorMsg.Append("The LES Registeration CDR is Empty. <br />").AppendLine();
            //        }
            //    }
            return "";// ErrorMsg.ToString();
        }

        public void MoveRejection_Invoice(int primaryID)
        {
        //    try
        //    {
        //        using (BillingSystemEntities context = new BillingSystemEntities())
        //        {
        //            Bst1A bst1A = db.Bst1A.FirstOrDefault(t => t.Bst1A_ID.Equals(primaryID));
        //            BSt2B bSt2B;
        //            string imsiID = string.Empty;
        //            List<string> SDescription = new List<string>();
        //            Dictionary<string, decimal?> dictServiceDescription = new Dictionary<string, decimal?>();

        //            // Customer prices from cusRegisteration
        //            decimal? CUS_RecuFee = 0;
        //            decimal? dataRateper1MB_SP = 0;
        //            decimal? VoicetoFixed_SP = 0;
        //            decimal? VoicetoCell_SP = 0;
        //            decimal? VoicetoFbb_SP = 0;
        //            decimal? VoicetoIridium = 0;
        //            decimal? VoicetoGpsp_SP = 0;
        //            decimal? VoicetoVceMail_SP = 0;
        //            decimal? VoicetoOthers_SP = 0;
        //            decimal? VoicetoBillIncr_SP = 0;
        //            decimal? Cus_OutBundleDataRateSP = 0;
        //            decimal? Cus_DataPlanSP = 0;

        //            if (bst1A != null)
        //            {
        //                imsiID = bst1A.Bt1LesFbIMSIId;
        //                BSt2A bSt2A = new BSt2A();
        //                string InvoiceNo = string.Empty;

        //                try
        //                {
        //                    InvoiceNo = context.BSt2A.Max(t => t.InvoiceNo);

        //                    if (!string.IsNullOrEmpty(InvoiceNo))
        //                    {
        //                        if (InvoiceNo.Contains("SM"))
        //                        {
        //                            string tempInv = InvoiceNo.Replace("SM", string.Empty);
        //                            if (int.TryParse(tempInv, out int result))
        //                            {
        //                                InvoiceNo = BillingHelpers.GenInvoice(false, result);
        //                            }
        //                        }
        //                    }
        //                    else
        //                        InvoiceNo = BillingHelpers.GenInvoice(false, 0);
        //                }
        //                catch (Exception error)
        //                {
        //                    //ErrorMsg.AppendFormat("MSI:{0} Error Msg: {1} Stack Trace: {2}. <br />", Imsi, error.Message.ToString(), error.StackTrace);
        //                }

        //                bSt2A.InvoiceNo = InvoiceNo;

        //                BPtFbbCusReg cusRegSP = null;
        //                try
        //                {
        //                    cusRegSP = db.BPtFbbCusRegs.FirstOrDefault(t => t.BPtCusFBimSingle.Equals(imsiID));
        //                }
        //                catch
        //                {
        //                    // ErrorMsg.AppendFormat("The IMSI: {0} not registerd in Customer Registeration Table.", Imsi).AppendLine();
        //                }

        //                if (cusRegSP != null)
        //                {
        //                    CUS_RecuFee = cusRegSP.BPtCusFbRecurFee;
        //                    dataRateper1MB_SP = cusRegSP.BPtCusFbDataRate;
        //                    dataRateper1MB_SP = Math.Round(Convert.ToDecimal(dataRateper1MB_SP), 2, MidpointRounding.AwayFromZero);
        //                    VoicetoFixed_SP = cusRegSP.BPtCusFbVce2Fixed;
        //                    VoicetoCell_SP = cusRegSP.BPtCusFbVce2Cell;
        //                    VoicetoFbb_SP = cusRegSP.BPtCusFbVce2Fb;
        //                    VoicetoIridium = cusRegSP.BPtCusFb2Iridium;
        //                    VoicetoGpsp_SP = cusRegSP.BPtCusFbVce2Gpsp;
        //                    VoicetoVceMail_SP = cusRegSP.BPtCusFbVce2VceMail;
        //                    VoicetoOthers_SP = cusRegSP.BPtCusFbVce2Others;
        //                    VoicetoBillIncr_SP = cusRegSP.BPtCusFbVcebillincre;
        //                    Cus_OutBundleDataRateSP = cusRegSP.BPtCusFbOutBundleDataRate;
        //                    if (!string.IsNullOrEmpty(cusRegSP.BPtCusFbDataPlanIn))
        //                        Cus_DataPlanSP = Convert.ToDecimal(cusRegSP.BPtCusFbDataPlanIn);
        //                }

        //                bSt2A.Bt2CusFbIMSIId = bst1A.Bt1LesFbIMSIId;
        //                bSt2A.Bt2CusFbMSIDNNo = bst1A.Bt1LesFbMSIDNNo;
        //                bSt2A.Bt2CusFbSimIdNo = bst1A.Bt1LesFbSimIdNo;
        //                bSt2A.Bt2CusFbServType = bst1A.Bt1LesFbServType;
        //                bSt2A.Bt2CusFbStartDate = bst1A.Bt1LesFbStartDate;
        //                bSt2A.Bt2CuspFbEndDate = bst1A.Bt1LespFbEndDate;
        //                bSt2A.Bt2CusFbRecurFee = CUS_RecuFee;
        //                bSt2A.Bt2CusFbCusName = db.BPtFbbCusRegs.FirstOrDefault(t => t.BPtCusFBimSingle.Equals(bst1A.Bt1LesFbIMSIId)).BPtCusName;
        //                bSt2A.Bt2CusFbCustomerAddress = db.BPtFbbCusRegs.FirstOrDefault(t => t.BPtCusFBimSingle.Equals(bst1A.Bt1LesFbIMSIId)).BPtCusAddrSt;
        //                // Add to Secondary records
        //                db.BSt2A.Add(bSt2A);
        //                //Remove the rejection data from Secondary table 1
        //                db.Bst1A.Remove(bst1A);

        //                List<Bst1B> lstbst1Bs = db.Bst1B.Where(t => t.Bt1LesFbIMSIId.Equals(imsiID) && t.Bt1LesFbTranDServType != null).Distinct().ToList();

        //                decimal dataOutBundle = 0;
        //                foreach (Bst1B record in lstbst1Bs)
        //                {
        //                    if (record != null)
        //                    {
        //                        bSt2B = new BSt2B();
        //                        bSt2B.Bt2CusFbIMSIId = record.Bt1LesFbIMSIId;
        //                        bSt2B.Bt2CusFbMSIDNNo = record.Bt1LesFbMSIDNNo;
        //                        bSt2B.Bt2CusFbSimIdNo = record.Bt1LesFbSimIdNo;
        //                        //
        //                        bSt2B.Bt2CusFbTranDServType = record.Bt1LesFbTranDServType;

        //                        bSt2B.Bt2CusFbTranDStartDate = record.Bt1LesFbTranDStartDate;

        //                        bSt2B.Bt2CusFbTranDStartTime = record.Bt1LesFbTranDStartTime;
        //                        bSt2B.Bt2CusFbTranDEndTime = record.Bt1LesFbTranDEndTime;

        //                        bSt2B.Bt2CusFbTranDOrig = record.Bt1LesFbTranDOrig;
        //                        bSt2B.Bt2CusFbTranDDest = record.Bt1LesFbTranDDest;

        //                        if (record.Bt1LesFbTranDServType.ToLower().Equals("subscription fee"))
        //                        {
        //                            bSt2B.Bt2CusFbRecurFee = CUS_RecuFee;
        //                            bSt2B.Bt2CusFbBillUnitType = "days";
        //                        }
        //                        else
        //                        {
        //                            long dataconvertToMP = Convert.ToInt64(record.Bt1LesFbBilIncre);

        //                            bSt2B.Bt2CusFbBilIncre = (decimal)Math.Round(Converters.ConvertKilobytesToMegabytes(dataconvertToMP), 2, MidpointRounding.AwayFromZero);
        //                            dataOutBundle += Convert.ToDecimal(bSt2B.Bt2CusFbBilIncre);

        //                            if (Cus_DataPlanSP < dataOutBundle && Cus_DataPlanSP > 0)
        //                            {
        //                                bSt2B.Bt2CusFbisOutBundle = true;
        //                                bSt2B.Bt2CusFbTranDUsage = Math.Round(Convert.ToDecimal(bSt2B.Bt2CusFbBilIncre * Cus_OutBundleDataRateSP), 2, MidpointRounding.AwayFromZero);
        //                            }
        //                            else
        //                            {
        //                                bSt2B.Bt2CusFbisOutBundle = false;
        //                                bSt2B.Bt2CusFbTranDUsage = Math.Round(Convert.ToDecimal(bSt2B.Bt2CusFbBilIncre * dataRateper1MB_SP), 2, MidpointRounding.AwayFromZero);
        //                            }
        //                            bSt2B.Bt2CusFbBillUnitType = "mb";
        //                        }
        //                        db.BSt2B.Add(bSt2B);
        //                    }
        //                }

        //                lstbst1Bs = new List<Bst1B>();
        //                lstbst1Bs = db.Bst1B.Where(t => t.Bt1LesFbIMSIId.Equals(imsiID) && t.Bt1LesFbTranVServType != null).Distinct().ToList();

        //                foreach (Bst1B record in lstbst1Bs)
        //                {
        //                    bSt2B = new BSt2B();
        //                    bSt2B.Bt2CusFbIMSIId = record.Bt1LesFbIMSIId;
        //                    bSt2B.Bt2CusFbMSIDNNo = record.Bt1LesFbMSIDNNo;
        //                    bSt2B.Bt2CusFbSimIdNo = record.Bt1LesFbSimIdNo;
        //                    //
        //                    bSt2B.Bt2CusFbTranVServType = record.Bt1LesFbTranVServType;

        //                    bSt2B.Bt2CusFbTranVStartDate = record.Bt1LesFbTranVStartDate;

        //                    bSt2B.Bt2CusFbTranVStartTime = record.Bt1LesFbTranVStartTime;
        //                    bSt2B.Bt2CusFbTranVEndTime = record.Bt1LesFbTranVEndTime;

        //                    bSt2B.Bt2CusFbTranVOrig = record.Bt1LesFbTranVOrig;
        //                    bSt2B.Bt2CusFbTranVDest = record.Bt1LesFbTranVDest;

        //                    double VceconverttoMin = Convert.ToDouble(record.Bt1LesFbVcebillincre);
        //                    bSt2B.Bt2CusFbVcebillincre = Math.Round((decimal)Converters.ConvertSecondsToMinutes(VceconverttoMin), 2, MidpointRounding.AwayFromZero);

        //                    if (record.Bt1LesFbTranVServType.ToLower().Contains("fixed"))
        //                        bSt2B.Bt2CusFbTranVUsage = Math.Round((Convert.ToDecimal(bSt2B.Bt2CusFbVcebillincre * VoicetoFixed_SP)), 2, MidpointRounding.AwayFromZero);
        //                    else if (record.Bt1LesFbTranVServType.ToLower().Contains("cellular"))
        //                        bSt2B.Bt2CusFbTranVUsage = Math.Round(Convert.ToDecimal((bSt2B.Bt2CusFbVcebillincre * VoicetoCell_SP)), 2, MidpointRounding.AwayFromZero);
        //                    else if (record.Bt1LesFbTranVServType.ToLower().Contains("fleetbroadband") || record.Bt1LesFbTranVServType.ToLower().Contains("fbb"))
        //                        bSt2B.Bt2CusFbTranVUsage = Math.Round((Convert.ToDecimal(bSt2B.Bt2CusFbVcebillincre * VoicetoFbb_SP)), 2, MidpointRounding.AwayFromZero);
        //                    else if (record.Bt1LesFbTranVServType.ToLower().Contains("iridium") || record.Bt1LesFbTranVServType.ToLower().Contains("fbb"))
        //                        bSt2B.Bt2CusFbTranVUsage = Math.Round((Convert.ToDecimal(bSt2B.Bt2CusFbVcebillincre * VoicetoIridium)), 2, MidpointRounding.AwayFromZero);

        //                    bSt2B.Bt2CusFbVcebillingUnittype = "min";

        //                    db.BSt2B.Add(bSt2B);
        //                }
        //                try
        //                {
        //                    db.SaveChanges();
        //                }
        //                catch (Exception rrr)
        //                {

        //                }
        //            }
        //        }
        //    }
        //    catch (Exception error)
        //    {

        //    }
        }

        private DateTime ContractStartDate;

        public void CusRegExcelRead(string fileName, string source)
        {
            try
            {
                using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fs, false))
                    {
                        WorkbookPart workbookPart = doc.WorkbookPart;
                        SharedStringTablePart sstpart = workbookPart.GetPartsOfType<SharedStringTablePart>().First();
                        SharedStringTable sst = sstpart.SharedStringTable;

                        WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                        Worksheet sheet = worksheetPart.Worksheet;

                        SheetData sheetData = sheet.GetFirstChild<SheetData>();
                        IEnumerable<Row> rows = sheetData.Descendants<Row>();

                        BillingSystemEntities context = new BillingSystemEntities();

                        BCtA bCtA = new BCtA();
                        BCtB bCtB = new BCtB();
                        BCtC bCtC = new BCtC();
                        BCtD bCtD = new BCtD();

                        List<string> receiptField = new List<string>();

                        string ColName = string.Empty;

                        string serviceType = string.Empty;

                        foreach (Cell cell in rows.ElementAt(0))
                        {
                            ColName = GetCellValue(doc, cell);
                            if (ColName.StartsWith(" "))
                                ColName = ColName.Substring(1);
                            receiptField.Add(System.Text.RegularExpressions.Regex.Replace(ColName, @"\t|\n|\r", ""));
                        }

                        if (!receiptField.Contains("Service Type"))
                        {
                            throw (new Exception("The Input file is not valid, please select valid CusReg File."));
                        }

                        BPtFbbCusReg cusregbill = new BPtFbbCusReg();

                        BsPtLesReg lesregbill = new BsPtLesReg();
                        BsPtLesReg bLesReg = new BsPtLesReg();

                        string colValue = string.Empty;
                        string Id = string.Empty;
                        string InvoiceFormat = string.Empty;

                        using (var billentities = new BillingSystemEntities())
                        {
                            foreach (Row row in rows)
                            {
                                if (row.RowIndex != 1)
                                {
                                    cusregbill = new BPtFbbCusReg();

                                    for (int i = 0, arrIndex = 0; i < receiptField.Count; i++, arrIndex++)
                                    {
                                        colValue = GetCellValue(doc, row.Descendants<Cell>().ElementAt(i));

                                        if (source.Equals("CusReg") && receiptField[arrIndex].ToLower().Equals("id"))
                                            Id = colValue;
                                        else if (receiptField[arrIndex].ToLower().Equals("imsi no"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    cusregbill.BPtCusFBimSingle = colValue;
                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusFBimSingle = null;
                                                }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("msdn no"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    cusregbill.BPtCusFBimDual = colValue;
                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusFBimDual = null;
                                                }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("sim id"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    cusregbill.BPtFbDIBSimId = colValue;
                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtFbDIBSimId = null;
                                                }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("customer name"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    cusregbill.BPtCusName = colValue;
                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusName = null;
                                                }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("customer address"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    cusregbill.BPtCusAddrSt = colValue;
                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusAddrSt = null;
                                                }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("contract start date"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double date = Convert.ToDouble(colValue);



                                                    // bCtA.BCtFbContStart = DateTime.FromOADate(date);
                                                    cusregbill.BPtCusFbContStart = DateTime.FromOADate(date);
                                                    //bSt2A.Bt2CusFbContStart = DateTime.FromOADate(date);
                                                    //bSt1A.Bt1LesFbContStart = DateTime.FromOADate(date);

                                                    // if (bCtA.BCtFbContStart > cusregbill.BPtCusFbContStart)
                                                    //   bSt2A.Bt2CusFbContStart = DateTime.FromOADate(date);
                                                    //else
                                                    //  bSt1A.Bt1LesFbContStart = DateTime.FromOADate(date);

                                                    //lesregbill.BPtLesRgnFbContStart = DateTime.FromOADate(date);
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                                cusregbill.BPtCusFbContStart = null;
                                            //bSt2A.Bt2CusFbContStart = null;
                                            //bSt1A.Bt1LesFbContStart = null;
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("contract end date"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double date = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFbContEnd = DateTime.FromOADate(date);
                                                    //bSt2A.Bt2CdrFbContEnd = DateTime.FromOADate(date);
                                                    //bSt1A.Bt1CdrFbContEnd = DateTime.FromOADate(date);
                                                    // lesregbill.BPtLesRgnFbContEnd = DateTime.FromOADate(date);
                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                                cusregbill.BPtCusFbContEnd = null;
                                            //bSt2A.Bt2CdrFbContEnd = null;
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("start date"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double date = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFBStartDate = DateTime.FromOADate(date);
                                                    //  bSt2A.Bt2CusFbStartDate = DateTime.FromOADate(date);
                                                    //bSt1A.Bt1LesFbStartDate = DateTime.FromOADate(date);
                                                    // if (bCtB.BCtFbTranDStartDate > cusregbill.BPtCusFBStartDate)
                                                    //   bSt2A.Bt2CusFbStartDate = DateTime.FromOADate(date);
                                                    //else
                                                    //   bSt1A.Bt1LesFbStartDate = DateTime.FromOADate(date);

                                                }
                                                catch (Exception ex) { }
                                            }
                                            else
                                                cusregbill.BPtCusFBStartDate = null;
                                            // bSt2A.Bt2CusFbStartDate = null;
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("end date"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double date = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCuspFbEndDate = DateTime.FromOADate(date);
                                                    //bSt2A.Bt2CuspFbEndDate = DateTime.FromOADate(date);
                                                    //bSt1A.Bt1LespFbEndDate = DateTime.FromOADate(date);
                                                    //  if (bCtB.BCtFbTranDStartDate > cusregbill.BPtCuspFbEndDate)
                                                    //    bSt2A.Bt2CuspFbEndDate = DateTime.FromOADate(date);
                                                    //else
                                                    //  bSt1A.Bt1LespFbEndDate = DateTime.FromOADate(date);

                                                }
                                                catch (Exception ex)
                                                { }

                                            }
                                            else
                                                cusregbill.BPtCuspFbEndDate = null;
                                            //bSt2A.Bt2CuspFbEndDate = null;
                                            //bSt1A.Bt1LespFbEndDate = null;
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("service type"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    if (colValue.ToLower().Trim().Equals("standard ip"))
                                                        cusregbill.BPtCusFbTranDServType = colValue;
                                                    else if (colValue.ToLower().Trim().Equals("subscription fee"))
                                                        cusregbill.BPtCusFbTranDServType = colValue;
                                                    else if (colValue.ToLower().Trim().Contains("voice"))
                                                        cusregbill.BPtCusFbTranVServType = colValue;
                                                    else if (colValue.ToLower().Trim().Equals("streaming"))
                                                        cusregbill.BPtCusFbTranStServType = colValue;

                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusFbTranDServType = null;
                                                    cusregbill.BPtCusFbTranVServType = null;
                                                }
                                            }
                                            else
                                            { }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("time"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    // TimeSpan time = TimeSpan.Parse(colValue);
                                                    TimeSpan t = DateTime.FromOADate(Convert.ToDouble(colValue)).TimeOfDay;


                                                    if (!string.IsNullOrEmpty(colValue))
                                                    {
                                                        try
                                                        {
                                                            if (cusregbill.BPtCusFbTranDServType != null && cusregbill.BPtCusFbTranDServType.ToLower().Trim().Equals("standard ip"))
                                                                cusregbill.BPtCusFbTranDStartTime = t;
                                                            else if (cusregbill.BPtCusFbTranDServType != null && cusregbill.BPtCusFbTranDServType.ToLower().Trim().Equals("subscription fee"))
                                                                cusregbill.BPtCusFbTranDStartTime = t;
                                                            else if (cusregbill.BPtCusFbTranVServType != null && cusregbill.BPtCusFbTranVServType.ToLower().Trim().Contains("voice"))
                                                                cusregbill.BPtCusFbTranVStartTime = t;
                                                            else if (cusregbill.BPtCusFbTranStServType != null && cusregbill.BPtCusFbTranStServType.ToLower().Trim().Equals("streaming"))
                                                                cusregbill.BPtCusFbTranStStartTime = t;

                                                        }
                                                        catch (Exception ex)
                                                        {

                                                        }
                                                    }

                                                    //lesregbill.BPtLesRgnFbTranVStartTime = t;
                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusFbTranDStartTime = null;
                                                    cusregbill.BPtCusFbTranStStartTime = null;
                                                    cusregbill.BPtCusFbTranVStartTime = null;
                                                }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("dataplan"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    //double d = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFbDataPlanIn = colValue;

                                                    //if (bCtB.BCtFbDataRate == cusregbill.BPtCusFbDataRate)
                                                    //  bSt2B.Bt2CusFbDataRate = Convert.ToDecimal(d);
                                                    //else
                                                    //  bSt1B.Bt1LesFbDataRate = Convert.ToDecimal(d);
                                                }
                                                catch (Exception ex)
                                                {

                                                }
                                            }

                                            else
                                                cusregbill.BPtCusFbDataPlanIn = null;
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("datarate"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFbDataRate = Convert.ToDecimal(colValue);

                                                    //if (bCtB.BCtFbDataRate == cusregbill.BPtCusFbDataRate)
                                                    //  bSt2B.Bt2CusFbDataRate = Convert.ToDecimal(d);
                                                    //else
                                                    //  bSt1B.Bt1LesFbDataRate = Convert.ToDecimal(d);
                                                }
                                                catch (Exception ex)
                                                {

                                                }
                                            }

                                            else
                                                cusregbill.BPtCusFbDataRate = null;
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("outbundle datarate"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFbOutBundleDataRate = Convert.ToDecimal(colValue);
                                                }
                                                catch (Exception ex)
                                                {

                                                }
                                            }
                                            else
                                                cusregbill.BPtCusFbOutBundleDataRate = 0;
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("recurfee"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {

                                                cusregbill.BPtCusFbRecurFee = Convert.ToDecimal(colValue);
                                                // bSt2B.Bt2CusFbRecurFee = Convert.ToDecimal(colValue);
                                                //bSt1B.Bt1LesFbRecurFee = Convert.ToDecimal(colValue);
                                            }
                                            else
                                                cusregbill.BPtCusFbRecurFee = null;
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("voice2fixed"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFbVce2Fixed = Convert.ToDecimal(colValue);

                                                    // if (bCtB.BCtFbVce2Fixed == cusregbill.BPtCusFbVce2Fixed)
                                                    //   bSt2B.Bt2CusFbVce2Fixed = Convert.ToDecimal(d);
                                                    //else
                                                    //  bSt1B.Bt1LesFbVce2Fixed = Convert.ToDecimal(d);

                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusFbVce2Fixed = null;
                                                }
                                            }


                                            //cusregbill.BPtCusFbVce2Fixed = null;
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("voice2cell"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFbVce2Cell = Convert.ToDecimal(colValue);


                                                    //  if (bCtB.BCtFbVce2Cell == cusregbill.BPtCusFbVce2Cell)
                                                    //    bSt2B.Bt2CusFbVce2Cell = Convert.ToDecimal(d);
                                                    //else
                                                    //  bSt1B.Bt1LesFbVce2Cell = Convert.ToDecimal(d);
                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusFbVce2Cell = null;
                                                }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("voice2bgan"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFbVc2Bgan = Convert.ToDecimal(colValue);

                                                    //  if (bCtB.BCtFbVc2Bgan == cusregbill.BPtCusFbVc2Bgan)
                                                    //    bSt2B.Bt2CusFbVc2Bgan = Convert.ToDecimal(d);
                                                    //else
                                                    //  bSt1B.Bt1LesFbVc2Bgan = Convert.ToDecimal(d);
                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusFbVc2Bgan = null;
                                                }
                                            }


                                            // cusregbill.BPtCusFbVc2Bgan = null;
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("voice2fb"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFbVce2Fb = Convert.ToDecimal(colValue);

                                                    //  if (bCtB.BCtFbVce2Fb == cusregbill.BPtCusFb2FB)
                                                    //    bSt2B.Bt2CusFbVce2Fb = Convert.ToDecimal(d);
                                                    //else
                                                    //  bSt1B.Bt1LesFbVce2Fb = Convert.ToDecimal(d);


                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusFbVce2Fb = null;
                                                }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("voice2gpsp"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFbVce2Gpsp = Convert.ToDecimal(colValue);


                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusFbVce2Gpsp = null;
                                                }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("voice2vcemail"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFbVce2VceMail = Convert.ToDecimal(colValue);
                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusFbVce2VceMail = null;
                                                }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("voice2iridium"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double rate = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFb2Iridium = Convert.ToDecimal(rate);
                                                }
                                                catch { cusregbill.BPtCusFb2Iridium = null; }
                                            }
                                            else
                                                cusregbill.BPtCusFb2Iridium = null;
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("voice2others"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFbVce2Others = Convert.ToDecimal(colValue);

                                                    // if (bCtB.BCtFbVce2Others == cusregbill.BPtCusFbVce2Others)
                                                    //   bSt2B.Bt2CusFbVce2Others = Convert.ToDecimal(d);
                                                    //else
                                                    //  bSt1B.Bt1LesFbVce2Others = Convert.ToDecimal(d);
                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusFbVce2Others = null;
                                                }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("stream 8kbps"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFb8kbps = Convert.ToDecimal(colValue);
                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusFb8kbps = null;
                                                }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("stream 16kbps"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFb16kbps = Convert.ToDecimal(colValue);
                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusFb16kbps = null;
                                                }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("stream 32kbps"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFb32kbps = Convert.ToDecimal(colValue);
                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusFb32kbps = null;
                                                }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("stream 64kbps"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFb64kbps = Convert.ToDecimal(colValue);
                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusFb64kbps = null;
                                                }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("stream 128kbps"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFb128kbps = Convert.ToDecimal(colValue);
                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusFb128kbps = null;
                                                }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("stream 256kbps"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFb256kbps = Convert.ToDecimal(colValue);
                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusFb256kbps = null;
                                                }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("sms"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFBSms = Convert.ToDecimal(colValue);

                                                    //  if (bCtC.BCFBms == cusregbill.BPtCusFBSms)
                                                    //    bSt2C.Bt2CusFBms = Convert.ToDecimal(d);
                                                    //else
                                                    //  bSt1C.Bt1LesFBms = Convert.ToDecimal(d);
                                                }
                                                catch (Exception ex)
                                                {

                                                }
                                            }
                                            else
                                                cusregbill.BPtCusFBSms = null;
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("curbill"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    cusregbill.BPtCusFbCurBill = Convert.ToDecimal(colValue);
                                                }
                                                catch (Exception ex)
                                                {
                                                    cusregbill.BPtCusFbCurBill = null;
                                                }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("bar date"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double date = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFbBarDate = DateTime.FromOADate(date);

                                                    //if (bCtB.BCtFbTranDStartDate < cusregbill.BPtCusFbBarDate)
                                                    //  bSt2D.Bt2CusFbBarDate = DateTime.FromOADate(date);
                                                    //else
                                                    //  bSt1D.Bt1LesFbBarDate = DateTime.FromOADate(date);

                                                }
                                                catch (Exception ex) { cusregbill.BPtCusFbBarDate = null; }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("bardate lifted"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double date = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFbBarDateLifted = DateTime.FromOADate(date);
                                                }
                                                catch (Exception ex) { cusregbill.BPtCusFbBarDateLifted = null; }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("suspension date"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double date = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFBSusDate = DateTime.FromOADate(date);

                                                    //if (bCtB.BCtFbTranDStartDate < cusregbill.BPtCusFBSusDate)
                                                    //  bSt2D.Bt2CusFbSusDate = DateTime.FromOADate(date);
                                                    //else
                                                    //  bSt1D.Bt1LesFbSusDate = DateTime.FromOADate(date);
                                                }
                                                catch (Exception ex) { cusregbill.BPtCusFBSusDate = null; }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("suspensiondate lifted"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double date = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFBusDateLifted = DateTime.FromOADate(date);
                                                }
                                                catch (Exception ex) { cusregbill.BPtCusFBusDateLifted = null; }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("layup date"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double date = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFblayUpDate = DateTime.FromOADate(date);

                                                    //if (bCtB.BCtFbTranDStartDate < cusregbill.BPtCusFblayUpDate)
                                                    //  bSt2D.Bt2CusFblayUpDate = DateTime.FromOADate(date);
                                                    //else
                                                    //  bSt1D.Bt1LesFblayUpDate = DateTime.FromOADate(date);
                                                }
                                                catch (Exception ex) { cusregbill.BPtCusFblayUpDate = null; }
                                            }
                                        }
                                        else if (receiptField[arrIndex].ToLower().Equals("layupdate lifted"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double date = Convert.ToDouble(colValue);
                                                    cusregbill.BPtCusFbLayUpDateLifted = DateTime.FromOADate(date);
                                                }
                                                catch (Exception ex)
                                                { cusregbill.BPtCusFbLayUpDateLifted = null; }
                                            }
                                        }
                                    }
                                    context.BPtFbbCusRegs.Add(cusregbill);
                                    context.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string LesRegExcelRead(string fileName, string source)
        {
            try
            {
                using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fs, false))
                    {
                        WorkbookPart workbookPart = doc.WorkbookPart;
                        SharedStringTablePart sstpart = workbookPart.GetPartsOfType<SharedStringTablePart>().First();
                        SharedStringTable sst = sstpart.SharedStringTable;

                        WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                        Worksheet sheet = worksheetPart.Worksheet;

                        SheetData sheetData = sheet.GetFirstChild<SheetData>();
                        IEnumerable<Row> rows = sheetData.Descendants<Row>();

                        List<string> lesregColumn = new List<string>();

                        string ColName = string.Empty;
                        string ColValue = string.Empty;
                        string serviceType = string.Empty;


                        foreach (Cell cell in rows.ElementAt(0))
                        {
                            ColName = GetCellValue(doc, cell);
                            if (ColName.StartsWith(" "))
                                ColName = ColName.Substring(1);
                            lesregColumn.Add(System.Text.RegularExpressions.Regex.Replace(ColName, @"\t|\n|\r", ""));
                        }

                        if (!lesregColumn.Contains("Service Type"))
                        {
                            throw (new Exception("The Input file is not valid, please select valid LesReg File."));
                        }

                        //Create Object and Context
                        BillingSystemEntities context = new BillingSystemEntities();
                        BsPtLesReg lesregbill = new BsPtLesReg();
                        List<Dictionary<string, dynamic>> lstDictLes = new List<Dictionary<string, dynamic>>();

                        string colValue = string.Empty;

                        string Id = string.Empty;

                        using (var billentities = new BillingSystemEntities())
                        {
                            foreach (Row row in rows)
                            {
                                if (row.RowIndex != 1)
                                {
                                    lesregbill = new BsPtLesReg();

                                    Dictionary<string, dynamic> dictLesReg = new Dictionary<string, dynamic>();

                                    for (int i = 0, arrIndex = 0; i < lesregColumn.Count; i++, arrIndex++)
                                    {
                                        colValue = GetCellValue(doc, row.Descendants<Cell>().ElementAt(i));

                                        if (lesregColumn[arrIndex].ToLower().Equals("id"))
                                        {
                                            try
                                            {
                                                // lesregbill.BPtLesRgnFbDataMSIDNId = colValue;
                                                Id = colValue;
                                                dictLesReg.Add("ID", colValue);
                                            }
                                            catch (Exception ex)
                                            {
                                                //lesregbill.BPtLesRgnFbDataMSIDNId = null;
                                                Id = null;
                                            }
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Contains("imsi no"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    lesregbill.BPtLesRgnFBimSingle = colValue;
                                                    dictLesReg.Add("IMSINO", colValue);
                                                }

                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFBimSingle = null;
                                                }
                                            }
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Contains("msdn no"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    lesregbill.BPtLesRgnFBimDual = colValue;
                                                    dictLesReg.Add("MSDNNo", colValue);
                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFBimDual = null;
                                                }
                                            }
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("sim id"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    lesregbill.BPtLesRgnFbDataSimardId = colValue;
                                                    dictLesReg.Add("SIMID", colValue);
                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFbDataSimardId = null;
                                                }
                                            }
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("contract start date"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double date = Convert.ToDouble(colValue);
                                                    dictLesReg.Add("ContractStartDate", DateTime.FromOADate(date));
                                                    lesregbill.BPtLesRgnFbContStart = DateTime.FromOADate(date);
                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFbContStart = null;
                                                }
                                            }
                                            else
                                                lesregbill.BPtLesRgnFbContStart = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("contract end date"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double date = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFbContEnd = DateTime.FromOADate(date);
                                                    dictLesReg.Add("ContractEndDate", DateTime.FromOADate(date));
                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFbContEnd = null;
                                                }
                                            }
                                            else
                                                lesregbill.BPtLesRgnFbContEnd = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("start date"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double date = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFBtartDate = DateTime.FromOADate(date);

                                                    dictLesReg.Add("StartDate", DateTime.FromOADate(date));
                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFBtartDate = null;
                                                }
                                            }
                                            else
                                                lesregbill.BPtLesRgnFBtartDate = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("end date"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double date = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnpFbEndDate = DateTime.FromOADate(date);

                                                    dictLesReg.Add("EndDate", DateTime.FromOADate(date));
                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnpFbEndDate = null;
                                                }

                                            }
                                            else
                                                lesregbill.BPtLesRgnpFbEndDate = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("service type"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    if (colValue.ToLower().Trim().Equals("standard ip"))
                                                        lesregbill.BPtLesRgnFbTranDServType = colValue;
                                                    else if (colValue.ToLower().Trim().Equals("subscription fee"))
                                                        lesregbill.BPtLesRgnFbTranDServType = colValue;
                                                    else if (colValue.ToLower().Trim().Contains("voice"))
                                                        lesregbill.BPtLesRgnFbTranVServType = colValue;
                                                    else if (colValue.ToLower().Trim().Equals("streaming"))
                                                        lesregbill.BPtLesRgnFbTranStServType = colValue;
                                                    dictLesReg.Add("ServiceType", colValue);
                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFbTranDServType = null;
                                                }
                                            }
                                            else
                                            { }
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("time"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    // TimeSpan time = TimeSpan.Parse(colValue);
                                                    TimeSpan t = DateTime.FromOADate(Convert.ToDouble(colValue)).TimeOfDay;


                                                    if (!string.IsNullOrEmpty(colValue))
                                                    {
                                                        try
                                                        {
                                                            if (lesregbill.BPtLesRgnFbTranDServType != null && lesregbill.BPtLesRgnFbTranDServType.ToLower().Trim().Equals("standard ip"))
                                                                lesregbill.BPtLesRgnFbTranDStartTime = t;
                                                            else if (lesregbill.BPtLesRgnFbTranDServType != null && lesregbill.BPtLesRgnFbTranDServType.ToLower().Trim().Equals("subscription fee"))
                                                                lesregbill.BPtLesRgnFbTranDStartTime = t;
                                                            else if (lesregbill.BPtLesRgnFbTranVServType != null && lesregbill.BPtLesRgnFbTranVServType.ToLower().Trim().Contains("voice"))
                                                                lesregbill.BPtLesRgnFbTranVStartTime = t;
                                                            else if (lesregbill.BPtLesRgnFbTranStServType != null && lesregbill.BPtLesRgnFbTranStServType.ToLower().Trim().Equals("streaming"))
                                                                lesregbill.BPtLesRgnFbTranStStartTime = t;
                                                            dictLesReg.Add("Time", colValue);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            lesregbill.BPtLesRgnFbTranDStartTime = null;
                                                        }
                                                    }

                                                    //lesregbill.BPtLesRgnFbTranVStartTime = t;
                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFbTranVStartDate = null;
                                                    lesregbill.BPtLesRgnFbTranStStartTime = null;
                                                    lesregbill.BPtLesRgnFbTranDStartTime = null;
                                                }
                                            }
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("datarate"))
                                        {

                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFbDataRate = Convert.ToDecimal(d);

                                                    dictLesReg.Add("DataRate", Convert.ToDecimal(d));
                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFbDataRate = null;
                                                }
                                            }
                                            else
                                                lesregbill.BPtLesRgnFbDataRate = null;

                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("recurfee"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                int count = 0;

                                                try
                                                {
                                                    lesregbill.BPtLesRgnFbRecurFee = Convert.ToDecimal(colValue);
                                                    dictLesReg.Add("RecurFee", Convert.ToDecimal(colValue));
                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFbRecurFee = null;
                                                }

                                            }
                                            else
                                                lesregbill.BPtLesRgnFbRecurFee = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("voice2fixed"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFbVce2Fixed = Convert.ToDecimal(colValue);
                                                    dictLesReg.Add("Voice2Fixed", Convert.ToDecimal(colValue));
                                                }
                                                catch (Exception ex)
                                                { lesregbill.BPtLesRgnFbVce2Fixed = null; }
                                            }

                                            else
                                                lesregbill.BPtLesRgnFbVce2Fixed = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("voice2cell"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFbVce2Cell = Convert.ToDecimal(colValue);
                                                    dictLesReg.Add("Voice2Cell", Convert.ToDecimal(colValue));
                                                }
                                                catch (Exception ex)
                                                { lesregbill.BPtLesRgnFbVce2Cell = null; }
                                            }

                                            else
                                                lesregbill.BPtLesRgnFbVce2Cell = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("voice2bgan"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFbVc2Bgan = Convert.ToDecimal(colValue);
                                                    dictLesReg.Add("Voice2Bgan", Convert.ToDecimal(colValue));
                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFbVc2Bgan = null;
                                                }
                                            }

                                            else
                                                lesregbill.BPtLesRgnFbVc2Bgan = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("voice2fb"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFbVce2Fb = Convert.ToDecimal(colValue);
                                                    dictLesReg.Add("Voice2Fb", Convert.ToDecimal(colValue));



                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFbVce2Fb = null;
                                                }
                                            }

                                            else
                                                lesregbill.BPtLesRgnFbVce2Fb = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("voice2gpsp"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFbVce2Gpsp = Convert.ToDecimal(colValue);
                                                    dictLesReg.Add("Voice2Gpsp", Convert.ToDecimal(colValue));



                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFbVce2Gpsp = null;
                                                }
                                            }

                                            else
                                                lesregbill.BPtLesRgnFbVce2Gpsp = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("voice2iridium"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double rate = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFb2Iridium = Convert.ToDecimal(rate);
                                                }
                                                catch { lesregbill.BPtLesRgnFb2Iridium = null; }
                                            }
                                            else
                                                lesregbill.BPtLesRgnFb2Iridium = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("voice2vcemail"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFbVce2VceMail = Convert.ToDecimal(colValue);
                                                    dictLesReg.Add("Voice2VceMail", Convert.ToDecimal(colValue));
                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFbVce2VceMail = null;
                                                }
                                            }
                                            else
                                                lesregbill.BPtLesRgnFbVce2VceMail = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("voice2others"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFbVce2Others = Convert.ToDecimal(colValue);

                                                    dictLesReg.Add("Voice2Others", Convert.ToDecimal(colValue));
                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFbVce2Others = null;
                                                }
                                            }
                                            else
                                                lesregbill.BPtLesRgnFbVce2Others = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("stream 8kbps"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFb8kbps = Convert.ToDecimal(colValue);
                                                    dictLesReg.Add("Stream 8kbps", Convert.ToDecimal(colValue));
                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFb8kbps = null;
                                                }
                                            }
                                            else
                                                lesregbill.BPtLesRgnFb8kbps = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("stream 16kbps"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFb16kbps = Convert.ToDecimal(colValue);
                                                    dictLesReg.Add("Stream 16kbps", Convert.ToDecimal(colValue));

                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFb16kbps = null;
                                                }
                                            }
                                            else
                                                lesregbill.BPtLesRgnFb16kbps = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("stream 32kbps"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFb32kbps = Convert.ToDecimal(colValue);

                                                    dictLesReg.Add("Stream 32kbps", Convert.ToDecimal(colValue));
                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFb32kbps = null;
                                                }
                                            }
                                            else
                                                lesregbill.BPtLesRgnFb32kbps = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("stream 64kbps"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFb64kbps = Convert.ToDecimal(colValue);
                                                    dictLesReg.Add("Stream 64kbps", Convert.ToDecimal(colValue));
                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFb64kbps = null;
                                                }
                                            }
                                            else
                                                lesregbill.BPtLesRgnFb64kbps = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("stream 128kbps"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFb128kbps = Convert.ToDecimal(colValue);
                                                    dictLesReg.Add("Stream 128kbps", Convert.ToDecimal(colValue));
                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFb128kbps = null;
                                                }
                                            }
                                            else
                                                lesregbill.BPtLesRgnFb128kbps = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("stream 256kbps"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFb256kbps = Convert.ToDecimal(colValue);
                                                    dictLesReg.Add("Stream 256kbps", Convert.ToDecimal(colValue));
                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFb256kbps = null;
                                                }
                                            }
                                            else
                                                lesregbill.BPtLesRgnFb256kbps = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("sms"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double d = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFBms = Convert.ToDecimal(colValue);
                                                    dictLesReg.Add("SMS", Convert.ToDecimal(colValue));

                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFBms = null;
                                                }
                                            }

                                            else
                                                lesregbill.BPtLesRgnFBms = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("cur bill"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    lesregbill.BPtLesRgnFbCurBill = Convert.ToDecimal(colValue);

                                                    dictLesReg.Add("CurBill", colValue);
                                                }
                                                catch (Exception ex)
                                                {
                                                    lesregbill.BPtLesRgnFbCurBill = null;
                                                }
                                            }
                                            else
                                                lesregbill.BPtLesRgnFbCurBill = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("bar date"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double date = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFbBarDate = DateTime.FromOADate(date);
                                                    dictLesReg.Add("BarDate", DateTime.FromOADate(date));

                                                }
                                                catch (Exception ex) { lesregbill.BPtLesRgnFbBarDate = null; }
                                            }
                                            else
                                                lesregbill.BPtLesRgnFbBarDate = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("bardate lifted"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double date = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFbBarDateLifted = DateTime.FromOADate(date);
                                                    dictLesReg.Add("BarDate Lifted", DateTime.FromOADate(date));

                                                }
                                                catch (Exception ex) { lesregbill.BPtLesRgnFbBarDateLifted = null; }
                                            }
                                            else
                                                lesregbill.BPtLesRgnFbBarDateLifted = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("suspension date"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double date = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFBusDate = DateTime.FromOADate(date);
                                                    dictLesReg.Add("Suspension Date", DateTime.FromOADate(date));
                                                }
                                                catch (Exception ex) { lesregbill.BPtLesRgnFBusDate = null; }
                                            }
                                            else
                                                lesregbill.BPtLesRgnFBusDate = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("suspensiondate lifted"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double date = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFBusDateLifted = DateTime.FromOADate(date);
                                                    dictLesReg.Add("SuspensionDate Lifted", DateTime.FromOADate(date));
                                                }
                                                catch (Exception ex) { lesregbill.BPtLesRgnFBusDateLifted = null; }
                                            }
                                            else
                                            {
                                                lesregbill.BPtLesRgnFBusDateLifted = null;
                                            }
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("layup date"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double date = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFblayUpDate = DateTime.FromOADate(date);
                                                    dictLesReg.Add("LayupDate", DateTime.FromOADate(date));
                                                }
                                                catch (Exception ex) { lesregbill.BPtLesRgnFblayUpDate = null; }
                                            }
                                            else
                                                lesregbill.BPtLesRgnFblayUpDate = null;
                                        }
                                        else if (lesregColumn[arrIndex].ToLower().Equals("layupdate lifted"))
                                        {
                                            if (!string.IsNullOrEmpty(colValue))
                                            {
                                                try
                                                {
                                                    double dates = Convert.ToDouble(colValue);
                                                    lesregbill.BPtLesRgnFbLayUpDateLifted = DateTime.FromOADate(dates);
                                                    dictLesReg.Add("LayupDate Lifted", DateTime.FromOADate(dates));
                                                }
                                                catch (Exception ex)
                                                { lesregbill.BPtLesRgnFbLayUpDateLifted = null; }
                                            }
                                            else
                                                lesregbill.BPtLesRgnFbLayUpDateLifted = null;
                                        }
                                    }
                                    context.BsPtLesRegs.Add(lesregbill);
                                    lstDictLes.Add(dictLesReg);
                                }
                            }
                        }
                        try
                        {
                            context.SaveChanges();
                        }
                        catch (DbEntityValidationException e)
                        {
                            string errormsg = string.Empty;
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                errormsg = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors: ", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    errormsg = string.Format(" - Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                                }
                            }
                            throw;
                        }
                        return "";
                    }
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string chceckLesToCT(List<Dictionary<string, dynamic>> lstDictLes)
        {
            //BillingSystemEntities db = new BillingSystemEntities();
            //List<BCtB> lstbCtBs = new List<BCtB>();
            //List<BPtFbbCusReg> lstCusReg = new List<BPtFbbCusReg>();
            //lstbCtBs = db.BCtBs.Where(t => t.BCtFbTranDServType.Equals("Subscription Fee")).ToList();

            StringBuilder errmasg = new StringBuilder();

            //foreach (Dictionary<string, dynamic> dictLes in lstDictLes)
            //{
            //    if (dictLes.ContainsKey("IMSINO") && !string.IsNullOrEmpty(dictLes["IMSINO"]))
            //    {
            //        string IMSINO = Convert.ToString(dictLes["IMSINO"]);
            //        BCtB bCtB = lstbCtBs.FirstOrDefault(t => t.BCtFbIMSIId.Equals(IMSINO));

            //        if (bCtB != null)
            //        {
            //            if (dictLes.ContainsKey("RecurFee") && dictLes["RecurFee"] != null)
            //            {
            //                if (bCtB.BCtFbRecurFee.Equals(Convert.ToDecimal(dictLes["RecurFee"])))
            //                {
            //                    BPtFbbCusReg bPtFbbCusReg = new BPtFbbCusReg();

            //                    bPtFbbCusReg.BPtFbDIBMSIDNId = IMSINO;
            //                    bPtFbbCusReg.BPtCusFbRecurFee = Convert.ToDecimal(dictLes["RecurFee"]);

            //                    if (dictLes.ContainsKey("ServiceType") && !string.IsNullOrEmpty(dictLes["ServiceType"]))
            //                    {
            //                        if (Convert.ToString(dictLes["ServiceType"]).ToLower().Equals("standard ip") || Convert.ToString(dictLes["ServiceType"]).ToLower().Equals("subscription fee"))
            //                        {
            //                            bPtFbbCusReg.BPtCusFbTranDServType = dictLes["ServiceType"];

            //                            if (dictLes.ContainsKey("DataRate") && dictLes["DataRate"] != null)
            //                                bPtFbbCusReg.BPtCusFbDataRate = Convert.ToDecimal(dictLes["DataRate"]);
            //                        }
            //                        else if ((Convert.ToString(dictLes["ServiceType"]).ToLower().Contains("voice")))
            //                        {
            //                            bPtFbbCusReg.BPtCusFbTranVServType = dictLes["ServiceType"];

            //                            if (bPtFbbCusReg.BPtCusFbTranVServType.ToLower().Contains("voice  to cellular"))
            //                                bPtFbbCusReg.BPtCusFbVce2Cell = Convert.ToDecimal(dictLes["Voice2Cell"]);
            //                            else if (bPtFbbCusReg.BPtCusFbTranVServType.ToLower().Contains("voice2fixed"))
            //                                bPtFbbCusReg.BPtCusFbVce2Fixed = Convert.ToDecimal(dictLes["Voice2Fixed"]);
            //                            else if (bPtFbbCusReg.BPtCusFbTranVServType.ToLower().Contains("voice2fb"))
            //                                bPtFbbCusReg.BPtCusFbVce2Fb = Convert.ToDecimal(dictLes["Voice2Fb"]);
            //                            else if (bPtFbbCusReg.BPtCusFbTranVServType.ToLower().Contains("voice2vcemail"))
            //                                bPtFbbCusReg.BPtCusFbVce2VceMail = Convert.ToDecimal(dictLes["Voice2VceMail"]);
            //                            else if (bPtFbbCusReg.BPtCusFbTranVServType.ToLower().Contains("voice2gpsp"))
            //                                bPtFbbCusReg.BPtCusFbVce2Gpsp = Convert.ToDecimal(dictLes["Voice2Gpsp"]);
            //                            else if (bPtFbbCusReg.BPtCusFbTranVServType.ToLower().Contains("voice2others"))
            //                                bPtFbbCusReg.BPtCusFbVce2Others = Convert.ToDecimal(dictLes["Voice2Others"]);

            //                        }

            //                        if (dictLes.ContainsKey("StartDate") && dictLes["StartDate"] != null)
            //                            bPtFbbCusReg.BPtCusFBStartDate = Convert.ToDateTime(dictLes["StartDate"]);
            //                        if (dictLes.ContainsKey("EndDate") && dictLes["EndDate"] != null)
            //                            bPtFbbCusReg.BPtCuspFbEndDate = Convert.ToDateTime(dictLes["EndDate"]);
            //                        if (dictLes.ContainsKey("ContractStartDate") && dictLes["ContractStartDate"] != null)
            //                            bPtFbbCusReg.BPtCusFbContStart = Convert.ToDateTime(dictLes["ContractStartDate"]);
            //                        if (dictLes.ContainsKey("ContractEndDate") && dictLes["ContractEndDate"] != null)
            //                            bPtFbbCusReg.BPtCusFbContEnd = Convert.ToDateTime(dictLes["ContractEndDate"]);

            //                    }

            //                    errmasg.Append(string.Format("{0} this IMSID Save successfully to CusReg Table", IMSINO));
            //                    db.BPtFbbCusRegs.Add(bPtFbbCusReg);
            //                    lstCusReg.Add(bPtFbbCusReg);
            //                }
            //            }
            //            else
            //            {
            //                errmasg.Append(string.Format("{0} this IMSID not match with CT Recur Fee", IMSINO));
            //            }

            //            db.SaveChanges();
            //            checkRejectorInvoice(lstDictLes, lstCusReg);
            //        }
            //        else
            //            errmasg.Append(string.Format("{0} this IMSID doesn't contain in the CT table.", IMSINO));
            //    }
            //}
            return errmasg.ToString();
        }

        public static string checkRejectorInvoice(List<Dictionary<string, dynamic>> lstDictLes, List<BPtFbbCusReg> lsrCusReg)
        {
            BillingSystemEntities db = new BillingSystemEntities();
            List<BCtA> lstbCtAs = db.BCtAs.ToList();
            List<BCtB> lstbCtBs = db.BCtBs.ToList();
            Bst1A bst1A = new Bst1A();
            BSt2A bSt2A = new BSt2A();

            foreach (Dictionary<string, dynamic> dictLes in lstDictLes)
            {
                //COndition No : 4

                //if (dictLes.ContainsKey("StartDate"))
                //{
                //    if ((lstbCtBs.FirstOrDefault(t => t.BCtFbIMSIId.Equals("IMSINO")).BCtFbTranDStartDate) > Convert.ToDateTime(dictLes["StartDate"]))
                //    {
                //        bst1A.Bt1LesFbStartDate =   
                //    }
                //    else
                //        bSt2A.Bt2CusFbStartDate = 
                //}

                //End of Condition No : 4

                //Condition No : 6

                if (dictLes.ContainsKey("ContractStartDate"))
                {

                }

                //End of Condition No : 5


            }

            return "";
        }

        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;

            try
            {
                string value = cell.CellValue.InnerXml;

                if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                {
                    return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
                }
                else
                {
                    return value;
                }
            }
            catch { return string.Empty; }


        }

        #region OldCode
        //public static string connectionstring { get; set; }

        //public static bool Rejection(Dictionary<string, string> dictrejection, string source)
        //{

        //    bool isSuccess = false;
        //    try
        //    {


        //        //BillingSystemEntities bse = new BillingSystemEntities();

        //        //BCtA BCta = new BCtA();

        //        //BCtB Bctb = new BCtB();

        //        //BCtC BctC = new BCtC();

        //        //BCtD Bctd = new BCtD();

        //        using (SqlConnection con = new SqlConnection(connectionstring))
        //        {
        //            using (SqlCommand cmd = new SqlCommand())
        //            {
        //                cmd.Connection = con;

        //                if (dictrejection.ContainsKey("ID") && !string.IsNullOrEmpty(dictrejection["ID"]))
        //                    cmd.Parameters.AddWithValue("@ID", dictrejection["ID"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@ID", DBNull.Value);

        //                if (dictrejection.ContainsKey("Contract Start Date") && !string.IsNullOrEmpty(dictrejection["Contract Start Date"]))
        //                    cmd.Parameters.AddWithValue("@ContractStartDate", dictrejection["Contract Start Date"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@ContractStartDate", DBNull.Value);

        //                if (dictrejection.ContainsKey("Contract End Date") && !string.IsNullOrEmpty(dictrejection["Contract End Date"]))
        //                    cmd.Parameters.AddWithValue("@ContractEndDate", DBNull.Value);

        //                if (dictrejection.ContainsKey("Start Date") && !string.IsNullOrEmpty(dictrejection["Start Date"]))
        //                    cmd.Parameters.AddWithValue("@StartDate", dictrejection["Start Date"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@StartDate", DBNull.Value);

        //                if (dictrejection.ContainsKey("End Date") && !string.IsNullOrEmpty(dictrejection["End Date"]))
        //                    cmd.Parameters.AddWithValue("@EndDate", dictrejection["End Date"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@EndDate", DBNull.Value);

        //                if (dictrejection.ContainsKey("Data Rate") && !string.IsNullOrEmpty(dictrejection["Data Rate"]))
        //                    cmd.Parameters.AddWithValue("@DataRate", dictrejection["Data Rate"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@DataRate", DBNull.Value);

        //                if (dictrejection.ContainsKey("Service Type") && !string.IsNullOrEmpty(dictrejection["Service Type"]))
        //                    cmd.Parameters.AddWithValue("@DataPlan", dictrejection["Service Type"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@DataPlan", DBNull.Value);

        //                if (dictrejection.ContainsKey("Units") && !string.IsNullOrEmpty(dictrejection["Units"]))
        //                    cmd.Parameters.AddWithValue("@Units", dictrejection["Units"]);

        //                if (dictrejection.ContainsKey("UnitType") && !string.IsNullOrEmpty(dictrejection["UnitType"]))
        //                    cmd.Parameters.AddWithValue("@UnitType", dictrejection["UnitType"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@UnitType", DBNull.Value);

        //                if (dictrejection.ContainsKey("Voice2Fixed") && !string.IsNullOrEmpty(dictrejection["Voice2Fixed"]))
        //                    cmd.Parameters.AddWithValue("@Voice2Fixed", dictrejection["Voice2Fixed"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@Voice2Fixed", DBNull.Value);

        //                if (dictrejection.ContainsKey("Voice2Cell") && !string.IsNullOrEmpty(dictrejection["Voice2Cell"]))
        //                    cmd.Parameters.AddWithValue("@Voice2Cell", dictrejection["Voice2Cell"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@Voice2Cell", DBNull.Value);


        //                if (dictrejection.ContainsKey("Voice2Bgan") && !string.IsNullOrEmpty(dictrejection["Voice2Bgan"]))
        //                    cmd.Parameters.AddWithValue("@Voice2Bgan", dictrejection["Voice2Bgan"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@Voice2Bgan", DBNull.Value);


        //                if (dictrejection.ContainsKey("Voice2Fb") && !string.IsNullOrEmpty(dictrejection["Voice2Fb"]))
        //                    cmd.Parameters.AddWithValue("@Voice2Fb", dictrejection["Voice2Fb"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@Voice2Fb", DBNull.Value);

        //                if (dictrejection.ContainsKey("Voice2Gpsp") && !string.IsNullOrEmpty(dictrejection["Voice2Gpsp"]))
        //                    cmd.Parameters.AddWithValue("@Voice2Gpsp", dictrejection["Voice2Gpsp"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@Voice2Gpsp", DBNull.Value);

        //                if (dictrejection.ContainsKey("Voice2VceMail") && !string.IsNullOrEmpty(dictrejection["Voice2VceMail"]))
        //                    cmd.Parameters.AddWithValue("@Voice2VceMail", dictrejection["Voice2VceMail"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@Voice2VceMail", DBNull.Value);

        //                if (dictrejection.ContainsKey("Voice2Others") && !string.IsNullOrEmpty(dictrejection["Voice2Others"]))
        //                    cmd.Parameters.AddWithValue("@Voice2Others", dictrejection["Voice2Others"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@Voice2Others", DBNull.Value);

        //                if (dictrejection.ContainsKey("SMS") && !string.IsNullOrEmpty(dictrejection["SMS"]))
        //                    cmd.Parameters.AddWithValue("@SMS", dictrejection["SMS"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@SMS", DBNull.Value);

        //                if (dictrejection.ContainsKey("Bar Date") && !string.IsNullOrEmpty(dictrejection["Bar Date"]))
        //                    cmd.Parameters.AddWithValue("@BarDate", dictrejection["Bar Date"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@BarDate", DBNull.Value);

        //                if (dictrejection.ContainsKey("BarDate Lifted") && !string.IsNullOrEmpty(dictrejection["BarDate Lifted"]))
        //                    cmd.Parameters.AddWithValue("@BarDateLifted", dictrejection["BarDate Lifted"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@BarDateLifted", DBNull.Value);

        //                if (dictrejection.ContainsKey("Suspension Date") && !string.IsNullOrEmpty(dictrejection["Suspension Date"]))
        //                    cmd.Parameters.AddWithValue("@SuspensionDate", dictrejection["Suspension Date"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@SuspensionDate", DBNull.Value);

        //                if (dictrejection.ContainsKey("SuspensionDate Lifted") && !string.IsNullOrEmpty(dictrejection["SuspensionDate Lifted"]))
        //                    cmd.Parameters.AddWithValue("@SuspensionDateLifted", dictrejection["SuspensionDate Lifted"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@SuspensionDateLifted", DBNull.Value);

        //                if (dictrejection.ContainsKey("Layup Date") && !string.IsNullOrEmpty(dictrejection["Layup Date"]))
        //                    cmd.Parameters.AddWithValue("@LayupDate", dictrejection["Layup Date"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@LayupDate", DBNull.Value);

        //                if (dictrejection.ContainsKey("LayupDate Lifted") && !string.IsNullOrEmpty(dictrejection["LayupDate Lifted"]))
        //                    cmd.Parameters.AddWithValue("@LayupDateLifted", dictrejection["LayupDate Lifted"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@LayupDateLifted", DBNull.Value);

        //                if (dictrejection.ContainsKey("InvoiceFormat") && !string.IsNullOrEmpty(dictrejection["InvoiceFormat"]))
        //                    cmd.Parameters.AddWithValue("@InvoiceFormat", dictrejection["InvoiceFormat"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@InvoiceFormat", DBNull.Value);

        //                if (dictrejection.ContainsKey("RecurFee") && !string.IsNullOrEmpty(dictrejection["RecurFee"]))
        //                    cmd.Parameters.AddWithValue("@RecurFee", dictrejection["RecurFee"]);
        //                else
        //                    cmd.Parameters.AddWithValue("@RecurFee", DBNull.Value);

        //                StringBuilder field = new StringBuilder();

        //                if (source.Equals("CusReg"))
        //                {
        //                    field.Append("BPtFbDIBMSIDNId, ");
        //                    field.Append("BPtCusFbContStart, ");
        //                    field.Append("BPtCusFbContEnd, ");
        //                    field.Append("BPtCusFBStartDate, ");
        //                    field.Append("BPtCuspFbEndDate, ");
        //                    field.Append("BPtCusFbDataRate, ");
        //                    field.Append("BPtCusFbTranDServType, ");
        //                    field.Append("BPtCusFbTranDStartDate, ");
        //                    field.Append("BPtCusFbVce2Fixed, ");
        //                    field.Append("BPtCusFbVce2Cell, ");
        //                    field.Append("BPtCusFbVc2Bgan, ");
        //                    field.Append("BPtCusFbVce2Fb, ");
        //                    field.Append("BPtCusFbVce2Gpsp, ");
        //                    field.Append("BPtCusFbVce2VceMail, ");
        //                    field.Append("BPtCusFbVce2Others, ");
        //                    field.Append("BPtCusFBSms, ");
        //                    field.Append("BPtCusFb8kbps, ");
        //                    field.Append("BPtCusFb16kbps, ");
        //                    field.Append("BPtCusFb32kbps, ");
        //                    field.Append("BPtCusFb64kbps, ");
        //                    field.Append("BPtCusFb128kbps, ");
        //                    field.Append("BPtCusFb256kbps, ");


        //                    if (dictrejection["Service Type"].ToLower().Trim().Contains("subscription fee"))
        //                        field.Append("BPtCusFbRecurFee, ");
        //                    else if (dictrejection["Service Type"].ToLower().Trim().Contains("voice"))
        //                        field.Append("BPtCusFbVcebillincre, ");
        //                    else if (dictrejection["Service Type"].ToLower().Trim().Contains("streaming"))
        //                        field.Append("BPtCusFBtBillingUnittype, ");
        //                    // else if(dictrejection["Service Type"].ToLower().Trim().Contains("standard ip") || dictrejection["Service Type"].ToUpper().Equals("ISDN"))

        //                    field.Append("BPtCusFbBarDate, ");
        //                    field.Append("BPtCusFbBarDateLifted, ");
        //                    field.Append("BPtCusFBSusDate, ");
        //                    field.Append("BPtCusFBusDateLifted, ");
        //                    field.Append("BPtCusFblayUpDate, ");
        //                    field.Append("BPtCusFbLayUpDateLifted, ");
        //                    field.Append("BPtCusFbIbvFmt ");

        //                }

        //                else if (source.Equals("LesReg"))
        //                {
        //                    field.Append("BPtLesRgnFbDataMSIDNId, ");
        //                    field.Append("BPtLesRgnFbContStart, ");
        //                    field.Append("BPtLesRgnFbContEnd, ");
        //                    field.Append("BPtLesRgnFBtartDate, ");
        //                    field.Append("BPtLesRgnpFbEndDate, ");
        //                    field.Append("BPtLesRgnFbTranDServType, ");
        //                    field.Append("BPtLesRgnFbTranDStartDate, ");
        //                    field.Append("BPtLesRgnFbVce2Fixed, ");
        //                    field.Append("BPtLesRgnFbVce2Cell, ");
        //                    field.Append("BPtLesRgnFbVc2Bgan, ");
        //                    field.Append("BPtLesRgnFbVce2Fb, ");
        //                    field.Append("BPtLesRgnFbVce2Gpsp, ");
        //                    field.Append("BPtLesRgnFbVce2VceMail, ");
        //                    field.Append("BPtLesRgnFbVce2Others, ");
        //                    field.Append("BPtLesRgnFb8kbps, ");
        //                    field.Append("BPtLesRgnFb16kbps, ");
        //                    field.Append("BPtLesRgnFb32kbps, ");
        //                    field.Append("BPtLesRgnFb64kbps, ");
        //                    field.Append("BPtLesRgnFb128kbps, ");
        //                    field.Append("BPtLesRgnFb256kbps, ");
        //                    field.Append("BPtLesRgnFBms, ");

        //                    if (dictrejection["Service Type"].ToLower().Trim().Contains("subscription fee"))
        //                        field.Append("BPtLesRgnFbRecurFee, ");
        //                    else if (dictrejection["Service Type"].ToLower().Trim().Contains("voice"))
        //                        field.Append("BPtLesRgnFbVcebillincre, ");
        //                    else if (dictrejection["Service Type"].ToLower().Trim().Contains("streaming"))
        //                        field.Append("BPtLesRgnFBtBillingUnittype, ");

        //                    field.Append("BPtLesRgnFbBarDate, ");
        //                    field.Append("BPtLesRgnFbBarDateLifted, ");
        //                    field.Append("BPtLesRgnFBusDate, ");
        //                    field.Append("BPtLesRgnFBusDateLifted, ");
        //                    field.Append("BPtLesRgnFblayUpDate, ");
        //                    field.Append("BPtLesRgnFbLayUpDateLifted, ");
        //                    field.Append("BPtLesRgnFbIbvFmt ");


        //                }

        //                StringBuilder values = new StringBuilder();
        //                values.Append("@ID, ");
        //                values.Append("@ContractStartDate, ");
        //                values.Append("@ContractEndDate, ");
        //                values.Append("@StartDate, ");
        //                values.Append("@EndDate, ");
        //                values.Append("@DataRate, ");
        //                values.Append("@DataPlan, ");
        //                values.Append("@Voice2Fixed, ");
        //                values.Append("@Voice2Cell, ");
        //                values.Append("@Voice2Bgan, ");
        //                values.Append("@Voice2Fb, ");
        //                values.Append("@Voice2Gpsp, ");
        //                values.Append("@Voice2VceMail, ");
        //                values.Append("@Voice2Others, ");
        //                values.Append("@SMS, ");

        //                if (dictrejection["Service Type"].ToLower().Trim().Contains("subscription fee"))
        //                    values.Append("@RecurFee, ");
        //                else
        //                    values.Append("@Units, ");

        //                values.Append("@BarDate, ");
        //                values.Append("@BarDateLifted, ");
        //                values.Append("@SuspensionDate, ");
        //                values.Append("@SuspensionDateLifted, ");
        //                values.Append("@LayupDate, ");
        //                values.Append("@LayupDateLifted, ");
        //                values.Append("@InvoiceFormat ");

        //                if (source.Equals("CusReg"))
        //                    cmd.CommandText = string.Format("INSERT INTO BPtFbbCusReg({0}) VALUES({1})", field.ToString(), values.ToString());
        //                else if (source.Equals("LesReg"))
        //                    cmd.CommandText = string.Format("INSERT INTO BsPtLesReg({0}) VALUES({1})", field.ToString(), values.ToString());

        //                cmd.ExecuteNonQuery();

        //                SqlDataReader rdr = cmd.ExecuteReader();

        //                string ServiceType = string.Empty;

        //                bool isExistCT = false;

        //                #region Customer Registeration

        //                while (rdr.Read())

        //                {
        //                    #region subscription fee
        //                    if (ServiceType.ToLower().Trim().Contains("subscription fee") && dictrejection["Service Type"].ToLower().Trim().Contains("subscription fee"))
        //                    {
        //                        if (source.Equals("CusReg"))
        //                        {
        //                            bool isvalid = false;

        //                            bool isnotValid = true;

        //                            if (rdr.GetString(rdr.GetOrdinal("BCtFbMSIDNNo")).Equals(dictrejection["ID"]))
        //                            {


        //                                if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCtFbStartDate")))) == (Convert.ToDateTime(dictrejection["startdate"])))
        //                                    isvalid = true;
        //                                else
        //                                    isnotValid = false;




        //                                if (dictrejection.ContainsKey("enddate") && !string.IsNullOrEmpty(dictrejection["enddate"]))
        //                                {
        //                                    if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCtpFbEndDate")))) > (Convert.ToDateTime(dictrejection["enddate"])))
        //                                        isvalid = true;
        //                                    else
        //                                        isnotValid = false;
        //                                }





        //                                if (dictrejection.ContainsKey("BarDateLifted") && !string.IsNullOrEmpty(dictrejection["BarDateLifted"]))
        //                                {
        //                                    if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbBarDateLifted")))) < (Convert.ToDateTime(dictrejection["BarDateLifted"])))
        //                                        isvalid = true;
        //                                    else
        //                                        isnotValid = false;
        //                                }

        //                                if (dictrejection.ContainsKey("SuspensionDateLifted") && !string.IsNullOrEmpty(dictrejection["SuspensionDateLifted"]))
        //                                {
        //                                    if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbSusDateLifted")))) < (Convert.ToDateTime(dictrejection["SuspensionDateLifted"])))
        //                                        isvalid = true;
        //                                    else
        //                                        isnotValid = false;
        //                                }

        //                                if (dictrejection.ContainsKey("Lay_LiftedDate") && !string.IsNullOrEmpty(dictrejection["Lay_LiftedDate"]))
        //                                {
        //                                    if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbLayUpDateLifted")))) < (Convert.ToDateTime(dictrejection["Lay_LiftedDate"])))
        //                                        isvalid = true;
        //                                    else
        //                                        isnotValid = false;
        //                                }

        //                                if (dictrejection.ContainsKey("Data Rate") && !string.IsNullOrEmpty(dictrejection["Data Rate"]))
        //                                {
        //                                    if (rdr.GetString(rdr.GetOrdinal("BCtFbDataRate")).Equals(dictrejection["Data Rate"]))
        //                                        isvalid = true;
        //                                    else
        //                                        isnotValid = false;
        //                                }

        //                                if (dictrejection.ContainsKey("RecurFee") && !string.IsNullOrEmpty(dictrejection["RecurFee"]))
        //                                {
        //                                    if (rdr.GetString(rdr.GetOrdinal("BCtFbRecurFee")).Equals(dictrejection["RecurFee"]))
        //                                        isvalid = true;
        //                                    else
        //                                        isnotValid = false;
        //                                }


        //                                if (isvalid && isnotValid)
        //                                {
        //                                    StringBuilder sb = new StringBuilder();
        //                                    sb.Append("Bt2CusFbMSIDNNo, ");
        //                                    sb.Append("Bt2CusFbStartDate, ");
        //                                    sb.Append("Bt2CuspFbEndDate, ");
        //                                    sb.Append("Bt2CusFbBarDateLifted, ");
        //                                    sb.Append("Bt2CusFbSusDateLifted, ");
        //                                    sb.Append("Bt2CusFbLayUpDateLifted, ");
        //                                    sb.Append("Bt2CusFbDataRate, ");
        //                                    sb.Append("Bt2CusFbRecurFee, ");
        //                                    sb.Append("Bt2CusFbDataPlanIn ");



        //                                    StringBuilder secvalues = new StringBuilder();
        //                                    secvalues.Append("@ID, ");
        //                                    secvalues.Append("@StartDate, ");
        //                                    secvalues.Append("@EndDate, ");
        //                                    secvalues.Append("@BarDateLifted, ");
        //                                    secvalues.Append("@SuspensionDateLifted, ");
        //                                    secvalues.Append("@LayupDateLifted, ");
        //                                    secvalues.Append("@DataRate, ");
        //                                    secvalues.Append("@RecurFee, ");
        //                                    secvalues.Append("@DataPlan ");

        //                                    SaveToInvoiceFBT(cmd, sb.ToString(), secvalues.ToString());
        //                                    isExistCT = true;
        //                                    break;
        //                                }

        //                                else
        //                                {
        //                                    StringBuilder sb1 = new StringBuilder();
        //                                    sb1.Append("Bt1LesFbMSIDNNo, ");
        //                                    sb1.Append("Bt1LesFbStartDate, ");
        //                                    sb1.Append("Bt1LespFbEndDate, ");
        //                                    sb1.Append("Bt1LesFbBarDateLifted, ");
        //                                    sb1.Append("Bt1LesFbSusDateLifted, ");
        //                                    sb1.Append("Bt1LesFbLayUpDateLifted, ");
        //                                    sb1.Append("Bt1LesFbDataRate, ");
        //                                    sb1.Append("Bt1LesFbRecurFee, ");
        //                                    sb1.Append("Bt1LesFbDataPlanIn ");

        //                                    StringBuilder sec1values = new StringBuilder();
        //                                    sec1values.Append("@ID, ");
        //                                    sec1values.Append("@StartDate, ");
        //                                    sec1values.Append("@EndDate, ");
        //                                    sec1values.Append("@BarDateLifted, ");
        //                                    sec1values.Append("@LayupDateLifted, ");
        //                                    sec1values.Append("@SuspensionDateLifted, ");
        //                                    sec1values.Append("@DataRate, ");
        //                                    sec1values.Append("@RecurFee, ");
        //                                    sec1values.Append("@DataPlan ");

        //                                    SaveToRejectionFBT(cmd, sb1.ToString(), sec1values.ToString());
        //                                    isExistCT = true;
        //                                    break;
        //                                }



        //                            }
        //                            #endregion
        //                            #endregion
        //                            #region Les registeration
        //                            else if (source.Equals("LesReg"))
        //                            {
        //                                bool isValid = false;
        //                                bool isnotvalid = true;

        //                                if (rdr.GetString(rdr.GetOrdinal("BCtFbMSIDNNo")).Equals(dictrejection["ID"]))
        //                                {

        //                                    if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCtFbStartDate")))) == (Convert.ToDateTime(dictrejection["startdate"])))
        //                                        isvalid = true;
        //                                    else
        //                                        isnotValid = false;




        //                                    if (dictrejection.ContainsKey("enddate") && !string.IsNullOrEmpty(dictrejection["enddate"]))
        //                                    {
        //                                        if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCtpFbEndDate")))) > (Convert.ToDateTime(dictrejection["enddate"])))
        //                                            isvalid = true;
        //                                        else
        //                                            isnotValid = false;
        //                                    }


        //                                    if (dictrejection.ContainsKey("BarDateLifted") && !string.IsNullOrEmpty(dictrejection["BarDateLifted"]))
        //                                    {
        //                                        if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbBarDateLifted")))) < (Convert.ToDateTime(dictrejection["BarDateLifted"])))
        //                                            isvalid = true;
        //                                        else
        //                                            isnotValid = false;
        //                                    }

        //                                    if (dictrejection.ContainsKey("SuspensionDateLifted") && !string.IsNullOrEmpty(dictrejection["SuspensionDateLifted"]))
        //                                    {
        //                                        if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbSusDateLifted")))) < (Convert.ToDateTime(dictrejection["SuspensionDateLifted"])))
        //                                            isvalid = true;
        //                                        else
        //                                            isnotValid = false;
        //                                    }

        //                                    if (dictrejection.ContainsKey("Lay_LiftedDate") && !string.IsNullOrEmpty(dictrejection["Lay_LiftedDate"]))
        //                                    {
        //                                        if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbLayUpDateLifted")))) < (Convert.ToDateTime(dictrejection["Lay_LiftedDate"])))
        //                                            isvalid = true;
        //                                        else
        //                                            isnotValid = false;
        //                                    }

        //                                    if (dictrejection.ContainsKey("RecurFee") && !string.IsNullOrEmpty(dictrejection["RecurFee"]))
        //                                    {
        //                                        if (rdr.GetString(rdr.GetOrdinal("BCtFbRecurFee")).Equals(dictrejection["RecurFee"]))
        //                                            isvalid = true;
        //                                        else
        //                                            isnotValid = false;
        //                                    }

        //                                    if (isvalid && isnotvalid)
        //                                    {
        //                                        StringBuilder secfields = new StringBuilder();
        //                                        secfields.Append("Bt2CusFbMSIDNNo, ");
        //                                        secfields.Append("Bt2CusFbStartDate, ");
        //                                        secfields.Append("Bt2CuspFbEndDate, ");
        //                                        secfields.Append("Bt2CusFbBarDateLifted, ");
        //                                        secfields.Append("Bt2CusFbSusDateLifted, ");
        //                                        secfields.Append("Bt2CusFbLayUpDateLifted, ");
        //                                        secfields.Append("Bt2CusFbDataRate, ");
        //                                        secfields.Append("Bt2CusFbRecurFee, ");
        //                                        secfields.Append("Bt2CusFbDataPlanIn ");





        //                                        StringBuilder sbvalues = new StringBuilder();
        //                                        sbvalues.Append("@ID, ");
        //                                        sbvalues.Append("@StartDate, ");
        //                                        sbvalues.Append("@EndDate, ");
        //                                        sbvalues.Append("@BarDateLifted, ");
        //                                        sbvalues.Append("@SuspensionDateLifted, ");
        //                                        sbvalues.Append("@LayupDateLifted, ");
        //                                        sbvalues.Append("@DataRate, ");
        //                                        sbvalues.Append("@RecurFee, ");
        //                                        sbvalues.Append("@DataPlan ");


        //                                        SaveToInvoiceFBT(cmd, secfields.ToString(), sbvalues.ToString());
        //                                        isExistCT = true;
        //                                        break;
        //                                    }
        //                                    else
        //                                    {
        //                                        StringBuilder sec1fields = new StringBuilder();
        //                                        sec1fields.Append("Bt1LesFbMSIDNNo, ");
        //                                        sec1fields.Append("Bt1LesFbStartDate, ");
        //                                        sec1fields.Append("Bt1LespFbEndDate, ");
        //                                        sec1fields.Append("Bt1LesFbBarDateLifted, ");
        //                                        sec1fields.Append("Bt1LesFbSusDateLifted, ");
        //                                        sec1fields.Append("Bt1LesFbLayUpDateLifted, ");
        //                                        sec1fields.Append("Bt1LesFbDataRate, ");
        //                                        sec1fields.Append("Bt1LesFbRecurFee, ");
        //                                        sec1fields.Append("Bt1LesFbDataPlanIn ");


        //                                        StringBuilder sec1values = new StringBuilder();
        //                                        sec1values.Append("@ID, ");
        //                                        sec1values.Append("@StartDate, ");
        //                                        sec1values.Append("@EndDate, ");
        //                                        sec1values.Append("@BarDateLifted, ");
        //                                        sec1values.Append("@SuspensionDateLifted, ");
        //                                        sec1values.Append("@LayupDateLifted, ");
        //                                        sec1values.Append("@DataRate, ");
        //                                        sec1values.Append("@RecurFee, ");
        //                                        sec1values.Append("@DataPlan ");

        //                                        SaveToRejectionFBT(cmd, sec1fields.ToString(), sec1values.ToString());
        //                                        isExistCT = true;
        //                                        break;
        //                                    }

        //                                }
        //                            }
        //                            #endregion

        //                            #region CusReg Standard IP

        //                            else if (ServiceType.ToLower().Trim().Contains("standard ip") && dictrejection["Service Type"].ToLower().Trim().Contains("standard ip"))
        //                            {
        //                                if (source.Equals("CusReg"))
        //                                {
        //                                    bool Isvalid = false;
        //                                    bool isnotvalid = true;

        //                                    if (rdr.GetString(rdr.GetOrdinal("BCtFbMSIDNNo")).Equals(dictrejection["ID"]))
        //                                    {
        //                                        if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCtFbStartDate")))) == (Convert.ToDateTime(dictrejection["start date"])))
        //                                            Isvalid = true;
        //                                        else
        //                                            isnotvalid = false;

        //                                        if (dictrejection.ContainsKey("enddate") && !string.IsNullOrEmpty(dictrejection["enddate"]))
        //                                        {
        //                                            if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCtpFbEndDate")))) < (Convert.ToDateTime(dictrejection["enddate"])))
        //                                                Isvalid = true;
        //                                            else
        //                                                isnotvalid = false;
        //                                        }

        //                                        if (dictrejection.ContainsKey("BarDateLifted") && !string.IsNullOrEmpty(dictrejection["BarDateLifted"]))
        //                                        {
        //                                            if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbBarDateLifted")))) < (Convert.ToDateTime(dictrejection["BarDateLifted"])))
        //                                                Isvalid = true;
        //                                            else
        //                                                isnotvalid = false;
        //                                        }

        //                                        if (dictrejection.ContainsKey("SuspensionDateLifted") && !string.IsNullOrEmpty(dictrejection["SuspensionDateLifted"]))
        //                                        {
        //                                            if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbSusDateLifted")))) < (Convert.ToDateTime(dictrejection["SuspensionDateLifted"])))
        //                                                Isvalid = true;
        //                                            else
        //                                                isnotvalid = false;
        //                                        }

        //                                        if (dictrejection.ContainsKey("Lay_LiftedDate") && !string.IsNullOrEmpty(dictrejection["Lay_LiftedDate"]))
        //                                        {
        //                                            if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbLayUpDateLifted")))) < (Convert.ToDateTime(dictrejection["Lay_LiftedDate"])))
        //                                                Isvalid = true;
        //                                            else
        //                                                isnotvalid = false;
        //                                        }

        //                                        if (dictrejection.ContainsKey("Units") && !string.IsNullOrEmpty(dictrejection["Units"]))
        //                                        {
        //                                            if ((rdr.GetString(rdr.GetOrdinal("BCtFbBilIncre")).Equals(dictrejection["Units"])))
        //                                                Isvalid = true;
        //                                            else
        //                                                isnotvalid = false;
        //                                        }


        //                                        if (Isvalid && isnotvalid)
        //                                        {
        //                                            StringBuilder secFields = new StringBuilder();
        //                                            secFields.Append("Bt2CusFbMSIDNNo, ");
        //                                            secFields.Append("Bt2CusFbStartDate, ");
        //                                            secFields.Append("Bt2CuspFbEndDate, ");
        //                                            secFields.Append("Bt2CusFbBarDateLifted, ");
        //                                            // secFields.Append("Bt2CusFbSusDateLifted, ");
        //                                            secFields.Append("Bt2CusFbLayUpDateLifted, ");
        //                                            // secFields.Append("Bt2CusFbDataRate, ");
        //                                            //secFields.Append("Bt2CusFbRecurFee, ");
        //                                            secFields.Append("Bt2CusFbDataPlanIn, ");
        //                                            secFields.Append("Bt2CusFbBilIncre ");



        //                                            StringBuilder secValues = new StringBuilder();
        //                                            secValues.Append("@ID, ");
        //                                            secValues.Append("@StartDate, ");
        //                                            secValues.Append("@EndDate, ");
        //                                            secValues.Append("@BarDateLifted, ");
        //                                            secValues.Append("@LayupDateLifted, ");
        //                                            secValues.Append("@Units, ");
        //                                            secValues.Append("@DataPlan ");


        //                                            SaveToInvoiceFBT(cmd, secFields.ToString(), secValues.ToString());
        //                                            isExistCT = true;
        //                                            break;
        //                                        }
        //                                        else
        //                                        {
        //                                            StringBuilder Secfields = new StringBuilder();
        //                                            Secfields.Append("Bt1LesFbMSIDNNo, ");
        //                                            Secfields.Append("Bt1LesFbStartDate, ");
        //                                            Secfields.Append("Bt1LespFbEndDate, ");
        //                                            Secfields.Append("Bt1LesFbBarDateLifted, ");
        //                                            // Secfields.Append("Bt1LesFbSusDateLifted, ");
        //                                            Secfields.Append("Bt1LesFbLayUpDateLifted, ");
        //                                            //Secfields.Append("Bt1LesFbDataRate, ");
        //                                            //Secfields.Append("Bt1LesFbRecurFee, ");
        //                                            Secfields.Append("Bt1LesFbDataPlanIn, ");
        //                                            Secfields.Append("Bt1LesFbBilIncre ");



        //                                            StringBuilder Secvalues = new StringBuilder();
        //                                            Secvalues.Append("@ID, ");
        //                                            Secvalues.Append("@StartDate, ");
        //                                            Secvalues.Append("@EndDate, ");
        //                                            Secvalues.Append("@BarDateLifted, ");
        //                                            Secvalues.Append("@LayupDateLifted, ");
        //                                            Secvalues.Append("@Units, ");
        //                                            Secvalues.Append("@DataPlan ");

        //                                            SaveToRejectionFBT(cmd, Secfields.ToString(), Secvalues.ToString());
        //                                            isExistCT = true;
        //                                            break;
        //                                        }
        //                                    }
        //                                }
        //                            }


        //                            #endregion

        //                            #region LesReg Standard IP

        //                            else if (source.Equals("LesReg"))
        //                            {
        //                                bool Isvalid = false;
        //                                bool isnotvalid = true;

        //                                if (rdr.GetString(rdr.GetOrdinal("BCtFbMSIDNNo")).Equals(dictrejection["ID"]))
        //                                {
        //                                    if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCtFbStartDate")))) == (Convert.ToDateTime(dictrejection["startdate"])))
        //                                        Isvalid = true;
        //                                    else
        //                                        isnotvalid = false;


        //                                    if (dictrejection.ContainsKey("enddate") && !string.IsNullOrEmpty(dictrejection["enddate"]))
        //                                    {
        //                                        if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCtpFbEndDate")))) < (Convert.ToDateTime(dictrejection["enddate"])))
        //                                            Isvalid = true;
        //                                        else
        //                                            isnotvalid = false;
        //                                    }


        //                                    if (dictrejection.ContainsKey("BarDateLifted") && !string.IsNullOrEmpty(dictrejection["BarDateLifted"]))
        //                                    {
        //                                        if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbBarDateLifted")))) < (Convert.ToDateTime(dictrejection["BarDateLifted"])))
        //                                            Isvalid = true;
        //                                        else
        //                                            isnotvalid = false;
        //                                    }

        //                                    if (dictrejection.ContainsKey("SuspensionDateLifted") && !string.IsNullOrEmpty(dictrejection["SuspensionDateLifted"]))
        //                                    {
        //                                        if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbSusDateLifted")))) < (Convert.ToDateTime(dictrejection["SuspensionDateLifted"])))
        //                                            Isvalid = true;
        //                                        else
        //                                            isnotvalid = false;
        //                                    }

        //                                    if (dictrejection.ContainsKey("Lay_LiftedDate") && !string.IsNullOrEmpty(dictrejection["Lay_LiftedDate"]))
        //                                    {
        //                                        if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbLayUpDateLifted")))) < (Convert.ToDateTime(dictrejection["Lay_LiftedDate"])))
        //                                            Isvalid = true;
        //                                        else
        //                                            isnotvalid = false;
        //                                    }

        //                                    if (dictrejection.ContainsKey("Units") && !string.IsNullOrEmpty(dictrejection["Units"]))
        //                                    {
        //                                        if ((rdr.GetString(rdr.GetOrdinal("BCtFbBilIncre")).Equals(dictrejection["Units"])))
        //                                            Isvalid = true;
        //                                        else
        //                                            isnotvalid = false;
        //                                    }

        //                                    if (Isvalid && isnotvalid)
        //                                    {
        //                                        StringBuilder secfield = new StringBuilder();

        //                                        secfield.Append("Bt2CusFbMSIDNNo, ");
        //                                        secfield.Append("Bt2CusFbStartDate, ");
        //                                        secfield.Append("Bt2CuspFbEndDate, ");
        //                                        secfield.Append("Bt2CusFbBarDateLifted, ");
        //                                        // secFields.Append("Bt2CusFbSusDateLifted, ");
        //                                        secfield.Append("Bt2CusFbLayUpDateLifted, ");
        //                                        // secFields.Append("Bt2CusFbDataRate, ");
        //                                        //secFields.Append("Bt2CusFbRecurFee, ");
        //                                        secfield.Append("Bt2CusFbDataPlanIn, ");
        //                                        secfield.Append("Bt2CusFbBilIncre ");



        //                                        StringBuilder secvalue = new StringBuilder();
        //                                        secvalue.Append("@ID, ");
        //                                        secvalue.Append("@StartDate, ");
        //                                        secvalue.Append("@EndDate, ");
        //                                        secvalue.Append("@BarDateLifted, ");
        //                                        secvalue.Append("@LayupDateLifted, ");
        //                                        secvalue.Append("@Units, ");
        //                                        secvalue.Append("@DataPlan ");

        //                                        SaveToInvoiceFBT(cmd, secfield.ToString(), secvalue.ToString());
        //                                        isExistCT = true;
        //                                        break;
        //                                    }
        //                                    else
        //                                    {
        //                                        StringBuilder secfields = new StringBuilder();
        //                                        secfields.Append("Bt1LesFbMSIDNNo, ");
        //                                        secfields.Append("Bt1LesFbStartDate, ");
        //                                        secfields.Append("Bt1LespFbEndDate, ");
        //                                        secfields.Append("Bt1LesFbBarDateLifted, ");
        //                                        // Secfields.Append("Bt1LesFbSusDateLifted, ");
        //                                        secfields.Append("Bt1LesFbLayUpDateLifted, ");
        //                                        //Secfields.Append("Bt1LesFbDataRate, ");
        //                                        //Secfields.Append("Bt1LesFbRecurFee, ");
        //                                        secfields.Append("Bt1LesFbDataPlanIn, ");
        //                                        secfields.Append("Bt1LesFbBilIncre ");



        //                                        StringBuilder secvalue = new StringBuilder();
        //                                        secvalue.Append("@ID, ");
        //                                        secvalue.Append("@StartDate, ");
        //                                        secvalue.Append("@EndDate, ");
        //                                        secvalue.Append("@BarDateLifted, ");
        //                                        secvalue.Append("LayupDateLifted, ");
        //                                        secvalue.Append("@Units, ");
        //                                        secvalue.Append("@DataPlan ");

        //                                        SaveToRejectionFBT(cmd, secfields.ToString(), secvalue.ToString());
        //                                        isExistCT = true;
        //                                        break;


        //                                    }


        //                                }
        //                            }
        //                            #endregion

        //                            #region Customer Reg voice
        //                            else if (ServiceType.ToLower().Trim().Contains("voice") && dictrejection["Service Type"].ToLower().Trim().Contains("voice"))
        //                            {
        //                                #region cus reg voice
        //                                if (source.Equals("CusReg"))
        //                                {
        //                                    bool isValid = false;
        //                                    bool isnotvalid = true;

        //                                    if (rdr.GetString(rdr.GetOrdinal("BCtFbMSIDNNo")).Equals(dictrejection["ID"]))
        //                                    {
        //                                        if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCtFbTranVStartDate")))) == (Convert.ToDateTime(dictrejection["startdate"])))
        //                                            isValid = true;
        //                                        else
        //                                            isnotvalid = false;
        //                                    }

        //                                    if (dictrejection.ContainsKey("enddate") && !string.IsNullOrEmpty(dictrejection["enddate"]))
        //                                    {
        //                                        if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BPtCuspFbEndDate")))) < (Convert.ToDateTime(dictrejection["enddate"])))
        //                                            isValid = true;
        //                                        else
        //                                            isnotvalid = false;

        //                                    }

        //                                    if (dictrejection.ContainsKey("BarDateLifted") && !string.IsNullOrEmpty(dictrejection["BarDateLifted"]))
        //                                    {
        //                                        if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbBarDateLifted")))) < (Convert.ToDateTime(dictrejection["BarDateLifted"])))
        //                                            isValid = true;
        //                                        else
        //                                            isnotvalid = false;
        //                                    }

        //                                    if (dictrejection.ContainsKey("SuspensionDateLifted") && !string.IsNullOrEmpty(dictrejection["SuspensionDateLifted"]))
        //                                    {
        //                                        if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbSusDateLifted")))) < (Convert.ToDateTime(dictrejection["SuspensionDateLifted"])))
        //                                            isvalid = true;
        //                                        else
        //                                            isnotvalid = false;
        //                                    }

        //                                    if (dictrejection.ContainsKey("Lay_LiftedDate") && !string.IsNullOrEmpty(dictrejection["Lay_LiftedDate"]))
        //                                    {
        //                                        if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbLayUpDateLifted")))) < (Convert.ToDateTime(dictrejection["Lay_LiftedDate"])))
        //                                            isvalid = true;
        //                                        else
        //                                            isnotvalid = false;
        //                                    }


        //                                    if (isvalid && isnotvalid)
        //                                    {
        //                                        StringBuilder secFields = new StringBuilder();
        //                                        secFields.Append("Bt2CusFbMSIDNNo, ");
        //                                        secFields.Append("Bt2CusFbStartDate, ");
        //                                        secFields.Append("Bt2CuspFbEndDate, ");
        //                                        secFields.Append("Bt2CusFbBilIncre, ");
        //                                        secFields.Append("Bt2CusFbBillUnitType, ");
        //                                        secFields.Append("Bt2CusFbVce2Fixed, ");
        //                                        secFields.Append("Bt2CusFbVce2Cell, ");
        //                                        secFields.Append("Bt2CusFbVc2Bgan, ");
        //                                        secFields.Append("Bt2CusFbVce2Fb, ");
        //                                        secFields.Append("Bt2CusFbVce2Gpsp, ");
        //                                        secFields.Append("Bt2CusFbVce2VceMail, ");
        //                                        secFields.Append("Bt2CusFbVce2Others, ");
        //                                        secFields.Append("Bt2CusFbDataPlanIn ");


        //                                        StringBuilder secvalues = new StringBuilder();
        //                                        secvalues.Append("@ID, ");
        //                                        secvalues.Append("@StartDate, ");
        //                                        secvalues.Append("@EndDate, ");
        //                                        secvalues.Append("@UnitType, ");
        //                                        secvalues.Append("@UnitType, ");
        //                                        secvalues.Append("@Voice2Fixed, ");
        //                                        secvalues.Append("@Voice2Cell, ");
        //                                        secvalues.Append("@Voice2Bgan, ");
        //                                        secvalues.Append("@Voice2Fb, ");
        //                                        secvalues.Append("@Voice2Gpsp, ");
        //                                        secvalues.Append("@Voice2VceMail, ");
        //                                        secvalues.Append("@Voice2Others, ");
        //                                        secvalues.Append("@DataPlan ");

        //                                        SaveToInvoiceFBT(cmd, secFields.ToString(), secvalues.ToString());
        //                                        isExistCT = true;
        //                                        break;


        //                                    }
        //                                    else
        //                                    {
        //                                        StringBuilder sec1Fields = new StringBuilder();
        //                                        sec1Fields.Append("Bt1LesFbMSIDNNo, ");
        //                                        sec1Fields.Append("Bt1LesFbStartDate, ");
        //                                        sec1Fields.Append("Bt1LespFbEndDate, ");
        //                                        sec1Fields.Append("Bt1LesFbDataPlanIn, ");
        //                                        sec1Fields.Append("Bt1LesFbVce2Fixed, ");
        //                                        sec1Fields.Append("Bt1LesFbVce2Cell, ");
        //                                        sec1Fields.Append("Bt1LesFbVc2Bgan, ");
        //                                        sec1Fields.Append("Bt1LesFbVce2Fb, ");
        //                                        sec1Fields.Append("Bt1LesFbVce2Gpsp, ");
        //                                        sec1Fields.Append("Bt1LesFbVce2VceMail, ");
        //                                        sec1Fields.Append("Bt1LesFbVce2Others ");


        //                                        StringBuilder sec1Values = new StringBuilder();
        //                                        sec1Values.Append("@ID, ");
        //                                        sec1Values.Append("@StartDate, ");
        //                                        sec1Values.Append("@EndDate, ");
        //                                        sec1Values.Append("@DataPlan, ");
        //                                        sec1Values.Append("@Voice2Fixed, ");
        //                                        sec1Values.Append("@Voice2Cell, ");
        //                                        sec1Values.Append("@Voice2Bgan, ");
        //                                        sec1Values.Append("@Voice2Fb, ");
        //                                        sec1Values.Append("@Voice2Gpsp, ");
        //                                        sec1Values.Append("@Voice2VceMail, ");
        //                                        sec1Values.Append("@Voice2Others ");

        //                                        SaveToRejectionFBT(cmd, sec1Fields.ToString(), sec1Values.ToString());
        //                                        isExistCT = true;
        //                                        break;
        //                                    }





        //                                }
        //                            }

        //                            #endregion


        //                            #region Les Reg Voice
        //                            else if (source.Equals("LesReg"))
        //                            {
        //                                bool Isvalid = false;
        //                                bool isnotvalid = true;

        //                                if (rdr.GetString(rdr.GetOrdinal("BCtFbMSIDNNo")).Equals(dictrejection["ID"]))
        //                                {
        //                                    if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCtFbTranVStartDate")))) == (Convert.ToDateTime(dictrejection["startdate"])))
        //                                        Isvalid = true;
        //                                    else
        //                                        isnotvalid = false;
        //                                }

        //                                if (dictrejection.ContainsKey("enddate") && !string.IsNullOrEmpty(dictrejection["enddate"]))
        //                                {
        //                                    if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BPtCuspFbEndDate")))) < (Convert.ToDateTime(dictrejection["enddate"])))
        //                                        Isvalid = true;
        //                                    else
        //                                        isnotvalid = false;

        //                                }

        //                                if (dictrejection.ContainsKey("BarDateLifted") && !string.IsNullOrEmpty(dictrejection["BarDateLifted"]))
        //                                {
        //                                    if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbBarDateLifted")))) < (Convert.ToDateTime(dictrejection["BarDateLifted"])))
        //                                        Isvalid = true;
        //                                    else
        //                                        isnotvalid = false;
        //                                }

        //                                if (dictrejection.ContainsKey("SuspensionDateLifted") && !string.IsNullOrEmpty(dictrejection["SuspensionDateLifted"]))
        //                                {
        //                                    if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbSusDateLifted")))) < (Convert.ToDateTime(dictrejection["SuspensionDateLifted"])))
        //                                        Isvalid = true;
        //                                    else
        //                                        isnotvalid = false;
        //                                }

        //                                if (dictrejection.ContainsKey("Lay_LiftedDate") && !string.IsNullOrEmpty(dictrejection["Lay_LiftedDate"]))
        //                                {
        //                                    if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbLayUpDateLifted")))) < (Convert.ToDateTime(dictrejection["Lay_LiftedDate"])))
        //                                        Isvalid = true;
        //                                    else
        //                                        isnotvalid = false;
        //                                }

        //                                if (Isvalid && isnotvalid)
        //                                {
        //                                    StringBuilder secfields = new StringBuilder();
        //                                    secfields.Append("Bt2CusFbMSIDNNo, ");
        //                                    secfields.Append("Bt2CusFbStartDate, ");
        //                                    secfields.Append("Bt2CuspFbEndDate, ");
        //                                    secfields.Append("Bt2CusFbBilIncre, ");
        //                                    secfields.Append("Bt2CusFbBillUnitType, ");
        //                                    secfields.Append("Bt2CusFbVce2Fixed, ");
        //                                    secfields.Append("Bt2CusFbVce2Cell, ");
        //                                    secfields.Append("Bt2CusFbVc2Bgan, ");
        //                                    secfields.Append("Bt2CusFbVce2Fb, ");
        //                                    secfields.Append("Bt2CusFbVce2Gpsp, ");
        //                                    secfields.Append("Bt2CusFbVce2VceMail, ");
        //                                    secfields.Append("Bt2CusFbVce2Others, ");
        //                                    secfields.Append("Bt2CusFbDataPlanIn ");



        //                                    StringBuilder secValues = new StringBuilder();
        //                                    secValues.Append("@ID, ");
        //                                    secValues.Append("@StartDate, ");
        //                                    secValues.Append("@EndDate, ");
        //                                    secValues.Append("@UnitType, ");
        //                                    secValues.Append("@UnitType, ");
        //                                    secValues.Append("@Voice2Fixed, ");
        //                                    secValues.Append("@Voice2Cell, ");
        //                                    secValues.Append("@Voice2Bgan, ");
        //                                    secValues.Append("@Voice2Fb, ");
        //                                    secValues.Append("@Voice2Gpsp, ");
        //                                    secValues.Append("@Voice2VceMail, ");
        //                                    secValues.Append("@Voice2Others, ");
        //                                    secValues.Append("@DataPlan ");

        //                                    SaveToInvoiceFBT(cmd, secfields.ToString(), secValues.ToString());
        //                                    isExistCT = true;
        //                                    break;
        //                                }

        //                                else
        //                                {
        //                                    StringBuilder sec1Fields = new StringBuilder();
        //                                    sec1Fields.Append("Bt1LesFbMSIDNNo, ");
        //                                    sec1Fields.Append("Bt1LesFbStartDate, ");
        //                                    sec1Fields.Append("Bt1LespFbEndDate, ");
        //                                    sec1Fields.Append("Bt1LesFbDataPlanIn, ");
        //                                    sec1Fields.Append("Bt1LesFbVce2Fixed, ");
        //                                    sec1Fields.Append("Bt1LesFbVce2Cell, ");
        //                                    sec1Fields.Append("Bt1LesFbVc2Bgan, ");
        //                                    sec1Fields.Append("Bt1LesFbVce2Fb, ");
        //                                    sec1Fields.Append("Bt1LesFbVce2Gpsp, ");
        //                                    sec1Fields.Append("Bt1LesFbVce2VceMail, ");
        //                                    sec1Fields.Append("Bt1LesFbVce2Others ");

        //                                    StringBuilder secValues = new StringBuilder();
        //                                    secValues.Append("@ID, ");
        //                                    secValues.Append("@StartDate, ");
        //                                    secValues.Append("@EndDate, ");
        //                                    secValues.Append("@DataPlan, ");
        //                                    secValues.Append("@Voice2Fixed, ");
        //                                    secValues.Append("@Voice2Cell, ");
        //                                    secValues.Append("@Voice2Bgan, ");
        //                                    secValues.Append("@Voice2Fb, ");
        //                                    secValues.Append("@Voice2Gpsp, ");
        //                                    secValues.Append("@Voice2VceMail, ");
        //                                    secValues.Append("@Voice2Others ");


        //                                    SaveToRejectionFBT(cmd, sec1Fields.ToString(), secValues.ToString());
        //                                    isExistCT = true;
        //                                    break;

        //                                }

        //                            }
        //                            #endregion


        //                            #region streaming
        //                            else if (ServiceType.ToLower().Trim().Contains("streaming") && dictrejection["streaming"].ToLower().Trim().Contains("streaming"))
        //                            {
        //                                if (source.Equals("CusReg"))
        //                                {
        //                                    bool Isvalid = false;
        //                                    bool isnotvalid = true;

        //                                    if (rdr.GetString(rdr.GetOrdinal("BCtFbMSIDNNo")).Equals(dictrejection["ID"]))
        //                                    {
        //                                        if ((Convert.ToDateTime(rdr.GetDateTime(rdr.GetOrdinal("BCtFbStartDate")))) < (Convert.ToDateTime(dictrejection["startdate"])))
        //                                            Isvalid = true;
        //                                        else
        //                                            isnotvalid = false;

        //                                        if (dictrejection.ContainsKey("enddate") && !string.IsNullOrEmpty(dictrejection["enddate"]))
        //                                        {
        //                                            if ((Convert.ToDateTime(rdr.GetDateTime(rdr.GetOrdinal("BCtpFbEndDate")))) < (Convert.ToDateTime(dictrejection["enddate"])))
        //                                                Isvalid = true;
        //                                            else
        //                                                isnotvalid = false;
        //                                        }

        //                                        if (dictrejection.ContainsKey("BarDateLifted") && !string.IsNullOrEmpty(dictrejection["BarDateLifted"]))
        //                                        {
        //                                            if ((Convert.ToDateTime(rdr.GetDateTime(rdr.GetOrdinal("BCFbBarDateLifted")))) < (Convert.ToDateTime(dictrejection["BarDateLifted"])))
        //                                                Isvalid = true;
        //                                            else
        //                                                isnotvalid = false;
        //                                        }

        //                                        if (dictrejection.ContainsKey("SuspensionDateLifted") && !string.IsNullOrEmpty(dictrejection["SuspensionDateLifted"]))
        //                                        {
        //                                            if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbSusDateLifted")))) < (Convert.ToDateTime(dictrejection["SuspensionDateLifted"])))
        //                                                Isvalid = true;
        //                                            else
        //                                                isnotvalid = false;
        //                                        }

        //                                        if (dictrejection.ContainsKey("Lay_LiftedDate") && !string.IsNullOrEmpty(dictrejection["Lay_LiftedDate"]))
        //                                        {
        //                                            if ((Convert.ToDateTime(rdr.GetString(rdr.GetOrdinal("BCFbLayUpDateLifted")))) < (Convert.ToDateTime(dictrejection["Lay_LiftedDate"])))
        //                                                Isvalid = true;
        //                                            else
        //                                                isnotvalid = false;
        //                                        }





        //                                    }


        //                                }
        //                                else
        //                                {

        //                                }
        //                            }
        //                            #endregion

        //                            #region SMS
        //                            else if (ServiceType.ToLower().Trim().Contains("sms") && dictrejection["sms"].ToLower().Trim().Contains("sms"))
        //                            {
        //                                if (source.Equals("CusReg"))
        //                                {
        //                                }
        //                                else
        //                                {
        //                                }
        //                            }
        //                            #endregion




        //                        if (!isExistCT)
        //                        {
        //                            StringBuilder sec1Fields = new StringBuilder();
        //                            StringBuilder sec1values = new StringBuilder();

        //                            if (dictrejection["Service Type"].ToLower().Trim().Contains("voice"))
        //                            {
        //                                sec1Fields.Append("Bt1LesFbMSIDNNo, ");
        //                                sec1Fields.Append("Bt1LesFbStartDate, ");
        //                                sec1Fields.Append("Bt1LespFbEndDate, ");
        //                                sec1Fields.Append("Bt1LesFbVce2Fixed, ");
        //                                sec1Fields.Append("Bt1LesFbVce2Cell, ");
        //                                sec1Fields.Append("Bt1LesFbVce2Fb, ");
        //                                sec1Fields.Append("Bt1LesFbDataPlanIn ");


        //                                sec1values.Append("@ID, ");
        //                                sec1values.Append("@StartDate, ");
        //                                sec1values.Append("@EndDate, ");
        //                                sec1values.Append("@Voice2Fixed, ");
        //                                sec1values.Append("@Voice2Cell, ");
        //                                sec1values.Append("@Voice2Fb, ");
        //                                sec1values.Append("@DataPlan ");



        //                            }

        //                            else if (dictrejection["Service Type"].ToLower().Trim().Contains("standard ip"))
        //                            {
        //                                sec1Fields.Append("Bt1LesFbMSIDNNo, ");
        //                                sec1Fields.Append("Bt1LesFbStartDate, ");
        //                                sec1Fields.Append("Bt1LesFbBilIncre, ");
        //                                sec1Fields.Append("Bt1LesFbDataPlanIn ");


        //                                sec1values.Append("@ID, ");
        //                                sec1values.Append("@StartDate, ");
        //                                sec1values.Append("@Units, ");
        //                                sec1values.Append("@DataPlan ");


        //                            }

        //                            else if (dictrejection["Service Type"].ToLower().Trim().Contains("subscription fee"))
        //                            {
        //                                sec1Fields.Append("Bt1LesFbMSIDNNo, ");
        //                                sec1Fields.Append("Bt1LesFbStartDate, ");
        //                                sec1Fields.Append("Bt1LesFbRecurFee, ");
        //                                sec1Fields.Append("Bt1LesFbDataPlanIn ");


        //                                sec1values.Append("@ID, ");
        //                                sec1values.Append("@StartDate, ");
        //                                sec1values.Append("@RecurFee, ");
        //                                sec1values.Append("@DataPlan ");
        //                            }

        //                            SaveToRejectionFBT(cmd, sec1Fields.ToString(), sec1values.ToString());
        //                        }

        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return isSuccess;
        //}
        #endregion

        private static void SaveToRejectionFBT(SqlCommand cmd, string sec1Field, string sec1Values)
        {
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = string.Format("INSERT INTO Bst1A,Bst1B,BSt1C,BSt1D({0}) VALUES({1})", sec1Field, sec1Values);
                cmd.ExecuteNonQuery();
            }
        }

        private static void SaveToInvoiceFBT(SqlCommand cmd, string sec1Field, string sec1Values)
        {
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = string.Format("INSERT INTO BSt2A,BSt2B,BSt2C,BSt2D({0}) VALUES({1})", sec1Field, sec1Values);
                cmd.ExecuteNonQuery();
            }
        }

        public static void LogError(Exception ex, string Path)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            string path = Path; // Server.MapPath("~/ErrorLog/ErrorLog.txt");
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(message);
                writer.Close();
            }
        }
    }
}

