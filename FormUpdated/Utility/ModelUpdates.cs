﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace FormUpdated
{
    public sealed class ModelUpdates
    {
        
        public static Models.NoonReports UpdateForm()
        {
            Models.NoonReports updatereports = new Models.NoonReports();

            string sql = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection con = new SqlConnection(sql);
            SqlCommand cmd = new SqlCommand("select top 1 * from NoonreportT order by ID desc", con);
            con.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                updatereports.VoyageNo = !dr.IsDBNull(dr.GetOrdinal("VoyageNo")) ? dr.GetString(dr.GetOrdinal("VoyageNo")) : string.Empty;

                updatereports.ShipName = !dr.IsDBNull(dr.GetOrdinal("ShipName")) ? dr.GetString(dr.GetOrdinal("ShipName")) : string.Empty;

                updatereports.ReportDate = !dr.IsDBNull(dr.GetOrdinal("ReportDate")) ? dr.GetDateTime(dr.GetOrdinal("ReportDate")).ToString() : string.Empty;

                updatereports.CallSign = !dr.IsDBNull(dr.GetOrdinal("CallSign")) ? dr.GetString(dr.GetOrdinal("CallSign")) : string.Empty;

                updatereports.Latitude = !dr.IsDBNull(dr.GetOrdinal("Latitude")) ? dr.GetString(dr.GetOrdinal("Latitude")) : string.Empty;

                updatereports.Longitude = !dr.IsDBNull(dr.GetOrdinal("Longitude")) ? dr.GetString(dr.GetOrdinal("Longitude")) : string.Empty;

                updatereports.NPTime = !dr.IsDBNull(dr.GetOrdinal("NPTime")) ? dr.GetTimeSpan(dr.GetOrdinal("NPTime")) : TimeSpan.FromMinutes(1);

                updatereports.Remarks = !dr.IsDBNull(dr.GetOrdinal("Remarks")) ? dr.GetString(dr.GetOrdinal("Remarks")) : string.Empty;

                updatereports.VesselHeading = !dr.IsDBNull(dr.GetOrdinal("VesselHeading")) ? dr.GetString(dr.GetOrdinal("VesselHeading")) : string.Empty;

                updatereports.CTime = !dr.IsDBNull(dr.GetOrdinal("CTime")) ? dr.GetTimeSpan(dr.GetOrdinal("CTime")) : TimeSpan.FromMinutes(1);

                updatereports.Remarks1 = !dr.IsDBNull(dr.GetOrdinal("Remarks1")) ? dr.GetString(dr.GetOrdinal("Remarks1")) : string.Empty;

                updatereports.AverageSpeed_At_Noons = !dr.IsDBNull(dr.GetOrdinal("AverageSpeed_At_Noons")) ? dr.GetString(dr.GetOrdinal("AverageSpeed_At_Noons")) : string.Empty;

                updatereports.Forvoyagesincelastfullaway = !dr.IsDBNull(dr.GetOrdinal("Forvoyagesincelastfullaway")) ? dr.GetString(dr.GetOrdinal("Forvoyagesincelastfullaway")) : string.Empty;

                updatereports.AtNoon = !dr.IsDBNull(dr.GetOrdinal("AtNoon")) ? dr.GetString(dr.GetOrdinal("AtNoon")) : string.Empty;

                updatereports.C_rdpsdp = !dr.IsDBNull(dr.GetOrdinal("C_rdpsdp")) ? dr.GetString(dr.GetOrdinal("C_rdpsdp")) : string.Empty;

                updatereports.Forthelast24hrs = !dr.IsDBNull(dr.GetOrdinal("Forthelast24hours")) ? dr.GetString(dr.GetOrdinal("Forthelast24hours")) : string.Empty;

                updatereports.Forthevoyage = !dr.IsDBNull(dr.GetOrdinal("Forthevoyage")) ? dr.GetString(dr.GetOrdinal("Forthevoyage")) : string.Empty;

                updatereports.ETADate = !dr.IsDBNull(dr.GetOrdinal("ETADate")) ? dr.GetDateTime(dr.GetOrdinal("ETADate")).ToString() : string.Empty;

                updatereports.LocalTime = !dr.IsDBNull(dr.GetOrdinal("LocalTime")) ? dr.GetTimeSpan(dr.GetOrdinal("LocalTime")) : TimeSpan.FromMinutes(1);

                Models.NoonReports._countryID =  !dr.IsDBNull(dr.GetOrdinal("CountryID")) ? dr.GetInt32(dr.GetOrdinal("CountryID")) : 0;
                

                updatereports.Port = !dr.IsDBNull(dr.GetOrdinal("Port")) ? dr.GetString(dr.GetOrdinal("Port")) : string.Empty;

                updatereports.MFOForMainEngine = !dr.IsDBNull(dr.GetOrdinal("MFOForMainEngine")) ? dr.GetString(dr.GetOrdinal("MFOForMainEngine")) : string.Empty;

                updatereports.MFOForBoiler = !dr.IsDBNull(dr.GetOrdinal("MFOForBoiler")) ? dr.GetString(dr.GetOrdinal("MFOForBoiler")) : string.Empty;

                updatereports.MGOForMainEngine = !dr.IsDBNull(dr.GetOrdinal("MGOForMainEngine")) ? dr.GetString(dr.GetOrdinal("MGOForMainEngine")) : string.Empty;

                updatereports.MGOForAuxiliaryEngines = !dr.IsDBNull(dr.GetOrdinal("MGOForAuxiliaryEngines")) ? dr.GetString(dr.GetOrdinal("MGOForAuxiliaryEngines")) : string.Empty;

                updatereports.MGOForBoiler = !dr.IsDBNull(dr.GetOrdinal("MGOForBoiler")) ? dr.GetString(dr.GetOrdinal("MGOForBoiler")) : string.Empty;

                updatereports.TotalMFO = !dr.IsDBNull(dr.GetOrdinal("TotalMFO")) ? dr.GetString(dr.GetOrdinal("TotalMFO")) : string.Empty;

                updatereports.TotalMGO = !dr.IsDBNull(dr.GetOrdinal("TotalMGO")) ? dr.GetString(dr.GetOrdinal("TotalMGO")) : string.Empty;

                updatereports.MFO = !dr.IsDBNull(dr.GetOrdinal("MFO")) ? dr.GetString(dr.GetOrdinal("MFO")) : string.Empty;

                updatereports.MGO = !dr.IsDBNull(dr.GetOrdinal("MGO")) ? dr.GetString(dr.GetOrdinal("MGO")) : string.Empty;

                updatereports.LOMainEnginesystem_Sump = !dr.IsDBNull(dr.GetOrdinal("LOMainEnginesystem_Sump")) ? dr.GetDecimal(dr.GetOrdinal("LOMainEnginesystem_Sump")) : Convert.ToDecimal(0);

                updatereports.LOMainEnginesystem_StorageTank = !dr.IsDBNull(dr.GetOrdinal("LOMainEnginesystem_StorageTank")) ? dr.GetDecimal(dr.GetOrdinal("LOMainEnginesystem_StorageTank")) : Convert.ToDecimal(0);

                updatereports.LOMainEngineCylinderOil = !dr.IsDBNull(dr.GetOrdinal("LOMainEngineCylinderOil")) ? dr.GetDecimal(dr.GetOrdinal("LOMainEngineCylinderOil")) : Convert.ToDecimal(0);

                updatereports.LOAuxiliaryEngines = !dr.IsDBNull(dr.GetOrdinal("LOAuxiliaryEngines")) ? dr.GetDecimal(dr.GetOrdinal("LOAuxiliaryEngines")) : Convert.ToDecimal(0);

                updatereports.LOHydraulicSystem = !dr.IsDBNull(dr.GetOrdinal("LOHydraulicSystem")) ? dr.GetDecimal(dr.GetOrdinal("LOHydraulicSystem")) : Convert.ToDecimal(0);

                updatereports.LOROPMainEnginesump = !dr.IsDBNull(dr.GetOrdinal("LOROPMainEnginesump")) ? dr.GetDecimal(dr.GetOrdinal("LOROPMainEnginesump")) : Convert.ToDecimal(0);

                updatereports.MEsystemoil = !dr.IsDBNull(dr.GetOrdinal("MEsystemoil")) ? dr.GetDecimal(dr.GetOrdinal("MEsystemoil")) : Convert.ToDecimal(0);

                updatereports.MainEnginecyclinderoil = !dr.IsDBNull(dr.GetOrdinal("MainEnginecylinderoil")) ? dr.GetDecimal(dr.GetOrdinal("MainEnginecylinderoil")) : Convert.ToDecimal(0);

                updatereports.AEsystemoil = !dr.IsDBNull(dr.GetOrdinal("AEsystemoil")) ? dr.GetDecimal(dr.GetOrdinal("AEsystemoil")) : Convert.ToDecimal(0);

                updatereports.LOROPHydraulicSystem = !dr.IsDBNull(dr.GetOrdinal("LOROPHydraulicSystem")) ? dr.GetDecimal(dr.GetOrdinal("LOROPHydraulicSystem")) : Convert.ToDecimal(0);

                updatereports.Meterreading = !dr.IsDBNull(dr.GetOrdinal("Meterreading")) ? dr.GetString(dr.GetOrdinal("Meterreading")) : string.Empty;

                updatereports.Runninghours = !dr.IsDBNull(dr.GetOrdinal("Runninghours")) ? dr.GetTimeSpan(dr.GetOrdinal("Runninghours")) : TimeSpan.FromMinutes(1);

                updatereports.Waterproduced = !dr.IsDBNull(dr.GetOrdinal("Waterproduced")) ? dr.GetString(dr.GetOrdinal("Waterproduced")) : string.Empty;

                updatereports.Totalwater = !dr.IsDBNull(dr.GetOrdinal("Totalwater")) ? dr.GetString(dr.GetOrdinal("Totalwater")) : string.Empty;

                updatereports.APT_PandS = !dr.IsDBNull(dr.GetOrdinal("APT_PandS")) ? dr.GetString(dr.GetOrdinal("APT_PandS")) : string.Empty;

                updatereports.Feedwatertank = !dr.IsDBNull(dr.GetOrdinal("Feedwatertank")) ? dr.GetString(dr.GetOrdinal("Feedwatertank")) : string.Empty;

                updatereports.CWT3_PandS = !dr.IsDBNull(dr.GetOrdinal("CWT3_PandS")) ? dr.GetString(dr.GetOrdinal("CWT3_PandS")) : string.Empty;

                updatereports.Main = !dr.IsDBNull(dr.GetOrdinal("Main")) ? dr.GetString(dr.GetOrdinal("Main")) : string.Empty;

                updatereports.FuelRackPosUnits1t06 = !dr.IsDBNull(dr.GetOrdinal("FuelRackPosUnits1to6")) ? dr.GetString(dr.GetOrdinal("FuelRackPosUnits1to6")) : string.Empty;

                updatereports.Exhausttemptunit1to6 = !dr.IsDBNull(dr.GetOrdinal("Exhausttemptunit1to6")) ? dr.GetString(dr.GetOrdinal("Exhausttemptunit1to6")) : string.Empty;

                updatereports.DailyMainEngine = !dr.IsDBNull(dr.GetOrdinal("DailyMainEngine")) ? dr.GetString(dr.GetOrdinal("DailyMainEngine")) : string.Empty;

                updatereports.AE2 = !dr.IsDBNull(dr.GetOrdinal("AE2")) ? dr.GetString(dr.GetOrdinal("AE2")) : string.Empty;

                updatereports.AE1 = !dr.IsDBNull(dr.GetOrdinal("AE1")) ? dr.GetString(dr.GetOrdinal("AE1")) : string.Empty;

                updatereports.Boiler = !dr.IsDBNull(dr.GetOrdinal("Boiler")) ? dr.GetString(dr.GetOrdinal("Boiler")) : string.Empty;

                updatereports.FuelMainEngine = !dr.IsDBNull(dr.GetOrdinal("FuelMainEngine")) ? dr.GetString(dr.GetOrdinal("FuelMainEngine")) : string.Empty;

                updatereports.FuelAuxiliraryEngine = !dr.IsDBNull(dr.GetOrdinal("FuelAuxiliaryEngine")) ? dr.GetString(dr.GetOrdinal("FuelAuxiliaryEngine")) : string.Empty;

                updatereports.FuelBoiler = !dr.IsDBNull(dr.GetOrdinal("FuelBoiler")) ? dr.GetString(dr.GetOrdinal("FuelBoiler")) : string.Empty;

                updatereports.AnswerYN = !dr.IsDBNull(dr.GetOrdinal("AnswerYN")) ? dr.GetBoolean(dr.GetOrdinal("AnswerYN")) ? true : false : false;

                updatereports.Reason = !dr.IsDBNull(dr.GetOrdinal("Reason")) ? dr.GetString(dr.GetOrdinal("Reason")) : string.Empty;

                updatereports.EventCompany = !dr.IsDBNull(dr.GetOrdinal("EventCompany")) ? dr.GetString(dr.GetOrdinal("EventCompany")) : string.Empty;

                updatereports.EventName = !dr.IsDBNull(dr.GetOrdinal("EventName")) ? dr.GetString(dr.GetOrdinal("EventName")) : string.Empty;

                updatereports.EventPurpose = !dr.IsDBNull(dr.GetOrdinal("EventPurpose")) ? dr.GetString(dr.GetOrdinal("EventPurpose")) : string.Empty;

                updatereports.EventDate = !dr.IsDBNull(dr.GetOrdinal("EventDate")) ? dr.GetDateTime(dr.GetOrdinal("EventDate")).ToString() : string.Empty;

                updatereports.EventTimein = !dr.IsDBNull(dr.GetOrdinal("EventTimein")) ? dr.GetTimeSpan(dr.GetOrdinal("EventTimein")) : TimeSpan.FromMinutes(1);

                updatereports.EventRemarks = !dr.IsDBNull(dr.GetOrdinal("EventRemarks")) ? dr.GetString(dr.GetOrdinal("EventRemarks")) : string.Empty;

                updatereports.Company2nd = !dr.IsDBNull(dr.GetOrdinal("Company2nd")) ? dr.GetString(dr.GetOrdinal("Company2nd")) : string.Empty;

                updatereports.Name2nd = !dr.IsDBNull(dr.GetOrdinal("Name2nd")) ? dr.GetString(dr.GetOrdinal("Name2nd")) : string.Empty;

                updatereports.Purpose2nd = !dr.IsDBNull(dr.GetOrdinal("Purpose2nd")) ? dr.GetString(dr.GetOrdinal("Purpose2nd")) : string.Empty;

                updatereports.Date_2nd = !dr.IsDBNull(dr.GetOrdinal("Date_2nd")) ? dr.GetDateTime(dr.GetOrdinal("Date_2nd")).ToString() : string.Empty;

                updatereports.Timein2nd = !dr.IsDBNull(dr.GetOrdinal("Timein2nd")) ? dr.GetTimeSpan(dr.GetOrdinal("Timein2nd")) : TimeSpan.FromMinutes(1);

                updatereports.Remarks2nd = !dr.IsDBNull(dr.GetOrdinal("Remarks2nd")) ? dr.GetString(dr.GetOrdinal("Remarks2nd")) : string.Empty;

                updatereports.Name_1 = !dr.IsDBNull(dr.GetOrdinal("Name1")) ? dr.GetString(dr.GetOrdinal("Name1")) : string.Empty;

                updatereports.Rank_ = !dr.IsDBNull(dr.GetOrdinal("Rank_")) ? dr.GetString(dr.GetOrdinal("Rank_")) : string.Empty;

                updatereports.Department = !dr.IsDBNull(dr.GetOrdinal("Department")) ? dr.GetString(dr.GetOrdinal("Department")) : string.Empty;

                updatereports.Date1 = !dr.IsDBNull(dr.GetOrdinal("Date1")) ? dr.GetDateTime(dr.GetOrdinal("Date1")).ToString() : string.Empty;

                updatereports.Timein1 = !dr.IsDBNull(dr.GetOrdinal("Timein1")) ? dr.GetTimeSpan(dr.GetOrdinal("Timein1")) : TimeSpan.FromMinutes(1);

                updatereports.Remarks3 = !dr.IsDBNull(dr.GetOrdinal("Remarks3")) ? dr.GetString(dr.GetOrdinal("Remarks3")) : string.Empty;

                updatereports.NameC = !dr.IsDBNull(dr.GetOrdinal("NameC")) ? dr.GetString(dr.GetOrdinal("NameC")) : string.Empty;

                updatereports.Rank_C = !dr.IsDBNull(dr.GetOrdinal("Rank_C")) ? dr.GetString(dr.GetOrdinal("Rank_C")) : string.Empty;

                updatereports.DepartmentC = !dr.IsDBNull(dr.GetOrdinal("DepartmentC")) ? dr.GetString(dr.GetOrdinal("DepartmentC")) : string.Empty;

                updatereports.DateC = !dr.IsDBNull(dr.GetOrdinal("DateC")) ? dr.GetDateTime(dr.GetOrdinal("DateC")).ToString() : string.Empty;

                updatereports.TimeinC = !dr.IsDBNull(dr.GetOrdinal("TimeinC")) ? dr.GetTimeSpan(dr.GetOrdinal("TimeinC")) : TimeSpan.FromMinutes(1);

                updatereports.RemarksC = !dr.IsDBNull(dr.GetOrdinal("RemarksC")) ? dr.GetString(dr.GetOrdinal("RemarksC")) : string.Empty;

                updatereports.SuplierCompany = !dr.IsDBNull(dr.GetOrdinal("SuplierCompany")) ? dr.GetString(dr.GetOrdinal("SuplierCompany")) : string.Empty;

                updatereports.SuplierDO = !dr.IsDBNull(dr.GetOrdinal("SuplierDO")) ? dr.GetString(dr.GetOrdinal("SuplierDO")) : string.Empty;

                updatereports.SuplierInvoice = !dr.IsDBNull(dr.GetOrdinal("SuplierInvoice")) ? dr.GetString(dr.GetOrdinal("SuplierInvoice")) : string.Empty;

                updatereports.SuplierDiscription = !dr.IsDBNull(dr.GetOrdinal("SuplierDiscription")) ? dr.GetString(dr.GetOrdinal("SuplierDiscription")) : string.Empty;

                updatereports.SuplierRemarks = !dr.IsDBNull(dr.GetOrdinal("SuplierRemarks")) ? dr.GetString(dr.GetOrdinal("SuplierRemarks")) : string.Empty;

                updatereports.ItemDelivery = !dr.IsDBNull(dr.GetOrdinal("ItemDelivery")) ? dr.GetString(dr.GetOrdinal("ItemDelivery")) : string.Empty;

                updatereports.ItemDate = !dr.IsDBNull(dr.GetOrdinal("ItemDate")) ? dr.GetDateTime(dr.GetOrdinal("ItemDate")).ToString() : string.Empty;

                updatereports.ItemCompany = !dr.IsDBNull(dr.GetOrdinal("ItemCompany")) ? dr.GetString(dr.GetOrdinal("ItemCompany")) : string.Empty;

                updatereports.ItemDiscription = !dr.IsDBNull(dr.GetOrdinal("ItemDiscription")) ? dr.GetString(dr.GetOrdinal("ItemDiscription")) : string.Empty;

                updatereports.ItemQTY = !dr.IsDBNull(dr.GetOrdinal("ItemQTY")) ? dr.GetString(dr.GetOrdinal("ItemQTY")) : string.Empty;

                updatereports.ItemPurchaseorder = !dr.IsDBNull(dr.GetOrdinal("ItemPurchaseorder")) ? dr.GetString(dr.GetOrdinal("ItemPurchaseorder")) : string.Empty;

                updatereports.DeliveryI = !dr.IsDBNull(dr.GetOrdinal("DeliveryI")) ? dr.GetString(dr.GetOrdinal("DeliveryI")) : string.Empty;

                updatereports.DateI = !dr.IsDBNull(dr.GetOrdinal("DateI")) ? dr.GetDateTime(dr.GetOrdinal("DateI")).ToString() : string.Empty;

                updatereports.CompanyI = !dr.IsDBNull(dr.GetOrdinal("CompanyI")) ? dr.GetString(dr.GetOrdinal("CompanyI")) : string.Empty;

                updatereports.DiscriptionI = !dr.IsDBNull(dr.GetOrdinal("DiscriptionI")) ? dr.GetString(dr.GetOrdinal("DiscriptionI")) : string.Empty;

                updatereports.QTYI = !dr.IsDBNull(dr.GetOrdinal("QTY1")) ? dr.GetString(dr.GetOrdinal("QTY1")) : string.Empty;

                updatereports.PurchaseorderI = !dr.IsDBNull(dr.GetOrdinal("PurchaseorderI")) ? dr.GetString(dr.GetOrdinal("PurchaseorderI")) : string.Empty;

                updatereports.ETA = !dr.IsDBNull(dr.GetOrdinal("ETA")) ? dr.GetString(dr.GetOrdinal("ETA")) : string.Empty;

                updatereports.ETD = !dr.IsDBNull(dr.GetOrdinal("ETD")) ? dr.GetString(dr.GetOrdinal("ETD")) : string.Empty;
            }
            con.Close();
            return updatereports;
        }
    }
}