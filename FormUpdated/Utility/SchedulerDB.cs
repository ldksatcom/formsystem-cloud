﻿using FormUpdated.Areas.LDKSatcom.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Web;

namespace FormUpdated
{
    public sealed class SchedulerDB
    {
        public static bool Validated(string val)
        {
            try
            {
                decimal de = Convert.ToDecimal(val);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Dear Developer. it method used to convert array data to formated dictionary data
        /// </summary>
        /// <param name="data"> it is the array data it retrieve from the email body text</param>
        /// <returns> Formated dictinary data it's used to store demonoonreport table in db</returns>
        public static Dictionary<string, dynamic> getFormData(string[] data)
        {

            Dictionary<string, dynamic> dicNoonReport = new Dictionary<string, dynamic>();

            dicNoonReport.Add("ReportType", data[0]);

            dicNoonReport.Add("VoyageNo", data[1]);


            dicNoonReport.Add("ShipName", data[2]);

            dicNoonReport.Add("ReportDate", data[3]);

            dicNoonReport.Add("CallSign", data[4]);

            dicNoonReport.Add("Latitude", data[5]);

            dicNoonReport.Add("Longitude", data[6]);

            dicNoonReport.Add("NPTime", data[7]);

            dicNoonReport.Add("Remarks", data[8]);

            dicNoonReport.Add("VesselHeading", data[9]);

            dicNoonReport.Add("CTime", data[10]);

            dicNoonReport.Add("Remarks1", data[11]);

            dicNoonReport.Add("AverageSpeed_At_Noons", data[12]);

            dicNoonReport.Add("Forvoyagesincelastfullaway", data[13]);

            dicNoonReport.Add("ForMainEngine", data[14]);

            dicNoonReport.Add("ForBoiler", data[15]);

            dicNoonReport.Add("ForMainEngine1", data[16]);

            dicNoonReport.Add("ForAuxiliaryEngines", data[17]);

            dicNoonReport.Add("ForBoiler1", data[18]);

            dicNoonReport.Add("TotalMFO", data[19]);

            dicNoonReport.Add("TotalMGO", data[20]);

            dicNoonReport.Add("MFO", data[21]);

            dicNoonReport.Add("MGO", data[22]);

            dicNoonReport.Add("MainEnginesystem_Sump", data[23]);

            dicNoonReport.Add("MainEnginesystem_StorageTank", data[24]);

            dicNoonReport.Add("IsUpdated", data[25]);

            return dicNoonReport;
        }

        /// <summary>
        /// To store dictionary value in NoonReports database
        /// </summary>
        /// <param name="dictNoonReport">Collection of Form Values as Dictionary </param>
        /// <returns></returns>
        public static Tuple<string, int> SavetoDB(Dictionary<string, dynamic> dictNoonReport)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString()))
            {
                StringBuilder strData = new StringBuilder();

                try
                {
                    con.Open();
                }
                catch { throw new Exception("The internet connection is not established, please make sure the internet connections."); }

                SqlCommand cmd = new SqlCommand("[SP_Insert_DemoNoonReport]", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                if (dictNoonReport.ContainsKey("ReportType") && !string.IsNullOrEmpty(dictNoonReport["ReportType"]))
                {
                    cmd.Parameters.AddWithValue("@ReportType", dictNoonReport["ReportType"]);
                    strData.Append(dictNoonReport["ReportType"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ReportType", DBNull.Value);
                    strData.Append(" ").Append(",");
                }

                if (dictNoonReport.ContainsKey("VoyageNo") && !string.IsNullOrEmpty(dictNoonReport["VoyageNo"]))
                {
                    cmd.Parameters.AddWithValue("@VoyageNo", dictNoonReport["VoyageNo"]);
                    strData.Append(dictNoonReport["VoyageNo"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@VoyageNo", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("ShipName") && !string.IsNullOrEmpty(dictNoonReport["ShipName"]))
                {
                    cmd.Parameters.AddWithValue("@ShipName", dictNoonReport["ShipName"]);
                    strData.Append(dictNoonReport["ShipName"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ShipName", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("ReportDate") && !string.IsNullOrEmpty(dictNoonReport["ReportDate"]))
                {
                    cmd.Parameters.AddWithValue("@ReportDate", Convert.ToDateTime(dictNoonReport["ReportDate"]));
                    strData.Append(dictNoonReport["ReportDate"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ReportDate", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("CallSign") && !string.IsNullOrEmpty(dictNoonReport["CallSign"]))
                {
                    cmd.Parameters.AddWithValue("@CallSign", dictNoonReport["CallSign"]);
                    strData.Append(dictNoonReport["CallSign"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@CallSign", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("Latitude") && !string.IsNullOrEmpty(dictNoonReport["Latitude"]))
                {
                    cmd.Parameters.AddWithValue("@Latitude", dictNoonReport["Latitude"]);
                    strData.Append(dictNoonReport["Latitude"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Latitude", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("Longitude") && !string.IsNullOrEmpty(dictNoonReport["Longitude"]))
                {
                    cmd.Parameters.AddWithValue("@Longitude", dictNoonReport["Longitude"]);
                    strData.Append(dictNoonReport["Longitude"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Longitude", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("NPTime") && !string.IsNullOrEmpty(dictNoonReport["NPTime"]))
                {
                    cmd.Parameters.AddWithValue("@NPTime", dictNoonReport["NPTime"]);
                    strData.Append(dictNoonReport["NPTime"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@NPTime", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("Remarks") && !string.IsNullOrEmpty(dictNoonReport["Remarks"]))
                {
                    cmd.Parameters.AddWithValue("@Remarks", dictNoonReport["Remarks"]);
                    strData.Append(dictNoonReport["Remarks"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Remarks", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("VesselHeading") && !string.IsNullOrEmpty(dictNoonReport["VesselHeading"]))
                {
                    cmd.Parameters.AddWithValue("@VesselHeading", dictNoonReport["VesselHeading"]);
                    strData.Append(dictNoonReport["VesselHeading"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@VesselHeading", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("CTime") && !string.IsNullOrEmpty(dictNoonReport["CTime"]))
                {
                    cmd.Parameters.AddWithValue("@CTime", dictNoonReport["CTime"]);
                    strData.Append(dictNoonReport["CTime"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@CTime", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("Remarks1") && !string.IsNullOrEmpty(dictNoonReport["Remarks1"]))
                {
                    cmd.Parameters.AddWithValue("@Remarks1", dictNoonReport["Remarks1"]);
                    strData.Append(dictNoonReport["Remarks1"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Remarks1", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("AverageSpeed_At_Noons") && !string.IsNullOrEmpty(dictNoonReport["AverageSpeed_At_Noons"]))
                {
                    cmd.Parameters.AddWithValue("@AverageSpeed_At_Noons", dictNoonReport["AverageSpeed_At_Noons"]);
                    strData.Append(dictNoonReport["AverageSpeed_At_Noons"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@AverageSpeed_At_Noons", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("Forvoyagesincelastfullaway") && !string.IsNullOrEmpty(dictNoonReport["Forvoyagesincelastfullaway"]))
                {
                    cmd.Parameters.AddWithValue("@Forvoyagesincelastfullaway", dictNoonReport["Forvoyagesincelastfullaway"]);
                    strData.Append(dictNoonReport["Forvoyagesincelastfullaway"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Forvoyagesincelastfullaway", DBNull.Value);
                    strData.Append(",");
                }


                if (dictNoonReport.ContainsKey("ForMainEngine") && !string.IsNullOrEmpty(dictNoonReport["ForMainEngine"]))
                {
                    cmd.Parameters.AddWithValue("@MFOForMainEngine", dictNoonReport["ForMainEngine"]);
                    strData.Append(dictNoonReport["ForMainEngine"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@MFOForMainEngine", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("ForBoiler") && !string.IsNullOrEmpty(dictNoonReport["ForBoiler"]))
                {
                    cmd.Parameters.AddWithValue("@MFOForBoiler", dictNoonReport["ForBoiler"]);
                    strData.Append(dictNoonReport["ForBoiler"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@MFOForBoiler", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("ForMainEngine1") && !string.IsNullOrEmpty(dictNoonReport["ForMainEngine1"]))
                {
                    cmd.Parameters.AddWithValue("@MGOForMainEngine1", dictNoonReport["ForMainEngine1"]);
                    strData.Append(dictNoonReport["ForMainEngine1"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@MGOForMainEngine1", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("ForAuxiliaryEngines") && !string.IsNullOrEmpty(dictNoonReport["ForAuxiliaryEngines"]))
                {
                    cmd.Parameters.AddWithValue("@MGOForAuxiliaryEngines", dictNoonReport["ForAuxiliaryEngines"]);
                    strData.Append(dictNoonReport["ForAuxiliaryEngines"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@MGOForAuxiliaryEngines", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("ForBoiler1") && !string.IsNullOrEmpty(dictNoonReport["ForBoiler1"]))
                {
                    cmd.Parameters.AddWithValue("@MGOForBoiler1", dictNoonReport["ForBoiler1"]);
                    strData.Append(dictNoonReport["ForBoiler1"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@MGOForBoiler1", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("TotalMFO") && !string.IsNullOrEmpty(dictNoonReport["TotalMFO"]))
                {
                    cmd.Parameters.AddWithValue("@TotalMFO", dictNoonReport["TotalMFO"]);
                    strData.Append(dictNoonReport["TotalMFO"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@TotalMFO", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("TotalMGO") && !string.IsNullOrEmpty(dictNoonReport["TotalMGO"]))
                {
                    cmd.Parameters.AddWithValue("@TotalMGO", dictNoonReport["TotalMGO"]);
                    strData.Append(dictNoonReport["TotalMGO"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@TotalMGO", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("MFO") && !string.IsNullOrEmpty(dictNoonReport["MFO"]))
                {
                    cmd.Parameters.AddWithValue("@MFO", dictNoonReport["MFO"]);
                    strData.Append(dictNoonReport["MFO"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@MFO", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("MGO") && !string.IsNullOrEmpty(dictNoonReport["MGO"]))
                {
                    cmd.Parameters.AddWithValue("@MGO", dictNoonReport["MGO"]);
                    strData.Append(dictNoonReport["MGO"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@MGO", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("MainEnginesystem_Sump") && !string.IsNullOrEmpty(dictNoonReport["MainEnginesystem_Sump"]))
                {
                    cmd.Parameters.AddWithValue("@LoMainEnginesystem_Sump", dictNoonReport["MainEnginesystem_Sump"]);
                    strData.Append(dictNoonReport["MainEnginesystem_Sump"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@LoMainEnginesystem_Sump", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("MainEnginesystem_StorageTank") && !string.IsNullOrEmpty(dictNoonReport["MainEnginesystem_StorageTank"]))
                {
                    cmd.Parameters.AddWithValue("@LoMainEnginesystem_StorageTank", dictNoonReport["MainEnginesystem_StorageTank"]);
                    strData.Append(dictNoonReport["MainEnginesystem_StorageTank"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@LoMainEnginesystem_StorageTank", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("IsUpdated") && !string.IsNullOrEmpty(dictNoonReport["IsUpdated"]))
                {
                    cmd.Parameters.AddWithValue("@isUpdated", dictNoonReport["IsUpdated"]);
                    strData.Append(dictNoonReport["IsUpdated"]).Append(",");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@isUpdated", DBNull.Value);
                    strData.Append(" ").Append(",");
                }

                cmd.Parameters.AddWithValue("@RID", System.Data.SqlDbType.Int);
                cmd.Parameters["@RID"].Direction = System.Data.ParameterDirection.Output;

                int NRID = 0;

                try
                {
                    cmd.ExecuteNonQuery();

                    if (cmd.Parameters["@RID"] != null)
                    {
                        NRID = Convert.ToInt32(cmd.Parameters["@RID"].Value);
                    }
                }
                catch (Exception er)
                {
                    throw new Exception(er.Message);
                }

                return new Tuple<string, int>(strData.ToString(), NRID);
            }
            return new Tuple<string, int>(string.Empty, 0);

        }


        // store Excel sheet (or any file for that matter) into a SQL Server table  
        public static void StoreFileToDatabase(string FileName, int ReportID, byte[] buffer, string contentType)
        {
            // if file doesn't exist --> terminate (you might want to show a message box or something)  
            //if (!File.Exists(FileName))
            //{
            //    return;
            //}

            //getContentType getContentTypes = new getContentType();
            //string contentType = getContentTypes[Path.GetExtension(FileName)];

            // define SQL statement to use  
            string insertStmt = "INSERT INTO AttachFiles(FileNames, ContentType, Data, NRid) VALUES(@FileName, @ContentType, @BinaryContent, @NRid)";

            // set up connection and command to do INSERT  
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString()))
            {
                using (SqlCommand cmdInsert = new SqlCommand(insertStmt, connection))
                {
                    cmdInsert.Parameters.Add("@FileName", System.Data.SqlDbType.NVarChar, 500).Value = Path.GetFileName(FileName);
                    cmdInsert.Parameters.Add("@ContentType", System.Data.SqlDbType.NVarChar, 300).Value = contentType;
                    cmdInsert.Parameters.Add("@BinaryContent", System.Data.SqlDbType.VarBinary, int.MaxValue).Value = buffer;
                    cmdInsert.Parameters.Add("@NRid", System.Data.SqlDbType.Int).Value = ReportID;

                    // open connection, execute SQL statement, close connection again  
                    connection.Open();
                    cmdInsert.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        public static List<Dictionary<string, string>> GetEmailPasswordsADO()
        {
            List<Dictionary<string, string>> lstDict = new List<Dictionary<string, string>>();
            // define SQL statement to use  
            string insertStmt = "select EmailID, Password from CrewDetails";
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString()))
            {
                using (SqlCommand cmdInsert = new SqlCommand(insertStmt, connection))
                {
                    connection.Open();
                    SqlDataReader rd = cmdInsert.ExecuteReader();

                    while (rd.Read())
                    {
                        Dictionary<string, string> dict = new Dictionary<string, string>();
                        if (!rd.IsDBNull(rd.GetOrdinal("EmailID")))
                            dict.Add("EmailID", rd.GetString(rd.GetOrdinal("EmailID")));
                        if (!rd.IsDBNull(rd.GetOrdinal("Password")))
                            dict.Add("Password", rd.GetString(rd.GetOrdinal("Password")));

                        lstDict.Add(dict);
                    }
                }
            }
            return lstDict;
        }
        

        public static List<Dictionary<string, string>> GetEmailPasswords()
        {
            List<Dictionary<string, string>> lstDict = new List<Dictionary<string, string>>();
            List<CrewDetail> lstCrew = new List<CrewDetail>();
            using (NoonReportFormatEntities noonreport = new NoonReportFormatEntities())
            {
                if (MySession.isAdmin)
                    lstCrew = noonreport.CrewDetails.Where(t => t.EmailID != null && t.Password != null).ToList();
                else
                    lstCrew = noonreport.CrewDetails.Where(t => t.CompanyName.Equals(MySession.curCompany)).ToList();

                foreach (CrewDetail content in lstCrew)
                {
                    Dictionary<string, string> dict = new Dictionary<string, string>();

                    dict.Add("EmailID", content.EmailID);
                    dict.Add("Password", content.Password);
                    lstDict.Add(dict);
                }
            }
            return lstDict;
        }
        
        public static void RetrieveExcelFileFromDatabase(int ID, string excelFileName)
        {
            byte[] excelContents;

            string selectStmt = "SELECT Data FROM AttachFiles WHERE ID = @ID";

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString()))
            using (SqlCommand cmdSelect = new SqlCommand(selectStmt, connection))
            {
                cmdSelect.Parameters.Add("@ID", System.Data.SqlDbType.Int).Value = 1;

                connection.Open();
                excelContents = (byte[])cmdSelect.ExecuteScalar();
                connection.Close();
            }

            File.WriteAllBytes(@"C:\Users\User\Desktop\Test.xlsx", excelContents);
        }

        public static string DecompressString(string compressedtext)
        {
            byte[] gZipBuffer = Convert.FromBase64String(compressedtext);
            using (var memoryStream = new MemoryStream())
            {
                int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
                memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

                var buffer = new byte[dataLength];

                memoryStream.Position = 0;
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                {
                    gZipStream.Read(buffer, 0, buffer.Length);
                }

                return Encoding.UTF8.GetString(buffer);
            }
        }

        public static string CompressedZIP(string filename, string withoutFN)
        {
            string outputfilename = Path.GetTempPath() + withoutFN + ".zip";

            using (FileStream inputstream = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite))

            using (FileStream outputstream = new FileStream(outputfilename, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                using (GZipStream csv = new GZipStream(outputstream, CompressionMode.Compress))
                {
                    inputstream.CopyTo(csv);
                }
            }
            return outputfilename;
        }
    }

    public class ContentTypes
    {
        public string Extenstion { get; set; }

        public string ContentType { get; set; }
    }

    public class getContentType
    {
        List<ContentTypes> lstContentTypes = new List<ContentTypes>();

        public getContentType()
        {
            lstContentTypes.Add(new ContentTypes { Extenstion = ".xls", ContentType = "application/vnd.ms-excel" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".xlsx", ContentType = "application/vnd.ms-excel" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".zip", ContentType = "application/zip" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".3gp", ContentType = "video/3gpp" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".7z", ContentType = "application/x-7z-compressed" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".txt", ContentType = "text/plain" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".tar", ContentType = "application/x-tar" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".rtf", ContentType = "application/rtf" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".rar", ContentType = "application/x-rar-compressed" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".ppt", ContentType = "application/vnd.ms-powerpoint" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".pdf", ContentType = "application/pdf" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".png", ContentType = "image/png" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".mpeg", ContentType = "video/mpeg" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".mp3", ContentType = "audio/mpeg" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".js", ContentType = "text/javascript" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".json", ContentType = "application/json" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".jpeg", ContentType = "image/jpeg" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".jpg", ContentType = "image/jpeg" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".jar", ContentType = "application/java-archive" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".ics", ContentType = "text/calendar" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".ico", ContentType = "	image/vnd.microsoft.icon" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".html", ContentType = "text/html" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".gif", ContentType = "image/gif" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".docx", ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".doc", ContentType = "application/msword" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".csv", ContentType = "text/csv" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".css", ContentType = "text/css" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".bmp", ContentType = "image/bmp" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".bin", ContentType = "application/octet-stream" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".avi", ContentType = "video/x-msvideo" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".aac", ContentType = "audio/aac" });
        }

        public string this[string exten]
        {
            get
            {
                string contType = string.Empty;
                foreach (ContentTypes contentTypes in lstContentTypes)
                {
                    if (contentTypes.Extenstion.Equals(exten))
                        contType = contentTypes.ContentType;
                }
                return contType;
            }
        }

    }
}
