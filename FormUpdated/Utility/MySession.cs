﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FormUpdated
{
    public class MySession
    {
        public static bool isAdmin
        {
            get { return Convert.ToBoolean(HttpContext.Current.Session["isAdmin"]); }
            set { HttpContext.Current.Session["isAdmin"] = value; }
        }

        public static string curCompany
        {
            get { return Convert.ToString(HttpContext.Current.Session["curCompany"]); }
            set { HttpContext.Current.Session["curCompany"] = value; }
        }

        public static string curVessel
        {
            get { return Convert.ToString(HttpContext.Current.Session["curVessel"]); }
            set { HttpContext.Current.Session["curVessel"] = value; }
        }

        public static string AccessLevel
        {
            get { return Convert.ToString(HttpContext.Current.Session["curAccessLevel"]); }
            set { HttpContext.Current.Session["curAccessLevel"] = value; }
        }

        public static string EmailID
        {
            get { return Convert.ToString(HttpContext.Current.Session["curEmailID"]); }
            set { HttpContext.Current.Session["curEmailID"] = value; }
        }

        public static string Password
        {
            get { return Convert.ToString(HttpContext.Current.Session["curPassword"]); }
            set { HttpContext.Current.Session["curPassword"] = value; }
        }

        public static string InvoiceLogoPath
        {
            get { return Convert.ToString(HttpContext.Current.Session["InvoiceLogoPath"]); }
            set { HttpContext.Current.Session["InvoiceLogoPath"] = value; }
        }
    }
}