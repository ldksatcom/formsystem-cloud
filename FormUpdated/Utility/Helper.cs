﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
//using Outlook = Microsoft.Office.Interop.Outlook;
//using Microsoft.Office.Interop.Outlook;
using System.Runtime.InteropServices;
using Compressed_Library;
using DocumentFormat.OpenXml.Packaging;
using FormUpdated.Models;
using FormUpdated.Areas.LDKSatcom.Models;

namespace FormUpdated
{
    public sealed class Helper

    {

        public static string GetOrderID()
        {
            string OrderKey = string.Empty;
            Guid obj = Guid.NewGuid();
            if (obj != null)
            {
                string[] guids = Convert.ToString(obj).Contains('-') ? Convert.ToString(obj).Split('-') : new string[0];
                if (guids.Length > 0 && !string.IsNullOrEmpty(guids[0]))
                    OrderKey = guids[0];

            }
            return OrderKey;
        }

        public static bool isDate(DateTime dateTime)
        {
            try
            {
                string[] formats = {"M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt",
                   "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss",
                   "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt",
                   "M/d/yyyy h:mm", "M/d/yyyy h:mm",
                   "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm"};

                DateTime parsedDateTime;
                return DateTime.TryParseExact(Convert.ToString(dateTime), formats, new System.Globalization.CultureInfo("en-US"),
                                               System.Globalization.DateTimeStyles.None, out parsedDateTime);
            }
            catch { return false; }
        }

        public static bool isTime(TimeSpan input)
        {
            TimeSpan dummyOutput;
            return TimeSpan.TryParse(Convert.ToString(input), out dummyOutput);
        }

        public static bool isDecimal(decimal? value)
        {
            Decimal val;
            return Decimal.TryParse(Convert.ToString(value),out val);
        }

        public static Dictionary<string, dynamic> getFormData( NoonReports objNoonReport = null, string[] data = null)
        {
            bool rec = false;
            if (data != null && data.Length > 1)
                rec = true;
            Dictionary<string, dynamic> dicNoonReport = new Dictionary<string, dynamic>();

            if ( (objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.VoyageNo)) || rec)
                dicNoonReport.Add("VoyageNo", rec ? data[0] : objNoonReport.VoyageNo);

            if ((objNoonReport!=null && !string.IsNullOrEmpty(objNoonReport.ShipName)) || rec)
                dicNoonReport.Add("ShipName", rec ? data[1] : objNoonReport.ShipName);

            if  ((objNoonReport!= null && !string.IsNullOrEmpty(objNoonReport.ReportDate))  || rec)
                dicNoonReport.Add("ReportDate", rec ? data[2] : Convert.ToDateTime(objNoonReport.ReportDate).Date.ToString("yyy/MM/dd"));

            if ((objNoonReport!=null && !string.IsNullOrEmpty(objNoonReport.CallSign)) || rec)
                dicNoonReport.Add("CallSign", rec ? data[3] : objNoonReport.CallSign);

            if ((objNoonReport!=null && !string.IsNullOrEmpty( objNoonReport.Latitude)) || rec)
                dicNoonReport.Add("Latitude", rec ? data[4] : objNoonReport.Latitude);

            if ((objNoonReport!=null && !string.IsNullOrEmpty(objNoonReport.Longitude)) || rec)
                dicNoonReport.Add("Longitude", rec ? data[5] : objNoonReport.Longitude);

            if ((objNoonReport != null && isTime(objNoonReport.NPTime)) || rec)
                dicNoonReport.Add("NPTime", rec ? data[6] : Convert.ToString(objNoonReport.NPTime));

            if ((objNoonReport!=null && !string.IsNullOrEmpty(objNoonReport.Remarks)) || rec)
                dicNoonReport.Add("Remarks", rec ? data[7] :objNoonReport.Remarks);

            if ((objNoonReport!=null && !string.IsNullOrEmpty(objNoonReport.VesselHeading)) || rec)
                dicNoonReport.Add("VesselHeading", rec ? data[8] : objNoonReport.VesselHeading);

            if ((objNoonReport != null && isTime(objNoonReport.CTime))  || rec)
                dicNoonReport.Add("CTime", rec ? data[9] : Convert.ToString(objNoonReport.CTime));

            if ((objNoonReport!=null && !string.IsNullOrEmpty(objNoonReport.Remarks1)) || rec)
                dicNoonReport.Add("Remarks1", rec ? data[10] : objNoonReport.Remarks1);

            if ((objNoonReport!=null && !string.IsNullOrEmpty(objNoonReport.AverageSpeed_At_Noons)) || rec)
                dicNoonReport.Add("AverageSpeed_At_Noons", rec ? data[11] : objNoonReport.AverageSpeed_At_Noons);

            if ((objNoonReport!=null && !string.IsNullOrEmpty(objNoonReport.Forvoyagesincelastfullaway)) || rec)
                dicNoonReport.Add("Forvoyagesincelastfullaway", rec ? data[12] : objNoonReport.Forvoyagesincelastfullaway);

            if ((objNoonReport!= null && !string.IsNullOrEmpty(objNoonReport.AtNoon)) || rec)
                dicNoonReport.Add("AtNoon", rec ? data[13] : objNoonReport.AtNoon);

            if ((objNoonReport!=null && !string.IsNullOrEmpty(objNoonReport.C_rdpsdp)) || rec)
                dicNoonReport.Add("C_rdpsdp", rec ? data[14] : objNoonReport.C_rdpsdp);

            if ((objNoonReport!=null && !string.IsNullOrEmpty(objNoonReport.Forthelast24hrs)) || rec)
                dicNoonReport.Add("Forthelast24hours", rec ? data[15] : objNoonReport.Forthelast24hrs);

            if ((objNoonReport !=null && !string.IsNullOrEmpty(objNoonReport.Forthevoyage)) || rec)
                dicNoonReport.Add("Forthevoyage", rec ? data[16] : objNoonReport.Forthevoyage);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.ETADate)) || rec)
                dicNoonReport.Add("ETADate", rec ? data[17] : Convert.ToDateTime(objNoonReport.ETADate).Date.ToString("yyy/MM/dd"));

            if ((objNoonReport != null && isTime(objNoonReport.LocalTime)) || rec)
                dicNoonReport.Add("LocalTime", rec ? data[18] : Convert.ToString(objNoonReport.LocalTime));

            if ((objNoonReport!=null && !string.IsNullOrEmpty(Convert.ToString(objNoonReport.CountryName)) || rec))
                dicNoonReport.Add("CountryID", rec ? !string.IsNullOrEmpty(data[19]) ? Convert.ToInt32(data[19]) : 0 : Convert.ToInt32(objNoonReport.CountryName));

            if ((objNoonReport!=null && !string.IsNullOrEmpty(objNoonReport.Port)) || rec)
                dicNoonReport.Add("Port", rec ? data[20] : objNoonReport.Port);

            if ((objNoonReport!=null && !string.IsNullOrEmpty(objNoonReport.MFOForMainEngine)) || rec)
                dicNoonReport.Add("ForMainEngine", rec ? data[21] : objNoonReport.MFOForMainEngine);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.MFOForBoiler)) || rec)
                dicNoonReport.Add("ForBoiler", rec ? data[22] : objNoonReport.MFOForBoiler);

            if (( objNoonReport!=null && !string.IsNullOrEmpty(objNoonReport.MGOForMainEngine)) || rec)
                dicNoonReport.Add("ForMainEngine1", rec ? data[23] : objNoonReport.MGOForMainEngine);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.MGOForAuxiliaryEngines)) || rec)
                dicNoonReport.Add("ForAuxiliaryEngines", rec ? data[24] : objNoonReport.MGOForAuxiliaryEngines);

            if ((objNoonReport!=null && !string.IsNullOrEmpty(objNoonReport.MGOForBoiler)) || rec)
                dicNoonReport.Add("ForBoiler1", rec ? data[25] : objNoonReport.MGOForBoiler);

            if ((objNoonReport!=null && !string.IsNullOrEmpty(objNoonReport.TotalMFO)) || rec)
                dicNoonReport.Add("TotalMFO", rec ? data[26] : objNoonReport.TotalMFO);

            if ((objNoonReport !=null && !string.IsNullOrEmpty(objNoonReport.TotalMGO)) || rec)
                dicNoonReport.Add("TotalMGO", rec ? data[27] :objNoonReport.TotalMGO);

            if ((objNoonReport !=null && !string.IsNullOrEmpty(objNoonReport.MFO)) || rec)
                dicNoonReport.Add("MFO", rec ? data[28] : objNoonReport.MFO);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.MGO)) || rec)
                dicNoonReport.Add("MGO", rec ? data[29] : objNoonReport.MGO);

            if ((objNoonReport != null && isDecimal(objNoonReport.LOMainEnginesystem_Sump)) || rec)
                dicNoonReport.Add("MainEnginesystem_Sump", rec ? data[30] :Convert.ToString( objNoonReport.LOMainEnginesystem_Sump));

            if ((objNoonReport != null && isDecimal(objNoonReport.LOMainEnginesystem_StorageTank)) || rec)
                dicNoonReport.Add("MainEnginesystem_StorageTank", rec ? data[31] : Convert.ToString(objNoonReport.LOMainEnginesystem_StorageTank));

            if ((objNoonReport != null && isDecimal(objNoonReport.LOMainEngineCylinderOil)) || rec)
                dicNoonReport.Add("MainEngineCylinderOil", rec ? data[32] : Convert.ToString(objNoonReport.LOMainEngineCylinderOil));

            if ((objNoonReport != null && isDecimal(objNoonReport.LOAuxiliaryEngines)) || rec)
                dicNoonReport.Add("AuxiliaryEngines", rec ? data[33] : Convert.ToString(objNoonReport.LOAuxiliaryEngines));

            if ((objNoonReport != null && isDecimal(objNoonReport.LOHydraulicSystem)) || rec)
                dicNoonReport.Add("HydraulicSystem", rec ? data[34] : Convert.ToString(objNoonReport.LOHydraulicSystem));

            if ((objNoonReport != null && isDecimal(objNoonReport.LOROPMainEnginesump)) || rec)
                dicNoonReport.Add("MainEnginesump", rec ? data[35] : Convert.ToString(objNoonReport.LOROPMainEnginesump));

            if ((objNoonReport != null && isDecimal(objNoonReport.MEsystemoil)) ||  rec)
                dicNoonReport.Add("MEsystemoil", rec ? data[36] : Convert.ToString(objNoonReport.MEsystemoil));

            if ((objNoonReport != null && isDecimal(objNoonReport.MainEnginecyclinderoil)) || rec)
                dicNoonReport.Add("MainEnginecylinderoil1", rec ? data[37] : Convert.ToString(objNoonReport.MainEnginecyclinderoil));

            if ((objNoonReport != null && isDecimal(objNoonReport.AEsystemoil)) || rec)
                dicNoonReport.Add("AEsystemoil", rec ? data[38] : Convert.ToString(objNoonReport.AEsystemoil));

            if ((objNoonReport != null && isDecimal(objNoonReport.LOROPHydraulicSystem)) || rec)
                dicNoonReport.Add("HydraulicSystem1", rec ? data[39] : Convert.ToString(objNoonReport.LOROPHydraulicSystem));

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.Meterreading)) || rec)
                dicNoonReport.Add("Meterreading", rec ? data[40] : objNoonReport.Meterreading);

            if ((objNoonReport != null && isTime(objNoonReport.Runninghours)) || rec)
                dicNoonReport.Add("Runninghours", rec ? data[41] : Convert.ToString(objNoonReport.Runninghours));

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.Waterproduced)) || rec)
                dicNoonReport.Add("Waterproduced", rec ? data[42] : objNoonReport.Waterproduced);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.Totalwater)) || rec)
                dicNoonReport.Add("Totalwater", rec ? data[43] : objNoonReport.Totalwater);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.APT_PandS)) || rec)
                dicNoonReport.Add("APT_PandS", rec ? data[44] : objNoonReport.APT_PandS);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.Feedwatertank)) || rec)
                dicNoonReport.Add("Feedwatertank", rec ? data[45] : objNoonReport.Feedwatertank);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.CWT3_PandS)) || rec)
                dicNoonReport.Add("CWT3_PandS", rec ? data[46] : objNoonReport.CWT3_PandS);

            if (( objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.Main)) || rec)
                dicNoonReport.Add("Main", rec ? data[47] : objNoonReport.Main);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.FuelRackPosUnits1t06)) || rec)
                dicNoonReport.Add("FuelRackPosUnits1to6", rec ? data[48] : objNoonReport.FuelRackPosUnits1t06);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.Exhausttemptunit1to6)) || rec)
                dicNoonReport.Add("Exhausttemptunit1to6", rec ? data[49] : objNoonReport.Exhausttemptunit1to6);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.DailyMainEngine)) || rec)
                dicNoonReport.Add("MainEngine", rec ? data[50] : objNoonReport.DailyMainEngine);

            if (( objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.AE2)) || rec)
                dicNoonReport.Add("AE2", rec ? data[51] : objNoonReport.AE2);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.AE1)) || rec)
                dicNoonReport.Add("AE1", rec ? data[52] : objNoonReport.AE1);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.Boiler)) || rec)
                dicNoonReport.Add("Boiler", rec ? data[53] : objNoonReport.Boiler);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.FuelMainEngine)) || rec)
                dicNoonReport.Add("MainEngine1", rec ? data[54] : objNoonReport.FuelMainEngine);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.FuelAuxiliraryEngine)) || rec)
                dicNoonReport.Add("AuxiliaryEngine", rec ? data[55] : objNoonReport.FuelAuxiliraryEngine);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.FuelBoiler)) || rec)
                dicNoonReport.Add("Boiler1", rec ? data[56] : objNoonReport.FuelBoiler);

            if (objNoonReport != null && (objNoonReport.AnswerYN) || rec)
                dicNoonReport.Add("AnswerYN", rec ? data[57] : "Yes");
            else
                dicNoonReport.Add("AnswerYN", rec ? data[57] : "No");
            
            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.Reason)) || rec)
                dicNoonReport.Add("Reason", rec ? data[58] : objNoonReport.Reason);

            //Events
            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.EventCompany)) || rec)
                dicNoonReport.Add("Company", rec ? data[59] : objNoonReport.EventCompany);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.EventName)) || rec)
                dicNoonReport.Add("Name", rec ? data[60] : objNoonReport.EventName);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.EventPurpose)) || rec)
                dicNoonReport.Add("Purpose", rec ? data[61] : objNoonReport.EventPurpose);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.EventDate)) || rec)
                dicNoonReport.Add("Date_", rec ? data[62] : Convert.ToDateTime(objNoonReport.EventDate).Date.ToString("yyy/MM/dd"));

            if ((objNoonReport != null && isTime(objNoonReport.EventTimein)) || rec)
                dicNoonReport.Add("Timein", rec ? data[63] : Convert.ToString(objNoonReport.EventTimein));

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.EventRemarks)) || rec)
                dicNoonReport.Add("Remarks2", rec ? data[64] : objNoonReport.EventRemarks);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.Company2nd)) || rec)
                dicNoonReport.Add("Company2nd", rec ? data[65] : objNoonReport.Company2nd);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.Name2nd)) || rec)
                dicNoonReport.Add("Name2nd", rec ? data[66] : objNoonReport.Name2nd);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.Purpose2nd)) || rec)
                dicNoonReport.Add("Purpose2nd", rec ? data[67] : objNoonReport.Purpose2nd);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.Date_2nd)) || rec)
                dicNoonReport.Add("Date_2nd", rec ? data[68] : Convert.ToDateTime(objNoonReport.Date_2nd).Date.ToString("yyy/MM/dd"));

            if ((objNoonReport != null && isTime(objNoonReport.Timein2nd)) || rec)
                dicNoonReport.Add("Timein2nd", rec ? data[69] : Convert.ToString(objNoonReport.Timein2nd));

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.Remarks2nd)) || rec)
                dicNoonReport.Add("Remarks2nd", rec ? data[70] : objNoonReport.Remarks2nd);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.Name_1)) || rec)
                dicNoonReport.Add("Name1", rec ? data[71] : objNoonReport.Name_1);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.Rank_)) || rec)
                dicNoonReport.Add("Rank_", rec ? data[72] : objNoonReport.Rank_);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.Department)) || rec)
                dicNoonReport.Add("Department", rec ? data[73] : objNoonReport.Department);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.Date1)) || rec)
                dicNoonReport.Add("Date1", rec ? data[74] : Convert.ToDateTime(objNoonReport.Date1).Date.ToString("yyy/MM/dd"));

            if ((objNoonReport != null && isTime(objNoonReport.Timein1)) || rec)
                dicNoonReport.Add("Timein1", rec ? data[75] : Convert.ToString(objNoonReport.Timein1));

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.Remarks3)) || rec)
                dicNoonReport.Add("Remarks3", rec ? data[76] : objNoonReport.Remarks3);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.NameC)) || rec)
                dicNoonReport.Add("NameC", rec ? data[77] : objNoonReport.NameC);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.Rank_C)) || rec)
                dicNoonReport.Add("Rank_C", rec ? data[78] : objNoonReport.Rank_C);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.DepartmentC)) || rec)
                dicNoonReport.Add("DepartmentC", rec ? data[79] : objNoonReport.DepartmentC);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.DateC)) || rec)
                dicNoonReport.Add("DateC", rec ? data[80] : Convert.ToDateTime(objNoonReport.DateC).Date.ToString("yyy/MM/dd"));

            if ((objNoonReport != null && isTime(objNoonReport.TimeinC)) || rec)
                dicNoonReport.Add("TimeinC", rec ? data[81] : Convert.ToString(objNoonReport.TimeinC));

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.RemarksC)) || rec)
                dicNoonReport.Add("RemarksC", rec ? data[82] : objNoonReport.RemarksC);



            //if (!string.IsNullOrEmpty(CrewDate2.Text))
            //    dicNoonReport.Add("DateC", Convert.ToDateTime(CrewDate2.Text).ToString("dd/MM/yyyy"));

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.SuplierCompany)) || rec)
                dicNoonReport.Add("Company1", rec ? data[83] : objNoonReport.SuplierCompany);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.SuplierDO)) || rec)
                dicNoonReport.Add("DO", rec ? data[84] : objNoonReport.SuplierDO);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.SuplierInvoice)) || rec)
                dicNoonReport.Add("Invoice", rec ? data[85] : objNoonReport.SuplierInvoice);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.SuplierDiscription)) || rec)
                dicNoonReport.Add("SDescription", rec ? data[86] : objNoonReport.SuplierDiscription);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.SuplierRemarks)) || rec)
                dicNoonReport.Add("Remarks4", rec ? data[87] : objNoonReport.SuplierRemarks);


            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.ItemDelivery)) || rec)
                dicNoonReport.Add("Delivery", rec ? data[88] : objNoonReport.ItemDelivery);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.ItemDate)) || rec)
                dicNoonReport.Add("Date_2", rec ? data[89] : Convert.ToDateTime(objNoonReport.ItemDate).Date.ToString("yyy/MM/dd"));

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.ItemCompany)) || rec)
                dicNoonReport.Add("Company2", rec ? data[90] : objNoonReport.ItemCompany);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.ItemDiscription)) || rec)
                dicNoonReport.Add("Description1", rec ? data[91] : objNoonReport.ItemDiscription);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.ItemQTY)) || rec)
                dicNoonReport.Add("QTY", rec ? data[92] : objNoonReport.ItemQTY);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.ItemPurchaseorder)) || rec)
                dicNoonReport.Add("Purchaseorder", rec ? data[93] : objNoonReport.ItemPurchaseorder);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.ItemDelivery)) || rec)
                dicNoonReport.Add("DeliveryI", rec ? data[94] : objNoonReport.ItemDelivery);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.DateI)) || rec)
                dicNoonReport.Add("Date_I", rec ? data[95] : Convert.ToDateTime(objNoonReport.DateI).ToString("yyy/MM/dd"));

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.CompanyI)) || rec)
                dicNoonReport.Add("CompanyI", rec ? data[96] : objNoonReport.CompanyI);

            if ((objNoonReport!= null && !string.IsNullOrEmpty(objNoonReport.DiscriptionI)) || rec)
                dicNoonReport.Add("DescriptionI", rec ? data[97] : objNoonReport.DiscriptionI);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.QTYI)) || rec)
                dicNoonReport.Add("QTYI", rec ? data[98] : objNoonReport.QTYI);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.PurchaseorderI)) || rec)
                dicNoonReport.Add("PurchaseorderI", rec ? data[99] : objNoonReport.PurchaseorderI);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.ETA)) || rec)
                dicNoonReport.Add("ETA", rec ? data[100] : objNoonReport.ETA);

            if ((objNoonReport != null && !string.IsNullOrEmpty(objNoonReport.ETD)) || rec)
                dicNoonReport.Add("ETD", rec ? data[101] : objNoonReport.ETD);

            return dicNoonReport;
        }

        public static Models.NoonReports BindForm(Dictionary<string, dynamic> dictFromData)
        {
            Models.NoonReports noonReports = new Models.NoonReports();

            noonReports.VoyageNo = dictFromData["VoyageNo"];
            noonReports.ShipName = dictFromData["ShipName"];
            noonReports.ReportDate = dictFromData["ReportDate"];
            noonReports.CallSign = dictFromData["CallSign"];
            noonReports.Latitude = dictFromData["Latitude"];
            noonReports.Longitude = dictFromData["Longitude"];
            if(string.IsNullOrEmpty(dictFromData["NPTime"]))
                noonReports.NPTime = TimeSpan.Parse("00:00:00"); 
            else
                noonReports.NPTime =  TimeSpan.Parse(dictFromData["NPTime"]);
            noonReports.Remarks = dictFromData["Remarks"];
            noonReports.VesselHeading = dictFromData["VesselHeading"];
            if(string.IsNullOrEmpty(dictFromData["CTime"]))
                noonReports.CTime = TimeSpan.Parse("00:00:00");
            else
                noonReports.CTime = TimeSpan.Parse(dictFromData["CTime"]);
            noonReports.Remarks1 = dictFromData["Remarks1"];
            noonReports.AverageSpeed_At_Noons = dictFromData["AverageSpeed_At_Noons"];
            noonReports.Forvoyagesincelastfullaway = dictFromData["Forvoyagesincelastfullaway"];
            noonReports.AtNoon = dictFromData["AtNoon"];
            noonReports.C_rdpsdp = dictFromData["C_rdpsdp"];
            noonReports.Forthelast24hrs = dictFromData["Forthelast24hours"];
            noonReports.Forthevoyage = dictFromData["Forthevoyage"];
            noonReports.ETADate = dictFromData["ETADate"];
            if(string.IsNullOrEmpty(dictFromData["LocalTime"]))
                noonReports.LocalTime = TimeSpan.Parse("00:00:00");
            else
                noonReports.LocalTime = TimeSpan.Parse(dictFromData["LocalTime"]);
            noonReports.CountryName = Convert.ToString(dictFromData["CountryID"]);
            noonReports.Port = dictFromData["Port"];
            noonReports.MFOForMainEngine = dictFromData["ForMainEngine"];
            noonReports.MFOForBoiler = dictFromData["ForBoiler"];
            noonReports.MGOForMainEngine = dictFromData["ForMainEngine1"];
            noonReports.MGOForAuxiliaryEngines = dictFromData["ForAuxiliaryEngines"];
            noonReports.MGOForBoiler = dictFromData["ForBoiler1"];
            noonReports.TotalMFO = dictFromData["TotalMFO"];
            noonReports.TotalMGO = dictFromData["TotalMGO"];
            noonReports.MFO = dictFromData["MFO"];
            noonReports.MGO = dictFromData["MGO"];
          //  noonReports.LOMainEnginesystem_Sump = string.IsNullOrEmpty(dictFromData["MainEnginesystem_Sump"] ? 0 : Convert.ToDecimal(dictFromData["MainEnginesystem_Sump"]));
            noonReports.LOMainEnginesystem_Sump = Decimal.Parse((dictFromData["MainEnginesystem_Sump"] == null || dictFromData["MainEnginesystem_Sump"].ToString() == string.Empty) ? "0" : dictFromData["MainEnginesystem_Sump"].ToString());
            noonReports.LOMainEnginesystem_StorageTank = Decimal.Parse((dictFromData["MainEnginesystem_StorageTank"] ==null || dictFromData["MainEnginesystem_StorageTank"].ToString() == string.Empty) ? "0" : dictFromData["MainEnginesystem_StorageTank"].ToString());
            noonReports.MainEnginecyclinderoil = Decimal.Parse((dictFromData["MainEngineCylinderOil"] == null || dictFromData["MainEngineCylinderOil"].ToString() == string.Empty) ? "0" : dictFromData["MainEngineCylinderOil"].ToString());
            noonReports.LOAuxiliaryEngines = Decimal.Parse((dictFromData["AuxiliaryEngines"] == null || dictFromData["AuxiliaryEngines"].ToString() == string.Empty) ? "0" : dictFromData["AuxiliaryEngines"].ToString());
            noonReports.LOHydraulicSystem = Decimal.Parse((dictFromData["HydraulicSystem"] == null || dictFromData["HydraulicSystem"].ToString() == string.Empty) ? "0" : dictFromData["HydraulicSystem"].ToString());
            if (dictFromData["AnswerYN"].Contains("Yes"))
                noonReports.AnswerYN = true;
            else
                noonReports.AnswerYN = false;
            noonReports.Reason = dictFromData["Reason"];
            noonReports.LOROPMainEnginesump = Decimal.Parse((dictFromData["MainEnginesump"] == null || dictFromData["MainEnginesump"].ToString() == string.Empty) ? "0" : dictFromData["MainEnginesump"].ToString());
            noonReports.MEsystemoil = Decimal.Parse((dictFromData["MEsystemoil"] == null || dictFromData["MEsystemoil"].ToString() == string.Empty) ? "0" : dictFromData["MEsystemoil"].ToString());
            noonReports.LOMainEngineCylinderOil = Decimal.Parse((dictFromData["MainEnginecylinderoil1"] == null || dictFromData["MainEnginecylinderoil1"].ToString() == string.Empty) ? "0" : dictFromData["MainEnginecylinderoil1"].ToString());
            noonReports.AEsystemoil = Decimal.Parse((dictFromData["AEsystemoil"] == null || dictFromData["AEsystemoil"].ToString() == string.Empty) ? "0" : dictFromData["AEsystemoil"].ToString());
            noonReports.LOROPHydraulicSystem = Decimal.Parse((dictFromData["HydraulicSystem1"] == null || dictFromData["HydraulicSystem1"].ToString() == string.Empty) ? "0" : dictFromData["HydraulicSystem1"].ToString());
            noonReports.Meterreading = dictFromData["Meterreading"];
            if(string.IsNullOrEmpty(dictFromData["Runninghours"]))
                noonReports.Runninghours = TimeSpan.Parse("00:00:00");
            else
                noonReports.Runninghours = TimeSpan.Parse(dictFromData["Runninghours"]);
            noonReports.Waterproduced = dictFromData["Waterproduced"];
            noonReports.Totalwater = dictFromData["Totalwater"];
            noonReports.APT_PandS = dictFromData["APT_PandS"];
            noonReports.Feedwatertank = dictFromData["Feedwatertank"];
            noonReports.CWT3_PandS = dictFromData["CWT3_PandS"];
            noonReports.Main = dictFromData["Main"];
            noonReports.FuelRackPosUnits1t06 = dictFromData["FuelRackPosUnits1to6"];
            noonReports.Exhausttemptunit1to6 = dictFromData["Exhausttemptunit1to6"];
            noonReports.DailyMainEngine = dictFromData["MainEngine"];
            noonReports.AE2 = dictFromData["AE2"];
            noonReports.AE1 = dictFromData["AE1"];
            noonReports.Boiler = dictFromData["Boiler"];
            noonReports.FuelMainEngine = dictFromData["MainEngine1"];
            noonReports.FuelAuxiliraryEngine = dictFromData["AuxiliaryEngine"];
            noonReports.FuelBoiler = dictFromData["Boiler1"];
            noonReports.EventCompany = dictFromData["Company"];
            noonReports.EventName = dictFromData["Name"];
            noonReports.EventPurpose = dictFromData["Purpose"];
            noonReports.EventDate = dictFromData["Date_"];
            if(string.IsNullOrEmpty(dictFromData["Timein"]))
                noonReports.EventTimein = TimeSpan.Parse("00:00:00");
            else
                noonReports.EventTimein = TimeSpan.Parse(dictFromData["Timein"]);
            noonReports.EventRemarks = dictFromData["Remarks2"];
            noonReports.Company2nd = dictFromData["Company2nd"];
            noonReports.Name2nd = dictFromData["Name2nd"];
            noonReports.Purpose2nd = dictFromData["Purpose2nd"];
            noonReports.Date_2nd = dictFromData["Date_2nd"];
            if(string.IsNullOrEmpty(dictFromData["Timein2nd"]))
                noonReports.Timein2nd = TimeSpan.Parse("00:00:00");
            else
                noonReports.Timein2nd = TimeSpan.Parse(dictFromData["Timein2nd"]);
            noonReports.Remarks2nd = dictFromData["Remarks2nd"];
            noonReports.Name_1 = dictFromData["Name1"];
            noonReports.Rank_ = dictFromData["Rank_"];
            noonReports.Department = dictFromData["Department"];
            noonReports.Date1 = dictFromData["Date1"];
            if(string.IsNullOrEmpty(dictFromData["Timein1"]))
                noonReports.Timein1 = TimeSpan.Parse("00:00:00");
            else
                noonReports.Timein1 = TimeSpan.Parse(dictFromData["Timein1"]);
            noonReports.Remarks3 = dictFromData["Remarks3"];
            noonReports.NameC = dictFromData["NameC"];
            noonReports.Rank_C = dictFromData["Rank_C"];
            noonReports.DepartmentC = dictFromData["DepartmentC"];
            noonReports.DateC = dictFromData["DateC"];
            if(string.IsNullOrEmpty(dictFromData["TimeinC"]))
                noonReports.TimeinC = TimeSpan.Parse("00:00:00");
            else
                noonReports.TimeinC = TimeSpan.Parse(dictFromData["TimeinC"]);
            noonReports.RemarksC = dictFromData["RemarksC"];
            noonReports.SuplierCompany = dictFromData["Company1"];
            noonReports.SuplierDO = dictFromData["DO"];
            noonReports.SuplierInvoice = dictFromData["Invoice"];
            noonReports.SuplierDiscription = dictFromData["SDescription"];
            noonReports.SuplierRemarks = dictFromData["Remarks4"];
            noonReports.ItemDelivery = dictFromData["Delivery"];
            noonReports.ItemDate = dictFromData["Date_2"];
            noonReports.ItemCompany = dictFromData["Company2"];
            noonReports.ItemDiscription = dictFromData["Description1"];
            noonReports.DeliveryI = dictFromData["DeliveryI"];
            noonReports.DateI = dictFromData["Date_I"];
            noonReports.QTYI = dictFromData["QTY"];
            noonReports.PurchaseorderI = dictFromData["Purchaseorder"];
            noonReports.CompanyI = dictFromData["CompanyI"];
            noonReports.DiscriptionI = dictFromData["DescriptionI"];
            noonReports.QTYI = dictFromData["QTYI"];
            noonReports.PurchaseorderI = dictFromData["PurchaseorderI"];
            noonReports.ETA = dictFromData["ETA"];
            noonReports.ETD = dictFromData["ETD"];

            return noonReports;
        }

        //public static bool SendMail(Models.EmailFormat emailFormat)
        //{
        //    try
        //    {
        //        Microsoft.Office.Interop.Outlook.Application oApp = new Outlook.Application();

        //        Microsoft.Office.Interop.Outlook.MailItem oMsg = (Outlook.MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);

        //        Outlook.Recipients oRecips = (Outlook.Recipients)oMsg.Recipients;

        //        Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(emailFormat.Tomail);
        //        oRecip.Resolve();

        //        oMsg.CC = emailFormat.CCMail[0];

        //        //Add an attachment.
        //        if (emailFormat.Attachement.Contains("|"))
        //        {
        //            string[] attached = emailFormat.Attachement.Split('|');

        //            String sDisplayName = emailFormat.Attachement;
        //            //int iPosition = (int)oMsg.Body.Length + 1;
        //            //int iAttachType = (int)Outlook.OlAttachmentType.olByValue;
        //            //now attached the file
        //            Outlook.Attachment oAttach;

        //            foreach (string att in attached)
        //            {
        //                oAttach = oMsg.Attachments.Add(emailFormat.Attachement, Type.Missing, Type.Missing, Type.Missing);
        //            }
        //        }
        //        else
        //        {
        //            //String sDisplayName = file;
        //            //int iPosition = (int)oMsg.Body.Length + 1;
        //            //int iAttachType = (int)Outlook.OlAttachmentType.olByValue;
        //            //now attached the file
        //            Outlook.Attachment oAttach = oMsg.Attachments.Add
        //                                         (emailFormat.Attachement, Type.Missing, Type.Missing, Type.Missing);
        //        }
        //        //Subject line
        //        oMsg.Subject = emailFormat.Subject;
        //        // Send.
        //        oMsg.Send();

               
        //    }
        //    catch (System.Exception ex)
        //    {
                
        //    }
        //    return false;
        //}

        private static List<string> GetColName()
        {
            List<string> lstCol = new List<string>();
            lstCol.Add("Crew Id");
            lstCol.Add("Crew Name");
            lstCol.Add("Crew Title");
            lstCol.Add("Crew Date of Joining");
            lstCol.Add("Crew Leaving Date");
            lstCol.Add("Access Level");
            lstCol.Add("Is Suspension");
            lstCol.Add("Username");
            lstCol.Add("Password");
            lstCol.Add("Email Id");
            lstCol.Add("Company Name");
            lstCol.Add("Ship Name");
            lstCol.Add("Type");
            lstCol.Add("To");
            lstCol.Add("CC");
            lstCol.Add("Is Admin");
            lstCol.Add("Order Key");
            lstCol.Add("Machine ID");
            return lstCol;
        }

        public static void WriteCrewDetails(List<CrewDetail> lstCrewDetails, string destination)
        {
            try
            {
                using (var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
                {
                    var workbookPart = workbook.AddWorkbookPart();

                    workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

                    workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();


                    var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                    var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                    sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                    DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                    string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                    uint sheetId = 1;
                    if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                    {
                        sheetId =
                            sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                    }

                    DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = "Noon Report" };
                    sheets.Append(sheet);

                    DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                    List<String> columns = new List<string>();
                    System.Data.DataTable table = new System.Data.DataTable();
                    List<string> arColName = GetColName();

                    DocumentFormat.OpenXml.UInt32Value rowIndex = 0;

                    foreach (string column in arColName)
                    {
                        columns.Add(column);

                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column);
                        headerRow.AppendChild(cell);
                    }
                    rowIndex++;
                    headerRow.RowIndex = rowIndex;
                    sheetData.AppendChild(headerRow);

                    int crewID = 0;

                    foreach (CrewDetail crew in lstCrewDetails)
                    {
                        // Create new Row in Excel sheet
                        DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                        foreach (string col in columns)
                        {
                            DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                            switch (col)
                            {
                                case "Crew Id":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(++crewID));
                                    newRow.AppendChild(cell);
                                    break;

                                case "Crew Name":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.Name);
                                    newRow.AppendChild(cell);
                                    break;

                                case "Crew Title":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.Title);
                                    newRow.AppendChild(cell);
                                    break;

                                case "Crew Date of Joining":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Date;

                                    if (crew.DateofJoining != null)
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToDateTime(crew.DateofJoining).Date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                    newRow.AppendChild(cell);
                                    break;
                                case "Crew Leaving Date":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Date;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToDateTime(crew.LeavingDate).Date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                                    newRow.AppendChild(cell);
                                    break;
                                case "Access Level":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (!string.IsNullOrEmpty(crew.AccessLevel))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.AccessLevel);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("partial");
                                    newRow.AppendChild(cell);
                                    break;
                                case "Is Suspension":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (crew.IsSuspension.Equals(true))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("1");
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("0");

                                    newRow.AppendChild(cell);
                                    break;
                                case "Username":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (!string.IsNullOrEmpty(crew.UserName))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.UserName);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();

                                    newRow.AppendChild(cell);
                                    break;
                                case "Password":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (!string.IsNullOrEmpty(crew.Password))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.Password);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(string.Empty);
                                    newRow.AppendChild(cell);
                                    break;
                                case "Email Id":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                    if (!string.IsNullOrEmpty(crew.EmailID))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.EmailID);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                    newRow.AppendChild(cell);
                                    break;
                                case "Company Name":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                    if (!string.IsNullOrEmpty(crew.CompanyName))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.CompanyName);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                    newRow.AppendChild(cell);
                                    break;

                                case "Ship Name":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (!string.IsNullOrEmpty(crew.ShipName))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.ShipName);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(string.Empty);
                                    newRow.AppendChild(cell);
                                    break;

                                case "Type":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (!string.IsNullOrEmpty(crew.Type))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.Type);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("Ship");
                                    newRow.AppendChild(cell);
                                    break;

                                case "To":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (!string.IsNullOrEmpty(crew.Tomail))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.Tomail);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                    newRow.AppendChild(cell);
                                    break;

                                case "CC":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                    if (!string.IsNullOrEmpty(crew.CCMail))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.CCMail);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                    newRow.AppendChild(cell);
                                    break;

                                case "Is Admin":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (crew.isAdmin.Equals(true))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString("1"));
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("0");
                                    newRow.AppendChild(cell);
                                    break;
                                case "Order Key":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (!string.IsNullOrEmpty(crew.OrderKey))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.OrderKey);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                    newRow.AppendChild(cell);
                                    break;

                                case "Machine ID":
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                    if (!string.IsNullOrEmpty(crew.MachineID))
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(crew.MachineID);
                                    else
                                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                    newRow.AppendChild(cell);
                                    break;
                            }
                        }

                        rowIndex++;
                        newRow.RowIndex = rowIndex;
                        // Append new row in Excel
                        sheetData.AppendChild(newRow);
                    }

                }
            }
            catch (System.Exception roor) { }
        }
    }
}