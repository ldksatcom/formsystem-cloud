﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAGetMail;

namespace FormUpdated
{
    public sealed class SingletonConcept
    {
        private static SingletonConcept singletonConcept;
        private SingletonConcept()
        {

        }

        public static SingletonConcept Instance
        {
            get
            {
                if (singletonConcept == null)
                {
                    singletonConcept = new SingletonConcept();
                    return singletonConcept;
                }
                else
                {
                    return singletonConcept;
                }
            }
        }

        public void Recursing(Dictionary<string, string> pairs)
        {
            string DecompressedData = string.Empty;

            if (pairs.ContainsKey("EmailID") && pairs.ContainsKey("Password") && !string.IsNullOrEmpty(pairs["EmailID"]) && !string.IsNullOrEmpty(pairs["Password"]))
            {
                MailServer oServer = new MailServer("mail.ldksatcom.in",
                 pairs["EmailID"], pairs["Password"], ServerProtocol.Imap4);

                MailClient oClient = new MailClient("TryIt");

                // Set SSL connection
                oServer.SSLConnection = false;

                // Set IMAP4 SSL port
                oServer.Port = 143;

                //ReadMailItems();

                oClient.Connect(oServer);

                MailInfo[] infos = oClient.GetMailInfos();
                int infosLength = infos.Length;

                try
                {
                    for (int i = infos.Length - 1; i < infosLength; i--)
                    {
                        MailInfo info = infos[i];
                        //Console.WriteLine("Index: {0}; Size: {1}; UIDL: {2}",
                        //    info.Index, info.Size, info.UIDL);

                        // Download email from Hotmail/MSN IMAP4 server
                        Mail oMail = oClient.GetMail(info);

                        //Console.WriteLine("From: {0}", oMail.From.ToString());
                        //Console.WriteLine("Subject: {0}\r\n", oMail.Subject);

                        if (oMail.Subject.ToString().Contains("Morning_Report") && !oMail.Subject.ToString().Contains("Reply From Shore"))
                        {
                            MailAddress mailFrom = oMail.From;
                            MailAddress mailTo = oMail.From;
                            MailAddress[] mailCC = oMail.Cc;

                            // Generate an email file name based on date time.
                            System.DateTime d = System.DateTime.Now;
                            System.Globalization.CultureInfo cur = new
                                System.Globalization.CultureInfo("en-US");
                            string sdate = d.ToString("yyyyMMddHHmmss", cur);
                            //string fileName = String.Format("{0}\\{1}{2}{3}.eml",
                            //    mailbox, sdate, d.Millisecond.ToString("d3"), i);

                            string textbody = oMail.TextBody;
                            string htmlbody = oMail.HtmlBody;
                            HeaderCollection bodyHeader = oMail.BodyHeaders;
                            BodyTextFormat bodyFormat = oMail.OriginalBodyFormat;
                            DecompressedData = SchedulerDB.DecompressString(textbody);

                            if (!string.IsNullOrEmpty(DecompressedData) && DecompressedData.Contains(","))
                            {
                                string a = string.Empty;
                                string[] arrData = DecompressedData.Split(',');
                                Dictionary<string, dynamic> dictFromData = SchedulerDB.getFormData(arrData);
                                var tupledata = SchedulerDB.SavetoDB(dictFromData);

                                string strdata = tupledata.Item1;

                                #region Attachements
                                try
                                {
                                    EAGetMail.Attachment[] attach = oMail.Attachments;
                                    if (attach.Length > 0)
                                    {
                                        byte[] buffer = attach[0].Content;

                                        string fileName = attach[0].Name;
                                        string contentType = attach[0].ContentType;

                                        string code = Encoding.UTF8.GetString(buffer);

                                        SchedulerDB.StoreFileToDatabase(fileName, tupledata.Item2, buffer, contentType);
                                    }
                                }
                                catch (Exception error)
                                {

                                }
                                #endregion
                            }

                            Imap4Folder destFolder = null;
                            Imap4Folder[] folders = oClient.Imap4Folders;
                            int count = folders.Length;

                            for (int j = 0; j < count; j++)
                            {
                                Imap4Folder folder = folders[j];
                                if (String.Compare("Morning", folder.Name, true) == 0)
                                {
                                    //find "Deleted Items" folder
                                    destFolder = folder;
                                    break;
                                }
                            }

                            // Save email to local disk
                            //oMail.SaveAs(fileName, true);
                            if (destFolder != null)
                            {
                                oClient.Move(info, destFolder);

                                // Mark email as deleted from POP3 server.
                                oClient.Delete(info);

                                // Quit and purge emails marked as deleted from Hotmail/MSN Live server.
                                oClient.Quit();

                                Recursing(pairs);
                            }
                            else
                            {
                                Imap4Folder folder = oClient.CreateFolder(null, "Morning");
                                oClient.Move(info, folder);

                                // Mark email as deleted from POP3 server.
                                oClient.Delete(info);

                                // Quit and purge emails marked as deleted from Hotmail/MSN Live server.
                                oClient.Quit();

                                Recursing(pairs);
                            }


                        }
                        else if (oMail.Subject.ToString().Contains("Noon_Report") && !oMail.Subject.ToString().Contains("Reply From Shore"))
                        {
                            MailAddress mailFrom = oMail.From;
                            MailAddress mailTo = oMail.From;
                            MailAddress[] mailCC = oMail.Cc;

                            // Generate an email file name based on date time.
                            System.DateTime d = System.DateTime.Now;
                            System.Globalization.CultureInfo cur = new System.Globalization.CultureInfo("en-US");
                            string sdate = d.ToString("yyyyMMddHHmmss", cur);
                            //string fileName = String.Format("{0}\\{1}{2}{3}.eml",
                            //    mailbox, sdate, d.Millisecond.ToString("d3"), i);

                            string textbody = oMail.TextBody;
                            string htmlbody = oMail.HtmlBody;
                            HeaderCollection bodyHeader = oMail.BodyHeaders;
                            BodyTextFormat bodyFormat = oMail.OriginalBodyFormat;
                            DecompressedData = SchedulerDB.DecompressString(textbody);

                            if (!string.IsNullOrEmpty(DecompressedData) && DecompressedData.Contains(","))
                            {
                                string a = string.Empty;
                                string[] arrData = DecompressedData.Split(',');
                                Dictionary<string, dynamic> dictFromData = SchedulerDB.getFormData(arrData);
                                var tupledata = SchedulerDB.SavetoDB(dictFromData);

                                string strdata = tupledata.Item1;

                                #region Attachments   
                                try
                                {
                                    EAGetMail.Attachment[] attach = oMail.Attachments;
                                    if (attach.Length > 0)
                                    {
                                        byte[] buffer = attach[0].Content;
                                        string fileName = attach[0].Name;
                                        string contentType = attach[0].ContentType;

                                        string code = Encoding.UTF8.GetString(buffer);

                                        SchedulerDB.StoreFileToDatabase(fileName, tupledata.Item2, buffer, contentType);
                                    }
                                }
                                catch (Exception error)
                                { }
                            }

                            #endregion

                            Imap4Folder destFolder = null;
                            Imap4Folder[] folders = oClient.Imap4Folders;
                            int count = folders.Length;

                            for (int j = 0; j < count; j++)
                            {
                                Imap4Folder folder = folders[j];
                                if (String.Compare("Noon", folder.Name, true) == 0)
                                {
                                    //find "Deleted Items" folder
                                    destFolder = folder;
                                    break;
                                }
                            }

                            // Save email to local disk
                            //oMail.SaveAs(fileName, true);
                            if (destFolder != null)
                            {
                                oClient.Move(info, destFolder);

                                // Mark email as deleted from POP3 server.
                                oClient.Delete(info);

                                // Quit and purge emails marked as deleted from Hotmail/MSN Live server.
                                oClient.Quit();

                                Recursing(pairs);
                            }
                            else
                            {
                                Imap4Folder folder = oClient.CreateFolder(null, "Noon");
                                oClient.Move(info, folder);

                                // Mark email as deleted from POP3 server.
                                oClient.Delete(info);

                                // Quit and purge emails marked as deleted from Hotmail/MSN Live server.
                                oClient.Quit();

                                Recursing(pairs);
                            }
                        }
                    }
                }
                catch(Exception err)
                {
                    return;
                }
            }
        }
    }
}
