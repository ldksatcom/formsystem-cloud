﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.LDKSatcom.Models;
using FormUpdated.Models;

namespace FormUpdated.Controllers
{
    public class CrewController : Controller
    {
        private NoonReportFormatEntities db = new NoonReportFormatEntities();

        // GET: Crew
        public ActionResult Index()
        {
            return View(db.CrewDetails.ToList());
        }

        // GET: Crew/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CrewDetail crewDetail = db.CrewDetails.Find(id);
            if (crewDetail == null)
            {
                return HttpNotFound();
            }
            return View(crewDetail);
        }

        // GET: Crew/Create
        public ActionResult Create()
        {
            if (!string.IsNullOrEmpty(MySession.curVessel) && !string.IsNullOrEmpty(MySession.curCompany))
            {
                CrewDetail crewDetail = new CrewDetail();
                string OrderKeyShip = string.Empty;
                string OrderKeyShore = string.Empty;

                List<CrewDetail> lstCrew = db.CrewDetails.Where(t => t.CompanyName.Equals(MySession.curCompany) && t.ShipName.Equals(MySession.curVessel)).ToList();
                foreach (var crews in lstCrew)
                {
                    if (crews.Type.ToLower().Equals("ship") && !string.IsNullOrEmpty(crews.OrderKey))
                        OrderKeyShip = crews.OrderKey;
                    else if (crews.Type.ToLower().Equals("shore") && !string.IsNullOrEmpty(crews.OrderKey))
                        OrderKeyShore = crews.OrderKey;

                    if (!string.IsNullOrEmpty(OrderKeyShip))
                        crewDetail.OrderKey = OrderKeyShip;
                    else if (!string.IsNullOrEmpty(OrderKeyShore))
                        crewDetail.OrderKey = OrderKeyShore;
                }
                return View(crewDetail);
            }

            return View();
        }

        // POST: Crew/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Title,DateofJoining,LeavingDate,AccessLevel,IsSuspension,UserName,Password,EmailID,CompanyName,ShipName,Type,Tomail,CCMail,OrderKey,isAdmin")] CrewDetail crewDetail)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(crewDetail.OrderKey))
                {
                    crewDetail.OrderKey = Helper.GetOrderID();
                }
                db.CrewDetails.Add(crewDetail);
                db.SaveChanges();
                ViewBag.OrderKeys = crewDetail.OrderKey;
                return View("Create");
            }

            return View(crewDetail);
        }

        // GET: Crew/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CrewDetail crewDetail = db.CrewDetails.Find(id);
            if (crewDetail == null)
            {
                return HttpNotFound();
            }

            TempData["DateofJoining"] = crewDetail.DateofJoining;
            TempData["LeavingDate"] = crewDetail.LeavingDate;
            return View(crewDetail);
        }

        // POST: Crew/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Title,DateofJoining,LeavingDate,AccessLevel,IsSuspension,UserName,Password,EmailID,CompanyName,ShipName,Type,Tomail,CCMail,isAdmin")] CrewDetail crewDetail)
        {
            if (ModelState.IsValid)
            {
                if (TempData["DateofJoining"] != null && crewDetail.DateofJoining == null)
                    crewDetail.DateofJoining = Convert.ToDateTime(TempData["DateofJoining"]);

                if (TempData["LeavingDate"] != null && crewDetail.LeavingDate == null)
                    crewDetail.LeavingDate = Convert.ToDateTime(TempData["LeavingDate"]);

                db.Entry(crewDetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(crewDetail);
        }

        // GET: Crew/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CrewDetail crewDetail = db.CrewDetails.Find(id);
            if (crewDetail == null)
            {
                return HttpNotFound();
            }
            return View(crewDetail);
        }

        // POST: Crew/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CrewDetail crewDetail = db.CrewDetails.Find(id);
            db.CrewDetails.Remove(crewDetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
