﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormUpdated.Areas.LDKSatcom.Models;
using FormUpdated.Models;

namespace FormUpdated.Controllers
{
    [Authorize]
    public class ReportsController : Controller
    {
        private NoonReportFormatEntities db = new NoonReportFormatEntities();

        public ActionResult Login()
        {
            return View();
        }

        // GET: Reports
        // GET: demoNoonReports
        public ActionResult Index(int id)
        {
            if (id == 1)
                return View(db.demoNoonReports.Where(t => t.ReportType.Equals("Morning")).ToList());
            else if (id == 2)
                return View(db.demoNoonReports.Where(t => t.ReportType.Equals("Noon")).ToList());
            else
                return View(db.demoNoonReports.ToList());
        }

        // GET: Reports/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            demoNoonReport demoNoonReport = db.demoNoonReports.Find(id);
            if (demoNoonReport == null)
            {
                return HttpNotFound();
            }
            return View(demoNoonReport);
        }

        // GET: Reports/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Reports/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,ReportType,VoyageNo,ShipName,ReportDate,CallSign,Latitude,Longitude,NPTime,Remarks,VesselHeading,CTime,Remarks1,AverageSpeed_At_Noons,Forvoyagesincelastfullaway,MFOForMainEngine,MFOForBoiler,MGOForMainEngine,MGOForAuxiliaryEngines,MGOForBoiler,TotalMFO,TotalMGO,MFO,MGO,LOMainEnginesystem_Sump,LOMainEnginesystem_StorageTank,isUpdated")] demoNoonReport demoNoonReport)
        {
            if (ModelState.IsValid)
            {
                db.demoNoonReports.Add(demoNoonReport);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(demoNoonReport);
        }

        // GET: Reports/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            demoNoonReport demoNoonReport = db.demoNoonReports.Find(id);
            if (demoNoonReport == null)
            {
                return HttpNotFound();
            }
            return View(demoNoonReport);
        }

        // POST: Reports/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,ReportType,VoyageNo,ShipName,ReportDate,CallSign,Latitude,Longitude,NPTime,Remarks,VesselHeading,CTime,Remarks1,AverageSpeed_At_Noons,Forvoyagesincelastfullaway,MFOForMainEngine,MFOForBoiler,MGOForMainEngine,MGOForAuxiliaryEngines,MGOForBoiler,TotalMFO,TotalMGO,MFO,MGO,LOMainEnginesystem_Sump,LOMainEnginesystem_StorageTank,isUpdated")] demoNoonReport demoNoonReport)
        {
            if (ModelState.IsValid)
            {
                db.Entry(demoNoonReport).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(demoNoonReport);
        }

        // GET: Reports/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            demoNoonReport demoNoonReport = db.demoNoonReports.Find(id);
            if (demoNoonReport == null)
            {
                return HttpNotFound();
            }
            return View(demoNoonReport);
        }

        // POST: Reports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            demoNoonReport demoNoonReport = db.demoNoonReports.Find(id);
            db.demoNoonReports.Remove(demoNoonReport);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
