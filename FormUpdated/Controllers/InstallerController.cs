﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using FormUpdated.Models;
using FormUpdated.Areas.LDKSatcom.Models;

namespace FormUpdated.Controllers
{
    public class InstallerController : Controller
    {
        // GET: Installer
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Download()
        {
            string machineID = string.Empty;
            try
            {
                using (NoonReportFormatEntities db = new NoonReportFormatEntities())
                {
                    CrewDetail crewDetail = db.CrewDetails.FirstOrDefault(t => t.UserName.Equals(User.Identity.Name));
                    if (crewDetail != null)
                    {
                        machineID = crewDetail.MachineID;
                    }
                    
                }
            }
            catch { }
            if (!string.IsNullOrEmpty(machineID))
                ViewBag.MachineID = machineID;
            else
                ViewBag.MachineID = null;

            ViewBag.isInsalled = "true";

            return View("_Download");
        }


        // GET: File
        public FilePathResult Install()
        {
            try
            {
                var filename = "DemoFormSystem.application";
                var dir = Server.MapPath("/WinFormApp");
                var path = Path.Combine(dir, filename);
                if (User != null && User.Identity.Name != null)
                {
                    try
                    {
                        if (!MySession.isAdmin)
                        {
                            using (NoonReportFormatEntities db = new NoonReportFormatEntities())
                            {
                                CrewDetail crewDetail = db.CrewDetails.FirstOrDefault(t => t.UserName.Equals(User.Identity.Name));
                                if (crewDetail != null)
                                {
                                    crewDetail.MachineID = "XXXX";
                                }
                                db.SaveChanges();
                            }
                        }
                    }
                    catch { }
                }
                ViewBag.isInsalled = "true";
                return File(path, GetMimeType(Path.GetExtension(filename)));
            }
            catch (Exception error)
            {
                ViewBag.isInsalled = "false";
                var dir = Server.MapPath("/WinFormApp");
                var path = Path.Combine(dir, "DemoFormSystem.application");
                return File(path, GetMimeType(Path.GetExtension("DemoFormSystem.application")));
            }
        }

        //[System.Web.Services.WebMethod]
        public ActionResult Robert()
        {
            string DecompressedData = string.Empty;
            object threadLock = new object();
            try
            {
                SingletonConcept singletonConcept = SingletonConcept.Instance;
                lock (singletonConcept)
                {
                    List<Dictionary<string, string>> dict = SchedulerDB.GetEmailPasswords();

                    foreach (Dictionary<string, string> pairs in dict)
                    {
                        if (pairs.ContainsKey("EmailID") && pairs.ContainsKey("Password") && !string.IsNullOrEmpty(pairs["EmailID"]) && !string.IsNullOrEmpty(pairs["Password"]))
                        {
                            singletonConcept.Recursing(pairs);
                        }
                    }
                }
            }
            catch (Exception error)
            {

            }
            return new EmptyResult();
        }

        private string GetMimeType(string extension)
        {
            if (extension == ".application" || extension == ".manifest")
                return "application/x-ms-application";
            else if (extension == ".deploy")
                return "application/octet-stream";
            else
                return "application/x-msdownload";
        }
    }
}