﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FormBusinessAccessLibrary;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Compressed_Library;
using System.Text;
using System.IO;
using System.IO.Compression;
//using Microsoft.Office.Interop.Outlook;
using System.Net.Mail;
using System.Net.Sockets;
using System.Net;
using EAGetMail;
using Rotativa;

namespace FormUpdated.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return new ActionAsPdf("Update");
        }

        public ActionResult GeneratePDF()
        {
            ModelState.Clear();
            Models.NoonReports noonReports = ModelUpdates.UpdateForm();
            return View(noonReports);
        }

        public string GeneratePDFs( string html)
        {
             
            
            //header("Content-Type: application/pdf"); //check this is the proper header for pdf
            //header("Content-Disposition: attachment; filename='some.pdf';");
            return "";
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Create()
        {
            Models.NoonReports objNoon = new Models.NoonReports();
            if (TempData["noonReports"] != null)
                objNoon = (Models.NoonReports)TempData["noonReports"];

            return View(objNoon);
        }
        [HttpPost]
        public ActionResult Save(Models.NoonReports objNoonReport)
        {
            Dictionary<string, dynamic> dict = Helper.getFormData(objNoonReport);
            FormInsert.connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            var data = FormInsert.CreateValue_Fields(dict);

            byte[] bufferOriginal = Encoding.ASCII.GetBytes(data.Item1);
            string originalFileSize = BytestoKB.SizeSuffix(bufferOriginal.Length);
            string tempPath = Path.GetTempPath() + objNoonReport.ShipName + "_" + Guid.NewGuid().ToString() + ".txt";

            string returnstring = Compress_DeCompress.CompressString(data.Item1);
            System.IO.File.WriteAllText(tempPath, returnstring);

            byte[] buffer = Convert.FromBase64String(returnstring);
            Int64 bufferSize = buffer.Length;
            String CompressedByteSize = BytestoKB.SizeSuffix(bufferSize);

            Int64 compressedLength = ((byte[])Encoding.ASCII.GetBytes(data.Item1)).Length - bufferSize;
            string CompressedPercentage = BytestoKB.GetPercentage(bufferOriginal.Length, compressedLength);
            string attachFileName = tempPath;
            string NRID = Convert.ToString(data.Item2);

            string[] temp = { attachFileName, CompressedPercentage, originalFileSize, CompressedByteSize, NRID };

            TempData["EmailFormat"] = temp;
            return RedirectToAction("Email", "Home");
        }

        public ActionResult Email()
        {
            if (TempData["EmailFormat"] == null)
                return RedirectToAction("Create");

            string[] temp = (string[])TempData["EmailFormat"];
            TempData.Keep();
            Models.EmailFormat emailFormat = new Models.EmailFormat();

            if (temp == null || temp.Length == 0)
                return RedirectToAction("Create");
            else
            {
                emailFormat.Attachement = temp[0];
                emailFormat.CompressedPercentage = temp[1];
                emailFormat.OriginalFileSize = temp[2];
                emailFormat.CompressedFileSize = temp[3];
                emailFormat.ID = Convert.ToInt32(temp[4]);
                return View(emailFormat);
            }
        }

        public ActionResult Load()
        {
            string DecompressedData = string.Empty;
            Models.NoonReports noonReports = new Models.NoonReports();
            try
            {
                string curpath = Path.GetDirectoryName(Server.MapPath("Inbox"));
                string mailbox = string.Format("{0}\\inbox", curpath);

                if (!Directory.Exists(mailbox))
                {
                    Directory.CreateDirectory(mailbox);
                }
                MailServer oServer = new MailServer("imap.gmail.com", "karthi@smtspl.com", "smtskarthi@2018", ServerProtocol.Imap4);

                MailClient oClient = new MailClient("TryIt");

                oServer.SSLConnection = true;
                oServer.Port = 993;

                try
                {
                    oClient.Connect(oServer);
                    MailInfo[] infos = oClient.GetMailInfos();
                    int infosLength = infos.Length;
                    for (int i = infos.Length - 1; i < infosLength; i--)
                    {
                        MailInfo info = infos[i];
                        Console.WriteLine("Index: {0}; Size: {1}; UIDL: {2}",
                            info.Index, info.Size, info.UIDL);
                        Mail oMail = oClient.GetMail(info);

                        Console.WriteLine("From: {0}", oMail.From.ToString());
                        Console.WriteLine("Subject: {0}\r\n", oMail.Subject);

                        if (oMail.From.ToString().Contains("guru@smtspl.com") || oMail.From.ToString().Contains("karthi@smtspl.com"))
                        {
                            EAGetMail.Attachment[] attach = oMail.Attachments;
                            if (attach == null || attach.Length == 0)
                                continue;

                            byte[] buffer = attach[0].Content;

                            try
                            {
                                string code = Encoding.UTF8.GetString(buffer);
                                DecompressedData = DecompressString(code);
                            }
                            catch
                            { }
                            System.DateTime d = System.DateTime.Now;
                            System.Globalization.CultureInfo cur = new
                                System.Globalization.CultureInfo("en-US");
                            string sdate = d.ToString("yyyyMMddHHmmss", cur);
                            string fileName = String.Format("{0}\\{1}{2}{3}.eml",
                                mailbox, sdate, d.Millisecond.ToString("d3"), i);
                            oMail.SaveAs(fileName, true);
                            if (attach.Length > 0)
                            {

                                //oClient.Delete(info);
                            }
                            break;
                        }
                    }
                    oClient.Quit();
                }
                catch (System.Exception ex)
                { }

                

                if (!string.IsNullOrEmpty(DecompressedData) && DecompressedData.Contains(","))
                {
                    string[] arrData = DecompressedData.Split(',');
                    Dictionary<string, dynamic> dictFromData = Helper.getFormData(null, arrData);
                    FormInsert.connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    var strdata = FormInsert.CreateValue_Fields(dictFromData);

                    noonReports = Helper.BindForm(dictFromData);

                    TempData["noonReports"] = noonReports;
                }
                else
                {
                    FormInsert.connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    DecompressedData = FormInsert.getCompressedData();

                    string[] arrData = DecompressedData.Split(',');
                    Dictionary<string, dynamic> dictFromData = Helper.getFormData(null, arrData);
                    FormInsert.connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    var strdata = FormInsert.CreateValue_Fields(dictFromData);

                    noonReports = Helper.BindForm(dictFromData);

                    TempData["noonReports"] = noonReports;
                }
            }
            catch {
                FormInsert.connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                DecompressedData = FormInsert.getCompressedData();

                string[] arrData = DecompressedData.Split(',');
                Dictionary<string, dynamic> dictFromData = Helper.getFormData(null, arrData);
                FormInsert.connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                var strdata = FormInsert.CreateValue_Fields(dictFromData);

                noonReports = Helper.BindForm(dictFromData);

                TempData["noonReports"] = noonReports;
            }
            return View(noonReports);
        }

        public static string DecompressString(string compressedtext)
        {
            byte[] gZipBuffer = Convert.FromBase64String(compressedtext);
            using (var memoryStream = new MemoryStream())
            {
                int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
                memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

                var buffer = new byte[dataLength];

                memoryStream.Position = 0;
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                {
                    gZipStream.Read(buffer, 0, buffer.Length);
                }
                return Encoding.UTF8.GetString(buffer);
            }
        }

        [HttpGet]
        public ActionResult Update()
       {

            //ModelState.Clear();
            Models.NoonReports noonReports = ModelUpdates.UpdateForm();
            return View(noonReports);
        }

        public ActionResult Send(Models.EmailFormat emailFormat)
        {
            if(TempData["EmailFormat"] == null)
                return RedirectToAction("Create");

            string[] temp = (string[])TempData["EmailFormat"];

            if (temp == null || temp.Length == 0)
                return RedirectToAction("Create");
            else
            {
                emailFormat.ID = Convert.ToInt32(temp[4]);
                emailFormat.Attachement = temp[0];
                emailFormat.CompressedPercentage = temp[1];
                emailFormat.OriginalFileSize = temp[2];
                emailFormat.CompressedFileSize = temp[3];
               // bool isSuccess = Helper.SendMail(emailFormat);
                int id = emailFormat.ID;
                return new ActionAsPdf("Update");
            }
        }
    }
}
        
    
