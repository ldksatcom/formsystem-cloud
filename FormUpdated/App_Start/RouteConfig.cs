﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FormUpdated
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

           // routes.MapRoute(
           //    name: "ShipDash",
           //    url: "{demoNoonReports}/{shipDashboard}/{companyName}",
           //    defaults: new { controller = "demoNoonReports", action = "shipDashboard", companyName = UrlParameter.Optional }
           //);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "demoNoonReports", action = "Login", id = UrlParameter.Optional }
            );
        }
    }
}
